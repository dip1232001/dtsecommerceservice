<?php
$page = 'category-order';
include('header-order.php');
?>
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class="modal-title ecom-h2" id="exampleModalLabel">Add New Category</div>
      </div>
      <div class="modal-body">
        <div class="input-group input-group-lg ecom-modal-content"> <span class="input-group-addon" id="sizing-addon1">Category Name</span>
        <input type="text" class="form-control" placeholder="Category Name" aria-describedby="sizing-addon1">
      </div>
      <div class="cat-devition">
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6">
            <div class="ecom-h3">Sort Products Alphabetically</div>
            <div class="col-lg-2 col-md-2 col-sm-3">
              <div class="radio">
                <label>
                  <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
                Yes </label>
              </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-3">
              <div class="radio">
                <label>
                  <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                No </label>
              </div>
            </div>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6">
            <div class="ecom-h3">Status</div>
            <div class="col-lg-2 col-md-2 col-sm-4">
              <div class="radio">
                <label>
                  <input type="radio" name="optionsRadios" id="optionsRadios3" value="option1" checked>
                Active </label>
              </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-4">
              <div class="radio">
                <label>
                  <input type="radio" name="optionsRadios" id="optionsRadios4" value="option2">
                Inactive </label>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-3 col-lg-3 col-sm-3">
            <div class="ecom-h3">Thumbnail</div>
            <div class="ecom-thumbnail"><i class="fa fa-file-image-o" aria-hidden="true"></i></div>
            <div class="panel-body img-file-tab">
              <div>
                <span class="btn btn-default btn-file img-select-btn" style="overflow: hidden;position: relative;">
                  <span>Change</span>
                  <input type="file" name="img-file-input">
                </span>
                <span class="btn btn-default img-remove-btn" style="display: inline-block;">Remove</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="modal-footer">
      <button type="button" class="btn btn-success">Save</button>
      <button type="button" class="btn btn-danger" data-dismiss="modal">Cancle</button>
    </div>
  </div>
</div>
</div>
<section class="container-fluid">
  <div class="ecom-category-area">
    <div class="row">
      <h3 class="pos-rel">
      <span>Category</span>
      </h3>
      <div class="pull-right">
        <button type="button" data-toggle="modal" data-target=".bs-example-modal-lg" class="btn btn-danger">Add</button>
      </div>
    </div>
  </div>
  <div class="ecom-product-item">
    <div class="dirCateItems">
      <div class="dir_bx narrowed stackorder biggerstackorder">
        <div class="ecom-product"> <img src="img/product01.jpg" alt=""/> </div>
        <a href="JavaScript:Void(0);" class="ecom-catnumber">Category 1</a> <a href="sub-category.php" class="sub-catnumber">1 Sub Category</a> <a href="JavaScript:Void(0);" class="sub-catnumber">0 Product</a>
        <span class="label label-success">Active</span>
        <span class="label label-danger hidden">Inactive</span>
        <div class="ecom-action"> <a href="#" class="action-setting"><i class="fa fa-cogs" aria-hidden="true"></i></a> </div>
        <div class="dirCateItemsOverlay">
          <a href="#" class="btn btn-danger" data-toggle="modal" data-target=".bs-example-modal-lg" > Edit</a>
          <a href="#" class="btn btn-danger"> Inactive</a>
          <a href="#" class="btn btn-danger"> Sub Categories</a>
          <a href="#" class="btn btn-danger"> Manage Products</a>
          <a href="#" class="btn btn-danger"> Delete</a>
          <a href="#" class="hideOverlay"> Close</a>
        </div>
      </div>
    </div>
    <div class="dirCateItems">
      <div class="dir_bx narrowed stackorder biggerstackorder">
        <div class="ecom-product"> <img src="img/product01.jpg" alt=""/> </div>
        <a href="JavaScript:Void(0);" class="ecom-catnumber">Category 2</a> <a href="sub-category-order.php" class="sub-catnumber">1 Sub Category</a> <a href="JavaScript:Void(0);" class="sub-catnumber">0 Product</a>
        <span class="label label-success hidden">Active</span>
        <span class="label label-danger">Inactive</span>
        <div class="ecom-action"><a href="#" class="action-setting"><i class="fa fa-cogs" aria-hidden="true"></i></a></div>
      </div>
    </div>
    <div class="dirCateItems">
      <div class="dir_bx narrowed stackorder biggerstackorder">
        <div class="ecom-product"> <img src="img/product01.jpg" alt=""/> </div>
        <a href="JavaScript:Void(0);" class="ecom-catnumber">Category 3</a> <a href="sub-category.php" class="sub-catnumber">1 Sub Category</a> <a href="JavaScript:Void(0);" class="sub-catnumber">0 Product</a>
        <span class="label label-success">Active</span>
        <span class="label label-danger hidden">Inactive</span>
        <div class="ecom-action"><a href="#" class="action-setting"><i class="fa fa-cogs" aria-hidden="true"></i></a></div>
      </div>
    </div>
    <div class="dirCateItems">
      <div class="dir_bx narrowed stackorder biggerstackorder">
        <div class="ecom-product"> <img src="img/product01.jpg" alt=""/> </div>
        <a href="JavaScript:Void(0);" class="ecom-catnumber">Category 4</a> <a href="sub-category.php" class="sub-catnumber">1 Sub Category</a> <a href="JavaScript:Void(0);" class="sub-catnumber">0 Product</a>
        <span class="label label-success">Active</span>
        <span class="label label-danger hidden">Inactive</span>
        <div class="ecom-action"><a href="#" class="action-setting"><i class="fa fa-cogs" aria-hidden="true"></i></a></div>
      </div>
    </div>
    <div class="dirCateItems">
      <div class="dir_bx narrowed stackorder biggerstackorder">
        <div class="ecom-product"> <img src="img/product01.jpg" alt=""/> </div>
        <a href="JavaScript:Void(0);" class="ecom-catnumber">Category 4</a> <a href="sub-category.php" class="sub-catnumber">1 Sub Category</a> <a href="JavaScript:Void(0);" class="sub-catnumber">0 Product</a>
        <span class="label label-success">Active</span>
        <span class="label label-danger hidden">Inactive</span>
        <div class="ecom-action"><a href="#" class="action-setting"><i class="fa fa-cogs" aria-hidden="true"></i></a></div>
      </div>
    </div>
    <div class="dirCateItems">
      <div class="dir_bx narrowed stackorder biggerstackorder">
        <div class="ecom-product"> <img src="img/product01.jpg" alt=""/> </div>
        <a href="JavaScript:Void(0);" class="ecom-catnumber">Category 4</a> <a href="sub-category.php" class="sub-catnumber">1 Sub Category</a> <a href="JavaScript:Void(0);" class="sub-catnumber">0 Product</a>
        <span class="label label-success">Active</span>
        <span class="label label-danger hidden">Inactive</span>
        <div class="ecom-action"><a href="#" class="action-setting"><i class="fa fa-cogs" aria-hidden="true"></i></a></div>
      </div>
    </div>
    <div class="dirCateItems">
      <div class="dir_bx narrowed stackorder biggerstackorder">
        <div class="ecom-product"> <img src="img/product01.jpg" alt=""/> </div>
        <a href="JavaScript:Void(0);" class="ecom-catnumber">Category 5</a> <a href="sub-category.php" class="sub-catnumber">1 Sub Category</a> <a href="JavaScript:Void(0);" class="sub-catnumber">0 Product</a>
        <span class="label label-success">Active</span>
        <span class="label label-danger hidden">Inactive</span>
        <div class="ecom-action"><a href="#" class="action-setting"><i class="fa fa-cogs" aria-hidden="true"></i></a></div>
      </div>
    </div>
    <div class="dirCateItems">
      <div class="dir_bx narrowed stackorder biggerstackorder">
        <div class="ecom-product"> <img src="img/product01.jpg" alt=""/> </div>
        <a href="JavaScript:Void(0);" class="ecom-catnumber">Category 6</a> <a href="sub-category.php" class="sub-catnumber">1 Sub Category</a> <a href="JavaScript:Void(0);" class="sub-catnumber">0 Product</a>
        <span class="label label-success">Active</span>
        <span class="label label-danger hidden">Inactive</span>
        <div class="ecom-action"><a href="#" class="action-setting"><i class="fa fa-cogs" aria-hidden="true"></i></a></div>
      </div>
    </div>
  </div>
</section>
<?php include 'footer.php'; ?>