<?php
$page = 'manage-customers';
include( 'header.php' );
?>
<div class="modal fade" tabindex="-1" role="dialog" id="myModal" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Confirm</h4>
			</div>
			<div class="modal-body" style="font-size:14px;">
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<div class="panel panel-default font-12">
							<div class="panel-heading"><strong>Order Details</strong>
							</div>
							<div class="table-responsive">
								<table class="table table-condensed">
									<tbody>
										<tr>
											<th class="active" width="10px"></th>
											<th class="active">Product Image</th>
											<th class="active">Product Name </th>
											<th class="active">Qty</th>
											<th class="active">Price</th>
											<th class="active">Total</th>
										</tr>
										<tr ng-repeat="prod in orderDet.productListArr" class="ng-scope">
											<td></td>
											<td>
												<img width="70" height="70" src="http://snappy.appypie.com/media/user_space/02e33100e77d/ecomm/ecomm_1499025767_7337.jpg">
											</td>
											<td>Prod 1 <br>Color:Blue</td>
											<td>2</td>
											<td>$50.00</td>
											<td>$100</td>
										</tr>
										<tr>
											<td></td>
											<td>
												<imgwidth="70" height="70" src="http://snappy.appypie.com/media/user_space/02e33100e77d/ecomm/ecomm_1499025767_7337.jpg">
											</td>
											<td>Prod 1 <br>Color:Red</td>
											<td>3</td>
											<td class="ng-binding">$40.00</td>
											<td class="ng-binding">$120</td>
										</tr>
										<tr>
											<td colspan="6">
												<table width="50%" class="table table-condensed offsetbottomnone" style="margin-top:-6px !important;">
													<tbody>
														<tr>
															<td width="87%" align="right">Sub total</td>
															<td width="" align="left"></td>
															<td width="" align="left">$220.00</td>
														</tr>
														<tr>
															<td align="right">Tax</td>
															<td align="left"></td>
															<td align="left">$0.00</td>
														</tr>
														<tr>
															<td align="right"> Coupon</td>
															<td align="left"></td>
															<td align="left">$22.00</td>
														</tr>
														<tr ng-repeat="misctax in orderDet.orderTotalArr.mtax" class="ng-scope">
															<td align="right">GST</td>
															<td align="left"></td>
															<td align="left">$39.60</td>
														</tr>
														<tr>
															<td align="right">Discount</td>
															<td align="left"></td>
															<td align="left">$0.00</td>
														</tr>
														<tr>
															<td align="right">SHIPPING</td>
															<td align="left"></td>
															<td align="left">$23.76</td>
														</tr>
														<tr>
															<td align="right" class="active"><strong>Grand Total</strong></td>
															<td align="left" class="active"></td>
															<td align="left" class="active"><strong>$261.36</strong></td>
														</tr>
													</tbody>
												</table>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6 col-sm-6">
						<div class="panel panel-default font-12 min-height-ecom">
							<div class="panel-heading"><strong>Order Information</strong>
							</div>
							<div class="panel-body">
								<div class="row offsetbottom10">
									<div class="col-md-12">
										<label><strong>Order Date</strong></label> : <span class="ng-binding">03 Jul 2017 15:28:22</span>
									</div>
								</div>
								<div class="row offsetbottom10">
									<div class="col-md-12"><strong>Order Status </strong> : <span>Processing</span> </div>
								</div>
								<div class="row offsetbottom10">
									<div class="col-md-12"><strong>Payment Method </strong> : <span>Cash on Delivery</span> </div>
								</div>
								<div class="row">
									<div class="col-md-12"><strong>Payment Status </strong> : <span> In Process</span> </div>
									<div class="col-md-12"><strong>Payment Status </strong> : <span> Complete</span> </div>
								</div>
								<div class="row">
									<div class="col-md-12"><strong>Transaction Id </strong> : <span>-</span> </div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6 col-sm-6 ">
						<div class="panel panel-default min-height-ecom">
							<div class="panel-heading"><strong>Customer Information</strong>
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-md-12"><strong>Customer Name </strong> : <span>test</span> </div>
								</div>
								<div class="row">
									<div class="col-md-12"><strong>Email</strong> : <span>dts.ritwik@dreamztech.com</span> </div>
								</div>
								<div class="row">
									<div class="col-md-12"><strong>Phone No</strong> : <span>9836229486</span> </div>
								</div>
								<div class="row">
									<div class="col-md-12">&nbsp;</div>
								</div>
								<div class="row">
									<div class="col-md-12">&nbsp;</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6 col-sm-6">
						<div class="panel panel-default min-height-ecom">
							<div class="panel-heading"><strong>Billing Address</strong>
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-md-12">
										<div>test,</div>
										<div>321 ,</div>
										<div>231 132131 sfds ()</div>
										<div>231 132131 sfds</div>
										<div>Telephone : 12312412</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6 col-sm-6">
						<div class="panel panel-default font-12 min-height-ecom">
							<div class="panel-heading"><strong>Shipping Address</strong>
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-md-12">
										<div>test,</div>
										<div>321 ,</div>
										<div>231 132131 sfds (AF)</div>
										<div>231 132131 sfds</div>
										<div>Telephone : 12312412</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<div class="panel panel-default font-12">
							<div class="panel-heading"><strong>Comments History</strong>
							</div>
							<div class="panel-body"> <strong>Status</strong> : <span>Pending</span><br>
								<strong>Comments</strong> : <span>Order Placed</span> <br>
								<strong>Date</strong> : <span>03 Jul 2017 15:28:22</span> </div>
							<div class="panel-body"> <strong>Status</strong> : <span class="ng-binding">Processing</span><br>
								<strong>Comments</strong> : <span>Payment Made</span> <br>
								<strong>Date</strong> : <span>03 Jul 2017 15:29:41</span> </div>
							<div class="panel-body"> <strong>Status</strong> : <span>Processing</span><br>
								<strong>Comments</strong> : <span>Invoice Created</span> <br>
								<strong>Date</strong> : <span>03 Jul 2017 15:29:46</span> </div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<div class="panel panel-default font-12 min-height-ecom">
							<div class="panel-heading"><strong>Shipping Information</strong>
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-md-6 col-sm-6 col-lg-6">
										<label>Carrier <span>*</span></label>
										<div class="form-group">
											<select class="form-control m-bot15">
												<option>1</option>
												<option>2</option>
												<option>3</option>
												<option>4</option>
												<option>5</option>
											</select>
										</div>
									</div>
									<div class="col-md-6 col-sm-6 col-lg-6">
										<label>Tracking URl <span>*</span></label>
										<div class="form-group">
											<input type="text" class="form-control">
										</div>
									</div>
									<div class="col-md-6 col-sm-6 col-lg-6">
										<label>Title <span>*</span></label>
										<div class="form-group">
											<input type="text" class="form-control">
										</div>
									</div>
									<div class="col-md-6 col-sm-6 col-lg-6">
										<label>Tracking Number <span>*</span></label>
										<div class="form-group">
											<input type="text" class="form-control">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		
		<div class="clearfix"></div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default hidden">Ship</button>
			<button type="button" class="btn btn-primary">Print Invoice</button>
		</div>
	</div>
</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="myModal1" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Confirm</h4>
			</div>
			<div class="modal-body">
				<p>Are you sure you want to delete selected order(s)?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-primary">Confirm</button>
			</div>
		</div>
	</div>
</div>
<section class="container-fluid">
	<div class="ecom-category-area">
		<div class="row">
			<h3 class="pos-rel">
      <span>Customers Details</span>
      </h3>
		
			<div>
				<div class="col-lg-6 col-md-6 col-sm-6">
					<div><a href="manage-customers.php"><i class="fa fa-angle-left" aria-hidden="true"></i> Back to Manage Customer</a>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	</div>
	<div class="ecom-product-item">
		<div class="col-md-12 col-sm-12 col-lg-12">
			<div class="customer-details">
				<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="headingOne">
							<h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Personal Information
            </a>
            </h4>
						
						</div>
						<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
							<div class="panel-body">
								<table class="table table-bordered">
									<thead>
										<tr>
											<th>Confirmed Email</th>
											<th>Account Created on</th>
											<th>Default Billing Address</th>
											<th>Default Shipping Address</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>dts.ritwik@dreamztech.com</td>
											<td>03 Jul 2017 01:50:19</td>
											<td>321, 231, sfds, 132131, AF, Telephone: 12312412</td>
											<td>321, 231, sfds, 132131, AF, Telephone: 12312412</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="headingTwo">
							<h4 class="panel-title">
            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
              Order History
            </a>
            </h4>
						
						</div>
						<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
							<div class="panel-body">
								<table class="table table-bordered">
									<thead>
										<tr>
											<th>Order ID</th>
											<th>Purchased On</th>
											<th>Bill to Name</th>
											<th>Ship to Name</th>
											<th>Purchased</th>
											<th>Status</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>web1707031499075902</td>
											<td>03 Jul 2017 15:28:22</td>
											<td>ritz</td>
											<td>ritz</td>
											<th>$261.36</th>
											<th>Processing</th>
											<th>
												<a href="javascript:void(0);" class="view" title="View" data-toggle="modal" data-target="#myModal"><i class="fa fa fa-eye"></i></a>
												<a href="javascript:void(0);" class="delete" title="Remove" data-toggle="modal" data-target="#myModal1"><i class="fa fa-trash-o"></i></a>
											</th>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>

				</div>
			</div>


		</div>
	</div>
</section>
<?php include 'footer.php'; ?>