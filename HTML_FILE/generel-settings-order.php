
<?php
$page = 'generel-settings';
include('header.php');
?>
    <div class="modal fade" tabindex="-1" role="dialog" id="myModal" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Add Discount Rule</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-12 col-sm-12 col-lg-12">
                <label>Total Amount <span>*</span></label>
                <div class="form-group">
                  <input type="text" class="form-control">
                </div>
              </div>
              <div class="col-md-12 col-sm-12 col-lg-12">
                <label>Select Rule <span>*</span></label>
                <div class="form-group">
                  <select class="form-control m-bot15">
                    <option>Select Rule</option>
                  </select>
                </div>
              </div>
              <div class="col-md-12 col-sm-12 col-lg-12">
                <label>Discount Price <span>*</span></label>
                <div class="form-group">
                  <input type="text" class="form-control">
                </div>
              </div>
              <div class="col-md-12 col-sm-12 col-lg-12">
                <label>Select Rate <span>*</span></label>
                <div class="form-group">
                  <select class="form-control m-bot15">
                    <option>Select Rate</option>
                    <option>Percentage</option>
                    <option>Flat</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-success" data-dismiss="modal">Save</button>
            <button type="button" class="btn btn-danger">Cancel</button>
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="myModal1" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Add Delivery Rule</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-12 col-sm-12 col-lg-12">
                <label>Total Amount <span>*</span></label>
                <div class="form-group">
                  <input type="text" class="form-control">
                </div>
              </div>
              <div class="col-md-12 col-sm-12 col-lg-12">
                <label>Select Rule <span>*</span></label>
                <div class="form-group">
                  <select class="form-control m-bot15">
                    <option>Select Rule</option>
                  </select>
                </div>
              </div>
              <div class="col-md-12 col-sm-12 col-lg-12">
                <label>Delivery Price <span>*</span></label>
                <div class="form-group">
                  <input type="text" class="form-control">
                </div>
              </div>
              <div class="col-md-12 col-sm-12 col-lg-12">
                <label>Select Rate <span>*</span></label>
                <div class="form-group">
                  <select class="form-control m-bot15">
                    <option>Select Rate</option>
                    <option>Percentage</option>
                    <option>Flat</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-success" data-dismiss="modal">Save</button>
            <button type="button" class="btn btn-danger">Cancel</button>
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="myModal2" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Add New Tax</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-12 col-sm-12 col-lg-12">
                <label>Total Amount <span>*</span></label>
                <div class="form-group">
                  <input type="text" class="form-control">
                </div>
              </div>
              <div class="col-md-12 col-sm-12 col-lg-12">
                <label>Select Rule <span>*</span></label>
                <div class="form-group">
                  <select class="form-control m-bot15">
                    <option>Select Rule</option>
                  </select>
                </div>
              </div>
              <div class="col-md-12 col-sm-12 col-lg-12">
                <label>Tax Price <span>*</span></label>
                <div class="form-group">
                  <input type="text" class="form-control">
                </div>
              </div>
              <div class="col-md-12 col-sm-12 col-lg-12">
                <label>Select Rate <span>*</span></label>
                <div class="form-group">
                  <select class="form-control m-bot15">
                    <option>Select Rate</option>
                    <option>Percentage</option>
                    <option>Flat</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-success" data-dismiss="modal">Save</button>
            <button type="button" class="btn btn-danger">Cancel</button>
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="myModal3" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Add New Misc. Tax</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-12 col-sm-12 col-lg-12">
                <label>Tax Type <span>*</span></label>
                <div class="form-group">
                  <input type="text" class="form-control">
                </div>
              </div>              
              <div class="col-md-12 col-sm-12 col-lg-12">
                <label>Tax Amount <span>*</span></label>
                <div class="form-group">
                  <input type="text" class="form-control">
                </div>
              </div>
              <div class="col-md-12 col-sm-12 col-lg-12">
                <label>Select Rate <span>*</span></label>
                <div class="form-group">
                  <select class="form-control m-bot15">
                    <option>Select Rate</option>
                    <option>Percentage</option>
                    <option>Flat</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-success" data-dismiss="modal">Save</button>
            <button type="button" class="btn btn-danger">Cancel</button>
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="myModal4" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Add Coupon</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-12 col-sm-12 col-lg-12">
                <label>Name of Coupon <span>*</span></label>
                <div class="form-group">
                  <input type="text" class="form-control">
                </div>
              </div>              
              <div class="col-md-12 col-sm-12 col-lg-12">
                <label>Coupon Code <span>*</span></label>
                <div class="form-group">
                  <input type="text" class="form-control">
                </div>
              </div>
              <div class="col-md-12 col-sm-12 col-lg-12">
                <label>Date Of issue <span>*</span></label>
                <div class="form-group">
                  <div class="input-group date" data-provide="datepicker">
                    <input type="text" class="form-control">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar-check-o" aria-hidden="true"></i>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-12 col-sm-12 col-lg-12">
                <label>Expiry Date <span>*</span></label>
                <div class="form-group">
                  <div class="input-group date" data-provide="datepicker">
                    <input type="text" class="form-control">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar-check-o" aria-hidden="true"></i>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-12 col-sm-12 col-lg-12">
                <label>Discount Type <span>*</span></label>
                <div class="form-group">
                  <input type="text" class="form-control">
                </div>
              </div>
              <div class="col-md-12 col-sm-12 col-lg-12">
                <label>Discount <span>*</span></label>
                <div class="form-group">
                  <input type="text" class="form-control">
                </div>
              </div>
              <div class="col-md-12 col-sm-12 col-lg-12">
                <label>Status <span>*</span></label>
                <div class="form-group">
                  <div class="row">
                      <div class="col-md-3 col-sm-3 col-lg-3">
                        <span class="bigcheck">
                          <label class="bigcheck">
                            <input type="checkbox" class="bigcheck" name="cheese" value="yes">
                            <span class="bigcheck-target"></span>
                          </label>
                        </span>
                        <label>Active</label>
                      </div>
                      <div class="col-md-3 col-sm-3 col-lg-3">
                        <span class="bigcheck">
                          <label class="bigcheck">
                            <input type="checkbox" class="bigcheck" name="cheese" value="yes">
                            <span class="bigcheck-target"></span>
                          </label>
                        </span>
                        <label>Inactive</label>
                      </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-success" data-dismiss="modal">Save Coupon</button>
            <button type="button" class="btn btn-danger">Cancel</button>
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="myModal5" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Update Coupon</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-12 col-sm-12 col-lg-12">
                <label>Name of Coupon <span>*</span></label>
                <div class="form-group">
                  <input type="text" class="form-control">
                </div>
              </div>              
              <div class="col-md-12 col-sm-12 col-lg-12">
                <label>Coupon Code <span>*</span></label>
                <div class="form-group">
                  <input type="text" class="form-control">
                </div>
              </div>
              <div class="col-md-12 col-sm-12 col-lg-12">
                <label>Date Of issue <span>*</span></label>
                <div class="form-group">
                  <div class="input-group date" data-provide="datepicker">
                    <input type="text" class="form-control">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar-check-o" aria-hidden="true"></i>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-12 col-sm-12 col-lg-12">
                <label>Expiry Date <span>*</span></label>
                <div class="form-group">
                  <div class="input-group date" data-provide="datepicker">
                    <input type="text" class="form-control">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar-check-o" aria-hidden="true"></i>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-12 col-sm-12 col-lg-12">
                <label>Discount Type <span>*</span></label>
                <div class="form-group">
                  <input type="text" class="form-control">
                </div>
              </div>
              <div class="col-md-12 col-sm-12 col-lg-12">
                <label>Discount <span>*</span></label>
                <div class="form-group">
                  <input type="text" class="form-control">
                </div>
              </div>
              <div class="col-md-12 col-sm-12 col-lg-12">
                <label>Status <span>*</span></label>
                <div class="form-group">
                  <div class="row">
                      <div class="col-md-3 col-sm-3 col-lg-3">
                        <span class="bigcheck">
                          <label class="bigcheck">
                            <input type="checkbox" class="bigcheck" name="cheese" value="yes">
                            <span class="bigcheck-target"></span>
                          </label>
                        </span>
                        <label>Active</label>
                      </div>
                      <div class="col-md-3 col-sm-3 col-lg-3">
                        <span class="bigcheck">
                          <label class="bigcheck">
                            <input type="checkbox" class="bigcheck" name="cheese" value="yes">
                            <span class="bigcheck-target"></span>
                          </label>
                        </span>
                        <label>Inactive</label>
                      </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-success" data-dismiss="modal">Save Coupon</button>
            <button type="button" class="btn btn-danger">Cancel</button>
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="myModal6" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Confirm</h4>
          </div>
          <div class="modal-body">
            <div class="row">                          
              <div class="col-md-12 col-sm-12 col-lg-12">
                <p>Are you sure, you want to <strong>update</strong> the status?</p>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-success" data-dismiss="modal">Confirm</button>
            <button type="button" class="btn btn-danger">Cancel</button>
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="myModal7" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Confirm</h4>
          </div>
          <div class="modal-body">
            <div class="row">                          
              <div class="col-md-12 col-sm-12 col-lg-12">
                <p>Are you sure you want to <strong>delete</strong> selected coupon(s)?</p>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-success" data-dismiss="modal">Confirm</button>
            <button type="button" class="btn btn-danger">Cancel</button>
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="myModal8" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Update Discount Rule</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-12 col-sm-12 col-lg-12">
                <label>Total Amount <span>*</span></label>
                <div class="form-group">
                  <input type="text" class="form-control">
                </div>
              </div>              
              <div class="col-md-12 col-sm-12 col-lg-12">
                <label>Select Rule <span>*</span></label>
                <div class="form-group">
                  <select class="form-control m-bot15">
                    <option>Select Rule</option>
                  </select>
                </div>
              </div>
              <div class="col-md-12 col-sm-12 col-lg-12">
                <label>Discount Price <span>*</span></label>
                <div class="form-group">
                  <input type="text" class="form-control">
                </div>
              </div>
              <div class="col-md-12 col-sm-12 col-lg-12">
                <label>Select Rate <span>*</span></label>
                <div class="form-group">
                  <select class="form-control m-bot15">
                    <option>Select Rate</option>
                    <option>Percentage</option>
                    <option>Flat</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-success" data-dismiss="modal">Save</button>
            <button type="button" class="btn btn-danger">Cancel</button>
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="myModal9" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Update Delivery Rule</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-12 col-sm-12 col-lg-12">
                <label>Total Amount <span>*</span></label>
                <div class="form-group">
                  <input type="text" class="form-control">
                </div>
              </div>              
              <div class="col-md-12 col-sm-12 col-lg-12">
                <label>Select Rule <span>*</span></label>
                <div class="form-group">
                  <select class="form-control m-bot15">
                    <option>Select Rule</option>
                  </select>
                </div>
              </div>
              <div class="col-md-12 col-sm-12 col-lg-12">
                <label>Delivery Price <span>*</span></label>
                <div class="form-group">
                  <input type="text" class="form-control">
                </div>
              </div>
              <div class="col-md-12 col-sm-12 col-lg-12">
                <label>Select Rate <span>*</span></label>
                <div class="form-group">
                  <select class="form-control m-bot15">
                    <option>Select Rate</option>
                    <option>Percentage</option>
                    <option>Flat</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-success" data-dismiss="modal">Save</button>
            <button type="button" class="btn btn-danger">Cancel</button>
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="myModal10" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Edit New Tax</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-12 col-sm-12 col-lg-12">
                <label>Total Amount <span>*</span></label>
                <div class="form-group">
                  <input type="text" class="form-control">
                </div>
              </div>              
              <div class="col-md-12 col-sm-12 col-lg-12">
                <label>Select Rule <span>*</span></label>
                <div class="form-group">
                  <select class="form-control m-bot15">
                    <option>Select Rule</option>
                  </select>
                </div>
              </div>
              <div class="col-md-12 col-sm-12 col-lg-12">
                <label>Tax Price <span>*</span></label>
                <div class="form-group">
                  <input type="text" class="form-control">
                </div>
              </div>
              <div class="col-md-12 col-sm-12 col-lg-12">
                <label>Select Rate <span>*</span></label>
                <div class="form-group">
                  <select class="form-control m-bot15">
                    <option>Select Rate</option>
                    <option>Percentage</option>
                    <option>Flat</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-success" data-dismiss="modal">Save</button>
            <button type="button" class="btn btn-danger">Cancel</button>
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="myModal11" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Edit New Misc. Tax</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-12 col-sm-12 col-lg-12">
                <label>Tax Type <span>*</span></label>
                <div class="form-group">
                  <input type="text" class="form-control">
                </div>
              </div>              
              <div class="col-md-12 col-sm-12 col-lg-12">
                <label>Tax Amount <span>*</span></label>
                <div class="form-group">
                  <input type="text" class="form-control">
                </div>
              </div>
              <div class="col-md-12 col-sm-12 col-lg-12">
                <label>Select Rate <span>*</span></label>
                <div class="form-group">
                  <select class="form-control m-bot15">
                    <option>Select Rate</option>
                    <option>Percentage</option>
                    <option>Flat</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-success" data-dismiss="modal">Save</button>
            <button type="button" class="btn btn-danger">Cancel</button>
          </div>
        </div>
      </div>
    </div>
   
    <section class="container-fluid">
      <div class="ecom-category-area">
        <div class="row">
          <h3 class="pos-rel">
          <span>Generel Settings</span>
          </h3>
        </div>
      </div>
      <div class="ecom-product-item">
        <div class="col-md-12 col-sm-12 col-lg-12">
          <div>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#Configuration" aria-controls="home" role="tab" data-toggle="tab">Configuration Settings</a></li>
              <li role="presentation"><a href="#Settings" aria-controls="profile" role="tab" data-toggle="tab">Language Settings</a></li>
              <li role="presentation"><a href="#Rules" aria-controls="messages" role="tab" data-toggle="tab">Rules</a></li>
              <li role="presentation"><a href="#Coupons" aria-controls="settings" role="tab" data-toggle="tab">Coupons</a></li>
              <li role="presentation"><a href="#CMS" aria-controls="settings" role="tab" data-toggle="tab">CMS Pages</a></li>
            </ul>
            <div class="clearfix"></div>
            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="Configuration">
                <div class="panel panel-default">
                  <div class="panel-heading">General Information</div>
                  <div class="panel-body">
                    <div class="bs-callout bs-callout-warning" id="callout-btn-group-accessibility"> 
                      <h4>Store Information</h4> 
                      <div class="row">
                        <div class="col-md-12 col-sm-12 col-lg-12">
                          <label>Store Name</label>
                          <div class="form-group">
                            <input type="text" class="form-control">
                          </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-lg-6">
                          <label>Store Email <span>*</span></label>
                          <div class="form-group">
                            <input type="text" class="form-control">
                          </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-lg-6">
                          <label>Currency Code <span>*</span></label>
                          <div class="form-group">
                            <select class="form-control m-bot15">
                              <option>1</option>
                              <option>2</option>
                              <option>3</option>
                              <option>4</option>
                              <option>5</option>
                            </select>
                          </div>
                        </div>                      
                        <div class="col-md-12 col-sm-12 col-lg-12">
                          <label>Mobile</label>
                          <div class="form-group">
                            <div class="row">
                              <div class="col-md-10 col-sm-10 col-lg-10"><input type="text" class="form-control"></div>
                              <div class="col-md-2 col-sm-2 col-lg-2">
                                <button type="button" class="btn btn-danger delete"><span><i class="fa fa-plus" aria-hidden="true"></i> Add More</span></button>
                              </div>
                            </div>                          
                          </div>
                        </div>                      
                        <div class="col-md-12 col-sm-12 col-lg-12">
                          <label>Address <span>*</span></label>
                          <div class="form-group">
                            <div class="row">
                              <div class="col-md-10 col-sm-10 col-lg-10"><input type="text" class="form-control"></div>
                              <div class="col-md-2 col-sm-2 col-lg-2">
                                <button type="button" class="btn btn-danger delete"><span><i class="fa fa-plus" aria-hidden="true"></i> Add More</span></button>
                              </div>
                            </div>                          
                          </div>
                        </div>
                      </div>                 
                    </div>
                    <div class="bs-callout bs-callout-warning" id="callout-btn-group-accessibility"> 
                      <h4>Delivery/Pick Up Information</h4> 
                      <div class="row">
                        <div class="col-md-6 col-sm-6 col-lg-6">
                          <label>Preferred Delivery/Pickup time</label>
                          <div class="form-group">
                            <select class="form-control m-bot15">
                              <option>Yes</option>
                              <option>No</option>
                            </select>
                          </div>
                        </div>
                      </div>                        
                      <div class="row">
                        <div class="col-md-6 col-sm-6 col-lg-6">
                          <label>Max. Delivery Distance</label>
                          <div class="form-group">
                            <input type="text" class="form-control">
                          </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-lg-6">
                          <label>Unit</label>
                          <div class="form-group">
                            <select class="form-control m-bot15">
                              <option>Select</option>
                              <option>Miles</option>
                              <option>KM</option>
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="bs-callout bs-callout-warning" id="callout-btn-group-accessibility"> 
                      <h4>Other Information</h4> 
                      <div class="row">
                          <div class="col-md-6 col-sm-6 col-lg-6">
                            <label>Pickup Duration (in minutes)</label>
                            <div class="form-group">
                              <input type="text" class="form-control">
                            </div>
                          </div>
                          <div class="col-md-6 col-sm-6 col-lg-6">
                            <label>Min. Order Amount</label>
                            <div class="form-group">
                              <input type="text" class="form-control">
                            </div>
                          </div>
                          <div class="col-md-6 col-sm-6 col-lg-6">
                            <label>Show Unavailable Products</label>
                            <div class="form-group">
                              <select class="form-control m-bot15">
                                <option>Yes</option>
                                <option>No</option>
                              </select>
                            </div>
                          </div>
                          <div class="col-md-6 col-sm-6 col-lg-6">
                            <label>Offered Product Discount (%)</label>
                            <div class="form-group">
                              <input type="text" class="form-control">
                            </div>
                          </div>
                          <div class="col-md-6 col-sm-6 col-lg-6">
                            <label>Sort Cat Alphabetically</label>
                            <div class="form-group">
                              <select class="form-control m-bot15">
                                <option>Yes</option>
                                <option>No</option>
                              </select>
                            </div>
                          </div>
                          <div class="col-md-6 col-sm-6 col-lg-6">
                            <label>Display Tip</label>
                            <div class="form-group">
                              <span class="bigcheck">
                                <label class="bigcheck">
                                  <input type="checkbox" class="bigcheck" name="cheese" value="yes">
                                  <span class="bigcheck-target"></span>
                                </label>
                              </span>
                            </div>
                          </div>
                      </div>                
                    </div> 
                    <div class="bs-callout bs-callout-warning" id="callout-btn-group-accessibility"> 
                      <h4>FAX <i class="fa fa-info-circle" aria-hidden"true"=""></i></h4> 
                      <div class="row">
                        <div class="col-md-6 col-sm-6 col-lg-6">
                          <label>Fax No.</label>
                          <div class="form-group">
                            <input type="text" class="form-control">
                          </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-lg-6">
                          <label>Mode</label>
                          <div class="form-group">
                            <select class="form-control m-bot15">
                              <option>Select</option>
                              <option>Test</option>
                              <option>Live</option>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-lg-6">
                          <label>API Key</label>
                          <div class="form-group">
                            <input type="text" class="form-control">
                          </div>
                        </div>                      
                        <div class="col-md-6 col-sm-6 col-lg-6">
                          <label>Merchant Salt</label>
                          <div class="form-group">
                            <input type="text" class="form-control">
                          </div>
                        </div>
                      </div>                 
                    </div> 
                    <div class="bs-callout bs-callout-warning" id="callout-btn-group-accessibility"> 
                      <h4>Cloud Printer <i class="fa fa-info-circle" aria-hidden"true"=""></i></h4> 
                      <div class="row">                        
                        <div class="col-md-6 col-sm-6 col-lg-6">
                          <label>Select Printer</label>
                          <div class="form-group">
                            <select class="form-control m-bot15">
                              <option>Select Printer</option>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-lg-6">
                          <label>&nbsp</label>
                          <div class="form-group">
                            <button type="button" class="btn btn-danger delete"><span><i class="fa fa-plus" aria-hidden="true"></i> Click To Get Print</span></button>
                          </div>
                        </div>
                      </div>                 
                    </div> 
                    <div class="bs-callout bs-callout-warning" id="callout-btn-group-accessibility"> 
                      <h4>SMS <i class="fa fa-info-circle" aria-hidden"true"=""></i></h4> 
                      <div class="row">                        
                        <div class="col-md-4 col-sm-4 col-lg-4">
                          <label>Account SID</label>
                          <div class="form-group">
                            <input type="text" class="form-control">
                          </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-lg-4">
                          <label>Auth Token</label>
                          <div class="form-group">
                            <input type="text" class="form-control">
                          </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-lg-4">
                          <label>From Number</label>
                          <div class="form-group">
                            <input type="text" class="form-control">
                          </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-lg-12">
                          <h4>Images</h4> 
                            <div class="ecom-thumbnail"><i class="fa fa-file-image-o" aria-hidden="true"></i></div>
                            <div class="panel-body img-file-tab">
                              <div class="text-center">
                                <span class="btn btn-default btn-file img-select-btn" style="overflow: hidden;position: relative;">
                                  <span>Change</span>
                                  <input type="file" name="img-file-input">
                                </span>
                                <span class="btn btn-default img-remove-btn" style="display: inline-block;">Remove</span>
                              </div>
                            </div>
                            <p>Note: For best results, image resolution should be 700x350</p>
                        </div>
                        <div class="col-md-12 col-sm-12 col-lg-12 text-right">
                          <button type="button" class="btn btn-success delete"><span>Save</span></button>
                        </div>
                      </div>                 
                    </div>          
                  </div>
                </div>
                
                <div class="panel panel-default">
                  <div class="panel-heading">Payment Method</div>
                  <div class="panel-body">
                    <div class="row">
                      <div class="col-md-12 col-sm-12 col-lg-12">
                        <div class="input-group">
                          <span class="input-group-addon"> <input type="checkbox" aria-label="Checkbox for following text input"> </span>
                          <input class="form-control" aria-label="Text input with radio button" placeholder="Cash on Delivery">
                        </div>
                      </div>
                      <div class="clearfix"></div>
                      <br>
                      <div class="col-md-12 col-sm-12 col-lg-12">
                        <div class="input-group">
                          <span class="input-group-addon"> <input type="checkbox" aria-label="Checkbox for following text input"> </span>
                          <input class="form-control" aria-label="Text input with radio button" placeholder="Pick Up">
                        </div>
                      </div>
                      <div class="clearfix"></div>
                      <br>
                      <div class="col-md-12 col-sm-12 col-lg-12">
                        <div class="input-group">
                          <span class="input-group-addon"> <input type="checkbox" aria-label="Checkbox for following text input"> </span>
                          <input class="form-control" aria-label="Text input with radio button" placeholder="PayPal Express">
                        </div>
                        <p>Accept money from anyone through your email address, in this option user goes to PayPal's website to make the payment.</p>
                      </div>
                      <div class="clearfix"></div>
                      <br>
                      <div class="col-md-12 col-sm-12 col-lg-12">
                        <div class="input-group">
                          <span class="input-group-addon"> <input type="checkbox" aria-label="Checkbox for following text input"> </span>
                          <input class="form-control" aria-label="Text input with radio button" placeholder="Credit Card via PayPal Payment Gateway">
                        </div>
                        <p>Direct Payments lets buyers who do not have a PayPal account use their credit cards without leaving your App. PayPal processes the payment in the background.</p>
                      </div>
                      <div class="clearfix"></div>
                      <br>
                      <div class="col-md-12 col-sm-12 col-lg-12">
                        <div class="input-group">
                          <span class="input-group-addon"> <input type="checkbox" aria-label="Checkbox for following text input"> </span>
                          <input class="form-control" aria-label="Text input with radio button" placeholder="Pay with credit card over the phone">
                        </div>
                        <p>Accept credit card payments securely over the phone.</p>
                      </div>
                      <div class="clearfix"></div>
                      <br>
                      <div class="col-md-12 col-sm-12 col-lg-12">
                        <div class="input-group">
                          <span class="input-group-addon"> <input type="checkbox" aria-label="Checkbox for following text input"> </span>
                          <input class="form-control" aria-label="Text input with radio button" placeholder="Credit Card via Stripe Payment Gateway">
                        </div>
                        <p>Direct Payments lets buyers use their credit cards without leaving your App.</p>
                      </div>
                    </div>
                  </div>
                  <div class="text-right col-md-12 col-sm-12 col-lg-12"><button type="button" class="btn btn-success delete"><span>Save</span></button></div>
                  <div class="clearfix"></div>
                  <br>
                </div>
              </div>
              <div role="tabpanel" class="tab-pane" id="Settings">
                <div class="panel panel-default hidden">
                  <div class="panel-heading">Manage Language Settings</div>
                  <div class="panel-body">
                    <div class="row">
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Categories</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>My Orders</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>My Account</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Instructions</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Home</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>My Shop</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Cart</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Pick Up</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Submit</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Forgot Password</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Email ID</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Password</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Don't have an account yet?</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Search</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Sign up Now</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Name</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Phone</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Confirm Password</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Sign up</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Already have an account?</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Sign In</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Contact Information</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>My Addresses</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Billing Address</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Default Delivery Address</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Order History</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Order ID</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Billing Address</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Delivery Address</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Order Details</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Product Name</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Price</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Qty.</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Order Date</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Subtotal</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Tax</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Coupon</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Discount</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Delivery</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Grand Total</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Payment Options</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Order Status</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Personal Information</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Change Password</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Current Password</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Confirm New Password</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>No Product Exists</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Add To Cart</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Cart List</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Edit</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Done</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Payment Details</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Coupon Code</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Enter coupon code</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Apply</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Continue Ordering</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Checkout</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>First Name</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Last Name</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Address</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Zip/Postal Code</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>City</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Please Select Country</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>State/Province</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Telephone</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Fax</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Same as Billing Address</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Pay Now</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Cash On Delivery</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Credit Card</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>PayPal</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>You will be redirected to PayPal site</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Order by Phone</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>You can order by calling</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Instock</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Out Of Stock</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Login</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>New Password</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Logout</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>No Product Exists</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Search Product</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Sort By</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>WishList</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Terms and Conditions</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Privacy Policy</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Featured Products</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Search Result for</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>add</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Product has been successfully added to your Cart</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Product Quantity should be greater than Zero.</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Delivery Charges</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Tip</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>There are no items in the cart</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Reset Password</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Call now</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Confirm</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Total payable amount</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Update Contact Information</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Update Billing Information</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Update Delivery Information</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>There is no featured product</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Delivery address different from billing address</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Complete Purchase</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>No reviews available</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Products</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Empty</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Offer</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>your cart is empty</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>delivery button</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Please select preferred delivery time</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>view time</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Transaction ID</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>view product</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Billing address</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>User review</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Click on Confirm Button to pay through PayUmoney</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Click on Confirm Button to pay through PayPal</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Pay using Cash-on-Delivery</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Thank You</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Description</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Check the back of your credit card for CVV</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Card holder name</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>No reviews available</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Tip</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Please enter Name</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Invalid Email</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Please enter State/Province</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Please enter your City</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Please enter Address</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Phone field can't be left blank</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Please enter a valid Email Address</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Blank Field</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Your Order was successful!</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Your Order has been successfully placed and a receipt for your purchase has been sent to your Email ID</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Continue</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Total</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>view products</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Quantity</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Reorder</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>food type</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Post Review</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Write a Review</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Comments</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Review Title</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Comment</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Please enter the review title</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Description</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Users Reviews</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Error</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Your review has been submitted successfully</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Success</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Add items to it now</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Your cart is</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Select from where you want to upload</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Camera</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Gallery</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Successfully Uploaded</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Upload error</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Try another keyword</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Logout</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>No,  Yes</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Please Enter valid Email and Password</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Please enter a valid Email ID</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Alert!</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Forget Password?</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>A new password has been sent to your Email ID</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Please fill detail.</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Invalid Coupon</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Coupon Code</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>You have entered wrong password</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Delivery</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Offer Zone</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Price -- High to Low</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Price -- Low to High</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Delivery is not available in your location</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Please enter a preferred delivery</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Delivery Time</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Preferred Delivery Time:-</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Please check delivery location or enter valid delivery location.</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>View Store Timings</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Store Timings</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Opening Timings</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Working hours</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Customer Serving Timings</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Start time</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Thanks</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Expiry Year</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>PLACE ORDER</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Expiry Month</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>You</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Your Order was successful</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Your Order has been successfully placed and receipt has been emailed to you</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Continue Ordering</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Card holder first name</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Coupon Applied Successfully</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Pickup Time</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Please enter a preferred pick up time</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Your preferred pick up time did not match to store time, Please view store time for more details.</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>And Instructions :-</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Sorry! You have entered wrong pickup timings</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>minutes later from current time</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Invalid CVV code. Try again!</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Your preferred Delivery time did not match to store time, Please view store time for more details.</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Please select store address.</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Please select address</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Your order will be delivered on today around at</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Post</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Sorry! This product is no longer available</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Product successfully removed from your wishlist</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Product successfully added in your wishlist</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Dear Customer, Your order was successfully created and sent to your email id for the Order ID: __orderid__. Thank you for using __storename__.</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Hey! New order __orderid__ has been placed on your __storename__ store.</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Please select required option</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Product is not available for order</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Choose any</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Are you sure? Do you want to delete the card?</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Save this card for faster checkout</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Card deleted successfully</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Please note, if you make payment using new card and doesn't save it, your existing card details will be automatically deleted. Do you want to continue?</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>CVV does not match</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Please note, if you save new card for faster checkout, your existing card details will be replaced with the new one. Do you want to continue?</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Choose any one</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Please enter CVV code</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Pay with this card</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Please enter card number</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Please enter valid card number</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Please enter expiry month</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Please enter valid expiry month</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Please enter expiry year</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Please enter valid expiry year</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Please enter valid card holder name</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Deliver ASAP</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Time your Order</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Your order will be delivered in</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>We are closed for now. Please place your order during our working hours</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                    </div>          
                  </div>
                </div>
              </div>
              <div role="tabpanel" class="tab-pane" id="Rules">
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                      <h4 class="panel-title">
                      <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        Discount
                      </a>
                      </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">  
                      <div class="panel-body">
                        <div class="pull-right">
                          <div class="btn-group" role="group" aria-label="...">
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus-square" aria-hidden="true"></i> Add New Discount Rule</button>
                            <button type="button" class="btn btn-danger"><i class="fa fa-trash-o"></i> Delete</button>
                            <div class="btn-group" role="group">
                              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              Change Status
                              <span class="caret"></span>
                              </button>
                              <ul class="dropdown-menu">
                                <li><a href="#">Active</a></li>
                                <li><a href="#">Inactive</a></li>
                              </ul>
                            </div>
                          </div>
                        </div>
                        <div class="clearfix"></div>
                        <table class="table table-bordered">
                          <thead>
                            <tr>
                              <th><input class="checkall" type="checkbox"></th>
                              <th>Rules</th>
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td><input class="checkall" type="checkbox"></td>
                              <td>If total amount is >= 300 then discount amount will be 25 Flat</td>
                              <td>
                                <a href="javascript:void(0);" class="view" title="View" data-toggle="modal" data-target="#myModal8"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                <a href="javascript:void(0);" class="active" title="active" data-toggle="modal" data-target="#myModal6"><i class="fa fa-check" aria-hidden="true"></i></a>
                                <a href="javascript:void(0);" class="delete" title="Remove" data-toggle="modal" data-target="#myModal7"><i class="fa fa-trash-o"></i></a>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwo">
                      <h4 class="panel-title">
                      <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        Delivery
                      </a>
                      </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">   
                      <div class="panel-body">
                        <div class="pull-right">
                          <div class="btn-group" role="group" aria-label="...">
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal1"><i class="fa fa-plus-square" aria-hidden="true"></i> Add Delivery Rule</button>
                            <button type="button" class="btn btn-danger"><i class="fa fa-trash-o"></i> Delete</button>
                            <div class="btn-group" role="group">
                              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              Change Status
                              <span class="caret"></span>
                              </button>
                              <ul class="dropdown-menu">
                                <li><a href="#">Active</a></li>
                                <li><a href="#">Inactive</a></li>
                              </ul>
                            </div>
                          </div>
                        </div>
                        <div class="clearfix"></div>
                        <table class="table table-bordered">
                          <thead>
                            <tr>
                              <th><input class="checkall" type="checkbox"></th>
                              <th>Rules</th>
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td><input class="checkall" type="checkbox"></td>
                              <td>If total amount is >= 300 then discount amount will be 25 Flat</td>
                              <td>
                                <a href="javascript:void(0);" class="view" title="View" data-toggle="modal" data-target="#myModal9"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                <a href="javascript:void(0);" class="active" title="active" data-toggle="modal" data-target="#myModal6"><i class="fa fa-check" aria-hidden="true"></i></a>
                                <a href="javascript:void(0);" class="delete" title="Remove" data-toggle="modal" data-target="#myModal7"><i class="fa fa-trash-o"></i></a>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThree">
                      <h4 class="panel-title">
                      <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                        Tax
                      </a>
                      </h4>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                      <div class="panel-body">
                        <div class="pull-right">
                          <div class="btn-group" role="group" aria-label="...">
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal2"><i class="fa fa-plus-square" aria-hidden="true"></i> Add New Tax</button>
                            <button type="button" class="btn btn-danger"><i class="fa fa-trash-o"></i> Delete</button>
                            <div class="btn-group" role="group">
                              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              Change Status
                              <span class="caret"></span>
                              </button>
                              <ul class="dropdown-menu">
                                <li><a href="#">Active</a></li>
                                <li><a href="#">Inactive</a></li>
                              </ul>
                            </div>
                          </div>
                        </div>
                        <div class="clearfix"></div>
                        <table class="table table-bordered">
                          <thead>
                            <tr>
                              <th><input class="checkall" type="checkbox"></th>
                              <th>Rules</th>
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td><input class="checkall" type="checkbox"></td>
                              <td>If total amount is >= 300 then discount amount will be 25 Flat</td>
                              <td>
                                <a href="javascript:void(0);" class="view" title="View" data-toggle="modal" data-target="#myModal10"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                <a href="javascript:void(0);" class="active" title="active" data-toggle="modal" data-target="#myModal6"><i class="fa fa-check" aria-hidden="true"></i></a>
                                <a href="javascript:void(0);" class="delete" title="Remove" data-toggle="modal" data-target="#myModal7"><i class="fa fa-trash-o"></i></a>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingFour">
                      <h4 class="panel-title">
                      <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                        Misc. Tax
                      </a>
                      </h4>
                    </div>
                    <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                      <div class="panel-body">
                        <div class="pull-right">
                          <div class="btn-group" role="group" aria-label="...">
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal3"><i class="fa fa-plus-square" aria-hidden="true"></i> Add New Misc. Tax</button>
                            <button type="button" class="btn btn-danger"><i class="fa fa-trash-o"></i> Delete</button>
                            <div class="btn-group" role="group">
                              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              Change Status
                              <span class="caret"></span>
                              </button>
                              <ul class="dropdown-menu">
                                <li><a href="#">Active</a></li>
                                <li><a href="#">Inactive</a></li>
                              </ul>
                            </div>
                          </div>
                        </div>
                        <div class="clearfix"></div>
                        <table class="table table-bordered">
                          <thead>
                            <tr>
                              <th><input class="checkall" type="checkbox"></th>
                              <th>Rules</th>
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td><input class="checkall" type="checkbox"></td>
                              <td>If total amount is >= 300 then discount amount will be 25 Flat</td>
                              <td>
                                <a href="javascript:void(0);" class="view" title="View" data-toggle="modal" data-target="#myModal11"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                <a href="javascript:void(0);" class="active" title="active" data-toggle="modal" data-target="#myModal6"><i class="fa fa-check" aria-hidden="true"></i></a>
                                <a href="javascript:void(0);" class="delete" title="Remove" data-toggle="modal" data-target="#myModal7"><i class="fa fa-trash-o"></i></a>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div role="tabpanel" class="tab-pane" id="Coupons">
                <div class="panel-group" role="tablist" aria-multiselectable="true">
                  <div style="margin-top: 15px;">
                      <div class="pull-right">
                        <div class="btn-group" role="group" aria-label="...">
                          <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal4"><i class="fa fa-plus-square" aria-hidden="true"></i> Add New Coupon</button>
                          <button type="button" class="btn btn-danger"><i class="fa fa-trash-o"></i> Delete</button>
                          <div class="btn-group" role="group">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Change Status
                            <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                              <li><a href="#">Active</a></li>
                              <li><a href="#">Inactive</a></li>
                            </ul>
                          </div>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th><input class="checkall" type="checkbox"></th>
                            <th>Name of Coupon</th>
                            <th>Code</th>
                            <th>Issue Date</th>
                            <th>Expiry Date</th>
                            <th>Discount</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td><input class="checkall" type="checkbox"></td>
                            <td>My Coupon</td>
                            <th>123456789</th>
                            <th>07/03/2017</th>
                            <th>07/31/2017</th>
                            <th>10%</th>
                            <td>
                              <a href="javascript:void(0);" class="view" title="View" data-toggle="modal" data-target="#myModal5"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                              <a href="javascript:void(0);" class="active" title="active" data-toggle="modal" data-target="#myModal6"><i class="fa fa-check" aria-hidden="true"></i></a>
                              <a href="javascript:void(0);" class="delete" title="Remove" data-toggle="modal" data-target="#myModal7"><i class="fa fa-trash-o"></i></a>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                </div>
              </div>
              <div role="tabpanel" class="tab-pane" id="CMS">...</div>
            </div>
          </div>
        </div>
      </div>
    </section>
     <?php include 'footer.php'; ?>
   