
<?php
$page = 'generel-settings';
include('header.php');
?>
 <div class="modal fade" tabindex="-1" role="dialog" id="myModal" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Confirm</h4>
          </div>
          <div class="modal-body">
            <p>Are you sure, you want to update the status?</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="button" class="btn btn-primary">Confirm</button>
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="myModal1" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Confirm</h4>
          </div>
          <div class="modal-body">
            <p>Are you sure you want to delete selected order(s)?</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="button" class="btn btn-primary">Confirm</button>
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="myModal2" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Update Rule</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-6 col-sm-6 col-lg-6">
                <label>Sub total <span>*</span></label>
                <div class="form-group">
                  <input type="text" class="form-control">
                </div>
              </div>
              <div class="col-md-6 col-sm-6 col-lg-6">
                <label>Select Rule <span>*</span></label>
                <div class="form-group">
                  <select class="form-control m-bot15">
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                  </select>
                </div>
              </div>
              <div class="col-md-6 col-sm-6 col-lg-6">
                <label>Discount Price <span>*</span></label>
                <div class="form-group">
                  <input type="text" class="form-control">
                </div>
              </div>
              <div class="col-md-6 col-sm-6 col-lg-6">
                <label>Select Rate <span>*</span></label>
                <div class="form-group">
                  <select class="form-control m-bot15">
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="button" class="btn btn-primary">Save</button>
          </div>
        </div>
      </div>
    </div>
   
    <section class="container-fluid">
      <div class="ecom-category-area">
        <div class="row">
          <h3 class="pos-rel">
          <span>Generel Settings</span>
          </h3>
        </div>
      </div>
      <div class="ecom-product-item">
        <div class="col-md-12 col-sm-12 col-lg-12">
          <div>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#Configuration" aria-controls="home" role="tab" data-toggle="tab">Configuration Settings</a></li>
              <li role="presentation"><a href="#Settings" aria-controls="profile" role="tab" data-toggle="tab">Language Settings</a></li>
              <li role="presentation"><a href="#Rules" aria-controls="messages" role="tab" data-toggle="tab">Rules</a></li>
              <li role="presentation"><a href="#Coupons" aria-controls="settings" role="tab" data-toggle="tab">Coupons</a></li>
              <li role="presentation"><a href="#CMS" aria-controls="settings" role="tab" data-toggle="tab">CMS Pages</a></li>
            </ul>
            <div class="clearfix"></div>
            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="Configuration">
                <div class="panel panel-default">
                  <div class="panel-heading">General Information</div>
                  <div class="panel-body">
                    <div class="row">
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Store Name</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Store Email <span>*</span></label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Currency Code <span>*</span></label>
                        <div class="form-group">
                          <select class="form-control m-bot15">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Offered Product Discount (%)</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Sort Cat Alphabetically</label>
                        <div class="form-group">
                          <select class="form-control m-bot15">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <label>Min. Order Amount</label>
                        <div class="form-group">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="text-left col-md-6 col-sm-6 col-lg-6">
                        <div><strong>Default Image</strong></div>
                        <div>
                        <img alt="140x140" data-src="holder.js/140x140" class="img-thumbnail" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMTQwIiBoZWlnaHQ9IjE0MCIgdmlld0JveD0iMCAwIDE0MCAxNDAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjwhLS0KU291cmNlIFVSTDogaG9sZGVyLmpzLzE0MHgxNDAKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNWQxZGEyZGY4NyB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1ZDFkYTJkZjg3Ij48cmVjdCB3aWR0aD0iMTQwIiBoZWlnaHQ9IjE0MCIgZmlsbD0iI0VFRUVFRSIvPjxnPjx0ZXh0IHg9IjQ0LjA1NDY4NzUiIHk9Ijc0LjUiPjE0MHgxNDA8L3RleHQ+PC9nPjwvZz48L3N2Zz4=" data-holder-rendered="true" style="width: 140px; height: 140px;">
                        </div>
                      </div>
                      <div class="text-right col-md-6 col-sm-6 col-lg-6"><button type="button" class="btn btn-danger delete"><span>Save</span></button></div>
                    </div>
                  </div>
                </div>
                <div class="panel panel-default">
                  <div class="panel-heading">Other Settings</div>
                  <div class="panel-body">
                    <div class="row">
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <span class="bigcheck">
                          <label class="bigcheck">
                            <input type="checkbox" class="bigcheck" name="cheese" value="yes"/>
                            <span class="bigcheck-target"></span>
                          </label>
                        </span>
                        <label>Share Product(s) Image</label>
                      </div>
                      <div class="col-md-6 col-sm-6 col-lg-6">
                        <span class="bigcheck">
                          <label class="bigcheck">
                            <input type="checkbox" class="bigcheck" name="cheese" value="yes"/>
                            <span class="bigcheck-target"></span>
                          </label>
                        </span>
                        <label>Enable Shipment Information</label>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="panel panel-default">
                  <div class="panel-heading">Payment Method</div>
                  <div class="panel-body">
                    <div class="row">
                      <div class="col-md-12 col-sm-12 col-lg-12">
                        <div class="input-group">
                          <span class="input-group-addon"> <input type="checkbox" aria-label="Checkbox for following text input"> </span>
                          <input class="form-control" aria-label="Text input with radio button" placeholder="Cash on Delivery">
                        </div>
                      </div>
                      <div class="clearfix"></div>
                      <br>
                      <div class="col-md-12 col-sm-12 col-lg-12">
                        <div class="input-group">
                          <span class="input-group-addon"> <input type="checkbox" aria-label="Checkbox for following text input"> </span>
                          <input class="form-control" aria-label="Text input with radio button" placeholder="PayPal Express">
                        </div>
                      </div>
                      <div class="clearfix"></div>
                      <br>
                      <div class="col-md-12 col-sm-12 col-lg-12">
                        <div class="input-group">
                          <span class="input-group-addon"> <input type="checkbox" aria-label="Checkbox for following text input"> </span>
                          <input class="form-control" aria-label="Text input with radio button" placeholder="Credit Card via PayPal Payment Gateway">
                        </div>
                      </div>
                      <div class="clearfix"></div>
                      <br>
                      <div class="col-md-12 col-sm-12 col-lg-12">
                        <div class="input-group">
                          <span class="input-group-addon"> <input type="checkbox" aria-label="Checkbox for following text input"> </span>
                          <input class="form-control" aria-label="Text input with radio button" placeholder="Pay with credit card over the phone">
                        </div>
                      </div>
                      <div class="clearfix"></div>
                      <br>
                      <div class="col-md-12 col-sm-12 col-lg-12">
                        <div class="input-group">
                          <span class="input-group-addon"> <input type="checkbox" aria-label="Checkbox for following text input"> </span>
                          <input class="form-control" aria-label="Text input with radio button" placeholder="Credit Card via Stripe Payment Gateway">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="text-right col-md-12 col-sm-12 col-lg-12"><button type="button" class="btn btn-danger delete"><span>Save</span></button></div>
                  <div class="clearfix"></div>
                  <br>
                </div>
              </div>
              <div role="tabpanel" class="tab-pane" id="Settings">...</div>
              <div role="tabpanel" class="tab-pane" id="Rules">
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                      <h4 class="panel-title">
                      <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        Discount
                      </a>
                      </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                      <div class="panel-body">
                        <div class="pull-right">
                          <div class="btn-group" role="group" aria-label="...">
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal2"><i class="fa fa-plus-square" aria-hidden="true"></i> Add New Discount Rule</button>
                            <button type="button" class="btn btn-danger"><i class="fa fa-trash-o"></i> Delete</button>
                            <div class="btn-group" role="group">
                              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              Change Status
                              <span class="caret"></span>
                              </button>
                              <ul class="dropdown-menu">
                                <li><a href="#">Active</a></li>
                                <li><a href="#">Inactive</a></li>
                              </ul>
                            </div>
                          </div>
                        </div>
                        <div class="clearfix"></div>
                        <table class="table table-bordered">
                          <thead>
                            <tr>
                              <th><input class="checkall" type="checkbox"></th>
                              <th>Rules</th>
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td><input class="checkall" type="checkbox"></td>
                              <td>If total amount is >= 300 then discount amount will be 25 Flat</td>
                              <td>
                                <a href="javascript:void(0);" class="view" title="View" data-toggle="modal" data-target="#myModal2"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                <a href="javascript:void(0);" class="active" title="active" data-toggle="modal" data-target="#myModal"><i class="fa fa-check" aria-hidden="true"></i></a>
                                <a href="javascript:void(0);" class="delete" title="Remove" data-toggle="modal" data-target="#myModal1"><i class="fa fa-trash-o"></i></a>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwo">
                      <h4 class="panel-title">
                      <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        Shipping
                      </a>
                      </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                      <div class="panel-body">
                        <div class="pull-right">
                          <div class="btn-group" role="group" aria-label="...">
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal2"><i class="fa fa-plus-square" aria-hidden="true"></i> Add New Shipping Rule</button>
                            <button type="button" class="btn btn-danger"><i class="fa fa-trash-o"></i> Delete</button>
                            <div class="btn-group" role="group">
                              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              Change Status
                              <span class="caret"></span>
                              </button>
                              <ul class="dropdown-menu">
                                <li><a href="#">Active</a></li>
                                <li><a href="#">Inactive</a></li>
                              </ul>
                            </div>
                          </div>
                        </div>
                        <div class="clearfix"></div>
                        <table class="table table-bordered">
                          <thead>
                            <tr>
                              <th><input class="checkall" type="checkbox"></th>
                              <th>Rules</th>
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td><input class="checkall" type="checkbox"></td>
                              <td>If total amount is <= 1000 then shipping amount will be 5 Percentage</td>
                              <td>
                                <a href="javascript:void(0);" class="view" title="View" data-toggle="modal" data-target="#myModal2"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                <a href="javascript:void(0);" class="active" title="active" data-toggle="modal" data-target="#myModal"><i class="fa fa-check" aria-hidden="true"></i></a>
                                <a href="javascript:void(0);" class="delete" title="Remove" data-toggle="modal" data-target="#myModal1"><i class="fa fa-trash-o"></i></a>
                              </td>
                            </tr>
                            <tr>
                              <td><input class="checkall" type="checkbox"></td>
                              <td>If total amount is <= 500 then shipping amount will be 10 Percentage</td>
                              <td>
                                <a href="javascript:void(0);" class="view" title="View" data-toggle="modal" data-target="#myModal2"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                <a href="javascript:void(0);" class="active" title="active" data-toggle="modal" data-target="#myModal"><i class="fa fa-check" aria-hidden="true"></i></a>
                                <a href="javascript:void(0);" class="delete" title="Remove" data-toggle="modal" data-target="#myModal1"><i class="fa fa-trash-o"></i></a>
                              </td>
                            </tr>
                            <tr>
                              <td><input class="checkall" type="checkbox"></td>
                              <td>If total amount is <= 200 then shipping amount will be 20 Percentage</td>
                              <td>
                                <a href="javascript:void(0);" class="view" title="View" data-toggle="modal" data-target="#myModal2"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                <a href="javascript:void(0);" class="active" title="active" data-toggle="modal" data-target="#myModal"><i class="fa fa-check" aria-hidden="true"></i></a>
                                <a href="javascript:void(0);" class="delete" title="Remove" data-toggle="modal" data-target="#myModal1"><i class="fa fa-trash-o"></i></a>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThree">
                      <h4 class="panel-title">
                      <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                        Tax
                      </a>
                      </h4>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                      <div class="panel-body">
                        <div class="pull-right">
                          <div class="btn-group" role="group" aria-label="...">
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal2"><i class="fa fa-plus-square" aria-hidden="true"></i> Add New Tax</button>
                            <button type="button" class="btn btn-danger"><i class="fa fa-trash-o"></i> Delete</button>
                            <div class="btn-group" role="group">
                              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              Change Status
                              <span class="caret"></span>
                              </button>
                              <ul class="dropdown-menu">
                                <li><a href="#">Active</a></li>
                                <li><a href="#">Inactive</a></li>
                              </ul>
                            </div>
                          </div>
                        </div>
                        <div class="clearfix"></div>
                        <table class="table table-bordered">
                          <thead>
                            <tr>
                              <th><input class="checkall" type="checkbox"></th>
                              <th>Rules</th>
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td><input class="checkall" type="checkbox"></td>
                              <td>If total amount is >= 300 then tax amount will be 10 Percentage</td>
                              <td>
                                <a href="javascript:void(0);" class="view" title="View" data-toggle="modal" data-target="#myModal2"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                <a href="javascript:void(0);" class="active" title="active" data-toggle="modal" data-target="#myModal"><i class="fa fa-check" aria-hidden="true"></i></a>
                                <a href="javascript:void(0);" class="delete" title="Remove" data-toggle="modal" data-target="#myModal1"><i class="fa fa-trash-o"></i></a>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingFour">
                      <h4 class="panel-title">
                      <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                        Misc. Tax
                      </a>
                      </h4>
                    </div>
                    <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                      <div class="panel-body">
                        <div class="pull-right">
                          <div class="btn-group" role="group" aria-label="...">
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal2"><i class="fa fa-plus-square" aria-hidden="true"></i> Add New Misc. Tax</button>
                            <button type="button" class="btn btn-danger"><i class="fa fa-trash-o"></i> Delete</button>
                            <div class="btn-group" role="group">
                              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              Change Status
                              <span class="caret"></span>
                              </button>
                              <ul class="dropdown-menu">
                                <li><a href="#">Active</a></li>
                                <li><a href="#">Inactive</a></li>
                              </ul>
                            </div>
                          </div>
                        </div>
                        <div class="clearfix"></div>
                        <table class="table table-bordered">
                          <thead>
                            <tr>
                              <th><input class="checkall" type="checkbox"></th>
                              <th>Tax Type</th>
                              <th>Tax Amount</th>
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td><input class="checkall" type="checkbox"></td>
                              <td>GST</td>
                              <td>18 Percentage</td>
                              <td>
                                <a href="javascript:void(0);" class="view" title="View"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                <a href="javascript:void(0);" class="active" title="active" data-toggle="modal" data-target="#myModal"><i class="fa fa-check" aria-hidden="true"></i></a>
                                <a href="javascript:void(0);" class="delete" title="Remove" data-toggle="modal" data-target="#myModal1"><i class="fa fa-trash-o"></i></a>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div role="tabpanel" class="tab-pane" id="Coupons">
                <div style="margin-top: 15px;">
                  <div class="pull-right">
                    <div class="btn-group" role="group" aria-label="...">
                      <button type="button" class="btn btn-danger"><i class="fa fa-plus-square" aria-hidden="true"></i> Add New Discount Rule</button>
                      <button type="button" class="btn btn-danger"><i class="fa fa-trash-o"></i> Delete</button>
                      <div class="btn-group" role="group">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Change Status
                        <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                          <li><a href="#">Active</a></li>
                          <li><a href="#">Inactive</a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th><input class="checkall" type="checkbox"></th>
                        <th>Name of Coupon</th>
                        <th>Code</th>
                        <th>Issue Date</th>
                        <th>Expiry Date</th>
                        <th>Discount</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td><input class="checkall" type="checkbox"></td>
                        <td>My Coupon</td>
                        <th>123456789</th>
                        <th>07/03/2017</th>
                        <th>07/31/2017</th>
                        <th>10%</th>
                        <td>
                          <a href="javascript:void(0);" class="view" title="View" data-toggle="modal" data-target="#myModal2"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                          <a href="javascript:void(0);" class="active" title="active" data-toggle="modal" data-target="#myModal"><i class="fa fa-check" aria-hidden="true"></i></a>
                          <a href="javascript:void(0);" class="delete" title="Remove" data-toggle="modal" data-target="#myModal1"><i class="fa fa-trash-o"></i></a>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <div role="tabpanel" class="tab-pane" id="CMS">...</div>
            </div>
          </div>
        </div>
      </div>
    </section>
     <?php include 'footer.php'; ?>
   