<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>HTML</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link href="css/webkon-ecommerce.min.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <!-- Intro Section -->
  
    <section class="intro-section">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-2 col-md-2 col-sm-2"> <a href="category-order.php" class="ecom-icon-box-view <?php if($page=='category-order'){echo 'active';}?>">
            <div class="ecom-icon"><i class="fa fa-archive" aria-hidden="true"></i></div>
            <div class="ecom-icon-name">Order</div>
          </a> </div>
          <div class="col-lg-2 col-md-2 col-sm-2"> <a href="manage-customers-order.php" class="ecom-icon-box-view <?php if($page=='manage-customers-order'){echo 'active';}?>">
            <div class="ecom-icon"><i class="fa fa-users" aria-hidden="true"></i></div>
            <div class="ecom-icon-name">Manage Customers Order</div>
          </a> </div>
          <div class="col-lg-2 col-md-2 col-sm-2"> <a href="manage-orders-order.php" class="ecom-icon-box-view <?php if($page=='manage-orders-order'){echo 'active';}?>">
            <div class="ecom-icon"><i class="fa fa-first-order" aria-hidden="true"></i></div>
            <div class="ecom-icon-name">Manage Orders Order</div>
          </a> </div>
          <div class="col-lg-2 col-md-2 col-sm-2"> <a href="generel-settings.php" class="ecom-icon-box-view <?php if($page=='generel-settings'){echo 'active';}?>">
            <div class="ecom-icon"><i class="fa fa-cog" aria-hidden="true"></i></div>
            <div class="ecom-icon-name">Generel Settings</div>
          </a> </div>
          <div class="col-lg-2 col-md-2 col-sm-2"> <a href="import-order.php" class="ecom-icon-box-view <?php if($page=='import-order'){echo 'active';}?>">
            <div class="ecom-icon"><i class="fa fa-truck" aria-hidden="true"></i></div>
            <div class="ecom-icon-name">Import/Export (Order)</div>
          </a> </div>
          <div class="col-lg-2 col-md-2 col-sm-2"> <a href="sales-report-order.php" class="ecom-icon-box-view <?php if($page=='sales-report-order'){echo 'active';}?>">
            <div class="ecom-icon"><i class="fa fa-bar-chart" aria-hidden="true"></i></div>
            <div class="ecom-icon-name">Sales Report Order</div>
          </a> </div>
        </div>
      </div>
    </section>
    
    