
<?php
$page = 'import-order';
include('header-order.php');
?>
    <section class="container-fluid">
      <div class="ecom-category-area">
        <div class="row">
          <h3 class="pos-rel">
          <span>Import/Export(Product)</span>
          </h3>
        </div>
      </div>
      <div class="ecom-product-item">
      <div class="col-md-12 col-sm-12 col-lg-12">
        <div class="row">
          <div class="col-sm-2 col-md-2 col-lg-2 hidden-xs"></div>
          <div class="col-sm-8 col-md-8 col-lg-8 hidden-xs">
            <div class="col-sm-4 col-md-4 col-lg-4">
              <div class="thumbnail text-center">
                <div class="red-icon"><i class="fa fa-upload" aria-hidden="true"></i></div>
                <div class="caption">
                  <h3>Upload CSV</h3>
                  <p><a href="#" class="btn btn-primary" role="button">Upload CSV File</a> </p>
                </div>
              </div>
            </div>
            <div class="col-sm-4 col-md-4 col-lg-4">
              <div class="thumbnail text-center">
                <div class="red-icon"><i class="fa fa-download" aria-hidden="true"></i></div>
                <div class="caption">
                  <h3>Export CSV</h3>
                  <p><a href="#" class="btn btn-primary" role="button">Export CSV File</a> </p>
                </div>
              </div>
            </div>
            <div class="col-sm-4 col-md-4 col-lg-4">
              <div class="thumbnail text-center">
                <div class="red-icon"><i class="fa fa-download" aria-hidden="true"></i></div>
                <div class="caption">
                  <h3>Export XLS</h3>
                  <p><a href="#" class="btn btn-primary" role="button">Export XLS File</a> </p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-2 col-md-2 col-lg-2 hidden-xs"></div>
        </div>
        </div>
      </div>
    </section>
  <?php include 'footer.php'; ?>