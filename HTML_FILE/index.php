<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>HTML</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link href="css/webkon-ecommerce.min.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
    <!-- Intro Section -->
    <section id="intro" class="intro-section">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-4">
                   <a class="ecom-icon-box" href="category.php">
                   <div class="ecom-icon"><i class="fa fa-archive" aria-hidden="true"></i></div>
                   <div class="ecom-category-name">Product Catalog</div>
                   </a>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-4">
                   <a class="ecom-icon-box" href="manage-customers.php">
                   <div class="ecom-icon"><i class="fa fa-users" aria-hidden="true"></i></div>
                   <div class="ecom-category-name">Manage Customers</div>
                   </a>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-4">
                   <a class="ecom-icon-box" href="manage-orders.php">
                   <div class="ecom-icon"><i class="fa fa-first-order" aria-hidden="true"></i></div>
                   <div class="ecom-category-name">Manage Orders</div>
                   </a>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-4">
                     <a class="ecom-icon-box" href="generel-settings.php">
                   <div class="ecom-icon"><i class="fa fa-cog" aria-hidden="true"></i></div>
                   <div class="ecom-category-name">General Settings</div>
                   </a>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-4">
                    <a class="ecom-icon-box" href="import.php">
                   <div class="ecom-icon"><i class="fa fa-truck" aria-hidden="true"></i></div>
                   <div class="ecom-category-name">Import/Export Product</div>
                   </a>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-4">
                    <a class="ecom-icon-box" href="sales-report.php">
                   <div class="ecom-icon"><i class="fa fa-bar-chart" aria-hidden="true"></i></div>
                   <div class="ecom-category-name">Sales Report</div>
                   </a>
                </div>
            </div>
            
        </div>
    </section>

  

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>



</body>

</html>
