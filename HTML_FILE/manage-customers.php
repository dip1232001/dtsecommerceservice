
<?php
$page = 'manage-customers';
include('header.php');
?>
    <div class="modal fade" tabindex="-1" role="dialog" id="myModal" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Confirm</h4>
          </div>
          <div class="modal-body">
            <p>Are you sure, you want to update the status?</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="button" class="btn btn-primary">Confirm</button>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" tabindex="-1" role="dialog" id="myModal1" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Confirm</h4>
          </div>
          <div class="modal-body">
            <p>Are you sure you want to delete selected order(s)?</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="button" class="btn btn-primary">Confirm</button>
          </div>
        </div>
      </div>
    </div>
    
    <section class="container-fluid">
      <div class="ecom-category-area">
        <div class="row">
          <h3 class="pos-rel">
          <span>Manage Customers</span>
          </h3>
          <div>
            <div class="col-lg-2 col-md-2 col-sm-2"><label style="line-height: 28px;">Total 1 Customer(s)</label></div>
            <div class="col-lg-4 col-md-4 col-sm-4">              
              <div class="input-group">                
                <input type="text" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                  <button class="btn btn-default" type="button"><i class="fa fa-search" aria-hidden="true"></i></button>
                </span>
              </div>
            </div>
            <div class="pull-right">
              <div class="btn-group" role="group" aria-label="...">
                <button type="button" class="btn btn-danger"><i class="fa fa-trash-o"></i> Delete</button>
                <div class="btn-group" role="group">
                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Change Status
                  <span class="caret"></span>
                  </button>
                  <ul class="dropdown-menu">
                    <li><a href="#">Active</a></li>
                    <li><a href="#">Inactive</a></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
    </div>
    <div class="ecom-product-item">
      <div class="col-md-12 col-sm-12 col-lg-12">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th><input class="checkall" type="checkbox"></th>
              <th>Customer Name</th>
              <th>Email ID</th>
              <th>Phone</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><input class="checkall" type="checkbox"></td>
              <td>John</td>
              <td>john@example.com</td>
              <td>+91-9836229486</td>
              <td>
                <a href="customers-details.php" class="view" title="View"><i class="fa fa fa-eye"></i></a>
                <a href="javascript:void(0);" class="active" title="active" data-toggle="modal" data-target="#myModal"><i class="fa fa-check" aria-hidden="true"></i></a>
                <a href="javascript:void(0);" class="delete" title="Remove" data-toggle="modal" data-target="#myModal1"><i class="fa fa-trash-o"></i></a>
              </td>
            </tr>
          </tbody>
        </table>
        <nav aria-label="Page navigation" class="hidden">
          <ul class="pagination">
            <li>
              <a href="#" aria-label="Previous">
                <span aria-hidden="true">Previous</span>
              </a>
            </li>
            <li class="active"><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">5</a></li>
            <li>
              <a href="#" aria-label="Next">
                <span aria-hidden="true">Next</span>
              </a>
            </li>
          </ul>
        </nav>
      </div>
    </div>
  </section>
 <?php include 'footer.php'; ?>