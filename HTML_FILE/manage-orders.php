
<?php
$page = 'manage-orders';
include('header.php');
?>
<div class="modal fade" tabindex="-1" role="dialog" id="myModal" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Confirm</h4>
      </div>
      <div class="modal-body" style="font-size:14px;">
        <div class="row">
          <div class="col-md-12 col-sm-12">
            <div class="panel panel-default font-12">
              <div class="panel-heading"><strong>Order Details</strong></div>
              <div class="table-responsive">
                <table class="table table-condensed">
                  <tbody><tr>
                    <th class="active" width="10px"></th>
                    <th class="active">Product Image</th>
                    <th class="active">Product Name </th>
                    <th class="active">Qty</th>
                    <th class="active">Price</th>
                    <th class="active">Total</th>
                  </tr>
                  <!-- ngRepeat: prod in orderDet.productListArr --><tr ng-repeat="prod in orderDet.productListArr" class="ng-scope">
                  <td></td>
                  <td>
                    <!-- ngIf: !prod.prodImage -->
                    <!-- ngIf: prod.prodImage --><img ng-if="prod.prodImage" ng-src="http://snappy.appypie.com/media/user_space/02e33100e77d/ecomm/ecomm_1499025767_7337.jpg" width="70" height="70" class="ng-scope" src="http://snappy.appypie.com/media/user_space/02e33100e77d/ecomm/ecomm_1499025767_7337.jpg"><!-- end ngIf: prod.prodImage --></td>
                    <td ng-bind-html="prod.prodName+' '+prod.customOption" class="ng-binding">Prod 1  <br>Color:Blue</td>
                    <td ng-bind-html="prod.qtyOrdered" class="ng-binding">2</td>
                    <td ng-bind-html="currencySymbol[prod.currency]+''+prod.productPrice" class="ng-binding">$50.00</td>
                    <td ng-bind-html="currencySymbol[prod.currency]+''+prod.rowTotal" class="ng-binding">$100</td>
                    </tr><!-- end ngRepeat: prod in orderDet.productListArr --><tr ng-repeat="prod in orderDet.productListArr" class="ng-scope">
                    <td></td>
                    <td>
                      <!-- ngIf: !prod.prodImage -->
                      <!-- ngIf: prod.prodImage --><img ng-if="prod.prodImage" ng-src="http://snappy.appypie.com/media/user_space/02e33100e77d/ecomm/ecomm_1499025767_7337.jpg" width="70" height="70" class="ng-scope" src="http://snappy.appypie.com/media/user_space/02e33100e77d/ecomm/ecomm_1499025767_7337.jpg"><!-- end ngIf: prod.prodImage --></td>
                      <td ng-bind-html="prod.prodName+' '+prod.customOption" class="ng-binding">Prod 1  <br>Color:Red</td>
                      <td ng-bind-html="prod.qtyOrdered" class="ng-binding">3</td>
                      <td ng-bind-html="currencySymbol[prod.currency]+''+prod.productPrice" class="ng-binding">$40.00</td>
                      <td ng-bind-html="currencySymbol[prod.currency]+''+prod.rowTotal" class="ng-binding">$120</td>
                      </tr><!-- end ngRepeat: prod in orderDet.productListArr -->
                      <tr>
                        <td colspan="6"><table width="50%" class="table table-condensed offsetbottomnone" style="margin-top:-6px !important;">
                          <tbody><tr>
                            <td width="87%" align="right">Sub total</td>
                            <td width="" align="left"></td>
                            <td width="" align="left" ng-bind-html="currencySymbol[orderDet.orderTotalArr.currency]+''+orderDet.orderTotalArr.subTotal" class="ng-binding">$220.00</td>
                          </tr>
                          <tr>
                            <td align="right">Tax</td>
                            <td align="left"></td>
                            <td align="left" ng-bind-html="currencySymbol[orderDet.orderTotalArr.currency]+''+orderDet.orderTotalArr.tax" class="ng-binding">$0.00</td>
                          </tr>
                          <tr>
                            <td align="right"> Coupon</td>
                            <td align="left"></td>
                            <td align="left" ng-bind-html="currencySymbol[orderDet.orderTotalArr.currency]+''+orderDet.orderTotalArr.coupon" class="ng-binding">$22.00</td>
                          </tr>
                          <!-- ngRepeat: misctax in orderDet.orderTotalArr.mtax --><tr ng-repeat="misctax in orderDet.orderTotalArr.mtax" class="ng-scope">
                          <td align="right" ng-bind-html="misctax.taxType" class="ng-binding">GST</td>
                          <td align="left"></td>
                          <td align="left" ng-bind-html="currencySymbol[orderDet.orderTotalArr.currency]+''+misctax.tax" class="ng-binding">$39.60</td>
                          </tr><!-- end ngRepeat: misctax in orderDet.orderTotalArr.mtax -->
                          <tr>
                            <td align="right">Discount</td>
                            <td align="left"></td>
                            <td align="left" ng-bind-html="currencySymbol[orderDet.orderTotalArr.currency]+''+orderDet.orderTotalArr.discount" class="ng-binding">$0.00</td>
                          </tr>
                          <tr>
                            <td align="right">SHIPPING</td>
                            <td align="left"></td>
                            <td align="left" ng-bind-html="currencySymbol[orderDet.orderTotalArr.currency]+''+orderDet.orderTotalArr.shipping" class="ng-binding">$23.76</td>
                          </tr>
                          <tr>
                            <td align="right" class="active"><strong>Grand Total</strong></td>
                            <td align="left" class="active"></td>
                            <td align="left" class="active"><strong ng-bind-html="currencySymbol[orderDet.orderTotalArr.currency]+''+orderDet.orderTotalArr.gTotal" class="ng-binding">$261.36</strong></td>
                          </tr>
                        </tbody></table></td>
                      </tr>
                    </tbody></table>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-sm-6">
                <div class="panel panel-default font-12 min-height-ecom">
                  <div class="panel-heading"><strong>Order Information</strong></div>
                  <div class="panel-body">
                    <div class="row offsetbottom10">
                      <div class="col-md-12">
                        <label><strong>Order Date</strong></label>
                        : <span ng-bind-html="orderDet.orderDate" class="ng-binding">03 Jul 2017 15:28:22</span></div>
                      </div>
                      <div class="row offsetbottom10">
                        <div class="col-md-12"><strong>Order Status </strong> : <span ng-bind-html="orderDet.orderStatus" class="ng-binding">Processing</span> </div>
                      </div>
                      <div class="row offsetbottom10">
                        <div class="col-md-12"><strong>Payment Method </strong> : <span ng-bind-html="orderDet.paymentMethod" class="ng-binding">Cash on Delivery</span> </div>
                      </div>
                      
                      <div class="row offsetbottom10 ng-hide" ng-show="(orderDet.paymentMethodCode=='payu_money' || orderDet.paymentMethodCode=='paypal_express')">
                        <div class="col-md-12" ng-show="orderDet.transactionId=='-'"><strong>Payment Status </strong> : <span> In Process</span> </div>
                        
                        <div class="col-md-12  ng-hide" ng-show="orderDet.transactionId!='-'"><strong>Payment Status </strong> : <span> Complete</span> </div>
                      </div>
                      
                      <div class="row offsetbottom10 ng-hide" ng-show="orderDet.paymentMethodCode=='payu_money' || orderDet.paymentMethodCode=='paypal_express'">
                        <div class="col-md-12"><strong>Transaction Id </strong> : <span ng-bind-html="orderDet.transactionId" class="ng-binding">-</span> </div>
                      </div>
                      
                    </div>
                  </div>
                </div>
                <div class="col-md-6 col-sm-6">
                  <div class="panel panel-default font-12 min-height-ecom">
                    <div class="panel-heading"><strong>Customer Information</strong></div>
                    <div class="panel-body">
                      <div class="row offsetbottom10">
                        <div class="col-md-12"><strong>Customer Name </strong> : <span ng-bind-html="orderDet.billingname" class="ng-binding">test</span> </div>
                      </div>
                      <div class="row offsetbottom10">
                        <div class="col-md-12"><strong>Email</strong> : <span ng-bind-html="orderDet.billingemail" class="ng-binding">dts.ritwik@dreamztech.com</span> </div>
                      </div>
                      <div class="row offsetbottom10">
                        <div class="col-md-12"><strong>Phone No</strong> : <span ng-bind-html="orderDet.customerPhone" class="ng-binding">9836229486</span> </div>
                      </div>
                      <div class="row offsetbottom10 ng-hide" ng-show="orderDet.paymentMethodCode=='payu_money' || orderDet.paymentMethodCode=='paypal_express'">
                        <div class="col-md-12">&nbsp;</div>
                      </div>
                      <div class="row offsetbottom10 ng-hide" ng-show="orderDet.paymentMethodCode=='payu_money' || orderDet.paymentMethodCode=='paypal_express'">
                        <div class="col-md-12">&nbsp;</div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-6 col-sm-6">
                  <div class="panel panel-default font-12 min-height-ecom">
                    <div class="panel-heading"><strong>Billing Address</strong></div>
                    <div class="panel-body">
                      <div class="row">
                        <div class="col-md-12">
                          <div class="offsetbottom10 ng-binding" ng-show="orderDet.billingname" ng-bind-html="orderDet.billingname+','">test,</div>
                          <div class="offsetbottom10 ng-binding" ng-show="orderDet.billingaddress" ng-bind-html="orderDet.billingaddress+' ,'">321 ,</div>
                          <div ng-show="orderDet.shippingcountry" class="offsetbottom10 ng-binding" ng-bind-html="orderDet.billingcity+' '+orderDet.billingzip+' '+orderDet.billingstate+' ('+orderDet.billingcountry+')'">231 132131 sfds ()</div>
                          <div ng-show="!orderDet.shippingcountry" class="offsetbottom10 ng-binding ng-hide" ng-bind-html="orderDet.billingcity+' '+orderDet.billingzip+' '+orderDet.billingstate">231 132131 sfds</div>
                          
                          <div ng-show="orderDet.billingphone" ng-bind-html="'Telephone'+' : '+orderDet.billingphone" class="ng-binding">Telephone : 12312412</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-6 col-sm-6">
                  <div class="panel panel-default font-12 min-height-ecom">
                    <div class="panel-heading"><strong>Shipping Address</strong></div>
                    <div class="panel-body">
                      <div class="row">
                        <div class="col-md-12">
                          <div class="offsetbottom10 ng-binding" ng-show="orderDet.shippingname" ng-bind-html="orderDet.shippingname+','">test,</div>
                          <div class="offsetbottom10 ng-binding" ng-show="orderDet.shippingaddress" ng-bind-html="orderDet.shippingaddress+' ,'">321 ,</div>
                          <div ng-show="orderDet.shippingcountry" class="offsetbottom10 ng-binding" ng-bind-html="orderDet.shippingcity+' '+orderDet.shippingzip+' '+orderDet.shippingstate+' ('+orderDet.shippingcountry+')'">231 132131 sfds (AF)</div>
                          
                          <div ng-show="!orderDet.shippingcountry" class="offsetbottom10 ng-binding ng-hide" ng-bind-html="orderDet.shippingcity+' '+orderDet.shippingzip+' '+orderDet.shippingstate">231 132131 sfds</div>
                          <div ng-show="orderDet.shippingphone" ng-bind-html="'Telephone'+' : '+orderDet.shippingphone" class="ng-binding">Telephone : 12312412</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row ng-scope" ng-if="orderDet.commentsArr.length>0">
                <div class="col-md-12 col-sm-12">
                  <div class="panel panel-default font-12">
                    <div class="panel-heading"><strong>Comments History</strong></div>
                    <!-- ngRepeat: comments in orderDet.commentsArr --><div class="panel-body ng-scope" ng-repeat="comments in orderDet.commentsArr"> <strong>Status</strong> : <span ng-bind-html="comments.commentsStatus" class="ng-binding">Pending</span><br>
                    <strong>Comments</strong> : <span ng-bind-html="comments.comment" class="ng-binding">Order Placed</span> <br>
                    <strong>Date</strong> : <span ng-bind-html="comments.commentdate" class="ng-binding">03 Jul 2017 15:28:22</span> </div><!-- end ngRepeat: comments in orderDet.commentsArr --><div class="panel-body ng-scope" ng-repeat="comments in orderDet.commentsArr"> <strong>Status</strong> : <span ng-bind-html="comments.commentsStatus" class="ng-binding">Processing</span><br>
                    <strong>Comments</strong> : <span ng-bind-html="comments.comment" class="ng-binding">Payment Made</span> <br>
                    <strong>Date</strong> : <span ng-bind-html="comments.commentdate" class="ng-binding">03 Jul 2017 15:29:41</span> </div><!-- end ngRepeat: comments in orderDet.commentsArr --><div class="panel-body ng-scope" ng-repeat="comments in orderDet.commentsArr"> <strong>Status</strong> : <span ng-bind-html="comments.commentsStatus" class="ng-binding">Processing</span><br>
                    <strong>Comments</strong> : <span ng-bind-html="comments.comment" class="ng-binding">Invoice Created</span> <br>
                    <strong>Date</strong> : <span ng-bind-html="comments.commentdate" class="ng-binding">03 Jul 2017 15:29:46</span> </div><!-- end ngRepeat: comments in orderDet.commentsArr -->
                  </div>
                </div>
              </div>
            </div>

            <div class="modal-footer">
              <button type="button" class="btn btn-default">Ship</button>
              <button type="button" class="btn btn-primary">Print Invoice</button>
            </div>
          </div>
        </div>
      </div>
    
<div class="modal fade" tabindex="-1" role="dialog" id="myModal1" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Confirm</h4>
      </div>
      <div class="modal-body">
        <p>Are you sure you want to delete selected order(s)?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary">Confirm</button>
      </div>
    </div>
  </div>
</div>

<section class="container-fluid">
  <div class="ecom-category-area">
    <div class="row">
      <h3 class="pos-rel">
      <span>Manage Orders</span>
      </h3>
      <div>
        <div class="col-lg-2 col-md-2 col-sm-3">
          <label>Search Order id</label>
          <div class="input-group">
            <input type="text" class="form-control">
            <span class="input-group-btn">
              <button class="btn btn-default" type="button"><i class="fa fa-search" aria-hidden="true"></i></button>
            </span>
          </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-3">
          <label>Start date</label>
          <div class="input-group date" data-provide="datepicker">
            <input type="text" class="form-control">
            <div class="input-group-addon">
              <i class="fa fa-calendar-check-o" aria-hidden="true"></i>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-3">
          <label>End date</label>
          <div class="input-group date" data-provide="datepicker">
            <input type="text" class="form-control">
            <div class="input-group-addon">
              <i class="fa fa-calendar-check-o" aria-hidden="true"></i>
            </div>
          </div>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-3">
          <label>Status</label>
          <div>
            <button type="button" style="width:100%" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Select
            <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
              <li><a href="#"> Pending </a></li>
              <li><a href="#"> Processing </a></li>
              <li><a href="#"> Complete </a></li>
              <li><a href="#"> Cancelled </a></li>
              <li><a href="#"> In Transit </a></li>
            </ul>
          </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-lg-12 col-md-12 col-sm-12">
          <div class="pull-right text-center" style="margin-top: 10px;">
            <div>
              <button type="button" class="btn btn-danger">Search</button>
              <button type="button" class="btn btn-danger">Reset</button>
              <button type="button" class="btn btn-danger">Export (CSV)</button>
              <button type="button" class="btn btn-danger">Export (XLS)</button>
              <button type="button" class="btn btn-danger"><i class="fa fa-trash-o"></i> Delete</button>
            </div>
          </div>
        </div>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
  <div class="ecom-product-item">
    <div class="col-md-12 col-sm-12 col-lg-12">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th><input class="checkall" type="checkbox"></th>
            <th>Order ID</th>
            <th>Purchased On</th>
            <th>Bill to Name</th>
            <th>Ship to Name</th>
            <th>Grand Total</th>
            <th>Transaction Id</th>
            <th>Payment Status</th>
            <th>Payment Method</th>
            <th>Order Status</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td><input class="checkall" type="checkbox"></td>
            <td>web1707031499075902</td>
            <td>03 Jul 2017 15:28:22</td>
            <td>test</td>
            <td>test</td>
            <td>$261.36</td>
            <td>cod</td>
            <td>Pending Payment</td>
            <td>Cash on Delivery</td>
            <td>Processing</td>
            <td>
              <a href="javascript:void(0);" class="view" title="View" data-toggle="modal" data-target="#myModal"><i class="fa fa fa-eye"></i></a>
              <a href="javascript:void(0);" class="delete" title="Remove" data-toggle="modal" data-target="#myModal1"><i class="fa fa-trash-o"></i></a>
            </td>
          </tr>
        </tbody>
      </table>
      <nav aria-label="Page navigation">
        <ul class="pagination">
          <li>
            <a href="#" aria-label="Previous">
              <span aria-hidden="true">Previous</span>
            </a>
          </li>
          <li class="active"><a href="#">1</a></li>
          <li><a href="#">2</a></li>
          <li><a href="#">3</a></li>
          <li><a href="#">4</a></li>
          <li><a href="#">5</a></li>
          <li>
            <a href="#" aria-label="Next">
              <span aria-hidden="true">Next</span>
            </a>
          </li>
        </ul>
      </nav>
    </div>
  </div>
</section>

<?php include 'footer.php'; ?>