<?php
$page = 'category';
include('header.php');
?>
<section class="container-fluid">
  <div class="ecom-category-area">
    <div class="row">
      <h3 class="pos-rel">
      <span>Product</span>
      </h3>
      <div class="pull-right">
        <button type="button" data-toggle="modal" data-target=".bs-example-modal-lg" class="btn btn-danger">Add</button>
      </div>
      <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <div class="modal-title ecom-h2">Add Product</div>
            </div>
            <div class="modal-body">
              <form>
                <div class="form-group">
                  <label for="exampleInput">Product Name</label>
                  <input type="text" class="form-control" placeholder="Enter Product name">
                </div>
                <div class="form-group">
                  <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6">
                      <div class="form-group">
                        <label for="exampleInput">SKU</label>
                        <input type="text" class="form-control" placeholder="Enter Product SKU Number">
                      </div>
                      <div class="form-group">
                        <label for="exampleInput">Price</label>
                        <input type="text" class="form-control" placeholder="Enter Price i.e 20">
                      </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                      <div class="form-group">
                        <label for="exampleInput">Quantity</label>
                        <input type="text" class="form-control" placeholder="Quantity">
                      </div>
                      <div class="form-group">
                        <label for="exampleInput">Unit</label>
                        <div class="row">
                          <div class="col-lg-6 col-md-6 col-sm-6">
                            <select class="form-control">
                              <option>Select</option>
                            </select>
                          </div>
                          <div class="col-lg-6 col-md-6 col-sm-6 spacer-top">
                            <input type="text" class="form-control" placeholder="Quantity">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="textarea">Description</label>
                  <textarea class="form-control" rows="3"></textarea>
                </div>
                <div class="media">
                  <a href="#">Media</a>
                  <div>
                    <div class="bs-callout bs-callout-warning" id="callout-btn-group-accessibility"> 
                      <h4>Images</h4> 
                      <div class="ecom-thumbnail"><i class="fa fa-file-image-o" aria-hidden="true"></i></div>
                      <div class="panel-body img-file-tab">
                        <div class="text-center">
                          <span class="btn btn-default btn-file img-select-btn" style="overflow: hidden;position: relative;">
                            <span>Change</span>
                            <input type="file" name="img-file-input">
                          </span>
                          <span class="btn btn-default img-remove-btn" style="display: inline-block;">Remove</span>
                        </div>
                      </div>
                      <p>Note: For best results, image resolution should be 700x350</p>                 
                    </div>
                    <div class="bs-callout bs-callout-warning" id="callout-btn-group-accessibility"> 
                      <h4>Video</h4> 
                      <div class="panel-body img-file-tab">
                        <div>
                          <p>Only one video can be uploaded.</p>  
                          <div class="input-group"> 
                            <span class="input-group-addon" id="basic-addon2">YouTube</span> 
                            <input class="form-control" placeholder="Recipient's username" aria-describedby="basic-addon2">
                          </div>   
                          <div class="text-center"><strong>or</strong></div>                       
                          <div class="input-group"> 
                            <span class="input-group-addon" id="basic-addon2">Upload Video</span> 
                            <input class="form-control" placeholder="Recipient's username" aria-describedby="basic-addon2">
                          </div>
                        </div>
                      </div>
                                     
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="row">
                    <label class="col-sm-2 control-label">Product Option</label>
                    <div class="pull-right col-sm-2 ">
                      <button type="submit" class="btn btn-info">Add Option</button>
                    </div>
                  </div>
                  <div>
                    <div>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="row">
                            <div class="col-md-4"><strong>Title (Color, Size etc.) <span style="color:red;">*</span></strong>
                            <input type="text" class="form-control">
                            <div class="addMoreAlert"><a class="f-right icon-cancel f-right" href=""></a><span></span></div>
                          </div>
                          <div class="col-md-4"> <strong>Required</strong>
                            <select class="form-control">
                              <option value="1">Yes</option>
                              <option value="0">No</option>
                            </select>
                            <div class="addMoreAlert"><a class="f-right icon-cancel f-right" href=""></a><span></span></div>
                          </div>
                          <div class="col-md-3"> <strong>Sort Order</strong>
                            <input type="text" class="form-control">
                            <div class="addMoreAlert"><a class="" href=""></a><span></span></div>
                          </div>
                          <div class="col-md-1"><br>
                            <a href="javascript:void(0)" class="btn pull-right btn-danger">
                              <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </a>
                            </div>
                          </div>
                          <br>
                          <div class="table-responsive">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table  table-bordered offsetbottomnone" style="min-width:650px">
                              <tbody><tr>
                                <th width="30%" class="info grayBg">Title Value <span style="color:red;">*</span> </th>
                                <th width="20%" class="info grayBg">Price <span style="color:red;">*</span></th>
                                <th width="20%" class="info grayBg">Price Type</th>
                                <th width="25%" class="info grayBg">Sort Order</th>
                              </tr>
                              <tr>
                                <td ><input type="text" class="form-control">
                                <div class="addMoreAlert"><a href=""></a><span class="ng-binding"></span></div></td>
                                <td class="alertRelative"><input type="text" class="form-control">
                                <div class="addMoreAlert ng-hide"><a href=""></a><span></span></div></td>
                                <td class="alertRelative"><select class="form-control">
                                  <option value="">Select</option>
                                  <option value="p">+</option>
                                  <option value="m">-</option>
                                </select>
                                <div class="addMoreAlert ng-hide"><a href=""></a><span></span></div></td>
                                <td class="alertRelative"><input type="text" class="form-control">
                                <div class="addMoreAlert ng-hide"><a href=""></a><span ></span></div></td>
                              </tr>
                              <tr>
                                <td colspan="5" align="right"><a href="javascript:void(0)" class="btn btn-danger">Add Row</a></td>
                              </tr>
                            </tbody></table>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-success">Save</button>
              <button type="button" class="btn btn-danger" data-dismiss="modal">Cancle</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="ecom-search-area">
    <form class="form-inline">
      <div class="row">
        <div class="col-lg-2 col-md-2 col-sm-2">
          <div class="form-group">
            <select class="form-control">
              <option>All</option>
            </select>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3">
          <div class="input-group">
            <input type="text" class="form-control" aria-describedby="inputGroupSuccess2Status" placeholder="Search Products">
            <span class="input-group-addon"><a href="#"><i class="fa fa-search" aria-hidden="true"></i></a></span> </div>
          </div>
          <div class="col-lg-1 col-md-1 col-sm-2 col-lg-offset-4 col-md-offset-4 col-sm-offset-3 spacer-top">
            <button type="submit" class="btn btn-primary">Select All</button>
          </div>
          <div class="col-lg-2 col-md-2 col-sm-2 spacer-top">
            <div class="btn-group" role="group">
              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Change Status
              <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                <li><a href="#">Active</a></li>
                <li><a href="#">Inactive</a></li>
                <li><a href="#">Featured</a></li>
                <li><a href="#">Remove Featured</a></li>
                <li><a href="#">Offered</a></li>
                <li><a href="#">Remove Offered</a></li>
                <li><a href="#">Delete</a></li>
                <li><a href="#">Selected</a></li>
              </ul>
            </div>
          </div>
        </div>
      </form>
    </div>
    <div class="ecom-product-item">
      <div class="dirCateItems">
        <div class="dir_bx narrowed biggerstackorder">
          <span class="bigcheck">
            <label class="bigcheck">
              <input type="checkbox" class="bigcheck" name="cheese" value="yes"/>
              <span class="bigcheck-target"></span>
            </label>
          </span>
          <div class="ecom-product"> <img src="img/product01.jpg" alt=""/> </div>
          <a href="#" class="ecom-catnumber">Product 1</a>
          <div class="ecom-product-name">Lorem Ipsum is simply dummy text of the ...</div>
          <div class="ecom-action"><a href="#" class="action-setting"><i class="fa fa-cogs" aria-hidden="true"></i></a>
          <div class="marg-nil">
            <span class="label label-success">In Stock</span>
            <span class="label label-danger hidden">Out of Stock</span>
          </div>
        </div>        
      </div>      
    </div>
    <div class="dirCateItems">
        <div class="dir_bx narrowed biggerstackorder">
          <span class="bigcheck">
            <label class="bigcheck">
              <input type="checkbox" class="bigcheck" name="cheese" value="yes"/>
              <span class="bigcheck-target"></span>
            </label>
          </span>
          <div class="ecom-product"> <img src="img/product01.jpg" alt=""/> </div>
          <a href="#" class="ecom-catnumber">Product 1</a>
          <div class="ecom-product-name">Lorem Ipsum is simply dummy text of the ...</div>
          <div class="ecom-action"><a href="#" class="action-setting"><i class="fa fa-cogs" aria-hidden="true"></i></a>
          <div class="marg-nil">
            <span class="label label-success">In Stock</span>
            <span class="label label-danger hidden">Out of Stock</span>
          </div>          
        </div>  
        <div class="dirCateItemsOverlay">
          <div class="row">
            <div class="col-md-7"><a href="#" class="btn btn-danger"> Edit</a></div>
            <div class="col-md-5"> <a href="#" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div>
          </div>
          <a href="#" class="btn btn-danger"> Inactive</a>
          <a href="#" class="btn btn-danger"> Add Featured Product</a>
          <a href="#" class="btn btn-danger"> Add Offer Zone Product</a>
          <a href="#" class="btn btn-danger"> Media</a>
          <a href="#" class="hideOverlay"> Close</a>
        </div>            
      </div>
  </div>
</section>
<?php include 'footer.php'; ?>