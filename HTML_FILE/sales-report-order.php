
<?php
$page = 'sales-report-order';
include('header-order.php');
?>
    <section class="container-fluid">
      <div class="ecom-category-area">
        <div class="row">
          <h3 class="pos-rel">
          <span>Sales Reports</span>
          </h3>
          <div>
            <div class="col-lg-4 col-md-4 col-sm-4">
              <label>Start date</label>
              <div class="input-group date" data-provide="datepicker">
                <input type="text" class="form-control">
                <div class="input-group-addon">
                  <i class="fa fa-calendar-check-o" aria-hidden="true"></i>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
              <label>End date</label>
              <div class="input-group date" data-provide="datepicker">
                <input type="text" class="form-control">
                <div class="input-group-addon">
                  <i class="fa fa-calendar-check-o" aria-hidden="true"></i>
                </div>
              </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
              <label>Period</label>
              <div>
                <button type="button" style="width:100%" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Select
                <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                  <li><a href="#"> Day </a></li>
                  <li><a href="#"> Month </a></li>
                  <li><a href="#"> Year </a></li>
                </ul>
              </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
              <label>Status</label>
              <div>
                <button type="button" style="width:100%" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Select
                <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                  <li><a href="#"> Pending </a></li>
                  <li><a href="#"> Processing </a></li>
                  <li><a href="#"> Complete </a></li>
                  <li><a href="#"> Cancelled </a></li>
                  <li><a href="#"> In Transit </a></li>
                </ul>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-lg-12 col-md-12 col-sm-12">
              <div class="pull-right text-center" style="margin-top: 10px;">
                <div>
                  <button type="button" class="btn btn-danger">Filter</button>
                  <button type="button" class="btn btn-danger">Reset</button>
                  <button type="button" class="btn btn-danger">Export (CSV)</button>
                  <button type="button" class="btn btn-danger">Export (XLS)</button>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
      <div class="ecom-product-item">
        <div class="col-md-12 col-sm-12 col-lg-12">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th>Period</th>
                <th>Quantity</th>
                <th>Orders</th>
                <th>Sales Items</th>
                <th>Sales Total</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>Last 7 days</td>
                <td>5</td>
                <td>1</td>
                <td>3</td>
                <td>$261.36</td>
              </tr>
              <tr>
                <td>Last 30 days</td>
                <td>5</td>
                <td>1</td>
                <td>3</td>
                <td>$261.36</td>
              </tr>
              <tr>
                <td>Last 1 year</td>
                <td>5</td>
                <td>1</td>
                <td>3</td>
                <td>$261.36</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </section>
    <?php include 'footer.php'; ?>
 