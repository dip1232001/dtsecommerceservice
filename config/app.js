var EcommerceApp = angular.module('EcommerceApp', ['ngRoute', 'ngCookies', 'ngSanitize', 'ui.bootstrap', 'Alertify', 'bw.paging', 'simditor', 'textAngular','ui.tree']);
EcommerceApp.run(['$rootScope', 'CommonService', '$cookies', '$location', function ($rootScope, CommonService, $cookies, $location) {
    $rootScope.$on('$routeChangeStart', function (event, current) {
        console.log('$location==>',$location);
        console.log('$location==>',$location.url());
        var BASEKEY = $cookies.get('K');
        if (!BASEKEY && angular.isDefined(current.$$route.originalPath) && current.$$route.originalPath !== '/login') {
            var parsedDate = new Date()
            var newDate = new Date(parsedDate.getTime() + (1000 * 3600) - 20);
            var expStr = moment(newDate).format('LLLL');
            $cookies.put('RP', encodeURI($location.url()), {'expires': expStr});
            event.preventDefault();
            $location.path('/login');
        }
    });

    $rootScope.getGeneralSettings = '';
    //$rootScope._ = window._;


    CommonService.getGeneralSettings().then(function (response) {
        //console.log(response);
        if (response.error) {
            $scope.error = true;
            $scope.msg = response.msg;
        } else {
            $rootScope.getGeneralSettings = response.data.data;
            console.log($rootScope.getGeneralSettings);
        }
    });

    $rootScope.MatchUrl = function (Url) {
        if (typeof (Url) !== 'undefined') {
            return (window.location.pathname.indexOf(Url) === -1) ? false : true;
        }
        return false;
    }

    $rootScope.setImage = function (inputfiled, elementid) {
        return setImage(inputfiled, elementid);
    }

    $rootScope.isMobile = {
        Android: function() {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function() {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function() {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function() {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function() {
            return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
        },
        any: function() {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
    };

}]);

EcommerceApp.directive('stringToNumber', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
            ngModel.$parsers.push(function (value) {
                return '' + value;
            });
            ngModel.$formatters.push(function (value) {
                return parseFloat(value);
            });
        }
    }

});
EcommerceApp.factory('serverUrls', [function () {
    return {
        authenticate: HOSTURL + 'Authenticate',

        createcategory: HOSTURL + 'Category/addCategory',
        getcategory: HOSTURL + 'Category/getCategories',
        deletecategory: HOSTURL + 'Category/deleteCategories',
        getCategoryTree: HOSTURL + 'Category/GetCategoriesTree',
        updateCategorieOrder: HOSTURL + 'Category/updateCategorieOrder',
        GetProductOrderForCategory: HOSTURL + 'Product/GetProductOrderForCategory',
        reorderProduct: HOSTURL + 'Product/SetProductOrderForCategory',

        getproductpricetype: HOSTURL + 'Product/getPriceTypes',
        createproduct: HOSTURL + 'Product/addProduct',
        getproduct: HOSTURL + 'Product/getAllProducts',
        deleteproduct: HOSTURL + 'Product/DeleteProduct',

        deleteproductImage: HOSTURL + 'Product/deleteProductImage',
        deleteproductVideo: HOSTURL + 'Product/deleteProductVideo',
        deleteproductDocument: HOSTURL + 'Product/deleteProductDocument',
        deleteproductTestimonial: HOSTURL + 'Product/deleteProductTestimonial',

        getsingleproduct: HOSTURL + 'Product/getProduct',
        changeproductstats: HOSTURL + 'Product/changeProductStats',
        getorder: HOSTURL + 'Product/getorders',
        GetOrderStatusList: HOSTURL + 'Product/GetOrderStatusList',
        ChangeOrderStatus: HOSTURL + 'Product/ChangeOrderStatus',
        UpdateTracking: HOSTURL + 'Product/UpdateTracking',
        getSalesreport: HOSTURL + 'Product/getorders',
        importProducts: HOSTURL + 'Product/ImportProducts',
        getPaymentTypes: HOSTURL + 'Product/getPaymentTypes',
        gePaymentStatusList: HOSTURL + 'Product/gePaymentStatusList',
        generateProductQR: HOSTURL + 'Product/generateProductQR',
        generateProductQRPDF: HOSTURL + 'Product/generateProductQRPDF',
        getrawdata: HOSTURL + 'Product/getrawdata',
        getDefinedARTagsByBU: HOSTURL + 'Product/getDefinedARTagsByBU',
        makeDefaultProductImage: HOSTURL + 'Product/makeDefaultProductImage',

        createcustomer: HOSTURL + 'Customer/Register',
        getcustomer: HOSTURL + 'Customer/getCustomers',
        getcustomerdetails: HOSTURL + 'Customer/getCustomer',
        addcustomerbillingaddress: HOSTURL + 'Customer/addBillingAddress',
        addcustomershippingaddress: HOSTURL + 'Customer/addShippingAddress',
        getcustomerbillingaddress: HOSTURL + 'Customer/getBillingAddress',
        getcustomershippingaddress: HOSTURL + 'Customer/getShippingAddress',
        setDefaultBillingAddress: HOSTURL + 'Customer/setDefaultBillingAddress',
        setDefaultShippingAddress: HOSTURL + 'Customer/setDefaultShippingAddress',
        DeleteCustomerAdress: HOSTURL + 'Customer/DeleteCustomerAdress',

        getcurrencylists: HOSTURL + 'Settings/GetCurrencyLists',
        getgeneralsettings: HOSTURL + 'Settings/GetGeneralSettings',
        savegeneralsettings: HOSTURL + 'Settings/setSettings',
        getAllPayments: HOSTURL + 'Settings/getAllPayments',
        savePayments: HOSTURL + 'Settings/setPayMentMethods',
        getAddedPaymentTypes: HOSTURL + 'Settings/getAddedPaymentTypes',
        saveGeneralImpSettings: HOSTURL + 'Settings/enableSettings',
        GetOrderTypeList: HOSTURL + 'Settings/GetDeliveyOptionsByBU',

        getTaxRule: HOSTURL + 'Tax/getTaxes',
        saveTaxRule: HOSTURL + 'Tax/addTax',
        changeTaxRuleStatus: HOSTURL + 'Tax/ChangeTaxStatus',
        DeleteTaxes: HOSTURL + 'Tax/DeleteTaxes',
        DeleteMultiTaxes: HOSTURL + 'Tax/DeleteMultiTaxes',

        GetReports: HOSTURL + 'Report/GetReports',

        getEmailList: HOSTURL + 'Email/getEmailList',
        getEmailDetails: HOSTURL + 'Email/getEmailDetails',
        editEmailDetails: HOSTURL + 'Email/editEmailDetails',

        ChangePaymentStatus: HOSTURL + 'Payment/ChangePaymentStatus',

        AddCoupon: HOSTURL + 'Coupon/addCoupon',
        GetCoupons: HOSTURL + 'Coupon/getCoupon',
        GetCouponTypes: HOSTURL + 'Coupon/GetCouponTypes',
        GetSpecialCouponTypes: HOSTURL + 'Coupon/GetCouponTypeSpecial',
        ChangeCouponStatus: HOSTURL + 'Coupon/ChangeCouponStatus',
        DeleteCoupon: HOSTURL + 'Coupon/DeleteCoupon',

        AddTestimonial: HOSTURL + 'Product/AddTestimonial',
        GetTestimonial: HOSTURL + 'Product/GetTestimonial',

        GetOutlet: HOSTURL + 'outlet/getOutletByBusinessOutletId',
        AddOutlet: HOSTURL + 'outlet/addOutlet',
        DeleteOutlet: HOSTURL + 'outlet/deleteOutlet',

        GetPreorderSettings: HOSTURL + 'Preorder/getPreorder',
        savePreorderSettings: HOSTURL + 'Preorder/addPreorder',
        GetPreorderTables: HOSTURL + 'Preorder/getPreorderTable',
        savePreorderTable: HOSTURL + 'Preorder/addPreorderTable',
        deletePreOrderTable: HOSTURL + 'Preorder/deletePreOrderTable',
        GetNewOrderCount: HOSTURL + 'Preorder/GetNewOrderCount'

    };
}]);
EcommerceApp.factory('storeType', [function () {
    return {
        store: "7",
        resturent: "6",
    };
}]);

EcommerceApp.factory('isMobile', [function () {
    return isMobile = {
        Android: function() {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function() {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function() {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function() {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function() {
            return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
        },
        any: function() {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
    };
}]);

EcommerceApp.filter('htmlToPlaintext', function () {
    return function (text) {
        return text ? String(text).replace(/<[^>]+>/gm, '') : '';
    };
}
);