EcommerceApp.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
        $routeProvider
                .when('/login', {
                    templateUrl: './modules/templetes/login.html',
                    controller: 'LoginController'
                })
                .when('/dashboard', {
                    templateUrl: './modules/templetes/dashboard.html',
                    controller: 'DashboardController'
                })
                .when('/category', {
                    templateUrl: './modules/templetes/category.html',
                    controller: 'CategoryController'
                })
                .when('/products', {
                    templateUrl: './modules/templetes/product.html',
                    controller: 'ProductController'
                })
                .when('/subcategory/:id', {
                    templateUrl: './modules/templetes/category.html',
                    controller: 'CategoryController'
                })
                .when('/products/:catId', {
                    templateUrl: './modules/templetes/product.html',
                    controller: 'ProductController'
                })
                .when('/categoryproductorder/:catId', {
                    templateUrl: './modules/templetes/categporyproductorder.html',
                    controller: 'CategoryProductOrderController'
                })
                .when('/product-details', {
                    templateUrl: './modules/templetes/product-details.html',
                    controller: 'ProductdetailsController'
                })
                .when('/product-testimonials/:id', {
                    templateUrl: './modules/templetes/product-testimonials.html',
                    controller: 'ProducttestimonialsController'
                })
                .when('/manage-customers', {
                    templateUrl: './modules/templetes/manage-customers.html',
                    controller: 'CustomerController'
                })
                .when('/edit-customer', {
                    templateUrl: './modules/templetes/edit-customer.html',
                    controller: 'EditcustomerController'
                })
                .when('/customer-details/:id', {
                    templateUrl: './modules/templetes/customer-details.html',
                    controller: 'CustomerController'
                })
                .when('/customer-address/:type/:id', {
                    templateUrl: './modules/templetes/customer-address.html',
                    controller: 'CustomeraddressController'
                })
                .when('/manage-orders', {
                    templateUrl: './modules/templetes/manage-orders.html',
                    controller: 'OrderController'   
                })
                .when('/order-details/:id', {
                    templateUrl: './modules/templetes/order-details.html',
                    controller: 'OrderdetailsController'
                })
                .when('/general-settings', {
                    templateUrl: './modules/templetes/general-settings.html',
					controller: 'GeneralsettingsController'
                })
                .when('/coupon-settings', {
                    templateUrl: './modules/templetes/coupon-settings.html',
					controller: 'CouponsettingsController'
                })
                .when('/import-export', {
                    templateUrl: './modules/templetes/import-export.html',
                    controller: 'ImportexportproductController'
                })
                .when('/sales-report', {
                    templateUrl: './modules/templetes/sales-report.html',
                    controller: 'SalesreportController'
                })
                .when('/email-template', {
                    templateUrl: './modules/templetes/email-template.html',
                    controller: 'EmailtemplateController'
                })
                .when('/manage-email-template/:id', {
                    templateUrl: './modules/templetes/manage-email-template.html',
                    controller: 'ManageemailtemplateController'
                })
                .when('/qrscanner', {
                    templateUrl: './modules/templetes/qrscanner.html',
                    controller: 'QrscannerController'
                })
                .otherwise({
                    redirectTo: '/dashboard'
                });

        $locationProvider.html5Mode({
            enabled: true
        });
    }]);