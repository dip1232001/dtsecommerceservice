USE [MTCEddyStoneV2_Stage]
GO
/****** Object:  StoredProcedure [dbo].[PHP_Ecommerce_AddVarient]    Script Date: 7/18/2017 12:21:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER  PROCEDURE [dbo].[PHP_Ecommerce_AddVarient]
(
@productID int,
@data  xml
)
AS
BEGIN
SET NOCOUNT ON
 DECLARE @TransCount AS INT
    BEGIN TRY
		
			if(@productID <= 0)
			 BEGIN
				  RAISERROR ('Sorry varient product is required',0,0)
				  RETURN
			 END

        SET @TransCount = @@TRANCOUNT
        IF @TransCount > 0 
            SAVE TRANSACTION PrevTran
        ELSE 
            BEGIN TRANSACTION
	create table #Tempvarient
	(
	id int,
	is_required bit,
	[name] nvarchar(500),
	sort_order smallint,
	childrens xml
	)
	insert into #Tempvarient
	select	ROW_NUMBER ( ) OVER(ORDER BY Data.value('name[1]', 'nvarchar(500)') ASC) AS ID ,
	 is_required = Data.value('is_required[1]', 'bit'),
	 [name]=Data.value('name[1]', 'nvarchar(500)'),
	 sort_order=Data.value('sort_order[1]', 'smallint'),
	  childrens=Data.query('childrens[1]')  
	  FROM
    @Data.nodes('data/varient') as Data([Data]) 
	--select * from #Tempvarient
		
		declare @count int
		declare @index int
		 select @count=count(id) from #Tempvarient
		 DECLARE @is_required bit
		 DECLARE @name nvarchar(500)
		 DECLARE @Sort_Order int
		 DECLARE @Childrens xml
		 DECLARE @VarientID int
		 set @index=1
		 While(@index<=@count)
			BEGIN
				SELECT  @is_required=is_required,@name=name,@Sort_Order=sort_order,@Childrens=childrens from #Tempvarient where id = @index 
				
			--print '@Childrens :'+convert(varchar(max),@Childrens)
			 
				INSERT INTO PHP_Ecommerce_Product_Varients (is_required,name,product_id,sort_order) VALUES(@is_required,@name,@productID,@Sort_Order)

				SET @VarientID=SCOPE_IDENTITY()
				
					/** After Getting Varient Id Create Children **/
					create table #TempvarientOptions
						(
						id int,
						varient_id int,
						title nvarchar(255),
						price decimal(18,2),
						pricetype smallint,
						sort_order smallint
						)
					insert into #TempvarientOptions

					select	ROW_NUMBER ( ) OVER(ORDER BY Data.value('title[1]', 'nvarchar(500)') ASC) AS ID ,
					 varient_id = @VarientID,
					 title=Data.value('title[1]', 'nvarchar(255)'),
					 price=Data.value('price[1]', 'decimal(18,2)'),
					 pricetype=Data.value('pricetype[1]', 'smallint'),
					 sort_order=Data.value('sort_order[1]', 'smallint')
					  FROM
					  @Childrens.nodes('childrens/children') as ChildData([Data]) 
					  --select * from #TempvarientOptions
						declare @childcount int
						declare @childindex int
						set @childindex=1
						select  @childcount=count(id) from #TempvarientOptions
						 DECLARE @title nvarchar(255)
						 DECLARE @price decimal(18,2)
						 DECLARE @pricetype smallint
						 DECLARE @optionsort_order smallint
						 While(@childindex<=@childcount)
						  BEGIN
							SELECT  @title=title,@price=price,@pricetype=pricetype,@optionsort_order=sort_order from #TempvarientOptions where id = @childindex 
						    if(LEN(LTRIM(RTRIM(@title)))=0)
							  BEGIN
							     RAISERROR ('Sorry varient title is required',0,0)
								 RETURN
							  END

							  if(LEN(LTRIM(RTRIM(@price)))=0)
							  BEGIN
							     RAISERROR ('Sorry varient price is required',0,0)
								 RETURN
							  END

							  if(LEN(LTRIM(RTRIM(@pricetype)))=0)
							  BEGIN
							     RAISERROR ('Sorry pricetype price is required',0,0)
								 RETURN
							  END

							  DECLARE @PRICETYPEVALIDID int

							  SELECT @PRICETYPEVALIDID FROM PHP_Ecommerce_Price_Types where id=@pricetype

							   if(LEN(LTRIM(RTRIM(@optionsort_order)))=0)
							  BEGIN
							    SET @optionsort_order=1 
							  END



							INSERT INTO PHP_Ecommerce_Product_Varient_Options_Map (varient_id,title,price,pricetype,sort_order) VALUES (@VarientID,@title,@price,@pricetype,@Sort_Order)

							SET @childindex=@childindex+1;
						  END
			drop table #TempvarientOptions

				SET @index=@index+1
            END


	
IF @TransCount = 0 
            COMMIT TRANSACTION
    END TRY
    BEGIN CATCH
        IF @TransCount = 0 
            ROLLBACK TRANSACTION
        ELSE 
            BEGIN
                IF XACT_STATE() <> -1 
                    ROLLBACK TRANSACTION PrevTran
            END
        DECLARE @ErrMsg AS VARCHAR(8000) ;
        DECLARE @ErrorSeverity INT ,
            @ErrorState INT
        SELECT  @ErrMsg = ERROR_MESSAGE() --+ ' Line No. - ' + ERROR_LINE()
        SELECT  @ErrorSeverity = ERROR_SEVERITY() ;
        SELECT  @ErrorState = ERROR_STATE() 
        RAISERROR(@ErrMsg, @ErrorSeverity, @ErrorState)

    END CATCH
END

