USE [MTCEddyStoneV2_Stage]
GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PHP_Ecommerce_Products', @level2type=N'COLUMN',@level2name=N'industry_id'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PHP_Ecommerce_Products', @level2type=N'COLUMN',@level2name=N'description'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PHP_Ecommerce_Products', @level2type=N'COLUMN',@level2name=N'buid'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PHP_Ecommerce_Products', @level2type=N'COLUMN',@level2name=N'slug'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PHP_Ecommerce_Products', @level2type=N'COLUMN',@level2name=N'Name'

GO
/****** Object:  StoredProcedure [dbo].[PHP_Ecommerce_MapProductPrice]    Script Date: 7/19/2017 3:49:16 AM ******/
DROP PROCEDURE [dbo].[PHP_Ecommerce_MapProductPrice]
GO
/****** Object:  StoredProcedure [dbo].[PHP_Ecommerce_MapProductCategory]    Script Date: 7/19/2017 3:49:16 AM ******/
DROP PROCEDURE [dbo].[PHP_Ecommerce_MapProductCategory]
GO
/****** Object:  StoredProcedure [dbo].[PHP_Ecommerce_GetProductVarients]    Script Date: 7/19/2017 3:49:16 AM ******/
DROP PROCEDURE [dbo].[PHP_Ecommerce_GetProductVarients]
GO
/****** Object:  StoredProcedure [dbo].[PHP_Ecommerce_Getproducts]    Script Date: 7/19/2017 3:49:16 AM ******/
DROP PROCEDURE [dbo].[PHP_Ecommerce_Getproducts]
GO
/****** Object:  StoredProcedure [dbo].[PHP_Ecommerce_GetProductImages]    Script Date: 7/19/2017 3:49:16 AM ******/
DROP PROCEDURE [dbo].[PHP_Ecommerce_GetProductImages]
GO
/****** Object:  StoredProcedure [dbo].[PHP_Ecommerce_GetProductDetails]    Script Date: 7/19/2017 3:49:16 AM ******/
DROP PROCEDURE [dbo].[PHP_Ecommerce_GetProductDetails]
GO
/****** Object:  StoredProcedure [dbo].[PHP_Ecommerce_AddVarient]    Script Date: 7/19/2017 3:49:16 AM ******/
DROP PROCEDURE [dbo].[PHP_Ecommerce_AddVarient]
GO
/****** Object:  StoredProcedure [dbo].[PHP_Ecommerce_AddProductStock]    Script Date: 7/19/2017 3:49:16 AM ******/
DROP PROCEDURE [dbo].[PHP_Ecommerce_AddProductStock]
GO
/****** Object:  StoredProcedure [dbo].[PHP_Ecommerce_AddProducts]    Script Date: 7/19/2017 3:49:16 AM ******/
DROP PROCEDURE [dbo].[PHP_Ecommerce_AddProducts]
GO
/****** Object:  StoredProcedure [dbo].[PHP_Ecommerce_AddProductImages]    Script Date: 7/19/2017 3:49:16 AM ******/
DROP PROCEDURE [dbo].[PHP_Ecommerce_AddProductImages]
GO
ALTER TABLE [dbo].[PHP_Ecommerce_Product_Varients] DROP CONSTRAINT [FK_PHP_Ecommerce_Product_Varients_PHP_Ecommerce_Product_Varients_PRODMAP]
GO
ALTER TABLE [dbo].[PHP_Ecommerce_Product_Varient_Options_Map] DROP CONSTRAINT [FK_PHP_Ecommerce_Product_Varient_Options_Map_PHP_Ecommerce_Product_Varients]
GO
ALTER TABLE [dbo].[PHP_Ecommerce_Product_Price_Map] DROP CONSTRAINT [FK_PHP_Ecommerce_Product_Price_Map_PHP_Ecommerce_Products]
GO
ALTER TABLE [dbo].[PHP_Ecommerce_Product_Price_Map] DROP CONSTRAINT [FK_PHP_Ecommerce_Product_Price_Map_PHP_Ecommerce_Price_Types]
GO
ALTER TABLE [dbo].[PHP_Ecommerce_Product_images] DROP CONSTRAINT [FK_PHP_Ecommerce_Product_images_PHP_Ecommerce_Products]
GO
ALTER TABLE [dbo].[PHP_Ecommerce_Product_Category_Map] DROP CONSTRAINT [FK_PHP_Ecommerce_Product_Category_Map_PHP_Ecommerce_Products1]
GO
ALTER TABLE [dbo].[PHP_Ecommerce_Product_Category_Map] DROP CONSTRAINT [FK_PHP_Ecommerce_Product_Category_Map_PHP_Ecommerce_categories]
GO
ALTER TABLE [dbo].[PHP_Ecommerce_Suppliers] DROP CONSTRAINT [DF_PHP_Ecommerce_Suppliers_markup]
GO
ALTER TABLE [dbo].[PHP_Ecommerce_Product_images] DROP CONSTRAINT [DF_PHP_Ecommerce_Product_images_default_image]
GO
/****** Object:  Index [PK_PHP_Ecommerce_Product_images_1]    Script Date: 7/19/2017 3:49:16 AM ******/
ALTER TABLE [dbo].[PHP_Ecommerce_Product_images] DROP CONSTRAINT [PK_PHP_Ecommerce_Product_images_1]
GO
/****** Object:  Table [dbo].[PHP_Ecommerce_Suppliers]    Script Date: 7/19/2017 3:49:16 AM ******/
DROP TABLE [dbo].[PHP_Ecommerce_Suppliers]
GO
/****** Object:  Table [dbo].[PHP_Ecommerce_Products]    Script Date: 7/19/2017 3:49:16 AM ******/
DROP TABLE [dbo].[PHP_Ecommerce_Products]
GO
/****** Object:  Table [dbo].[PHP_Ecommerce_Product_Varients]    Script Date: 7/19/2017 3:49:16 AM ******/
DROP TABLE [dbo].[PHP_Ecommerce_Product_Varients]
GO
/****** Object:  Table [dbo].[PHP_Ecommerce_Product_Varient_Options_Map]    Script Date: 7/19/2017 3:49:16 AM ******/
DROP TABLE [dbo].[PHP_Ecommerce_Product_Varient_Options_Map]
GO
/****** Object:  Table [dbo].[PHP_Ecommerce_Product_Stock]    Script Date: 7/19/2017 3:49:16 AM ******/
DROP TABLE [dbo].[PHP_Ecommerce_Product_Stock]
GO
/****** Object:  Table [dbo].[PHP_Ecommerce_Product_Price_Map]    Script Date: 7/19/2017 3:49:16 AM ******/
DROP TABLE [dbo].[PHP_Ecommerce_Product_Price_Map]
GO
/****** Object:  Table [dbo].[PHP_Ecommerce_Product_images]    Script Date: 7/19/2017 3:49:16 AM ******/
DROP TABLE [dbo].[PHP_Ecommerce_Product_images]
GO
/****** Object:  Table [dbo].[PHP_Ecommerce_Product_Category_Map]    Script Date: 7/19/2017 3:49:16 AM ******/
DROP TABLE [dbo].[PHP_Ecommerce_Product_Category_Map]
GO
/****** Object:  Table [dbo].[PHP_Ecommerce_Price_Types]    Script Date: 7/19/2017 3:49:16 AM ******/
DROP TABLE [dbo].[PHP_Ecommerce_Price_Types]
GO
/****** Object:  Table [dbo].[PHP_Ecommerce_industries]    Script Date: 7/19/2017 3:49:16 AM ******/
DROP TABLE [dbo].[PHP_Ecommerce_industries]
GO
/****** Object:  Table [dbo].[PHP_Ecommerce_categories]    Script Date: 7/19/2017 3:49:16 AM ******/
DROP TABLE [dbo].[PHP_Ecommerce_categories]
GO
/****** Object:  Table [dbo].[PHP_Ecommerce_BU_INDUSTRY_map]    Script Date: 7/19/2017 3:49:16 AM ******/
DROP TABLE [dbo].[PHP_Ecommerce_BU_INDUSTRY_map]
GO
/****** Object:  Table [dbo].[PHP_Ecommerce_Brands]    Script Date: 7/19/2017 3:49:16 AM ******/
DROP TABLE [dbo].[PHP_Ecommerce_Brands]
GO
/****** Object:  Table [dbo].[oauth_users]    Script Date: 7/19/2017 3:49:16 AM ******/
DROP TABLE [dbo].[oauth_users]
GO
/****** Object:  Table [dbo].[oauth_scopes]    Script Date: 7/19/2017 3:49:16 AM ******/
DROP TABLE [dbo].[oauth_scopes]
GO
/****** Object:  Table [dbo].[oauth_refresh_tokens]    Script Date: 7/19/2017 3:49:16 AM ******/
DROP TABLE [dbo].[oauth_refresh_tokens]
GO
/****** Object:  Table [dbo].[oauth_jwt]    Script Date: 7/19/2017 3:49:16 AM ******/
DROP TABLE [dbo].[oauth_jwt]
GO
/****** Object:  Table [dbo].[oauth_clients]    Script Date: 7/19/2017 3:49:16 AM ******/
DROP TABLE [dbo].[oauth_clients]
GO
/****** Object:  Table [dbo].[oauth_authorization_codes]    Script Date: 7/19/2017 3:49:16 AM ******/
DROP TABLE [dbo].[oauth_authorization_codes]
GO
/****** Object:  Table [dbo].[oauth_access_tokens]    Script Date: 7/19/2017 3:49:16 AM ******/
DROP TABLE [dbo].[oauth_access_tokens]
GO
/****** Object:  UserDefinedFunction [dbo].[slugify]    Script Date: 7/19/2017 3:49:16 AM ******/
DROP FUNCTION [dbo].[slugify]
GO
/****** Object:  UserDefinedFunction [dbo].[slugify]    Script Date: 7/19/2017 3:49:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[slugify](@string varchar(4000))   
    RETURNS varchar(4000) AS BEGIN   
    declare @out varchar(4000)  
  
    --convert to ASCII  
    set @out = lower(@string COLLATE SQL_Latin1_General_CP1251_CS_AS)  
      
    declare @pi int   
    --I'm sorry T-SQL have no regex. Thanks for patindex, MS .. :-)  
    set @pi = patindex('%[^a-z0-9 -]%',@out) 
    while @pi>0 begin 
        set @out = replace(@out, substring(@out,@pi,1), '') 
        --set @out = left(@out,@pi-1) + substring(@out,@pi+1,8000) 
        set @pi = patindex('%[^a-z0-9 -]%',@out) 
    end 
 
    set @out = ltrim(rtrim(@out)) 
 
    -- replace space to hyphen   
    set @out = replace(@out, ' ', '-') 
 
    -- remove double hyphen 
    while CHARINDEX('--', @out) > 0 set @out = replace(@out, '--', '-')  
      
    return (@out)  
END  
  




GO
/****** Object:  Table [dbo].[oauth_access_tokens]    Script Date: 7/19/2017 3:49:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[oauth_access_tokens](
	[access_token] [varchar](40) NOT NULL,
	[client_id] [varchar](80) NOT NULL,
	[user_id] [int] NULL,
	[expires] [datetime] NULL,
	[scope] [varchar](2000) NULL,
 CONSTRAINT [access_token_pk] PRIMARY KEY CLUSTERED 
(
	[access_token] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[oauth_authorization_codes]    Script Date: 7/19/2017 3:49:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[oauth_authorization_codes](
	[authorization_code] [varchar](40) NOT NULL,
	[client_id] [varchar](80) NOT NULL,
	[user_id] [varchar](255) NULL,
	[redirect_uri] [varchar](2000) NULL,
	[expires] [timestamp] NOT NULL,
	[scope] [varchar](2000) NULL,
 CONSTRAINT [auth_code_pk] PRIMARY KEY CLUSTERED 
(
	[authorization_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[oauth_clients]    Script Date: 7/19/2017 3:49:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[oauth_clients](
	[client_id] [varchar](80) NOT NULL,
	[client_secret] [varchar](80) NULL,
	[redirect_uri] [varchar](2000) NOT NULL,
	[grant_types] [varchar](80) NULL,
	[scope] [varchar](100) NULL,
	[user_id] [int] NULL,
 CONSTRAINT [clients_client_id_pk] PRIMARY KEY CLUSTERED 
(
	[client_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[oauth_jwt]    Script Date: 7/19/2017 3:49:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[oauth_jwt](
	[client_id] [varchar](80) NOT NULL,
	[subject] [varchar](80) NULL,
	[public_key] [varchar](2000) NULL,
 CONSTRAINT [jwt_client_id_pk] PRIMARY KEY CLUSTERED 
(
	[client_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[oauth_refresh_tokens]    Script Date: 7/19/2017 3:49:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[oauth_refresh_tokens](
	[refresh_token] [varchar](40) NOT NULL,
	[client_id] [varchar](80) NOT NULL,
	[user_id] [varchar](255) NULL,
	[expires] [timestamp] NOT NULL,
	[scope] [varchar](2000) NULL,
 CONSTRAINT [refresh_token_pk] PRIMARY KEY CLUSTERED 
(
	[refresh_token] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[oauth_scopes]    Script Date: 7/19/2017 3:49:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[oauth_scopes](
	[scope] [text] NULL,
	[is_default] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[oauth_users]    Script Date: 7/19/2017 3:49:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[oauth_users](
	[username] [varchar](255) NOT NULL,
	[password] [varchar](2000) NULL,
	[first_name] [varchar](255) NULL,
	[last_name] [varchar](255) NULL,
 CONSTRAINT [username_pk] PRIMARY KEY CLUSTERED 
(
	[username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PHP_Ecommerce_Brands]    Script Date: 7/19/2017 3:49:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PHP_Ecommerce_Brands](
	[id] [smallint] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[buid] [int] NOT NULL,
 CONSTRAINT [PK_PHP_Ecommerce_Brands] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PHP_Ecommerce_BU_INDUSTRY_map]    Script Date: 7/19/2017 3:49:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PHP_Ecommerce_BU_INDUSTRY_map](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[buid] [int] NOT NULL,
	[industry_id] [smallint] NOT NULL,
 CONSTRAINT [PK_PHP_Ecommerce_BU_INDUSTRY_map] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PHP_Ecommerce_categories]    Script Date: 7/19/2017 3:49:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PHP_Ecommerce_categories](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[buid] [int] NOT NULL,
 CONSTRAINT [PK_PHP_Ecommerce_categories] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PHP_Ecommerce_industries]    Script Date: 7/19/2017 3:49:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PHP_Ecommerce_industries](
	[id] [smallint] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[folderpath] [varchar](50) NULL,
 CONSTRAINT [PK__PHP_Ecom__3213E83F9456EAB8] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PHP_Ecommerce_Price_Types]    Script Date: 7/19/2017 3:49:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PHP_Ecommerce_Price_Types](
	[id] [smallint] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_PHP_Ecommerce_Price_Types] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PHP_Ecommerce_Product_Category_Map]    Script Date: 7/19/2017 3:49:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PHP_Ecommerce_Product_Category_Map](
	[category_id] [int] NOT NULL,
	[product_id] [int] NOT NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_PHP_Ecommerce_Product_Category_Map] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PHP_Ecommerce_Product_images]    Script Date: 7/19/2017 3:49:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PHP_Ecommerce_Product_images](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](max) NULL,
	[imagePath] [nvarchar](max) NOT NULL,
	[default_image] [bit] NOT NULL,
	[product_id] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PHP_Ecommerce_Product_Price_Map]    Script Date: 7/19/2017 3:49:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PHP_Ecommerce_Product_Price_Map](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[product_id] [int] NOT NULL,
	[price_type_id] [smallint] NOT NULL,
	[price] [decimal](18, 2) NOT NULL CONSTRAINT [DF_PHP_Ecomerce_Product_Price_Map_price]  DEFAULT ((0.00)),
	[units] [nvarchar](250) NULL,
 CONSTRAINT [PK_PHP_Ecomerce_Product_Price_Map] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PHP_Ecommerce_Product_Stock]    Script Date: 7/19/2017 3:49:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PHP_Ecommerce_Product_Stock](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[product_id] [int] NOT NULL,
	[volume] [float] NOT NULL,
 CONSTRAINT [PK_PHP_Ecommerce_Product_Stock] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PHP_Ecommerce_Product_Varient_Options_Map]    Script Date: 7/19/2017 3:49:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PHP_Ecommerce_Product_Varient_Options_Map](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[varient_id] [int] NOT NULL,
	[title] [nvarchar](255) NOT NULL,
	[price] [decimal](18, 2) NOT NULL CONSTRAINT [DF_PHP_Ecommerce_Product_Varient_Options_Map_price]  DEFAULT ((0.00)),
	[pricetype] [smallint] NOT NULL CONSTRAINT [DF_PHP_Ecommerce_Product_Varient_Options_Map_pricetype]  DEFAULT ('+'),
	[sort_order] [smallint] NOT NULL CONSTRAINT [DF_PHP_Ecommerce_Product_Varient_Options_Map_sort_order]  DEFAULT ((1)),
 CONSTRAINT [PK_PHP_Ecommerce_Product_Varient_Options_Map] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PHP_Ecommerce_Product_Varients]    Script Date: 7/19/2017 3:49:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PHP_Ecommerce_Product_Varients](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[is_required] [bit] NOT NULL CONSTRAINT [DF_PHP_Ecommerce_Product_Varients_is_required]  DEFAULT ((1)),
	[name] [varchar](50) NOT NULL,
	[product_id] [int] NOT NULL,
	[sort_order] [smallint] NOT NULL CONSTRAINT [DF_PHP_Ecommerce_Product_Varients_sort_order]  DEFAULT ((1)),
 CONSTRAINT [PK_Table_PKey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PHP_Ecommerce_Products]    Script Date: 7/19/2017 3:49:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PHP_Ecommerce_Products](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](500) NOT NULL,
	[slug] [varchar](255) NOT NULL,
	[buid] [int] NOT NULL,
	[description] [nvarchar](max) NOT NULL,
	[industry_id] [smallint] NOT NULL,
	[brand_id] [smallint] NULL,
	[supplier_id] [smallint] NULL,
	[supplier_code] [int] NULL,
	[sales_account_code] [int] NULL,
	[purchase_account_code] [int] NULL,
	[sku] [varchar](50) NOT NULL,
	[is_disbaled] [bit] NOT NULL CONSTRAINT [DF_PHP_Ecommerce_Products_is_disbaled]  DEFAULT ((0)),
 CONSTRAINT [PK__PHP_Ecom__3213E83FEB271F59] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PHP_Ecommerce_Suppliers]    Script Date: 7/19/2017 3:49:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PHP_Ecommerce_Suppliers](
	[id] [smallint] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[markup] [float] NOT NULL,
	[buid] [int] NOT NULL,
 CONSTRAINT [PK_PHP_Ecommerce_Suppliers] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[oauth_access_tokens] ([access_token], [client_id], [user_id], [expires], [scope]) VALUES (N'03af74116dd7eef72b3560507b01fb6c8e6111bd', N'testclient', 500, CAST(N'2017-07-12 05:02:39.000' AS DateTime), NULL)
INSERT [dbo].[oauth_access_tokens] ([access_token], [client_id], [user_id], [expires], [scope]) VALUES (N'06970839caa1ccde13de5455b038154df90b177c', N'testclient', 500, CAST(N'2017-07-12 03:53:21.000' AS DateTime), NULL)
INSERT [dbo].[oauth_access_tokens] ([access_token], [client_id], [user_id], [expires], [scope]) VALUES (N'09c13489e720edff2cb3d9414a302a52d0b9628c', N'testclient', 500, CAST(N'2017-07-18 12:25:31.000' AS DateTime), NULL)
INSERT [dbo].[oauth_access_tokens] ([access_token], [client_id], [user_id], [expires], [scope]) VALUES (N'141d0a0d858e7589e792a887d2bdf13f05e32a31', N'testclient', 500, CAST(N'2017-07-18 10:59:19.000' AS DateTime), NULL)
INSERT [dbo].[oauth_access_tokens] ([access_token], [client_id], [user_id], [expires], [scope]) VALUES (N'263305cc9e6c5d00e4d3f5a07a91adf4c36c7550', N'testclient', 500, CAST(N'2017-07-17 16:56:56.000' AS DateTime), NULL)
INSERT [dbo].[oauth_access_tokens] ([access_token], [client_id], [user_id], [expires], [scope]) VALUES (N'2ecf5db0607cda9e01cec6f3643abaaa0a99d312', N'testclient', 500, CAST(N'2017-07-12 05:09:08.000' AS DateTime), NULL)
INSERT [dbo].[oauth_access_tokens] ([access_token], [client_id], [user_id], [expires], [scope]) VALUES (N'5296b2ff12e58db39b350b30d9cd545d95d61c2e', N'testclient', 500, CAST(N'2017-07-18 05:27:08.000' AS DateTime), NULL)
INSERT [dbo].[oauth_access_tokens] ([access_token], [client_id], [user_id], [expires], [scope]) VALUES (N'532e58f7145badd8de14ec0ea5abbc61a4d75486', N'testclient', 500, CAST(N'2017-07-18 07:53:08.000' AS DateTime), NULL)
INSERT [dbo].[oauth_access_tokens] ([access_token], [client_id], [user_id], [expires], [scope]) VALUES (N'57b73ff5ceaaf89c6b34b0e76d68f936c989a769', N'testclient', 500, CAST(N'2017-07-17 17:57:33.000' AS DateTime), NULL)
INSERT [dbo].[oauth_access_tokens] ([access_token], [client_id], [user_id], [expires], [scope]) VALUES (N'5a2858f8b9fe65a875766ed8b167941fdba7f666', N'testclient', 500, CAST(N'2017-07-12 09:48:40.000' AS DateTime), NULL)
INSERT [dbo].[oauth_access_tokens] ([access_token], [client_id], [user_id], [expires], [scope]) VALUES (N'6295ddc32241f08b1042003decc10723cb09988e', N'testclient', 500, CAST(N'2017-07-18 06:47:03.000' AS DateTime), NULL)
INSERT [dbo].[oauth_access_tokens] ([access_token], [client_id], [user_id], [expires], [scope]) VALUES (N'67c4006cf818d58d79af842162fabed141c869eb', N'testclient', 500, CAST(N'2017-07-12 08:28:38.000' AS DateTime), NULL)
INSERT [dbo].[oauth_access_tokens] ([access_token], [client_id], [user_id], [expires], [scope]) VALUES (N'6e147db4ab36504ede25a761b394fa8357eb98d1', N'testclient', 500, CAST(N'2017-07-18 19:07:59.000' AS DateTime), NULL)
INSERT [dbo].[oauth_access_tokens] ([access_token], [client_id], [user_id], [expires], [scope]) VALUES (N'74c552e3c52ad8dc65eac942dcb280c77ab403ee', N'testclient', 500, CAST(N'2017-07-18 17:02:00.000' AS DateTime), NULL)
INSERT [dbo].[oauth_access_tokens] ([access_token], [client_id], [user_id], [expires], [scope]) VALUES (N'805c5a8e37dd17c9b212967fc0f56c71c8380df5', N'testclient', 500, CAST(N'2017-07-12 05:37:37.000' AS DateTime), NULL)
INSERT [dbo].[oauth_access_tokens] ([access_token], [client_id], [user_id], [expires], [scope]) VALUES (N'81fdd1ac7275caf46a6d4cf691999d58f9349e13', N'testclient', 500, CAST(N'2017-07-18 15:15:20.000' AS DateTime), NULL)
INSERT [dbo].[oauth_access_tokens] ([access_token], [client_id], [user_id], [expires], [scope]) VALUES (N'885c8ef064ad5a129f4f9ca63bd810efff64145a', N'testclient', 500, CAST(N'2017-07-18 16:57:50.000' AS DateTime), NULL)
INSERT [dbo].[oauth_access_tokens] ([access_token], [client_id], [user_id], [expires], [scope]) VALUES (N'8917ff082ecf12c9cda2e833d45d09e9ffc8ed0f', N'testclient', 500, CAST(N'2017-07-12 07:27:34.000' AS DateTime), NULL)
INSERT [dbo].[oauth_access_tokens] ([access_token], [client_id], [user_id], [expires], [scope]) VALUES (N'98140bb61648ea656f2656b8849d726dec1630c5', N'testclient', 500, CAST(N'2017-07-18 18:07:28.000' AS DateTime), NULL)
INSERT [dbo].[oauth_access_tokens] ([access_token], [client_id], [user_id], [expires], [scope]) VALUES (N'a9373dbf6ac64f2ebc894ced8ed69a589fc0c11d', N'testclient', 500, CAST(N'2017-07-12 06:46:28.000' AS DateTime), NULL)
INSERT [dbo].[oauth_access_tokens] ([access_token], [client_id], [user_id], [expires], [scope]) VALUES (N'add7f1b2edc944d1dfaf7817c96905587c325b1d', N'testclient', 500, CAST(N'2017-07-18 16:32:22.000' AS DateTime), NULL)
INSERT [dbo].[oauth_access_tokens] ([access_token], [client_id], [user_id], [expires], [scope]) VALUES (N'af866dd644b9c43bd46e6b98345aec80212920b6', N'testclient', 500, CAST(N'2017-07-13 10:50:01.000' AS DateTime), NULL)
INSERT [dbo].[oauth_access_tokens] ([access_token], [client_id], [user_id], [expires], [scope]) VALUES (N'b61eb3e7b43676e8c19a97594275fd95ab422398', N'testclient', 500, CAST(N'2017-07-10 12:31:48.000' AS DateTime), NULL)
INSERT [dbo].[oauth_access_tokens] ([access_token], [client_id], [user_id], [expires], [scope]) VALUES (N'b62f43f554911af16702948c6638dfaaf8044609', N'testclient', 500, CAST(N'2017-07-13 05:16:22.000' AS DateTime), NULL)
INSERT [dbo].[oauth_access_tokens] ([access_token], [client_id], [user_id], [expires], [scope]) VALUES (N'cfe312567c37caf0e5cabd589421dcbd504dd2bb', N'testclient', 500, CAST(N'2017-07-13 12:07:11.000' AS DateTime), NULL)
INSERT [dbo].[oauth_access_tokens] ([access_token], [client_id], [user_id], [expires], [scope]) VALUES (N'e19660439c76118b5e0ce135f5d53d716f587c82', N'testclient', 500, CAST(N'2017-07-18 09:58:49.000' AS DateTime), NULL)
INSERT [dbo].[oauth_access_tokens] ([access_token], [client_id], [user_id], [expires], [scope]) VALUES (N'e66215831b7bfe499a0bc96c6af4d574b8e7e4b4', N'testclient', 500, CAST(N'2017-07-12 05:03:16.000' AS DateTime), NULL)
INSERT [dbo].[oauth_access_tokens] ([access_token], [client_id], [user_id], [expires], [scope]) VALUES (N'e74679bfe60487a301679b72004bfcadeca59ade', N'testclient', 500, CAST(N'2017-07-12 05:03:15.000' AS DateTime), NULL)
INSERT [dbo].[oauth_access_tokens] ([access_token], [client_id], [user_id], [expires], [scope]) VALUES (N'e8295fe553385bbc6b75c71993bc5be8d642814a', N'testclient', 500, CAST(N'2017-07-18 08:58:30.000' AS DateTime), NULL)
INSERT [dbo].[oauth_clients] ([client_id], [client_secret], [redirect_uri], [grant_types], [scope], [user_id]) VALUES (N'testclient', N'testpass', N'http://fake/', NULL, NULL, 500)
SET IDENTITY_INSERT [dbo].[PHP_Ecommerce_Brands] ON 

INSERT [dbo].[PHP_Ecommerce_Brands] ([id], [name], [buid]) VALUES (8, N'test', 500)
INSERT [dbo].[PHP_Ecommerce_Brands] ([id], [name], [buid]) VALUES (9, N'ipankard', 500)
INSERT [dbo].[PHP_Ecommerce_Brands] ([id], [name], [buid]) VALUES (10, N'ipankardd', 500)
INSERT [dbo].[PHP_Ecommerce_Brands] ([id], [name], [buid]) VALUES (11, N'Dibyendu', 500)
SET IDENTITY_INSERT [dbo].[PHP_Ecommerce_Brands] OFF
SET IDENTITY_INSERT [dbo].[PHP_Ecommerce_BU_INDUSTRY_map] ON 

INSERT [dbo].[PHP_Ecommerce_BU_INDUSTRY_map] ([id], [buid], [industry_id]) VALUES (1, 500, 5)
INSERT [dbo].[PHP_Ecommerce_BU_INDUSTRY_map] ([id], [buid], [industry_id]) VALUES (3, 500, 6)
INSERT [dbo].[PHP_Ecommerce_BU_INDUSTRY_map] ([id], [buid], [industry_id]) VALUES (5, 500, 7)
SET IDENTITY_INSERT [dbo].[PHP_Ecommerce_BU_INDUSTRY_map] OFF
SET IDENTITY_INSERT [dbo].[PHP_Ecommerce_categories] ON 

INSERT [dbo].[PHP_Ecommerce_categories] ([id], [name], [buid]) VALUES (1, N'Alchohol', 500)
SET IDENTITY_INSERT [dbo].[PHP_Ecommerce_categories] OFF
SET IDENTITY_INSERT [dbo].[PHP_Ecommerce_industries] ON 

INSERT [dbo].[PHP_Ecommerce_industries] ([id], [name], [folderpath]) VALUES (5, N'Artist', N'artist')
INSERT [dbo].[PHP_Ecommerce_industries] ([id], [name], [folderpath]) VALUES (6, N'Resturant', N'resturant')
INSERT [dbo].[PHP_Ecommerce_industries] ([id], [name], [folderpath]) VALUES (7, N'Store', N'store')
SET IDENTITY_INSERT [dbo].[PHP_Ecommerce_industries] OFF
SET IDENTITY_INSERT [dbo].[PHP_Ecommerce_Price_Types] ON 

INSERT [dbo].[PHP_Ecommerce_Price_Types] ([id], [name]) VALUES (1, N'Liter')
SET IDENTITY_INSERT [dbo].[PHP_Ecommerce_Price_Types] OFF
SET IDENTITY_INSERT [dbo].[PHP_Ecommerce_Product_Category_Map] ON 

INSERT [dbo].[PHP_Ecommerce_Product_Category_Map] ([category_id], [product_id], [id]) VALUES (1, 230, 207)
INSERT [dbo].[PHP_Ecommerce_Product_Category_Map] ([category_id], [product_id], [id]) VALUES (1, 241, 208)
INSERT [dbo].[PHP_Ecommerce_Product_Category_Map] ([category_id], [product_id], [id]) VALUES (1, 243, 209)
INSERT [dbo].[PHP_Ecommerce_Product_Category_Map] ([category_id], [product_id], [id]) VALUES (1, 244, 210)
INSERT [dbo].[PHP_Ecommerce_Product_Category_Map] ([category_id], [product_id], [id]) VALUES (1, 245, 211)
INSERT [dbo].[PHP_Ecommerce_Product_Category_Map] ([category_id], [product_id], [id]) VALUES (1, 246, 212)
SET IDENTITY_INSERT [dbo].[PHP_Ecommerce_Product_Category_Map] OFF
SET IDENTITY_INSERT [dbo].[PHP_Ecommerce_Product_Price_Map] ON 

INSERT [dbo].[PHP_Ecommerce_Product_Price_Map] ([id], [product_id], [price_type_id], [price], [units]) VALUES (211, 230, 1, CAST(500.00 AS Decimal(18, 2)), NULL)
INSERT [dbo].[PHP_Ecommerce_Product_Price_Map] ([id], [product_id], [price_type_id], [price], [units]) VALUES (212, 241, 1, CAST(500.00 AS Decimal(18, 2)), N'Liter')
INSERT [dbo].[PHP_Ecommerce_Product_Price_Map] ([id], [product_id], [price_type_id], [price], [units]) VALUES (213, 243, 1, CAST(500.00 AS Decimal(18, 2)), N'Liter')
INSERT [dbo].[PHP_Ecommerce_Product_Price_Map] ([id], [product_id], [price_type_id], [price], [units]) VALUES (214, 244, 1, CAST(500.00 AS Decimal(18, 2)), NULL)
INSERT [dbo].[PHP_Ecommerce_Product_Price_Map] ([id], [product_id], [price_type_id], [price], [units]) VALUES (215, 245, 1, CAST(500.00 AS Decimal(18, 2)), NULL)
INSERT [dbo].[PHP_Ecommerce_Product_Price_Map] ([id], [product_id], [price_type_id], [price], [units]) VALUES (216, 246, 1, CAST(500.00 AS Decimal(18, 2)), NULL)
SET IDENTITY_INSERT [dbo].[PHP_Ecommerce_Product_Price_Map] OFF
SET IDENTITY_INSERT [dbo].[PHP_Ecommerce_Product_Stock] ON 

INSERT [dbo].[PHP_Ecommerce_Product_Stock] ([id], [product_id], [volume]) VALUES (153, 230, 20)
INSERT [dbo].[PHP_Ecommerce_Product_Stock] ([id], [product_id], [volume]) VALUES (154, 241, 20)
INSERT [dbo].[PHP_Ecommerce_Product_Stock] ([id], [product_id], [volume]) VALUES (155, 243, 20)
INSERT [dbo].[PHP_Ecommerce_Product_Stock] ([id], [product_id], [volume]) VALUES (156, 244, 20)
INSERT [dbo].[PHP_Ecommerce_Product_Stock] ([id], [product_id], [volume]) VALUES (157, 245, 20)
INSERT [dbo].[PHP_Ecommerce_Product_Stock] ([id], [product_id], [volume]) VALUES (158, 246, 20)
SET IDENTITY_INSERT [dbo].[PHP_Ecommerce_Product_Stock] OFF
SET IDENTITY_INSERT [dbo].[PHP_Ecommerce_Product_Varient_Options_Map] ON 

INSERT [dbo].[PHP_Ecommerce_Product_Varient_Options_Map] ([id], [varient_id], [title], [price], [pricetype], [sort_order]) VALUES (127, 128, N'Test2', CAST(200.00 AS Decimal(18, 2)), 2, 1)
INSERT [dbo].[PHP_Ecommerce_Product_Varient_Options_Map] ([id], [varient_id], [title], [price], [pricetype], [sort_order]) VALUES (128, 129, N'Test2', CAST(200.00 AS Decimal(18, 2)), 2, 1)
INSERT [dbo].[PHP_Ecommerce_Product_Varient_Options_Map] ([id], [varient_id], [title], [price], [pricetype], [sort_order]) VALUES (129, 130, N'Test2', CAST(200.00 AS Decimal(18, 2)), 2, 1)
INSERT [dbo].[PHP_Ecommerce_Product_Varient_Options_Map] ([id], [varient_id], [title], [price], [pricetype], [sort_order]) VALUES (130, 131, N'Test2', CAST(200.00 AS Decimal(18, 2)), 2, 1)
INSERT [dbo].[PHP_Ecommerce_Product_Varient_Options_Map] ([id], [varient_id], [title], [price], [pricetype], [sort_order]) VALUES (131, 132, N'Test2', CAST(200.00 AS Decimal(18, 2)), 2, 1)
INSERT [dbo].[PHP_Ecommerce_Product_Varient_Options_Map] ([id], [varient_id], [title], [price], [pricetype], [sort_order]) VALUES (132, 133, N'Test2', CAST(200.00 AS Decimal(18, 2)), 2, 1)
SET IDENTITY_INSERT [dbo].[PHP_Ecommerce_Product_Varient_Options_Map] OFF
SET IDENTITY_INSERT [dbo].[PHP_Ecommerce_Product_Varients] ON 

INSERT [dbo].[PHP_Ecommerce_Product_Varients] ([id], [is_required], [name], [product_id], [sort_order]) VALUES (128, 1, N'Hmmmmmm', 230, 1)
INSERT [dbo].[PHP_Ecommerce_Product_Varients] ([id], [is_required], [name], [product_id], [sort_order]) VALUES (129, 1, N'Hmmmmmm', 241, 1)
INSERT [dbo].[PHP_Ecommerce_Product_Varients] ([id], [is_required], [name], [product_id], [sort_order]) VALUES (130, 1, N'Hmmmmmm', 243, 1)
INSERT [dbo].[PHP_Ecommerce_Product_Varients] ([id], [is_required], [name], [product_id], [sort_order]) VALUES (131, 1, N'Hmmmmmm', 244, 1)
INSERT [dbo].[PHP_Ecommerce_Product_Varients] ([id], [is_required], [name], [product_id], [sort_order]) VALUES (132, 1, N'Hmmmmmm', 245, 1)
INSERT [dbo].[PHP_Ecommerce_Product_Varients] ([id], [is_required], [name], [product_id], [sort_order]) VALUES (133, 1, N'Hmmmmmm', 246, 1)
SET IDENTITY_INSERT [dbo].[PHP_Ecommerce_Product_Varients] OFF
SET IDENTITY_INSERT [dbo].[PHP_Ecommerce_Products] ON 

INSERT [dbo].[PHP_Ecommerce_Products] ([id], [Name], [slug], [buid], [description], [industry_id], [brand_id], [supplier_id], [supplier_code], [sales_account_code], [purchase_account_code], [sku], [is_disbaled]) VALUES (230, N'Alchohol', N'alchohol', 500, N'Tesproduct', 5, NULL, NULL, NULL, NULL, NULL, N'Test123', 0)
INSERT [dbo].[PHP_Ecommerce_Products] ([id], [Name], [slug], [buid], [description], [industry_id], [brand_id], [supplier_id], [supplier_code], [sales_account_code], [purchase_account_code], [sku], [is_disbaled]) VALUES (241, N'chompa', N'alchohol-1', 500, N'Tesproduct', 5, NULL, NULL, NULL, NULL, NULL, N'Test1231', 1)
INSERT [dbo].[PHP_Ecommerce_Products] ([id], [Name], [slug], [buid], [description], [industry_id], [brand_id], [supplier_id], [supplier_code], [sales_account_code], [purchase_account_code], [sku], [is_disbaled]) VALUES (243, N'Alchohol', N'alchohol-2', 500, N'Tesproduct', 5, NULL, NULL, NULL, NULL, NULL, N'Test12313', 0)
INSERT [dbo].[PHP_Ecommerce_Products] ([id], [Name], [slug], [buid], [description], [industry_id], [brand_id], [supplier_id], [supplier_code], [sales_account_code], [purchase_account_code], [sku], [is_disbaled]) VALUES (244, N'Alchohol', N'alchohol-3', 500, N'Tesproduct', 5, NULL, NULL, NULL, NULL, NULL, N'Test123', 0)
INSERT [dbo].[PHP_Ecommerce_Products] ([id], [Name], [slug], [buid], [description], [industry_id], [brand_id], [supplier_id], [supplier_code], [sales_account_code], [purchase_account_code], [sku], [is_disbaled]) VALUES (245, N'Alchohol', N'alchohol-4', 500, N'Tesproduct', 5, NULL, NULL, NULL, NULL, NULL, N'Test123', 0)
INSERT [dbo].[PHP_Ecommerce_Products] ([id], [Name], [slug], [buid], [description], [industry_id], [brand_id], [supplier_id], [supplier_code], [sales_account_code], [purchase_account_code], [sku], [is_disbaled]) VALUES (246, N'Alchohol', N'alchohol-5', 500, N'Tesproduct', 5, NULL, NULL, NULL, NULL, NULL, N'Test123', 0)
SET IDENTITY_INSERT [dbo].[PHP_Ecommerce_Products] OFF
/****** Object:  Index [PK_PHP_Ecommerce_Product_images_1]    Script Date: 7/19/2017 3:49:16 AM ******/
ALTER TABLE [dbo].[PHP_Ecommerce_Product_images] ADD  CONSTRAINT [PK_PHP_Ecommerce_Product_images_1] PRIMARY KEY NONCLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PHP_Ecommerce_Product_images] ADD  CONSTRAINT [DF_PHP_Ecommerce_Product_images_default_image]  DEFAULT ((0)) FOR [default_image]
GO
ALTER TABLE [dbo].[PHP_Ecommerce_Suppliers] ADD  CONSTRAINT [DF_PHP_Ecommerce_Suppliers_markup]  DEFAULT ((0.00)) FOR [markup]
GO
ALTER TABLE [dbo].[PHP_Ecommerce_Product_Category_Map]  WITH CHECK ADD  CONSTRAINT [FK_PHP_Ecommerce_Product_Category_Map_PHP_Ecommerce_categories] FOREIGN KEY([category_id])
REFERENCES [dbo].[PHP_Ecommerce_categories] ([id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PHP_Ecommerce_Product_Category_Map] CHECK CONSTRAINT [FK_PHP_Ecommerce_Product_Category_Map_PHP_Ecommerce_categories]
GO
ALTER TABLE [dbo].[PHP_Ecommerce_Product_Category_Map]  WITH CHECK ADD  CONSTRAINT [FK_PHP_Ecommerce_Product_Category_Map_PHP_Ecommerce_Products1] FOREIGN KEY([product_id])
REFERENCES [dbo].[PHP_Ecommerce_Products] ([id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PHP_Ecommerce_Product_Category_Map] CHECK CONSTRAINT [FK_PHP_Ecommerce_Product_Category_Map_PHP_Ecommerce_Products1]
GO
ALTER TABLE [dbo].[PHP_Ecommerce_Product_images]  WITH CHECK ADD  CONSTRAINT [FK_PHP_Ecommerce_Product_images_PHP_Ecommerce_Products] FOREIGN KEY([product_id])
REFERENCES [dbo].[PHP_Ecommerce_Products] ([id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PHP_Ecommerce_Product_images] CHECK CONSTRAINT [FK_PHP_Ecommerce_Product_images_PHP_Ecommerce_Products]
GO
ALTER TABLE [dbo].[PHP_Ecommerce_Product_Price_Map]  WITH CHECK ADD  CONSTRAINT [FK_PHP_Ecommerce_Product_Price_Map_PHP_Ecommerce_Price_Types] FOREIGN KEY([price_type_id])
REFERENCES [dbo].[PHP_Ecommerce_Price_Types] ([id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PHP_Ecommerce_Product_Price_Map] CHECK CONSTRAINT [FK_PHP_Ecommerce_Product_Price_Map_PHP_Ecommerce_Price_Types]
GO
ALTER TABLE [dbo].[PHP_Ecommerce_Product_Price_Map]  WITH CHECK ADD  CONSTRAINT [FK_PHP_Ecommerce_Product_Price_Map_PHP_Ecommerce_Products] FOREIGN KEY([product_id])
REFERENCES [dbo].[PHP_Ecommerce_Products] ([id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PHP_Ecommerce_Product_Price_Map] CHECK CONSTRAINT [FK_PHP_Ecommerce_Product_Price_Map_PHP_Ecommerce_Products]
GO
ALTER TABLE [dbo].[PHP_Ecommerce_Product_Varient_Options_Map]  WITH CHECK ADD  CONSTRAINT [FK_PHP_Ecommerce_Product_Varient_Options_Map_PHP_Ecommerce_Product_Varients] FOREIGN KEY([varient_id])
REFERENCES [dbo].[PHP_Ecommerce_Product_Varients] ([id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PHP_Ecommerce_Product_Varient_Options_Map] CHECK CONSTRAINT [FK_PHP_Ecommerce_Product_Varient_Options_Map_PHP_Ecommerce_Product_Varients]
GO
ALTER TABLE [dbo].[PHP_Ecommerce_Product_Varients]  WITH CHECK ADD  CONSTRAINT [FK_PHP_Ecommerce_Product_Varients_PHP_Ecommerce_Product_Varients_PRODMAP] FOREIGN KEY([product_id])
REFERENCES [dbo].[PHP_Ecommerce_Products] ([id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PHP_Ecommerce_Product_Varients] CHECK CONSTRAINT [FK_PHP_Ecommerce_Product_Varients_PHP_Ecommerce_Product_Varients_PRODMAP]
GO
/****** Object:  StoredProcedure [dbo].[PHP_Ecommerce_AddProductImages]    Script Date: 7/19/2017 3:49:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PHP_Ecommerce_AddProductImages] (@ProductID int,@data xml)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
		if(@productID <= 0)
			 BEGIN
				  RAISERROR ('Sorry image product is required',0,0)
				  RETURN
			 END
	 DECLARE @TransCount AS INT

	 BEGIN TRY
		 SET @TransCount = @@TRANCOUNT
        IF @TransCount > 0 
            SAVE TRANSACTION PrevTran
        ELSE 
            BEGIN TRANSACTION

			CREATE TABLE #TempImageTable
				(
				id int,
				[name] nvarchar(MAX),
				imagePath nvarchar(MAX),
				default_image bit				
				)
			INSERT INTO #TempImageTable

			SELECT	ROW_NUMBER ( ) OVER(ORDER BY Data.value('name[1]', 'nvarchar(MAX)') ASC) AS ID ,
			 [name] = Data.value('name[1]', 'nvarchar(MAX)'),
			 imagePath=Data.value('imagePath[1]', 'nvarchar(MAX)'),
			 default_image=Data.value('default_image[1]', 'bit')

			 FROM
		 @Data.nodes('data/images') as Data([Data]) 

			declare @count int
			declare @index int=1
			select  @count=count(id) from #TempImageTable

			DECLARE @name nvarchar(MAX)
			DECLARE @imagePath nvarchar(MAX)
			DECLARE @default_image bit

				While(@index<=@count)
				 BEGIN
				 SELECT  @name=name,@imagePath=imagePath,@default_image=default_image from #TempImageTable where id = @index
					
						IF(LEN(RTRIM(LTRIM(@imagePath)))=0 OR @imagePath IS NULL)
							BEGIN
								RAISERROR ('Sorry Image Path is needed',0,0)
								RETURN
							END
						IF(@default_image IS NULL)

							BEGIN
								SET @default_image=0
							END

				 INSERT INTO PHP_Ecommerce_Product_images (name,imagePath,product_id,default_image) VALUES(@name,@imagePath,@ProductID,@default_image)	
				 SET @index=@index+1		 
				 END		

				


	IF (@TransCount = 0 )
			BEGIN
				COMMIT TRANSACTION
			END
    END TRY

	  BEGIN CATCH
        IF @TransCount = 0 
            ROLLBACK TRANSACTION
        ELSE 
            BEGIN
                IF XACT_STATE() <> -1 
                    ROLLBACK TRANSACTION PrevTran
            END
        DECLARE @ErrMsg AS VARCHAR(8000) ;
        DECLARE @ErrorSeverity INT ,
            @ErrorState INT
        SELECT  @ErrMsg = ERROR_MESSAGE() --+ ' Line No. - ' + ERROR_LINE()
        SELECT  @ErrorSeverity = ERROR_SEVERITY() ;
        SELECT  @ErrorState = ERROR_STATE() 
        RAISERROR(@ErrMsg, @ErrorSeverity, @ErrorState)

    END CATCH
END


GO
/****** Object:  StoredProcedure [dbo].[PHP_Ecommerce_AddProducts]    Script Date: 7/19/2017 3:49:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PHP_Ecommerce_AddProducts](
	-- Add the parameters for the stored procedure here
	@Name nvarchar(500), 
	@description nvarchar(MAX),
	@sku varchar(50),
	@industry_id smallint,
	@category_id int,
	@PriceTypeID smallint,
	@Amount decimal(18,2),
	@stock float,
	@buid int,
	@slug varchar(255) NULL,
	@brand_id smallint NULL,
	@supplier_id smallint NULL,
	@supplier_code int NULL,
	@sales_account_code int  NULL,
	@purchase_account_code int NULL,
	@units nvarchar(255) NULL,
	@VARIENTS xml NULL,
	@PRODUCTIMAGES xml null,
	@PRODUCTID int OUT
	)
AS
BEGIN 
DECLARE @TransCount AS INT
BEGIN TRY
SET NOCOUNT ON;
SET @TransCount = @@TRANCOUNT
        IF @TransCount > 0 
            SAVE TRANSACTION PrevTran
        ELSE 
           BEGIN TRANSACTION

	
	
	DECLARE @ErrorSeverity int=0
	DECLARE @ErrorState int=0

	IF(LEN(LTRIM(RTRIM(@Name))) = 0)
		BEGIN
			RAISERROR ('Sorry name is required', @ErrorSeverity,@ErrorState)
			RETURN
		END
	IF(LEN(LTRIM(RTRIM(@description))) < 5 AND LEN(LTRIM(RTRIM(@description))) > 1000)
		BEGIN
			RAISERROR ('Sorry product description length should be between 5 to 1000 charectors',@ErrorSeverity,@ErrorState)
			RETURN
		END


	IF(LEN(LTRIM(RTRIM(@industry_id))) = 0)
		BEGIN
			RAISERROR ('Sorry invalid industryid',@ErrorSeverity,@ErrorState)
			RETURN
		END

	IF(LEN(LTRIM(RTRIM(@PriceTypeID))) = 0)
	BEGIN
		RAISERROR ('Sorry price type is needed to map price',@ErrorSeverity,@ErrorState)
		RETURN
	END

	IF(LEN(LTRIM(RTRIM(@Amount))) = 0)
	BEGIN
		RAISERROR ('Sorry amount is needed to map price',@ErrorSeverity,@ErrorState)
		RETURN
	END
	
	if(LEN(LTRIM(RTRIM(@sku))) = 0 OR @sku IS  NULL)
		BEGIN
			RAISERROR ('Please add sku for product',@ErrorSeverity,@ErrorState)
			RETURN	
		END


	


	IF(@slug is null OR LEN(LTRIM(RTRIM(@slug))) = 0)
		BEGIN
		 DECLARE @SAMESLUGCOUNT int
		 DECLARE @CONTRUCTSLUG varchar(4000)
		 select  @CONTRUCTSLUG=dbo.slugify(@Name);

		 --PRINT 'SLUG - '+ @CONTRUCTSLUG

		 SELECT @SAMESLUGCOUNT= COUNT(id) FROM PHP_Ecommerce_Products WHERE slug LIKE @CONTRUCTSLUG+'%';
		 --print 'slug count' +convert(varchar(500),@SAMESLUGCOUNT)
			IF(@SAMESLUGCOUNT=0)
				BEGIN
					SET @slug=@CONTRUCTSLUG
				END
			ELSE
				BEGIN
					SET @slug=@CONTRUCTSLUG+'-'+convert(VARCHAR(255),@SAMESLUGCOUNT)
				END

		END
	
		/* Check If Industry ID Exists or not */
			DECLARE @IndustryIdCount int =0

			SELECT @IndustryIdCount=COUNT(id) FROM PHP_Ecommerce_BU_INDUSTRY_map where industry_id=@industry_id AND buid=@buid

				IF(@IndustryIdCount=0)
					BEGIN
						RAISERROR ('Sorry this industry is not assigned to your bussiness',@ErrorSeverity,@ErrorState)
						RETURN
					END

			/* END INDUSTRY CHECK */

			/* INSERT STATEMENT FOR PRODUCT */

			INSERT INTO [dbo].[PHP_Ecommerce_Products]
					   ([Name]
					   ,[slug]
					   ,[buid]
					   ,[description]
					   ,[industry_id]
					   ,[brand_id]
					   ,[supplier_id]
					   ,[supplier_code]
					   ,[sales_account_code]
					   ,[purchase_account_code]
					   ,[sku])
				 VALUES
					   (@Name
					   ,@slug
					   ,@buid
					   ,@description
					   ,@industry_id
					   ,@brand_id
					   ,@supplier_id
					   ,@supplier_code
					   ,@supplier_code
					   ,@sales_account_code
					   ,@sku)

		   /* END INSERT STATEMENT FOR PRODUCT */

		  SET @PRODUCTID = SCOPE_IDENTITY()

		  if(LEN(LTRIM(RTRIM(@PRODUCTID)))=0)
			BEGIN
			/* CHeck PRODUCT IF */
					RAISERROR ('Sorry Product Not Created',@ErrorSeverity,@ErrorState)
					RETURN
			END

			/*INSERT CATEGORY FOR PRODUCT*/
			 EXEC PHP_Ecommerce_MapProductCategory @PRODUCTID,@category_id
			/* */

			/* INSERT PRICE INTO PRODUCT */
			EXEC PHP_Ecommerce_MapProductPrice @PRODUCTID ,@Amount,@PriceTypeID,@units
			/**/

		/** Add Stocks **/
		
		EXEC PHP_Ecommerce_AddProductStock @PRODUCTID,@stock

		if(@VARIENTS is not NULL) 
		 BEGIN
			EXEC PHP_Ecommerce_AddVarient @PRODUCTID ,@VARIENTS
         END


		 if(@PRODUCTIMAGES is not NULL) 
		 BEGIN
			EXEC PHP_Ecommerce_AddProductImages @PRODUCTID,@PRODUCTIMAGES
         END


		 IF @TransCount = 0 
            COMMIT TRANSACTION
			RETURN @PRODUCTID
END TRY

BEGIN CATCH
 IF @TransCount = 0 
            ROLLBACK TRANSACTION
        ELSE 
            BEGIN
                IF XACT_STATE() <> -1 
                    ROLLBACK TRANSACTION PrevTran
            END

	   DECLARE @ErrMsg AS VARCHAR(8000) ;
        DECLARE @ErrorSeverityy INT ,
            @ErrorStatee INT
        SELECT  @ErrMsg = ERROR_MESSAGE() --+ ' Line No. - ' + ERROR_LINE()
        SELECT  @ErrorSeverityy = ERROR_SEVERITY() ;
        SELECT  @ErrorStatee = ERROR_STATE() 
        RAISERROR(@ErrMsg, @ErrorSeverityy, @ErrorStatee)
END CATCH

END




GO
/****** Object:  StoredProcedure [dbo].[PHP_Ecommerce_AddProductStock]    Script Date: 7/19/2017 3:49:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PHP_Ecommerce_AddProductStock](@ProductID int,@StockAmount float) 
AS
BEGIN
	
DECLARE @TransCount AS INT
 BEGIN TRY
 	SET @TransCount = @@TRANCOUNT
        IF @TransCount > 0 
            SAVE TRANSACTION PrevTran
        ELSE 
            BEGIN TRANSACTION

	SET NOCOUNT ON;
	DECLARE @ErrorSeverity int=0
	DECLARE @ErrorState int=0	
	

		IF(LEN(LTRIM(RTRIM(@ProductID))) = 0)
		BEGIN
			RAISERROR ('Sorry productID needed to add stock', @ErrorSeverity,@ErrorState)
			RETURN
		END

		IF(LEN(LTRIM(RTRIM(@StockAmount))) = 0)
		BEGIN
			RAISERROR ('Sorry Stock quantity needed', @ErrorSeverity,@ErrorState)
			RETURN
		END
		
		Declare @StockKeyID int
		SELECT @StockKeyID = id from PHP_Ecommerce_Product_Stock where product_id=@ProductID
	

		IF(LEN(LTRIM(RTRIM(@StockKeyID))) = 0 OR @StockKeyID IS NULL)
			BEGIN
				INSERT INTO PHP_Ecommerce_Product_Stock (product_id,volume) VALUES(@ProductID,@StockAmount)
			END  
		ELSE

		   BEGIN
			UPDATE PHP_Ecommerce_Product_Stock SET volume=@StockAmount WHERE id=@StockKeyID
		   END


		 IF @TransCount = 0 
            COMMIT TRANSACTION
 END TRY
 BEGIN CATCH
	IF @TransCount = 0 
            ROLLBACK TRANSACTION
        ELSE 
            BEGIN
                IF XACT_STATE() <> -1 
                    ROLLBACK TRANSACTION PrevTran
            END
	  DECLARE @ErrMsg AS VARCHAR(8000) ;
        DECLARE @ErrorSeverityy INT ,
            @ErrorStatee INT
        SELECT  @ErrMsg = ERROR_MESSAGE() --+ ' Line No. - ' + ERROR_LINE()
        SELECT  @ErrorSeverityy = ERROR_SEVERITY() ;
        SELECT  @ErrorStatee = ERROR_STATE() 
        RAISERROR(@ErrMsg, @ErrorSeverityy, @ErrorStatee)
  END CATCH
    
END

GO
/****** Object:  StoredProcedure [dbo].[PHP_Ecommerce_AddVarient]    Script Date: 7/19/2017 3:49:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[PHP_Ecommerce_AddVarient]
(
@productID int,
@data  xml
)
AS
BEGIN
SET NOCOUNT ON
 DECLARE @TransCount AS INT
    BEGIN TRY
		
			if(@productID <= 0)
			 BEGIN
				  RAISERROR ('Sorry varient product is required',0,0)
				  RETURN
			 END

        SET @TransCount = @@TRANCOUNT
        IF @TransCount > 0 
            SAVE TRANSACTION PrevTran
        ELSE 
            BEGIN TRANSACTION
	create table #Tempvarient
	(
	id int,
	is_required bit,
	[name] nvarchar(500),
	sort_order smallint,
	childrens xml
	)
	insert into #Tempvarient
	select	ROW_NUMBER ( ) OVER(ORDER BY Data.value('name[1]', 'nvarchar(500)') ASC) AS ID ,
	 is_required = Data.value('is_required[1]', 'bit'),
	 [name]=Data.value('name[1]', 'nvarchar(500)'),
	 sort_order=Data.value('sort_order[1]', 'smallint'),
	  childrens=Data.query('childrens[1]')  
	  FROM
    @Data.nodes('data/varient') as Data([Data]) 
	--select * from #Tempvarient
		
		declare @count int
		declare @index int
		 select @count=count(id) from #Tempvarient
		 DECLARE @is_required bit
		 DECLARE @name nvarchar(500)
		 DECLARE @Sort_Order int
		 DECLARE @Childrens xml
		 DECLARE @VarientID int
		 set @index=1
		 While(@index<=@count)
			BEGIN
				SELECT  @is_required=is_required,@name=name,@Sort_Order=sort_order,@Childrens=childrens from #Tempvarient where id = @index 
				
			--print '@Childrens :'+convert(varchar(max),@Childrens)
			 /*
				Check Duplicate Varient
			 */

			  DECLARE @DOESEXISTID int;

			  SELECT @DOESEXISTID=id FROM PHP_Ecommerce_Product_Varients where product_id=@productID AND [name]=@name
			  IF(@DOESEXISTID > 0 OR @DOESEXISTID IS NOT NULL)
					BEGIN
						UPDATE PHP_Ecommerce_Product_Varients SET is_required=@is_required,name=@name,product_id=@productID,sort_order=@Sort_Order where id=@DOESEXISTID
					END
			  ELSE
			  
				BEGIN
					INSERT INTO PHP_Ecommerce_Product_Varients (is_required,name,product_id,sort_order) VALUES(@is_required,@name,@productID,@Sort_Order)
				END	
				

				SET @VarientID=SCOPE_IDENTITY()
				
					/** After Getting Varient Id Create Children **/
					create table #TempvarientOptions
						(
						id int,
						varient_id int,
						title nvarchar(255),
						price decimal(18,2),
						pricetype smallint,
						sort_order smallint
						)
					insert into #TempvarientOptions

					select	ROW_NUMBER ( ) OVER(ORDER BY Data.value('title[1]', 'nvarchar(500)') ASC) AS ID ,
					 varient_id = @VarientID,
					 title=Data.value('title[1]', 'nvarchar(255)'),
					 price=Data.value('price[1]', 'decimal(18,2)'),
					 pricetype=Data.value('pricetype[1]', 'smallint'),
					 sort_order=Data.value('sort_order[1]', 'smallint')
					  FROM
					  @Childrens.nodes('childrens/children') as ChildData([Data]) 
					  --select * from #TempvarientOptions
						declare @childcount int
						declare @childindex int
						set @childindex=1
						select  @childcount=count(id) from #TempvarientOptions
						 DECLARE @title nvarchar(255)
						 DECLARE @price decimal(18,2)
						 DECLARE @pricetype smallint
						 DECLARE @optionsort_order smallint
						 While(@childindex<=@childcount)
						  BEGIN
							SELECT  @title=title,@price=price,@pricetype=pricetype,@optionsort_order=sort_order from #TempvarientOptions where id = @childindex 
						    if(LEN(LTRIM(RTRIM(@title)))=0)
							  BEGIN
							     RAISERROR ('Sorry varient title is required',0,0)
								 RETURN
							  END

							  if(LEN(LTRIM(RTRIM(@price)))=0)
							  BEGIN
							     RAISERROR ('Sorry varient price is required',0,0)
								 RETURN
							  END

							  if(LEN(LTRIM(RTRIM(@pricetype)))=0)
							  BEGIN
							     RAISERROR ('Sorry pricetype price is required',0,0)
								 RETURN
							  END

							  DECLARE @PRICETYPEVALIDID int

							  SELECT @PRICETYPEVALIDID FROM PHP_Ecommerce_Price_Types where id=@pricetype

							   if(LEN(LTRIM(RTRIM(@optionsort_order)))=0)
							  BEGIN
							    SET @optionsort_order=1 
							  END

							/*
								CHECK VARIENT EXISTS
							*/

							DECLARE @VarientOPTIONID int
								
									SELECT @VarientOPTIONID=id FROM PHP_Ecommerce_Product_Varient_Options_Map WHERE varient_id=@VarientID AND title=@title

									IF(@VarientOPTIONID > 0 OR @VarientOPTIONID IS NOT NULL)

										BEGIN
											UPDATE PHP_Ecommerce_Product_Varient_Options_Map SET varient_id=@VarientID,title=@title,price=@price,sort_order=@Sort_Order WHERE id=@VarientOPTIONID
										END
									ELSE
										BEGIN

											INSERT INTO PHP_Ecommerce_Product_Varient_Options_Map (varient_id,title,price,pricetype,sort_order) VALUES (@VarientID,@title,@price,@pricetype,@Sort_Order)
										END

							SET @childindex=@childindex+1;
						  END
			drop table #TempvarientOptions

				SET @index=@index+1
            END


	
IF @TransCount = 0 
            COMMIT TRANSACTION
    END TRY
    BEGIN CATCH
        IF @TransCount = 0 
            ROLLBACK TRANSACTION
        ELSE 
            BEGIN
                IF XACT_STATE() <> -1 
                    ROLLBACK TRANSACTION PrevTran
            END
        DECLARE @ErrMsg AS VARCHAR(8000) ;
        DECLARE @ErrorSeverity INT ,
            @ErrorState INT
        SELECT  @ErrMsg = ERROR_MESSAGE() --+ ' Line No. - ' + ERROR_LINE()
        SELECT  @ErrorSeverity = ERROR_SEVERITY() ;
        SELECT  @ErrorState = ERROR_STATE() 
        RAISERROR(@ErrMsg, @ErrorSeverity, @ErrorState)

    END CATCH
END



GO
/****** Object:  StoredProcedure [dbo].[PHP_Ecommerce_GetProductDetails]    Script Date: 7/19/2017 3:49:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PHP_Ecommerce_GetProductDetails](@BUID int,@PRODUCTID int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT TOP 1 P.id,P.Name as Productname,P.slug,Cat.name as Categoryname,CAT.id as category_id,P.description,P.sku,PSTOCK.volume as Stock,PMAP.price,
	CASE 
	 WHEN PMAP.units IS NULL
	 THEN  PTYPE.name

	 WHEN PMAP.units IS NOT NULL

	 THEN PMAP.units

	 END

	 as PriceType
		
	FROM PHP_Ecommerce_Products as P 
		
		INNER JOIN PHP_Ecommerce_Product_Category_Map as CP on CP.product_id=P.id
		INNER JOIN PHP_Ecommerce_categories as Cat On Cat.id=CP.category_id
		INNER JOIN PHP_Ecommerce_industries as IND on IND.id=P.industry_id
		INNER JOIN PHP_Ecommerce_Product_Stock as PSTOCK on PSTOCK.product_id=P.id
		INNER JOIN PHP_Ecommerce_Product_Price_Map as PMAP on PMAP.product_id=P.id
		INNER JOIN PHP_Ecommerce_Price_Types as PTYPE on PTYPE.id=PMAP.price_type_id
		WHERE p.buid=@buid AND P.id=@PRODUCTID

   
   EXEC PHP_Ecommerce_GetProductImages @PRODUCTID

   EXEC PHP_Ecommerce_GetProductVarients @PRODUCTID

END

GO
/****** Object:  StoredProcedure [dbo].[PHP_Ecommerce_GetProductImages]    Script Date: 7/19/2017 3:49:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PHP_Ecommerce_GetProductImages](@Productid int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM PHP_Ecommerce_Product_images where product_id=@Productid
END

GO
/****** Object:  StoredProcedure [dbo].[PHP_Ecommerce_Getproducts]    Script Date: 7/19/2017 3:49:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PHP_Ecommerce_Getproducts] (@buid int,@Start int null, @Limit int null, @Likename nvarchar(MAX) null,@INDUSTRYID int null,@CATAGORYID nvarchar(MAX) null,@SHOWALL BIT null,@PRICEOVER float null)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if(@Start IS NULL)
		BEGIN
			SET @Start = 0
		END
	if(@Limit IS NULL)
		BEGIN
			SET @Limit = 10
		END
	IF(@Limit > 10)
		BEGIN
			SET @Limit=10
		END

	IF(@SHOWALL IS NULL)
		BEGIN
			SET @SHOWALL=0
		END
    -- Insert statements for procedure here
	SELECT DISTINCT COUNT(P.id) as TotalProducts
		
	FROM PHP_Ecommerce_Products as P 
		
		INNER JOIN PHP_Ecommerce_Product_Category_Map as CP on CP.product_id=P.id
		INNER JOIN PHP_Ecommerce_categories as Cat On Cat.id=CP.category_id
		INNER JOIN PHP_Ecommerce_industries as IND on IND.id=P.industry_id
		INNER JOIN PHP_Ecommerce_Product_Stock as PSTOCK on PSTOCK.product_id=P.id
		INNER JOIN PHP_Ecommerce_Product_Price_Map as PMAP on PMAP.product_id=P.id
		INNER JOIN PHP_Ecommerce_Price_Types as PTYPE on PTYPE.id=PMAP.price_type_id
		LEFT JOIN PHP_Ecommerce_Product_images as PIMAGE ON PIMAGE.id= (
			SELECT TOP 1 imagePath from PHP_Ecommerce_Product_images WHERE product_id=P.id AND default_image=1
		) 
		WHERE p.buid=@buid 
		 AND ((@SHOWALL = 0  AND  P.is_disbaled = 0 ) OR @SHOWALL = 1)
		 AND ((@Likename IS NULL  OR  P.name LIKE @Likename+'%'))
		 AND (@INDUSTRYID IS NULL  OR  P.industry_id= @INDUSTRYID)
		 AND (@CATAGORYID IS NULL  OR  CP.category_id= @CATAGORYID)
		 AND (@PRICEOVER IS  NULL  OR   PMAP.price >= @PRICEOVER)

	--PRINT 'TOTAL'+convert(nvarchar(500),@TotalRows)

	SELECT P.id,P.Name as Productname,P.slug,PIMAGE.imagePath,Cat.name as Categoryname,CAT.id as category_id,P.description,P.sku,PSTOCK.volume as Stock,PMAP.price,
	CASE 
	 WHEN PMAP.units IS NULL
	 THEN  PTYPE.name

	 WHEN PMAP.units IS NOT NULL

	 THEN PMAP.units

	 END

	 as PriceType
		
	FROM PHP_Ecommerce_Products as P 
		
		INNER JOIN PHP_Ecommerce_Product_Category_Map as CP on CP.product_id=P.id
		INNER JOIN PHP_Ecommerce_categories as Cat On Cat.id=CP.category_id
		INNER JOIN PHP_Ecommerce_industries as IND on IND.id=P.industry_id
		INNER JOIN PHP_Ecommerce_Product_Stock as PSTOCK on PSTOCK.product_id=P.id
		INNER JOIN PHP_Ecommerce_Product_Price_Map as PMAP on PMAP.product_id=P.id
		INNER JOIN PHP_Ecommerce_Price_Types as PTYPE on PTYPE.id=PMAP.price_type_id
		LEFT JOIN PHP_Ecommerce_Product_images as PIMAGE ON PIMAGE.id= (
			SELECT TOP 1 imagePath from PHP_Ecommerce_Product_images WHERE product_id=P.id AND default_image=1
		) 
		WHERE p.buid=@buid 
		 AND ((@SHOWALL = 0  AND  P.is_disbaled = 0 ) OR @SHOWALL = 1)
		 AND ((@Likename IS NULL  OR  P.name LIKE @Likename+'%'))
		 AND (@INDUSTRYID IS NULL  OR  P.industry_id= @INDUSTRYID)
		 AND (@CATAGORYID IS NULL  OR  CP.category_id= @CATAGORYID)
		 AND (@PRICEOVER IS  NULL  OR   PMAP.price >= @PRICEOVER)
		 ORDER BY P.id OFFSET @Start ROWS FETCH NEXT @Limit ROWS ONLY
END


GO
/****** Object:  StoredProcedure [dbo].[PHP_Ecommerce_GetProductVarients]    Script Date: 7/19/2017 3:49:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PHP_Ecommerce_GetProductVarients](@PRODUCTID int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	SELECT * FROM  PHP_Ecommerce_Product_Varients where product_id=@PRODUCTID

	SELECT * FROM PHP_Ecommerce_Product_Varient_Options_Map where varient_id in (SELECT id FROM  PHP_Ecommerce_Product_Varients where product_id=@PRODUCTID)
END

GO
/****** Object:  StoredProcedure [dbo].[PHP_Ecommerce_MapProductCategory]    Script Date: 7/19/2017 3:49:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PHP_Ecommerce_MapProductCategory](@ProductID int,@CategoryID int)
AS
BEGIN
	DECLARE @TransCount AS INT
	BEGIN TRY
	 SET @TransCount = @@TRANCOUNT
        IF @TransCount > 0 
            SAVE TRANSACTION PrevTran
        ELSE 
            BEGIN TRANSACTION
		SET NOCOUNT ON;
		
		IF(LEN(LTRIM(RTRIM(@ProductID)))= 0 OR @ProductID is null)
			BEGIN
				RAISERROR('Product id can not be empty while adding category',0,0)
				RETURN
			END

	   
	   IF(LEN(LTRIM(RTRIM(@CategoryID)))= 0 OR @CategoryID is null)
			BEGIN
				RAISERROR('Cateogry id can not be empty while adding category',0,0)
				RETURN
			END
		DECLARE @CATEGORYCOUNT int= 0
			
			SELECT  @CATEGORYCOUNT = COUNT(id) FROM PHP_Ecommerce_categories where id=@CategoryID
				IF(@CATEGORYCOUNT=0)
					BEGIN
					 RAISERROR ('Sorry Invalid Category id',0,0)
					 RETURN
					END

	   INSERT INTO PHP_Ecommerce_Product_Category_Map (category_id,product_id) VALUES (@CategoryID,@ProductID)


	  IF @TransCount = 0 COMMIT TRANSACTION


	END TRY

	BEGIN CATCH
	IF @TransCount = 0 
            ROLLBACK TRANSACTION
        ELSE 
            BEGIN
                IF XACT_STATE() <> -1 
                    ROLLBACK TRANSACTION PrevTran
            END
		DECLARE @ErrMsg AS VARCHAR(8000) ;
        DECLARE @ErrorSeverityy INT ,
            @ErrorStatee INT
        SELECT  @ErrMsg = ERROR_MESSAGE() 
        SELECT  @ErrorSeverityy = ERROR_SEVERITY() ;
        SELECT  @ErrorStatee = ERROR_STATE() 
        RAISERROR(@ErrMsg, @ErrorSeverityy, @ErrorStatee)

	END CATCH
END



GO
/****** Object:  StoredProcedure [dbo].[PHP_Ecommerce_MapProductPrice]    Script Date: 7/19/2017 3:49:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PHP_Ecommerce_MapProductPrice] (@ProductID int,@Amount Decimal (18,2),@PriceTypeID smallint, @units nVarchar(250) null)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	DECLARE @TransCount AS INT
	BEGIN TRY
		SET @TransCount = @@TRANCOUNT
        IF @TransCount > 0 
            SAVE TRANSACTION PrevTran
        ELSE 
            BEGIN TRANSACTION

	SET NOCOUNT ON;
	  
	  IF(LEN(LTRIM(RTRIM(@ProductID))) = 0 OR @ProductID is null)
		BEGIN
			RAISERROR('Sorry Product id can not be empty while adding product price', 0, 0)
			RETURN
		END

		IF(LEN(LTRIM(RTRIM(@Amount))) = 0 OR @Amount is null)
		BEGIN
			RAISERROR('Sorry Product price can not be empty while adding product price', 0, 0)
			RETURN
		END

		IF(LEN(LTRIM(RTRIM(@PriceTypeID))) = 0 OR @PriceTypeID is null)
		BEGIN
			RAISERROR('Sorry Product Price Type can not be empty while adding product price', 0, 0)
			RETURN
		END

		INSERT INTO [dbo].[PHP_Ecommerce_Product_Price_Map]
					   ([product_id]
					   ,[price_type_id]
					   ,[price]
					   ,[units])
			 VALUES
				   (@ProductID
				   ,@PriceTypeID
				   ,@Amount
				   ,@units)

				   IF @TransCount = 0 
            COMMIT TRANSACTION
 END TRY
 BEGIN CATCH
	 IF @TransCount = 0 
            ROLLBACK TRANSACTION
        ELSE 
            BEGIN
                IF XACT_STATE() <> -1 
                    ROLLBACK TRANSACTION PrevTran
            END
	  DECLARE @ErrMsg AS VARCHAR(8000) ;
        DECLARE @ErrorSeverityy INT ,
            @ErrorStatee INT
        SELECT  @ErrMsg = ERROR_MESSAGE() --+ ' Line No. - ' + ERROR_LINE()
        SELECT  @ErrorSeverityy = ERROR_SEVERITY() ;
        SELECT  @ErrorStatee = ERROR_STATE() 
        RAISERROR(@ErrMsg, @ErrorSeverityy, @ErrorStatee)
  END CATCH
END



GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PHP_Ecommerce_Products', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PHP_Ecommerce_Products', @level2type=N'COLUMN',@level2name=N'slug'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PHP_Ecommerce_Products', @level2type=N'COLUMN',@level2name=N'buid'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PHP_Ecommerce_Products', @level2type=N'COLUMN',@level2name=N'description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PHP_Ecommerce_Products', @level2type=N'COLUMN',@level2name=N'industry_id'
GO
