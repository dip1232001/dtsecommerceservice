var gulp = require('gulp');
var sass = require('gulp-sass');

// Compile all the .scss files from
// ./scss to ./css
gulp.task('scss', function () {
  gulp.src('./sass/**/*.scss')
    .pipe(sass())
    .pipe(gulp.dest('./css'));
});

// Watches for file changes
gulp.task('watch', function(){
  gulp.watch('./sass/**/*.scss', ['scss']);
});

gulp.task('develop', ['scss', 'watch']);

gulp.task('compile',['scss']);
