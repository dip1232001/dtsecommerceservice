<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With");
header('P3P: CP="CAO PSA OUR"');

if (!empty($_POST['username']) && !empty($_POST['password'])) {
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://ecom.myteamconnector.com/webservice/Authenticate",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "grant_type=client_credentials",
        CURLOPT_HTTPHEADER => array(
            "authorization: Basic " . base64_encode(trim(strip_tags($_POST['username'])) . ':' . trim(strip_tags($_POST['password']))),
            "cache-control: no-cache",
            "content-type: application/x-www-form-urlencoded"
        ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
        die("cURL Error #:" . $err);
    }
    else {
        $response = json_decode($response, true);
        if (!empty($response['error'])) {
            die($response['error_description']);
        }
        else {
            setcookie('AT', $response['access_token'], time() + $response['expires_in']);
            setcookie('K', base64_encode(trim(strip_tags($_POST['username'])) . ':' . trim(strip_tags($_POST['password']))), (time() + 2600 * 24));
            if (empty($_GET['redirecto'])) {
                header("Location: /dashboard");
            }
            else {
                header("Location: /" . $_GET['redirecto']);
            }
            exit();
        }
    }
}
else {
    die("Yolo !!!");
}