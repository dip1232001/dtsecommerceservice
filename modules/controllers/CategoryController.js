EcommerceApp.controller('CategoryController', ['$scope', 'categoryService', '$routeParams', 'Alertify','isMobile', function($scope, categoryService, $routeParams, Alertify, isMobile) {

        $scope.error = null;
        $scope.modalerror = null;
        $scope.msg = null;
        $scope.modalmsg = null;
        $scope.isM = 'Desktop';
        if( isMobile.any() ) 
        {
            var ggg = isMobile.any();
            $scope.isM = 'Mobile';
            if(ggg["0"] == 'iPad'){
                $scope.isM = 'iPad';
            }        
        }

        $scope.CategoryForm = {
            name: null,
            parent_id: null,
            status: 1,
            displayorder: 1,
            image_url: false
        };
        $scope.categorydata = [];
        $scope.parent_id = 0;

        $scope.frommode = 'add';
        $scope.category_id = null;
        $scope.myFile = false;
        $scope.ShowLoader = true;
        $scope.exhemp = false;
        $scope.showoverlay = false;
        $scope.createCategory = function(name) {

            angular.element('#categorySaveButton').button('loading');
            $scope.error = null;
            $scope.modalerror = null;
            $scope.msg = null;
            $scope.modalmsg = null;

            if (typeof (name) !== "undefined") {
                $scope.CategoryForm.name = name;
            }
            if ($scope.CategoryForm.name === null) {
                $scope.modalerror = true;
                $scope.msg = "Category can't be blank";
            }

            var file = $scope.myFile;
            var fd = new FormData();
            if ($scope.frommode == 'edit') {
                fd.append('id', $scope.category_id);
            }
            if (file) {
                fd.append('Images', file);
                fd.append('image_url', null);
            } else {
                fd.append('image_url', $scope.CategoryForm.image_url);
                fd.append('Images', null);
            }
            fd.append('name', $scope.CategoryForm.name);
            if ($routeParams.hasOwnProperty('id') && typeof ($routeParams.id) !== 'undefined' && $routeParams.id > 0) {
                $scope.CategoryForm.parent_id = $routeParams.id;
            }
            fd.append('parent_id', $scope.CategoryForm.parent_id);
            fd.append('status', $scope.CategoryForm.status);
            fd.append('displayorder', $scope.CategoryForm.displayorder);
            //console.log(fd);
            if($scope.modalerror == null){
                categoryService.createCategory(fd).then(function(response) {
                    if (response.data.error) {
                        $scope.modalerror = true;
                        $scope.modalmsg = response.data.msg;
                    } else {
                        $scope.modalerror = false;
                        $scope.getCategoryTree();
                        $scope.modalmsg = response.data.msg;
                        $scope.msg = response.data.msg;
                        $scope.error = false;
                        angular.element('.bs-example-modal-lg').modal('toggle');
                    }
                    angular.element('#categorySaveButton').button('reset');
                    

                    //console.log(response);
                }).catch(function(e){
                    $scope.modalerror = true;
                    $scope.modalmsg = e.statusText;
                });
            }
        };

        $scope.getCategory = function() {
            if ($routeParams.hasOwnProperty('id') && typeof ($routeParams.id) !== 'undefined' && $routeParams.id > 0) {
                $scope.parent_id = $routeParams.id;
            }
            categoryService.getCategory({parent_id: $scope.parent_id}).then(function(response) {
                if (response.error) {
                    $scope.error = true;
                    $scope.msg = response.msg;
                }
                //console.log(data);
                $scope.categorydata = response.data.data.categories;
                $scope.ShowLoader = false;
                //console.log(response);

            }).catch(function(e){
                $scope.error = true;
                $scope.msg = e.statusText;
                $scope.ShowLoader = false;
            });
        };

        $scope.deleteCategory = function(category) {
            Alertify.confirm('Are you sure to delete this Category?').then(
                    function onOk() {
                        var data = {"id": category.id};
                        categoryService.deleteCategory(data).then(function(response) {
                            if (response.error) {
                                $scope.error = true;
                                $scope.msg = response.msg;
                            } else {
                                //$scope.removeCategoryFromList(category);
                                $scope.getCategoryTree();
                            }
                        }).catch(function(e){
                            $scope.error = true;
                            $scope.msg = e.statusText;
                        });
                    },
                    function onCancel() {
                        console.log('you have click cancel;');
                    }
            );
        };

        $scope.removeCategoryFromList = function(item) {
            var index = $scope.categorydata.indexOf(item);
            $scope.categorydata.splice(index, 1);
        }

        $scope.show_overlay = function() {
            $scope.showoverlay = true;
        };

        $scope.hide_overlay = function() {
            $scope.showoverlay = false;
        };

        $scope.edit_category = function(category) {
            //console.log(category);
            $scope.modalerror = null;
            $scope.modalmsg = null;
            $scope.showoverlay = false;
            if (typeof (category.status) == 'undefined') {
                category.status = 1;
            }
            $scope.category_id = category.id;
            $scope.CategoryForm.name = category.name;
            $scope.CategoryForm.parent_id = category.parent_id;
            $scope.CategoryForm.status = category.status;
            $scope.CategoryForm.displayorder = category.displayorder;
            $scope.CategoryForm.image_url = category.image;
            $scope.myFile = false;
            $scope.msg = null;
            $scope.error = null;
            $scope.frommode = 'edit';
            angular.element('.bs-example-modal-lg').modal('toggle');
        };

        $scope.makeInactive = function(category, status) {
            if(status==0){
                var stat='inactive';
            }
            else{
                var stat='active';
            }
            
            Alertify.confirm('Are you sure to make this category '+stat+'?').then(
                    function onOk() {
                        if (typeof (status) === "undefined") {
                            status = 1;
                        }
                        var fd = new FormData();
                        fd.append('name', category.name);
                        fd.append('parent_id', Math.abs(category.parent_id));
                        fd.append('status', Math.abs(status));
                        fd.append('image_url', category.image);
                        fd.append('id', Math.abs(category.id));
                        categoryService.createCategory(fd).then(function(response) {
                            $scope.msg = response.msg;
                            if (response.error) {
                                $scope.error = true;
                            } else {
                                category.is_active = status;

                            }
                        }).catch(function(e){
                            $scope.error = true;
                            $scope.msg = e.statusText;
                        });
                    },
                    function onCancel() {
                        console.log('you have click cancel;');
                    }
            );
        }

        $scope.AddNewCategoryModal = function() {
            $scope.CategoryForm = {
                name: null,
                parent_id: null,
                status: 1,
                displayorder: 1,
                image_url: false
            };
            $scope.msg = null;
            $scope.error = null;
            $scope.modalerror = null;
            $scope.modalmsg = null;
            $scope.frommode = 'add';
            $scope.myFile = false;
            //$scope.myFile = null;
            angular.element('.bs-example-modal-lg').modal('toggle');
        }

        $scope.AddNewSubCategoryModal = function(node) {
            $scope.CategoryForm = {
                name: null,
                parent_id: node.id,
                status: 1,
                displayorder: 1,
                image_url: false
            };
            $scope.msg = null;
            $scope.error = null;
            $scope.modalerror = null;
            $scope.modalmsg = null;
            $scope.frommode = 'add';
            $scope.myFile = false;
            //$scope.myFile = null;
            angular.element('.bs-example-modal-lg').modal('toggle');
        }

        $scope.expandhelp = function(){
            $scope.exhemp = true;
        }

        $scope.closehelp = function(){
            $scope.exhemp = true;
        }

        $scope.showop = function(tt){
            this.showoverlay = tt;
            console.log(this.showoverlay);
        }

        $scope.getCategoryTree = function() {
            if ($routeParams.hasOwnProperty('id') && typeof ($routeParams.id) !== 'undefined' && $routeParams.id > 0) {
                $scope.parent_id = $routeParams.id;
            }
            categoryService.getCategoryTree({parent_id: $scope.parent_id}).then(function(response) {
                if (response.error) {
                    $scope.error = true;
                    $scope.msg = response.msg;
                }
                //console.log(data);
                $scope.categorydata = response.data.data;
                $scope.ShowLoader = false;
                console.log("Tree==>",response);

            }).catch(function(e){
                $scope.error = true;
                $scope.msg = e.statusText;
                $scope.ShowLoader = false;
            });
        };

        $scope.gotoProductPage = function(id){
            console.log(id);
            setTimeout(function(){ $window.location.href = 'categoryproducts/' + id; }, 10);
        }

        $scope.treeOptions = {
            dropped : function (e) {
                console.log(event);

                console.log("new tree ====>", $scope.categorydata);
                categoryService.updateCategorieOrder({data: $scope.categorydata}).then(function(response) {
                    /*if (response.error) {
                        $scope.error = true;
                        $scope.msg = response.msg;
                    }*/
                    //console.log(data);
                    /*$scope.categorydata = response.data.data;
                    $scope.ShowLoader = false;*/
                    console.log("Tree==>",response);
                    $scope.getCategoryTree();
    
                }).catch(function(e){
                    $scope.error = true;
                    $scope.msg = e.statusText;
                    $scope.ShowLoader = false;
                });



            }
        }

    }]);

