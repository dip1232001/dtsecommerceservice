EcommerceApp.controller('CategoryProductOrderController', ['$scope', 'productService', '$timeout', '$routeParams', 'Alertify', '$location', '$window', 'isMobile', function ($scope, productService, $timeout, $routeParams, Alertify, $location, $window, isMobile) {

    $scope.error = null;
    $scope.modalerror = null;
    $scope.msg = null;
    $scope.modalmsg = null;
    $scope.isM = 'Desktop';
    if( isMobile.any() ) 
    {
        var ggg = isMobile.any();
        $scope.isM = 'Mobile';
        if(ggg["0"] == 'iPad'){
            $scope.isM = 'iPad';
        }        
    }

    $scope.category_id = null;

    if ($routeParams.hasOwnProperty('catId') && typeof ($routeParams.catId) !== 'undefined' && $routeParams.catId > 0) {
        $scope.category_id = $routeParams.catId;
    }

    $scope.productdata = [];

    $scope.getProduct = function () {
        productService.GetProductOrderForCategory({
            category_id: $scope.category_id,
            showall: true,
            name: $scope.search
        }).then(function (response) {
            if (response.error) {
                $scope.error = true;
                $scope.msg = response.msg;
            } else {
                $scope.error = null;
                $scope.msg = null;
            }
            $scope.productdata = response.data.data.Products;
            $scope.ShowLoader = false;

        }).catch(function (e) {
            $scope.error = true;
            $scope.msg = e.statusText;
            $scope.ShowLoader = false;
        });
    };

    $scope.searchProduct = function () {
        $scope.ShowLoader = true;
        $scope.start = 0;
        $scope.currentPage = 1;

        $scope.productcheck = [];
        $scope.selectall = false;
        //$location.search('page', null);

        $scope.getProduct();

        /*productService.getProduct({category_id: $scope.category_id, showall: true, name: $scope.search}, $scope.start, $scope.pageSize).then(function (response) {
            if (response.error) {
                $scope.error = true;
                $scope.msg = response.msg;
            }
            $scope.productdata = response.data.data.Products;
            $scope.total = response.data.data.Paginations.Total;
            $scope.ShowLoader = false;
        }).catch(function (e) {
            $scope.error = true;
            $scope.msg = e.statusText;
        });*/

    };

    $scope.resetsearchProduct = function () {
        $scope.search = null;
        $scope.start = 0;
        $scope.currentPage = 1;
        $location.search('page', null);
        $scope.getProduct();

    };

    $scope.reorderProduct = function () {
        productService.reorderProduct($scope.productdata).then(function (response) {
            //$scope.msg = response.data.msg;
            if (response.data.error) {
                $scope.error = true;
            } else {
                $scope.error = false;
                $scope.msg = "Product status changed successfully";
                $scope.getProduct();
            }
        }).catch(function (e) {
            $scope.error = true;
            $scope.msg = e.statusText;
        });
    }



}]);