EcommerceApp.controller('CustomerController', ['$scope', 'customerService', '$routeParams', 'Alertify', 'storeType', '$location', '$routeParams', 'isMobile', function ($scope, customerService, $routeParams, Alertify, storeType, $location, $routeParams, isMobile) {
        $scope.error = null;
        $scope.modalerror = null;
        $scope.msg = null;
        $scope.modalmsg = null;
        $scope.ShowLoader = true;
        $scope.customerID = null;
        $scope.frommode = '';
        $scope.isM = 'Desktop';
        if( isMobile.any() ) 
        {
            var ggg = isMobile.any();
            $scope.isM = 'Mobile';
            if(ggg["0"] == 'iPad'){
                $scope.isM = 'iPad';
            }        
        }

        $scope.pgHref = $location.path();

        if ($routeParams.hasOwnProperty('id') && typeof ($routeParams.id) !== 'undefined' && $routeParams.id > 0) {
            $scope.customerID = $routeParams.id;
        }

        $scope.start = 0;
        $scope.currentPage = 1;
        $scope.pageSize = 20;
        $scope.total = null;
        $scope.hideIfEmpty = true;
        $scope.showPrevNext = true;
        $scope.showFirstLast = true;

        if ($routeParams.hasOwnProperty('page') && typeof ($routeParams.page) !== 'undefined' && $routeParams.page > 0) {
            $scope.currentPage = $routeParams.page;
        }

        $scope.makeshipping=false;
        $scope.selectedOrderID = null;

        $scope.customerdata = [];
        $scope.customerDetailsdata = [];
        $scope.customerDetailsAddressdata = [];
        $scope.address_type = null;
        $scope.search = null;

        $scope.customercreatedate = new Date();

        $scope.addressform = {
            id: null,
            customer_id: null,
            address_type: null,
            name: null,
            phoneno: null,
            pincode: null,
            nearByLocation: null,
            address: null,
            city: null,
            state: null,
            country: null,
            landbark: null,
            alrternetPH: null
        }

        $scope.customerform = {
            id: null,
            firstname: null,
            lastname: null,
            email: null,
            phone: null,
            password: null,
            confirm_password: null,
        }

        $scope.searchData = {
            id: null,
            startdate: null,
            enddate: null,
        };

        $scope.ShipmentForm = {
            shipperAddress: null,
            trackingNO: null
        };

        $scope.orderData = [];
        $scope.orderDetailsData = [];
        $scope.orderStatusData = [];
        $scope.selectedOrder = [];

        $scope.storeType=storeType;

        $scope.AddNewCustomerModal = function () {
            $scope.customerform = {
                id: null,
                firstname: null,
                lastname: null,
                email: null,
                phone: null,
                password: null,
                confirm_password: null,
            }
            $scope.msg = null;
            $scope.error = null;
            $scope.modalmsg = null;
            $scope.frommode = 'Add';

            angular.element('#customerModal').modal('toggle');
        }

        $scope.editCustomer = function (data) {
            $scope.customerform = {
                id: data.id,
                firstname: data.FirstName,
                lastname: data.Lastname,
                email: data.Email,
                phone: data.PhoneNumber,
                password: null,
                confirm_password: null,
            }
            $scope.msg = null;
            $scope.error = null;
            $scope.modalmsg = null;
            $scope.modalerror = null;
            $scope.frommode = 'Edit';

            angular.element('#customerModal').modal('toggle');
        }

        $scope.addCustomer = function () {
            $scope.error = null;
            $scope.modalerror = null;
            $scope.modalmsg = null;
            $scope.msg = null;

            if ($scope.customerform.firstname === null || $scope.customerform.firstname === '') {
                $scope.modalerror = true;
                $scope.modalmsg = "First name can't be blank";
            } else if ($scope.customerform.lastname === null || $scope.customerform.lastname === '') {
                $scope.modalerror = true;
                $scope.modalmsg = "Last name can't be blank";
            } else if ($scope.customerform.email === null || $scope.customerform.email === '') {
                $scope.modalerror = true;
                $scope.modalmsg = "Email can't be blank";
            } else if ($scope.customerform.phone === null || $scope.customerform.phone === '') {
                $scope.modalerror = true;
                $scope.modalmsg = "Phono no can't be blank";
            } else if ($scope.customerform.id == null && ($scope.customerform.password === null || $scope.customerform.password === '')) {
                $scope.modalerror = true;
                $scope.modalmsg = "password can't be blank";
            } else if ($scope.customerform.id == null &&($scope.customerform.confirm_password === null || $scope.customerform.confirm_password === '')) {
                $scope.modalerror = true;
                $scope.modalmsg = "Confirm password can't be blank";
            } else if ($scope.customerform.password != $scope.customerform.confirm_password) {
                $scope.modalerror = true;
                $scope.modalmsg = "Password and confiorm password does not matched !!";
            }
            if ($scope.modalerror == null) {
                angular.element('#savecustomerbutton').button('loading');
                $scope.error = null;
                $scope.modalerror = null;
                $scope.msg = null;
                $scope.modalmsg = null;
                customerService.createCustomer(toFormData($scope.customerform)).then(function (response) {
                    if (response.data.error) {
                        $scope.modalerror = true;
                        $scope.modalmsg = response.data.msg;
                    } else {
                        $scope.modalerror = false;
                        $scope.getCustomer();
                        $scope.modalmsg = response.data.msg;
                        if($scope.frommode == 'Edit'){
                            $scope.modalmsg = 'Customer Edited succesfully';
                        }
                    }
                    angular.element('#savecustomerbutton').button('reset');
                });

            }
        }

        $scope.getCustomer = function () {
            if( $scope.currentPage > 1 ){
                var Newstart = (($scope.currentPage - 1) * $scope.pageSize);
                $scope.start = Newstart;
            }

            customerService.getCustomer({search: $scope.search}, $scope.start, $scope.pageSize).then(function (response) {
                if (response.error) {
                    $scope.error = true;
                    $scope.msg = response.data.msg;
                }
                $scope.customerdata = response.data.data;
                $scope.total = response.data.data.Pagination.total;
                if ($routeParams.hasOwnProperty('page') && typeof ($routeParams.page) !== 'undefined' && $routeParams.page > 0) {
                    $scope.currentPage = $routeParams.page;
                }
                $scope.ShowLoader = false;
            }).catch(function (e) {
                $scope.error = true;
                $scope.msg = e.statusText;
                $scope.ShowLoader = false;
            });
        }

        $scope.searchCustomer = function () {
            $scope.start = 0;
            $scope.currentPage = 1;
            $scope.getCustomer();
        }

        $scope.DoCtrlPagingAct = function (text, page, pageSize, total) {

            var Newstart = ((page - 1) * pageSize);
            $scope.start = Newstart;
            $scope.ShowLoader = true;
            customerService.getCustomer({search: $scope.search}, Newstart, $scope.pageSize).then(function (response) {
                if (response.error) {
                    $scope.error = true;
                    $scope.msg = response.msg;
                }
                $scope.customerdata = response.data.data;
                $scope.total = response.data.data.Pagination.total;
                $scope.ShowLoader = false;
            });
        }

        $scope.getCustomerDetails = function () {
            customerService.getCustomerDetails({id: $scope.customerID}).then(function (response) {
                if (response.error) {
                    $scope.error = true;
                    $scope.msg = response.msg;
                }
                $scope.customerDetailsdata = response.data.data;
                $scope.customercreatedate = new Date($scope.customerDetailsdata.CreatedDate);
                $scope.ShowLoader = false;
            });
        }

        $scope.getCustomerDetailsAddress = function (id, type) {
            if (type == 'billing') {
                customerService.getBillingAddress(id).then(function (response) {
                    $scope.customerDetailsAddressdata = response.data.data;
                }).catch(function (e) {
                    $scope.modalerror = true;
                    $scope.modalmsg = e.statusText;
                });
            }
            if (type == 'shipping') {
                customerService.getShippingAddress(id).then(function (response) {
                    $scope.customerDetailsAddressdata = response.data.data;
                }).catch(function (e) {
                    $scope.modalerror = true;
                    $scope.modalmsg = e.statusText;
                });
            }

        }

        $scope.addbillingAddress = function () {
            $scope.error = null;
            $scope.modalerror = null;
            $scope.msg = null;
            $scope.modalmsg = null;
            
            $scope.addressform = {
                id: null,
                customer_id: $scope.customerID,
                address_type: 'billing',
                name: null,
                phoneno: null,
                pincode: null,
                nearByLocation: null,
                address: null,
                city: null,
                state: null,
                country: null,
                landmark: null,
                alrternetPH: null
            }
            $scope.address_type = 'billing';
            //codes for get billing address details
            $scope.getCustomerDetailsAddress($scope.customerID, 'billing');

            angular.element('#addressModal').modal('toggle');
        }

        $scope.addshippingAddress = function () {
            $scope.error = null;
            $scope.modalerror = null;
            $scope.msg = null;
            $scope.modalmsg = null;

            $scope.addressform = {
                id: null,
                customer_id: $scope.customerID,
                address_type: 'shipping',
                name: null,
                phoneno: null,
                poncode: null,
                nearByLocation: null,
                address: null,
                city: null,
                state: null,
                landmark: null,
                alrternetPH: null
            }

            //codes for get shipping address details
            $scope.address_type = 'shipping';
            $scope.getCustomerDetailsAddress($scope.customerID, 'shipping');

            angular.element('#addressModal').modal('toggle');
        }

        $scope.addCustomerAddress = function () {
            $scope.error = null;
            $scope.modalerror = null;
            $scope.msg = null;
            $scope.modalmsg = null;

            if ($scope.addressform.name === null || $scope.addressform.name === '') {
                $scope.modalerror = true;
                $scope.modalmsg = "Name can't be blank";
            } else if ($scope.addressform.phoneno === null || $scope.addressform.phoneno === '') {
                $scope.modalerror = true;
                $scope.modalmsg = "Phone no can't be blank";
            } else if ($scope.addressform.pincode === null || $scope.addressform.pincode === '') {
                $scope.modalerror = true;
                $scope.modalmsg = "Pincode can't be blank";
            } else if ($scope.addressform.phone === null || $scope.addressform.phone === '') {
                $scope.modalerror = true;
                $scope.modalmsg = "Phono no can't be blank";
            } else if ($scope.addressform.address === null || $scope.addressform.address === '') {
                $scope.modalerror = true;
                $scope.modalmsg = "Address can't be blank";
            } else if ($scope.addressform.city === null || $scope.addressform.city === '') {
                $scope.modalerror = true;
                $scope.modalmsg = "City can't be blank";
            } else if ($scope.addressform.state === null || $scope.addressform.state === '') {
                $scope.modalerror = true;
                $scope.modalmsg = "State can't be blank";
            } else if ($scope.addressform.country === null || $scope.addressform.country === '') {
                $scope.modalerror = true;
                $scope.modalmsg = "Country can't be blank";
            }


            if ($scope.modalerror == null) {
                angular.element('#saveaddressbutton').button('loading');
                $scope.error = null;
                $scope.msg = null;
                if ($scope.addressform.address_type == 'billing') {
                    customerService.addBillingAddress(toFormData($scope.addressform)).then(function (response) {
                        if (response.data.error) {
                            $scope.modalerror = true;
                            $scope.modalmsg = response.data.msg;
                        } else {
                            $scope.modalerror = false;
                            $scope.modalmsg = response.data.msg;
                            $scope.getCustomerDetailsAddress($scope.customerID, 'billing');
                            $scope.getCustomerDetails();
                        }
                        angular.element('#saveaddressbutton').button('reset');
                    }).catch(function (e) {
                        $scope.modalerror = true;
                        $scope.modalmsg = e.statusText;
                    });
                }

                if ($scope.addressform.address_type == 'shipping') {
                    customerService.addShippingAddress(toFormData($scope.addressform)).then(function (response) {
                        if (response.data.error) {
                            $scope.modalerror = true;
                            $scope.modalmsg = response.data.msg;
                        } else {
                            $scope.modalerror = false;
                            $scope.modalmsg = response.data.msg;
                            $scope.getCustomerDetailsAddress($scope.customerID, 'shipping');
                            $scope.getCustomerDetails();
                        }
                        angular.element('#saveaddressbutton').button('reset');
                    }).catch(function (e) {
                        $scope.modalerror = true;
                        $scope.modalmsg = e.statusText;
                    });
                }
            }
        }

        $scope.makeDefault = function (data) {
            $scope.error = null;
            $scope.modalerror = null;
            $scope.msg = null;
            $scope.modalmsg = null;
            if ($scope.address_type == 'billing') {
                customerService.setDefaultBillingAddress({customer_id: data.customer_id, id: data.id}).then(function (response) {
                    if (response.data.error) {
                        $scope.modalerror = true;
                        $scope.modalmsg = response.data.msg;
                    } else {
                        $scope.modalerror = false;
                        $scope.modalmsg = response.data.msg;
                        $scope.getCustomerDetailsAddress($scope.customerID, 'billing');
                        $scope.getCustomerDetails();
                    }
                }).catch(function (e) {
                    $scope.modalerror = true;
                    $scope.modalmsg = e.statusText;
                });
            }
            if ($scope.address_type == 'shipping') {
                customerService.setDefaultShippingAddress({customer_id: data.customer_id, id: data.id}).then(function (response) {
                    if (response.data.error) {
                        $scope.modalerror = true;
                        $scope.modalmsg = response.data.msg;
                    } else {
                        $scope.modalerror = false;
                        $scope.modalmsg = response.data.msg;
                        $scope.getCustomerDetailsAddress($scope.customerID, 'shipping');
                        $scope.getCustomerDetails();
                    }
                }).catch(function (e) {
                    $scope.modalerror = true;
                    $scope.modalmsg = e.statusText;
                });
            }
        }

        $scope.editAddress = function (data) {
            $scope.addressform = data;
        }

        $scope.DeleteAddress = function (data) {
            $scope.error = null;
            $scope.modalerror = null;
            $scope.msg = null;
            $scope.modalmsg = null;
            Alertify.confirm('Are you sure to delete this address ?').then(
                    function onOk() {
                        if ($scope.address_type == 'billing') {
                            customerService.DeleteCustomerAdress({customer_id: data.customer_id, id: data.id}).then(function (response) {
                                if (response.data.error) {
                                    $scope.modalerror = true;
                                    $scope.modalmsg = response.data.msg;
                                } else {
                                    $scope.modalerror = false;
                                    $scope.modalmsg = response.data.msg;
                                    $scope.getCustomerDetailsAddress($scope.customerID, 'billing');
                                    $scope.getCustomerDetails();
                                }
                            }).catch(function (e) {
                                $scope.modalerror = true;
                                $scope.modalmsg = e.statusText;
                            });
                        }
                        if ($scope.address_type == 'shipping') {
                            customerService.DeleteCustomerAdress({customer_id: data.customer_id, id: data.id}).then(function (response) {
                                if (response.data.error) {
                                    $scope.modalerror = true;
                                    $scope.modalmsg = response.data.msg;
                                } else {
                                    $scope.modalerror = false;
                                    $scope.modalmsg = response.data.msg;
                                    $scope.getCustomerDetailsAddress($scope.customerID, 'shipping');
                                    $scope.getCustomerDetails();
                                }
                            }).catch(function (e) {
                                $scope.modalerror = true;
                                $scope.modalmsg = e.statusText;
                            });
                        }
                    },
                    function onCancel() {
                        //console.log('you have click cancel;');
                    }
            );
        }

        $scope.getOrder = function () {
            $scope.error = null;
            $scope.modalerror = null;
            $scope.msg = null;
            $scope.modalmsg = null;
            $scope.ShowLoader = true;

            if( $scope.currentPage > 1 ){
                var Newstart = (($scope.currentPage - 1) * $scope.pageSize);
                $scope.start = Newstart;
            }

            var data = {customer_id:$scope.customerID, id: $scope.searchData.id, startdate: $scope.searchData.startdate, enddate: $scope.searchData.enddate};
            customerService.getOrder(data, $scope.start, $scope.pageSize).then(function (response) {
                if (response.error) {
                    $scope.error = true;
                    $scope.msg = response.msg;
                } else {
                    $scope.error = null;
                }
                $scope.orderData = response.data.data.orders;
                $scope.total = response.data.data.Paginations.Total;
                if ($routeParams.hasOwnProperty('page') && typeof ($routeParams.page) !== 'undefined' && $routeParams.page > 0) {
                    $scope.currentPage = $routeParams.page;
                }
                $scope.ShowLoader = false;
            }).catch(function (e) {
                $scope.error = true;
                $scope.msg = e.statusText;
                $scope.ShowLoader = false;
            });
        }

        $scope.DoCtrlPagingActOrder = function (text, page, pageSize, total) {
            var Newstart = ((page - 1) * pageSize);
            $scope.start = Newstart;
            $scope.ShowLoader = true;
            var data = {customer_id:$scope.customerID, id: $scope.searchData.id, startdate: $scope.searchData.startdata, enddate: $scope.searchData.enddate};
            customerService.getOrder(data, Newstart, $scope.pageSize).then(function (response) {
                if (response.error) {
                    $scope.error = true;
                    $scope.msg = response.msg;
                } else {
                    $scope.error = null;
                }
                $scope.orderData = response.data.data.orders;
                $scope.total = response.data.data.Paginations.Total;
                $scope.ShowLoader = false;
            }).catch(function (e) {
                $scope.error = true;
                $scope.msg = e.statusText;
                $scope.ShowLoader = false;
            });
        }

        $scope.viewOrderDetails = function (data) {
            $scope.error = null;
            $scope.modalerror = null;
            $scope.msg = null;
            $scope.modalmsg = null;

            $scope.selectedOrderID = data.id;
            customerService.getOrder({id: data.id}).then(function (response) {
                if (response.error) {
                    $scope.modalerror = true;
                    $scope.modalmsg = response.msg;
                }
                $scope.orderDetailsData = response.data.data.orders[0];
            }).catch(function (e) {
                $scope.modalerror = true;
                $scope.modalmsg = e.statusText;
            });            

            angular.element('#orderModal').modal('toggle');
        }

        $scope.addition = function (varibaleone, variabletwo) {
            var total = Math.floor(varibaleone) + Math.floor(variabletwo);
            return total;
        }

        $scope.makeProcessing =function(){
            $scope.error = null;
            $scope.modalerror = null;
            $scope.msg = null;
            $scope.modalmsg = null;
            var data = {orderids: [$scope.selectedOrderID], status: 2};
            customerService.ChangeOrderStatus(data).then(function (response) {
                if (response.data.error) {
                    $scope.modalerror = true;
                    $scope.modalmsg = response.data.msg;
                } else {
                    $scope.modalerror = false;
                }
                $scope.modalmsg = response.data.msg;  
                $scope.viewOrderDetailsCS(); 
                $scope.getOrderCS();             
            }).catch(function (e) {
                $scope.modalerror = true;
                $scope.modalmsg = e.statusText;
                $scope.ShowLoader = false;
            });
        }

        $scope.getOrderCS = function () {
            var data = {id: $scope.searchData.id, startdate: $scope.searchData.startdate, enddate: $scope.searchData.enddate};
            customerService.getOrder(data, $scope.start, $scope.pageSize).then(function (response) {
                if (response.error) {
                    $scope.error = true;
                    $scope.msg = response.msg;
                } 
                $scope.orderData = response.data.data.orders;
                $scope.total = response.data.data.Paginations.Total;
            }).catch(function (e) {
                $scope.error = true;
                $scope.msg = e.statusText;
                $scope.ShowLoader = false;
            });
        }

        $scope.viewOrderDetailsCS = function () {
            customerService.getOrderCS({id: $scope.selectedOrderID}).then(function (response) {
                if (response.error) {
                    $scope.modalerror = true;
                    $scope.modalmsg = response.msg;
                }
                $scope.orderDetailsData = response.data.data.orders[0];
            }).catch(function (e) {
                $scope.modalerror = true;
                $scope.modalmsg = e.statusText;
            });
        }

        $scope.makeShipment = function(){
            $scope.error = null;
            $scope.modalerror = null;
            $scope.msg = null;
            $scope.modalmsg = null;
            $scope.makeshipping=true;
        }

        $scope.processShipment = function(){
            $scope.makeshipping=true;
            $scope.error = null;
            $scope.modalerror = null;
            $scope.msg = null;
            $scope.modalmsg = null;
            var data = {orderids: [$scope.selectedOrderID], status: 4};
            orderService.ChangeOrderStatus(data).then(function (response) {
                if (response.data.error) {
                    $scope.modalerror = true;
                    $scope.modalmsg = response.data.msg;
                } else {
                    $scope.modalerror = false;
                }
                $scope.modalmsg = response.data.msg;  
                $scope.viewOrderDetailsCS(); 
                $scope.getOrderCS();             
            }).catch(function (e) {
                $scope.modalerror = true;
                $scope.modalmsg = e.statusText;
                $scope.ShowLoader = false;
            });
        }

        $scope.cancelOrder = function (data) {
            $scope.makeshipping=true;
            $scope.error = null;
            $scope.modalerror = null;
            $scope.msg = null;
            $scope.modalmsg = null;
            var data = {orderids: [$scope.selectedOrderID], status: 8};
            Alertify.confirm('Are you sure to cancel this Order?').then(
                    function onOk() {
                        orderService.ChangeOrderStatus(data).then(function (response) {
                            if (response.data.error) {
                                $scope.modalerror = true;
                                $scope.modalmsg = response.data.msg;
                            } else {
                                $scope.modalerror = false;
                            }
                            $scope.modalmsg = response.data.msg;  
                            $scope.viewOrderDetailsCS(); 
                            $scope.getOrderCS();             
                        }).catch(function (e) {
                            $scope.modalerror = true;
                            $scope.modalmsg = e.statusText;
                            $scope.ShowLoader = false;
                        });
                    },
                    function onCancel() {
                        //console.log('you have click cancel;');
                    }
            );
        }

        $scope.printDiv = function(printSectionId) {
            var innerContents = document.getElementById(printSectionId).innerHTML;
            var popupWinindow = window.open('', '_blank', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
            popupWinindow.document.open();
            popupWinindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="css/webkon-ecommerce.css" /></head><body onload="window.print(); window.close()">' + innerContents + '</html>');
            popupWinindow.document.close();
        }

    }]);