EcommerceApp.controller('CustomeraddressController', ['$scope', 'customerService', '$routeParams', 'Alertify', 'storeType', '$location', '$routeParams', 'isMobile', function ($scope, customerService, $routeParams, Alertify, storeType, $location, $routeParams, isMobile) {
        $scope.error = null;
        $scope.modalerror = null;
        $scope.msg = null;
        $scope.modalmsg = null;
        $scope.ShowLoader = true;
        $scope.customerID = null;
        $scope.frommode = null;
        $scope.customerDetailsAddressdata = [];
        $scope.address_type = null;
        $scope.isM = 'Desktop';
        if( isMobile.any() ) 
        {
            var ggg = isMobile.any();
            $scope.isM = 'Mobile';
            if(ggg["0"] == 'iPad'){
                $scope.isM = 'iPad';
            }        
        }

        if ($routeParams.hasOwnProperty('id') && typeof ($routeParams.id) !== 'undefined' && $routeParams.id > 0) {
            $scope.customerID = $routeParams.id;
        }
        
        if ($routeParams.hasOwnProperty('type') && typeof ($routeParams.type) !== 'undefined' && $routeParams.type !='') {
            $scope.address_type = $routeParams.type;
        }

        
        $scope.addressform = {
            id: null,
            customer_id: null,
            address_type: $scope.address_type,
            name: null,
            phoneno: null,
            pincode: null,
            nearByLocation: null,
            address: null,
            city: null,
            state: null,
            country: null,
            landbark: null,
            alrternetPH: null
        }

        $scope.getCustomerDetails = function () {
            customerService.getCustomerDetails({id: $scope.customerID}).then(function (response) {
                if (response.error) {
                    $scope.error = true;
                    $scope.msg = response.msg;
                }
                $scope.customerDetailsdata = response.data.data;
                $scope.customercreatedate = new Date($scope.customerDetailsdata.CreatedDate);
                $scope.ShowLoader = false;
            });
        }

        $scope.getCustomerDetailsAddress = function (id, type) {
            if (type == 'billing') {
                customerService.getBillingAddress(id).then(function (response) {
                    $scope.customerDetailsAddressdata = response.data.data;
                }).catch(function (e) {
                    $scope.modalerror = true;
                    $scope.modalmsg = e.statusText;
                });
            }
            if (type == 'shipping') {
                customerService.getShippingAddress(id).then(function (response) {
                    $scope.customerDetailsAddressdata = response.data.data;
                }).catch(function (e) {
                    $scope.modalerror = true;
                    $scope.modalmsg = e.statusText;
                });
            }

        }

        $scope.addbillingAddress = function () {
            $scope.error = null;
            $scope.modalerror = null;
            $scope.msg = null;
            $scope.modalmsg = null;
            
            $scope.addressform = {
                id: null,
                customer_id: $scope.customerID,
                address_type: 'billing',
                name: null,
                phoneno: null,
                pincode: null,
                nearByLocation: null,
                address: null,
                city: null,
                state: null,
                country: null,
                landmark: null,
                alrternetPH: null
            }
            $scope.address_type = 'billing';
            $scope.frommode = 'Add';
            $scope.getCustomerDetailsAddress($scope.customerID, 'billing');
        }

        $scope.addshippingAddress = function () {
            $scope.error = null;
            $scope.modalerror = null;
            $scope.msg = null;
            $scope.modalmsg = null;

            $scope.addressform = {
                id: null,
                customer_id: $scope.customerID,
                address_type: 'shipping',
                name: null,
                phoneno: null,
                poncode: null,
                nearByLocation: null,
                address: null,
                city: null,
                state: null,
                landmark: null,
                alrternetPH: null
            }
            $scope.address_type = 'shipping';
            $scope.frommode = 'Add';
            $scope.getCustomerDetailsAddress($scope.customerID, 'shipping');
        }

        $scope.addCustomerAddress = function () {
            $scope.error = null;
            $scope.modalerror = null;
            $scope.msg = null;
            $scope.modalmsg = null;

            if ($scope.addressform.name === null || $scope.addressform.name === '') {
                $scope.modalerror = true;
                $scope.modalmsg = "Name can't be blank";
            } else if ($scope.addressform.phoneno === null || $scope.addressform.phoneno === '') {
                $scope.modalerror = true;
                $scope.modalmsg = "Phone no can't be blank";
            } else if ($scope.addressform.pincode === null || $scope.addressform.pincode === '') {
                $scope.modalerror = true;
                $scope.modalmsg = "Pincode can't be blank";
            } else if ($scope.addressform.phone === null || $scope.addressform.phone === '') {
                $scope.modalerror = true;
                $scope.modalmsg = "Phono no can't be blank";
            } else if ($scope.addressform.address === null || $scope.addressform.address === '') {
                $scope.modalerror = true;
                $scope.modalmsg = "Address can't be blank";
            } else if ($scope.addressform.city === null || $scope.addressform.city === '') {
                $scope.modalerror = true;
                $scope.modalmsg = "City can't be blank";
            } else if ($scope.addressform.state === null || $scope.addressform.state === '') {
                $scope.modalerror = true;
                $scope.modalmsg = "State can't be blank";
            } else if ($scope.addressform.country === null || $scope.addressform.country === '') {
                $scope.modalerror = true;
                $scope.modalmsg = "Country can't be blank";
            }

            if ($scope.modalerror == null) {
                angular.element('#saveaddressbutton').button('loading');
                $scope.error = null;
                $scope.msg = null;
                if ($scope.addressform.address_type == 'billing') {
                    customerService.addBillingAddress(toFormData($scope.addressform)).then(function (response) {
                        if (response.data.error) {
                            $scope.modalerror = true;
                            $scope.modalmsg = response.data.msg;
                        } else {
                            $scope.modalerror = false;
                            $scope.modalmsg = response.data.msg;
                            $scope.getCustomerDetailsAddress($scope.customerID, 'billing');
                            $scope.getCustomerDetails();
                        }
                        angular.element('#saveaddressbutton').button('reset');
                    }).catch(function (e) {
                        $scope.modalerror = true;
                        $scope.modalmsg = e.statusText;
                    });
                }

                if ($scope.addressform.address_type == 'shipping') {
                    customerService.addShippingAddress(toFormData($scope.addressform)).then(function (response) {
                        if (response.data.error) {
                            $scope.modalerror = true;
                            $scope.modalmsg = response.data.msg;
                        } else {
                            $scope.modalerror = false;
                            $scope.modalmsg = response.data.msg;
                            $scope.getCustomerDetailsAddress($scope.customerID, 'shipping');
                            $scope.getCustomerDetails();
                        }
                        angular.element('#saveaddressbutton').button('reset');
                    }).catch(function (e) {
                        $scope.modalerror = true;
                        $scope.modalmsg = e.statusText;
                    });
                }
                var trgt = angular.element('.container-fluid').eq(0);
                $('html,body').animate({scrollTop: trgt.offset().top}, 1000);
            }
        }

        $scope.makeDefault = function (data) {
            $scope.error = null;
            $scope.modalerror = null;
            $scope.msg = null;
            $scope.modalmsg = null;
            if ($scope.address_type == 'billing') {
                customerService.setDefaultBillingAddress({customer_id: data.customer_id, id: data.id}).then(function (response) {
                    if (response.data.error) {
                        $scope.modalerror = true;
                        $scope.modalmsg = response.data.msg;
                    } else {
                        $scope.modalerror = false;
                        $scope.modalmsg = response.data.msg;
                        $scope.getCustomerDetailsAddress($scope.customerID, 'billing');
                        $scope.getCustomerDetails();
                    }
                }).catch(function (e) {
                    $scope.modalerror = true;
                    $scope.modalmsg = e.statusText;
                });
            }
            if ($scope.address_type == 'shipping') {
                customerService.setDefaultShippingAddress({customer_id: data.customer_id, id: data.id}).then(function (response) {
                    if (response.data.error) {
                        $scope.modalerror = true;
                        $scope.modalmsg = response.data.msg;
                    } else {
                        $scope.modalerror = false;
                        $scope.modalmsg = response.data.msg;
                        $scope.getCustomerDetailsAddress($scope.customerID, 'shipping');
                        $scope.getCustomerDetails();
                    }
                }).catch(function (e) {
                    $scope.modalerror = true;
                    $scope.modalmsg = e.statusText;
                });
            }
            var trgt = angular.element('.container-fluid').eq(0);
            $('html,body').animate({scrollTop: trgt.offset().top}, 1000);
        }

        $scope.editAddress = function (data) {
            $scope.addressform = data;
            $scope.frommode = 'Edit';
        }

        $scope.DeleteAddress = function (data) {
            $scope.error = null;
            $scope.modalerror = null;
            $scope.msg = null;
            $scope.modalmsg = null;
            Alertify.confirm('Are you sure to delete this address ?').then(
                    function onOk() {
                        if ($scope.address_type == 'billing') {
                            customerService.DeleteCustomerAdress({customer_id: data.customer_id, id: data.id}).then(function (response) {
                                if (response.data.error) {
                                    $scope.modalerror = true;
                                    $scope.modalmsg = response.data.msg;
                                } else {
                                    $scope.modalerror = false;
                                    $scope.modalmsg = response.data.msg;
                                    $scope.getCustomerDetailsAddress($scope.customerID, 'billing');
                                    $scope.getCustomerDetails();
                                }
                            }).catch(function (e) {
                                $scope.modalerror = true;
                                $scope.modalmsg = e.statusText;
                            });
                        }
                        if ($scope.address_type == 'shipping') {
                            customerService.DeleteCustomerAdress({customer_id: data.customer_id, id: data.id}).then(function (response) {
                                if (response.data.error) {
                                    $scope.modalerror = true;
                                    $scope.modalmsg = response.data.msg;
                                } else {
                                    $scope.modalerror = false;
                                    $scope.modalmsg = response.data.msg;
                                    $scope.getCustomerDetailsAddress($scope.customerID, 'shipping');
                                    $scope.getCustomerDetails();
                                }
                            }).catch(function (e) {
                                $scope.modalerror = true;
                                $scope.modalmsg = e.statusText;
                            });
                        }
                        var trgt = angular.element('.container-fluid').eq(0);
                        $('html,body').animate({scrollTop: trgt.offset().top}, 1000);
                    },
                    function onCancel() {
                        //console.log('you have click cancel;');
                    }
            );
        }

        
        if($scope.customerID){
            if($scope.address_type == 'billing'){
                $scope.addbillingAddress();
            }
            if($scope.address_type == 'shipping'){
                $scope.addshippingAddress();
            }
        }

        $scope.AddNewaddress= function(){
            if($scope.address_type == 'billing'){
                $scope.addbillingAddress();
            }
            if($scope.address_type == 'shipping'){
                $scope.addshippingAddress();
            }
        }
        

    }]);