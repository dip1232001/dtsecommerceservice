EcommerceApp.controller('EditcustomerController', ['$scope', 'customerService', '$routeParams', 'Alertify', 'storeType', '$location', '$routeParams', 'isMobile', function ($scope, customerService, $routeParams, Alertify, storeType, $location, $routeParams, isMobile) {
    $scope.error = null;
    $scope.modalerror = null;
    $scope.msg = null;
    $scope.modalmsg = null;
    $scope.ShowLoader = true;
    $scope.customerID = null;
    $scope.frommode = null;
    $scope.isM = 'Desktop';
    if( isMobile.any() ) 
    {
        var ggg = isMobile.any();
        $scope.isM = 'Mobile';
        if(ggg["0"] == 'iPad'){
            $scope.isM = 'iPad';
        }        
    }

    $scope.customerdata = [];
    $scope.customerDetailsdata = [];

    $scope.customerform = {
        id: null,
        firstname: null,
        lastname: null,
        email: null,
        phone: null,
        password: null,
        confirm_password: null,
        dob: null,
        marriageAnniversaryDate: null,
        Gender: 0
    }

    $scope.AddNewCustomer = function () {
        $scope.customerform = {
            id: null,
            firstname: null,
            lastname: null,
            email: null,
            phone: null,
            password: null,
            confirm_password: null,
            dob: null,
            marriageAnniversaryDate: null,
            Gender: 0
        }
        $scope.msg = null;
        $scope.error = null;
        $scope.modalmsg = null;
        $scope.frommode = 'Add';
    }

    $scope.editCustomer = function (data) {
        $scope.customerform = {
            id: data.id,
            firstname: data.FirstName,
            lastname: data.Lastname,
            email: data.Email,
            phone: data.PhoneNumber,
            password: null,
            confirm_password: null,
            dob: data.DOBformated,
            marriageAnniversaryDate: data.MADformated,
            Gender: '' + data.Gender
        }
        $scope.msg = null;
        $scope.error = null;
        $scope.modalmsg = null;
        $scope.modalerror = null;
        $scope.frommode = 'Edit';
    }

    $scope.addCustomer = function () {
        $scope.error = null;
        $scope.modalerror = null;
        $scope.modalmsg = null;
        $scope.msg = null;

        if ($scope.customerform.firstname === null || $scope.customerform.firstname === '') {
            $scope.modalerror = true;
            $scope.modalmsg = "First name can't be blank";
        } else if ($scope.customerform.lastname === null || $scope.customerform.lastname === '') {
            $scope.modalerror = true;
            $scope.modalmsg = "Last name can't be blank";
        } else if ($scope.customerform.email === null || $scope.customerform.email === '') {
            $scope.modalerror = true;
            $scope.modalmsg = "Email can't be blank";
        } else if ($scope.customerform.phone === null || $scope.customerform.phone === '') {
            $scope.modalerror = true;
            $scope.modalmsg = "Phono no can't be blank";
        } else if ($scope.customerform.id == null && ($scope.customerform.password === null || $scope.customerform.password === '')) {
            $scope.modalerror = true;
            $scope.modalmsg = "password can't be blank";
        } else if ($scope.customerform.id == null && ($scope.customerform.confirm_password === null || $scope.customerform.confirm_password === '')) {
            $scope.modalerror = true;
            $scope.modalmsg = "Confirm password can't be blank";
        } else if ($scope.customerform.password != $scope.customerform.confirm_password) {
            $scope.modalerror = true;
            $scope.modalmsg = "Password and confiorm password does not matched !!";
        }
        if ($scope.modalerror == null) {
            angular.element('#savecustomerbutton').button('loading');
            $scope.error = null;
            $scope.modalerror = null;
            $scope.msg = null;
            $scope.modalmsg = null;
            customerService.createCustomer(toFormData($scope.customerform)).then(function (response) {
                if (response.data.error) {
                    $scope.modalerror = true;
                    $scope.modalmsg = response.data.msg;
                } else {
                    $scope.modalerror = false;
                    $scope.modalmsg = response.data.msg;
                    if ($scope.frommode == 'Edit') {
                        $scope.modalmsg = 'Customer Edited succesfully';
                    }
                }
                angular.element('#savecustomerbutton').button('reset');
            });
            var trgt = angular.element('.container-fluid').eq(0);
            $('html,body').animate({
                scrollTop: trgt.offset().top
            }, 1000);
        }
    }



    $scope.getCustomerDetails = function () {
        customerService.getCustomerDetails({
            id: $scope.customerID
        }).then(function (response) {
            if (response.error) {
                $scope.modalerror = true;
                $scope.modalmsg = response.msg;
            }
            $scope.customerDetailsdata = response.data.data;
            $scope.editCustomer($scope.customerDetailsdata);
        });
    }

    if ($routeParams.hasOwnProperty('id') && typeof ($routeParams.id) !== 'undefined' && $routeParams.id > 0) {
        $scope.customerID = $routeParams.id;
    }

    if ($scope.customerID) {
        $scope.getCustomerDetails();

    } else {
        $scope.AddNewCustomer();
    }



}]);