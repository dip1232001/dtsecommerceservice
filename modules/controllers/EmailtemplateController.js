EcommerceApp.controller('EmailtemplateController', ['$scope', 'emailtemplateService', '$routeParams', 'Alertify', 'isMobile', function($scope, emailtemplateService, $routeParams, Alertify, isMobile) {

        $scope.error = null;
        $scope.modalerror = null;
        $scope.msg = null;
        $scope.modalmsg = null;

        $scope.emaildata = [];
        $scope.emailDetailsdata = [];
        $scope.ShowLoader = true;

        $scope.editForm = false;
        $scope.email_type = null;
        $scope.isM = 'Desktop';
        if( isMobile.any() ) 
        {
            var ggg = isMobile.any();
            $scope.isM = 'Mobile';
            if(ggg["0"] == 'iPad'){
                $scope.isM = 'iPad';
            }        
        }
        
        $scope.getEmailList = function() {
            emailtemplateService.getEmailList().then(function(response) {
                $scope.msg = response.data.msg;
                if (response.data.error) {
                    $scope.error = true;
                }
                $scope.emaildata = response.data.data;
                $scope.ShowLoader = false;
                console.log($scope.emaildata);

            }).catch(function(e){
                $scope.error = true;
                $scope.msg = e.statusText;
                $scope.ShowLoader = false;
            });
        };

        $scope.getEmailDetails = function() {
            emailtemplateService.getEmailDetails({email_type:$scope.email_type}).then(function(response) {
                $scope.msg = response.data.msg;
                if (response.data.error) {
                    $scope.error = true;
                }
                $scope.emailDetailsdata = response.data.data;
                $scope.ShowLoader = false;
                console.log($scope.emailDetailsdata);

            }).catch(function(e){
                $scope.error = true;
                $scope.msg = e.statusText;
                $scope.ShowLoader = false;
            });
        };

        $scope.editTemplate = function(data){
            $scope.editForm = true;
            $scope.email_type = data.email_type;
            $scope.getEmailDetails();
        }

        $scope.saveEmail = function(){
            $scope.editForm = false;
            $scope.email_type = null;
            $scope.getEmailList();
        }

    }]);

