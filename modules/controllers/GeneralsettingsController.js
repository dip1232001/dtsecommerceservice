EcommerceApp.controller('GeneralsettingsController', ['$scope', 'generalsettingsService', 'storeType', '$routeParams', 'Alertify', 'isMobile', function ($scope, generalsettingsService, storeType, $routeParams, Alertify, isMobile) {

    $scope.error = null;
    $scope.modalerror = null;
    $scope.msg = null;
    $scope.modalmsg = null;
    $scope.isM = 'Desktop';
    if( isMobile.any() ) 
    {
        var ggg = isMobile.any();
        $scope.isM = 'Mobile';
        if(ggg["0"] == 'iPad'){
            $scope.isM = 'iPad';
        }        
    }

    $scope.generalsettingImage = false;

    $scope.selectedTab = 'GeneralSetting';
    $scope.selectedSubTabRules = 'Tax';

    $scope.currencylists = [];

    $scope.showPaymentForm = [];
    $scope.showPaymentFormData = [];
    $scope.checkPayment = [];
    $scope.couponTypes = [];
    $scope.specialCouponTypes = [];
    $scope.ShowSpecialCoupon = true;
    $scope.GS = [];
    $scope.storeType = storeType;
    $scope.exhemp = false;

    /*$scope.paypalExpressEnable = false;
     $scope.paypalCCEnable = false;
     $scope.payCCTelephone = false;
     $scope.payStripe = false;*/

    $scope.generalsettingsform = {
        id: null,
        name: null,
        email: null,
        currency: null,
        Images: null,
        Imges_url: false,
        address: null,
        vatNo: null,
        isVatNoViewInInvoice: 0,
        phoneNo: null
    };

    $scope.generalImpsettings = {
        couponEnable: 0,
        deliveryEnable: 0,
        dieinEnable: 0,
        pickupEnable: 0,
        preorderEnable: 0
    };

    $scope.generalImpsettingsE = {
        couponEnable: false,
        deliveryEnable: true,
        dieinEnable: false,
        pickupEnable: false,
        preorderEnable: false,
        GuestdeliveryEnable: true,
        GuestdieinEnable: false,
        GuestpickupEnable: false,
        GuestpreorderEnable: false
    };

    $scope.allowedRuletypes = [
        {
            'name': '>'
        },
        {
            'name': '>='
        },
        {
            'name': '='
        },
        {
            'name': '<='
        },
        {
            'name': '<'
        }
    ];

    $scope.allowedRatetypes = [
        {
            'name': 'Flat'
        },
        {
            'name': 'Percentage'
        }
    ];

    $scope.allowedislive = [
        {
            'name': 'Live',
            'id': 1
        },
        {
            'name': 'Stage',
            'id': 0
        }
    ];

    $scope.rulesFormType = null;
    $scope.rulesForm = {
        id: null,
        rule_type: null,
        amount: null,
        price: null,
        rate_type: null
    };

    $scope.taxRuleId = null;
    $scope.taxRuleData = [];
    $scope.singletaxRuleData = [];
    $scope.paymentTypes = [];
    $scope.payments = [];
    $scope.addedPaymentTypes = [];

    $scope.couponFormType = null;
    $scope.couponForm = {
        id: null,
        name: null,
        code: null,
        date_of_issue: null,
        expiry_date: null,
        type: null,
        scoupon_type: null,
        discount_type: null,
        discount: null,
        select_rule: null,
        discount_on_price: null,
        specialType: null,
        status: 1,
        maximumDiscount: null,
        useTotalOrderValue: null,
        useTotalNoOfOrder: null,
        description: null
    };
    $scope.couponId = null;
    $scope.coupons = [];

    $scope.outletFormType = null;
    $scope.Outlets = [];
    $scope.OutletForm = {
        id: null,
        name: null,
        status: null,
    };

    $scope.preOrderSettings = {
        id: null,
        monsh: 0,
        monsm: 0,
        moneh: 0,
        monem: 0,
        ismonoffday: '0',
        tuesh: 0,
        tuesm: 0,
        tueeh: 0,
        tueem: 0,
        istueoffday: '0',
        wedsh: 0,
        wedsm: 0,
        wedeh: 0,
        wedem: 0,
        iswedoffday: '0',
        thush: 0,
        thusm: 0,
        thueh: 0,
        thuem: 0,
        isthuoffday: '0',
        frish: 0,
        frism: 0,
        frieh: 0,
        friem: 0,
        isfrioffday: '0',
        satsh: 0,
        satsm: 0,
        sateh: 0,
        satem: 0,
        issatoffday: '0',
        sunsh: 0,
        sunsm: 0,
        suneh: 0,
        sunem: 0,
        issunfoffday: '0',
        defaulyreservationlength: 0,
        reservexhrsearlier: 0
    }

    $scope.preOrderTables = [];
    $scope.preorderTableFormType = null;
    $scope.preOrderTableForm = {
        id: null,
        name: null,
        capacity: 1,
        status: 1
    };


    $scope.getCurrencyLists = function () {
        generalsettingsService.getCurrencyLists().then(function (response) {
            if (response.error) {
                $scope.error = true;
                $scope.msg = response.msg;
            }
            $scope.currencylists = response.data.data;
        });
    };

    $scope.getGeneralSettingsS = function () {
        generalsettingsService.getGeneralSettings().then(function (response) {
            if (response.error) {
                $scope.error = true;
                $scope.msg = response.msg;
            } else {
                $scope.GS = response.data.data;
                console.log("$scope.GS===>", $scope.GS);
                $scope.generalsettingsform = {
                    id: response.data.data.id,
                    name: response.data.data.store_name,
                    email: response.data.data.store_email,
                    currency: response.data.data.currency_code,
                    Imges_url: response.data.data.default_image,
                    address: response.data.data.address,
                    vatNo: response.data.data.vatNo,
                    isVatNoViewInInvoice: response.data.data.isVatNoViewInInvoice,
                    phoneNo: response.data.data.phoneNo
                };

                if (response.data.data.couponEnable == 1) {
                    $scope.generalImpsettingsE.couponEnable = true;
                } else {
                    $scope.generalImpsettingsE.couponEnable = false;
                }

                if (response.data.data.deliveryEnable == 1) {
                    $scope.generalImpsettingsE.deliveryEnable = true;
                } else {
                    $scope.generalImpsettingsE.deliveryEnable = false;
                }
                if (response.data.data.dineinEnable == 1) {
                    $scope.generalImpsettingsE.dieinEnable = true;
                } else {
                    $scope.generalImpsettingsE.dieinEnable = false;
                }
                if (response.data.data.pickupEnable == 1) {
                    $scope.generalImpsettingsE.pickupEnable = true;
                } else {
                    $scope.generalImpsettingsE.pickupEnable = false;
                }
                if (response.data.data.preorderEnable == 1) {
                    $scope.generalImpsettingsE.preorderEnable = true;
                } else {
                    $scope.generalImpsettingsE.preorderEnable = false;
                }

                if (response.data.data.GuestdeliveryEnable == 1) {
                    $scope.generalImpsettingsE.GuestdeliveryEnable = true;
                } else {
                    $scope.generalImpsettingsE.GuestdeliveryEnable = false;
                }
                if (response.data.data.GuestdineinEnable == 1) {
                    $scope.generalImpsettingsE.GuestdieinEnable = true;
                } else {
                    $scope.generalImpsettingsE.GuestdieinEnable = false;
                }
                if (response.data.data.GuestpickupEnable == 1) {
                    $scope.generalImpsettingsE.GuestpickupEnable = true;
                } else {
                    $scope.generalImpsettingsE.GuestpickupEnable = false;
                }
                if (response.data.data.GuestpreorderEnable == 1) {
                    $scope.generalImpsettingsE.GuestpreorderEnable = true;
                } else {
                    $scope.generalImpsettingsE.GuestpreorderEnable = false;
                }

                /*$scope.generalImpsettings = {
                    couponEnable = response.data.data.couponEnable,
                    deliveryEnable = response.data.data.deliveryEnable,
                    dieinEnable = response.data.data.dieinEnable,
                    pickupEnable = response.data.data.pickupEnable,
                    preorderEnable = response.data.data.preorderEnable
                };*/
            }
        });
    };

    $scope.remove_image = function () {
        $scope.generalsettingImage = false;
        $scope.generalsettingsform.Imges_url = false;

    };

    $scope.saveGeneralSettings = function () {
        $scope.error = null;
        $scope.modalerror = null;
        $scope.msg = null;
        $scope.modalmsg = null;

        if ($scope.generalsettingsform.Name === null || $scope.generalsettingsform.Name === '') {
            $scope.error = true;
            $scope.msg = "Name can't be blank";
        } else if ($scope.generalsettingsform.email === null || $scope.generalsettingsform.email === '') {
            $scope.error = true;
            $scope.msg = "Email can't be blank";
        } else if ($scope.generalsettingsform.phoneNo === null || $scope.generalsettingsform.phoneNo === '') {
            $scope.error = true;
            $scope.msg = "Phone No can't be blank";
        } else if ($scope.generalsettingsform.address === null || $scope.generalsettingsform.address === '') {
            $scope.error = true;
            $scope.msg = "Address can't be blank";
        }

        if ($scope.error == null) {
            angular.element('#generalsettingsSaveButton').button('loading');
            $scope.error = null;
            $scope.msg = null;
            $scope.generalsettingsform.Images = ($scope.generalsettingImage == false) ? null : $scope.generalsettingImage;
            generalsettingsService.saveGeneralSettings(toFormData($scope.generalsettingsform)).then(function (response) {
                if (response.data.error) {
                    $scope.error = true;
                    $scope.msg = response.data.msg;
                } else {
                    $scope.error = false;
                    $scope.getGeneralSettingsS();
                    $scope.msg = response.data.msg;
                }
                angular.element('#generalsettingsSaveButton').button('reset');
            });

        }
    }

    $scope.saveGeneralImpSettings = function () {
        $scope.error = null;
        $scope.modalerror = null;
        $scope.msg = null;
        $scope.modalmsg = null;
        //coupone
        if ($scope.generalImpsettingsE.couponEnable) {
            $scope.generalImpsettings.couponEnable = 1;
        } else {
            $scope.generalImpsettings.couponEnable = 0;
        }
        //delivery
        if ($scope.generalImpsettingsE.deliveryEnable) {
            $scope.generalImpsettings.deliveryEnable = 1;
        } else {
            $scope.generalImpsettings.deliveryEnable = 0;
        }

        if ($scope.generalImpsettingsE.dieinEnable) {
            $scope.generalImpsettings.dieinEnable = 1;
        } else {
            $scope.generalImpsettings.dieinEnable = 0;
        }

        if ($scope.generalImpsettingsE.pickupEnable) {
            $scope.generalImpsettings.pickupEnable = 1;
        } else {
            $scope.generalImpsettings.pickupEnable = 0;
        }

        if ($scope.generalImpsettingsE.preorderEnable) {
            $scope.generalImpsettings.preorderEnable = 1;
        } else {
            $scope.generalImpsettings.preorderEnable = 0;
        }
        //guest
        if ($scope.generalImpsettingsE.GuestdeliveryEnable) {
            $scope.generalImpsettings.GuestdeliveryEnable = 1;
        } else {
            $scope.generalImpsettings.GuestdeliveryEnable = 0;
        }

        if ($scope.generalImpsettingsE.GuestdieinEnable) {
            $scope.generalImpsettings.GuestdieinEnable = 1;
        } else {
            $scope.generalImpsettings.GuestdieinEnable = 0;
        }

        if ($scope.generalImpsettingsE.GuestpickupEnable) {
            $scope.generalImpsettings.GuestpickupEnable = 1;
        } else {
            $scope.generalImpsettings.GuestpickupEnable = 0;
        }

        if ($scope.generalImpsettingsE.GuestpreorderEnable) {
            $scope.generalImpsettings.GuestpreorderEnable = 1;
        } else {
            $scope.generalImpsettings.GuestpreorderEnable = 0;
        }



        if ($scope.error == null) {
            angular.element('#generalImpsettingsSaveButton').button('loading');
            $scope.error = null;
            $scope.msg = null;
            generalsettingsService.saveGeneralImpSettings(toFormData($scope.generalImpsettings)).then(function (response) {
                if (response.data.error) {
                    $scope.error = true;
                    $scope.msg = response.data.msg;
                } else {
                    $scope.error = false;
                    $scope.getGeneralSettingsS();
                    $scope.msg = response.data.msg;
                }
                angular.element('#generalImpsettingsSaveButton').button('reset');
            });

        }
    }

    $scope.selectTab = function (id) {
        $scope.selectedTab = id;
    }

    $scope.selectSubTabRules = function (id) {
        $scope.selectedSubTabRules = id;
    }

    $scope.addTaxRule = function () {
        $scope.error = null;
        $scope.modalerror = null;
        $scope.msg = null;
        $scope.modalmsg = null;

        $scope.rulesForm = {
            id: null,
            rule_type: null,
            amount: null,
            price: null,
            rate_type: null
        }
        $scope.rulesFormType = 'Add';
        angular.element('#ruleModal').modal('toggle');
    }

    $scope.editTaxRule = function (data) {
        $scope.error = null;
        $scope.modalerror = null;
        $scope.msg = null;
        $scope.modalmsg = null;

        $scope.taxRuleId = data.id;
        generalsettingsService.getTaxRule({
            id: $scope.taxRuleId
        }).then(function (response) {
            if (response.data.error) {
                $scope.error = true;
                $scope.msg = response.data.msg;
            } else {
                $scope.error = null;
                $scope.msg = null;
            }
            $scope.singletaxRuleData = response.data.data[0];
            $scope.rulesForm = {
                id: data.id,
                rule_type: $scope.singletaxRuleData.ruletype.replace(/ /g, ''),
                amount: $scope.singletaxRuleData.amount,
                price: $scope.singletaxRuleData.price,
                rate_type: $scope.singletaxRuleData.selecttype
            }
        }).catch(function (e) {
            $scope.error = true;
            $scope.msg = e.statusText;
        });

        $scope.rulesFormType = 'Edit';
        angular.element('#ruleModal').modal('toggle');
    }

    $scope.getTaxRule = function () {
        generalsettingsService.getTaxRule({
            id: $scope.taxRuleId
        }).then(function (response) {
            if (response.data.error) {
                $scope.error = true;
                $scope.msg = response.data.msg;
            }
            $scope.taxRuleData = response.data;
        }).catch(function (e) {
            $scope.error = true;
            $scope.msg = e.statusText;
        });
    }

    $scope.getTaxRule();

    $scope.saveTaxRule = function () {
        $scope.error = null;
        $scope.modalerror = null;
        $scope.msg = null;
        $scope.modalmsg = null;

        if ($scope.rulesForm.rate_type === null) {
            $scope.modalerror = true;
            $scope.modalmsg = "Rate can't be blank";
        }
        if ($scope.rulesForm.price === null) {
            $scope.modalerror = true;
            $scope.modalmsg = "Price can't be blank";
        }
        if ($scope.rulesForm.rule_type === null) {
            $scope.modalerror = true;
            $scope.modalmsg = "Rule can't be blank";
        }
        if ($scope.rulesForm.amount === null) {
            $scope.modalerror = true;
            $scope.modalmsg = "Sub total can't be blank";
        }
        //console.log($scope.rulesForm);
        for (var i = 0; i < $scope.taxRuleData.data.length; i++) {
            if ($scope.rulesForm.id != 'null') {
                if ($scope.taxRuleData.data[i].id != $scope.rulesForm.id) {
                    if (Math.round($scope.taxRuleData.data[i].amount) == Math.round($scope.rulesForm.amount)) {
                        $scope.modalerror = true;
                        $scope.modalmsg = "Rule with these amount already exists.";
                    }
                }
            } else {
                if (Math.round($scope.taxRuleData.data[i].amount) == Math.round($scope.rulesForm.amount)) {
                    $scope.modalerror = true;
                    $scope.modalmsg = "Rule with these amount already exists.";
                }
            }
        }
        if ($scope.modalerror == null) {
            //angular.element('#taxaddbutton').button('loading');
            generalsettingsService.saveTaxRule($scope.rulesForm).then(function (response) {
                if (response.data.error) {
                    $scope.modalerror = true;
                    $scope.modalmsg = response.data.msg;
                } else {
                    $scope.modalerror = false;
                    $scope.taxRuleId = '';
                    $scope.getTaxRule();
                    $scope.modalmsg = response.data.msg;
                    $scope.rulesForm = {
                        id: null,
                        rule_type: null,
                        amount: null,
                        price: null,
                        rate_type: null
                    }
                }
                //angular.element('#taxaddbutton').button('reset');
            }).catch(function (e) {
                $scope.modalerror = true;
                $scope.modalmsg = e.statusText;
            });
        }
    }

    $scope.makeInactiveTax = function (tax, status) {
        $scope.error = null;
        $scope.modalerror = null;
        $scope.msg = null;
        $scope.modalmsg = null;
        if (status == 0) {
            var stat = 'inactive';
        } else {
            var stat = 'active';
        }

        Alertify.confirm('Are you sure to make this tax rule ' + stat + '?').then(
            function onOk() {
                if (typeof (status) === "undefined") {
                    status = 1;
                }
                generalsettingsService.ChangeTaxStatus({
                    id: tax.id,
                    status: status
                }).then(function (response) {
                    $scope.msg = response.data.msg;
                    if (response.data.error) {
                        $scope.error = true;
                    } else {
                        tax.is_active = status;
                        $scope.error = false;
                    }
                }).catch(function (e) {
                    $scope.error = true;
                    $scope.msg = e.statusText;
                });
            },
            function onCancel() {
                //console.log('you have click cancel;');
            }
        );
    }

    $scope.DeleteTaxes = function (data) {
        $scope.error = null;
        $scope.modalerror = null;
        $scope.msg = null;
        $scope.modalmsg = null;
        console.log(data);
        Alertify.confirm('Are you sure to delete this tax rule?').then(
            function onOk() {
                console.log(data);
                generalsettingsService.DeleteTaxes({
                    id: data.id
                }).then(function (response) {
                    $scope.msg = response.data.msg;
                    if (response.error) {
                        $scope.error = true;
                        $scope.msg = response.msg;
                    } else {
                        $scope.error = false;
                        $scope.getTaxRule();
                        $scope.msg = 'Tax Rule deleted successfully.';
                    }
                }).catch(function (e) {
                    $scope.error = true;
                    $scope.msg = e.statusText;
                });
            },
            function onCancel() {
                //console.log('you have click cancel;');
            }
        );
    }

    $scope.getPaymentTypes = function () {
        generalsettingsService.getPaymentTypes().then(function (response) {
            if (response.error) {
                $scope.error = true;
                $scope.msg = response.msg;
            }
            $scope.paymentTypes = response.data.data;
        });
    };

    $scope.GetCouponTypes = function () {
        generalsettingsService.GetCouponTypes().then(function (response) {
            if (response.error) {
                $scope.error = true;
                $scope.msg = response.msg;
            }
            $scope.couponTypes = response.data.data;
        });
    };

    $scope.GetSpecialCouponTypes = function () {
        generalsettingsService.GetSpecialCouponTypes().then(function (response) {
            if (response.error) {
                $scope.error = true;
                $scope.msg = response.msg;
            }
            $scope.specialCouponTypes = response.data.data;
        });
    };

    $scope.getAddedPaymentTypes = function () {
        generalsettingsService.getAddedPaymentTypes().then(function (response) {
            console.log("getAddedPaymentTypes", response);
            if (response.error) {
                $scope.error = true;
                $scope.msg = response.msg;
            }
            $scope.addedPaymentTypes = response.data.data;
            //$scope.addedPaymentTypes = [];
            $scope.showPaymentForm = [];
            $scope.checkPayment = [];
            for (var i = 0; i < $scope.payments.length; i++) {
                $scope.showPaymentFormData[i] = {
                    'PAYMENTYPE': $scope.payments[i].id,
                    'CLIENTKEY': '',
                    'CLIENTSECRET': '',
                    'ISLIVE': 0
                };
                for (var j = 0; j < $scope.addedPaymentTypes.length; j++) {
                    if ($scope.payments[i].id == $scope.addedPaymentTypes[j].paymentmethodid) {
                        $scope.checkPayment[$scope.payments[i].id] = true;
                        $scope.showPaymentForm[i] = $scope.addedPaymentTypes[j].is_active;
                        $scope.showPaymentFormData[i] = {
                            'isChecked': $scope.addedPaymentTypes[j].is_active,
                            'ISACTIVE': $scope.addedPaymentTypes[j].is_active,
                            'PAYMENTYPE': $scope.payments[i].id,
                            'id': $scope.addedPaymentTypes[j].id,
                            'CLIENTKEY': $scope.addedPaymentTypes[j].clientkey,
                            'CLIENTSECRET': $scope.addedPaymentTypes[j].clientsecret,
                            'ISLIVE': '' + $scope.addedPaymentTypes[j].is_live
                        };
                    }
                }
            }
        });
    };

    $scope.getAllPayments = function () {
        generalsettingsService.getAllPayments().then(function (response) {
            console.log(response);
            if (response.error) {
                $scope.error = true;
                $scope.msg = response.msg;
            } else {
                $scope.payments = response.data.data;
                $scope.getAddedPaymentTypes();
            }
        });
    };

    $scope.changeCoponType = function () {
        let selectCoupon = $scope.couponTypes.filter(p => p.id == $scope.couponForm.type);
        console.log("selectCoupon", selectCoupon);
        if (selectCoupon !== undefined && selectCoupon !== null) {
            if (selectCoupon[0].typekey === 'special') {
                $scope.ShowSpecialCoupon = false;
                $scope.ShowreturningCoupon = true;
            } else if (selectCoupon[0].typekey === 'returning') {
                $scope.ShowreturningCoupon = false;
                $scope.ShowSpecialCoupon = true;
            } else {
                $scope.ShowreturningCoupon = true;
                $scope.ShowSpecialCoupon = true;
            }

        } else {
            $scope.ShowSpecialCoupon = true;
            $scope.ShowreturningCoupon = true;
        }

    }

    $scope.enablePaymentForm = function (id, payment, formData) {
        //$scope.showPaymentForm = [];
        var pid = null;
        for (var i = 0; i < $scope.addedPaymentTypes.length; i++) {
            if ($scope.addedPaymentTypes[i].paymentmethodid == payment.id) {
                pid = $scope.addedPaymentTypes[i].id;
            }
        }
        if ($scope.checkPayment[payment.id]) {
            $scope.showPaymentForm[id] = true;
            var parray = {
                'isChecked': true,
                'ISACTIVE': 1,
                'ISLIVE': formData.ISLIVE,
                'PAYMENTYPE': payment.id,
                'id': pid,
                'CLIENTKEY': (typeof (formData) !== "undefined" && formData.hasOwnProperty("CLIENTKEY") ? formData.CLIENTKEY : null),
                'CLIENTSECRET': (typeof (formData) !== "undefined" && formData.hasOwnProperty("CLIENTSECRET") ? formData.CLIENTSECRET : null)
            };
            $scope.showPaymentFormData[id] = parray;
        } else {
            $scope.showPaymentForm[id] = false;
            var parray = {
                'isChecked': false,
                'ISACTIVE': 0,
                'ISLIVE': formData.ISLIVE,
                'PAYMENTYPE': payment.id,
                'id': pid,
                'CLIENTKEY': (typeof (formData) !== "undefined" && formData.hasOwnProperty("CLIENTKEY") ? formData.CLIENTKEY : null),
                'CLIENTSECRET': (typeof (formData) !== "undefined" && formData.hasOwnProperty("CLIENTSECRET") ? formData.CLIENTSECRET : null)
            };
            $scope.showPaymentFormData[id] = parray;
        }
    }

    $scope.paymentLength = 0;

    $scope.savePayments = function () {
        var data = null;
        //for (var i = 0; i < $scope.showPaymentFormData.length; i++) {
        //if ($scope.showPaymentFormData[0].isChecked == true && $scope.showPaymentForm[0] == true) {
        data = {
            CLIENTKEY: $scope.showPaymentFormData[$scope.paymentLength].CLIENTKEY,
            CLIENTSECRET: $scope.showPaymentFormData[$scope.paymentLength].CLIENTSECRET,
            ISACTIVE: $scope.showPaymentFormData[$scope.paymentLength].ISACTIVE,
            PAYMENTYPE: $scope.showPaymentFormData[$scope.paymentLength].PAYMENTYPE,
            ISLIVE: $scope.showPaymentFormData[$scope.paymentLength].ISLIVE,
            id: $scope.showPaymentFormData[$scope.paymentLength].id
        };
        console.log(data);
        generalsettingsService.savePayments(data).then(function (response) {
            console.log(response);
            if (response.data.data.error) {
                $scope.error = true;
                $scope.msg = response.data.data.msg;
            } else {
                $scope.error = false;
                $scope.msg = 'Payment Methods saved successfully.';

                $scope.paymentLength = $scope.paymentLength + 1;
                if ($scope.showPaymentFormData.length > $scope.paymentLength) {
                    $scope.savePayments();
                } else {
                    $scope.paymentLength = 0;
                }
                //$scope.getAllPayments();

                /*data = {CLIENTKEY: $scope.showPaymentFormData[1].CLIENTKEY, CLIENTSECRET: $scope.showPaymentFormData[1].CLIENTSECRET, ISACTIVE: $scope.showPaymentFormData[1].ISACTIVE, PAYMENTYPE: $scope.showPaymentFormData[1].PAYMENTYPE, id: $scope.showPaymentFormData[1].id};
                console.log(data);
                generalsettingsService.savePayments(data).then(function(response) {
                    console.log(response);
                    if (response.data.data.error) {
                        $scope.error = true;
                        $scope.msg = response.data.data.msg;
                    } else {
                        $scope.error = false;
                        $scope.msg = 'Payment Methods saved successfully.';
                        $scope.getAllPayments();
                    }
                });*/
            }


        });



        //}
        /* else {
         data = {CLIENTKEY: $scope.showPaymentFormData[i].CLIENTKEY, CLIENTSECRET: $scope.showPaymentFormData[i].CLIENTSECRET, ISACTIVE: $scope.showPaymentFormData[i].ISACTIVE, PAYMENTYPE: $scope.showPaymentFormData[i].PAYMENTYPE, id: $scope.showPaymentFormData[i].id};
         console.log(data);
         generalsettingsService.savePaymentsNew(data).then(function (response) {
         if (response.data.data.error) {
         $scope.error = true;
         $scope.msg = response.data.data.msg;
         } else {
         $scope.error = false;
         $scope.msg = 'Payment Methods saved successfully.';
         $scope.getAllPayments();
         }
         });
         
         }*/
        //}
    };

    $scope.addCoupon = function () {
        $scope.error = null;
        $scope.modalerror = null;
        $scope.msg = null;
        $scope.modalmsg = null;
        $scope.ShowSpecialCoupon = true;
        $scope.ShowreturningCoupon = true;

        $scope.couponForm = {
            id: null,
            name: null,
            code: null,
            date_of_issue: null,
            expiry_date: null,
            discount_type: null,
            discount: null,
            select_rule: null,
            discount_on_price: null,
            status: 1,
            type: null,
            specialType: null,
            description: null
        };

        $scope.couponFormType = 'Add';
        angular.element('#couponModal').modal('toggle');
    }

    $scope.editCoupon = function (data) {
        $scope.error = null;
        $scope.modalerror = null;
        $scope.msg = null;
        $scope.modalmsg = null;

        //$scope.couponId = data.id;

        $scope.couponForm = {
            id: data.id,
            name: data.name,
            code: data.code,
            date_of_issue: data.dateofissueformated,
            expiry_date: data.expirydateformated,
            discount_type: data.discount_type.replace(/ /g, ''),
            discount: data.discount,
            select_rule: data.select_rule,
            discount_on_price: data.discount_on_price,
            status: data.status,
            type: '' + data.type,
            specialType: data.specialType,
            maximumDiscount: data.maximumDiscount,
            useTotalOrderValue: data.useTotalOrderValue,
            useTotalNoOfOrder: data.useTotalNoOfOrder,
            description: data.description
        };
        $scope.changeCoponType();
        $scope.couponFormType = 'Edit';
        angular.element('#couponModal').modal('toggle');
    }

    $scope.getCoupon = function () {
        generalsettingsService.GetCoupons({
            id: $scope.couponId
        }).then(function (response) {
            if (response.data.error) {
                $scope.error = true;
                $scope.msg = response.data.msg;
            }
            $scope.coupons = response.data.data.coupons;
        }).catch(function (e) {
            $scope.error = true;
            $scope.msg = e.statusText;
        });
    }

    $scope.getOutlet = function () {
        generalsettingsService.GetOutlet({
            id: 1
        }).then(function (response) {
            if (response.data.error) {
                $scope.error = true;
                $scope.msg = response.data.msg;
            }
            $scope.Outlets = response.data.data;
            console.log($scope.Outlets);
        }).catch(function (e) {
            $scope.error = true;
            $scope.msg = e.statusText;
        });
    }

    $scope.GetCouponTypes();
    $scope.GetSpecialCouponTypes();
    $scope.getCoupon();
    $scope.getOutlet();

    $scope.saveCoupon = function () {
        $scope.error = null;
        $scope.modalerror = null;
        $scope.msg = null;
        $scope.modalmsg = null;
        if ($scope.couponForm.type === null) {
            $scope.modalerror = true;
            $scope.modalmsg = "Coupon type can't be blank";
        }
        if ($scope.couponForm.name === null) {
            $scope.modalerror = true;
            $scope.modalmsg = "Name can't be blank";
        }
        if ($scope.couponForm.code === null) {
            $scope.modalerror = true;
            $scope.modalmsg = "Code can't be blank";
        }
        if ($scope.couponForm.date_of_issue === null) {
            $scope.modalerror = true;
            $scope.modalmsg = "Date of issue can't be blank";
        }
        if ($scope.couponForm.expiry_date === null) {
            $scope.modalerror = true;
            $scope.modalmsg = "Expiry date can't be blank";
        }
        if ($scope.couponForm.discount_type === null) {
            $scope.modalerror = true;
            $scope.modalmsg = "Discount type can't be blank";
        }
        if ($scope.couponForm.discount === null) {
            $scope.modalerror = true;
            $scope.modalmsg = "Discount can't be blank";
        }
        if ($scope.couponForm.select_rule === null) {
            $scope.modalerror = true;
            $scope.modalmsg = "Select rule can't be blank";
        }
        if ($scope.couponForm.discount_on_price === null) {
            $scope.modalerror = true;
            $scope.modalmsg = "Discount on price can't be blank";
        }

        if ($scope.couponForm.description === null) {
            $scope.modalerror = true;
            $scope.modalmsg = "Description can't be blank";
        }

        if ($scope.modalerror == null) {
            angular.element('#couponaddbutton').button('loading');
            console.log($scope.couponForm);
            generalsettingsService.AddCoupon($scope.couponForm).then(function (response) {
                if (response.data.error) {
                    $scope.modalerror = true;
                    $scope.modalmsg = response.data.msg;
                } else {
                    $scope.modalerror = false;
                    $scope.getCoupon();
                    $scope.modalmsg = response.data.msg;
                }
                angular.element('#couponaddbutton').button('reset');
            }).catch(function (e) {
                $scope.modalerror = true;
                $scope.modalmsg = e.statusText;
            });
        }
    }

    $scope.CouponChangeStatus = function (data, status) {
        $scope.error = null;
        $scope.modalerror = null;
        $scope.msg = null;
        $scope.modalmsg = null;
        if (status == 0) {
            var stat = 'inactive';
        } else {
            var stat = 'active';
        }
        Alertify.confirm('Are you sure to make this coupon rule ' + stat + '?').then(
            function onOk() {
                if (typeof (status) === "undefined") {
                    status = 1;
                }
                generalsettingsService.ChangeCouponStatus({
                    id: data.id,
                    status: status
                }).then(function (response) {
                    $scope.msg = response.data.msg;
                    if (response.data.error) {
                        $scope.error = true;
                    } else {
                        data.status = status;
                        $scope.error = false;
                    }
                }).catch(function (e) {
                    $scope.error = true;
                    $scope.msg = e.statusText;
                });

                /*$scope.couponForm = {
                    id: data.id,
                    type: data.type,
                    name: data.name,
                    code: data.code,
                    date_of_issue: data.dateofissueformated,
                    expiry_date: data.expirydateformated,
                    discount_type: data.discount_type.replace(/ /g, ''),
                    discount: data.discount,
                    select_rule: data.select_rule,
                    discount_on_price: data.discount_on_price,
                    status: status
                };

                console.log($scope.couponForm);
                $scope.saveCoupon();*/

            },
            function onCancel() {
                //console.log('you have click cancel;');
            }
        );
    }

    $scope.DeleteCoupon = function (data) {
        $scope.error = null;
        $scope.modalerror = null;
        $scope.msg = null;
        $scope.modalmsg = null;
        Alertify.confirm('Are you sure to delete this coupon?').then(
            function onOk() {
                generalsettingsService.DeleteCoupon({id: data.id}).then(function (response) {
                 $scope.msg = response.data.msg;
                 if (response.error) {
                 $scope.error = true;
                 $scope.msg = response.msg;
                 } else {
                 $scope.error = false;
                 $scope.getCoupon();
                 $scope.msg = 'Coupon deleted successfully.';
                 }
                 }).catch(function (e) {
                 $scope.error = true;
                 $scope.msg = e.statusText;
                 });
            },
            function onCancel() {
                //console.log('you have click cancel;');
            }
        );
    }

    $scope.DeleteOutlet = function (outletID) {
        Alertify.confirm('Are you sure to delete this outlet?').then(
            function onOk() {
                generalsettingsService.DeletOutlet({
                    id: outletID
                }).then(function (response) {
                    $scope.msg = response.data.msg;
                    if (response.error) {
                        $scope.error = true;
                        $scope.msg = response.msg;
                    } else {
                        $scope.error = false;
                        $scope.getOutlet();
                        $scope.msg = 'Outlet deleted successfully.';
                    }
                }).catch(function (e) {
                    $scope.error = true;
                    $scope.msg = e.statusText;
                });
            },
            function onCancel() {
                //console.log('you have click cancel;');
            }
        );
    }

    $scope.addOutlet = function () {
        $scope.modalerror = null;
        $scope.OutletForm = {
            id: null,
            name: null,
            status: '1'
        }
        $scope.outletFormType = 'Add';
        angular.element('#outletModal').modal('toggle');
    }

    $scope.saveOutlet = function () {
        $scope.error = null;
        $scope.modalerror = null;
        $scope.msg = null;
        $scope.modalmsg = null;

        if ($scope.OutletForm.name === null) {
            $scope.modalerror = true;
            $scope.modalmsg = "Outlet name can't be blank";
        } else if ($scope.OutletForm.status === null) {
            $scope.modalerror = true;
            $scope.modalmsg = "Please select outlet status";
        } else {
            generalsettingsService.AddOutlet($scope.OutletForm).then(function (response) {
                if (response.data.error) {
                    $scope.modalerror = true;
                    $scope.modalmsg = response.data.msg;
                } else {
                    $scope.modalerror = false;
                    $scope.taxRuleId = '';
                    $scope.getOutlet();
                    $scope.modalmsg = response.data.msg;
                }
            }).catch(function (e) {
                $scope.modalerror = true;
                $scope.modalmsg = e.statusText;
            });
        }
    }

    $scope.editOutlet = function (outlet) {
        $scope.modalerror = null;
        $scope.OutletForm = {
            id: outlet.id,
            name: outlet.name,
            status: '' + outlet.status
        }
        $scope.outletFormType = 'Edit';
        angular.element('#outletModal').modal('toggle');
    }

    $scope.getPreorderSettings = function () {
        generalsettingsService.getPreorderSettings().then(function (response) {
            if (response.error) {
                $scope.error = true;
                $scope.msg = response.msg;
            } else {
                console.log("dasd", response);
                //$scope.preOrderSettings = response.data.data.PreOrderSettings;
                if (response.data.data.PreOrderSettings.length > 0) {
                    //$scope.preOrderSettings = response.data.data.PreOrderSettings[0];
                    $scope.preOrderSettings = {
                        id: response.data.data.PreOrderSettings[0].id,
                        monsh: '' + response.data.data.PreOrderSettings[0].monsh,
                        monsm: '' + response.data.data.PreOrderSettings[0].monsm,
                        moneh: '' + response.data.data.PreOrderSettings[0].moneh,
                        monem: '' + response.data.data.PreOrderSettings[0].monem,
                        ismonoffday: '' + response.data.data.PreOrderSettings[0].ismonoffday,
                        tuesh: '' + response.data.data.PreOrderSettings[0].tuesh,
                        tuesm: '' + response.data.data.PreOrderSettings[0].tuesm,
                        tueeh: '' + response.data.data.PreOrderSettings[0].tueeh,
                        tueem: '' + response.data.data.PreOrderSettings[0].tueem,
                        istueoffday: '' + response.data.data.PreOrderSettings[0].istueoffday,
                        wedsh: '' + response.data.data.PreOrderSettings[0].wedsh,
                        wedsm: '' + response.data.data.PreOrderSettings[0].wedsm,
                        wedeh: '' + response.data.data.PreOrderSettings[0].wedeh,
                        wedem: '' + response.data.data.PreOrderSettings[0].wedem,
                        iswedoffday: '' + response.data.data.PreOrderSettings[0].iswedoffday,
                        thush: '' + response.data.data.PreOrderSettings[0].thush,
                        thusm: '' + response.data.data.PreOrderSettings[0].thusm,
                        thueh: '' + response.data.data.PreOrderSettings[0].thueh,
                        thuem: '' + response.data.data.PreOrderSettings[0].thuem,
                        isthuoffday: '' + response.data.data.PreOrderSettings[0].isthuoffday,
                        frish: '' + response.data.data.PreOrderSettings[0].frish,
                        frism: '' + response.data.data.PreOrderSettings[0].frism,
                        frieh: '' + response.data.data.PreOrderSettings[0].frieh,
                        friem: '' + response.data.data.PreOrderSettings[0].friem,
                        isfrioffday: '' + response.data.data.PreOrderSettings[0].isfrioffday,
                        satsh: '' + response.data.data.PreOrderSettings[0].satsh,
                        satsm: '' + response.data.data.PreOrderSettings[0].satsm,
                        sateh: '' + response.data.data.PreOrderSettings[0].sateh,
                        satem: '' + response.data.data.PreOrderSettings[0].satem,
                        issatoffday: '' + response.data.data.PreOrderSettings[0].issatoffday,
                        sunsh: '' + response.data.data.PreOrderSettings[0].sunsh,
                        sunsm: '' + response.data.data.PreOrderSettings[0].sunsm,
                        suneh: '' + response.data.data.PreOrderSettings[0].suneh,
                        sunem: '' + response.data.data.PreOrderSettings[0].sunem,
                        issunfoffday: '' + response.data.data.PreOrderSettings[0].issunfoffday,
                        defaulyreservationlength: response.data.data.PreOrderSettings[0].defaulyreservationlength,
                        reservexhrsearlier: '' + response.data.data.PreOrderSettings[0].reservexhrsearlier
                    }

                    console.log("$scope.preOrderSettings==>", $scope.preOrderSettings);

                }

            }
        });
    };

    $scope.savePreorderSettings = function () {
        $scope.error = null;
        $scope.modalerror = null;
        $scope.msg = null;
        $scope.modalmsg = null;
        console.log("asdasdas", $scope.error);
        //if ($scope.error == null) {
        angular.element('#preordersettingsSaveButton').button('loading');
        $scope.error = null;
        $scope.msg = null;
        generalsettingsService.savePreorderSettings($scope.preOrderSettings).then(function (response) {
            if (response.data.error) {
                $scope.error = true;
                $scope.msg = response.data.msg;
            } else {
                $scope.error = false;
                $scope.getPreorderSettings();
                $scope.msg = response.data.msg;
            }
            angular.element('#preordersettingsSaveButton').button('reset');
        });

        //}
    };

    $scope.GetPreorderTables = function () {
        generalsettingsService.GetPreorderTables().then(function (response) {
            if (response.error) {
                $scope.error = true;
                $scope.msg = response.msg;
            } else {
                $scope.preOrderTables = response.data.data.PreOrderTables;
            }
        });
    };

    $scope.addPreOrderTable = function () {
        $scope.modalerror = null;
        $scope.preOrderTableForm = {
            id: null,
            name: null,
            capacity: 1,
            status: '1'
        };
        $scope.preorderTableFormType = 'Add';
        angular.element('#preorderTableModal').modal('toggle');
    }

    $scope.editPreOrderTable = function (outlet) {
        $scope.modalerror = null;
        $scope.preOrderTableForm = {
            id: outlet.id,
            name: outlet.name,
            capacity: outlet.capacity,
            status: '' + outlet.status
        }
        $scope.preorderTableFormType = 'Edit';
        angular.element('#preorderTableModal').modal('toggle');
    }

    $scope.savePreOrderTable = function () {
        $scope.error = null;
        $scope.modalerror = null;
        $scope.msg = null;
        $scope.modalmsg = null;

        if ($scope.preOrderTableForm.name === null) {
            $scope.modalerror = true;
            $scope.modalmsg = "Name can't be blank";
        } else if ($scope.preOrderTableForm.capacity === null) {
            $scope.modalerror = true;
            $scope.modalmsg = "Please enter capacity";
        } else if ($scope.preOrderTableForm.status === null) {
            $scope.modalerror = true;
            $scope.modalmsg = "Please select status";
        } else {
            generalsettingsService.savePreorderTable($scope.preOrderTableForm).then(function (response) {
                if (response.data.error) {
                    $scope.modalerror = true;
                    $scope.modalmsg = response.data.msg;
                } else {
                    $scope.modalerror = false;
                    $scope.GetPreorderTables();
                    $scope.modalmsg = response.data.msg;
                }
            }).catch(function (e) {
                $scope.modalerror = true;
                $scope.modalmsg = e.statusText;
            });
        }
    }

    $scope.deletePreOrderTable = function (outletID) {
        Alertify.confirm('Are you sure to delete this table?').then(
            function onOk() {
                generalsettingsService.deletePreOrderTable({
                    id: outletID
                }).then(function (response) {
                    $scope.msg = response.data.msg;
                    if (response.error) {
                        $scope.error = true;
                        $scope.msg = response.msg;
                    } else {
                        $scope.error = false;
                        $scope.GetPreorderTables();
                        $scope.msg = 'Table deleted successfully.';
                    }
                }).catch(function (e) {
                    $scope.error = true;
                    $scope.msg = e.statusText;
                });
            },
            function onCancel() {
                //console.log('you have click cancel;');
            }
        );
    }
    $scope.selectedTaxRule = [];
    $scope.selall =function (){
        if($scope.selectAll){
            for(var i=0; i<$scope.taxRuleData.data.length; i++){
                $scope.selectedTaxRule[i] = $scope.taxRuleData.data[i].id;
            }
        }
        else{
            $scope.selectedTaxRule = [];
        }
    }

    $scope.deleteSelectedTAxRules = function () {
        var data = {taxids: $scope.selectedTaxRule};
        Alertify.confirm('Are you sure to delete this Tax Rules?').then(
            function onOk() {
                $scope.ShowLoader = true;
                generalsettingsService.deleteSelectedTAxRules(data).then(function (response) {
                    if (response.data.error) {
                        $scope.error = true;
                        $scope.msg = response.data.msg;
                    } else {
                        $scope.error = false;
                    }
                    $scope.msg = response.data.msg; 
                    $scope.getTaxRule();
                    $scope.ShowLoader = false;  
                }).catch(function (e) {
                    $scope.error = true;
                    $scope.msg = e.statusText;
                    $scope.ShowLoader = false;
                });


                /*generalsettingsService.DeleteTaxes({
                    id: data.id
                }).then(function (response) {
                    $scope.msg = response.data.msg;
                    if (response.error) {
                        $scope.error = true;
                        $scope.msg = response.msg;
                    } else {
                        $scope.error = false;
                        $scope.getTaxRule();
                        $scope.msg = 'Tax Rule deleted successfully.';
                    }
                }).catch(function (e) {
                    $scope.error = true;
                    $scope.msg = e.statusText;
                });*/
            },
            function onCancel() {
                //console.log('you have click cancel;');
            }
        );
    }

}]);