EcommerceApp.controller('ImportexportController', ['$scope', 'importexportService', '$routeParams', 'Alertify', 'isMobile', function($scope, importexportService, $routeParams, Alertify, isMobile) {

        $scope.error = null;
        $scope.modalerror = null;
        $scope.msg = null;
        $scope.modalmsg = null;
        $scope.isM = 'Desktop';
        if( isMobile.any() ) 
        {
            var ggg = isMobile.any();
            $scope.isM = 'Mobile';
            if(ggg["0"] == 'iPad'){
                $scope.isM = 'iPad';
            }        
        }
        
        $scope.importFile = false;
        $scope.ShowLoader = true;

        $scope.importProducts = function(name) {

            angular.element('#importProductsButton').button('loading');
            $scope.error = null;
            $scope.modalerror = null;
            $scope.msg = null;
            $scope.modalmsg = null;
            
            var importFile = $scope.importFile;
            var type=importFile.type;
            if(type!='text/csv'){
                $scope.error = true;
                $scope.msg = 'Please select csv file to upload.';
            }
            var fd = new FormData();
            var importFiletoUpload = [importFile];
            if (importFile) {
                fd.append('Import', importFiletoUpload);
            }
            else{
                $scope.error = true;
                $scope.msg = 'Please select csv file to upload.';
            }
            console.log(fd);
            /*if($scope.error == null){
                importexportService.importProducts(fd).then(function(response) {
                    if (response.data.error) {
                        $scope.error = true;
                        $scope.msg = response.data.msg;
                    } else {
                        $scope.error = false;
                        $scope.msg = response.data.msg;
                    }
                    angular.element('#importProductsButton').button('reset');

                    console.log(response);
                }).catch(function(e){
                    $scope.error = true;
                    $scope.msg = e.statusText;
                });
            }*/
        };

        

    }]);

