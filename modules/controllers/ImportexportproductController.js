EcommerceApp.controller('ImportexportproductController', ['$scope', 'importexportService', '$routeParams', 'Alertify', 'isMobile', function($scope, importexportService, $routeParams, Alertify, isMobile) {

        $scope.error = null;
        $scope.modalerror = null;
        $scope.msg = null;
        $scope.modalmsg = null;
        $scope.isM = 'Desktop';
        if( isMobile.any() ) 
        {
            var ggg = isMobile.any();
            $scope.isM = 'Mobile';
            if(ggg["0"] == 'iPad'){
                $scope.isM = 'iPad';
            }        
        }
        
        $scope.importFile = false;
        $scope.ShowLoader = false;

        $scope.importProducts = function(name) {
            
            $scope.error = null;
            $scope.modalerror = null;
            $scope.msg = null;
            $scope.modalmsg = null;

            $scope.importFile = name.files[0];
            
            var importFile = $scope.importFile;
            var fd = new FormData();
            if (importFile) {
                fd.append('Import[0]', importFile);
            }
            else{
                $scope.error = true;
                $scope.msg = 'Please select csv file to upload.';
            }
            if($scope.error == null){
                $scope.ShowLoader = true;
                angular.element('#importProductsButton').button('loading');
                importexportService.importProducts(fd).then(function(response) {
                    if (response.data.error) {
                        $scope.error = true;
                        $scope.msg = response.data.msg;
                    } else {
                        $scope.error = false;
                        $scope.msg = response.data.msg;
                    }
                    angular.element('#importProductsButton').button('reset');
                    $scope.ShowLoader = false;
                    console.log(response);
                }).catch(function(e){
                    $scope.error = true;
                    $scope.msg = e.statusText;
                });
            }
        };

        $scope.downloadFile = function() { 
            window.open('/img/simple_product.csv', '_blank', '');  
        }

    }]);

