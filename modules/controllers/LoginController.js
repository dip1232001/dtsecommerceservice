EcommerceApp.controller('LoginController', ['$scope', '$http', '$q', 'serverUrls', '$cookies', 'Base64', '$location', function ($scope, $http, $q, serverUrls, $cookies, Base64, $location) {
        $scope.error = false;
        $scope.msg = null;
        $scope.username = null;
        $scope.password = null;
        var Authcanceller = null;
        $scope.submitloginform = function () {
            if (Authcanceller) {
                Authcanceller.resolve();
            }
            Authcanceller = $q.defer();
            $http({
                url: serverUrls.authenticate,
                method: 'POST',
                data: {"grant_type": "client_credentials"},
                headers: {
                    'Authorization': 'Basic ' + Base64.encode($scope.username + ':' + $scope.password)
                },
                timeout: Authcanceller.promise
            }).then(function (response) {
                var parsedDate = new Date()
                var newDate = new Date(parsedDate.getTime() + (1000 * response.data.expires_in) - 20);
                var expStr = moment(newDate).format('LLLL');
                var newDate2 = new Date(parsedDate.getTime() + (1000 * (3600 * 24)));
                var expStr2 = moment(newDate2).format('LLLL');
                $cookies.put('AT', response.data.access_token, {'expires': expStr});
                $cookies.put('K', Base64.encode($scope.username + ':' + $scope.password), {'expires': expStr2});
            }).then(function () {
                var rpath = $cookies.get('RP');
                console.log('rpath=>',rpath);
                if(rpath){
                    var nrp = decodeURI(rpath);
                    $location.path(nrp);
                }else{
                    $location.path('/dashboard');
                }                
            }).catch(function (e) {
                // handle errors in processing or in error.
                $scope.error = true;
                $scope.msg = e.data.error_description;
            });
        }
    }]);

