EcommerceApp.controller('ManagecategoryController', ['$scope', 'categoryService', '$routeParams', 'Alertify', 'isMobile', function($scope, categoryService, $routeParams, Alertify, isMobile) {

        $scope.error = null;
        $scope.modalerror = null;
        $scope.msg = null;
        $scope.modalmsg = null;
        $scope.isM = 'Desktop';
        if( isMobile.any() ) 
        {
            var ggg = isMobile.any();
            $scope.isM = 'Mobile';
            if(ggg["0"] == 'iPad'){
                $scope.isM = 'iPad';
            }        
        }

        $scope.CategoryForm = {
            name: null,
            parent_id: null,
            status: 1,
            image_url: false
        };
        $scope.categorydata = [];
        $scope.parent_id = 0;

        $scope.frommode = 'add';
        $scope.category_id = null;
        $scope.myFile = false;
        $scope.ShowLoader = true;

        if ($routeParams.hasOwnProperty('parentID') && typeof ($routeParams.parentID) !== 'undefined' && $routeParams.parentID > 0) {
            $scope.parent_id = $routeParams.parentID;
        }
        if ($routeParams.hasOwnProperty('id') && typeof ($routeParams.id) !== 'undefined' && $routeParams.id > 0) {
            $scope.category_id = $routeParams.id;
        }

        $scope.createCategory = function(name) {
            
            $scope.error = null;
            $scope.modalerror = null;
            $scope.msg = null;
            $scope.modalmsg = null;

            if (typeof (name) !== "undefined") {
                $scope.CategoryForm.name = name;
            }
            if ($scope.CategoryForm.name === null) {
                $scope.modalerror = true;
                $scope.msg = "Category can't be blank";
            }

            var file = $scope.myFile;
            var fd = new FormData();
            if ($scope.frommode == 'edit') {
                fd.append('id', $scope.category_id);
            }
            if (file) {
                fd.append('Images', file);
                fd.append('image_url', null);
            } else {
                fd.append('image_url', $scope.CategoryForm.image_url);
                fd.append('Images', null);
            }
            fd.append('name', $scope.CategoryForm.name);
            fd.append('parent_id', $scope.CategoryForm.parent_id);
            fd.append('status', $scope.CategoryForm.status);
            if($scope.modalerror == null){
                angular.element('#categorySaveButton').button('loading');
                categoryService.createCategory(fd).then(function(response) {
                    if (response.data.error) {
                        $scope.modalerror = true;
                        $scope.modalmsg = response.data.msg;
                    } else {
                        $scope.modalerror = false;
                        $scope.modalmsg = response.data.msg;
                    }
                    angular.element('#categorySaveButton').button('reset');
                }).catch(function(e){
                    $scope.modalerror = true;
                    $scope.modalmsg = e.statusText;
                });
            }
        };

        $scope.getCategory = function() {
            
            categoryService.getCategory({parent_id: $scope.parent_id}).then(function(response) {
                if (response.error) {
                    $scope.error = true;
                    $scope.msg = response.msg;
                }
                //console.log(data);
                $scope.categorydata = response.data.data.categories;
                $scope.ShowLoader = false;
                //console.log(response);

            }).catch(function(e){
                $scope.error = true;
                $scope.msg = e.statusText;
                $scope.ShowLoader = false;
            });
        };

        
        $scope.edit_category = function(category) {
            //console.log(category);
            $scope.showoverlay = false;
            if (typeof (category.status) == 'undefined') {
                category.status = 1;
            }
            $scope.category_id = category.id;
            $scope.CategoryForm.name = category.name;
            $scope.CategoryForm.parent_id = category.parent_id;
            $scope.CategoryForm.status = category.status;
            $scope.CategoryForm.image_url = category.image;
            $scope.myFile = false;
            $scope.msg = null;
            $scope.error = null;
            $scope.frommode = 'edit';
        };
        
        $scope.AddNewCategory = function() {
            $scope.CategoryForm = {
                name: null,
                parent_id: $scope.parent_id,
                status: 1,
                image_url: false
            };
            $scope.msg = null;
            $scope.error = null;
            $scope.frommode = 'add';
        }

    }]);

