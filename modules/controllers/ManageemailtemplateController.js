EcommerceApp.controller('ManageemailtemplateController', ['$scope', 'emailtemplateService', '$routeParams', 'Alertify', 'isMobile', function($scope, emailtemplateService, $routeParams, Alertify, isMobile) {

        $scope.error = null;
        $scope.modalerror = null;
        $scope.msg = null;
        $scope.modalmsg = null;
        $scope.isM = 'Desktop';
        if( isMobile.any() ) 
        {
            var ggg = isMobile.any();
            $scope.isM = 'Mobile';
            if(ggg["0"] == 'iPad'){
                $scope.isM = 'iPad';
            }        
        }

        $scope.emailDetailsdata = [];
        $scope.email_type = null;
        
        $scope.emailDetailsForm = {
            TeamplateType: null,
            emailsubject: null,
            emailbody: null
        };

        var events = ['trixInitialize', 'trixChange', 'trixSelectionChange', 'trixFocus', 'trixBlur', 'trixFileAccept', 'trixAttachmentAdd', 'trixAttachmentRemove'];

        for (var i = 0; i < events.length; i++) {
            $scope[events[i]] = function(e) {
                console.info('Event type:', e.type);
            }
        };

        
        
        $scope.getEmailDetails = function() {
            emailtemplateService.getEmailDetails({email_type:$scope.email_type}).then(function(response) {
                $scope.msg = response.data.msg;
                if (response.data.error) {
                    $scope.error = true;
                }
                $scope.emailDetailsdata = response.data.data;
                $scope.ShowLoader = false;
                console.log($scope.emailDetailsdata);
                
                $scope.emailDetailsForm.TeamplateType = $scope.email_type;
                $scope.emailDetailsForm.emailsubject = $scope.emailDetailsdata.email_subject;
                $scope.emailDetailsForm.emailbody = $scope.emailDetailsdata.email_body;

            }).catch(function(e){
                $scope.error = true;
                $scope.msg = e.statusText;
                $scope.ShowLoader = false;
            });
        };
        
        $scope.saveEmail = function(){
            angular.element('#emailSaveButton').button('loading');
            $scope.modalerror = null;
            $scope.modalmsg = null;
            //$scope.editForm = false;
            //$scope.email_type = null;
            //$scope.getEmailList();
            console.log($scope.emailDetailsForm);
            if ($scope.emailDetailsForm.emailsubject === null || $scope.emailDetailsForm.emailsubject === '') {
                $scope.modalerror = true;
                $scope.modalmsg = "Email subject can't be blank";
            } else if ($scope.emailDetailsForm.emailbody === null || $scope.emailDetailsForm.emailbody === '') {
                $scope.modalerror = true;
                $scope.modalmsg = "Email body can't be blank";
            }
            if ($scope.modalerror == null) {
                emailtemplateService.editEmailDetails($scope.emailDetailsForm).then(function (response) {
                    console.log(response);
                    if (response.data.data.error) {
                        $scope.modalerror = true;
                        $scope.modalmsg = response.data.data.msg;                    
                    } else {
                        $scope.modalerror = false;
                        $scope.modalmsg = response.data.data.msg;
                        $scope.getEmailDetails();
                        if ($scope.frommode == 'edit') {
                            $scope.modalmsg = 'Template edited successfully';
                        }
                    }
                    angular.element('#emailSaveButton').button('reset');
                }).catch(function (e) {
                    $scope.modalerror = true;
                    $scope.modalmsg = e.statusText;
                    angular.element('#emailSaveButton').button('reset');
                });
                var trgt = angular.element('.container-fluid').eq(0);
                $('html,body').animate({scrollTop: trgt.offset().top}, 1000);
            }
        }

        if ($routeParams.hasOwnProperty('id') && typeof ($routeParams.id) !== 'undefined' && $routeParams.id != '') {
            $scope.email_type = $routeParams.id;
            $scope.getEmailDetails();
        }

    }]);

