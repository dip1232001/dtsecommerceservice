EcommerceApp.controller('OrderController', ['$scope', 'Alertify', 'orderService', 'storeType', '$location', '$routeParams','$timeout', 'isMobile', function ($scope, Alertify, orderService, storeType, $location, $routeParams,$timeout, isMobile) {

        $scope.error = null;
        $scope.modalerror = null;
        $scope.msg = null;
        $scope.modalmsg = null;
        $scope.ShowLoader = true;
        $scope.isM = 'Desktop';
        if( isMobile.any() ) 
        {
            var ggg = isMobile.any();
            $scope.isM = 'Mobile';
            if(ggg["0"] == 'iPad'){
                $scope.isM = 'iPad';
            }        
        }

        $scope.start = 0;
        $scope.currentPage = 1;
        $scope.pageSize = 20;
        $scope.total = null;
        $scope.hideIfEmpty = true;
        $scope.showPrevNext = true;
        $scope.showFirstLast = true;
        $scope.kitchenPrintInfo=null;
        $scope.deliveryType='';
        $scope.GetNewOrderCount = null;

        $scope.getGeneralSettingsN = [];

        $scope.pgHref = $location.path();

        /*if ($routeParams.hasOwnProperty('page') && typeof ($routeParams.page) !== 'undefined' && $routeParams.page > 0) {
            $scope.currentPage = $routeParams.page;
        }*/

        $scope.makeshipping=false;

        $scope.searchData = {
            deliveryType :'',
            id: null,
            startdate: null,
            enddate: null
        };

        $scope.ShipmentForm = {
            shipperAddress: null,
            trackingNO: null
        };

        $scope.selectedOrderID = null;

        $scope.orderData = [];
        $scope.orderDetailsData = [];
        $scope.orderStatusData = [];
        $scope.orderTypeData = [];
        $scope.selectedOrder = [];

        $scope.storeType=storeType;

        $scope.selectedTab = 'OrderDashboard';
        $scope.selectTab = function (id, type) {                       
            $scope.selectedTab = id;
            if(id !== 'OrderDashboard'){
                $scope.searchData.deliveryType = type; 
                $scope.start = 0;
                $scope.currentPage = 1;
                $scope.getOrder();
            }
        }

        $scope.getGeneralSettingsS = function () {
            orderService.getGeneralSettingsN().then(function (response) {
                if (response.error) {
                    $scope.error = true;
                    $scope.msg = response.msg;
                } else {
                    $scope.getGeneralSettingsN = response.data.data;
                    console.log("$scope.getGeneralSettingsN===>",$scope.getGeneralSettingsN);                    
                }
            });
        };

        $scope.getOrder = function () {
            $scope.error = null;
            $scope.modalerror = null;
            $scope.msg = null;
            $scope.modalmsg = null;
            $scope.ShowLoader = true;
            $scope.selectedOrder = [];
            $scope.deliveryType=$scope.searchData.deliveryType;

            if( $scope.currentPage > 1 ){
                var Newstart = (($scope.currentPage - 1) * $scope.pageSize);
                $scope.start = Newstart;
            } 
            var data = {identifier: $scope.searchData.id, startdate: $scope.searchData.startdate, enddate: $scope.searchData.enddate,deliveryType:$scope.searchData.deliveryType};
            orderService.getOrder(data, $scope.start, $scope.pageSize).then(function (response) {
                if (response.error) {
                    $scope.error = true;
                    $scope.msg = response.msg;
                } else {
                    $scope.error = null;
                    $scope.msg = null;
                }
                $scope.orderData = response.data.data.orders;
                $scope.total = response.data.data.Paginations.Total;
                /*if ($routeParams.hasOwnProperty('page') && typeof ($routeParams.page) !== 'undefined' && $routeParams.page > 0) {
                    $scope.currentPage = $routeParams.page;
                }*/
                $scope.ShowLoader = false;
            }).catch(function (e) {
                $scope.error = true;
                $scope.msg = e.statusText;
                $scope.ShowLoader = false;
            });
        }

        $scope.search = function () {
            $scope.start = 0;
            $scope.currentPage = 1;
            $scope.getOrder();
        }

        $scope.resetSearch = function () {
            $scope.start = 0;
            $scope.currentPage = 1;
            $scope.searchData = {
                id: null,
                startdate: null,
                enddate: null,
            };

            //$location.search('page', null);
            
            $scope.getOrder();
        }

        $scope.DoCtrlPagingAct = function (text, page, pageSize, total) {
            //console.log("page",page);
            $scope.currentPage = page;
            var Newstart = ((page - 1) * pageSize);
            $scope.start = Newstart;

            $scope.getOrder();

            /*
            $scope.ShowLoader = true;
            var data = {identifier: $scope.searchData.id, startdate: $scope.searchData.startdata, enddate: $scope.searchData.enddate};
            orderService.getOrder(data, Newstart, $scope.pageSize).then(function (response) {
                if (response.error) {
                    $scope.error = true;
                    $scope.msg = response.msg;
                } else {
                    $scope.error = null;
                }
                $scope.orderData = response.data.data.orders;
                $scope.total = response.data.data.Paginations.Total;
                $scope.ShowLoader = false;
            }).catch(function (e) {
                $scope.error = true;
                $scope.msg = e.statusText;
                $scope.ShowLoader = false;
            });*/
        }


        $scope.viewOrderDetails = function (data) {
            $scope.error = null;
            $scope.modalerror = null;
            $scope.msg = null;
            $scope.modalmsg = null;
			
			$scope.ShipmentForm = {
				shipperAddress: null,
				trackingNO: null
			};
			$scope.makeshipping = false;

            $scope.selectedOrderID = data.id;
            orderService.getOrder({id: data.id}).then(function (response) {
                if (response.error) {
                    $scope.modalerror = true;
                    $scope.modalmsg = response.msg;
                }
                $scope.orderDetailsData = response.data.data.orders[0];
            }).catch(function (e) {
                $scope.modalerror = true;
                $scope.modalmsg = e.statusText;
            });            

            angular.element('#orderModal').modal('toggle');
        }

        /*$scope.deleteOrder = function (data) {
            Alertify.confirm('Are you sure to delete this Order?').then(
                    function onOk() {
                        console.log('You have click Ok;');
                    },
                    function onCancel() {
                        console.log('you have click cancel;');
                    }
            );
        }*/

        $scope.changeSearchStatus = function (status) {
            var data = {orderids: $scope.selectedOrder, status: status};
            var sID = null;
            for(var i=0; i<$scope.orderStatusData.length; i++){
                if($scope.orderStatusData[i].id==status){
                    sID = $scope.orderStatusData[i].hooks;
                }
            }
            if(sID=='Cancelled'){
                Alertify.confirm('Are you sure to cancel this Order?').then(
                    function onOk() {
                        $scope.ShowLoader = true;
                        orderService.ChangeOrderStatus(data).then(function (response) {
                            if (response.data.error) {
                                $scope.error = true;
                                $scope.msg = response.data.msg;
                            } else {
                                $scope.error = false;
                            }
                            $scope.msg = response.data.msg; 
                            $scope.ShowLoader = false; 
                            $scope.getOrderCS();              
                        }).catch(function (e) {
                            $scope.error = true;
                            $scope.msg = e.statusText;
                            $scope.ShowLoader = false;
                        });
                    },
                    function onCancel() {
                        //console.log('you have click cancel;');
                    }
                );
            }
            else{
                $scope.ShowLoader = true; 
                orderService.ChangeOrderStatus(data).then(function (response) {
                    if (response.data.error) {
                        $scope.error = true;
                        $scope.msg = response.data.msg;
                    } else {
                        $scope.error = false;
                    }
                    $scope.ShowLoader = false; 
                    $scope.msg = response.data.msg;  
                    $scope.getOrderCS();              
                }).catch(function (e) {
                    $scope.error = true;
                    $scope.msg = e.statusText;
                    $scope.ShowLoader = false;
                });
            }

        }

        $scope.makeProcessing =function(){
            $scope.error = null;
            $scope.modalerror = null;
            $scope.msg = null;
            $scope.modalmsg = null;
            var data = {orderids: [$scope.selectedOrderID], status: 2};
            orderService.ChangeOrderStatus(data).then(function (response) {
                if (response.data.error) {
                    $scope.modalerror = true;
                    $scope.modalmsg = response.data.msg;
                } else {
                    $scope.modalerror = false;
                }
                $scope.modalmsg = response.data.msg;  
                $scope.viewOrderDetailsCS(); 
                $scope.getOrderCS();             
            }).catch(function (e) {
                $scope.modalerror = true;
                $scope.modalmsg = e.statusText;
                $scope.ShowLoader = false;
            });
        }

        $scope.getOrderCS = function () {
            $scope.ShowLoader = true;
            var data = {id: $scope.searchData.id, startdate: $scope.searchData.startdate, enddate: $scope.searchData.enddate,deliveryType:$scope.searchData.deliveryType};
            orderService.getOrder(data, $scope.start, $scope.pageSize).then(function (response) {
                if (response.error) {
                    $scope.error = true;
                    $scope.msg = response.msg;
                } 
                $scope.orderData = response.data.data.orders;
                $scope.total = response.data.data.Paginations.Total;
                $scope.ShowLoader = false;
            }).catch(function (e) {
                $scope.error = true;
                $scope.msg = e.statusText;
                $scope.ShowLoader = false;
            });
        }

        $scope.viewOrderDetailsCS = function () {
            orderService.getOrderCS({id: $scope.selectedOrderID}).then(function (response) {
                if (response.error) {
                    $scope.modalerror = true;
                    $scope.modalmsg = response.msg;
                }
                $scope.orderDetailsData = response.data.data.orders[0];
            }).catch(function (e) {
                $scope.modalerror = true;
                $scope.modalmsg = e.statusText;
            });
        }

        $scope.GetNewOrderCount = function () {
            orderService.GetNewOrderCount().then(function (response) {
                if (response.error) {
                    $scope.modalerror = true;
                    $scope.modalmsg = response.msg;
                }
                $scope.GetNewOrderCount = response.data.data;
            }).catch(function (e) {
                $scope.modalerror = true;
                $scope.modalmsg = e.statusText;
            });
        }


        $scope.addition = function (varibaleone, variabletwo) {
            var total = Math.floor(varibaleone) + Math.floor(variabletwo);
            return total;
        }

        $scope.GetOrderStatusList = function () {
            orderService.GetOrderStatusList().then(function (response) {
                if (response.error) {
                    $scope.error = true;
                    $scope.msg = response.msg;
                } 
                $scope.orderStatusData = response.data.data;
            }).catch(function (e) {
                $scope.error = true;
                $scope.msg = e.statusText;
                $scope.ShowLoader = false;
            });
        }
        $scope.GetOrderTypeList = function () {
            orderService.GetOrderTypeList().then(function (response) {
                if (response.error) {
                    $scope.error = true;
                    $scope.msg = response.msg;
                } 
                $scope.orderTypeData = response.data.data;
            }).catch(function (e) {
                $scope.error = true;
                $scope.msg = e.statusText;
                $scope.ShowLoader = false;
            });
        }

        $scope.makeShipment = function(){
            $scope.error = null;
            $scope.modalerror = null;
            $scope.msg = null;
            $scope.modalmsg = null;
            $scope.makeshipping=true;
			$scope.ShipmentForm = {
				shipperAddress: null,
				trackingNO: null
			};
        }

        $scope.processShipment = function(){
            $scope.makeshipping=true;
            $scope.error = null;
            $scope.modalerror = null;
            $scope.msg = null;
            $scope.modalmsg = null;

            var dataTracking = {order_id : $scope.selectedOrderID, shipperadress : $scope.ShipmentForm.shipperAddress, trackingid : $scope.ShipmentForm.trackingNO};
            
            orderService.UpdateTracking(dataTracking).then(function (response) {
                if (response.data.error) {
                    $scope.modalerror = true;
                    $scope.modalmsg = response.data.msg;
                } else {
                    $scope.modalerror = false;
                    $scope.makeshipping=false;
                }
                //$scope.modalmsg = response.data.msg;
            }).catch(function (e) {
                $scope.modalerror = true;
                $scope.modalmsg = e.statusText;
                $scope.ShowLoader = false;
            });

            var data = {orderids: [$scope.selectedOrderID], status: 4};
            orderService.ChangeOrderStatus(data).then(function (response) {
                if (response.data.error) {
                    $scope.modalerror = true;
                    $scope.modalmsg = response.data.msg;
                } else {
                    $scope.modalerror = false;
					$scope.makeshipping = false;
                }
                $scope.modalmsg = response.data.msg;  
                $scope.viewOrderDetailsCS(); 
                $scope.getOrderCS();             
            }).catch(function (e) {
                $scope.modalerror = true;
                $scope.modalmsg = e.statusText;
                $scope.ShowLoader = false;
            });
        }

        $scope.cancelOrder = function (data) {
            $scope.makeshipping=true;
            $scope.error = null;
            $scope.modalerror = null;
            $scope.msg = null;
            $scope.modalmsg = null;
            var data = {orderids: [$scope.selectedOrderID], status: 8};
            Alertify.confirm('Are you sure to cancel this Order?').then(
                    function onOk() {
                        orderService.ChangeOrderStatus(data).then(function (response) {
                            if (response.data.error) {
                                $scope.modalerror = true;
                                $scope.modalmsg = response.data.msg;
                            } else {
                                $scope.modalerror = false;
                            }
                            $scope.modalmsg = response.data.msg;  
                            $scope.viewOrderDetailsCS(); 
                            $scope.getOrderCS();             
                        }).catch(function (e) {
                            $scope.modalerror = true;
                            $scope.modalmsg = e.statusText;
                            $scope.ShowLoader = false;
                        });
                    },
                    function onCancel() {
                        //console.log('you have click cancel;');
                    }
            );
        }

        $scope.printDiv = function(printSectionId) {
            var innerContents = document.getElementById(printSectionId).innerHTML;
            var popupWinindow = window.open('', '_blank', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
            popupWinindow.document.open();
            popupWinindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="css/webkon-ecommerce.css" /></head><body onload="window.print(); window.close()">' + innerContents + '</html>');
            popupWinindow.document.close();
        }

        $scope.selall =function (){
            if($scope.selectAll){
                for(var i=0; i<$scope.orderData.length; i++){
                    $scope.selectedOrder[i] = $scope.orderData[i].id;
                }
            }
            else{
                $scope.selectedOrder = [];
            }
        }
        $scope.kitchenPrint=function(order){
            $scope.kitchenPrintInfo=order;
            $timeout( function(){
            var innerContents = document.getElementById('kitchenPrintdiv').innerHTML;
            var popupWinindow = window.open('', '_blank', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
            popupWinindow.document.open();
            popupWinindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="css/webkon-ecommerce.css" /></head><body onload="window.print(); window.close()">' + innerContents + '</html>');
            popupWinindow.document.close();   
            }, 500 );
                   
        }
    }]);