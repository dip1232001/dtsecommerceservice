EcommerceApp.controller('OrderdetailsController', ['$scope', 'Alertify', 'orderService', 'storeType', '$location', '$routeParams','isMobile', function ($scope, Alertify, orderService, storeType, $location, $routeParams, isMobile) {

        $scope.error = null;
        $scope.modalerror = null;
        $scope.msg = null;
        $scope.modalmsg = null;
        $scope.ShowLoader = true;   
        $scope.EmptyOrder = false;
        $scope.isM = 'Desktop';     

        $scope.makeshipping=false;

        $scope.ShipmentForm = {
            shipperAddress: null,
            trackingNO: null
        };
        
        if( isMobile.any() ) 
        {
            var ggg = isMobile.any();
            $scope.isM = 'Mobile';
            if(ggg["0"] == 'iPad'){
                $scope.isM = 'iPad';
            }        
        }

        $scope.selectedOrderID = null;

        $scope.orderData = [];
        $scope.orderDetailsData = [];
        $scope.orderStatusData = [];
        $scope.selectedOrder = [];
        $scope.PaymentStatusList = [];
        $scope.orderStatusHooksData = [];

        $scope.storeType=storeType;
        $scope.generalsettingsN = [];

        $scope.viewOrderDetails = function (id) {
            $scope.error = null;
            $scope.modalerror = null;
            $scope.msg = null;
            $scope.modalmsg = null;
            
            $scope.ShipmentForm = {
                shipperAddress: null,
                trackingNO: null
            };
            $scope.makeshipping = false;

            $scope.selectedOrderID = id;
            orderService.getOrder({id: id}).then(function (response) {
                if (response.error) {
                    $scope.modalerror = true;
                    $scope.modalmsg = response.msg;
                }
                if(response.data.data.orders.length>0){
                    $scope.orderDetailsData = response.data.data.orders[0];
                    $scope.EmptyOrder = false;
                }else{
                    $scope.orderDetailsData = [];
                    $scope.EmptyOrder = true;
                }
                //$scope.orderDetailsData = response.data.data.orders[0];
                
                console.log('orderDetailsData=>>>', $scope.orderDetailsData);
            }).catch(function (e) {
                //$scope.modalerror = true;
                //$scope.modalmsg = e.statusText;
            });
            var trgt = angular.element('.container-fluid').eq(0);
            $('html,body').animate({ scrollTop: (trgt.offset().top + 200) }, 1000);
        }

        $scope.gePaymentStatusList = function (id) {
            orderService.gePaymentStatusList().then(function (response) {
                if (response.data.error) {
                    $scope.modalerror = true;
                    $scope.modalmsg = response.data.msg;
                }
                $scope.PaymentStatusList = response.data.data;
            }).catch(function (e) {
                $scope.modalerror = true;
                $scope.modalmsg = e.statusText;
            });
        }

        $scope.GetOrderStatusList = function () {
            orderService.GetOrderStatusList().then(function (response) {
                if (response.error) {
                    $scope.error = true;
                    $scope.msg = response.msg;
                } 
                $scope.orderStatusData = response.data.data;
                
                for(var i=0; i<$scope.orderStatusData.length; i++){
                    $scope.orderStatusHooksData[$scope.orderStatusData[i].name] = $scope.orderStatusData[i].hooks;
                }
                
            }).catch(function (e) {
                $scope.error = true;
                $scope.msg = e.statusText;
                $scope.ShowLoader = false;
            });
        }

        $scope.viewOrderDetailsCS = function () {
            orderService.getOrderCS({id: $scope.selectedOrderID}).then(function (response) {
                if (response.error) {
                    $scope.modalerror = true;
                    $scope.modalmsg = response.msg;
                }
                $scope.orderDetailsData = response.data.data.orders[0];
            }).catch(function (e) {
                $scope.modalerror = true;
                $scope.modalmsg = e.statusText;
            });
        }        
        
        $scope.makeProcessing =function(statusID){
            $scope.error = null;
            $scope.modalerror = null;
            $scope.msg = null;
            $scope.modalmsg = null;
            
            var sID = null;
            for(var i=0; i<$scope.orderStatusData.length; i++){
                if($scope.orderStatusData[i].hooks==statusID){
                    sID = $scope.orderStatusData[i].id;
                }
            }
                
            var data = {orderids: [$scope.selectedOrderID], status: sID};
            orderService.ChangeOrderStatus(data).then(function (response) {
                if (response.data.error) {
                    $scope.modalerror = true;
                    $scope.modalmsg = response.data.msg;
                } else {
                    $scope.modalerror = false;
                }
                $scope.modalmsg = response.data.msg;  
                $scope.viewOrderDetailsCS();   
            }).catch(function (e) {
                $scope.modalerror = true;
                $scope.modalmsg = e.statusText;
                $scope.ShowLoader = false;
            });
            var trgt = angular.element('.container-fluid').eq(0);
            $('html,body').animate({scrollTop: trgt.offset().top}, 1000);
        } 

        $scope.addition = function (varibaleone, variabletwo) {
            var total = Math.floor(varibaleone) + Math.floor(variabletwo);
            return total;
        }

        /*$scope.GetOrderStatusList = function () {
            orderService.GetOrderStatusList().then(function (response) {
                if (response.error) {
                    $scope.error = true;
                    $scope.msg = response.msg;
                } 
                $scope.orderStatusData = response.data.data;
            }).catch(function (e) {
                $scope.error = true;
                $scope.msg = e.statusText;
                $scope.ShowLoader = false;
            });
        }*/

        $scope.makeShipment = function(){
            $scope.error = null;
            $scope.modalerror = null;
            $scope.msg = null;
            $scope.modalmsg = null;
            $scope.makeshipping=true;
            $scope.ShipmentForm = {
                shipperAddress: null,
                trackingNO: null
            };
        }

        $scope.processShipment = function(statusID){
            $scope.makeshipping=true;
            $scope.error = null;
            $scope.modalerror = null;
            $scope.msg = null;
            $scope.modalmsg = null;
            
            if ($scope.ShipmentForm.shipperAddress === null) {
                $scope.modalerror = true;
                $scope.modalmsg = "Address can't be blank";
            }
            
            if ($scope.ShipmentForm.trackingNO === null) {
                $scope.modalerror = true;
                $scope.modalmsg = "Tracking No can't be blank";
            }
            
            if($scope.modalerror == null){
                var dataTracking = {order_id : $scope.selectedOrderID, shipperadress : $scope.ShipmentForm.shipperAddress, trackingid : $scope.ShipmentForm.trackingNO};

                orderService.UpdateTracking(dataTracking).then(function (response) {
                    if (response.data.error) {
                        $scope.modalerror = true;
                        $scope.modalmsg = response.data.msg;
                    } else {
                        $scope.modalerror = false;
                        $scope.makeshipping=false;
                    }
                    //$scope.modalmsg = response.data.msg;
                }).catch(function (e) {
                    $scope.modalerror = true;
                    $scope.modalmsg = e.statusText;
                    $scope.ShowLoader = false;
                });
                
                var sID = null;
                for(var i=0; i<$scope.orderStatusData.length; i++){
                    if($scope.orderStatusData[i].hooks==statusID){
                        sID = $scope.orderStatusData[i].id;
                    }
                }

                var data = {orderids: [$scope.selectedOrderID], status: sID};
                orderService.ChangeOrderStatus(data).then(function (response) {
                    if (response.data.error) {
                        $scope.modalerror = true;
                        $scope.modalmsg = response.data.msg;
                    } else {
                        $scope.modalerror = false;
                        $scope.makeshipping = false;
                    }
                    $scope.modalmsg = response.data.msg;  
                    $scope.viewOrderDetailsCS();   
                }).catch(function (e) {
                    $scope.modalerror = true;
                    $scope.modalmsg = e.statusText;
                    $scope.ShowLoader = false;
                });
            }
            var trgt = angular.element('.container-fluid').eq(0);
            $('html,body').animate({scrollTop: trgt.offset().top}, 1000);
        }

        $scope.cancelOrder = function (statusID) {
            $scope.makeshipping=true;
            $scope.error = null;
            $scope.modalerror = null;
            $scope.msg = null;
            $scope.modalmsg = null;
            
            var sID = null;
            for(var i=0; i<$scope.orderStatusData.length; i++){
                if($scope.orderStatusData[i].hooks==statusID){
                    sID = $scope.orderStatusData[i].id;
                }
            }
            var data = {orderids: [$scope.selectedOrderID], status: sID};
            Alertify.confirm('Are you sure to cancel this Order?').then(
                    function onOk() {
                        orderService.ChangeOrderStatus(data).then(function (response) {
                            if (response.data.error) {
                                $scope.modalerror = true;
                                $scope.modalmsg = response.data.msg;
                            } else {
                                $scope.modalerror = false;
                            }
                            $scope.modalmsg = response.data.msg;  
                            $scope.viewOrderDetailsCS();  
                        }).catch(function (e) {
                            $scope.modalerror = true;
                            $scope.modalmsg = e.statusText;
                            $scope.ShowLoader = false;
                        });
                        var trgt = angular.element('.container-fluid').eq(0);
                        $('html,body').animate({scrollTop: trgt.offset().top}, 1000);
                    },
                    function onCancel() {
                        //console.log('you have click cancel;');
                    }
            );
        }

        $scope.printDiv = function(printSectionId) {
            var innerContents = document.getElementById(printSectionId).innerHTML;
            var popupWinindow = window.open('', '_blank', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
            popupWinindow.document.open();
            popupWinindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="css/webkon-ecommerce.css" /></head><body onload="window.print(); window.close()">' + innerContents + '</html>');
            popupWinindow.document.close();
        }

        if ($routeParams.hasOwnProperty('id') && typeof ($routeParams.id) !== 'undefined' && $routeParams.id > 0) {
            $scope.selectedOrderID = $routeParams.id;
        }

        if($scope.selectedOrderID){
            $scope.viewOrderDetails($scope.selectedOrderID);
        }

        $scope.changeSearchStatus = function (status) {
            var sID = null;
            for(var i=0; i<$scope.orderStatusData.length; i++){
                if($scope.orderStatusData[i].id==status){
                    sID = $scope.orderStatusData[i].hooks;
                }
            }
            
            var data = {orderids: [$scope.selectedOrderID], status: status};
            if(sID=='Cancelled'){
                Alertify.confirm('Are you sure to cancel this Order?').then(
                    function onOk() {
                        orderService.ChangeOrderStatus(data).then(function (response) {
                            if (response.data.error) {
                                $scope.modalerror = true;
                                $scope.modalmsg = response.data.msg;
                            } else {
                                $scope.modalerror = false;
                            }
                            $scope.modalmsg = response.data.msg;  
                            $scope.viewOrderDetailsCS();              
                        }).catch(function (e) {
                            $scope.modalerror = true;
                            $scope.modalmsg = e.statusText;
                            $scope.ShowLoader = false;
                        });
                    },
                    function onCancel() {
                        //console.log('you have click cancel;');
                    }
                );
            }
            else{
                orderService.ChangeOrderStatus(data).then(function (response) {
                    if (response.data.error) {
                        $scope.modalerror = true;
                        $scope.modalmsg = response.data.msg;
                    } else {
                        $scope.modalerror = false;
                    }
                    $scope.modalmsg = response.data.msg;  
                    $scope.viewOrderDetailsCS();              
                }).catch(function (e) {
                    $scope.modalerror = true;
                    $scope.modalmsg = e.statusText;
                    $scope.ShowLoader = false;
                });
            }
            var trgt = angular.element('.container-fluid').eq(0);
            $('html,body').animate({scrollTop: trgt.offset().top}, 1000);
        }

        $scope.changePaymentStatus = function (status) {
            var data = {orderids: [$scope.selectedOrderID], status: status};            
            orderService.ChangePaymentStatus(data).then(function (response) {
                if (response.data.error) {
                    $scope.modalerror = true;
                    $scope.modalmsg = response.data.msg;
                } else {
                    $scope.modalerror = false;
                    $scope.modalmsg = 'Payment status updated successfully.';
                }                  
                $scope.viewOrderDetailsCS();              
            }).catch(function (e) {
                $scope.modalerror = true;
                $scope.modalmsg = e.statusText;
                $scope.ShowLoader = false;
            });
            var trgt = angular.element('.container-fluid').eq(0);
            $('html,body').animate({scrollTop: trgt.offset().top}, 1000);
        }

        $scope.getGeneralSettingsN = function() {
            orderService.getGeneralSettingsN().then(function(response) {
                if (response.error) {
                    $scope.error = true;
                    $scope.msg = response.msg;
                } else {
                    $scope.generalsettingsN = response.data.data;
                }
            });
        };

        $scope.getGeneralSettingsN();

    }]);