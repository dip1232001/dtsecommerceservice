EcommerceApp.controller('ProductController', ['$scope', 'productService', '$timeout', '$routeParams', 'Alertify', '$location', '$window', 'isMobile', function ($scope, productService, $timeout, $routeParams, Alertify, $location, $window, isMobile) {

    $scope.error = null;
    $scope.modalerror = null;
    $scope.msg = null;
    $scope.modalmsg = null;
    $scope.isM = 'Desktop';
    if( isMobile.any() ) 
    {
        var ggg = isMobile.any();
        $scope.isM = 'Mobile';
        if(ggg["0"] == 'iPad'){
            $scope.isM = 'iPad';
        }        
    }

    $scope.category_id = null;
    $scope.selectedTab = 'General';
    
    $scope.start = 0;
    $scope.currentPage = 1;
    $scope.curPage = 1;
    $scope.pageSize = 10;
    $scope.total = null;
    $scope.hideIfEmpty = true;
    $scope.showPrevNext = true;
    $scope.showFirstLast = true;
    $scope.qrpath = '';
    $scope.categorydata = [];
    $scope.qrsize = [
        {
            "width": 32,
            "height": 32
        },
        {
            "width": 64,
            "height": 64
        },
        {
            "width": 128,
            "height": 128
        },
        {
            "width": 256,
            "height": 256
        },
        {
            "width": 512,
            "height": 512
        }
    ];

    $scope.selectedqrsize = null;


    $scope.pgHref = $location.path();    

    if ($routeParams.hasOwnProperty('page') && typeof ($routeParams.page) !== 'undefined' && $routeParams.page > 0) {
        $scope.currentPage = $routeParams.page;
    }

    if ($routeParams.hasOwnProperty('catId') && typeof ($routeParams.catId) !== 'undefined' && $routeParams.catId > 0) {
        $scope.category_id = $routeParams.catId;
    }
    $scope.product_id = null;

    $scope.newVarientOptions = {
        title: null,
        pricetype: null,
        price: null,
        sort_order: 1
    }

    $scope.newVarient = {
        name: null,
        is_required: false,
        is_multiple: false,
        sort_order: 1,
        options: [$scope.newVarientOptions]
    }

    $scope.ProductForm = {
        Name: null,
        description: null,
        sku: null,
        category_id: $scope.category_id,
        PriceTypeID: 38,
        Amount: null,
        Varient: [],
        stock: 10,
        unit: null,
        Images: null,
        ProductImages: false,
        youtube_link: null,
        image_url: false,
        videoUrl: null
    };

    $scope.productdata = [];
    $scope.productPriceType = null;
    $scope.singleproductdata = [];
    $scope.frommode = 'add';
    $scope.productImage = false;
    $scope.search = null;
    $scope.ShowLoader = true;
    $scope.selectall = false;
    $scope.productcheck = [];

    $scope.show_overlay = function () {
        $scope.showoverlay = true;
    };

    $scope.hide_overlay = function () {
        $scope.showoverlay = false;
    };

    $scope.AddNewProductModal = function () {
        $scope.ProductForm = {
            Name: null,
            description: null,
            sku: null,
            category_id: $scope.category_id,
            PriceTypeID: 38,
            Amount: null,
            Varient: [],
            stock: 10,
            unit: null,
            Images: null,
            ProductImages: false,
            image_url: false,
            youtube_link: null,
            videoUrl: null
        };
        $scope.error = null;
        $scope.modalerror = null;
        $scope.msg = null;
        $scope.modalmsg = null;
        $scope.frommode = 'add';
        $scope.productImage = false;
        if (!$scope.productPriceType) {
            $scope.getProductPriceType();
        }
        angular.element('.bs-example-modal-lg').modal('toggle');
    }

    $scope.addOption = function () {
        var newVarientOptions = {
            title: null,
            pricetype: null,
            price: null,
            sort_order: 1
        }
        var newVarient = {
            name: null,
            is_required: false,
            is_multiple: false,
            sort_order: 1,
            options: [newVarientOptions]
        }
        $scope.ProductForm.Varient.push(newVarient);
    };
    $scope.removeOption = function (index) {
        $scope.ProductForm.Varient.splice(index, 1);
    };

    $scope.addOptionRow = function (index) {
        var newVarientOptions = {
            title: null,
            pricetype: null,
            price: null,
            sort_order: 1
        }
        $scope.ProductForm.Varient[index].options.push(newVarientOptions);
    };
    $scope.removeRow = function (mainIndex, index) {
        $scope.ProductForm.Varient[mainIndex].options.splice(index, 1);
    };

    $scope.createProduct = function () {
        $scope.error = null;
        $scope.modalerror = null;
        $scope.msg = null;
        $scope.modalmsg = null;
        if ($scope.frommode == 'edit') {
            $scope.ProductForm.id = $scope.product_id;
        }

        if ($scope.ProductForm.Name === null || $scope.ProductForm.Name === '') {
            $scope.modalerror = true;
            $scope.modalmsg = "Product name can't be blank";
        } else if ($scope.ProductForm.sku === null || $scope.ProductForm.sku === '') {
            $scope.modalerror = true;
            $scope.modalmsg = "Product sku can't be blank";
        } else if ($scope.ProductForm.Amount === null || $scope.ProductForm.Amount === '') {
            $scope.modalerror = true;
            $scope.modalmsg = "Product Price can't be blank";
        } else if (!(/^(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test($scope.ProductForm.Amount))) {
            $scope.modalerror = true;
            $scope.modalmsg = "Please enter valid Product Price.";
        } else if ($scope.ProductForm.description === null || $scope.ProductForm.description === '') {
            $scope.modalerror = true;
            $scope.modalmsg = "Product description can't be blank";
        } else if ($scope.ProductForm.Varient.length > 0) {
            var keepGoing = true;
            angular.forEach($scope.ProductForm.Varient, function (value, key) {
                if (keepGoing) {
                    if (value.name === null || value.name === '') {
                        $scope.modalerror = true;
                        $scope.modalmsg = "Product option title can't be blank";
                        keepGoing = false;
                    }
                    angular.forEach(value.options, function (options, optionskey) {
                        if (keepGoing) {
                            if (options.title === null || options.title === '') {
                                $scope.modalerror = true;
                                $scope.modalmsg = "Product Variant option title can't be blank";
                                keepGoing = false;
                            } else {
                                if (options.title.toLowerCase() === 'select') {
                                    $scope.modalerror = true;
                                    $scope.modalmsg = "Invalid product Variant option title ...";
                                    keepGoing = false;
                                }
                            }
                        }
                    });
                }
            });
        }
        if ($scope.modalerror == null) {
            angular.element('#productSaveButton').button('loading');
            $scope.modalerror = null;
            $scope.modalmsg = null;
            var Images = ($scope.productImage == false) ? null : $scope.productImage;
            $scope.ProductForm.Images = [Images];
            $scope.ProductForm.image_url = $scope.ProductForm.ProductImages;

            productService.createProduct(toFormData($scope.ProductForm)).then(function (response) {
                if (response.data.error) {
                    $scope.modalerror = true;
                    $scope.modalmsg = response.data.msg;
                } else {
                    $scope.modalerror = false;
                    $scope.getProduct();
                    $scope.modalmsg = response.data.msg;
                    $scope.error = false;
                    $scope.msg = response.data.msg;
                    if ($scope.frommode == 'edit') {
                        $scope.msg = 'Product EDITED successfully';
                    }
                    angular.element('.bs-example-modal-lg').modal('toggle');
                }
                angular.element('#productSaveButton').button('reset');
            }).catch(function (e) {
                $scope.modalerror = true;
                $scope.modalmsg = e.statusText;
                angular.element('#productSaveButton').button('reset');
            });

        }
    };

    $scope.editProduct = function (product) {
        $scope.product_id = product.id;
        $scope.showoverlay = false;

        if (!$scope.productPriceType) {
            $scope.getProductPriceType();
        }

        $scope.getSingleProduct(product.id);

        $scope.error = null;
        $scope.modalerror = null;
        $scope.msg = null;
        $scope.modalmsg = null;
        $scope.frommode = 'edit';
        angular.element('.bs-example-modal-lg').modal('toggle');
    };

    $scope.getProductPriceType = function () {
        productService.getproductpricetype().then(function (response) {
            $scope.productPriceType = response.data.data;
        }).catch(function (e) {
            $scope.error = true;
            $scope.msg = e.statusText;
        });
    };

    $scope.selectproductPriceType = function (data) {
        $scope.ProductForm.unit = data.name;
    };

    $scope.getProduct = function () {
        if ($scope.currentPage > 1) {
            var Newstart = (($scope.currentPage - 1) * $scope.pageSize);
            $scope.start = Newstart;
        }

        $scope.productcheck = [];
        $scope.selectall = false;
        productService.getProduct({ category_id: $scope.category_id, showall: true, name: $scope.search }, $scope.start, $scope.pageSize).then(function (response) {
            if (response.error) {
                $scope.error = true;
                $scope.msg = response.msg;
            } else {
                //$scope.error = null;
                // $scope.msg = null;
            }
            $scope.productdata = response.data.data.Products;
            $scope.total = response.data.data.Paginations.Total;
            if ($routeParams.hasOwnProperty('page') && typeof ($routeParams.page) !== 'undefined' && $routeParams.page > 0) {
                $scope.currentPage = $routeParams.page;
            }
            $scope.ShowLoader = false;

        }).catch(function (e) {
            $scope.error = true;
            $scope.msg = e.statusText;
            $scope.ShowLoader = false;
        });
    };

    $scope.getSingleProduct = function (ProductID) {
        productService.getSingleProduct({ ProductID: ProductID }).then(function (response) {
            $scope.singleproductdata = response.data.data.ProductDetails;
            if ($scope.singleproductdata.ProductVarients.length > 0) {
                for (i = 0; i <= $scope.singleproductdata.ProductVarients.length; i++) {
                    if (typeof ($scope.singleproductdata.ProductVarients[i]) !== 'undefined') {
                        var op1 = $scope.singleproductdata.ProductVarients[i].options;
                        if (op1.length > 0) {
                            for (j = 0; j <= op1.length; j++) {
                                var op = op1[j];
                                if (typeof (op) !== 'undefined') {
                                    if (op.hasOwnProperty('type') && typeof (op.type) !== 'undefined' && op.type == 'default') {
                                        op1.splice(j, 1);
                                    }
                                }
                            }
                            $scope.singleproductdata.ProductVarients[i].options = op1;
                        }
                    }
                }
            }

            $scope.ProductForm = {
                id: $scope.singleproductdata.id,
                Name: $scope.singleproductdata.Productname,
                description: $scope.singleproductdata.description,
                sku: $scope.singleproductdata.sku,
                category_id: $scope.category_id,
                PriceTypeID: $scope.singleproductdata.PriceTypeID,
                Amount: $scope.singleproductdata.price,
                Varient: $scope.singleproductdata.ProductVarients,
                stock: $scope.singleproductdata.Stock,
                unit: $scope.singleproductdata.PriceType,
                slug: $scope.singleproductdata.slug,
                ProductImages: $scope.singleproductdata.ProductImages,
                videoUrl: $scope.singleproductdata.video_url
            };
            $scope.product_id = $scope.singleproductdata.id;
        }).catch(function (e) {
            $scope.error = true;
            $scope.msg = e.statusText;
        });
    };

    $scope.makeInactive = function (product, status) {
        Alertify.confirm('Are you sure to change this Product status ?').then(
            function onOk() {
                if (typeof (status) === "undefined") {
                    status = 1;
                }
                var data = { product_id: [product.id], is_disabled: Math.abs(status), is_featured: null };
                productService.changeProductStats(data).then(function (response) {
                    //$scope.msg = response.data.msg;
                    if (response.data.error) {
                        $scope.error = true;
                    } else {
                        product.is_disabled = status;
                        $scope.error = false;
                        $scope.msg = "Product status changed successfully";
                    }
                }).catch(function (e) {
                    $scope.error = true;
                    $scope.msg = e.statusText;
                });
            },
            function onCancel() {
                //console.log('you have click cancel;');
            }
        );
    }

    $scope.makeFeatured = function (product, featured) {
        var txt = '';
        if (featured == 1) {
            txt = 'Are you sure to make this Product as featured ?';
        }
        else {
            txt = 'Are you sure to remove this Product from featured ?'
        }
        Alertify.confirm(txt).then(
            function onOk() {
                if (typeof (status) === "undefined") {
                    status = 1;
                }
                var data = { product_id: [product.id], is_disabled: null, is_featured: Math.abs(featured) };
                productService.changeProductStats(data).then(function (response) {
                    //$scope.msg = response.data.msg;
                    if (response.data.error) {
                        $scope.error = true;
                    } else {
                        product.is_featured = featured;
                        $scope.error = false;
                        $scope.msg = "Product status changed successfully";
                    }
                }).catch(function (e) {
                    $scope.error = true;
                    $scope.msg = e.statusText;
                });
            },
            function onCancel() {
                //console.log('you have click cancel;');
            }
        );
    }

    $scope.deleteProduct = function (product) {
        Alertify.confirm('Are you sure to delete this Product?').then(
            function onOk() {
                var data = { "product_id": product.id };
                productService.deleteProduct(data).then(function (response) {
                    if (response.error) {
                        $scope.error = true;
                        $scope.msg = response.msg;
                    } else {
                        //$scope.removeProductFromList(product);
                        $scope.getProduct();
                    }
                }).catch(function (e) {
                    $scope.error = true;
                    $scope.msg = e.statusText;
                });
            },
            function onCancel() {
                //console.log('you have click cancel;');
            }
        );
    };
    $scope.removeProductFromList = function (item) {
        var index = $scope.productdata.indexOf(item);
        $scope.productdata.splice(index, 1);
    }

    $scope.searchProduct = function () {
        $scope.ShowLoader = true;
        $scope.start = 0;
        $scope.currentPage = 1;

        $scope.productcheck = [];
        $scope.selectall = false;
        //$location.search('page', null);

        $scope.getProduct();

        /*productService.getProduct({category_id: $scope.category_id, showall: true, name: $scope.search}, $scope.start, $scope.pageSize).then(function (response) {
            if (response.error) {
                $scope.error = true;
                $scope.msg = response.msg;
            }
            $scope.productdata = response.data.data.Products;
            $scope.total = response.data.data.Paginations.Total;
            $scope.ShowLoader = false;
        }).catch(function (e) {
            $scope.error = true;
            $scope.msg = e.statusText;
        });*/

    };

    $scope.resetsearchProduct = function () {
        $scope.search = null;
        $scope.start = 0;
        $scope.currentPage = 1;
        $location.search('page', null);
        $scope.getProduct();

    };

    $scope.DoCtrlPagingAct = function (text, page, pageSize, total) {
        var Newstart = ((page - 1) * pageSize);
        $scope.start = Newstart;
        $scope.ShowLoader = true;
        $scope.productcheck = [];
        $scope.selectall = false;

        $scope.getProduct();

        /*productService.getProduct({category_id: $scope.category_id, showall: true, name: $scope.search}, Newstart, $scope.pageSize).then(function (response) {
            if (response.error) {
                $scope.error = true;
                $scope.msg = response.msg;
            }
            $scope.productdata = response.data.data.Products;
            $scope.total = response.data.data.Paginations.Total;
            $scope.ShowLoader = false;
        }).catch(function (e) {
            $scope.error = true;
            $scope.msg = e.statusText;
        });*/

    };

    $scope.remove_image = function () {
        $scope.productImage = false;
        $scope.ProductForm.ProductImages = false;

    };

    $scope.selectedItemChanged = function (selectedItem) {
        for (var i = 0; i < $scope.productPriceType.length; i++) {
            if ($scope.productPriceType[i].id == selectedItem) {
                $scope.ProductForm.unit = $scope.productPriceType[i].name;
                break;
            }

        }

    }

    $scope.selall = function () {
        $scope.selectall = true;
        if ($scope.selectall) {
            for (var i = 0; i < $scope.productdata.length; i++) {
                $scope.productcheck[i] = $scope.productdata[i].id;
            }
        }
        else {
            $scope.productcheck = [];
        }
    }

    $scope.selnone = function () {
        $scope.selectall = false;
        $scope.productcheck = [];
    }

    $scope.makeallactive = function () {
        Alertify.confirm('Are you sure to change this Product status ?').then(
            function onOk() {
                var data = { product_id: $scope.productcheck, is_disabled: 0, is_featured: null };
                productService.changeProductStats(data).then(function (response) {
                    //$scope.msg = response.data.msg;
                    if (response.data.error) {
                        $scope.error = true;
                    } else {
                        $scope.error = false;
                        $scope.msg = "Product status changed successfully";
                        $scope.getProduct();
                    }
                }).catch(function (e) {
                    $scope.error = true;
                    $scope.msg = e.statusText;
                });
            },
            function onCancel() {
                //console.log('you have click cancel;');
            }
        );
    }

    $scope.makeallinactive = function () {
        Alertify.confirm('Are you sure to change this Product status ?').then(
            function onOk() {
                var data = { product_id: $scope.productcheck, is_disabled: 1, is_featured: null };
                productService.changeProductStats(data).then(function (response) {
                    //$scope.msg = response.data.msg;
                    if (response.data.error) {
                        $scope.error = true;
                    } else {
                        $scope.error = false;
                        $scope.msg = "Product status changed successfully";
                        $scope.getProduct();
                    }
                }).catch(function (e) {
                    $scope.error = true;
                    $scope.msg = e.statusText;
                });
            },
            function onCancel() {
                //console.log('you have click cancel;');
            }
        );
    }

    $scope.makeallfeatured = function () {
        Alertify.confirm('Are you sure to make these Product as featured ?').then(
            function onOk() {
                var data = { product_id: $scope.productcheck, is_disabled: null, is_featured: 1 };
                productService.changeProductStats(data).then(function (response) {
                    //$scope.msg = response.data.msg;
                    if (response.data.error) {
                        $scope.error = true;
                    } else {
                        $scope.error = false;
                        $scope.msg = "Product status changed successfully";
                        $scope.getProduct();
                    }
                }).catch(function (e) {
                    $scope.error = true;
                    $scope.msg = e.statusText;
                });
            },
            function onCancel() {
                //console.log('you have click cancel;');
            }
        );
    }

    $scope.makenonefeatured = function () {
        Alertify.confirm('Are you sure to remove these Product from featured ?').then(
            function onOk() {
                var data = { product_id: $scope.productcheck, is_disabled: null, is_featured: 0 };
                productService.changeProductStats(data).then(function (response) {
                    //$scope.msg = response.data.msg;
                    if (response.data.error) {
                        $scope.error = true;
                    } else {
                        $scope.error = false;
                        $scope.msg = "Product status changed successfully";
                        $scope.getProduct();
                    }
                }).catch(function (e) {
                    $scope.error = true;
                    $scope.msg = e.statusText;
                });
            },
            function onCancel() {
                //console.log('you have click cancel;');
            }
        );
    }

    $scope.viewQRCode = function (data) {
        //console.log(data)
        $scope.qrpath = '';
        angular.element('#qr').modal('toggle');
        var dataS = { product_id: data.id, category_id: data.category_id, image: data.imagePath };
        productService.generateProductQR(dataS).then(function (response) {
            //$scope.msg = response.data.msg;
            if (response.data.error) {
                $scope.error = true;
                $scope.msg = response.data.msg;
            } else {
                //$scope.error = false;
                //$scope.msg = "QR generated successfully";
                //$scope.getProduct();
                $scope.qrpath = response.data.data;
                //console.log(response);
            }
        }).catch(function (e) {
            $scope.error = true;
            $scope.msg = e.statusText;
        });
    }

    $scope.dd = function (img) {
        var canvas = document.createElement("canvas");
        canvas.width = img.width;
        canvas.height = img.height;
        var ctx = canvas.getContext("2d");
        ctx.drawImage(img, 0, 0);
        var dataURL = canvas.toDataURL("image/png");
        return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
    }

    $scope.generatePDF = function () {


        productService.getrawdata({ img: "http://api.qrserver.com/v1/create-qr-code/?data=" + $scope.qrpath + "&size=512x512&qzone=0" }).then(function (response) {
            //$scope.msg = response.data.msg;
            console.log(response);
            var doc = new jsPDF();

            doc.setFontSize(10);
            let width = $scope.qrsize[$scope.selectedqrsize].width;
            let height = $scope.qrsize[$scope.selectedqrsize].height;
            if ($scope.selectedqrsize > 2) {
                width = $scope.qrsize[$scope.selectedqrsize].width / 3;
                height = $scope.qrsize[$scope.selectedqrsize].height / 3;
            }
            //console.log("======>>>>",HOSTURLIMG + '/' + response.data.data);
            doc.addImage(HOSTURLIMG + '/' + response.data.data, 'PNG', 1, 1, width, height);
            doc.save(new Date().getTime() + '.pdf')
            if (response.data.error) {
                $scope.error = true;
            } else {
                //$scope.error = false;
                //$scope.msg = "QR generated successfully";
                //$scope.getProduct();
                //$scope.qrpath = response.data.data;

            }
        }).catch(function (e) {
            $scope.error = true;
            $scope.msg = e.statusText;
        });



        //var imgData = 'https://api.qrserver.com/v1/create-qr-code/?data=' + $scope.qrpath + '&size=512x512&qzone=6' + '/' + $scope.qrpath;

        /*productService.generateProductQRPDF({}).then(function (response) {
            //$scope.msg = response.data.msg;
            if (response.data.error) {
                $scope.error = true;
            } else {
                $scope.error = false;
                $scope.msg = "QR generated successfully";
                //$scope.getProduct();
                //$scope.qrpath = response.data.data;
                console.log(response);
            }
        }).catch(function (e) {
            $scope.error = true;
            $scope.msg = e.statusText;
        });*/
    }

    $scope.selQR = function (data) {
        $scope.selectedqrsize = data;
    }   

    $scope.imgErr = function(i){
        return $scope.imageupdateerrororder.includes(i);
    }

}]);