EcommerceApp.controller('ProducttestimonialsController', ['$scope', 'customerService', '$routeParams', 'Alertify', 'storeType', '$location', '$routeParams', 'isMobile', function ($scope, customerService, $routeParams, Alertify, storeType, $location, $routeParams, isMobile) {
    $scope.error = null;
    $scope.modalerror = null;
    $scope.msg = null;
    $scope.modalmsg = null;
    $scope.ShowLoader = true;
    $scope.customerID = null;
    $scope.frommode = '';
    $scope.isM = 'Desktop';
    if( isMobile.any() ) 
    {
        var ggg = isMobile.any();
        $scope.isM = 'Mobile';
        if(ggg["0"] == 'iPad'){
            $scope.isM = 'iPad';
        }        
    }

    $scope.pgHref = $location.path();

    if ($routeParams.hasOwnProperty('id') && typeof ($routeParams.id) !== 'undefined' && $routeParams.id > 0) {
        $scope.customerID = $routeParams.id;
    }

    $scope.start = 0;
    $scope.currentPage = 1;
    $scope.pageSize = 20;
    $scope.total = null;
    $scope.hideIfEmpty = true;
    $scope.showPrevNext = true;
    $scope.showFirstLast = true;

    if ($routeParams.hasOwnProperty('page') && typeof ($routeParams.page) !== 'undefined' && $routeParams.page > 0) {
        $scope.currentPage = $routeParams.page;
    }

    $scope.makeshipping = false;
    $scope.selectedOrderID = null;

    $scope.customerdata = [];
    $scope.customerDetailsdata = [];
    $scope.customerDetailsAddressdata = [];
    $scope.address_type = null;
    $scope.search = null;

    $scope.customercreatedate = new Date();

    $scope.customerform = {
        id: null,
        firstname: null,
        lastname: null,
        email: null,
        phone: null,
        password: null,
        confirm_password: null,
    }

    $scope.storeType = storeType;

    $scope.AddNewCustomerModal = function () {
        $scope.customerform = {
            id: null,
            firstname: null,
            lastname: null,
            email: null,
            phone: null,
            password: null,
            confirm_password: null,
        }
        $scope.msg = null;
        $scope.error = null;
        $scope.modalmsg = null;
        $scope.frommode = 'Add';

        angular.element('#customerModal').modal('toggle');
    }

    $scope.editCustomer = function (data) {
        $scope.customerform = {
            id: data.id,
            firstname: data.FirstName,
            lastname: data.Lastname,
            email: data.Email,
            phone: data.PhoneNumber,
            password: null,
            confirm_password: null,
        }
        $scope.msg = null;
        $scope.error = null;
        $scope.modalmsg = null;
        $scope.modalerror = null;
        $scope.frommode = 'Edit';

        angular.element('#customerModal').modal('toggle');
    }

    $scope.addCustomer = function () {
        $scope.error = null;
        $scope.modalerror = null;
        $scope.modalmsg = null;
        $scope.msg = null;

        if ($scope.customerform.name === null || $scope.customerform.name === '') {
            $scope.modalerror = true;
            $scope.modalmsg = "Customer name can't be blank";
        } else if ($scope.customerform.testimonial === null || $scope.customerform.testimonial === '') {
            $scope.modalerror = true;
            $scope.modalmsg = "Testimonial can't be blank";
        }
        if ($scope.modalerror == null) {
            angular.element('#savecustomerbutton').button('loading');
            $scope.error = null;
            $scope.modalerror = null;
            $scope.msg = null;
            $scope.modalmsg = null;
            customerService.addTestimonial({ product_id: $scope.customerID, testimonial_by: $scope.customerform.name, testimonial: $scope.customerform.testimonial }).then(function (response) {
                if (response.data.error) {
                    $scope.modalerror = true;
                    $scope.modalmsg = response.data.msg;
                } else {
                    $scope.modalerror = false;
                    $scope.getCustomer();
                    $scope.modalmsg = response.data.msg;
                    if ($scope.frommode == 'Edit') {
                        $scope.modalmsg = 'Testimonials Edited succesfully';
                    }
                }
                angular.element('#savecustomerbutton').button('reset');
            });

        }
    }

    $scope.getCustomer = function () {
        if ($scope.currentPage > 1) {
            var Newstart = (($scope.currentPage - 1) * $scope.pageSize);
            $scope.start = Newstart;
        }

        customerService.GetTestimonial({ product_id: $scope.customerID, start: $scope.start, limit: $scope.pageSize, SearchString: $scope.search }, $scope.start, $scope.pageSize).then(function (response) {
            if (response.data.error) {
                $scope.error = true;
                $scope.msg = response.data.data.msg;
            }
            $scope.customerdata = response.data.data;
            $scope.total = response.data.data.Pagination.total;
            if ($routeParams.hasOwnProperty('page') && typeof ($routeParams.page) !== 'undefined' && $routeParams.page > 0) {
                $scope.currentPage = $routeParams.page;
            }
            $scope.ShowLoader = false;
        }).catch(function (e) {
            $scope.error = true;
            $scope.msg = e.statusText;
            $scope.ShowLoader = false;
        });
    }

    $scope.searchCustomer = function () {
        $scope.start = 0;
        $scope.currentPage = 1;
        $scope.getCustomer();
    }

    $scope.DoCtrlPagingAct = function (text, page, pageSize, total) {

        var Newstart = ((page - 1) * pageSize);
        $scope.start = Newstart;
        $scope.ShowLoader = true;
        $scope.getCustomer();
    }

    $scope.deleteTestimonial = function (item) {
        customerService.deleteproductTestimonial({ product_id: $scope.customerID, testimonialid: item.id }).then(function (response) {
            if (response.data.error) {
                $scope.error = true;
                $scope.msg = response.data.data.msg;
            }
            else {
                $scope.getCustomer();
            }
        }).catch(function (e) {

        });
    }

}]);