EcommerceApp.controller('ProductdetailsController', ['$scope', 'productService', '$timeout', '$routeParams', 'Alertify', '$location', '$window','categoryService', 'isMobile' , function ($scope, productService, $timeout, $routeParams, Alertify, $location, $window, categoryService, isMobile) {

    $scope.error = null;
    $scope.modalerror = null;
    $scope.msg = null;
    $scope.modalmsg = null;
    $scope.imageupdateerror = false;
    $scope.imageupdateerrororder = [];
    $scope.isM = 'Desktop';
    if( isMobile.any() ) 
    {
        var ggg = isMobile.any();
        $scope.isM = 'Mobile';
        if(ggg["0"] == 'iPad'){
            $scope.isM = 'iPad';
        }        
    }

    $scope.product_id = null;
    $scope.category_id = null;
    $scope.selectedTab = "General";
    $scope.categorydata = [];

    $scope.newVarientOptions = {
        title: null,
        pricetype: null,
        price: null,
        sort_order: 1
    }

    $scope.newVarient = {
        name: null,
        is_required: false,
        is_multiple: false,
        sort_order: 1,
        options: [$scope.newVarientOptions]
    }

    $scope.isVirtualOptionVal = [
        {
            id : 0,
            name : 'No'
        },
        {
            id : 1,
            name : 'Yes'
        }
    ];
    
    $scope.ProductForm = {
        Name: null,
        description: null,
        sku: null,
        category_id: $scope.category_id,
        PriceTypeID: 38,
        Amount: null,
        Varient: [],
        VIDEOS: [],
        stock: 10,
        unit: null,
        Images: null,
        PRODUCTDOCUMENTS: null,
        DOCUMENTS: null,
        ARTAGS: null,
        ProductImages: false,
        youtube_link: null,
        image_url: false,
        videoUrl: null,
        is_virtual: 0
    };

    $scope.productPriceType = null;
    $scope.productDefaultARTags = null
    $scope.singleproductdata = [];
    $scope.frommode = 'add';
    $scope.productImage = false;
    $scope.productDocuments = false;
    $scope.ShowLoader = true;

    $scope.show_overlay = function () {
        $scope.showoverlay = true;
    };

    $scope.hide_overlay = function () {
        $scope.showoverlay = false;
    };

    $scope.selectTab = function (id) {
        $scope.selectedTab = id;
    }

    $scope.AddNewProduct = function () {
        $scope.ProductForm = {
            Name: null,
            description: null,
            sku: null,
            category_id: $scope.category_id,
            PriceTypeID: 38,
            Amount: null,
            Varient: [],
            VIDEOS: [],
            CATEGORIES: [],
            stock: 10,
            unit: null,
            Images: null,
            PRODUCTDOCUMENTS: null,
            DOCUMENTS: null,
            ARTAGS: null,
            ProductImages: false,
            image_url: false,
            youtube_link: null,
            videoUrl: null,
            is_virtual: 0
        };
        $scope.error = null;
        $scope.modalerror = null;
        $scope.msg = null;
        $scope.modalmsg = null;
        $scope.frommode = 'add';
        $scope.productImage = false;
        //angular.element('.bs-example-modal-lg').modal('toggle');
        if ($scope.category_id !== null) {
            $scope.ProductForm.CATEGORIES[$scope.category_id] = {
                'category_id': $scope.category_id,
                'productorder': 1,
                "status": true
            }
            /*$scope.ProductForm.CATEGORIES[$scope.category_id].category_id = $scope.category_id;
            $scope.ProductForm.CATEGORIES[$scope.category_id].productorder = 1;
            $scope.ProductForm.CATEGORIES[$scope.category_id].status = true;*/
        }
    }

    $scope.addVideo = function () {
        var newVideo = {
            id: null,
            name: null,
            videoURL: null
        }
        $scope.ProductForm.VIDEOS.push(newVideo);
    };

    $scope.removeVideo = function (index, item) {
        if (item.id != null) {
            productService.deleteproductVideo({ product_id: item.product_id, videoid: item.id }).then(function (response) {
                if (response.data.error) {
                    $scope.modalerror = true;
                    $scope.modalmsg = response.data.msg;
                } else {
                    $scope.ProductForm.VIDEOS.splice(index, 1);
                }
            }).catch(function (e) {

            });
        } else {
            $scope.ProductForm.VIDEOS.splice(index, 1);
        }
        //$scope.ProductForm.VIDEOS.splice(index, 1);
    };

    $scope.removeImage = function (index, item) {
        if (item.id != null) {
            productService.deleteproductImage({ product_id: item.product_id, imageid: item.id }).then(function (response) {
                if (response.data.error) {
                    $scope.modalerror = true;
                    $scope.modalmsg = response.data.msg;
                } else {
                    $scope.ProductForm.ProductImages.splice(index, 1);
                }
            }).catch(function (e) {

            });
        } else {
            $scope.ProductForm.ProductImages.splice(index, 1);
        }

    };

    $scope.makeDefaultImg = function (index, item) {
        Alertify.confirm('Are you sure to make this image as default?').then(
            function onOk() {
                if (item.id != null) {
                    productService.makeDefaultProductImage({
                        product_id: item.product_id,
                        imageid: item.id
                    }).then(function (response) {
                        if (response.data.error) {
                            $scope.modalerror = true;
                            $scope.modalmsg = response.data.msg;
                        } else {
                            $scope.getSingleProduct(item.product_id);
                        }
                    }).catch(function (e) {
        
                    });
                } 
            },
            function onCancel() {
            }
        );
    };

    $scope.removeDoc = function (index, item) {
        if (item.id != null) {
            productService.deleteproductDocument({ product_id: item.product_id, documentid: item.id }).then(function (response) {
                if (response.data.error) {
                    $scope.modalerror = true;
                    $scope.modalmsg = response.data.msg;
                } else {
                    $scope.ProductForm.DOCUMENTS.splice(index, 1);
                }
            }).catch(function (e) {

            });
        } else {
            $scope.ProductForm.DOCUMENTS.splice(index, 1);
        }
        //$scope.ProductForm.DOCUMENTS.splice(index, 1);
    };

    $scope.addOption = function () {
        var newVarientOptions = {
            title: null,
            pricetype: null,
            price: null,
            sort_order: 1
        }
        var newVarient = {
            name: null,
            is_required: false,
            is_multiple: false,
            sort_order: 1,
            options: [newVarientOptions]
        }
        $scope.ProductForm.Varient.push(newVarient);
    };
    $scope.removeOption = function (index) {
        $scope.ProductForm.Varient.splice(index, 1);
    };

    $scope.addOptionRow = function (index) {
        var newVarientOptions = {
            title: null,
            pricetype: null,
            price: null,
            sort_order: 1
        }
        $scope.ProductForm.Varient[index].options.push(newVarientOptions);
    };
    $scope.removeRow = function (mainIndex, index) {
        $scope.ProductForm.Varient[mainIndex].options.splice(index, 1);
    };

    $scope.onFileChange = function (event) {
        let files = event.target.files;
    }

    $scope.getTheFiles = function ($files) {
        angular.forEach($files, function (value, key) {
            // formdata.append(key, value);
            $scope.productImage[key] = value;
        });
    };

    $scope.createProduct = function () {
        $scope.error = null;
        $scope.modalerror = null;
        $scope.msg = null;
        $scope.modalmsg = null;
        if ($scope.frommode == 'edit') {
            $scope.ProductForm.id = $scope.product_id;
        }

        if ($scope.ProductForm.Name === null || $scope.ProductForm.Name === '') {
            $scope.modalerror = true;
            $scope.modalmsg = "Product name can't be blank";
        } else if ($scope.ProductForm.sku === null || $scope.ProductForm.sku === '') {
            $scope.modalerror = true;
            $scope.modalmsg = "Product sku can't be blank";
        } else if ($scope.ProductForm.Amount === null || $scope.ProductForm.Amount === '') {
            $scope.modalerror = true;
            $scope.modalmsg = "Product Price can't be blank";
        } else if (!(/^(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test($scope.ProductForm.Amount))) {
            $scope.modalerror = true;
            $scope.modalmsg = "Please enter valid Product Price.";
        } else if ($scope.ProductForm.description === null || $scope.ProductForm.description === '') {
            $scope.modalerror = true;
            $scope.modalmsg = "Product description can't be blank";
        } else if ($scope.ProductForm.Varient.length > 0) {
            var keepGoing = true;
            angular.forEach($scope.ProductForm.Varient, function (value, key) {
                if (keepGoing) {
                    if (value.name === null || value.name === '') {
                        $scope.modalerror = true;
                        $scope.modalmsg = "Product option title can't be blank";
                        keepGoing = false;
                    }
                    angular.forEach(value.options, function (options, optionskey) {
                        if (keepGoing) {
                            if (options.title === null || options.title === '') {
                                $scope.modalerror = true;
                                $scope.modalmsg = "Product Variant option title can't be blank";
                                keepGoing = false;
                            } else {

                                if (typeof (options.title) === "string" && options.title.toLowerCase() === 'select') {
                                    $scope.modalerror = true;
                                    $scope.modalmsg = "Invalid product Variant option title ...";
                                    keepGoing = false;
                                }
                            }
                        }
                    });
                }
            });
        } else if($scope.productImage !== false){
            if($scope.imageupdateerror){
                $scope.modalerror = true;
                $scope.modalmsg = "Plese upload proper size image. (one of your image height is < 500px)";
            }
            /*angular.forEach($scope.productImage, function (value, key) {
                ddd = $scope.readImageFile(value);
                if(ddd<500){
                    
                }
            });*/
        }
        if ($scope.modalerror == null) {
            angular.element('#productSaveButton').button('loading');
            $scope.modalerror = null;
            $scope.modalmsg = null;
            var Images = ($scope.productImage == false) ? null : $scope.productImage;
            $scope.ProductForm.Images = Images;

            var Documents = ($scope.productDocuments == false) ? null : $scope.productDocuments;
            $scope.ProductForm.PRODUCTDOCUMENTS = Documents;

            $scope.ProductForm.image_url = $scope.ProductForm.ProductImages;
            //angular.element('#productSaveButton').button('reset');
            productService.createProduct(toFormData($scope.ProductForm)).then(function (response) {
                if (response.data.error) {
                    $scope.modalerror = true;
                    $scope.modalmsg = response.data.msg;
                } else {
                    $scope.modalerror = false;
                    $scope.modalmsg = response.data.msg;
                    if ($scope.frommode == 'edit') {
                        $scope.modalmsg = 'Product edited successfully';
                    }
                    $scope.getSingleProduct(response.data.data.id);
                    $scope.productImage = false;

                    //setTimeout(function(){ $window.location.href = 'categoryproducts/' + $scope.ProductForm.category_id; }, 500);
                    if($scope.category_id !== null){
                        setTimeout(function(){ $window.location.href = 'products/' + $scope.category_id; }, 500);
                    }else{
                        setTimeout(function(){ $window.location.href = 'products/'; }, 500);
                    }
                }
                angular.element('#productSaveButton').button('reset');
            }).catch(function (e) {
                $scope.modalerror = true;
                $scope.modalmsg = e.statusText;
                angular.element('#productSaveButton').button('reset');
            });
            var trgt = angular.element('.container-fluid').eq(0);
            $('html,body').animate({ scrollTop: trgt.offset().top }, 1000);
        }
    };

    $scope.createProductAN = function () {
        $scope.error = null;
        $scope.modalerror = null;
        $scope.msg = null;
        $scope.modalmsg = null;
        if ($scope.frommode == 'edit') {
            $scope.ProductForm.id = $scope.product_id;
        }

        if ($scope.ProductForm.Name === null || $scope.ProductForm.Name === '') {
            $scope.modalerror = true;
            $scope.modalmsg = "Product name can't be blank";
        } else if ($scope.ProductForm.sku === null || $scope.ProductForm.sku === '') {
            $scope.modalerror = true;
            $scope.modalmsg = "Product sku can't be blank";
        } else if ($scope.ProductForm.Amount === null || $scope.ProductForm.Amount === '') {
            $scope.modalerror = true;
            $scope.modalmsg = "Product Price can't be blank";
        } else if (!(/^(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test($scope.ProductForm.Amount))) {
            $scope.modalerror = true;
            $scope.modalmsg = "Please enter valid Product Price.";
        } else if ($scope.ProductForm.description === null || $scope.ProductForm.description === '') {
            $scope.modalerror = true;
            $scope.modalmsg = "Product description can't be blank";
        } else if ($scope.ProductForm.Varient.length > 0) {
            var keepGoing = true;
            angular.forEach($scope.ProductForm.Varient, function (value, key) {
                if (keepGoing) {
                    if (value.name === null || value.name === '') {
                        $scope.modalerror = true;
                        $scope.modalmsg = "Product option title can't be blank";
                        keepGoing = false;
                    }
                    angular.forEach(value.options, function (options, optionskey) {
                        if (keepGoing) {
                            if (options.title === null || options.title === '') {
                                $scope.modalerror = true;
                                $scope.modalmsg = "Product Variant option title can't be blank";
                                keepGoing = false;
                            } else {

                                if (typeof (options.title) === "string" && options.title.toLowerCase() === 'select') {
                                    $scope.modalerror = true;
                                    $scope.modalmsg = "Invalid product Variant option title ...";
                                    keepGoing = false;
                                }
                            }
                        }
                    });
                }
            });
        } else if($scope.productImage !== false){
            if($scope.imageupdateerror){
                $scope.modalerror = true;
                $scope.modalmsg = "Plese upload proper size image. (one of your image height is < 500px)";
            }
            /*angular.forEach($scope.productImage, function (value, key) {
                ddd = $scope.readImageFile(value);
                if(ddd<500){
                    
                }
            });*/
        }
        if ($scope.modalerror == null) {
            angular.element('#productSaveButtonAN').button('loading');
            $scope.modalerror = null;
            $scope.modalmsg = null;
            var Images = ($scope.productImage == false) ? null : $scope.productImage;
            $scope.ProductForm.Images = Images;

            var Documents = ($scope.productDocuments == false) ? null : $scope.productDocuments;
            $scope.ProductForm.PRODUCTDOCUMENTS = Documents;

            $scope.ProductForm.image_url = $scope.ProductForm.ProductImages;
            //angular.element('#productSaveButton').button('reset');
            productService.createProduct(toFormData($scope.ProductForm)).then(function (response) {
                if (response.data.error) {
                    $scope.modalerror = true;
                    $scope.modalmsg = response.data.msg;
                } else {
                    $scope.modalerror = false;
                    $scope.modalmsg = response.data.msg;
                    if ($scope.frommode == 'edit') {
                        $scope.modalmsg = 'Product edited successfully';
                    }
                    //$scope.getSingleProduct(response.data.data.id);
                    $scope.AddNewProduct();
                    $scope.productImage = false;

                    //setTimeout(function(){ $window.location.href = 'categoryproducts/' + $scope.ProductForm.category_id; }, 500);
                }
                angular.element('#productSaveButtonAN').button('reset');
            }).catch(function (e) {
                $scope.modalerror = true;
                $scope.modalmsg = e.statusText;
                angular.element('#productSaveButtonAN').button('reset');
            });
            var trgt = angular.element('.container-fluid').eq(0);
            $('html,body').animate({ scrollTop: trgt.offset().top }, 1000);
        }
    };

    $scope.readImageFile = function(file) {
        //var reader = new FileReader(); // CREATE AN NEW INSTANCE.

        //reader.onload = function (e) {
            var _URL = window.URL || window.webkitURL;
            var img = new Image();   

            img.onload = function () {
                var w = this.width;
                if(w<500){
                    $scope.modalerror = true;
                    $scope.modalmsg = "Image error";
                }
                //return w;
            }
            img.src = _URL.createObjectURL(file);

        //};
        //reader.readAsDataURL(file);
    }

    $scope.editProduct = function (id) {
        $scope.showoverlay = false;

        $scope.getSingleProduct(id);

        $scope.error = null;
        $scope.modalerror = null;
        $scope.msg = null;
        $scope.modalmsg = null;
        $scope.frommode = 'edit';
    };

    $scope.reqVal = [
        {
            id : 0,
            name : 'No'
        },
        {
            id : 1,
            name : 'Yes'
        }
    ];

    $scope.getProductPriceType = function () {
        productService.getproductpricetype().then(function (response) {
            $scope.productPriceType = response.data.data;
        }).catch(function (e) {
            $scope.error = true;
            $scope.msg = e.statusText;
        });

    };

    $scope.getDefinedARTagsByBU = function () {
        productService.getDefinedARTagsByBU().then(function (response) {
            if(response.data.data.hasOwnProperty("BU")){
                $scope.productDefaultARTags = response.data.data.BU.concat(response.data.data.default);
            }else{
                $scope.productDefaultARTags = response.data.data.default;
            }
            
        }).catch(function (e) {
            $scope.error = true;
            $scope.msg = e.statusText;
        });

    };

    $scope.selectproductPriceType = function (data) {
        $scope.ProductForm.unit = data.name;
    };

    $scope.getSingleProduct = function (ProductID) {
        productService.getSingleProduct({ ProductID: ProductID }).then(function (response) {
            $scope.singleproductdata = response.data.data.ProductDetails;
            if ($scope.singleproductdata.ProductVarients.length > 0) {
                for (i = 0; i <= $scope.singleproductdata.ProductVarients.length; i++) {
                    //$scope.singleproductdata.ProductVarients[0].is_required = 1;
                    if($scope.singleproductdata.ProductVarients[0].is_required){
                        $scope.singleproductdata.ProductVarients[0].is_required = 1;

                    }else{
                        $scope.singleproductdata.ProductVarients[0].is_required = 0;
                    }
                    if (typeof ($scope.singleproductdata.ProductVarients[i]) !== 'undefined') {
                        var op1 = $scope.singleproductdata.ProductVarients[i].options;
                        if (op1.length > 0) {
                            for (j = 0; j <= op1.length; j++) {
                                var op = op1[j];
                                if (typeof (op) !== 'undefined') {
                                    if (op.hasOwnProperty('type') && typeof (op.type) !== 'undefined' && op.type == 'default') {
                                        op1.splice(j, 1);
                                    }
                                }
                            }
                            $scope.singleproductdata.ProductVarients[i].options = op1;
                        }
                    }
                }
            }
            //setTimeout(function(){
                $scope.ProductForm = {
                    id: $scope.singleproductdata.id,
                    Name: $scope.singleproductdata.Productname,
                    description: $scope.singleproductdata.description,
                    sku: $scope.singleproductdata.sku,
                    //category_id: $scope.singleproductdata.category_id,
                    CATEGORIES:$scope.singleproductdata.CATEGORIESFORADMIN,
                    PriceTypeID: $scope.singleproductdata.PriceTypeID,
                    Amount: $scope.singleproductdata.price,
                    Varient: $scope.singleproductdata.ProductVarients,
                    stock: $scope.singleproductdata.Stock,
                    unit: $scope.singleproductdata.PriceType,
                    slug: $scope.singleproductdata.slug,
                    ProductImages: $scope.singleproductdata.ProductImages,
                    videoUrl: $scope.singleproductdata.video_url,
                    VIDEOS: $scope.singleproductdata.VIDEOS,
                    DOCUMENTS: $scope.singleproductdata.DOCUMENTS,
                    ARTAGS: $scope.singleproductdata.ARTAGS,
                    is_virtual: $scope.singleproductdata.is_virtual
                };
                if($scope.ProductForm.PriceTypeID == ''){
                    $scope.ProductForm.PriceTypeID = 38;
                    $scope.ProductForm.unit = Others;
                }
    
            //},100);
            
            $scope.product_id = $scope.singleproductdata.id;
            $scope.frommode = 'edit';
        }).catch(function (e) {
            $scope.error = true;
            $scope.msg = e.statusText;
        });
    };


    $scope.remove_image = function () {
        $scope.productImage = false;
        $scope.ProductForm.ProductImages = false;

    };

    $scope.selectedItemChanged = function (selectedItem) {
        for (var i = 0; i < $scope.productPriceType.length; i++) {
            if ($scope.productPriceType[i].id == selectedItem) {
                $scope.ProductForm.unit = $scope.productPriceType[i].name;
                break;
            }

        }

    }

    $scope.getProductPriceType();
    $scope.getDefinedARTagsByBU();

    if ($routeParams.hasOwnProperty('id') && typeof ($routeParams.id) !== 'undefined' && $routeParams.id > 0) {
        $scope.product_id = $routeParams.id;
    }
    if ($routeParams.hasOwnProperty('catId') && typeof ($routeParams.catId) !== 'undefined' && $routeParams.catId > 0) {
        $scope.category_id = $routeParams.catId;
    }

    if ($scope.product_id == null) {
        $scope.AddNewProduct();
    } else {
        $scope.editProduct($scope.product_id);
    }

    $scope.getCategoryTree = function() {
        categoryService.getCategoryTree({parent_id: 0}).then(function(response) {
            if (response.error) {
                $scope.error = true;
                $scope.msg = response.msg;
            }
            $scope.categorydata = response.data.data;
        }).catch(function(e){

        });
    };

    $scope.getCategoryTree();

    $scope.enablecat= function(node){
        //if(ProductForm.CATEGORIES[node.id].status){
            $scope.ProductForm.CATEGORIES[node.id].category_id  = node.id;
            //this.ProductForm.CATEGORIES[node.id]={category_id : node.id, productorder: 1};
        //}else{
           //delete this.ProductForm.CATEGORIES[node.id];
        //}
        
    }

    $scope.imgErr = function(i){
        return $scope.imageupdateerrororder.includes(i.toString());
    }

    $scope.getIMG = function(imgobj){
        var _URL = window.URL || window.webkitURL;
        return _URL.createObjectURL(imgobj);
    }

    $scope.removetempImage = function(i){
        var newFileList = Array.from($scope.productImage);
        newFileList.splice(i,1);
        $scope.productImage = newFileList;
        $scope.checkImageSize();
    }

    $scope.checkImageSize = function(){
        $scope.imageupdateerror = false;
        var _URL = window.URL || window.webkitURL;
        var files = $scope.productImage;
        for (var i = 0; i < files.length; i++) {
            var img = new Image();
            img.onload = function () {
                if (this.height < 500) {
                    $scope.imageupdateerror = true;
                    $scope.imageupdateerrororder.push(this.getAttribute('key'));
                }
            };
            img.src = _URL.createObjectURL(files[i]);
            img.setAttribute("key",i);        }
    }

    console.log("isMobile.any()==>",isMobile.any());

}]);