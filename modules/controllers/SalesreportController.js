EcommerceApp.controller('SalesreportController', ['$scope', 'Alertify', 'salesreportService', 'storeType', 'isMobile', function ($scope, Alertify, salesreportService, storeType, isMobile) {

        $scope.error = null;
        $scope.modalerror = null;
        $scope.msg = null;
        $scope.modalmsg = null;
        $scope.ShowLoader = true;
        $scope.isM = 'Desktop';
        if( isMobile.any() ) 
        {
            var ggg = isMobile.any();
            $scope.isM = 'Mobile';
            if(ggg["0"] == 'iPad'){
                $scope.isM = 'iPad';
            }        
        }

        $scope.start = 0;
        $scope.currentPage = 1;
        $scope.pageSize = 40;
        $scope.total = 5;
        $scope.hideIfEmpty = true;
        $scope.showPrevNext = true;
        $scope.showFirstLast = true;

        $scope.searchData = {
            startdate: null,
            enddate: null,
            period: 'Date Range',
            status: 'Select',
            statusv: null,
        };

        $scope.searchDataN = {
            startdate: null,
            enddate: null,
        }

        
        $scope.GetReports = [];
        $scope.orderStatusData = [];
        $scope.showdates = true;
        
        $scope.GetReports = function () {
            $scope.error = null;
            $scope.modalerror = null;
            $scope.msg = null;
            $scope.modalmsg = null;
            $scope.ShowLoader = true;

            $scope.searchDataN = {
                startdate: $scope.searchData.startdate,
                enddate: $scope.searchData.enddate,
            }        

            var data = {startdate: $scope.searchDataN.startdate, enddate: $scope.searchDataN.enddate, status: $scope.searchData.statusv};
            salesreportService.GetReports(data, $scope.start, $scope.pageSize).then(function (response) {
                if (response.error) {
                    $scope.error = true;
                    $scope.msg = response.msg;
                } else {
                    $scope.error = null;
                    $scope.msg = null;
                }
                $scope.GetReportsData = response.data.data;
                //$scope.total = response.data.data.Paginations.Total;
                $scope.searchDataN = {
                    startdate: null,
                    enddate: null,
                }
                $scope.ShowLoader = false;
            }).catch(function (e) {
                $scope.error = true;
                $scope.msg = e.statusText;
                $scope.ShowLoader = false;
            });
        }

        $scope.search = function () {
            $scope.GetReports();
        }

        $scope.resetSearch = function () {
            $scope.searchData = {
                startdate: null,
                enddate: null,
                period: 'Date Range',
                status: 'Select',
                statusv: null,
            };
			$scope.showdates = true;

            $scope.GetReports();
        }

        $scope.DoCtrlPagingAct = function (text, page, pageSize, total) {
            var Newstart = ((page - 1) * pageSize);
            $scope.ShowLoader = true;
            $scope.searchDataN = {
                startdate: $scope.searchData.startdate,
                enddate: $scope.searchData.enddate,
            }        

            var data = {startdate: $scope.searchDataN.startdate, enddate: $scope.searchDataN.enddate};
            salesreportService.GetReports(data, $scope.start, $scope.pageSize).then(function (response) {
                if (response.error) {
                    $scope.error = true;
                    $scope.msg = response.msg;
                } else {
                    $scope.error = null;
                    $scope.msg = null;
                }
                $scope.GetReportsData = response.data.data;
                //$scope.total = response.data.data.Paginations.Total;
                $scope.searchDataN = {
                    startdate: null,
                    enddate: null,
                }
                $scope.ShowLoader = false;
            }).catch(function (e) {
                $scope.error = true;
                $scope.msg = e.statusText;
                $scope.ShowLoader = false;
            });
        }

        $scope.changeperiod = function(data){
            $scope.searchData.period = data;
            if(data=='Current Month'){
                $scope.showdates = false;
                var date = new Date();
                var fdate = new Date(date.getFullYear(), date.getMonth(), 1);
                var ldate = new Date(date.getFullYear(), date.getMonth() + 1, 0);

                var D1 = fdate.getFullYear();
                var D2 = (fdate.getMonth())+1;
                var D3 = fdate.getDate();

                var D4 = ldate.getFullYear();
                var D5 = (ldate.getMonth())+1;
                var D6 = ldate.getDate();


                $scope.searchData.startdate = D1 + "-" + D2 + "-" + D3;
                $scope.searchData.enddate = D4 + "-" + D5 + "-" + D6;
            }

            if(data=='Current Year'){
                $scope.showdates = false;
                var date = new Date();

                var D1 = date.getFullYear();
                var D2 = 1;
                var D3 = 1;

                var D4 = date.getFullYear();
                var D5 = 12;
                var D6 = 31;


                $scope.searchData.startdate = D1 + "-" + D2 + "-" + D3;
                $scope.searchData.enddate = D4 + "-" + D5 + "-" + D6;
            }  
            if(data=='Date Range'){
                $scope.searchData = {
                    startdate: null,
                    enddate: null,
                    status: 'Select',
                    statusv: null,
                };
                $scope.showdates = true;
            } 
            $scope.searchData.period = data;

        }

        $scope.changestatus = function(data,datav){
            $scope.searchData.status = data;
            $scope.searchData.statusv = datav;
        }

        $scope.GetOrderStatusList = function () {
            salesreportService.GetOrderStatusList().then(function (response) {
                if (response.error) {
                    $scope.error = true;
                    $scope.msg = response.msg;
                } 
                $scope.orderStatusData = response.data.data;
            }).catch(function (e) {
                $scope.error = true;
                $scope.msg = e.statusText;
                $scope.ShowLoader = false;
            });
        }

        $scope.GetReports();
        $scope.GetOrderStatusList();

    }]);