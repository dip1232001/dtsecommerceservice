EcommerceApp.factory('CommonService', ['$http', 'serverUrls', 'Base64', '$cookies', '$q', function ($http, serverUrls, Base64, $cookies, $q) {
        var data = {};
        var Authcanceller = null;
        var canceller = null;

        data.http = function (params) {
            var Token = $cookies.get('AT');
            var BASEKEY = $cookies.get('K');
            if (!Token) {
                if (Authcanceller) {
                    Authcanceller.resolve();
                }
                Authcanceller = $q.defer();
                return $http({
                    url: serverUrls.authenticate,
                    method: 'POST',
                    data: {"grant_type": "client_credentials"},
                    headers: {
                        'Authorization': 'Basic ' + BASEKEY// Base64.encode(USERNAME + ':' + PASSWORD)
                    },
                    timeout: Authcanceller.promise
                }).then(function (response) {
                    var parsedDate = new Date()
                    var newDate = new Date(parsedDate.getTime() + (1000 * response.data.expires_in) - 20);
                    var expStr = moment(newDate).format('LLLL');
                    $cookies.put('AT', response.data.access_token, {'expires': expStr});
                    return  response.data.access_token;
                }).then(function (Token) {
                    if (!params.hasOwnProperty('url') || typeof (params.url) === 'undefined') {
                        return {error: true};
                    }
                    params.url = updateQueryStringParameter(params.url, 'access_token', Token);
                    return $http(params);
                });
            } else {
                if (!params.hasOwnProperty('url') || typeof (params.url) === 'undefined') {
                    return false;
                }
                params.url = updateQueryStringParameter(params.url, 'access_token', Token);
                return $http(params);
            }
        };

        data.getGeneralSettings = function () {
            if (canceller) {
                canceller.resolve();
            }
            canceller = $q.defer();
            return data.http({
                url: serverUrls.getgeneralsettings,
                method: 'GET',
                timeout: canceller.promise
            });
        };

        return data;
    }]);


