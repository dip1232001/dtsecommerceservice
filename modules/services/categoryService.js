EcommerceApp.factory('categoryService', ['serverUrls', 'CommonService', '$q', function (serverUrls, CommonService, $q) {
        var data = {};
        var canceller = null;
        var treecanceller = null;
        var uoceller = null;
        data.createCategory = function (Data) {
            if (canceller) {
                canceller.resolve();
            }
            canceller = $q.defer();
            return CommonService.http({
                url: serverUrls.createcategory,
                method: 'POST',
                data: Data,
                headers: {'Content-Type': undefined},
                transformRequest: angular.identity,
                timeout: canceller.promise
            });
        };

        data.getCategory = function (Data) {
            if (canceller) {
                canceller.resolve();
            }
            canceller = $q.defer();
            return CommonService.http({
                url: serverUrls.getcategory,
                method: 'GET',
                params: Data,
                timeout: canceller.promise
            });
        };

        data.deleteCategory = function (Data) {
            if (canceller) {
                canceller.resolve();
            }
            canceller = $q.defer();
            return CommonService.http({
                url: serverUrls.deletecategory,
                method: 'POST',
                data: Data,
                timeout: canceller.promise
            });
        };

        data.getCategoryTree = function (Data) {
            if (treecanceller) {
                treecanceller.resolve();
            }
            treecanceller = $q.defer();
            return CommonService.http({
                url: serverUrls.getCategoryTree,
                method: 'GET',
                params: Data,
                timeout: treecanceller.promise
            });
        };

        data.updateCategorieOrder = function (Data) {
            if (uoceller) {
                uoceller.resolve();
            }
            uoceller = $q.defer();
            return CommonService.http({
                url: serverUrls.updateCategorieOrder,
                method: 'POST',
                data: Data,
                timeout: uoceller.promise
            });
        };

        return data;
    }]);


