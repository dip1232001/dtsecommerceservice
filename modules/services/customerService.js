EcommerceApp.factory('customerService', ['serverUrls', 'CommonService', '$q', function (serverUrls, CommonService, $q) {
        var data = {};
        var canceller = null;
        var addresscanceller = null;
        var ordercanceller = null;
        var orderstatuscanceller = null;
        var cancellerCS = null;

        data.createCustomer = function (Data) {
            if (canceller) {
                canceller.resolve();
            }
            canceller = $q.defer();
            return CommonService.http({
                url: serverUrls.createcustomer,
                method: 'POST',
                data: Data,
                headers: {'Content-Type': undefined},
                transformRequest: angular.identity,
                timeout: canceller.promise
            });
        };

        data.getCustomer = function (Data,Start,Limit) {
            var p={start:Start,limit:Limit};
            if (canceller) {
                canceller.resolve();
            }
            canceller = $q.defer();
            return CommonService.http({
                url: serverUrls.getcustomer,
                method: 'POST',
                data: Data,
                params: p,
                timeout: canceller.promise
            });
        };

        data.getCustomerDetails = function (data) {
            if (canceller) {
                canceller.resolve();
            }
            canceller = $q.defer();
            return CommonService.http({
                url: serverUrls.getcustomerdetails,
                method: 'POST',
                data : data,
                timeout: canceller.promise
            });
        };

        data.addBillingAddress = function (Data) {
            if (addresscanceller) {
                addresscanceller.resolve();
            }
            addresscanceller = $q.defer();
            return CommonService.http({
                url: serverUrls.addcustomerbillingaddress,
                method: 'POST',
                data: Data,
                headers: {'Content-Type': undefined},
                transformRequest: angular.identity,
                timeout: addresscanceller.promise
            });
        };

        data.addShippingAddress = function (Data) {
            if (addresscanceller) {
                addresscanceller.resolve();
            }
            addresscanceller = $q.defer();
            return CommonService.http({
                url: serverUrls.addcustomershippingaddress,
                method: 'POST',
                data: Data,
                headers: {'Content-Type': undefined},
                transformRequest: angular.identity,
                timeout: addresscanceller.promise
            });
        };

        data.getBillingAddress = function (data) {
            var p={customer_id:data};
            if (addresscanceller) {
                addresscanceller.resolve();
            }
            addresscanceller = $q.defer();
            return CommonService.http({
                url: serverUrls.getcustomerbillingaddress,
                method: 'POST',
                data : p,
                timeout: addresscanceller.promise
            });
        };

        data.getShippingAddress = function (data) {
            var p={customer_id:data};
            if (addresscanceller) {
                addresscanceller.resolve();
            }
            addresscanceller = $q.defer();
            return CommonService.http({
                url: serverUrls.getcustomershippingaddress,
                method: 'POST',
                data : p,
                timeout: addresscanceller.promise
            });
        };

        data.setDefaultBillingAddress = function (data) {
            if (addresscanceller) {
                addresscanceller.resolve();
            }
            addresscanceller = $q.defer();
            return CommonService.http({
                url: serverUrls.setDefaultBillingAddress,
                method: 'POST',
                data : data,
                timeout: addresscanceller.promise
            });
        };

        data.setDefaultShippingAddress = function (data) {
            if (addresscanceller) {
                addresscanceller.resolve();
            }
            addresscanceller = $q.defer();
            return CommonService.http({
                url: serverUrls.setDefaultShippingAddress,
                method: 'POST',
                data : data,
                timeout: addresscanceller.promise
            });
        };

        data.DeleteCustomerAdress = function (data) {
            if (addresscanceller) {
                addresscanceller.resolve();
            }
            addresscanceller = $q.defer();
            return CommonService.http({
                url: serverUrls.DeleteCustomerAdress,
                method: 'POST',
                data : data,
                timeout: addresscanceller.promise
            });
        };

        data.getOrder = function (data,start,limit) {
            var p={start:start,limit:limit};
            if (ordercanceller) {
                ordercanceller.resolve();
            }
            ordercanceller = $q.defer();
            return CommonService.http({
                url: serverUrls.getorder,
                method: 'POST',
                data: data,
                params: p,
                timeout: ordercanceller.promise
            });
        };

        data.GetOrderStatusList = function () {
            if (orderstatuscanceller) {
                orderstatuscanceller.resolve();
            }
            orderstatuscanceller = $q.defer();
            return CommonService.http({
                url: serverUrls.GetOrderStatusList,
                method: 'GET',
                timeout: orderstatuscanceller.promise
            });
        };

        data.getOrderCS = function (data,start,limit) {
            var p={start:start,limit:limit};
            if (cancellerCS) {
                cancellerCS.resolve();
            }
            cancellerCS = $q.defer();
            return CommonService.http({
                url: serverUrls.getorder,
                method: 'POST',
                data: data,
                params: p,
                timeout: cancellerCS.promise
            });
        };

        data.ChangeOrderStatus = function (data) {
            if (orderstatuscanceller) {
                orderstatuscanceller.resolve();
            }
            orderstatuscanceller = $q.defer();
            return CommonService.http({
                url: serverUrls.ChangeOrderStatus,
                method: 'POST',
                data : data,
                timeout: orderstatuscanceller.promise
            });
        };

        data.addTestimonial = function (Data) {
            if (canceller) {
                canceller.resolve();
            }
            canceller = $q.defer();
            return CommonService.http({
                url: serverUrls.AddTestimonial,
                method: 'POST',
                data: Data,
                timeout: canceller.promise
            });
        };
        
        data.GetTestimonial = function (Data,Start,Limit) {
            var p={start:Start,limit:Limit};
            if (canceller) {
                canceller.resolve();
            }
            canceller = $q.defer();
            return CommonService.http({
                url: serverUrls.GetTestimonial,
                method: 'POST',
                data: Data,
                params: p,
                timeout: canceller.promise
            });
        };

        data.deleteproductTestimonial = function (Data) {
            if (canceller) {
                canceller.resolve();
            }
            canceller = $q.defer();
            return CommonService.http({
                url: serverUrls.deleteproductTestimonial,
                method: 'POST',
                data: Data,
                timeout: canceller.promise
            });
        };

        return data;
    }]);


