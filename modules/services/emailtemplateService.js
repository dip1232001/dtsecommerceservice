EcommerceApp.factory('emailtemplateService', ['serverUrls', 'CommonService', '$q', function (serverUrls, CommonService, $q) {
        var data = {};
        var canceller = null;
        
        data.getEmailList = function (Data) {
            if (canceller) {
                canceller.resolve();
            }
            canceller = $q.defer();
            return CommonService.http({
                url: serverUrls.getEmailList,
                method: 'GET',
                params: Data,
                timeout: canceller.promise
            });
        };

        data.getEmailDetails = function (Data) {
            if (canceller) {
                canceller.resolve();
            }
            canceller = $q.defer();
            return CommonService.http({
                url: serverUrls.getEmailDetails,
                method: 'POST',
                data: Data,
                timeout: canceller.promise
            });
        };
        
        data.editEmailDetails = function (Data) {
            if (canceller) {
                canceller.resolve();
            }
            canceller = $q.defer();
            return CommonService.http({
                url: serverUrls.editEmailDetails,
                method: 'POST',
                data: Data,
                timeout: canceller.promise
            });
        };

        
        return data;
    }]);


