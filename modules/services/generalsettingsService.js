EcommerceApp.factory('generalsettingsService', ['serverUrls', 'CommonService', '$q', function (serverUrls, CommonService, $q) {
        var data = {};
        var canceller = null;
        var cancellerCurrency = null;
        var cancellerGeneralSettingsG = null;
        var saveGeneralSettingscanceller = null;
        var canceltaxrule = null;
        var cancellerpaymenttypes = null;
        var cancellerpayment = null;
        var cancellerApayment = null;
        var Couponecanceller = null;
        var Outletcanceller=null;
        var OutletDelete=null;
        var Savepaymentcanceller = null;
        var coupontypecanceller = null;
        var specialcoupontypecanceller = null;
        var preorderceller = null;
        var preorderTableceller = null;

        
        data.getCurrencyLists = function (Data) {
            if (cancellerCurrency) {
                cancellerCurrency.resolve();
            }
            cancellerCurrency = $q.defer();
            return CommonService.http({
                url: serverUrls.getcurrencylists,
                method: 'GET',
                params: Data,
                timeout: cancellerCurrency.promise
            });
        };
		
		data.getGeneralSettings = function (Data) {
            if (cancellerGeneralSettingsG) {
                cancellerGeneralSettingsG.resolve();
            }
            cancellerGeneralSettingsG = $q.defer();
            return CommonService.http({
                url: serverUrls.getgeneralsettings,
                method: 'GET',
                params: Data,
                timeout: cancellerGeneralSettingsG.promise
            });
        };
		
		data.saveGeneralSettings = function (Data) {
            if (saveGeneralSettingscanceller) {
                saveGeneralSettingscanceller.resolve();
            }
            saveGeneralSettingscanceller = $q.defer();
            return CommonService.http({
                url: serverUrls.savegeneralsettings,
                method: 'POST',
                data: Data,
                headers: {'Content-Type': undefined},
                transformRequest: angular.identity,
                timeout: saveGeneralSettingscanceller.promise
            });
        };

        data.saveGeneralImpSettings = function (Data) {
            if (saveGeneralSettingscanceller) {
                saveGeneralSettingscanceller.resolve();
            }
            saveGeneralSettingscanceller = $q.defer();
            return CommonService.http({
                url: serverUrls.saveGeneralImpSettings,
                method: 'POST',
                data: Data,
                headers: {'Content-Type': undefined},
                transformRequest: angular.identity,
                timeout: saveGeneralSettingscanceller.promise
            });
        };

        data.getTaxRule = function (Data) {
            if (canceltaxrule) {
                canceltaxrule.resolve();
            }
            canceltaxrule = $q.defer();
            return CommonService.http({
                url: serverUrls.getTaxRule,
                method: 'POST',
                data: Data,
                timeout: canceltaxrule.promise
            });
        };

        data.saveTaxRule = function (Data) {
            if (canceller) {
                canceller.resolve();
            }
            canceller = $q.defer();
            return CommonService.http({
                url: serverUrls.saveTaxRule,
                method: 'POST',
                data: Data,
                timeout: canceller.promise
            });
        };

        data.ChangeTaxStatus = function (Data) {
            if (canceller) {
                canceller.resolve();
            }
            canceller = $q.defer();
            return CommonService.http({
                url: serverUrls.changeTaxRuleStatus,
                method: 'POST',
                data: Data,
                timeout: canceller.promise
            });
        };

        data.DeleteTaxes = function(Data){
            if (canceller) {
                canceller.resolve();
            }
            canceller = $q.defer();
            return CommonService.http({
                url: serverUrls.DeleteTaxes,
                method: 'POST',
                data: Data,
                timeout: canceller.promise
            });
        }

        data.getPaymentTypes = function () {
            if (cancellerpaymenttypes) {
                cancellerpaymenttypes.resolve();
            }
            cancellerpaymenttypes = $q.defer();
            return CommonService.http({
                url: serverUrls.getPaymentTypes,
                method: 'GET',
                timeout: cancellerpaymenttypes.promise
            });
        };

        data.getAllPayments = function () {
            if (cancellerpayment) {
                cancellerpayment.resolve();
            }
            cancellerpayment = $q.defer();
            return CommonService.http({
                url: serverUrls.getAllPayments,
                method: 'POST',
                timeout: cancellerpayment.promise
            });
        };

        data.getAddedPaymentTypes = function () {
            if (cancellerApayment) {
                cancellerApayment.resolve();
            }
            cancellerApayment = $q.defer();
            return CommonService.http({
                url: serverUrls.getAddedPaymentTypes,
                method: 'POST',
                timeout: cancellerApayment.promise
            });
        };

        data.savePayments = function (Data) {
            if (saveGeneralSettingscanceller) {
                saveGeneralSettingscanceller.resolve();
            }
            saveGeneralSettingscanceller = $q.defer();
            return CommonService.http({
                url: serverUrls.savePayments,
                method: 'POST',
                data: Data,
                timeout: saveGeneralSettingscanceller.promise
            });
        };
        
        data.savePaymentsNew = function (Data) {
            if (Savepaymentcanceller) {
                Savepaymentcanceller.resolve();
            }
            Savepaymentcanceller = $q.defer();
            return CommonService.http({
                url: serverUrls.savePayments,
                method: 'POST',
                data: Data,
                timeout: Savepaymentcanceller.promise
            });
        };
        
        data.AddCoupon = function (Data) {
            if (canceller) {
                canceller.resolve();
            }
            canceller = $q.defer();
            return CommonService.http({
                url: serverUrls.AddCoupon,
                method: 'POST',
                data: Data,
                timeout: canceller.promise
            });
        };

        data.GetCouponTypes = function (Data) {
            if (coupontypecanceller) {
                coupontypecanceller.resolve();
            }
            coupontypecanceller = $q.defer();
            return CommonService.http({
                url: serverUrls.GetCouponTypes,
                method: 'GET',
                params: Data,
                timeout: coupontypecanceller.promise
            });
        };

        data.GetSpecialCouponTypes = function (Data) {
            if (specialcoupontypecanceller) {
                specialcoupontypecanceller.resolve();
            }
            specialcoupontypecanceller = $q.defer();
            return CommonService.http({
                url: serverUrls.GetSpecialCouponTypes,
                method: 'GET',
                params: Data,
                timeout: specialcoupontypecanceller.promise
            });
        };
        
        data.GetCoupons = function (Data) {
            if (Couponecanceller) {
                Couponecanceller.resolve();
            }
            Couponecanceller = $q.defer();
            return CommonService.http({
                url: serverUrls.GetCoupons,
                method: 'POST',
                params: Data,
                timeout: Couponecanceller.promise
            });
        };

        data.GetOutlet = function (Data) {
            if (Outletcanceller) {
                Outletcanceller.resolve();
            }
            Outletcanceller = $q.defer();
            return CommonService.http({
                url: serverUrls.GetOutlet,
                method: 'POST',
                params: Data,
                timeout: Outletcanceller.promise
            });
        };

        data.AddOutlet = function (Data) {
            if (canceller) {
                canceller.resolve();
            }
            canceller = $q.defer();
            return CommonService.http({
                url: serverUrls.AddOutlet,
                method: 'POST',
                data: Data,
                timeout: canceller.promise
            });
        };

        data.DeletOutlet = function (Data) {
            if (OutletDelete) {
                OutletDelete.resolve();
            }
            OutletDelete = $q.defer();
            return CommonService.http({
                url: serverUrls.DeleteOutlet,
                method: 'POST',
                data: Data,
                timeout: OutletDelete.promise
            });
        };

        data.getPreorderSettings = function (Data) {
            if (preorderceller) {
                preorderceller.resolve();
            }
            preorderceller = $q.defer();
            return CommonService.http({
                url: serverUrls.GetPreorderSettings,
                method: 'GET',
                params: Data,
                timeout: preorderceller.promise
            });
        };

        data.savePreorderSettings = function(Data){
            if (saveGeneralSettingscanceller) {
                saveGeneralSettingscanceller.resolve();
            }
            saveGeneralSettingscanceller = $q.defer();
            return CommonService.http({
                url: serverUrls.savePreorderSettings,
                method: 'POST',
                data: Data,
                timeout: saveGeneralSettingscanceller.promise
            });
        }

        data.GetPreorderTables = function (Data) {
            if (preorderTableceller) {
                preorderTableceller.resolve();
            }
            preorderTableceller = $q.defer();
            return CommonService.http({
                url: serverUrls.GetPreorderTables,
                method: 'GET',
                params: Data,
                timeout: preorderTableceller.promise
            });
        };

        data.savePreorderTable = function(Data){
            if (saveGeneralSettingscanceller) {
                saveGeneralSettingscanceller.resolve();
            }
            saveGeneralSettingscanceller = $q.defer();
            return CommonService.http({
                url: serverUrls.savePreorderTable,
                method: 'POST',
                data: Data,
                timeout: saveGeneralSettingscanceller.promise
            });
        }

        data.deletePreOrderTable = function (Data) {
            if (OutletDelete) {
                OutletDelete.resolve();
            }
            OutletDelete = $q.defer();
            return CommonService.http({
                url: serverUrls.deletePreOrderTable,
                method: 'POST',
                data: Data,
                timeout: OutletDelete.promise
            });
        };

        data.deleteSelectedTAxRules = function (Data) {
            if (OutletDelete) {
                OutletDelete.resolve();
            }
            OutletDelete = $q.defer();
            return CommonService.http({
                url: serverUrls.DeleteMultiTaxes,
                method: 'POST',
                data: Data,
                timeout: OutletDelete.promise
            });
        };

        data.ChangeCouponStatus = function (Data) {
            if (canceller) {
                canceller.resolve();
            }
            canceller = $q.defer();
            return CommonService.http({
                url: serverUrls.ChangeCouponStatus,
                method: 'POST',
                data: Data,
                timeout: canceller.promise
            });
        };

        data.DeleteCoupon = function (Data) {
            if (canceller) {
                canceller.resolve();
            }
            canceller = $q.defer();
            return CommonService.http({
                url: serverUrls.DeleteCoupon,
                method: 'POST',
                data: Data,
                timeout: canceller.promise
            });
        };

        return data;
    }]);


