EcommerceApp.factory('importexportService', ['serverUrls', 'CommonService', '$q', function (serverUrls, CommonService, $q) {
        var data = {};
        var canceller = null;
        data.importProducts = function (Data) {
            if (canceller) {
                canceller.resolve();
            }
            canceller = $q.defer();
            return CommonService.http({
                url: serverUrls.importProducts,
                method: 'POST',
                data: Data,
                headers: {'Content-Type': undefined},
                transformRequest: angular.identity,
                timeout: canceller.promise
            });
        };

        
        return data;
    }]);


