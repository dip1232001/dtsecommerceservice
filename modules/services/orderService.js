EcommerceApp.factory('orderService', ['serverUrls', 'CommonService', '$q', function (serverUrls, CommonService, $q) {
        var data = {};
        var canceller = null;
        var orderstatuscanceller = null;
        var orderTypecanceller=null;
        var cancellerCS = null;
        var cancellerPaymentStatusList = null;
        var cancellerGeneralSettingsG = null;
        data.getOrder = function (data,start,limit) {
            var p={start:start,limit:limit};
            if (canceller) {
                canceller.resolve();
            }
            canceller = $q.defer();
            return CommonService.http({
                url: serverUrls.getorder,
                method: 'POST',
                data: data,
                params: p,
                timeout: canceller.promise
            });
        };

        data.getOrderCS = function (data,start,limit) {
            var p={start:start,limit:limit};
            if (cancellerCS) {
                cancellerCS.resolve();
            }
            cancellerCS = $q.defer();
            return CommonService.http({
                url: serverUrls.getorder,
                method: 'POST',
                data: data,
                params: p,
                timeout: cancellerCS.promise
            });
        };

        data.GetOrderStatusList = function () {
            if (orderstatuscanceller) {
                orderstatuscanceller.resolve();
            }
            orderstatuscanceller = $q.defer();
            return CommonService.http({
                url: serverUrls.GetOrderStatusList,
                method: 'GET',
                timeout: orderstatuscanceller.promise
            });
        };

        data.GetOrderTypeList = function () {
            if (orderTypecanceller) {
                orderTypecanceller.resolve();
            }
            orderTypecanceller = $q.defer();
            return CommonService.http({
                url: serverUrls.GetOrderTypeList,
                method: 'GET',
                timeout: orderTypecanceller.promise
            });
        };

        data.ChangeOrderStatus = function (data) {
            if (orderstatuscanceller) {
                orderstatuscanceller.resolve();
            }
            orderstatuscanceller = $q.defer();
            return CommonService.http({
                url: serverUrls.ChangeOrderStatus,
                method: 'POST',
                data : data,
                timeout: orderstatuscanceller.promise
            });
        };

        data.UpdateTracking = function (data) {
            if (canceller) {
                canceller.resolve();
            }
            canceller = $q.defer();
            return CommonService.http({
                url: serverUrls.UpdateTracking,
                method: 'POST',
                data : data,
                timeout: canceller.promise
            });
        }

        data.gePaymentStatusList = function () {
            if (cancellerPaymentStatusList) {
                cancellerPaymentStatusList.resolve();
            }
            cancellerPaymentStatusList = $q.defer();
            return CommonService.http({
                url: serverUrls.gePaymentStatusList,
                method: 'GET',
                timeout: cancellerPaymentStatusList.promise
            });
        }

        data.ChangePaymentStatus = function (data) {
            if (orderstatuscanceller) {
                orderstatuscanceller.resolve();
            }
            orderstatuscanceller = $q.defer();
            return CommonService.http({
                url: serverUrls.ChangePaymentStatus,
                method: 'POST',
                data : data,
                timeout: orderstatuscanceller.promise
            });
        };

        data.getGeneralSettingsN = function (Data) {
            if (cancellerGeneralSettingsG) {
                cancellerGeneralSettingsG.resolve();
            }
            cancellerGeneralSettingsG = $q.defer();
            return CommonService.http({
                url: serverUrls.getgeneralsettings,
                method: 'GET',
                params: Data,
                timeout: cancellerGeneralSettingsG.promise
            });
        };

        data.GetNewOrderCount = function (data) {
            if (orderstatuscanceller) {
                orderstatuscanceller.resolve();
            }
            orderstatuscanceller = $q.defer();
            return CommonService.http({
                url: serverUrls.GetNewOrderCount,
                method: 'POST',
                data : data,
                timeout: orderstatuscanceller.promise
            });
        };
        
        return data;
    }]);


