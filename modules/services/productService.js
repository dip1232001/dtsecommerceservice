EcommerceApp.factory('productService', ['serverUrls', 'CommonService', '$q', function (serverUrls, CommonService, $q) {
        var data = {};
        var canceller = null;
        var cancellerProduct = null;
        var cancellerProductAR = null;
        data.createProduct = function (Data) {
            if (canceller) {
                canceller.resolve();
            }
            canceller = $q.defer();
            return CommonService.http({
                url: serverUrls.createproduct,
                method: 'POST',
                data: Data,
                headers: {'Content-Type': undefined},
                transformRequest: angular.identity,
                timeout: canceller.promise
            });
        };

        data.getProduct = function (Data,Start,Limit) {
			var p={start:Start,limit:Limit};
            if (cancellerProduct) {
                cancellerProduct.resolve();
            }
            cancellerProduct = $q.defer();
            return CommonService.http({
                url: serverUrls.getproduct,
                method: 'POST',
                data: Data,
                params: p,
                timeout: cancellerProduct.promise
            });
        };

        data.deleteProduct = function (Data) {
            if (canceller) {
                canceller.resolve();
            }
            canceller = $q.defer();
            return CommonService.http({
                url: serverUrls.deleteproduct,
                method: 'POST',
                data: Data,
                timeout: canceller.promise
            });
        };

        data.getproductpricetype = function (Data) {
            if (canceller) {
                canceller.resolve();
            }
            canceller = $q.defer();
            return CommonService.http({
                url: serverUrls.getproductpricetype,
                method: 'GET',
                params: Data,
                timeout: canceller.promise
            });
        };

        data.getSingleProduct = function (Data) {
            if (cancellerProduct) {
                cancellerProduct.resolve();
            }
            cancellerProduct = $q.defer();
            return CommonService.http({
                url: serverUrls.getsingleproduct,
                method: 'GET',
                params: Data,
                timeout: cancellerProduct.promise
            });
        };
		
		data.changeProductStats = function (Data) {
            if (cancellerProduct) {
                cancellerProduct.resolve();
            }
            cancellerProduct = $q.defer();
            return CommonService.http({
                url: serverUrls.changeproductstats,
                method: 'POST',
                data: Data,
                timeout: cancellerProduct.promise
            });
        };

        data.deleteproductImage = function (Data) {
            if (canceller) {
                canceller.resolve();
            }
            canceller = $q.defer();
            return CommonService.http({
                url: serverUrls.deleteproductImage,
                method: 'POST',
                data: Data,
                timeout: canceller.promise
            });
        };

        data.deleteproductVideo = function (Data) {
            if (canceller) {
                canceller.resolve();
            }
            canceller = $q.defer();
            return CommonService.http({
                url: serverUrls.deleteproductVideo,
                method: 'POST',
                data: Data,
                timeout: canceller.promise
            });
        };

        data.deleteproductDocument = function (Data) {
            if (canceller) {
                canceller.resolve();
            }
            canceller = $q.defer();
            return CommonService.http({
                url: serverUrls.deleteproductDocument,
                method: 'POST',
                data: Data,
                timeout: canceller.promise
            });
        };

        data.deleteproductTestimonial = function (Data) {
            if (canceller) {
                canceller.resolve();
            }
            canceller = $q.defer();
            return CommonService.http({
                url: serverUrls.deleteproductTestimonial,
                method: 'POST',
                data: Data,
                timeout: canceller.promise
            });
        };

        data.generateProductQR = function (Data) {
			if (cancellerProduct) {
                cancellerProduct.resolve();
            }
            cancellerProduct = $q.defer();
            return CommonService.http({
                url: serverUrls.generateProductQR,
                method: 'POST',
                data: Data,
                timeout: cancellerProduct.promise
            });
        };

        data.generateProductQRPDF = function (Data) {
			if (cancellerProduct) {
                cancellerProduct.resolve();
            }
            cancellerProduct = $q.defer();
            return CommonService.http({
                url: serverUrls.generateProductQRPDF,
                method: 'POST',
                data: Data,
                timeout: cancellerProduct.promise
            });
        };

        data.getrawdata = function (Data) {
			if (cancellerProduct) {
                cancellerProduct.resolve();
            }
            cancellerProduct = $q.defer();
            return CommonService.http({
                url: serverUrls.getrawdata,
                method: 'POST',
                data: Data,
                timeout: cancellerProduct.promise
            });
        };

        data.getDefinedARTagsByBU = function () {
			if (cancellerProductAR) {
                cancellerProductAR.resolve();
            }
            cancellerProductAR = $q.defer();
            return CommonService.http({
                url: serverUrls.getDefinedARTagsByBU,
                method: 'GET',
                timeout: cancellerProductAR.promise
            });
        };

        data.makeDefaultProductImage = function(Data){
            if (cancellerProductAR) {
                cancellerProductAR.resolve();
            }
            cancellerProductAR = $q.defer();
            return CommonService.http({
                url: serverUrls.makeDefaultProductImage,
                method: 'POST',
                data: Data,
                timeout: cancellerProductAR.promise
            });
        }

        data.GetProductOrderForCategory = function(Data){
            if (cancellerProductAR) {
                cancellerProductAR.resolve();
            }
            cancellerProductAR = $q.defer();
            return CommonService.http({
                url: serverUrls.GetProductOrderForCategory,
                method: 'GET',
                params: Data,
                timeout: cancellerProductAR.promise
            });
        }

        data.reorderProduct = function(Data){
            if (cancellerProductAR) {
                cancellerProductAR.resolve();
            }
            cancellerProductAR = $q.defer();
            return CommonService.http({
                url: serverUrls.reorderProduct,
                method: 'POST',
                data: Data,
                timeout: cancellerProductAR.promise
            });
        }

        return data;
    }]);


