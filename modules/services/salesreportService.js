EcommerceApp.factory('salesreportService', ['serverUrls', 'CommonService', '$q', function (serverUrls, CommonService, $q) {
        var data = {};
        var canceller = null;
        var orderstatuscanceller = null;
        data.GetReports = function (data,start,limit) {
            var p={start:start,limit:limit};
            if (canceller) {
                canceller.resolve();
            }
            canceller = $q.defer();
            return CommonService.http({
                url: serverUrls.GetReports,
                method: 'POST',
                data: data,
                params: p,
                timeout: canceller.promise
            });
        };

        data.GetOrderStatusList = function () {
            if (orderstatuscanceller) {
                orderstatuscanceller.resolve();
            }
            orderstatuscanceller = $q.defer();
            return CommonService.http({
                url: serverUrls.GetOrderStatusList,
                method: 'GET',
                timeout: orderstatuscanceller.promise
            });
        };
        
        return data;
    }]);


