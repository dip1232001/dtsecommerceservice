<?php

require_once CONTROLLER_PATH . DS . 'App.php';
require_once MODEL_PATH . DS . 'ARProductModel.php';
require_once MODEL_PATH . DS . 'PriceModel.php';
require_once MODEL_PATH . DS . 'CartModel.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class ARProduct extends AppController
{    

    public function getAllProductsByARTag()
    {
        $start = empty($this->request['GET']['start']) ? 0 : $this->request['GET']['start'];
        $limit = (empty($this->request['GET']['limit']) || $this->request['GET']['limit'] > 10 ) ? 10 : $this->request['GET']['limit'];
        $view = new View();
        try {
            $ProductModel = new ARProductModel();
            return $view->json([
                    'data' => $ProductModel->getAllProductsByARTag($start, $limit, $this->request['POST']),
                    'error' => false
            ]);
        } catch (Exception $exc) {
            return $view->json([
                'data' => [],
                'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                'error' => true
            ]);
        }
    }


    public function getARTagsByBU()
    {
        $buid = $this->request['GET']['buid'];
        $start = empty($this->request['GET']['start']) ? 0 : $this->request['GET']['start'];
        $limit = (empty($this->request['GET']['limit']) || $this->request['GET']['limit'] > 100 ) ? 100 : $this->request['GET']['limit'];
        $view = new View();
        try {
            $ProductModel = new ARProductModel();
            return $view->json([
                    'data' => $ProductModel->getARTagsByBU($start, $limit, $buid),
                    'error' => false
            ]);
        } catch (Exception $exc) {
            return $view->json([
                'data' => [],
                'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                'error' => true
            ]);
        }
    }

    public function addARTagsByBU()
    {
        $data = $this->request['POST'];
        $view = new View();
        try {
            $ProductModel = new ARProductModel();
            return $view->json([
                    'data' => $ProductModel->addARTagsByBU($data),
                    'error' => false
            ]);
        } catch (Exception $exc) {
            return $view->json([
                'data' => [],
                'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                'error' => true
            ]);
        }
    }

    public function getProductImagesByBU()
    {
        $buid = $this->request['GET']['buid'];
        $start = empty($this->request['GET']['start']) ? 0 : $this->request['GET']['start'];
        $limit = empty($this->request['GET']['limit']) ? 100 : $this->request['GET']['limit'];
        $view = new View();
        try {
            $ProductModel = new ARProductModel();
            return $view->json([
                    'data' => $ProductModel->getProductImagesByBU($start, $limit, $buid),
                    'error' => false
            ]);
        } catch (Exception $exc) {
            return $view->json([
                'data' => [],
                'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                'error' => true
            ]);
        }
    }

    public function addARTagWithProduct()
    {
        $data = $this->request['POST'];
        $view = new View();
        try {
            $ProductModel = new ARProductModel();
            return $view->json([
                    'data' => $ProductModel->addARTagWithProduct($data),
                    'msg' => 'Tag inserted successfully',
                    'error' => false
            ]);
        } catch (Exception $exc) {
            return $view->json([
                'data' => [],
                'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                'error' => true
            ]);
        }
    }

    public function addARScheduleJobByBU()
    {
        $data = $this->request['POST'];
        $view = new View();
        try {
            $ProductModel = new ARProductModel();
            return $view->json([
                    'data' => $ProductModel->addARScheduleJobByBU($data),
                    'error' => false
            ]);
        } catch (Exception $exc) {
            return $view->json([
                'data' => [],
                'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                'error' => true
            ]);
        }
    }

    public function getPendingScheduleJobsByBU()
    {
        $buid = $this->request['GET']['BUID'];
        //$start = empty($this->request['GET']['start']) ? 0 : $this->request['GET']['start'];
        //$limit = (empty($this->request['GET']['limit']) || $this->request['GET']['limit'] > 10 ) ? 10 : $this->request['GET']['limit'];
        $view = new View();
        try {
            $ProductModel = new ARProductModel();
            return $view->json([
                    'data' => $ProductModel->getPendingScheduleJobsByBU($start = 0, $limit = 1, $buid),
                    'error' => false
            ]);
        } catch (Exception $exc) {
            return $view->json([
                'data' => [],
                'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                'error' => true
            ]);
        }
    }

    public function getTotalPendingScheduleJobsByBU()
    {
        $buid = $this->request['GET']['BUID'];
        //$start = empty($this->request['GET']['start']) ? 0 : $this->request['GET']['start'];
        //$limit = (empty($this->request['GET']['limit']) || $this->request['GET']['limit'] > 10 ) ? 10 : $this->request['GET']['limit'];
        $view = new View();
        try {
            $ProductModel = new ARProductModel();
            return $view->json([
                    'data' => $ProductModel->getTotalPendingScheduleJobsByBU($start = 0, $limit = 1, $buid),
                    'error' => false
            ]);
        } catch (Exception $exc) {
            return $view->json([
                'data' => [],
                'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                'error' => true
            ]);
        }
    }

    public function getScheduleJobsByBU()
    {
        $buid = $this->request['GET']['BUID'];
        $start = empty($this->request['GET']['start']) ? 0 : $this->request['GET']['start'];
        $limit = (empty($this->request['GET']['limit']) || $this->request['GET']['limit'] > 100 ) ? 100 : $this->request['GET']['limit'];
        $view = new View();
        try {
            $ProductModel = new ARProductModel();
            return $view->json([
                    'data' => $ProductModel->getScheduleJobsByBU($start, $limit, $buid),
                    'error' => false
            ]);
        } catch (Exception $exc) {
            return $view->json([
                'data' => [],
                'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                'error' => true
            ]);
        }
    }

    public function updateARScheduleJobStatus()
    {
        $data = $this->request['POST'];
        $view = new View();
        try {
            $ProductModel = new ARProductModel();
            return $view->json([
                    'data' => $ProductModel->updateARScheduleJobStatus($data),
                    'error' => false
            ]);
        } catch (Exception $exc) {
            return $view->json([
                'data' => [],
                'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                'error' => true
            ]);
        }
    }

    public function getARTags()
    {
        $start = empty($this->request['GET']['start']) ? 0 : $this->request['GET']['start'];
        $limit = (empty($this->request['GET']['limit']) || $this->request['GET']['limit'] > 100 ) ? 100 : $this->request['GET']['limit'];
        $view = new View();
        try {
            $ProductModel = new ARProductModel();
            return $view->json([
                    'data' => $ProductModel->getARTags($start, $limit),
                    'error' => false
            ]);
        } catch (Exception $exc) {
            return $view->json([
                'data' => [],
                'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                'error' => true
            ]);
        }
    }

    public function AREmail()
    {
        $buid = $this->request['GET']['BUID'];
        $view = new View();
        try {
            $ProductModel = new ARProductModel();
            return $view->json([
                    'data' => $ProductModel->AREmail($buid),
                    'error' => false
            ]);
        } catch (Exception $exc) {
            return $view->json([
                'data' => [],
                'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                'error' => true
            ]);
        }
    }

}
