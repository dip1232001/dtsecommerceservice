<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ContactCards
 *
 * @author Abhijit Manna
 */

require_once CONTROLLER_PATH . DS . 'App.php';
require_once MODEL_PATH . DS . 'AdminloginModel.php';
require_once MODEL_PATH . DS . 'CustomersModel.php';

class Adminlogin extends AppController {

    //put your code here
    public function __construct() {
        parent::__construct();
    }

    public function Login() {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $AdminloginModel = new AdminloginModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Profile created succesfully.'),
                        'data' => $AdminloginModel->Login($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }
}
