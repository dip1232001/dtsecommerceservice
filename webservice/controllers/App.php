<?php
require_once COMPONENT_PATH . DS . 'Views' . DS . 'View.php';
require_once MODEL_PATH . DS . 'App.php';

class AppController {

    public $request;
    public $GeoDetails;
    public $Pusher = null;
    public $routerparams = [];
    public $RewardsExceptionsRedirect = [
    ];
    public $isVio = false;

    public function __construct() {
        global $functionToCall;
        $this->routerparams = $functionToCall;
    }

}
