<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AugmentedReality
 *
 * @author Achyut Sinha
 */

require_once CONTROLLER_PATH . DS . 'App.php';
require_once MODEL_PATH . DS . 'AugmentedRealityModel.php';

class Augmentedreality extends AppController {

    //put your code here
    public function __construct() {
        parent::__construct();
    }

    public function saveARTempImg() {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $AugmentedRealityModel = new AugmentedRealityModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('image saved succesfully'),
                        'data' => $AugmentedRealityModel->saveARTempImg($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function getARTempImg() {
        $view = new View();
        $data["companyid"] = $this->request['GET']['companyid'];
        $data["BUID"] = $this->request['GET']['BUID'];
        $data["start"] = $this->request['GET']['start'];
        $data["limit"] = $this->request['GET']['limit'];
        
        try {
            $AugmentedRealityModel = new AugmentedRealityModel();
            return $view->json([
                        'error' => false,
                        'msg'   => __t('Images listed succesfully.'),
                        'data'  => $AugmentedRealityModel->getARTempImg($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data'  => [],
                        'msg'   => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function uploadARTempImg() {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $AugmentedRealityModel = new AugmentedRealityModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Image uploaded succesfully'),
                        'data' => $AugmentedRealityModel->uploadARTempImg($_FILES, $data['BUID'])
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function saveARTagImg() {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $AugmentedRealityModel = new AugmentedRealityModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('image tag saved succesfully'),
                        'data' => $AugmentedRealityModel->saveARTagImg($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function getlinktempval() {
        $view = new View();
        //$data["appid"] = $this->request['GET']['appid'];
        //$data["BUID"] = $this->request['GET']['BUID'];
        //$data["templateid"] = $this->request['GET']['templateid'];
        $data = $this->request['POST'];
        try {
            $AugmentedRealityModel = new AugmentedRealityModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('get list succesfully'),
                        'data' => $AugmentedRealityModel->getlinktempval($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function getARTagImg() {
        $view = new View();
        $data["companyid"] = $this->request['GET']['companyid'];
        $data["BUID"] = $this->request['GET']['BUID'];
        $data["start"] = $this->request['GET']['start'];
        $data["limit"] = $this->request['GET']['limit'];
        try {
            $AugmentedRealityModel = new AugmentedRealityModel();
            return $view->json([
                        'error' => false,
                        'msg'   => __t('Images listed succesfully.'),
                        'data'  => $AugmentedRealityModel->getARTagImg($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data'  => [],
                        'msg'   => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function getDeviceList() {
        $view = new View();
        $data["BUID"] = $this->request['GET']['BUID'];
        try {
            $AugmentedRealityModel = new AugmentedRealityModel();
            return $view->json([
                        'error' => false,
                        'msg'   => __t('Images listed succesfully.'),
                        'data'  => $AugmentedRealityModel->getDeviceList($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data'  => [],
                        'msg'   => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

}
