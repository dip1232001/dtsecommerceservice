<?php

require_once CONTROLLER_PATH . DS . 'App.php';
require_once MODEL_PATH . DS . 'BeaconsModel.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Category
 *
 * @author dipankar
 */
class Beacons extends AppController {

    //put your code here

    public function addDevice() {
        $view = new View();
        try {
            $Beacons = new BeaconsModel();
            $devices = $Beacons->addDevice($this->request['POST']);
            return $view->json([
                        'error' => false,
                        'data' => $devices,
                        'msg' => $devices['msg']
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }
    
    public function getDevice() {
        $view = new View();
        $i_intFKCompanyID = empty($this->request['POST']['i_intFKCompanyID']) ? null : $this->request['POST']['i_intFKCompanyID'];
        $i_intEddystoneDeviceID = empty($this->request['POST']['i_intEddystoneDeviceID']) ? null : $this->request['POST']['i_intEddystoneDeviceID'];
        $i_intFKBusinessUnitID = empty($this->request['POST']['i_intFKBusinessUnitID']) ? null : $this->request['POST']['i_intFKBusinessUnitID'];
        try {
            $Beacons = new BeaconsModel();
            $devices = $Beacons->getDevice($i_intFKCompanyID,$i_intEddystoneDeviceID,$i_intFKBusinessUnitID);
            return $view->json([
                        'error' => false,
                        'data' => $devices,
                        'msg' => $devices['msg']
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }
    
    public function addDeviceStatus() {
        $view = new View();
        try {
            $Beacons = new BeaconsModel();
            $devices = $Beacons->addDeviceStatus($this->request['POST']);
            return $view->json([
                        'error' => false,
                        'data' => $devices,
                        'msg' => $devices['msg']
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }
    
    public function getDeviceStatus() {
        $view = new View();
        $companyid = empty($this->request['POST']['companyid']) ? null : $this->request['POST']['companyid'];
        $uuid = empty($this->request['POST']['uuid']) ? null : $this->request['POST']['uuid'];
        try {
            $Beacons = new BeaconsModel();
            $devices = $Beacons->getDeviceStatus($companyid,$uuid);
            return $view->json([
                        'error' => false,
                        'data' => $devices,
                        'msg' => $devices['msg']
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }
    
    public function deleteDevice() {
        $view = new View();
        $id = empty($this->request['POST']['id']) ? null : $this->request['POST']['id'];
        $buid = empty($this->request['POST']['buid']) ? null : $this->request['POST']['buid'];
        try {
            $Beacons = new BeaconsModel();
            $devices = $Beacons->deleteDevice($id,$buid);
            return $view->json([
                        'error' => false,
                        'data' => $devices,
                        'msg' => $devices['msg']
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

}
