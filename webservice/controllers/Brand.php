<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BrandController
 *
 * @author dipankar
 */
require_once CONTROLLER_PATH . DS . 'App.php';
require_once MODEL_PATH . DS . 'Brandmodel.php';

class Brand extends AppController {

    //put your code here

    function addBrands() {
        $view = new View();
        try {
            $BrandModel = new BrandModel();
            return $view->json([
                        'error' => false,
                        'data' => $BrandModel->addBrand($this->request['POST']),
                        'msg' => 'Brand added succesfully'
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'error' => true,
                        'msg' => $exc->getMessage(),
                        'data' => []
            ]);
        }
    }

}
