<?php

require_once CONTROLLER_PATH . DS . 'App.php';
require_once MODEL_PATH . DS . 'CategoryModel.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Category
 *
 * @author dipankar
 */
class Category extends AppController {

    //put your code here

    public function addCategory() {
        $view = new View();
        try {
            $Category = new CategoryModel();
            $addCat = $Category->addCategory($this->request['POST'], $_FILES);
            return $view->json([
                        'error' => false,
                        'data' => $addCat,
                        'msg' => $addCat['msg']
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function getCategories() {
        $view = new View();
        try {
            $Category = new CategoryModel();
            return $view->json([
                        'error' => false,
                        'data' => $Category->GetCategories($this->request['GET']),
                        'msg' => 'Categories retrived'
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function deleteCategories() {
        $view = new View();
        try {
            $Category = new CategoryModel();
            return $view->json([
                        'error' => false,
                        'data' => $Category->DeleteCategory($this->request['POST']['id'], $this->request['POST']['BUID']),
                        'msg' => 'Categories deleted'
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function GetCategoriesTree() {
        $view = new View();
        $data = $this->request['GET'];
        try {
            $Category = new CategoryModel();
            return $view->json([
                        'error' => false,
                        'data' => $Category->GetCategoriesTree($data['BUID'], 0, $data),
                        'msg' => 'Categories retrived'
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function updateCategorieOrder() {
        $view = new View();
        try {
            $Category = new CategoryModel();
            return $view->json([
                        'error' => false,
                        'data' => $Category->UpdateCategoryOrder($this->request['POST'], $this->request['POST']['BUID']),
                        'msg' => 'Categorie order updated'
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

}
