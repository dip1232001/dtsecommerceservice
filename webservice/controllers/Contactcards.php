<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ContactCards
 *
 * @author Abhijit Manna
 */

require_once CONTROLLER_PATH . DS . 'App.php';
require_once MODEL_PATH . DS . 'ContactCardsModel.php';
require_once MODEL_PATH . DS . 'CustomersModel.php';

class Contactcards extends AppController {

    //put your code here
    public function __construct() {
        parent::__construct();
    }

    public function GetProfession() {
        $view = new View();
        try {
            $ContactCardsModel = new ContactCardsModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Profession retrived succesfully'),
                        'data' => $ContactCardsModel->GetProfession()
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function GetIndustries() {
        try {
            $view = new View();
            $data = $this->request['POST'];
            $ContactCardsModel = new ContactCardsModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Industries retrived succesfully'),
                        'data' => $ContactCardsModel->GetIndustries()
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function AddPerson() {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $ContactCardsModel = new ContactCardsModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Profile created succesfully.'),
                        'data' => $ContactCardsModel->AddPerson($data, $_FILES)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function GetPrerson() {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $ContactCardsModel = new ContactCardsModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Person listed succesfully'),
                        'data' => $ContactCardsModel->GetPrerson($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function CustomerLogin() {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $ContactCardsModel = new ContactCardsModel();
            //$CustomerModel = new CustomersModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Customer Login successful.'),
                        'data' => $ContactCardsModel->CustomerLogin($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function AddFolder() {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $ContactCardsModel = new ContactCardsModel();
            if($data['ID'] == ""){
                return $view->json([
                            'error' => false,
                            'msg' => __t('Folder created.'),
                            'data' => $ContactCardsModel->AddFolder($data)
                ]);
            }
            else{
                return $view->json([
                    'error' => false,
                    'msg' => __t('Folder renamed.'),
                    'data' => $ContactCardsModel->AddFolder($data)
                ]);
            }
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function AddCards($data) {
        $view = new View();
        $data = $this->request['POST'];
        
        try {
            $ContactCardsModel = new ContactCardsModel();
            if($data['ID'] == ""){
                return $view->json([
                            'error' => false,
                            'msg' => __t('Your card was saved.'),
                            'data' => $ContactCardsModel->AddCards($data)
                ]);
            }else{
                return $view->json([
                    'error' => false,
                    'msg' => __t('Edits saved.'),
                    'data' => $ContactCardsModel->AddCards($data)
                ]);
            }
        } catch (Exception $exc) {
            return $view->json([
                        'data'  => [],
                        'msg'   => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }    

    public function deletePerson($data) {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $ContactCardsModel = new ContactCardsModel();
            return $view->json([
                        'error' => false,
                        'msg'   => __t('Person deleted succesfully.'),
                        'data'  => $ContactCardsModel->deletePerson($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function deleteFolder($data) {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $ContactCardsModel = new ContactCardsModel();
            return $view->json([
                        'error' => false,
                        'msg'   => __t('Folder deleted succesfully.'),
                        'data'  => $ContactCardsModel->deleteFolder($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }
    
    public function deleteCard($data) {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $ContactCardsModel = new ContactCardsModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Card deleted succesfully'),
                        'data' => $ContactCardsModel->deleteCard($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }
    
    public function getFolder() {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $ContactCardsModel = new ContactCardsModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Folder listed succesfully'),
                        'data' => $ContactCardsModel->getFolder($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function addFoldersCardsMap() {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $ContactCardsModel = new ContactCardsModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Card added to selected folder(s).'),
                        'data' => $ContactCardsModel->addFoldersCardsMap($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function getCards() {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $ContactCardsModel = new ContactCardsModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Cards listed succesfully'),
                        'data' => $ContactCardsModel->getCards($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function uploadProfileImage() {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $ContactCardsModel = new ContactCardsModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Profile image uploaded succesfully'),
                        'data' => $ContactCardsModel->uploadProfileImage($data, $_FILES)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function uploadPersonImage() {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $ContactCardsModel = new ContactCardsModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Image uploaded succesfully.'),
                        'data' => $ContactCardsModel->uploadPersonImage($data, $_FILES)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function getPersonImages() {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $ContactCardsModel = new ContactCardsModel();
            return $view->json([
                        'error' => false,
                        'msg'   => __t('Images listed succesfully.'),
                        'data'  => $ContactCardsModel->getPersonImages($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data'  => [],
                        'msg'   => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function deleteFolderCardMap($data) {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $ContactCardsModel = new ContactCardsModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Folder card map deleted successfully.'),
                        'data' => $ContactCardsModel->deleteFolderCardMap($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }
    
    public function saveImage() {
        $view = new View();
        $data = $this->request['POST'];

        $img_data = getimagesize($data['image_url']);
        $width = $img_data[0];
        $hight = $img_data[1];

        $path = $data['image_url'];
        $type = pathinfo($path, PATHINFO_EXTENSION);
        if(strlen($type)>4){
            $type = "png";
        }

        $data = file_get_contents($path);
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);

        $data_arr = array("width"=>$width,"Height"=>$hight,"img_data"=>$base64);

        try {
            return $view->json([
                        'error' => false,
                        'msg' => __t('Image saved successfully.'),
                        'data' => $data_arr
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function addLike() {
        $view = new View();
        $data = $this->request['POST'];
        $ContactCardsModel = new ContactCardsModel();
        try {
            $retData = $ContactCardsModel->addLike($data);

            if($retData['LIKEID'] === null ){
                return $view->json([
                        'error' => false,
                        'msg' => __t('Card disliked.'),
                        'data' => $retData
                ]);
            }else{
                return $view->json([
                        'error' => false,
                        'msg' => __t('Card liked.'),
                        'data' => $retData
                ]);
            }            
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function deleteLike() {
        $view = new View();
        $data = $this->request['POST'];
        $ContactCardsModel = new ContactCardsModel();
        try {
            return $view->json([
                        'error' => false,
                        'msg' => __t('Like deleted successfully.'),
                        'data' => $ContactCardsModel->deleteLike($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }
    
    public function getShareCustomerList() {
        $view = new View();
        $data = $this->request['POST'];
        $ContactCardsModel = new ContactCardsModel();
        try {
            return $view->json([
                        'error' => false,
                        'msg' => __t('Share customer listed successfully.'),
                        'data' => $ContactCardsModel->getShareCustomerList($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function addSubscription() { 
        $view = new View();
        $data = $this->request['POST'];
        $ContactCardsModel = new ContactCardsModel();
        try {
            if($data['ID'] == ""){
                return $view->json([
                            'error' => false,
                            'msg' => __t('Subscription added successfully.'),
                            'data' => $ContactCardsModel->addSubscription($data)
                ]);
            }else{
                return $view->json([
                    'error' => false,
                    'msg' => __t('Subscription edited successfully.'),
                    'data' => $ContactCardsModel->addSubscription($data)
                ]);
            }
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function getActiveSUbscriptions() {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $ContactCardsModel = new ContactCardsModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Subscription retrived succesfully.'),
                        'data' => $ContactCardsModel->getActiveSUbscriptions($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function upgradeSubscription() {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $ContactCardsModel = new ContactCardsModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Subscription upgraded succesfully.'),
                        'data' => $ContactCardsModel->upgradeSubscription($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }
    
    public function getAllCards() {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $ContactCardsModel = new ContactCardsModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Card listed succesfully.'),
                        'data' => $ContactCardsModel->getAllCards($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function shareCard() {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $ContactCardsModel = new ContactCardsModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Shared.'),
                        'data' => $ContactCardsModel->shareCard($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function shareFolder() {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $ContactCardsModel = new ContactCardsModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Shared.'),
                        'data' => $ContactCardsModel->shareFolder($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function getAllCardsShareTo() {
        $view = new View();
        $data = $this->request['POST'];
            try {
                $ContactCardsModel = new ContactCardsModel();
            return $view->json([
                        'error' => false,
                        'msg'   => __t('Get all shared card succesfully.'),
                        'data'  => $ContactCardsModel->getAllCardsShareTo($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg'  => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error'=> true
            ]);
        }
    }

    public function addReview() {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $ContactCardsModel = new ContactCardsModel();
            return $view->json([
                        'error' => false,
                        'msg'   => __t('Your review was posted.'),
                        'data'  => $ContactCardsModel->addReview($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data'  => [],
                        'msg'   => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function getReviews() {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $ContactCardsModel = new ContactCardsModel();
            return $view->json([
                        'error' => false,
                        'msg'   => __t('Get all reviews succesfully.'),
                        'data'  => $ContactCardsModel->getReviews($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg'  => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error'=> true
            ]);
        }
    }

    public function getAllFolder() {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $ContactCardsModel = new ContactCardsModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Folder listed succesfully'),
                        'data' => $ContactCardsModel->getAllFolder($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function getMyCard() {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $ContactCardsModel = new ContactCardsModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Card listed succesfully'),
                        'data' => $ContactCardsModel->getMyCard($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function deleteShareFolder() {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $ContactCardsModel = new ContactCardsModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Share deleted succesfully'),
                        'data' => $ContactCardsModel->deleteShareFolder($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function getHtml() {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $ContactCardsModel = new ContactCardsModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('HTML succesfully'),
                        'data' => $ContactCardsModel->getHtml($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

     public function addNotifications($data) {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $ContactCardsModel = new ContactCardsModel();
            return $view->json([
                        'error' => false,
                        'msg'   => __t('Notifications added successfully.'),
                        'data'  => $ContactCardsModel->addNotifications($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data'  => [],
                        'msg'   => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function sendPushNotification($data) {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $ContactCardsModel = new ContactCardsModel();
            return $view->json([
                        'error' => false,
                        'msg'   => __t('Notifications added successfully.'),
                        'data'  => $ContactCardsModel->sendPushNotification($data['BUID'],$data['customerid'],$data['title'],$data['msg'])
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data'  => [],
                        'msg'   => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

	public function updateNotificationStatus($data) {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $ContactCardsModel = new ContactCardsModel();
            return $view->json([
                        'error' => false,
                        'msg'   => __t('Notifications updated successfully.'),
                        'data'  => $ContactCardsModel->updateNotificationStatus($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data'  => [],
                        'msg'   => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function getAllNotofocations($data) {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $ContactCardsModel = new ContactCardsModel();
            return $view->json([
                        'error' => false,
                        'msg'   => __t('Get all notifications successfully.'),
                        'data'  => $ContactCardsModel->getAllNotofocations($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data'  => [],
                        'msg'   => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function generatevcard($data) {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $ContactCardsModel = new ContactCardsModel();
            return $view->json([
                        'error' => false,
                        'msg'   => __t('VCF card generated successfully.'),
                        'data'  => $ContactCardsModel->generatevcard($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data'  => [],
                        'msg'   => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }
	
	public function pushChatData($data) {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $ContactCardsModel = new ContactCardsModel();
			$retdata=$ContactCardsModel->pushchatdata($data);
			if($retdata===1){
				return $view->json([
                        'error' => false,
                        'msg'   => __t('Chat pushed successfully.'),
                        'data'  => 1
				]);				
			}else{
				return $view->json([
                        'error' => true,
                        'msg'   => __t('Chat pushed failed.'),
                        'data'  => 0
				]);	
			}
            
        } catch (Exception $exc) {
            return $view->json([
                        'data'  => [],
                        'msg'   => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }
	
	public function getchatdata($data) {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $ContactCardsModel = new ContactCardsModel();
            return $view->json([
                        'error' => false,
                        'msg'   => __t('get chat data successfully.'),
                        'data'  => $ContactCardsModel->getchatdata($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data'  => [],
                        'msg'   => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }
	
	public function ChangeChatReadByID($data) {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $ContactCardsModel = new ContactCardsModel();
            return $view->json([
                        'error' => false,
                        'msg'   => __t('get chat data successfully.'),
                        'data'  => $ContactCardsModel->ChangeChatReadByID($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data'  => [],
                        'msg'   => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }
	
	public function getchatUnreaddata($data) {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $ContactCardsModel = new ContactCardsModel();
            return $view->json([
                        'error' => false,
                        'msg'   => __t('get chat data successfully.'),
                        'data'  => $ContactCardsModel->getchatUnreaddata($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data'  => [],
                        'msg'   => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }
	
	public function ChangeChatReadByCustomer($data) {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $ContactCardsModel = new ContactCardsModel();
            return $view->json([
                        'error' => false,
                        'msg'   => __t('get chat data successfully.'),
                        'data'  => $ContactCardsModel->ChangeChatReadByCustomer($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data'  => [],
                        'msg'   => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function RenewSubscription() {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $ContactCardsModel = new ContactCardsModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Profile renewed succesfully.'),
                        'data' => $ContactCardsModel->RenewSubscription($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function ipushdata($data) {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $ContactCardsModel = new ContactCardsModel();
			$retdata=$ContactCardsModel->ipushdata($data);
			if($retdata===1){
				return $view->json([
                        'error' => false,
                        'msg'   => __t('Chat pushed successfully.'),
                        'data'  => 1
				]);				
			}else{
				return $view->json([
                        'error' => true,
                        'msg'   => __t('Chat pushed failed.'),
                        'data'  => 0
				]);	
			}
            
        } catch (Exception $exc) {
            return $view->json([
                        'data'  => [],
                        'msg'   => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function CustomerSocialLogin() {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $ContactCardsModel = new ContactCardsModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Customer Login succesfully.'),
                        'data' => $ContactCardsModel->CustomerSocialLogin($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function ProfileSocialImage() {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $ContactCardsModel = new ContactCardsModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Customer Login succesfully.'),
                        'data' => $ContactCardsModel->ProfileSocialImage($data, $_FILES)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function UploadRawImageData($data) {
        $view = new View();
        $data = $this->request['POST'];
        
        try {
            $ContactCardsModel = new ContactCardsModel();
            return $view->json([
                'error' => false,
                'msg' => __t('Edits saved.'),
                'data' => $ContactCardsModel->UploadRawImageData($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data'  => [],
                        'msg'   => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    } 

    public function SendForgotPasswordEmail() {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $ContactCardsModel = new ContactCardsModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Password reset email sent succesfully'),
                        'data' => $ContactCardsModel->SendForgotPasswordEmail($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function CheckForgotPasswordKey() {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $ContactCardsModel = new ContactCardsModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Success'),
                        'data' => $ContactCardsModel->CheckForgotPasswordKey($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function UpdateForgotPassword() {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $ContactCardsModel = new ContactCardsModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Paswsword change successfully'),
                        'data' => $ContactCardsModel->UpdateForgotPassword($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }
    
    public function addNetworkRequest($data) {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $ContactCardsModel = new ContactCardsModel();
			$retdata=$ContactCardsModel->addNetworkRequest($data);
			if($retdata===1){
				return $view->json([
                        'error' => false,
                        'msg'   => __t('Send request successfully.'),
                        'data'  => 1
				]);				
			}else{
				return $view->json([
                        'error' => true,
                        'msg'   => __t('Send request failed.'),
                        'data'  => 0
				]);	
			}
            
        } catch (Exception $exc) {
            return $view->json([
                        'data'  => [],
                        'msg'   => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function GetCustomerNetworkRequest() {
        $view = new View();
        $data = $this->request['POST'];
            try {
                $ContactCardsModel = new ContactCardsModel();
            return $view->json([
                        'error' => false,
                        'msg'   => __t('Get all request succesfully.'),
                        'data'  => $ContactCardsModel->GetCustomerNetworkRequest($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg'  => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error'=> true
            ]);
        }
    }

    public function GetCustomerNetworkSuggestion() {
        $view = new View();
        $data = $this->request['POST'];
            try {
                $ContactCardsModel = new ContactCardsModel();
            return $view->json([
                        'error' => false,
                        'msg'   => __t('Get all request succesfully.'),
                        'data'  => $ContactCardsModel->GetCustomerNetworkSuggestion($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg'  => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error'=> true
            ]);
        }
    }

    public function ChangeCustomerNetworkRequestStatus($data) {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $ContactCardsModel = new ContactCardsModel();
			$retdata=$ContactCardsModel->ChangeCustomerNetworkRequestStatus($data);
			if($retdata===1){
				return $view->json([
                        'error' => false,
                        'msg'   => __t('Change request status successfully.'),
                        'data'  => 1
				]);				
			}else{
				return $view->json([
                        'error' => true,
                        'msg'   => __t('Change request status failed.'),
                        'data'  => 0
				]);	
			}
            
        } catch (Exception $exc) {
            return $view->json([
                        'data'  => [],
                        'msg'   => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function DeleteCustomerNetworkRequest($data) {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $ContactCardsModel = new ContactCardsModel();
			$retdata=$ContactCardsModel->DeleteCustomerNetworkRequest($data);
			if($retdata===1){
				return $view->json([
                        'error' => false,
                        'msg'   => __t('Request removed successfully.'),
                        'data'  => 1
				]);				
			}else{
				return $view->json([
                        'error' => true,
                        'msg'   => __t('Request remove failed.'),
                        'data'  => 0
				]);	
			}
            
        } catch (Exception $exc) {
            return $view->json([
                        'data'  => [],
                        'msg'   => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function GetCustomerNetworkList() {
        $view = new View();
        $data = $this->request['POST'];
            try {
                $ContactCardsModel = new ContactCardsModel();
            return $view->json([
                        'error' => false,
                        'msg'   => __t('Get all customer succesfully.'),
                        'data'  => $ContactCardsModel->GetCustomerNetworkList($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg'  => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error'=> true
            ]);
        }
    }

    public function DeleteCustomerNetwork($data) {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $ContactCardsModel = new ContactCardsModel();
			$retdata=$ContactCardsModel->DeleteCustomerNetwork($data);
			if($retdata===1){
				return $view->json([
                        'error' => false,
                        'msg'   => __t('Removed from network successfully.'),
                        'data'  => 1
				]);				
			}else{
				return $view->json([
                        'error' => true,
                        'msg'   => __t('Request remove failed.'),
                        'data'  => 0
				]);	
			}
            
        } catch (Exception $exc) {
            return $view->json([
                        'data'  => [],
                        'msg'   => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function addCardVault() {
        ini_set("upload_max_filesize","20M");
        $view = new View();
        $data = $this->request['POST'];
        try {
            $ContactCardsModel = new ContactCardsModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Card image uploaded succesfully'),
                        'data' => $ContactCardsModel->addCardVault($data, $_FILES)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function getCardVault() {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $ContactCardsModel = new ContactCardsModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Card image uploaded succesfully'),
                        'data' => $ContactCardsModel->getCardVault($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function deleteCardVault() {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $ContactCardsModel = new ContactCardsModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Card image deleted succesfully'),
                        'data' => $ContactCardsModel->deleteCardVault($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function CheckOCRContact() {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $ContactCardsModel = new ContactCardsModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Card contact listed succesfully'),
                        'data' => $ContactCardsModel->CheckOCRContact($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function AddOCRContact() {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $ContactCardsModel = new ContactCardsModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Card contact Added succesfully'),
                        'data' => $ContactCardsModel->AddOCRContact($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function AddOCRContactWithExisting() {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $ContactCardsModel = new ContactCardsModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Card contact listed succesfully'),
                        'data' => $ContactCardsModel->AddOCRContactWithExisting($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function GetCustomerCardVaultList() {
        $view = new View();
        $data = $this->request['POST'];
            try {
                $ContactCardsModel = new ContactCardsModel();
            return $view->json([
                        'error' => false,
                        'msg'   => __t('Get all customer succesfully.'),
                        'data'  => $ContactCardsModel->GetCustomerCardVaultList($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg'  => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error'=> true
            ]);
        }
    }

    public function deleteOCRContact() {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $ContactCardsModel = new ContactCardsModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Contact deleted succesfully'),
                        'data' => $ContactCardsModel->deleteOCRContact($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }
}
