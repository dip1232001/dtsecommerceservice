<?php

require_once CONTROLLER_PATH . DS . 'App.php';
require_once MODEL_PATH . DS . 'CopyEcomDataModel.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class CopyEcomData extends AppController
{    

    public function CopyData()
    {
        $view = new View();
        try {
            $CopyEcomDataModel = new CopyEcomDataModel();
            return $view->json([
                    'data' => $CopyEcomDataModel->CopyData($this->request['GET']),
                    'error' => false
            ]);
        } catch (Exception $exc) {
            return $view->json([
                'data' => [],
                'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                'error' => true
            ]);
        }
    }
}
