<?php

require_once CONTROLLER_PATH . DS . 'App.php';
require_once MODEL_PATH . DS . 'CouponModel.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Category
 *
 * @author dipankar
 */
class Coupon extends AppController {

    //put your code here

    public function GetCouponTypes() {
        $view = new View();
        try {
            $CouponModel = new CouponModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Coupon types retrived succesfully'),
                        'data' => $CouponModel->GetCouponTypes()
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function GetCouponTypeSpecial() {
        $view = new View();
        try {
            $CouponModel = new CouponModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Special coupon types retrived succesfully'),
                        'data' => $CouponModel->GetCouponTypeSpecial()
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function addCoupon() {
        $view = new View();
        try {
            $Coupon = new CouponModel();
            $addCat = $Coupon->addCoupon($this->request['POST']);
            return $view->json([
                        'error' => false,
                        'data' => $addCat,
                        'msg' => $addCat['msg']
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }
    
    public function getCoupon() {
        $view = new View();
        $BUID =$this->request['GET']['BUID'];
        $id = empty($this->request['GET']['id']) ? null : $this->request['GET']['id'];
        try {
            $Coupon = new CouponModel();
            $addCat = $Coupon->getCoupon($id, $BUID);
            return $view->json([
                        'error' => false,
                        'data' => $addCat,
                        'msg' => $addCat['msg']
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function getCouponByBU() {
        $view = new View();
        $BUID =$this->request['GET']['BUID'];
        $id = empty($this->request['GET']['id']) ? null : $this->request['GET']['id'];
        try {
            $Coupon = new CouponModel();
            $addCat = $Coupon->getCouponByBU($id, $BUID);
            return $view->json([
                        'error' => false,
                        'data' => $addCat,
                        'msg' => $addCat['msg']
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function getCouponByBUByType() {
        $view = new View();
        $BUID =$this->request['GET']['BUID'];
        $id = empty($this->request['GET']['id']) ? null : $this->request['GET']['id'];
        try {
            $Coupon = new CouponModel();
            $addCat = $Coupon->getCouponByBUByType($id, $BUID);
            return $view->json([
                        'error' => false,
                        'data' => $addCat,
                        'msg' => $addCat['msg']
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function getDiscount() {
        $view = new View();
        try {
            $Coupon = new CouponModel();
            $addCat = $Coupon->getDiscount($this->request['GET']);
            return $view->json([
                        'error' => false,
                        'data' => $addCat,
                        'msg' => $addCat['msg']
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function ChangeCouponStatus()
    {
        $view = new View();
        try {
            $Coupon = new CouponModel();

            return $view->json([
                        'error' => false,
                        'data' => $Coupon->ChangeCouponStatus($this->request['POST']),
                        'msg' => __t("Coupon rule status changed succesfully")
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function DeleteCoupon()
    {
        $view = new View();
        try {
            $Coupon = new CouponModel();

            return $view->json([
                        'error' => false,
                        'data' => $Coupon->DeleteCoupon($this->request['POST']),
                        'msg' =>null
            ]);
        } catch (Exception $exc) {
            return $view->json([
                    'data' => [],
                    'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                    'error' => true
            ]);
        }
    }

}
