<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Customer
 *
 * @author dipankar
 */
require_once CONTROLLER_PATH . DS . 'App.php';
require_once MODEL_PATH . DS . 'CustomersModel.php';
require_once MODEL_PATH . DS . 'CustomerAuth.php';

class Customer extends AppController {

    //put your code here
    public function __construct() {
        parent::__construct();
    }

    public function getCustomers() {
        $view = new View();
        $BUID = (int) $this->request['GET']['BUID'];
        $start = (int) $this->request['GET']['start'];
        $limit = (int) $this->request['GET']['limit'];
        $searchstring = trim(strip_tags($this->request['POST']['search']));
        try {
            $CustomerModel = new CustomersModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Customers retrived succesfully'),
                        'data' => $CustomerModel->GetCustomers($start, $limit, $searchstring, $BUID)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function getCustomer() {
        $view = new View();
        $BUID = trim(strip_tags($this->request['POST']['BUID']));
        $customerid = trim(strip_tags($this->request['POST']['id']));
        try {
            $CustomerModel = new CustomersModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Customer retrived succesfully'),
                        'data' => $CustomerModel->getCustomerDetails($customerid, $BUID)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function getCustomerAuth() {
        $view = new View();
        $customerid = trim(strip_tags($this->request['POST']['id']));
        try {
            $CustomerModel = new CustomerAuth();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Customer retrived succesfully'),
                        'data' => $CustomerModel->getCustomerDetails($customerid, $this->request['POST']['buid'])
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function addShippingAddress() {
        $data = $this->request['POST'];
        $data['address_type'] = 'shipping';
        $view = new View();
        try {
            $CustomerModel = new CustomersModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Customers address added succesfully'),
                        'data' => $CustomerModel->AddCustomerAddress($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function addBillingAddress() {
        $data = $this->request['POST'];
        $data['address_type'] = 'billing';
        $view = new View();
        try {
            $CustomerModel = new CustomersModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Customers address added succesfully'),
                        'data' => $CustomerModel->AddCustomerAddress($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function getBillingAddress() {
        $customerid = $this->request['POST']['customer_id'];
        $address_type = 'billing';
        $view = new View();
        try {
            $CustomerModel = new CustomersModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Customers billing address retrived succesfully'),
                        'data' => $CustomerModel->getAddresses($customerid, $address_type)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function getShippingAddress() {
        $customerid = $this->request['POST']['customer_id'];
        $address_type = 'shipping';
        $view = new View();
        try {
            $CustomerModel = new CustomersModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Customers shipping address retrived succesfully'),
                        'data' => $CustomerModel->getAddresses($customerid, $address_type)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function setDefaultShippingAddress() {
        $customerid = $this->request['POST']['customer_id'];
        $address_type = 'shipping';
        $id = $this->request['POST']['id'];
        $view = new View();
        try {
            $CustomerModel = new CustomersModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Shipping address set as default succesfully'),
                        'data' => $CustomerModel->setDefaultAddress($customerid, $id, $address_type)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function setDefaultBillingAddress() {
        $customerid = $this->request['POST']['customer_id'];
        $address_type = 'billing';
        $id = $this->request['POST']['id'];
        $view = new View();
        try {
            $CustomerModel = new CustomersModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Billing address set as default succesfully'),
                        'data' => $CustomerModel->setDefaultAddress($customerid, $id, $address_type)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function DeleteCustomerAdress() {
        $customerid = $this->request['POST']['customer_id'];
        $id = $this->request['POST']['id'];
        $view = new View();
        try {
            $CustomerModel = new CustomersModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Address deleted succesfully'),
                        'data' => $CustomerModel->DeleteCustomerAddress($customerid, $id)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function Register() {
        $data = $this->request['POST'];
        $view = new View();
        try {
            $CustomerModel = new CustomersModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Customer Created succesfully'),
                        'data' => $CustomerModel->RegisterUser($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function RegisterAuth() {
        $data = $this->request['POST'];
        $view = new View();
        try {
            $CustomerModel = new CustomerAuth();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Customer Created succesfully'),
                        'data' => $CustomerModel->RegisterUser($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function Login() {

        $view = new View();
        try {
            $CustomerModel = new CustomersModel();
            //$CustomerModel = new CustomerAuth();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Customer logged in succesfully'),
                        'data' => $CustomerModel->CustomerLogin($this->request['POST']['username'], $this->request['POST']['password'], $this->request['POST']['BUID'])
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function LoginAuth() {

        $view = new View();
        try {
            $CustomerModel = new CustomerAuth();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Customer logged in succesfully'),
                        'data' => $CustomerModel->CustomerLogin($this->request['POST']['email'], $this->request['POST']['password'], $this->request['POST']['buid'])
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function ForgotPasswordAuth() {

        $view = new View();
        try {
            $CustomerModel = new CustomerAuth();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Customer logged in succesfully'),
                        'data' => $CustomerModel->CustomerSendForgotEmail($this->request['POST']['email'], $this->request['POST']['buid'], $this->request['POST']['forgotpasswordlink'])
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function ResetThePassword() {

        $view = new View();
        try {
            $CustomerModel = new CustomerAuth();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Customer logged in succesfully'),
                        'data' => $CustomerModel->CustomerGetDataFROmhash($this->request['POST']['hash'], $this->request['POST']['loginlink'])
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function Changepassword() {

        $view = new View();
        try {
            $CustomerModel = new CustomerAuth();
            return $view->json([
                        'error' => false,
                        'msg' => __t('You password has been updated successfully'),
                        'data' => $CustomerModel->changePassword($this->request['POST'])
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }
    
      public function SocialLogin() {

        $view = new View();
        try {
            $CustomerModel = new CustomerAuth();
            return $view->json([
                        'error' => false,
                        'msg' => __t('You have logged in successfully'),
                        'data' => $CustomerModel->SocialLogin($this->request['POST'])
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }
    
    public function checkAddress() {
        $customerid = $this->request['POST']['customer_id'];
        $address_type = $this->request['POST']['address_type'];
        $address = $this->request['POST']['address'];;
        $view = new View();
        try {
            $CustomerModel = new CustomersModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t(''),
                        'data' => $CustomerModel->checkAddress($customerid, $address_type, $address)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    /*public function updateCustomer() {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $CustomerModel = new CustomerAuth();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Customer retrived succesfully'),
                        'data' => $CustomerModel->updateCustomer($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }*/

    public function updateCustomerMarketplacePlan() {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $CustomerModel = new CustomersModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Customer retrived succesfully'),
                        'data' => $CustomerModel->updateCustomerMarketplacePlan($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }
    

}
