<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ContactCards
 *
 * @author Abhijit Manna
 */

require_once CONTROLLER_PATH . DS . 'App.php';
require_once MODEL_PATH . DS . 'DormerModel.php';

class Dormer extends AppController {

    //put your code here
    public function __construct() {
        parent::__construct();
    }

    public function Register() {
        $data = $this->request['POST'];
        $view = new View();
        try {
            $DormerModel = new DormerModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Customer Created succesfully'),
                        'data' => $DormerModel->RegisterUser($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function Login() {

        $view = new View();
        try {
            $DormerModel = new DormerModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Customer logged in succesfully'),
                        'data' => $DormerModel->CustomerLogin($this->request['POST']['email'], $this->request['POST']['password'], $this->request['POST']['buid'])
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function getCustomer() {
        $view = new View();
        $customerid = $this->request['POST']['id'];
        try {
            $DormerModel = new DormerModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Customer retrived succesfully'),
                        'data' => $DormerModel->getCustomerDetails($customerid, $this->request['POST']['buid'])
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function Changepassword() {

        $view = new View();
        try {
            $DormerModel = new DormerModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('You password has been updated successfully'),
                        'data' => $DormerModel->changePassword($this->request['POST'])
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }
    
    public function SocialLogin() {

        $view = new View();
        try {
            $DormerModel = new DormerModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('You have logged in successfully'),
                        'data' => $DormerModel->SocialLogin($this->request['POST'])
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function SendForgotPasswordEmail() {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $DormerModel = new DormerModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Password reset email sent succesfully'),
                        'data' => $DormerModel->SendForgotPasswordEmail($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function CheckForgotPasswordKey() {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $DormerModel = new DormerModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Success'),
                        'data' => $DormerModel->CheckForgotPasswordKey($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function UpdateForgotPassword() {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $DormerModel = new DormerModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Paswsword change successfully'),
                        'data' => $DormerModel->UpdateForgotPassword($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function sendGiftEmail() {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $DormerModel = new DormerModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Email sent successfully'),
                        'data' => $DormerModel->sendGiftEmail($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function getCustomerPoint() {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $DormerModel = new DormerModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Get Point successfully'),
                        'data' => $DormerModel->getCustomerPoint($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function setCustomerPoint() {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $DormerModel = new DormerModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Get Point successfully'),
                        'data' => $DormerModel->setCustomerPoint($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }    
    
}
