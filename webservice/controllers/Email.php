<?php

require_once CONTROLLER_PATH . DS . 'App.php';
require_once MODEL_PATH . DS . 'EmailModel.php';

class Email extends AppController
{

    public function getEmailList()
    {
         $view = new View();
        try {
            $EmailModel = new EmailModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Emails retrived succesfully'),
                        'data' => $EmailModel->getEmailList()
            ]);
        } catch (Exception $exc) {
            return $view->json([
                      'data' => [],
                      'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                      'error' => true
            ]);
        }
    }

    public function getEmailDetails()
    {
         $view = new View();
        try {
            $EmailModel = new EmailModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Email Template retrived succesfully'),
                        'data' => $EmailModel->getEmailDetails($this->request['POST']['email_type'], $this->request['POST']['BUID'])
            ]);
        } catch (Exception $exc) {
            return $view->json([
                      'data' => [],
                      'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                      'error' => true
            ]);
        }
    }
    
    public function prepareEmailDetails()
    {
         $view = new View();
        try {
            $EmailModel = new EmailModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Email Template retrived succesfully'),
                        'data' => $EmailModel->prepareEmailData($this->request['POST']['order_id'],$this->request['POST']['email_type'])
            ]);
        } catch (Exception $exc) {
            return $view->json([
                      'data' => [],
                      'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                      'error' => true
            ]);
        }
    }
    
    public function editEmailDetails()
    {
         $view = new View();
        try {
            $EmailModel = new EmailModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Email Template Edited succesfully'),
                        'data' => $EmailModel->editEmailTemplate($this->request['POST'])
            ]);
        } catch (Exception $exc) {
            return $view->json([
                      'data' => [],
                      'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                      'error' => true
            ]);
        }
    }
}
