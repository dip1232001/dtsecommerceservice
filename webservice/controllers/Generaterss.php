<?php

require_once CONTROLLER_PATH . DS . 'App.php';
require_once MODEL_PATH . DS . 'GeneraterssModel.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Category
 *
 * @author dipankar
 */
class Generaterss extends AppController {

    //put your code here

    public function index() {
        $view = new View();
        try {
            $Rssmodel = new GeneraterssModel();
           return $view->json($Rssmodel->Generaterss($_GET['rssfeed']));
           
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }
}
