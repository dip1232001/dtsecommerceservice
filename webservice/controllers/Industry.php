<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Industry
 *
 * @author dipankar
 */
require_once CONTROLLER_PATH . DS . 'App.php';
require_once MODEL_PATH . DS . 'IndustryModel.php';

class Industry extends AppController {

    //put your code here

    public function addIndustry() {
        $view = new View();
        try {
            $inDustryModel = new IndustryModel();
            return $view->json([
                        'error' => false,
                        'data' => $inDustryModel->addIndustry($this->request['POST']),
                        'msg' => 'Industry added succesfully'
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'error' => true,
                        'data' => [],
                        'msg' => $exc->getMessage()
            ]);
        }
    }

    public function DeleteIndustry() {
        $view = new View();
        try {
            $inDustryModel = new IndustryModel();
            return $view->json([
                        'error' => false,
                        'data' => $inDustryModel->deleteIndustry($this->request['POST']['industryIDS']),
                        'msg' => 'Industry deleted succesfully'
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'error' => true,
                        'data' => [],
                        'msg' => $exc->getMessage()
            ]);
        }
    }

    public function MapIndustry() {
        $view = new View();
        try {
            $inDustryModel = new IndustryModel();
            return $view->json([
                        'error' => false,
                        'data' => $inDustryModel->mapBuWithIndustry((int) $this->request['POST']['industryID'], (int) $this->request['POST']['BUID']),
                        'msg' => 'Industry deleted succesfully'
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'error' => true,
                        'data' => [],
                        'msg' => $exc->getMessage()
            ]);
        }
    }

}
