<?php

require_once CONTROLLER_PATH . DS . 'App.php';
require_once MODEL_PATH . DS . 'LambdaProductModel.php';
require_once MODEL_PATH . DS . 'PriceModel.php';
require_once MODEL_PATH . DS . 'CartModel.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class LambdaProduct extends AppController
{    

    public function GetCategories()
    {
        $buid = $this->request['GET']['buid'];
        $start = empty($this->request['GET']['start']) ? 0 : $this->request['GET']['start'];
        $limit = (empty($this->request['GET']['limit']) || $this->request['GET']['limit'] > 9 ) ? 9 : $this->request['GET']['limit'];
        $view = new View();
        try {
            $LambdaProductModel = new LambdaProductModel();
            return $view->json([
                    'data' => $LambdaProductModel->GetCategories($buid, $start, $limit),
                    'error' => false
            ]);
        } catch (Exception $exc) {
            return $view->json([
                'data' => [],
                'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                'error' => true
            ]);
        }
    }

    public function getAllProducts()
    {
        $buid = $this->request['GET']['buid'];
        $start = empty($this->request['GET']['start']) ? 0 : $this->request['GET']['start'];
        $limit = (empty($this->request['GET']['limit']) || $this->request['GET']['limit'] > 9 ) ? 9 : $this->request['GET']['limit'];
        $category_id = (empty($this->request['GET']['category_id']) || $this->request['GET']['category_id'] == '' ) ? '' : $this->request['GET']['category_id'];
        $name = '';

        $view = new View();
        //dd($category_id, true);
        try {
            $LambdaProductModel = new LambdaProductModel();
            return $view->json([
                    'data' => $LambdaProductModel->Getproducts($buid, $start, $limit, $this->request['POST'], $category_id, $name),
                    'error' => false
            ]);
        } catch (Exception $exc) {
            return $view->json([
                'data' => [],
                'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                'error' => true
            ]);
        }
    }

    public function addAppointment()
    {
        $data['BUID'] = $this->request['GET']['BUID'];
        $data['name'] = $this->request['GET']['name'];
        $data['phone'] = $this->request['GET']['phone'];
        $data['bookingDate'] = $this->request['GET']['bookingDate'];
        $data['bookingTime'] = $this->request['GET']['bookingTime'];
        $data['description'] = $this->request['GET']['description'];
        $view = new View();
        try {
            $LambdaProductModel = new LambdaProductModel();
            return $view->json([
                    'data' => $LambdaProductModel->addAppointment($data),
                    'msg' => 'Appoinment added succesfully',
                    'error' => false
            ]);
        } catch (Exception $exc) {
            return $view->json([
                'data' => [],
                'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                'error' => true
            ]);
        }
    }

    public function GetAppointments()
    {
        $buid = $this->request['GET']['buid'];
        $start = empty($this->request['GET']['start']) ? 0 : $this->request['GET']['start'];
        $limit = (empty($this->request['GET']['limit']) || $this->request['GET']['limit'] > 9 ) ? 9 : $this->request['GET']['limit'];
        $view = new View();
        try {
            $LambdaProductModel = new LambdaProductModel();
            return $view->json([
                    'data' => $LambdaProductModel->GetAppointments($buid, $start, $limit),
                    'error' => false
            ]);
        } catch (Exception $exc) {
            return $view->json([
                'data' => [],
                'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                'error' => true
            ]);
        }
    }

    public function placeLambdaOrder()
    {
        /*$data['BUID'] = $this->request['GET']['BUID'];
        $data['email'] = $this->request['GET']['email'];
        $data['phone'] = $this->request['GET']['phone'];
        $data['bookingDate'] = $this->request['GET']['bookingDate'];
        $data['bookingTime'] = $this->request['GET']['bookingTime'];
        $data['description'] = $this->request['GET']['description'];*/
        $view = new View();
        try {
            $LambdaProductModel = new LambdaProductModel();
            return $view->json([
                    'data' => $LambdaProductModel->placeLambdaOrder($this->request['POST']),
                    'msg' => 'Appoinment added succesfully',
                    'error' => false
            ]);
        } catch (Exception $exc) {
            return $view->json([
                'data' => [],
                'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                'error' => true
            ]);
        }
    }
    
}
