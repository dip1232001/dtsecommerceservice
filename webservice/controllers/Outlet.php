<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Industry
 *
 * @author dipankar
 */
require_once CONTROLLER_PATH . DS . 'App.php';
require_once MODEL_PATH . DS . 'OutletModel.php';

class Outlet extends AppController {
    
    public function getOutletForBusinessFrotend() {
        $view = new View();
        try {
            $outletModel = new OutletModel();
            return $view->json([
                        'error' => false,
                        'data' => $outletModel->getOutletsForFrontend($this->request['GET']),
                        'msg' => 'Outlet listed succesfully'
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'error' => true,
                        'data' => [],
                        'msg' => $exc->getMessage()
            ]);
        }
    }
    
    public function getOutletByBusinessOutletId() {
        $view = new View();
        try {
            $outletModel = new OutletModel();
            return $view->json([
                        'error' => false,
                        'data' => $outletModel->getOutletsByBusinessOutletId($this->request['GET']),
                        'msg' => 'Single Outlet succesfully'
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'error' => true,
                        'data' => [],
                        'msg' => $exc->getMessage()
            ]);
        }
    }
    
    public function addOutlet() {
        $view = new View();
        try {
            $outletModel = new OutletModel();
            return $view->json([
                        'error' => false,
                        'data' => $outletModel->addOutlet($this->request['POST']),
                        'msg' => 'Outlet added succesfully'
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'error' => true,
                        'data' => [],
                        'msg' => $exc->getMessage()
            ]);
        }
    }
    
    public function deleteOutlet() {
        $view = new View();
        try {
            $outletModel = new OutletModel();
            return $view->json([
                        'error' => false,
                        'data' => $outletModel->deleteOutlet($this->request['POST']),
                        'msg' => 'Outlet deleted succesfully'
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'error' => true,
                        'data' => [],
                        'msg' => $exc->getMessage()
            ]);
        }
    }

}
