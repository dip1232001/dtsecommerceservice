<?php
require_once CONTROLLER_PATH . DS . 'App.php';
require_once MODEL_PATH . DS . 'PaymentModel.php';
require_once MODEL_PATH . DS . 'CartModel.php';
class Payment extends AppController
{
    public function PaypalIpn()
    {
         $view = new View();
        try {
            $PaymentModel = new PaymentModel();
           

            return $view->json([
                    'data' => $PaymentModel->__processPaypal(),
                    'error' => false,
                    'msg' => ""
            ]);
        } catch (Exception $exc) {
              file_put_contents(MODEL_PATH . DS .'test.txt', print_r($exc->getMessage(), true), FILE_APPEND);
            return $view->json([
                'data' => [],
                'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                'error' => true
            ]);
        }
    }

    public function ChangePaymentStatus()
    {
        $view = new View();
        try {
            $PaymentModel = new PaymentModel();
           

            return $view->json([
                    'data' => $PaymentModel->ChangePaymentStatus($this->request['POST']['orderids'], $this->request['POST']['status']),
                    'error' => false,
                    'msg' => ""
            ]);
        } catch (Exception $exc) {
              file_put_contents(MODEL_PATH . DS .'test.txt', print_r($exc->getMessage(), true), FILE_APPEND);
            return $view->json([
                'data' => [],
                'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                'error' => true
            ]);
        }
    }


    public function stripePayment(){
        $view = new View();
        try {
            $cartModel = new CartModel();
           

            return $view->json([
                    'data' => $cartModel->processorderWithStripe($this->request['POST']),
                    'error' => false,
                    'msg' => ""
            ]);
        } catch (Exception $exc) {
              file_put_contents(MODEL_PATH . DS .'test.txt', print_r($exc->getMessage(), true), FILE_APPEND);
            return $view->json([
                'data' => [],
                'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                'error' => true
            ]);
        } 
    }
}
