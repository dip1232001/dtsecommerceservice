<?php

require_once CONTROLLER_PATH . DS . 'App.php';
require_once MODEL_PATH . DS . 'PreorderModel.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Category
 *
 * @author dipankar
 */
class Preorder extends AppController {

    //put your code here

    public function addPreorder() {
        $view = new View();
        try {
            $PreorderModel = new PreorderModel();
            $addCat = $PreorderModel->addPreorder($this->request['POST']);
            return $view->json([
                        'error' => false,
                        'data' => $addCat,
                        'msg' => $addCat['msg']
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function getPreorder() {
        $view = new View();
        try {
            $PreorderModel = new PreorderModel();
            return $view->json([
                        'error' => false,
                        'data' => $PreorderModel->getPreorder($this->request['GET']),
                        'msg' => 'Preorder retrived'
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function addPreorderTable() {
        $view = new View();
        try {
            $PreorderModel = new PreorderModel();
            $addCat = $PreorderModel->addPreorderTable($this->request['POST']);
            return $view->json([
                        'error' => false,
                        'data' => $addCat,
                        'msg' => $addCat['msg']
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function getPreorderTable() {
        $view = new View();
        try {
            $PreorderModel = new PreorderModel();
            return $view->json([
                        'error' => false,
                        'data' => $PreorderModel->getPreorderTable($this->request['GET']),
                        'msg' => 'Preorder retrived'
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function GetPreOrderSettingsByDate() {
        $view = new View();
        try {
            $PreorderModel = new PreorderModel();
            $addCat = $PreorderModel->GetPreOrderSettingsByDate($this->request['POST']);
            return $view->json([
                        'error' => false,
                        'data' => $addCat,
                        'msg' => $addCat['msg']
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function GetPreOrderTableListByDateAndTime() {
        $view = new View();
        try {
            $PreorderModel = new PreorderModel();
            $addCat = $PreorderModel->GetPreOrderTableListByDateAndTime($this->request['POST']);
            return $view->json([
                        'error' => false,
                        'data' => $addCat,
                        'msg' => $addCat['msg']
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function deletePreOrderTable() {
        $view = new View();
        try {
            $PreorderModel = new PreorderModel();
            return $view->json([
                        'error' => false,
                        'data' => $PreorderModel->deletePreOrderTable($this->request['POST']),
                        'msg' => 'Table deleted succesfully'
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'error' => true,
                        'data' => [],
                        'msg' => $exc->getMessage()
            ]);
        }
    }

    public function GetNewOrderCount() {
        $view = new View();
        try {
            $PreorderModel = new PreorderModel();
            $addCat = $PreorderModel->GetNewOrderCount($this->request['POST']);
            return $view->json([
                        'error' => false,
                        'data' => $addCat,
                        'msg' => $addCat['msg']
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }



}
