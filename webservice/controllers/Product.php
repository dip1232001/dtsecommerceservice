<?php

require_once CONTROLLER_PATH . DS . 'App.php';
require_once MODEL_PATH . DS . 'ProductModel.php';
require_once MODEL_PATH . DS . 'PriceModel.php';
require_once MODEL_PATH . DS . 'CartModel.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Product extends AppController
{

    public function addProduct()
    {
        $view = new View();
        try {
            $ProductModel = new ProductModel();
            return $view->json([
                    'data' => $ProductModel->addProduct($this->request['POST'], $this->request['FILES']),
                    'msg' => 'Product added succesfully',
                    'error' => false
            ]);
        } catch (Exception $exc) {
            return $view->json([
                'data' => [],
                'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                'error' => true
            ]);
        }
    }

    public function addPriceType()
    {
        $view = new View();
        try {
            $PriceModel = new PriceModel();
            return $view->json([
                    'data' => $PriceModel->addPriceType($this->request['POST']),
                    'msg' => 'Price added succesfully',
                    'error' => false
            ]);
        } catch (Exception $exc) {
            return $view->json([
                'data' => [],
                'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                'error' => true
            ]);
        }
    }

    public function getAllProducts()
    {
        $BUID = $this->request['GET']['BUID'];
        $start = empty($this->request['GET']['start']) ? 0 : $this->request['GET']['start'];
        $limit = (empty($this->request['GET']['limit']) || $this->request['GET']['limit'] > 10 ) ? 10 : $this->request['GET']['limit'];
        $category_id = (empty($this->request['GET']['category_id']) || $this->request['GET']['category_id'] == '' ) ? '' : $this->request['GET']['category_id'];
        $name = (empty($this->request['GET']['name']) || $this->request['GET']['name'] == '' ) ? '' : $this->request['GET']['name'];
        $category_slug = (empty($this->request['GET']['category_slug']) || $this->request['GET']['category_slug'] == '' ) ? '' : $this->request['GET']['category_slug'];
        if(!empty($this->request['GET']['isfeatured'])){
            $this->request['POST']['isfeatured'] = (empty($this->request['GET']['isfeatured']) || $this->request['GET']['isfeatured'] == '' ) ? '' : $this->request['GET']['isfeatured'];
        }
        $view = new View();
        //dd($category_id, true);
        try {
            $ProductModel = new ProductModel();
            return $view->json([
                    'data' => $ProductModel->Getproducts($start, $limit, $this->request['POST'], $category_id, $name, $BUID,$category_slug),
                    'error' => false
            ]);
        } catch (Exception $exc) {
            return $view->json([
                'data' => [],
                'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                'error' => true
            ]);
        }
    }

    public function getProduct()
    {
        $view = new View();
        try {
            $ProductModel = new ProductModel();
            return $view->json([
                    'data' => $ProductModel->getProduct($this->request['GET']['ProductID'], $this->request['GET']['BUID'],$this->request['GET']['ProductSlug']),
                    'error' => false
            ]);
        } catch (Exception $exc) {
            return $view->json([
                'data' => [],
                'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                'error' => true
            ]);
        }
    }

    public function getPriceTypes()
    {
        $view = new View();
        try {
            $PriceModel = new PriceModel();
            return $view->json([
                    'data' => $PriceModel->getPrieTypes(),
                    'error' => false
            ]);
        } catch (Exception $exc) {
            return $view->json([
                'data' => [],
                'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                'error' => true
            ]);
        }
    }

    public function changeProductStats()
    {
        $view = new View();
        try {
            $ProductModel = new ProductModel();
            return $view->json([
                    'data' => $ProductModel->makeInactive($this->request['POST']),
                    'error' => false
            ]);
        } catch (Exception $exc) {
            return $view->json([
                'data' => [],
                'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                'error' => true
            ]);
        }
    }

    public function DeleteProduct()
    {
        $view = new View();
        try {
            $ProductModel = new ProductModel();
            return $view->json([
                    'data' => $ProductModel->deleteProduct($this->request['POST']),
                    'error' => false,
                    'msg' => __t('Product deleted succesfully')
            ]);
        } catch (Exception $exc) {
            return $view->json([
                'data' => [],
                'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                'error' => true
            ]);
        }
    }

    public function AddToCart()
    {
        $view = new View();
        try {
            $CartModel = new CartModel();
            return $view->json([
                    'data' => $CartModel->AddToCart($this->request['POST']),
                    'error' => false,
                    'msg' => __t('Product added to cart succesfully')
            ]);
        } catch (Exception $exc) {
            return $view->json([
                'data' => [],
                'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                'error' => true
            ]);
        }
    }

    public function DeleteFromCart()
    {
        $view = new View();
        try {
            $CartModel = new CartModel();
            return $view->json([
                    'data' => $CartModel->deleteFromCart($this->request['POST']),
                    'error' => false,
                    'msg' => __t('cart item deleted succesfully')
            ]);
        } catch (Exception $exc) {
            return $view->json([
                'data' => [],
                'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                'error' => true
            ]);
        }
    }

    public function getCartItems()
    {
        $view = new View();
        try {
            $CartModel = new CartModel();
            return $view->json([
                    'data' => $CartModel->getCartitems($this->request['POST']),
                    'error' => false,
                    'msg' => ''
            ]);
        } catch (Exception $exc) {
            return $view->json([
                'data' => [],
                'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                'error' => true
            ]);
        }
    }

    public function getPaymentTypes()
    {
        $view = new View();
        try {
            $CartModel = new CartModel();
            return $view->json([
                    'data' => $CartModel->getPaymentTypes($this->request['GET']['BUID']),
                    'error' => false,
                    'msg' => ''
            ]);
        } catch (Exception $exc) {
            return $view->json([
                'data' => [],
                'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                'error' => true
            ]);
        }
    }

    public function gePaymentStatusList()
    {
        $view = new View();
        try {
            $CartModel = new CartModel();
            return $view->json([
                    'data' => $CartModel->gePaymentStatusList(),
                    'error' => false,
                    'msg' => ''
            ]);
        } catch (Exception $exc) {
            return $view->json([
                'data' => [],
                'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                'error' => true
            ]);
        }
    }

    public function GetOrderStatusList()
    {
        $view = new View();
        try {
            $CartModel = new CartModel();
            return $view->json([
                    'data' => $CartModel->GetOrderStatusList($this->request['GET']['BUID']),
                    'error' => false,
                    'msg' => ''
            ]);
        } catch (Exception $exc) {
            return $view->json([
                'data' => [],
                'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                'error' => true
            ]);
        }
    }

    public function PrepareOrder()
    {
        $view = new View();
        try {
            $CartModel = new CartModel();
            return $view->json([
                    'data' => $CartModel->PrepareOrder($this->request['POST']),
                    'error' => false,
                    'msg' => ''
            ]);
        } catch (Exception $exc) {
            return $view->json([
                'data' => [],
                'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                'error' => true
            ]);
        }
    }

    public function BuyNow(){
        $view= new View();
        try {
            $CartModel = new CartModel();
            return $view->json([
                    'data' => $CartModel->PrepareSingleOrder($this->request['POST']),
                    'error' => false,
                    'msg' => ''
            ]);
        } catch (Exception $exc) {
            return $view->json([
                'data' => [],
                'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                'error' => true
            ]);
        }
    }

    public function getorders()
    {
        $view = new View();
        try {
            $CartModel = new CartModel();

            return $view->json([
                    'data' => $CartModel->getAllorders($this->request['GET']['start'], $this->request['GET']['limit'], $this->request['POST'], $this->request['GET']['BUID']),
                    'error' => false,
                    'msg' => ''
            ]);
        } catch (Exception $exc) {
            return $view->json([
                'data' => [],
                'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                'error' => true
            ]);
        }
    }

    public function ChangeOrderStatus()
    {
        $view = new View();
        try {
            $CartModel = new CartModel();

            return $view->json([
                    'data' => $CartModel->ChangeorderStatus($this->request['POST']['orderids'], $this->request['POST']['status']),
                    'error' => false,
                    'msg' => __t("Order Status changed successfully")
            ]);
        } catch (Exception $exc) {
            return $view->json([
                'data' => [],
                'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                'error' => true
            ]);
        }
    }
    
    public function UpdateTracking()
    {
        $view = new View();
        try {
            $CartModel = new CartModel();

            return $view->json([
                    'data' => $CartModel->updateOrder($this->request['POST']),
                    'error' => false,
                    'msg' => __t("Order shipment updated successfully")
            ]);
        } catch (Exception $exc) {
            return $view->json([
                'data' => [],
                'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                'error' => true
            ]);
        }
    }

    public function ImportProducts()
    {
        $view = new View();
        try {
            $ProductModel = new ProductModel();
           

            return $view->json([
                    'data' => $ProductModel->importProducts($this->request['FILES']),
                    'error' => false,
                    'msg' => __t("Product imported succesfully, you can add images and varients from the product details")
            ]);
        } catch (Exception $exc) {
            return $view->json([
                'data' => [],
                'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                'error' => true
            ]);
        }
    }

    public function AddTestimonial() {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $ProductModel = new ProductModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Testimonial added succesfully.'),
                        'data' => $ProductModel->AddProductTestimonial($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function GetTestimonial() {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $ProductModel = new ProductModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Person listed succesfully'),
                        'data' => $ProductModel->getProductTestimonials($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function deleteProductImage()
    {
        $view = new View();
        try {
            $ProductModel = new ProductModel();
            return $view->json([
                    'data' => $ProductModel->deleteProductImage($this->request['POST']),
                    'error' => false,
                    'msg' => __t('Product Image deleted succesfully')
            ]);
        } catch (Exception $exc) {
            return $view->json([
                'data' => [],
                'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                'error' => true
            ]);
        }
    }

    public function makeDefaultProductImage()
    {
        $view = new View();
        try {
            $ProductModel = new ProductModel();
            return $view->json([
                    'data' => $ProductModel->makeDefaultProductImage($this->request['POST']),
                    'error' => false,
                    'msg' => __t('Product Image deleted succesfully')
            ]);
        } catch (Exception $exc) {
            return $view->json([
                'data' => [],
                'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                'error' => true
            ]);
        }
    }

    public function deleteProductVideo()
    {
        $view = new View();
        try {
            $ProductModel = new ProductModel();
            return $view->json([
                    'data' => $ProductModel->deleteProductVideo($this->request['POST']),
                    'error' => false,
                    'msg' => __t('Product Video deleted succesfully')
            ]);
        } catch (Exception $exc) {
            return $view->json([
                'data' => [],
                'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                'error' => true
            ]);
        }
    }

    public function deleteProductDocument()
    {
        $view = new View();
        try {
            $ProductModel = new ProductModel();
            return $view->json([
                    'data' => $ProductModel->deleteProductDocument($this->request['POST']),
                    'error' => false,
                    'msg' => __t('Product Document deleted succesfully')
            ]);
        } catch (Exception $exc) {
            return $view->json([
                'data' => [],
                'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                'error' => true
            ]);
        }
    }

    public function deleteProductTestimonial()
    {
        $view = new View();
        try {
            $ProductModel = new ProductModel();
            return $view->json([
                    'data' => $ProductModel->deleteProductTestimonial($this->request['POST']),
                    'error' => false,
                    'msg' => __t('Product Testimonial deleted succesfully')
            ]);
        } catch (Exception $exc) {
            return $view->json([
                'data' => [],
                'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                'error' => true
            ]);
        }
    }

    public function getAllProductsByARTag()
    {
        $BUID = $this->request['GET']['BUID'];
        $start = empty($this->request['GET']['start']) ? 0 : $this->request['GET']['start'];
        $limit = (empty($this->request['GET']['limit']) || $this->request['GET']['limit'] > 10 ) ? 10 : $this->request['GET']['limit'];
        $view = new View();
        try {
            $ProductModel = new ProductModel();
            return $view->json([
                    'data' => $ProductModel->getAllProductsByARTag($BUID, $start, $limit, $this->request['POST']),
                    'error' => false
            ]);
        } catch (Exception $exc) {
            return $view->json([
                'data' => [],
                'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                'error' => true
            ]);
        }
    }

    public function generateProductQR()
    {
        $view = new View();
        $data = $this->request['POST'];
        $ProductModel = new ProductModel();
        //$ProductModel->generateProductQR($data);
        try {
            $ProductModel = new ProductModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Person listed succesfully'),
                        'data' => $ProductModel->generateProductQR($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
        //$start = empty($this->request['GET']['start']) ? 0 : $this->request['GET']['start'];
        //$limit = (empty($this->request['GET']['limit']) || $this->request['GET']['limit'] > 10 ) ? 10 : $this->request['GET']['limit'];
        //$view = new View();.
        /*$view = new View();
        $data = $this->request['POST'];
        try {
            $ProductModel = new ProductModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Person listed succesfully'),
                        'data' => $ProductModel->getProductTestimonials($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }*/
        /*try {
            $ProductModel = new ProductModel();
            return $view->json([
                    'data' => $ProductModel->getAllProductsByARTag($start, $limit, $this->request['POST']),
                    'error' => false
            ]);
        } catch (Exception $exc) {
            return $view->json([
                'data' => [],
                'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                'error' => true
            ]);
        }*/
    }

    public function generateProductQRPDF()
    {
        $view = new View();
        $data = $this->request['POST'];
        $ProductModel = new ProductModel();
        //$ProductModel->generateProductQR($data);
        try {
            $ProductModel = new ProductModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Person listed succesfully'),
                        'data' => $ProductModel->generateProductQRPDF($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }

    }

    public function getrawdata(){
        $view = new View();
        $data = $this->request['POST'];



        $url = $data['img'];
        $folder="../qrimg/";
        $file_name=time()."-QR.png";
        $file_nameR='qrimg/'.$file_name;
        $file_name=$folder.$file_name;
        
        $fp = fopen ($file_name, 'w+');
        //Here is the file we are downloading, replace spaces with %20
        $ch = curl_init(str_replace(" ","%20",$url));
        curl_setopt($ch, CURLOPT_TIMEOUT, 50);
        // write curl response to file
        curl_setopt($ch, CURLOPT_FILE, $fp); 
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        // get curl response
        curl_exec($ch); 
        curl_close($ch);
        fclose($fp);

        //copy($url, $file_name);
        
        try {
            $ProductModel = new ProductModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Person listed succesfully'),
                        'data' => $file_nameR
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function getDefinedARTagsByBU()
    {
        $view = new View();
        try {
            $PriceModel = new PriceModel();
            return $view->json([
                    'data' => $PriceModel->getDefinedARTagsByBU($this->request['POST']),
                    'msg' => 'Price added succesfully',
                    'error' => false
            ]);
        } catch (Exception $exc) {
            return $view->json([
                'data' => [],
                'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                'error' => true
            ]);
        }
    }

    public function GetProductOrderForCategory()
    {
        $BUID = $this->request['GET']['BUID'];
        $category_id = (empty($this->request['GET']['category_id']) || $this->request['GET']['category_id'] == '' ) ? '' : $this->request['GET']['category_id'];
        $name = (empty($this->request['GET']['name']) || $this->request['GET']['name'] == '' ) ? '' : $this->request['GET']['name'];
        $view = new View();
        //dd($category_id, true);
        try {
            $ProductModel = new ProductModel();
            return $view->json([
                    'data' => $ProductModel->GetProductOrderForCategory($category_id, $BUID, $name),
                    'error' => false
            ]);
        } catch (Exception $exc) {
            return $view->json([
                'data' => [],
                'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                'error' => true
            ]);
        }
    }

    public function SetProductOrderForCategory()
    {
        $BUID = $this->request['GET']['BUID'];
        $view = new View();
        //dd($category_id, true);
        try {
            $ProductModel = new ProductModel();
            return $view->json([
                    'data' => $ProductModel->SetProductOrderForCategory($this->request['POST'], $BUID),
                    'error' => false
            ]);
        } catch (Exception $exc) {
            return $view->json([
                'data' => [],
                'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                'error' => true
            ]);
        }
    }

}
