<?php

require_once CONTROLLER_PATH . DS . 'App.php';
require_once MODEL_PATH . DS . 'ReportsModel.php';

class Report extends AppController
{
    public function GetReports()
    {
        $view = new View();
        try {
            $ReportModel = new ReportsModels();

            return $view->json([
                        'error' => false,
                        'data' => $ReportModel->SalesReport($this->request['POST']),
                        'msg' => null
            ]);
        } catch (Exception $exc) {
            return $view->json([
                  'data' => [],
                  'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                  'error' => true
            ]);
        }
    }
}
