<?php
require_once CONTROLLER_PATH . DS . 'App.php';
require_once MODEL_PATH . DS . 'ServicesModel.php';


class Services extends AppController
{

    public function setBuFeedPush()
    {
        $view = new View();
        try {
            $servicesModel = new ServicesModel();
            return $view->json([
                'msg' => __t("Push status set for bu."),
                'data' => $servicesModel->setBuFeedStatus($this->request['POST']),
                'error' => false
            ]);
        } catch (Exception $exc) {
            return $view->json([
                'data' => [],
                'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                'error' => true
            ]);
        }
    }


    public function sendPushNotifications(){
        $view = new View();
        try {
            $servicesModel = new ServicesModel();
            return $view->json([
                'msg' => __t("Push status set for bu."),
                'data' => $servicesModel->FeedPush(),
                'error' => false
            ]);
        } catch (Exception $exc) {
            return $view->json([
                'data' => [],
                'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                'error' => true
            ]);
        }
    }
    public function sendPushNotificationsFortwentyfour(){
        $view = new View();
        try {
            $servicesModel = new ServicesModel();
            return $view->json([
                'msg' => __t("Push status set for bu."),
                'data' => $servicesModel->FeedPush('t24PushProduction.pem','AIzaSyD9jF1bAXfZpraIYCudB_XB3k3gTyITKPc',9999),
                'error' => false
            ]);
        } catch (Exception $exc) {
            return $view->json([
                'data' => [],
                'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                'error' => true
            ]);
        }
    }
}