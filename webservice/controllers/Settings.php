<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Settings
 *
 * @author dipan
 */
require_once CONTROLLER_PATH . DS . 'App.php';
require_once MODEL_PATH . DS . 'GeneralSettingsModel.php';
require_once MODEL_PATH . DS . 'ContactCardsModel.php';

class Settings extends AppController
{

    //put your code here

    public function __construct()
    {
        parent::__construct();
    }

    public function setSettings()
    {
        $view = new View();
        try {
            $SettingsModel = new GeneralSettingsModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Settings added successfully'),
                        'data' => $SettingsModel->setGeneralSettings($this->request['POST'], $_FILES)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function GetCurrencyLists()
    {
        $view = new View();
        try {
            $SettingsModel = new GeneralSettingsModel();
            return $view->json([
                        'error' => false,
                        'msg' => '',
                        'data' => $SettingsModel->getCurrencyList()
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function GetGeneralSettings()
    {
        $view = new View();
        try {
            $SettingsModel = new GeneralSettingsModel();
            return $view->json([
                        'error' => false,
                        'msg' => '',
                        'data' => $SettingsModel->getGeneralSettings($this->request['GET']['BUID'])
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }


    public function setPayMentMethods()
    {
            $view = new View();
        try {
            $SettingsModel = new GeneralSettingsModel();
            return $view->json([
                        'error' => false,
                        'msg' => '',
                        'data' => $SettingsModel->AddPaymentTypes($this->request['POST'])
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function getAllPayments()
    {
            $view = new View();
        try {
            $SettingsModel = new GeneralSettingsModel();
            return $view->json([
                        'error' => false,
                        'msg' => '',
                        'data' => $SettingsModel->GetAllPaymentMethods()
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function getAddedPaymentTypes()
    {
            $view = new View();
        try {
            $SettingsModel = new GeneralSettingsModel();
            return $view->json([
                        'error' => false,
                        'msg' => '',
                        'data' => $SettingsModel->getPaymentMethods($this->request['POST'])
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function GetCountryCodes() {
        try {
            $view = new View();
            $data = $this->request['POST'];
            $ContactCardsModel = new ContactCardsModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Country codes retrived succesfully'),
                        'data' => $ContactCardsModel->GetCountryCodes()
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function enableSettings()
    {
        $view = new View();
        try {
            $SettingsModel = new GeneralSettingsModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Settings added successfully'),
                        'data' => $SettingsModel->enableSettings($this->request['POST'])
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function GetDeliveyOptionsByBU()
    {
        $view = new View();
        //dd($this->request['POST']);
        try {
            $SettingsModel = new GeneralSettingsModel();
            return $view->json([
                        'error' => false,
                        'msg' => '',
                        'data' => $SettingsModel->GetDeliveyOptionsByBU($this->request['POST'])
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function DeleteEcomForBU()
    {
        $view = new View();
        try {
            $SettingsModel = new GeneralSettingsModel();
            return $view->json([
                        'error' => false,
                        'msg' => 'Ecom data delete request accepts successfully.',
                        'data' => $SettingsModel->DeleteEcomForBU($this->request['GET']['buid'])
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }
}
