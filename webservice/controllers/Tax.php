<?php

require_once CONTROLLER_PATH . DS . 'App.php';
require_once MODEL_PATH . DS . 'TaxModel.php';

class Tax extends AppController
{

    public function addTax()
    {
        $view = new View();
        try {
            $TaxModel = new TaxModel();

            return $view->json([
                        'error' => false,
                        'data' => $TaxModel->AddTaxRule($this->request['POST']),
                        'msg' => __t("Tax rule added succesfully")
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function ChangeTaxStatus()
    {
        $view = new View();
        try {
            $TaxModel = new TaxModel();

            return $view->json([
                        'error' => false,
                        'data' => $TaxModel->TaxRuleStatusChange($this->request['POST']),
                        'msg' => __t("Tax rule status changed succesfully")
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }



    public function getTaxes()
    {
        $view = new View();
        try {
            $TaxModel = new TaxModel();

            return $view->json([
                        'error' => false,
                        'data' => $TaxModel->getTaxRules($this->request['POST']),
                        'msg' =>null
            ]);
        } catch (Exception $exc) {
            return $view->json([
                    'data' => [],
                    'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                    'error' => true
            ]);
        }
    }

    public function DeleteTaxes()
    {
        $view = new View();
        try {
            $TaxModel = new TaxModel();

            return $view->json([
                        'error' => false,
                        'data' => $TaxModel->DeleteTax($this->request['POST']),
                        'msg' =>null
            ]);
        } catch (Exception $exc) {
            return $view->json([
                    'data' => [],
                    'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                    'error' => true
            ]);
        }
    }

    public function DeleteMultiTaxes()
    {
        $view = new View();
        try {
            $TaxModel = new TaxModel();

            return $view->json([
                        'error' => false,
                        'data' => $TaxModel->DeleteMultiTaxes($this->request['POST']),
                        'msg' =>'Tax rules deleted successfully.'
            ]);
        } catch (Exception $exc) {
            return $view->json([
                    'data' => [],
                    'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                    'error' => true
            ]);
        }
    }
}
