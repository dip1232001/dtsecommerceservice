<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AugmentedReality
 *
 * @author Achyut Sinha
 */

require_once CONTROLLER_PATH . DS . 'App.php';
require_once MODEL_PATH . DS . 'UploadfilesModel.php';

class Uploadfile extends AppController {

    //put your code here
    public function __construct() {
        parent::__construct();
    }

    public function uploadFile() {
        $view = new View();
        $data = $this->request['POST'];
        $files = $_FILES;
        //dd($data);
        //dd($files,true);
        try {
            $UploadfilesModel = new UploadfilesModel();
            $res = $UploadfilesModel->uploadFile($files,$data);
            return $view->json([
                        'error' => false,
                        'msg' => __t('Component saved succesfully'),
                        'data' => $res["URL"],
                        'size' => $res["SIZE"]
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function getImageDataFromURL() {
        $view = new View();
        $data = $this->request['GET'];
        //dd($data, true);
        try {
            $ContactCardsModel = new UploadfilesModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('HTML succesfully'),
                        'data' => $ContactCardsModel->getImageData($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function deleteFileS3() {
        $view = new View();
        $data = $this->request['POST'];
        //dd($data);
        //dd($files,true);
        try {
            $UploadfilesModel = new UploadfilesModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Component saved succesfully'),
                        'data' => $UploadfilesModel->deleteFileS3($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

}
