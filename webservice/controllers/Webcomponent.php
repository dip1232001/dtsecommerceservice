<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AugmentedReality
 *
 * @author Achyut Sinha
 */

require_once CONTROLLER_PATH . DS . 'App.php';
require_once MODEL_PATH . DS . 'WebcomponentModel.php';

class Webcomponent extends AppController {

    //put your code here
    public function __construct() {
        parent::__construct();
    }

    public function uploadComponent() {
        $view = new View();
        $data = $this->request['POST'];
        $files = $_FILES;
        try {
            $WebcomponentModel = new WebcomponentModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Component saved succesfully'),
                        'data' => $WebcomponentModel->uploadComponent($data,$files)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

    public function mapComponentWithBU() {
        $view = new View();
        $data = $this->request['POST'];
        try {
            $WebcomponentModel = new WebcomponentModel();
            return $view->json([
                        'error' => false,
                        'msg' => __t('Component mapped succesfully'),
                        'data' => $WebcomponentModel->mapComponentWithBU($data)
            ]);
        } catch (Exception $exc) {
            return $view->json([
                        'data' => [],
                        'msg' => showException($exc->getMessage()) . (APP_DEBUG ? ":" . $exc->getFile() . ":" . $exc->getLine() : ''),
                        'error' => true
            ]);
        }
    }

}
