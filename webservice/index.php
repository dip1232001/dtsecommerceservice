<?php
try {
    require_once 'libs/includes/config.php';
} catch (\Exception $error) {
    $errorMsg = $error->getMessage();
    include_once('libs/components/Views/View.php');
    $viewObj = new View();
    include_once (__DIR__ . '/view/' . 'error.php');
}
