<?php

include_once COMPONENT_PATH . DS . 'Cache' . DS . CACHE_DRIVER . DS . 'Driver.php';

Class Cache extends Driver {

    public function __construct() {
        parent::__construct();
    }

    public function set($name, $value, $expire = '3 hours') {
        if (CACHE_ENABLED == false) {
            return false;
        }
        if (is_array($value) && isset($value['ApiUrlCalled'])) {
            unset($value['ApiUrlCalled']);
        }
		if(!empty($value)){
          parent::set(COMPANY_ID.$name, $value, $expire);
		}
    }

    public function get($name) {
        if (CACHE_ENABLED == false) {
            parent::delete(COMPANY_ID.$name);
            return false;
        }
        return parent::get(COMPANY_ID.$name);
    }

}
