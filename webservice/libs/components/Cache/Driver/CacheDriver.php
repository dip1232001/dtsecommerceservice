<?php

abstract class CacheDriver {

    abstract public function set($name, $value);

    abstract public function get($name);

    abstract public function delete($name);
}
