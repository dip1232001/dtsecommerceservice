<?php

require_once COMPONENT_PATH . DS . 'Cache' . DS . 'Driver' . DS . 'CacheDriver.php';
require_once COMPONENT_PATH . DS . 'Cache' . DS . 'Redis' . DS . 'Predis' . DS . 'autoload.php';

Class Driver extends CacheDriver {

    private $redis = null;

    public function __construct() {
        if ($this->redis == null) {
            Predis\Autoloader::register();
            $this->redis = new Predis\Client([
                'scheme' => 'tcp',
                'host' => REDIS_HOST,
                'port' => REDIS_PORT,
                'password' => REDIS_PASS,
                'database' => 2,
                'alias' => 'COMPANY_' . COMPANY_ID,
            ]);
        }
    }

    public function set($name, $value, $expire = '3 hours') {
        if (!empty($_SERVER['HTTP_HOST'])) {
            $name = md5($name . $_SERVER['HTTP_HOST']);
        }
        $timeToset = abs(time() - strtotime('+ ' . $expire)); //round(abs(time() - strtotime('+ ' . $expire)) / 60, 2);
        $this->redis->set($name, serialize($value));
        $this->redis->expire($name, $timeToset);
    }

    public function delete($name) {
        if (!empty($_SERVER['HTTP_HOST'])) {
            $name = md5($name . $_SERVER['HTTP_HOST']);
        }
        return $this->redis->del($name);
    }

    public function get($name) {
        if (!empty($_SERVER['HTTP_HOST'])) {
            $name = md5($name . $_SERVER['HTTP_HOST']);
        }
        if (!$this->redis->exists($name)) {
            return false;
        }
        return unserialize($this->redis->get($name));
    }

}
