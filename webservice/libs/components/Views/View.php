<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of View
 *
 * @author dipankar
 */
class View {

    //put your code here
    protected $theme = THEME_PATH;
    protected $Values = [];
    public $layout = THEME_PATH . DS . 'master.php';
    public $renderView = null;
    public $jsInArray = [];
    public $jsRawScript = [];
    public $cssRawScript = [];
    public $cssInArray = [];
    private $header = "Content-Type: text/html";

    public function __construct() {
        return $this;
    }

    public function set($name, $value) {
        $this->Values[$name] = $value;
    }

    public function setTheme($path) {
        $this->theme = $path;
    }

    public function get($name) {
        return isset($this->Values[$name]) ? $this->Values[$name] : null;
    }

    public function setJs($attributes = []) {
        if (is_array($attributes)) {
            if (count($this->jsInArray) > 0) {
                $this->jsInArray = array_merge($this->jsInArray, $attributes);
            } else {
                $this->jsInArray = $attributes;
            }
        } else {
            $this->jsInArray[$attributes] = ['type' => 'text/javascript'];
        }
    }

    public function setJsScript($script) {
        if (is_array($script)) {
            if (count($this->jsRawScript) > 0) {
                $this->jsRawScript = array_merge($this->jsRawScript, $script);
            } else {
                $this->jsRawScript = $script;
            }
        } else {
            $this->jsRawScript[] = $script;
        }
    }

    /**
     * 
     * @param type $script
     * @input <?php
      View::setCssScript([
      "body{ font-family:arial; }"
      ]);
      ?>
     */
    public function setCssScript($script) {
        if (is_array($script)) {
            if (count($this->cssRawScript) > 0) {
                $this->cssRawScript = array_merge($this->cssRawScript, $script);
            } else {
                $this->cssRawScript = $script;
            }
        } else {
            $this->cssRawScript[] = $script;
        }
    }

    /**
     * 
     * @param type $attributes
     * @Return 
     * View::setCss([
      '/assets/default/css/style.css' => ['rel' => 'stylesheet', 'type' => 'text/css', 'media' => 'all']
      ]);
     */
    public function setCss($attributes = []) {
        if (is_array($attributes)) {
            if (count($this->cssInArray) > 0) {
                $this->cssInArray = array_merge($this->cssInArray, $attributes);
            } else {
                $this->cssInArray = $attributes;
            }
        } else {
            $this->cssInArray[$attributes] = ['rel' => 'stylesheet', 'type' => 'text/css', 'media' => 'all'];
        }
    }

    /**
     * 
     * @return string
     * @Call <?php echo View::flushCss(); ?>
     */
    public function flushCss() {
        $cssLines = '';
        $cssRedisKey = 'cssdd_' . md5(json_encode($this->cssInArray));

        $cache = new Cache();
        if (($cssLines = $cache->get($cssRedisKey)) == false) {
            if (count($this->cssInArray) > 0) {
                foreach ($this->cssInArray as $key => $cssValue) {
                    if (!is_numeric($cssValue)) {
                        $cssLink = 'href="' . $key . '" ';
                    }
                    if (is_array($cssValue)) {
                        if (count($cssValue) > 0) {
                            foreach ($cssValue as $key => $attr) {
                                $cssLink .= trim($key) . '="' . trim($attr) . '" ';
                            }
                        }
                    } else {
                        $cssLink = 'href="' . $cssValue . '" rel="stylesheet" media="all" ';
                    }
                    $cssLines .= '<link ' . $cssLink . '/>';
                }
            }

            $cache->set($cssRedisKey, $cssLines, '7 days');
        }
        if (!empty($this->cssRawScript) && is_array($this->cssRawScript)) {
            $cssLines .= '<style type="text/css">' . (implode(' ', $this->cssRawScript)) . '</style>';
        } else if (!empty($this->cssRawScript) && !is_array($this->cssRawScript)) {
            $cssLines .= '<style type="text/css">' . $this->cssRawScript . '</style>';
        }

        return $cssLines;
    }

    /**
     * 
     * @return string
     * @call <?php echo View::flushJs(); ?>
     */
    public function flushJs() {
        $jsLines = '';
        $jsRedisKey = 'js_' . md5(json_encode($this->jsInArray));
        $cache = new Cache();
        if (($jsLines = $cache->get($jsRedisKey)) == false) {
            if (count($this->jsInArray) > 0) {
                foreach ($this->jsInArray as $key => $jsValue) {
                    if (!is_numeric($jsValue)) {
                        $jsLink = 'src="' . $key . '" ';
                    }
                    if (is_array($jsValue)) {
                        if (count($jsValue) > 0) {
                            foreach ($jsValue as $key => $attr) {
                                $jsLink .= trim($key) . '="' . trim($attr) . '" ';
                            }
                        }
                    } else {
                        $jsLink = 'src="' . $jsValue . '" type="text/javascript"';
                    }
                    $jsLines .= '<script ' . $jsLink . '></script>';
                }
            }

            $cache->set($jsRedisKey, $jsLines, '7 days');
        }
        if (!empty($this->jsRawScript) && is_array($this->jsRawScript)) {
            $jsLines .= '<script type="text/javascript">' . implode(" ", $this->jsRawScript) . '</script>';
        } else if (!empty($this->jsRawScript) && !is_array($this->jsRawScript)) {
            $jsLines .= '<script type="text/javascript">' . $this->jsRawScript . '</script>';
        }
        return $jsLines;
    }

    public function render($view = null) {
        $getClass = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 2)[1];

        if ($view != null) {
            $this->renderView = $this->theme . DS . rtrim($view, '.php') . '.php';
        } else {
            $this->renderView = $this->theme . DS . $getClass['class'] . DS . $getClass['function'] . '.php';
        }
        if (!is_file($this->renderView)) {
            throw new Exception(__t('View File Not Found : ' . $this->renderView));
        }
        header("Content-Type: text/html");

        include_once $this->layout;
        return $this;
    }

    public function loadView($view = null, $passParams = [], $includeonce = true) {
        if (!empty($passParams)) {
            $this->Values = array_merge($this->Values, $passParams);
        }
        if (!empty($this->Values)) {
            extract($this->Values);
        }
        if ($view == null) {
            if ($includeonce) {
                include_once $this->renderView;
            } else {
                include $this->renderView;
            }
        } else {
            if ($includeonce) {
                include_once $this->theme . DS . rtrim($view, '.php') . '.php';
            } else {
                include $this->theme . DS . rtrim($view, '.php') . '.php';
            }
        }
    }

    public function json($data) {
        if (is_array($data)) {
            $data = json_encode($data,JSON_NUMERIC_CHECK,JSON_UNESCAPED_UNICODE);
        }
        header('Content-Type: application/json');
        echo $data;
        exit();
    }

    public function Xml($data, $parentNode = 'data', $loopIndex = null) {
        if (is_array($data)) {
            $xml_data = new SimpleXMLElement("<?xml version=\"1.0\"?>$parentNode");
            array_to_xml($data, $xml_data, $loopIndex);
            header('Content-Type: application/xml');
            echo $xml_data->asXML();
            exit();
        } else {
            throw new Exception("No array passed");
        }
    }

    public function headerStatus($string = null) {
        if ($string != null) {
            $this->header = $string;
        }
        header($this->header);
        return $this;
    }

}
