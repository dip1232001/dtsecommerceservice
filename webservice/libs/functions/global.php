<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function dd($data, $exit = false)
{
    echo '<pre>';
    print_r($data);
    echo '</pre>';
    if ($exit) {
        exit();
    }
}

function slugify($text)
{
    $text = preg_replace('~[^\pL\d]+~u', '-', $text);
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
    $text = preg_replace('~[^-\w]+~', '', $text);
    $text = trim($text, '-');
    $text = preg_replace('~-+~', '-', $text);
    $text = strtolower($text);
    if (empty($text)) {
        return 'n-a';
    }
    return $text;
}

function setCookies($name, $value, $expire = null, $path = null, $domain = null, $secure = null)
{
    if (empty($expire)) {
        $expire = COOKIE_SET_TIME;
    }
    if (empty($path)) {
        $path = COOKIE_PATH;
    }
    if (empty($domain)) {
        $domain = COOKIE_DOMAIN;
    }
    if ($secure == null) {
        $secure = ($_SERVER['SERVER_PORT'] == 443) ? true : false;
    }
    if ($_SERVER['SERVER_PORT'] == 80) {
        $secure = false;
    }
    setcookie($name, $value, $expire, $path, $domain, $secure);
}

function callableClass($params = array())
{
    $classPath = CONTROLLER_PATH . DS . $params['controller'] . ".php";
    include_once $classPath;

    if (!class_exists($params['controller'])) {
        throw new Exception("Sorry class " . $params['controller'] . " does not exists !!!!!!");
    }
    $class = new $params['controller'];
    if (!method_exists($class, $params['action'])) {
        throw new Exception("Oops  Method " . $params['action'] . " does not exists in class " . $params['controller']);
    }
    if (!isset($params['params'])) {
        $parameter = null;
    } elseif (isset($params['params']) && !isset($params['params']['named'])) {
        $parameter = null;
    } else {
        $parameter = $params['params']['named'];
    }
    if (isset($params['params']) && isset($params['params']['query'])) {
        $class->request['GET'] = $params['params']['query'];
    }
    if (!empty($params['params']['request']) && isset($params['params']['request'])) {
        $class->request['POST'] = $params['params']['request'];
    }
    if (!empty($_FILES)) {
        $class->request['FILES'] = $_FILES;
    }
    if (is_array($parameter)) {
        $return = call_user_func_array(array($class, $params['action']), $parameter);
    } else {
        $return = $class->{$params['action']}($parameter);
    }

    $class = null;
    unset($class);
    return $return;
}

function _redirect($url, $array = array(), $topframe = false)
{
    if (!(session_id())) {
        session_start();
    }

    foreach ($array as $key => $value) {
        $_SESSION[$key] = $value;
    }
    if (empty($url)) {
        $url = $_SERVER['HTTP_REFERER'];
    }
    if (!$topframe) {
        header("Location:" . $url);
    } else {
        echo '<script type="text/javascript">';
        echo "top.window.location='" . $url . "';";
        echo '</script>';
    }
    exit();
}

function _flashMessage($name)
{
    if (isset($_SESSION[$name])) {
        echo $_SESSION[$name];
        unset($_SESSION[$name]);
    }
}

function GetBaseUrl()
{
    return sprintf(
            "%s://%s", isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http', $_SERVER['SERVER_NAME']
    );
}

function GetConnonicalUrl()
{
    return sprintf(
            "%s://%s", isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http', (rtrim($_SERVER['SERVER_NAME'], '/') . '/' . ((strpos($_SERVER["REQUEST_URI"], $_SESSION['lang']) === false) ? $_SESSION['lang'] . $_SERVER["REQUEST_URI"] : ltrim($_SERVER["REQUEST_URI"], '/') ))
    );
}

function __t($phrase)
{
    global $LANG;
    return isset($LANG[$phrase]) ? $LANG[$phrase] : $phrase;
}

function getCurrentUrl()
{
    return (isset($_SERVER['HTTPS']) ? "https" : "http") . "://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
}

function array_to_xml($data, &$xml_data, $indexLoopKey = null)
{
    foreach ($data as $key => $value) {
        if ($indexLoopKey == null && is_numeric($key)) {
            $key = $indexLoopKey . $key; //dealing with <0/>..<n/> issues
        } elseif ($indexLoopKey != null) {
            $key = $indexLoopKey;
        }
        if (is_array($value)) {
            $subnode = $xml_data->addChild($key);
            array_to_xml($value, $subnode);
        } else {
            $xml_data->addChild("$key", htmlspecialchars("$value"));
        }
    }
}

function cjConvertionPrice($Price)
{
    return number_format((($Price * 95) / 100), 2);
}

function file_upload_max_size()
{
    static $max_size = -1;

    if ($max_size < 0) {
        // Start with post_max_size.
        $max_size = parse_size(ini_get('post_max_size'));

        // If upload_max_size is less, then reduce. Except if upload_max_size is
        // zero, which indicates no limit.
        $upload_max = parse_size(ini_get('upload_max_filesize'));
        if ($upload_max > 0 && $upload_max < $max_size) {
            $max_size = $upload_max;
        }
    }
    return $max_size;
}

function parse_size($size)
{
    $unit = preg_replace('/[^bkmgtpezy]/i', '', $size); // Remove the non-unit characters from the size.
    $size = preg_replace('/[^0-9\.]/', '', $size); // Remove the non-numeric characters from the size.
    if ($unit) {
        // Find the position of the unit in the ordered string which is the power of magnitude to multiply a kilobyte by.
        return round($size * pow(1024, stripos('bkmgtpezy', $unit[0])));
    } else {
        return round($size);
    }
}

function getExtension($str)
{
    $explode = explode(".", $str);
    return end($explode);
}

function array_search_find_key($array, $field, $value)
{
    foreach ($array as $key => $arr) {
        if ($arr[$field] === $value) {
            return $key;
        }
    }
    return false;
}

function showException($message)
{
    if (APP_DEBUG) {
        return $message;
    }
    return preg_replace("/SQLSTATE\[(.*?)\]: \[Microsoft\]\[(.*?)\]\[(.*?)\]/i", "", $message);
}

function makeSqlINString($array)
{
        return "'{$array}'";
}

function greater_than($option1, $option2)
{
    return ($option1 > $option2);
}


function greater_than_equals($option1, $option2)
{
    return ($option1 >= $option2);
}

function equals($option1, $option2)
{
    return ($option1 == $option2);
}

function less_than_equals($option1, $option2)
{
    return ($option1 <= $option2);
}

function less_than($option1, $option2)
{
    return ($option1 < $option2);
}

function getAuthToken() {

    $session_auth = !isset($_SESSION['ACCESS_TOKEN']) ? null : $_SESSION['ACCESS_TOKEN'];

    //if it does, check if it has expired (this may also include this request being in the EXPIRE_BUFFER time)
    if (null != $session_auth) {
        //Session auth is an array. Get the expire time, and check it.
        //get current timestamp
        $curr_time = time();

        //compare the session auth expire
        if ($session_auth['expire_time'] > $curr_time || empty($session_auth['access_token'])) {
            //set the class auth token variable
            $session_auth = null;
        }
    }


    //No token or expired token means we need a new one
    if (null == $session_auth) {
        //build the params for the call
        $url = API_URL_ENDPOINT . "/webapi/Authenticate/login";

        //curl headers
        $headers = array(
            "authorization: Bearer apiuser:wXPocq0fG24=",
            "cache-control: no-cache",
            "content-type: application/x-www-form-urlencoded"
        );

        //request body (make an array, then convert it to a query string)
        $body_arr = array(
        );

        $body_str = http_build_query($body_arr);

        //Set the CURL Options
        $curl_opts = array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_URL => $url,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $body_str,
            CURLOPT_CONNECTTIMEOUT => 10,
            CURLOPT_TIMEOUT => 20,
            CURLOPT_HEADER => 1,
            CURLOPT_VERBOSE => 1
        );

        //Set up the CURL request
        $resCurl = curl_init();
        curl_setopt_array($resCurl, $curl_opts);

        // Send the request & save response to $resp
        $response = curl_exec($resCurl);
        $info = curl_getinfo($resCurl, CURLINFO_HEADER_SIZE);
        $err_no = curl_errno($resCurl);
        curl_close($resCurl);
        $res = get_headers_from_curl_response($response);
        //If curl returned an error, abort, otherwise, save the token
        if (0 != $err_no) {
            throw new Exception('Error Retrieving API Auth Token. Curl Err:' . $err_no);
        } else {
            if (isset($res['error'])) {
                throw new Exception('Error Retrieving API Auth Token. API err:' . $res['error'] . " - " . $res['error_description']);
            }

            //calculate the expire time (now + expires in - buffer)
            $expire_time = time() + (empty($res['ExpireTime']) ? (3 * 60 * 60) : $res['ExpireTime']) - (5 * 60);

            //store it in the session
            $session_auth = array(
                "access_token" => $res['Token'],
                "expire_time" => $expire_time
            );
            $_SESSION['ACCESS_TOKEN'] = $session_auth;
        }//end if/esle err
    }//end if null auth_token
}

function callApi($function_url, $call_type = "GET", $request_params = array(), array $url_params = array(), $return_raw = false, $debug = false, $authRequired = true) {
    if ($authRequired) {
        getAuthToken();
    }
    //may change to an array (result, error, other, etc)
    $response = "";
    //All call must include the apiaccount in the url, so we need to add it to the passed url params
    //then convert it all to a query string

    //dd($_SESSION['ACCESS_TOKEN']);

    $url_query = http_build_query($url_params);

    //Complete the url
    $call_url = $function_url . @(empty($url_query) ? "" : "?" . $url_query);
    //Build the body string (json encode the request params
    $body_str = http_build_query($request_params);
	
    $headers = array(
        'Content-type:application/x-www-form-urlencoded; charset=utf-8'
    );
	
    if ($authRequired) {
        $headers[] = 'Token:' . $_SESSION['ACCESS_TOKEN']['access_token'];
    }
    //Set the CURL Options
    $curl_opts = array(
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_HTTPHEADER => $headers,
        CURLOPT_URL => $call_url,
        CURLOPT_CUSTOMREQUEST => $call_type,
        CURLOPT_CONNECTTIMEOUT => 3,
        CURLOPT_TIMEOUT => 500,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1
    );



    //If the type is PUT or POST, we need to add the body str
    if ("PUT" == $call_type || "POST" == $call_type || "DELETE" == $call_type) {
        $curl_opts[CURLOPT_POSTFIELDS] = $body_str;
    }

    //Make the call
    //Set up the CURL request
    $apiCurl = curl_init();
    curl_setopt_array($apiCurl, $curl_opts);

    //TEMP FIX TO STOP TIMING OUT
    curl_setopt($apiCurl, CURLOPT_CONNECTTIMEOUT, 0);
    curl_setopt($apiCurl, CURLOPT_TIMEOUT, 0);
    curl_setopt($apiCurl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($apiCurl, CURLOPT_SSL_VERIFYPEER, false);
    // Send the request & save response to $resp
    $res = curl_exec($apiCurl);
    $info = curl_getinfo($apiCurl);
    $err_no = curl_errno($apiCurl);
    $httpcode = curl_getinfo($apiCurl, CURLINFO_HTTP_CODE);
    curl_close($apiCurl);
    if ($debug) {
        var_dump($curl_opts, $res, $info, $err_no);
        print $body_str;
        die();
    }

    //If curl returned an error, abort, otherwise, save the token
    if (0 != $err_no) {
        throw new Exception('Error In API Call "' . $call_url . '" - Curl Err:' . $err_no);
    } else {
        $response = ($return_raw) ? $res : json_decode($res, true);
        if ($httpcode !== 200) {
            $response = (is_null($response) || empty($response)) ? $httpcode : $response;
        }
    }

    //debug requests by writing to tail of log in /tmp/tmt-api-log.txt
    /* error_log ('URL : \n', 3, '/tmp/tmt-api-log.txt');
      error_log ($call_url."\n\n", 3, '/tmp/tmt-api-log.txt');
      error_log ('headers : \n', 3, '/tmp/tmt-api-log.txt');
      error_log (json_encode($headers)."\n\n", 3, '/tmp/tmt-api-log.txt');
      error_log ('body : \n', 3, '/tmp/tmt-api-log.txt');
      error_log ($body_str."\n\n", 3, '/tmp/tmt-api-log.txt');
      error_log ("http code :\n", 3, '/tmp/tmt-api-log.txt');
      error_log ($httpcode."\n\n", 3, '/tmp/tmt-api-log.txt');
      error_log ("response :\n", 3, '/tmp/tmt-api-log.txt');
      error_log ($res."\n\n", 3, '/tmp/tmt-api-log.txt'); */

    return $response;
}
function get_headers_from_curl_response($response) {
    $headers = array();

    $header_text = substr($response, 0, strpos($response, "\r\n\r\n"));

    foreach (explode("\r\n", $header_text) as $i => $line) {
        if ($i === 0) {
            $headers['http_code'] = $line;
        } else {
            list ($key, $value) = explode(': ', $line);

            $headers[$key] = $value;
        }
    }

    return $headers;
}