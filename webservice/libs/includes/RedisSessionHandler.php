<?php

if (!class_exists('\Predis\Client')) {
// require_once('modules/predis/lib/Predis/Autoloader.php');
    require_once COMPONENT_PATH . DS . 'Cache' . DS . 'Redis' . DS . 'Predis' . DS . 'autoload.php';
    Predis\Autoloader::register();
}

class RedisSessionHandler implements SessionHandlerInterface {

    private $redis;
    private $redisSessionPrefix = "SESSION::REDIS::";

    public function __construct($conf) {
        $this->redis = new \Predis\Client($conf);
    }

    public function open($path, $name) {
        return true;
    }

    public function close() {
        return true;
    }

    public function read($id) {
        return ($this->redis->get($this->redisSessionPrefix . $id));
    }

    public function write($id, $data) {
        return $this->redis->setEx($this->redisSessionPrefix . $id, SESSION_EXPIRE_TIME, ($data));
    }

    public function destroy($id) {
        $this->redis->del($this->redisSessionPrefix . $id);
        return true;
    }

    public function gc($maxlifetime) {

        return true;
    }

}
