<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * Tmt Configurations
 */



if (!defined('ADMIN_API_USER')) {
    define('ADMIN_API_USER', $ENV['ADMIN_API_USER']);
}
if (!defined('ADMIN_API_PASS')) {
    define('ADMIN_API_PASS', $ENV['ADMIN_API_PASS']);
}
if (!defined('API_USER')) {
    define('API_USER', $ENV['API_USER']);
}
if (!defined('API_PASSWORD')) {
    define('API_PASSWORD', $ENV['API_PASS']);
}
if (!defined('COMPANY_ID')) {
    define('COMPANY_ID', $ENV['API_COMPANY']);
}
if (!defined('CURRENCY')) {
    define('CURRENCY', $ENV['CURRENCY']);
}
if (!defined('LOGIN_REQUIRED')) {
    define('LOGIN_REQUIRED', $ENV['LOGIN_REQUIRED']);
}
if (!defined('IS_REWARD_SITE')) {
    define('IS_REWARD_SITE', $ENV['IS_REWARD_SITE']);
}