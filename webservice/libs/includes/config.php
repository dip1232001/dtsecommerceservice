<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With");
header('P3P: CP="CAO PSA OUR"');

require_once dirname(dirname(dirname(__FILE__))) . DIRECTORY_SEPARATOR . '.env';

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/*
 *  Session Set handlerer
 */
if ($ENV['APP_DEBUG'] == true) {
    set_error_handler("CustomErrorHandler", E_ALL & ~E_NOTICE & ~E_WARNING);
    error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING);
    ini_set('display_errors', 'on');
} else {
   // set_error_handler("CustomErrorHandler", 0);
    error_reporting(0);
    ini_set('display_errors', 'off');
}

function CustomErrorHandler($errno, $errstr, $errfile, $errline, $errcontext) {
    $errorMsg = $errstr . ' In ' . $errfile . ' Line number: ' . $errline;
    include(VIEW_PATH . DS . 'error.php');
}

/*
 * Path Settings
 */
if (!defined('WWW_ROOT')) {
    define('WWW_ROOT', dirname(dirname(dirname(__FILE__))));
}
if (!defined('DS')) {
    define('DS', DIRECTORY_SEPARATOR);
}
if (!defined('WWW_ASSETS')) {
    define('WWW_ASSETS', WWW_ROOT . DS . 'assets');
}
if (!defined('ROUTE_PATH')) {
    define('ROUTE_PATH', WWW_ROOT . DS . 'libs' . DS . 'routes');
}
if (!defined('MODEL_PATH')) {
    define('MODEL_PATH', WWW_ROOT . DS . 'models');
}
if (!defined('FUNCTION_PATH')) {
    define('FUNCTION_PATH', WWW_ROOT . DS . 'libs' . DS . 'functions');
}
if (!defined('CONTROLLER_PATH')) {
    define('CONTROLLER_PATH', WWW_ROOT . DS . 'controllers');
}
if (!defined('VIEW_PATH')) {
    define('VIEW_PATH', WWW_ROOT . DS . 'view');
}
if (!defined('LANGUAGE_PATH')) {
    define('LANGUAGE_PATH', WWW_ROOT . DS . 'libs' . DS . 'language');
}
if (!defined('COMPONENT_PATH')) {
    define('COMPONENT_PATH', WWW_ROOT . DS . 'libs' . DS . 'components');
}
if (!defined('VENDOR_PATH')) {
    define('VENDOR_PATH', WWW_ROOT . DS . 'vendor');
}

/*
 * Cache Enabled
 */
if (!defined('CACHE_ENABLED')) {
    define('CACHE_ENABLED', $ENV['CACHE_ENABLED']);
}
if (!defined('CACHE_DRIVER')) {
    define('CACHE_DRIVER', $ENV['CACHE_DRIVER']);
}
if (!defined('GEOIP_USERID')) {
    define('GEOIP_USERID', $ENV['GEOIP_USERID']);
}
if (!defined('GEOIP_LICENCE')) {
    define('GEOIP_LICENCE', $ENV['GEOIP_LICENCE']);
}
require_once VENDOR_PATH . DS . 'autoload.php';
/*
 * To change theme work from here
 */
$detectMobile = new Mobile_Detect;
if (!defined('THEME_PATH')) {
    if ($detectMobile->isMobile() && !$detectMobile->isTablet()) {
        define('THEME_PATH', VIEW_PATH . DS . 'default' . DS . 'mobile');
    } else {
        define('THEME_PATH', VIEW_PATH . DS . 'default');
    }
}
if (!defined('THEME_LANG_PATH')) {
    define('THEME_LANG_PATH', VIEW_PATH . DS . 'default' . DS . 'lang');
}
if (!defined('API_URL_ENDPOINT')) {
    define('API_URL_ENDPOINT', $ENV['API_ENDPOINT']);
}

if (!defined('OFFLINE_PUSHER_API_ENDPOINT')) {
    define('OFFLINE_PUSHER_API_ENDPOINT', $ENV['OFFLINE_PUSHER_API_ENDPOINT']);
}

if (!defined('PUSHERKEY')) {
    define('PUSHERKEY', $ENV['PUSHERKEY']);
}

if (!defined('PUSHERSECRET')) {
    define('PUSHERSECRET', $ENV['PUSHERSECRET']);
}

if (!defined('PUSHERAPPID')) {
    define('PUSHERAPPID', $ENV['PUSHERAPPID']);
}



require_once FUNCTION_PATH . DS . 'global.php';

require_once COMPONENT_PATH . DS . 'Cache' . DS . 'Cache.php';
require_once WWW_ROOT . DS . 'libs' . DS . 'includes' . DS . 'settings.php';
require_once WWW_ROOT . DS . 'libs' . DS . 'includes' . DS . 'session.php';
require_once WWW_ROOT . DS . 'libs' . DS . 'includes' . DS . 'TmtApiSettings.php';
require_once LANGUAGE_PATH . DS . 'base.php';
require_once ROUTE_PATH . DS . 'route.php';
