<?php


session_name(SESSION_NAME);
if ($_SERVER['SERVER_PORT'] == 443) {
    session_set_cookie_params(SESSION_EXPIRE_TIME, '/', COOKIE_DOMAIN, true);
} else {
    session_set_cookie_params(SESSION_EXPIRE_TIME, '/', COOKIE_DOMAIN);
}

function session_shut_down() {
    return true;
}

if ($ENV['SESSION_DRIVER'] == 'Redis') {
	require_once 'RedisSessionHandler.php';
    try {
        $handler = new RedisSessionHandler([
            'scheme' => 'tcp',
            'host' => REDIS_HOST,
            'port' => REDIS_PORT,
            'password' => REDIS_PASS,
            'database' => (!defined('REDIS_SESSION_DB') ? 1 : REDIS_SESSION_DB)
        ]);
        register_shutdown_function('session_shut_down');
        session_set_save_handler($handler, true);
    } catch (Exception $exc) {
        echo $exc->getMessage();
    }
}

session_start();


if (!isset($_SESSION['lang']) && empty($_SESSION['lang'])) {
    $_SESSION['lang'] = 'en';
}

