<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * Cookie Setting
 */


/**
 * Redis Settings
 */
if (!defined('REDIS_HOST')) {
    define('REDIS_HOST', $ENV['REDIS_HOST']);
}
if (!defined('REDIS_PORT')) {
    define('REDIS_PORT', $ENV['REDIS_PORT']);
}
if (!defined('REDIS_PASS')) {
    define('REDIS_PASS', $ENV['REDIS_PASS']);
}
if (!defined('API_ACCESS_KEY')) {
    define('API_ACCESS_KEY', $ENV['API_ACCESS_KEY']);
}


if (!defined('COOKIE_SET_TIME')) {
    define('COOKIE_SET_TIME', time() + (3600 * (empty($ENV['SESSION_TIMEOUT']) ? 24 : $ENV['SESSION_TIMEOUT'])));
}
if (!defined('COOKIE_PATH')) {
    define('COOKIE_PATH', '/');
}
if (!defined('COOKIE_DOMAIN')) {
    define('COOKIE_DOMAIN', $_SERVER['HTTP_HOST']);
}
/*
 * Session Settings
 */
if (!defined('SESSION_NAME')) {
    define('SESSION_NAME', $ENV['SESSION_NAME']);
}
if (!defined('SESSION_EXPIRE_TIME')) {
    define('SESSION_EXPIRE_TIME', ((empty($ENV['SESSION_TIMEOUT']) ? 24 : $ENV['SESSION_TIMEOUT']) * 3600));
}
if (!defined('TIMEZONE')) {
    define('TIMEZONE', $ENV['TIMEZONE']);
}


if (!defined('LOGIN_REQUIRED')) {
    define('LOGIN_REQUIRED', $ENV['LOGIN_REQUIRED']);
}
if (!defined('IS_REWARD_SITE')) {
    define('IS_REWARD_SITE', $ENV['IS_REWARD_SITE']);
}


/*
 * Other Settings
 */


if (!defined('APP_DEBUG')) {
    define('APP_DEBUG', $ENV['APP_DEBUG']);
}
if (!defined('VERSION')) {
    define('VERSION', $ENV['VERSION']);
}
if (!defined('FACEBOOK_ID')) {
    define('FACEBOOK_ID', $ENV['FACEBOOK_ID']);
}
if (!defined('FACEBOOK_SECRET')) {
    define('FACEBOOK_SECRET', $ENV['FACEBOOK_SECRET']);
}


if (!defined('GOOGLE_ID')) {
    define('GOOGLE_ID', $ENV['GOOGLE_ID']);
}
if (!defined('GOOGLE_SECRET')) {
    define('GOOGLE_SECRET', $ENV['GOOGLE_SECRET']);
}
if (!defined('LINK_COLOR')) {
    define('LINK_COLOR', $ENV['LINK_COLOR']);
}
if (!defined('BG_COLOR')) {
    define('BG_COLOR', $ENV['BG_COLOR']);
}


/*
 * DB settings
 */

if (!defined('DB_HOST')) {
    define('DB_HOST', $ENV['DB_HOST']);
}
if (!defined('DB_NAME')) {
    define('DB_NAME', $ENV['DB_NAME']);
}
if (!defined('DB_PASS')) {
    define('DB_PASS', $ENV['DB_PASS']);
}
if (!defined('DB_USER')) {
    define('DB_USER', $ENV['DB_USER']);
}
if (!defined('DB_PORT')) {
    define('DB_PORT', $ENV['DB_PORT']);
}
if (!defined('AWS_ACCESS_KEY')) {
    define('AWS_ACCESS_KEY', $ENV['AWS_ACCESS_KEY']);
}
if (!defined('AWS_SECRET_KEY')) {
    define('AWS_SECRET_KEY', $ENV['AWS_SECRET_KEY']);
}
if (!defined('AWS_BUCKET_NAME')) {
    define('AWS_BUCKET_NAME', $ENV['AWS_BUCKET_NAME']);
}
if (!defined('AWS_REGION')) {
    define('AWS_REGION', $ENV['AWS_REGION']);
}

if (!defined('FILEUPLOADPATH')) {
    define('FILEUPLOADPATH', $ENV['FILEUPLOADPATH']);
}

if (!defined('PAYMENTMODE')) {
    define('PAYMENTMODE', $ENV['PAYMENTMODE']);
}


date_default_timezone_set(TIMEZONE);
include_once VIEW_PATH . DS . 'lang' . DS . 'en.php';
