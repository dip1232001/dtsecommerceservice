<?php
$Routerlanguages = array(
    'da' => 'Dansk',    
    'de' => 'Deutsch',
    'en' => 'English',
    'es' => 'Español',
    'fr' => 'Français',
    'it' => 'Italiano',
    'hu' => 'Magyar',
    'nl' => 'Nederlands',
    'nb' => 'Norsk',
    'pl' => 'Polski',
    'pt' => 'Português',
    'sv' => 'Svenska',
    'ru' => 'русский',
    'ko' => '한국어',
    'zh' => '中文',
    'ja' => '日本語'
);
?>