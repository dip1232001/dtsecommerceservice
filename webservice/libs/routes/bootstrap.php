<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * Check For Language Append
 */
$Cache = New Cache();
$URI = rtrim(urldecode(preg_replace('/(\/+)/', '/', strip_tags(strtok(str_replace(dirname($_SERVER['PHP_SELF']), '', $_SERVER['REQUEST_URI']), "?")))), '/');
if(empty($URI)){
    $URI='/';
}
//$URI = (isset($_GET['url']) && !empty($_GET['url'])) ? basename($_GET['url']) : '/';
$functionToCall = isset($routes[$URI]) ? $routes[$URI] : null;

if (empty($functionToCall)) {
    $explodeUri = explode(DS, $URI);
    if (isset($Routerlanguages[strtolower($explodeUri[1])])) {
        setCookies('lang', strtolower($explodeUri[1]));
        $_SESSION['lang'] = strtolower($explodeUri[1]);
        $filename = VIEW_PATH . DS . 'lang' . DS . ((isset($_SESSION['lang']) ? $_SESSION['lang'] : 'en')) . '.php';
        if (is_file($filename)) {
            include_once $filename;
        }

        unset($explodeUri[1]);
        if (count($explodeUri) == 1 && isset($explodeUri[0]) && empty($explodeUri[0])) {
            $explodeUri[0] = '/';
        }
    }
    $implodeUrl = implode(DS, $explodeUri);

    if (($ConstructedUrl = $Cache->get($implodeUrl)) == false) {
        $ConstructedUrl = isset($routes[$implodeUrl]) ? $routes[$implodeUrl] : null;
        if (empty($ConstructedUrl)) {
            foreach ($routes as $mainRoutePattern => $value) {
                if (isset($value['params'])) {
                    $countmainRouter = explode("/", $mainRoutePattern);
                    if (count($explodeUri) != count($countmainRouter)) {
                        continue;
                    }
                    $regexPatterns = '/' . str_replace("/", "\/", ltrim(str_replace(array_keys($value['params']), $value['params'], $mainRoutePattern), '/'));
                    $pregGreparray = preg_match($regexPatterns . '$/i', $implodeUrl);
                    if ($pregGreparray == 1) {
                        //$parameterstoSend = array_keys($value['params']);
                        $g = 0;
                        $stripurl = explode(DS, ltrim($mainRoutePattern, DS));
                        $explurl = explode(DS, ltrim($implodeUrl, DS));
                        foreach ($stripurl as $param) {
                            if (isset($value['params'][$param])) {
                                $value['params']['named'][$param] = trim(strip_tags($explurl[$g]));
                            }
                            $g++;
                        }
                        // parse_str($_SERVER['QUERY_STRING'], $value['params']['query']);
                        // $value['params']['request'] = $_POST;
                        $ConstructedUrl = $value;
                        break;
                    } else {
                        $pregGreparray = preg_match($regexPatterns . "\/$/i", $implodeUrl);
                        if ($pregGreparray == 1) {
                            //$parameterstoSend = array_keys($value['params']);
                            $g = 0;
                            $stripurl = explode(DS, ltrim($mainRoutePattern, DS));
                            $explurl = explode(DS, ltrim($implodeUrl, DS));
                            foreach ($stripurl as $param) {
                                if (isset($value['params'][$param])) {
                                    $value['params']['named'][$param] = trim(strip_tags($explurl[$g]));
                                }
                                $g++;
                            }
                            // parse_str($_SERVER['QUERY_STRING'], $value['params']['query']);
                            // $value['params']['request'] = $_POST;
                            $ConstructedUrl = $value;

                            break;
                        }
                    }
                }
            }
        }
        if (empty($ConstructedUrl)) {
            $urlExp = explode('/', ltrim($implodeUrl, '/'));
            if (isset($urlExp[0]) && !empty($urlExp[0])) {
                $controller = $urlExp[0];
                if (!isset($urlExp[1]) && empty($urlExp[1])) {
                    $method = 'index';
                } else {
                    $method = $urlExp[1];
                }
                $ConstructedUrl = [
                    'controller' => ucfirst($controller),
                    'action' => $method
                ];
            }
            //  $ConstructedUrl = $implodeUr
        }
        if (!empty($ConstructedUrl)) {
            $Cache->set($implodeUrl, $ConstructedUrl, '30 days');
        }
    }

    if (!empty($ConstructedUrl)) {
        parse_str($_SERVER['QUERY_STRING'], $ConstructedUrl['params']['query']);
        $ConstructedUrl['params']['request'] = $_POST;
        $request_body = json_decode(file_get_contents('php://input'), true);
        if (!empty($request_body)) {
            $ConstructedUrl['params']['request'] = array_merge($ConstructedUrl['params']['request'], $request_body);
        }
    }
    $functionToCall = $ConstructedUrl;
    if (empty($functionToCall)) {
        $functionToCall = $routes['/404'];
    }
} else {
    parse_str($_SERVER['QUERY_STRING'], $_GET);
    $functionToCall['params']['request'] = $_POST;
    $request_body = json_decode(file_get_contents('php://input'), true);
    if (!empty($request_body)) {
        $functionToCall['params']['request'] = array_merge($functionToCall['params']['request'], $request_body);
    }
}
callableClass($functionToCall);

