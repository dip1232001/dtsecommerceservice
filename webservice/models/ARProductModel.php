<?php

use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProductModel
 *
 * @author dipankar
 */
require_once MODEL_PATH . DS . 'App.php';
require_once MODEL_PATH . DS . 'CategoryModel.php';
require_once MODEL_PATH . DS . 'PriceModel.php';
require_once MODEL_PATH . DS . 'ProductStockModel.php';
require_once MODEL_PATH . DS . 'VarientModel.php';
require_once MODEL_PATH . DS . 'ProductImageModel.php';

class ARProductModel extends AppModel {

    //put your code here
    public $Table = 'Products';

    public function __construct($callAuth = false) {
        parent::__construct($callAuth);
    }    

    public function getAllProductsByARTag($start = 0, $limit = 10, $searchparams = []) {
        if($searchparams['BUID']=="" || $searchparams['BUID']==null || $searchparams['BUID']==0){
            throw new Exception(__t("Enter BUID"));
        }
        if($searchparams['ARTag']==""){
            throw new Exception(__t("Enter AR Tag Name"));
        }



        /*******************************/
        $LINK = '';
        $url = API_URL_ENDPOINT . "/webapi/Authenticate/login";

        //curl headers
        $headers = array(
            "authorization: Bearer apiuser:wXPocq0fG24=",
            "cache-control: no-cache",
            "content-type: application/x-www-form-urlencoded"
        );

        //request body (make an array, then convert it to a query string)
        $body_arr = array();

        $body_str = http_build_query($body_arr);

        //Set the CURL Options
        $curl_opts = array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_URL => $url,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $body_str,
            CURLOPT_CONNECTTIMEOUT => 10,
            CURLOPT_TIMEOUT => 20,
            CURLOPT_HEADER => 1,
            CURLOPT_VERBOSE => 1
        );

        //Set up the CURL request
        $resCurl = curl_init();
        curl_setopt_array($resCurl, $curl_opts);

        // Send the request & save response to $resp
        $response = curl_exec($resCurl);
        $info = curl_getinfo($resCurl, CURLINFO_HEADER_SIZE);
        $err_no = curl_errno($resCurl);
        curl_close($resCurl);
        $res = get_headers_from_curl_response($response);

        if (0 != $err_no) {
            throw new Exception('Error Retrieving API Auth Token. Curl Err:' . $err_no);
        } else {
            if (isset($res['error'])) {
                throw new Exception('Error Retrieving API Auth Token. API err:' . $res['error'] . " - " . $res['error_description']);
            }
            $token = $res['Token'];

            //dd($token, true);

            $url = API_URL_ENDPOINT."/webapi/BusinessUnit/GetECommerceComponentInfo";
        
            $headers = array(
                "Token: ".$token,
                "cache-control: no-cache",
                "content-type: application/x-www-form-urlencoded"
            );
            //echo $this->BUID; exit;
            $body_arr = array("buid"=>$searchparams ['BUID']);
            $body_str = http_build_query($body_arr);
            $curl_opts = array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_HTTPHEADER => $headers,
                CURLOPT_URL => $url,
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => $body_str,
                CURLOPT_CONNECTTIMEOUT => 10,
                CURLOPT_TIMEOUT => 20,
                CURLOPT_HEADER => 0,
                CURLOPT_VERBOSE => 1
            );

            $resCurl = curl_init();
            curl_setopt_array($resCurl, $curl_opts);
            $response = curl_exec($resCurl);
            $info = curl_getinfo($resCurl, CURLINFO_HEADER_SIZE);
            $err_no = curl_errno($resCurl);
            curl_close($resCurl);
            //dd($response, true);
            if (0 != $err_no) {
                throw new Exception('Error Retrieving GetECommerceComponentInfo API. Curl Err:' . $err_no);
            } else {
                $temARR = json_decode($response);
                $TID=0;
                //dd($temARR, true);
                foreach ($temARR->Templates as $val){
                    if($val->TemplateType == 'Product_Details'){
                        $TID = $val->PKID;
                    }
                }

                //$LINK = "/template/".$TID."-Template?product_id=".$data['product_id'].'&category_id='.$data['category_id'];
                $LINK = "/template/".$TID."-Template";
            }

        }
        /****************************/

        $stateMent = $this->PDO->prepare("{CALL PHP_Ecommerce_GetproductsByTag(@buid = :buid, @Start = :Start, @Limit = :Limit, @Likename = :LikeName, @INDUSTRYID = :Industry_ID, @CATAGORYID = :Category_id, @SHOWALL = :ShowAll, @PRICEOVER = :PriceOver, @ISFEATURED = :IsFeatured, @ARTag=:ARTag)}");
        $stateMent->bindParam(':buid', $searchparams ['BUID'], PDO::PARAM_INT);
        $stateMent->bindParam(':Start', $start, PDO::PARAM_INT);
        $stateMent->bindParam(':Limit', $limit, PDO::PARAM_INT);

        if (!empty($searchparams['name'])) {
            $stateMent->bindParam(':LikeName', $searchparams ['name'], PDO::PARAM_STR);
        } else {
            $stateMent->bindParam(':LikeName', $searchparams['name'] = null, PDO::PARAM_INT);
        }
        if (!empty($searchparams['industry_id'])) {
            $stateMent->bindParam(':Industry_ID', $searchparams['industry_id'], PDO::PARAM_INT);
        } else {
            $stateMent->bindParam(':Industry_ID', $searchparams['industry_id'] = null, PDO::PARAM_INT);
        }
        if (!empty($searchparams['category_id'])) {
            $stateMent->bindParam(':Category_id', $searchparams['category_id'], PDO::PARAM_INT);
        } else {
            $stateMent->bindParam(':Category_id', $searchparams['category_id'] = null, PDO::PARAM_INT);
        }
        if (!empty($searchparams['showall'])) {
            $stateMent->bindParam(':ShowAll', $searchparams['showall'], PDO::PARAM_BOOL);
        } else {
            $stateMent->bindParam(':ShowAll', $searchparams['showall'] = null, PDO::PARAM_INT);
        }
        if (!empty($searchparams['priceover'])) {
            $stateMent->bindParam(':PriceOver', $searchparams['priceover'], PDO::PARAM_STR);
        } else {
            $stateMent->bindParam(':PriceOver', $searchparams['priceover'] = null, PDO::PARAM_INT);
        }
        if (!empty($searchparams['isfeatured'])) {
            $stateMent->bindParam(':IsFeatured', $searchparams['isfeatured'], PDO::PARAM_INT);
        } else {
            $stateMent->bindParam(':IsFeatured', $searchparams['isfeatured'] = null, PDO::PARAM_INT);
        }

        $stateMent->bindParam(':ARTag', $searchparams['ARTag'], PDO::PARAM_STR);

        $stateMent->execute();
        do {
            $rows = $stateMent->fetchAll(PDO::FETCH_ASSOC);

            if ($rows) {
                if (isset($rows[0]['TotalProducts'])) {
                    $data['Paginations'] = [
                        'Total' => $rows[0] ['TotalProducts'],
                        'Start' => $start,
                        'limit' => $limit,
                        'ProductLink' => $LINK
                    ];
                } else {
                    $prod = array();
                    foreach ($rows as $key=>$tag) {
                        $tag['description'] = strip_tags($tag['description']);
                        $prod[$key]=$tag;
                    }
                    $data['Products'] = $prod;
                }
            }
        } while ($stateMent->nextRowset());
        $stateMent->closeCursor();
        if (empty($data['Products'])) {
            $data['Products'] = [];
        }

        return $data;
    }

    public function getARTagsByBU($start, $limit, $buid) {
        if($buid==0){
            throw new Exception(__t("BUID 0"));
        }
        if($buid==""){
            throw new Exception(__t("Enter BUID"));
        }
        
        $statement = $this->PDO->prepare("{CALL PHP_AR_GetTagByBU(
            @BUID=:buid,
            @Start=:start,
            @Limit=:limit
        )}");

        $statement->bindParam(':buid', $buid, PDO::PARAM_INT);
        $statement->bindParam(':start', $start, PDO::PARAM_INT);
        $statement->bindParam(':limit', $limit, PDO::PARAM_INT);
        $statement->execute();
        $x = 0;        
        $data1 = [];

            do {            
                $rows = $statement->fetchAll(PDO::FETCH_ASSOC);                       
                switch ($x) {                
                    case 0:                    
                        $data1['Pagination'] = [                        
                                            'total' => (int) $rows[0]['totalPersons'],                        
                                            'start' => $start,                        
                                            'limit' => $limit                    
                                        ];                    
                        break;                
                    case 1:                    
                        $data1['Images'] = $rows;                    
                        break;            
                }            
                $x++;

            } while ($statement->nextRowset());
        return $data1;
    }

    public function addARTagsByBU($data) {
        if($data['BUID']==""){
            throw new Exception(__t("Enter BUID."));
        }
        if($data['tag']==""){
            throw new Exception(__t("Enter tag."));
        }

        $statement = $this->PDO->prepare("{CALL PHP_AR_AddTagByBU(
            @BUID=:buid,
            @tag=:tag,
            @tagid=:tagid,
            @ID=:id
        )}");

        $statement->bindParam(':buid', $data['BUID'], PDO::PARAM_INT);
        $statement->bindParam(':tag', $data['tag'], PDO::PARAM_STR);
        $statement->bindParam(':tagid', $data['tagid'], PDO::PARAM_INT);
        $statement->bindParam(':id', $ID, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 400);
        
        if (!$statement->execute()) {
            throw new Exception(__t("Sorry folder not added"));
        }

        $statement->closeCursor();
        return ['ID' => $ID];
    }

    public function getProductImagesByBU($start, $limit, $buid) {
        if($buid==0){
            throw new Exception(__t("BUID 0"));
        }
        if($buid==""){
            throw new Exception(__t("Enter BUID"));
        }
        
        $statement = $this->PDO->prepare("{CALL PHP_AR_GetProductImagesByBU(
            @BUID=:buid,
            @Start=:start,
            @Limit=:limit
        )}");

        $statement->bindParam(':buid', $buid, PDO::PARAM_INT);
        $statement->bindParam(':start', $start, PDO::PARAM_INT);
        $statement->bindParam(':limit', $limit, PDO::PARAM_INT);
        $statement->execute();
        $x = 0;        
        $data1 = [];

            do {            
                $rows = $statement->fetchAll(PDO::FETCH_ASSOC);                       
                switch ($x) {                
                    case 0:                    
                        $data1['Pagination'] = [                        
                                            'total' => (int) $rows[0]['totalPersons'],                        
                                            'start' => $start,                        
                                            'limit' => $limit                    
                                        ];                    
                        break;                
                    case 1:                    
                        $data1['Images'] = $rows;                    
                        break;            
                }            
                $x++;

            } while ($statement->nextRowset());
        return $data1;
    }

    public function addARTagWithProduct($data) {
        //dd($data,true);
        $statement = $this->PDO->prepare("{CALL PHP_AR_AddProductARTagsMap(
            @data=:data
        )}");

        if (!empty($data)) {
            $ProductVideos = $this->prepareTagProductXML($data);
            if (!empty($ProductVideos)) {
                $statement->bindParam(':data', $ProductVideos, PDO::PARAM_STR);
            } else {
                $statement->bindParam(':data', $ProductVideos = null, PDO::PARAM_INT);
            }
        }else{
            $statement->bindParam(':data', $PRODUCTVIDEOS = null, PDO::PARAM_INT);
        }
        
        if (!$statement->execute()) {
            throw new Exception(__t("Sorry folder not added"));
        }

        $statement->closeCursor();
        return true;
    }

    public function prepareTagProductXML($data) {
        $FileXml = '';
        foreach ($data['productlabel'] as $key => $value) {
            if(!empty($value['label']) && $value['label'] !== null){
                $FileXml .= '<artags>';
                $FileXml .= '<productid>' . $value['productid'] . '</productid>';
                $FileXml .= '<name>' . $value['label'] . '</name>';
                $FileXml .= '</artags>';
            }            
        }
        if (strlen(trim($FileXml)) > 0) {
            return '<data>' . $FileXml . '</data>';
        }
        return false;
    }

    public function addARScheduleJobByBU($data) {
        if($data['BUID']==""){
            throw new Exception(__t("Enter BUID."));
        }
        if($data['name']==""){
            throw new Exception(__t("Enter name."));
        }
        if($data['status']==""){
            throw new Exception(__t("Enter status."));
        }
        if($data['link']==""){
            throw new Exception(__t("Enter link."));
        }

        if($data['jobid']==""){            
            $data['jobid']=null;
        }

        $data["timestamp"] = gmdate(DATE_ATOM);

        $statement = $this->PDO->prepare("{CALL PHP_AR_AddScheduleJobByBU(
            @BUID=:BUID
            , @name=:name
            , @status=:status 
            , @link=:link
            , @adddate=:adddate
            , @editdate=:editdate
            , @jobid=:jobid 
            , @ID=:id
        )}");

        $statement->bindParam(':BUID', $data['BUID'], PDO::PARAM_INT);
        $statement->bindParam(':name', $data['name'], PDO::PARAM_STR);
        $statement->bindParam(':status', $data['status'], PDO::PARAM_INT);
        $statement->bindParam(':link', $data['link'], PDO::PARAM_STR);
        $statement->bindParam(':adddate', $data['timestamp'], PDO::PARAM_STR);
        $statement->bindParam(':editdate', $data['timestamp'], PDO::PARAM_STR);
        $statement->bindParam(':jobid', $data['jobid'], PDO::PARAM_INT);
        $statement->bindParam(':id', $ID, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 400);
        
        if (!$statement->execute()) {
            throw new Exception(__t("Sorry job not added"));
        }

        $statement->closeCursor();
        return ['ID' => $ID];
    }

    public function getPendingScheduleJobsByBU($start = 0, $limit = 1, $buid) {
        if($buid==0){
            throw new Exception(__t("BUID 0"));
        }
        if($buid==""){
            throw new Exception(__t("Enter BUID"));
        }
        
        $statement = $this->PDO->prepare("{CALL PHP_AR_GetPendingScheduleJobsByBU(
            @BUID=:buid,
            @Start=:start,
            @Limit=:limit
        )}");

        $statement->bindParam(':buid', $buid, PDO::PARAM_INT);
        $statement->bindParam(':start', $start, PDO::PARAM_INT);
        $statement->bindParam(':limit', $limit, PDO::PARAM_INT);
        $statement->execute();
        $x = 0;        
        $data1 = [];

            do {            
                $rows = $statement->fetchAll(PDO::FETCH_ASSOC);                       
                switch ($x) {                
                    case 0:                    
                        //$data1['TotalPendingJobs'] = (int) $rows[0]['totalPersons'];                    
                        break;                
                    case 1:                    
                        $data1['jobs'] = $rows;                    
                        break;            
                }            
                $x++;

            } while ($statement->nextRowset());
        return $data1;
    }

    public function getTotalPendingScheduleJobsByBU($start = 0, $limit = 1, $buid) {
        if($buid==0){
            throw new Exception(__t("BUID 0"));
        }
        if($buid==""){
            throw new Exception(__t("Enter BUID"));
        }
        
        $statement = $this->PDO->prepare("{CALL PHP_AR_GetPendingScheduleJobsByBU(
            @BUID=:buid,
            @Start=:start,
            @Limit=:limit
        )}");

        $statement->bindParam(':buid', $buid, PDO::PARAM_INT);
        $statement->bindParam(':start', $start, PDO::PARAM_INT);
        $statement->bindParam(':limit', $limit, PDO::PARAM_INT);
        $statement->execute();
        $x = 0;        
        $data1 = [];

            do {            
                $rows = $statement->fetchAll(PDO::FETCH_ASSOC);                       
                switch ($x) {                
                    case 0:                    
                        $data1['TotalPendingJobs'] = (int) $rows[0]['totalPersons'];                    
                        break;                
                    case 1:                    
                        //$data1['jobs'] = $rows;                    
                        break;            
                }            
                $x++;

            } while ($statement->nextRowset());
        return $data1;
    }

    public function getScheduleJobsByBU($start, $limit, $buid) {
        if($buid==0){
            throw new Exception(__t("BUID 0"));
        }
        if($buid==""){
            throw new Exception(__t("Enter BUID"));
        }
        
        $statement = $this->PDO->prepare("{CALL PHP_AR_GetScheduleJobsByBU(
            @BUID=:buid,
            @Start=:start,
            @Limit=:limit
        )}");

        $statement->bindParam(':buid', $buid, PDO::PARAM_INT);
        $statement->bindParam(':start', $start, PDO::PARAM_INT);
        $statement->bindParam(':limit', $limit, PDO::PARAM_INT);
        $statement->execute();
        $x = 0;        
        $data1 = [];

            do {            
                $rows = $statement->fetchAll(PDO::FETCH_ASSOC);                       
                switch ($x) {                
                    case 0:                    
                        $data1['TotalJobs'] = (int) $rows[0]['totalPersons'];                    
                        break;                
                    case 1:                    
                        $data1['jobs'] = $rows;                    
                        break;            
                }            
                $x++;

            } while ($statement->nextRowset());
        return $data1;
    }


    public function updateARScheduleJobStatus($data) {
        
        if($data['jobid']==""){            
            throw new Exception(__t("Enter Job ID."));
        }
        if($data['status']==""){            
            throw new Exception(__t("Enter status."));
        }

        $data["timestamp"] = gmdate(DATE_ATOM);

        $statement = $this->PDO->prepare("{CALL PHP_AR_updateScheduleJobStatus(
           @status=:status 
            , @editdate=:editdate
            , @jobid=:jobid 
            , @ID=:id
        )}");

        $statement->bindParam(':status', $data['status'], PDO::PARAM_INT);
        $statement->bindParam(':editdate', $data['timestamp'], PDO::PARAM_STR);
        $statement->bindParam(':jobid', $data['jobid'], PDO::PARAM_INT);
        $statement->bindParam(':id', $ID, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 400);
        
        if (!$statement->execute()) {
            throw new Exception(__t("Sorry folder not added"));
        }

        $statement->closeCursor();
        return ['ID' => $ID];
    }

    public function AREmail($buid){
        if($buid==0){
            throw new Exception(__t("BUID 0"));
        }
        if($buid==""){
            throw new Exception(__t("Enter BUID"));
        }

        $statement = $this->PDO->prepare("{CALL PHP_AR_GetBUDetails(
            @BUID=:BUID
        )}");

        $statement->bindParam(':BUID', $buid, PDO::PARAM_INT);
        $statement->execute();
        $rows = $statement->fetchAll(PDO::FETCH_ASSOC);
        $BUDEtails = $rows[0];
        
        $emailhtml='<table style="width: 720px; background:#f6f6f6; padding-top: 15px; padding-bottom: 15px; margin: 0 auto">
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0" style="width: 620px; border-top: 3px solid #a12328; background: #fff; margin: 0 auto; padding: 0;  box-shadow: 0 0 8px #dfdfdf;">
                        <tr>
                            <td style="font-size: 22px; padding: 20px 10px; text-align: center;font-family: arial; text-transform: uppercase;">
                                Products training have been completed
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top; padding: 0; margin: 0;">
                                <table width="100%" cellpadding="0" cellspacing="0" border="0">                            
                                    <tr>
                                        <td style="padding: 10px; vertical-align: top;">
                                            <h1 style="text-align: center; color: #000; border-bottom: 0px solid #005b96; text-align: left; padding: 0 0 0px; margin: 10px 0 20px; font-family: Arial, Helvetica, sans-serif; font-size: 16px;">Hey there!</h1>
                                            <p style="font-size: 14px; line-height: 24px; color: #222; font-family: Arial, Helvetica, sans-serif; margin: 0 0 15px; padding: 0;">
                                                The training of your products have been completed and ready to use for Web AR.
                                            </p><br>
                                            <p style="font-size: 14px; line-height: 24px; color: #222; font-family: Arial, Helvetica, sans-serif; margin: 0 0 15px; padding: 0;">
                                                Products trained for company <strong>"'.$BUDEtails["CompanyName"].'"</strong> under business unit <strong>"'.$BUDEtails["BusinessUnitName"].'"</strong>.
                                            </p>
                                            <p style="font-size: 14px; font-weight: bold; line-height: 24px; color: #222; font-family: Arial, Helvetica, sans-serif; margin: 0 0 15px; padding: 0;">
                                                This is an automated email and please do not reply here.
                                            </p>
                                            <br>

                                            <p style="font-size: 15px; color: #222; font-family: Arial, Helvetica, sans-serif; margin: 0 0 15px; padding: 0; font-weight: bold;">
                                                Thanks & Regards <br>WAR Support
                                            </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="background: #a12429;padding: 10px 20px;font-size: 12px; line-height: 24px; color: #fff; font-family: Arial, Helvetica, sans-serif; text-align: center">
                                            © 2018 All rights reserved. '.$BUDEtails["CompanyName"].' | Powered By WEBKON
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>';

        $emailData = array();                        
        $emailData['email_subject'] = "Products training have been completed.";            
        $emailData['email_body'] = $emailhtml;
        $emailData['to_email'] = $BUDEtails["Email"]; //$data['email'];

        $CURLOPT_POSTFIELDS = array(
            'To' => $emailData['to_email'],
            'Subject' => $emailData['email_subject'],
            'MAilBody' => $emailData['email_body'],
            'CompanyID' => $BUDEtails["FKCompanyID"]
        );

        $response = callApi(API_URL_ENDPOINT . '/webapi/Mail/SendMail', 'POST', $CURLOPT_POSTFIELDS);

        return $response;
    }

    public function getARTags($start, $limit) {
        
        $statement = $this->PDO->prepare("{CALL PHP_AR_GetTags(
            @Start=:start,
            @Limit=:limit
        )}");

        $statement->bindParam(':start', $start, PDO::PARAM_INT);
        $statement->bindParam(':limit', $limit, PDO::PARAM_INT);
        $statement->execute();
        $x = 0;        
        $data1 = [];

            do {            
                $rows = $statement->fetchAll(PDO::FETCH_ASSOC);                       
                switch ($x) {                
                    case 0:                    
                        $data1['Pagination'] = [                        
                                            'total' => (int) $rows[0]['totalPersons'],                        
                                            'start' => $start,                        
                                            'limit' => $limit                    
                                        ];                    
                        break;                
                    case 1:                    
                        $data1['Images'] = $rows;                    
                        break;            
                }            
                $x++;

            } while ($statement->nextRowset());
        return $data1;
    }

}
