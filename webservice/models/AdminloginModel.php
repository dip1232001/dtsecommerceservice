<?php
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ContactsCardsModel
 *
 * @author Abhijit Manna
 */

require_once MODEL_PATH . DS . 'App.php';
//require_once MODEL_PATH . DS . 'EmailModel.php';

class AdminloginModel extends AppModel {

    //put your code here

    public function __construct($callAuth = false) {
        parent::__construct($callAuth);
    }

    public function Login($data, $file = []) {
        
        
        if($data['email']==""){
            throw new Exception(__t("Enter Email"));
        }
        if($data['password']==""){
            throw new Exception(__t("Enter password"));
        }


        $url = "https://stagingmtc365.myteamconnector.com/webapi/Authenticate/login";

        //curl headers
        $headers = array(
            "authorization: Bearer apiuser:wXPocq0fG24=",
            "cache-control: no-cache",
            "content-type: application/x-www-form-urlencoded"
        );

        //request body (make an array, then convert it to a query string)
        $body_arr = array();

        $body_str = http_build_query($body_arr);

        //Set the CURL Options
        $curl_opts = array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_URL => $url,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $body_str,
            CURLOPT_CONNECTTIMEOUT => 10,
            CURLOPT_TIMEOUT => 20,
            CURLOPT_HEADER => 1,
            CURLOPT_VERBOSE => 1
        );

        //Set up the CURL request
        $resCurl = curl_init();
        curl_setopt_array($resCurl, $curl_opts);

        // Send the request & save response to $resp
        $response = curl_exec($resCurl);
        $info = curl_getinfo($resCurl, CURLINFO_HEADER_SIZE);
        $err_no = curl_errno($resCurl);
        curl_close($resCurl);
        $res = get_headers_from_curl_response($response);

        if (0 != $err_no) {
            throw new Exception('Error Retrieving API Auth Token. Curl Err:' . $err_no);
        } else {
            if (isset($res['error'])) {
                throw new Exception('Error Retrieving API Auth Token. API err:' . $res['error'] . " - " . $res['error_description']);
            }
            $token = $res['Token'];

            $headers = array(
                "Token: ".$token,
                "cache-control: no-cache"
            );
            $url = "https://stagingmtc365.myteamconnector.com/webapi/User/CheckLogin?Email=".$data['email']."&password=".$data['password'];

            //Set the CURL Options
            $curl_opts = array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_HTTPHEADER => $headers,
                CURLOPT_URL => $url,
                CURLOPT_CONNECTTIMEOUT => 10,
                CURLOPT_TIMEOUT => 20,
                CURLOPT_HEADER => 0,
                CURLOPT_VERBOSE => 1
            );

            //Set up the CURL request
            $resCurl = curl_init();
            curl_setopt_array($resCurl, $curl_opts);
            // Send the request & save response to $resp
            $response = curl_exec($resCurl);
            $info = curl_getinfo($resCurl, CURLINFO_HEADER_SIZE);
            $err_no = curl_errno($resCurl);
            curl_close($resCurl);
            
            $userID = $response;

            if($userID > 0){







            }else{
                throw new Exception('Invalid login details');
            }
        }
        return $data;
    }   

    function getAuthToken()
    {

        $session_auth = !isset($_SESSION['ACCESS_TOKEN']) ? null : $_SESSION['ACCESS_TOKEN'];

        //if it does, check if it has expired (this may also include this request being in the EXPIRE_BUFFER time)
        if (null != $session_auth) {
            //Session auth is an array. Get the expire time, and check it.
            //get current timestamp
            $curr_time = time();

            //compare the session auth expire
            if ($session_auth['expire_time'] > $curr_time || empty($session_auth['access_token'])) {
                //set the class auth token variable
                $session_auth = null;
            }
        }


        //No token or expired token means we need a new one
        if (null == $session_auth) {
            //build the params for the call
            $url = WEBAPIENDPOINT . "/Authenticate/login";

            //curl headers
            $headers = array(
                "authorization: Bearer apiuser:wXPocq0fG24=",
                "cache-control: no-cache",
                "content-type: application/x-www-form-urlencoded"
            );

            //request body (make an array, then convert it to a query string)
            $body_arr = array();

            $body_str = http_build_query($body_arr);

            //Set the CURL Options
            $curl_opts = array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_HTTPHEADER => $headers,
                CURLOPT_URL => $url,
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => $body_str,
                CURLOPT_CONNECTTIMEOUT => 10,
                CURLOPT_TIMEOUT => 20,
                CURLOPT_HEADER => 1,
                CURLOPT_VERBOSE => 1
            );

            //Set up the CURL request
            $resCurl = curl_init();
            curl_setopt_array($resCurl, $curl_opts);

            // Send the request & save response to $resp
            $response = curl_exec($resCurl);
            $info = curl_getinfo($resCurl, CURLINFO_HEADER_SIZE);
            $err_no = curl_errno($resCurl);
            curl_close($resCurl);
            $res = get_headers_from_curl_response($response);
            //If curl returned an error, abort, otherwise, save the token
            if (0 != $err_no) {
                throw new Exception('Error Retrieving API Auth Token. Curl Err:' . $err_no);
            } else {
                if (isset($res['error'])) {
                    throw new Exception('Error Retrieving API Auth Token. API err:' . $res['error'] . " - " . $res['error_description']);
                }

                //store it in the session
                $session_auth = array(
                    "access_token" => $res['Token'],
                    "expire_time" => $expire_time
                );
                $_SESSION['ACCESS_TOKEN'] = $session_auth;
            }//end if/esle err

        }
    }

    function get_headers_from_curl_response($response)
    {
        $headers = array();

        $header_text = substr($response, 0, strpos($response, "\r\n\r\n"));

        foreach (explode("\r\n", $header_text) as $i => $line) {
            if ($i === 0) {
                $headers['http_code'] = $line;
            } else {
                list($key, $value) = explode(': ', $line);

                $headers[$key] = $value;
            }
        }

        return $headers;
    }
    
    

}
