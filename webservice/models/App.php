<?php

require_once VENDOR_PATH . DS . 'oauth2-server-php' . DS . 'src' . DS . 'OAuth2' . DS . 'Autoloader.php';
require_once MODEL_PATH . DS . 'BussinessUnitModel.php';
OAuth2\Autoloader::register();
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of App
 *
 * @author dipankar
 */
class AppModel
{

    public $AuthServer = null;
    protected $DbAlias = 'PHP_Ecommerce_';
    protected $BUID;
    protected $PDO = null;

    function __construct()
    {
        if ($this->AuthServer == null) {
            $storage = new OAuth2\Storage\Pdo(array('dsn' => 'sqlsrv:Server=' . DB_HOST . ( DB_PORT == null ? '' : ',' . DB_PORT) . ';Database=' . DB_NAME, 'username' => DB_USER, 'password' => DB_PASS));
            $this->AuthServer = new OAuth2\Server($storage);
            $this->AuthServer->addGrantType(new OAuth2\GrantType\ClientCredentials($storage));
            $this->AuthServer->addGrantType(new OAuth2\GrantType\AuthorizationCode($storage));
        }
        $this->__preparePDO();
    }

    function __preparePDO()
    {
        if (null === $this->PDO) {
            $this->PDO = new PDO('sqlsrv:Server=' . DB_HOST . ( DB_PORT == null ? '' : ',' . DB_PORT) . ';Database=' . DB_NAME, DB_USER, DB_PASS, [
                PDO::ATTR_EMULATE_PREPARES => false,
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
                    ]
                );
        }
    }
    protected function CheckAuthenticated()
    {
        if (!$this->AuthServer->verifyResourceRequest(OAuth2\Request::createFromGlobals())) {
            header("HTTP/1.1 401 Unauthorized");
            throw new Exception("Authentication Token Expired");
        }

        $Buid = $this->AuthServer->getAccessTokenData(OAuth2\Request::createFromGlobals());
        if (empty($Buid['user_id'])) {
            header("HTTP/1.1 401 Unauthorized");
            throw new Exception("Invalid Token Request, company doesn't exists");
        }
        $this->__preparePDO();
        $this->BUID = (int) $Buid['user_id'];
        $Business = new BussinessUnitModel();
        if (!$Business->_checkBuExits()) {
            header("HTTP/1.1 401 Unauthorized");
            throw new Exception("Sorry, you can not access this api, your authentication is disbaled");
        }
    }

    function getOAuthSecret()
    {
        return $this->AuthServer->handleTokenRequest(OAuth2\Request::createFromGlobals())->send();
    }
}
