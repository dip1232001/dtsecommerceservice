<?php
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AugmentedRealityModel
 *
 * @author Achyut Sinha
 */

require_once MODEL_PATH . DS . 'App.php';
//require_once MODEL_PATH . DS . 'EmailModel.php';

class AugmentedRealityModel extends AppModel {

    //put your code here

    public function __construct($callAuth = false) {
        parent::__construct($callAuth);
    }

    public function saveARTempImg($data, $file = []) {
        //dd($data,true);
        if($data['BUID']==""){
            throw new Exception(__t("Enter BUID."));
        }
        if($data['companyid']==""){
            throw new Exception(__t("Enter company Id."));
        }
        if(empty($data['documents'])){
            throw new Exception(__t("Enter documents."));
        }
       
        if(is_string ($data["documents"]) && ($documents= json_decode("[".$data["documents"]."]",true))!=false){
            $data["documents"] = $documents;
        }

        $Documents = $this->prepareXML($data['documents']);

        $statement = $this->PDO->prepare("{CALL PHP_AR_AddTempImages(@Compayid=:Compayid,@BUID=:BUID,@data=:data)}");
        
        $statement->bindParam(':Compayid', $data['companyid'], PDO::PARAM_INT);
        $statement->bindParam(':BUID', $data['BUID'], PDO::PARAM_INT);
        $statement->bindParam(':data', $Documents, PDO::PARAM_STR);


        if (!$statement->execute()) {
            throw new Exception(__t("Sorry data not uploaded."));
        }

        $statement->closeCursor();
    }

    public function getARTempImg($data) {
        if($data['companyid']==""){
            throw new Exception(__t("Enter company Id."));
        }
                
        if($data['start']==''){
            $data['start']=0;
        }
        if($data['limit']==''){
            $data['limit']=20;
        }

        //dd($data, true);

        $statement = $this->PDO->prepare("{CALL PHP_AR_GetTempImg(
            @BUID=:buid,
            @Compayid=:Compayid,
            @Start=:start,
            @Limit=:limit
        )}");

        $statement->bindParam(':buid', $data['BUID'], PDO::PARAM_INT);
        $statement->bindParam(':Compayid', $data['companyid'], PDO::PARAM_INT);
        $statement->bindParam(':start', $data['start'], PDO::PARAM_INT);
        $statement->bindParam(':limit', $data['limit'], PDO::PARAM_INT);
        $statement->execute();
        $x = 0;        
        $data1 = [];

            do {            
                $rows = $statement->fetchAll(PDO::FETCH_ASSOC);                       
                switch ($x) {                
                    case 0:                    
                        $data1['Pagination'] = [                        
                                            'total' => (int) $rows[0]['totalPersons'],                        
                                            'start' => $data['start'],                        
                                            'limit' => $data['limit']                    
                                        ];                    
                        break;                
                    case 1:                    
                        $data1['Images'] = $rows;                    
                        break;            
                }            
                $x++;

            } while ($statement->nextRowset());
        return $data1;
    }

    public function uploadARTempImg($FILES, $BUID) {
        $max_size = file_upload_max_size();
        $client   = new S3Client([
                    'version' => 'latest',
                    'region' => AWS_REGION,
                    'credentials' => [
                    'key' => AWS_ACCESS_KEY,
                    'secret' => AWS_SECRET_KEY
            ]
        ]);

        //dd($FILES);
        //dd($BUID,true);

        $valid_formats = ["jpg", "png", "gif", "jpeg"];
        $FileXml = '';

        $ret = [];

        //foreach($FILES['Images'] as $values){
            //dd($FILES,true);
            foreach($FILES as $key=>$val){
                //dd($val,true);
                if ($FILES[$key]['error'] == 0) {
                    $name =$FILES[$key]['name'];
                    $size = $FILES[$key]['size'];
                    $tmp = $FILES[$key]['tmp_name'];
                    $type = $FILES[$key]['type'];
        
                    if ($size > $max_size) {
                        throw new Exception(__t("Sorry image " . $name . " size is bigger than accepted size"));
                    }
                    $ext = strtolower(getExtension($name));
        
                    if (!in_array($ext, $valid_formats)) {
                        throw new Exception(__t("Sorry image " . $name . " is not valid ,we accept " . implode(",", $valid_formats)));
                    }
        
                    $image_name_actual = time() ."".$key. "." . $ext;
                    try {
                        $Result = $client->putObject(array(
                            'Bucket'        => AWS_BUCKET_NAME,
                            'Key'           => 'BU' . $BUID . '/' . $image_name_actual,
                            'SourceFile'    => $tmp,
                            'StorageClass'  => 'REDUCED_REDUNDANCY',
                            'ACL'           => 'public-read',
                            'ContentType'   => $type,
                            'Expires'       => date('Y-m-d H:i:s', strtotime("2 years"))
                        ));
                    } catch (S3Exception $exc) {
                        throw new Exception("Aws S3 Error : " . $exc->getAwsErrorMessage());
                    }
        
                    $ret[]= $Result['ObjectURL'];
        
                }

            //}
        }
        return $ret;
    }

    public function prepareXML($data) {
        $FileXml = '';
        foreach ($data as $key => $value) {
            $FileXml .= '<documents>';
            $FileXml .= '<imageurl>' . strip_tags(trim($value['imageurl'])) . '</imageurl>';
            $FileXml .= '<xmlDoc>' . strip_tags(trim($value['xmlDoc'])) . '</xmlDoc>';
            $FileXml .= '</documents>';
        }
        if (strlen(trim($FileXml)) > 0) {
            return '<data>' . $FileXml . '</data>';
        }
        return false;
    }

    public function saveARTagImg($data) {
        /*if($data['BUID']==""){
            throw new Exception(__t("Enter BUID."));
        }
        if($data['companyid']==""){
            throw new Exception(__t("Enter company Id."));
        }
        if($data['tag']==""){
            throw new Exception(__t("Enter tag."));
        }
        if($data['link']==""){
            throw new Exception(__t("Enter link."));
        }
        if(empty($data['imageUrl'])){
            throw new Exception(__t("Enter image Urls."));
        }*/
        if($data == ''){
            throw new Exception(__t("invalid post data."));
        }

        $imageUrl = $this->prepareiuXML($data);

        //$statement = $this->PDO->prepare("{CALL PHP_AR_AddTagImages(@Compayid=:Compayid,@BUID=:BUID,@tag=:tag,@link=:link,@data=:data)}");

        $statement = $this->PDO->prepare("{CALL PHP_AR_AddTagImages(@data=:data)}");
        
        /*$statement->bindParam(':Compayid', $data['companyid'], PDO::PARAM_INT);
        $statement->bindParam(':BUID', $data['BUID'], PDO::PARAM_INT);
        $statement->bindParam(':tag', $data['tag'], PDO::PARAM_STR);
        $statement->bindParam(':link', $data['link'], PDO::PARAM_STR);*/
        $statement->bindParam(':data', $imageUrl, PDO::PARAM_STR);


        if (!$statement->execute()) {
            throw new Exception(__t("Sorry data not uploaded."));
        }

        $statement->closeCursor();
    }

    public function prepareiuXML($data) {
        $FileXml = '';
        foreach ($data as $key => $value) {
            $FileXml .= '<documents>';
            $FileXml .= '<BUID>' . strip_tags(trim($value['BUID'])) . '</BUID>';
            $FileXml .= '<companyid>' . strip_tags(trim($value['companyid'])) . '</companyid>';
            $FileXml .= '<tag>' . strip_tags(trim($value['tag'])) . '</tag>';
            $FileXml .= '<link>' . strip_tags(trim($value['link'])) . '</link>';
            $FileXml .= '<imageurl>' . strip_tags(trim($value['imageUrl'])) . '</imageurl>';
            $FileXml .= '</documents>';
        }
        if (strlen(trim($FileXml)) > 0) {
            return '<data>' . $FileXml . '</data>';
        }
        return false;
    }

    function get_headers_from_curl_response($response)
    {
        $headers = array();

        $header_text = substr($response, 0, strpos($response, "\r\n\r\n"));

        foreach (explode("\r\n", $header_text) as $i => $line) {
            if ($i === 0) {
                $headers['http_code'] = $line;
            } else {
                list($key, $value) = explode(': ', $line);

                $headers[$key] = $value;
            }
        }

        return $headers;
    }

    public function getlinktempval($data) {
        
        if($data['BUID']==""){
            throw new Exception(__t("BUID is required."));
        }

        $url = API_URL_ENDPOINT . "/TemplateManager/templates/getlinktempval";
        $headers = array(
            "Referer: ". API_URL_ENDPOINT,
            "cache-control: no-cache",
            "content-type: multipart/form-data"
        );
        $body_arr = array("buid"=>$data['BUID'], "templateid"=>$data['templateid'],"appid"=>$data['appid']);
        $curl_opts = array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_URL => $url,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $body_arr,
            CURLOPT_CONNECTTIMEOUT => 10,
            CURLOPT_TIMEOUT => 20,
            CURLOPT_HEADER => 0,
            CURLOPT_VERBOSE => 1
        );

        $resCurl = curl_init();
        curl_setopt_array($resCurl, $curl_opts);
        $response = curl_exec($resCurl);
        $info = curl_getinfo($resCurl, CURLINFO_HEADER_SIZE);
        $err_no = curl_errno($resCurl);
        curl_close($resCurl);

        if (0 != $err_no) {
            throw new Exception('Error Retrieving API Auth Token. Curl Err:' . $err_no);
        } else {
            return json_decode($response);
        }
    } 

    public function getARTagImg($data) {
        if($data['companyid']==""){
            throw new Exception(__t("Enter company Id."));
        }
                
        if($data['start']==''){
            $data['start']=0;
        }
        if($data['limit']==''){
            $data['limit']=20;
        }

        //dd($data, true);

        $statement = $this->PDO->prepare("{CALL PHP_AR_GetTagImg(
            @BUID=:buid,
            @Compayid=:Compayid,
            @Start=:start,
            @Limit=:limit
        )}");

        $statement->bindParam(':buid', $data['BUID'], PDO::PARAM_INT);
        $statement->bindParam(':Compayid', $data['companyid'], PDO::PARAM_INT);
        $statement->bindParam(':start', $data['start'], PDO::PARAM_INT);
        $statement->bindParam(':limit', $data['limit'], PDO::PARAM_INT);
        $statement->execute();
        $x = 0;        
        $data1 = [];

            do {            
                $rows = $statement->fetchAll(PDO::FETCH_ASSOC);                       
                switch ($x) {                
                    case 0:                    
                        $data1['Pagination'] = [                        
                                            'total' => (int) $rows[0]['totalPersons'],                        
                                            'start' => $data['start'],                        
                                            'limit' => $data['limit']                    
                                        ];                    
                        break;                
                    case 1:                    
                        $data1['Images'] = $rows;                    
                        break;            
                }            
                $x++;

            } while ($statement->nextRowset());
        return $data1;
    }

    public function getDeviceList($data) {
        if($data['BUID']==""){
            throw new Exception(__t("Enter BUID Id."));
        }

        //dd($data, true);

        $statement = $this->PDO->prepare("{CALL PHP_AR_GetDeviceLIst(
            @BUID=:buid
        )}");

        $statement->bindParam(':buid', $data['BUID'], PDO::PARAM_INT);
        $statement->execute();
        $rows = $statement->fetchAll(PDO::FETCH_ASSOC);    
        /*$x = 0;        
        $data1 = [];

            do {            
                $rows = $statement->fetchAll(PDO::FETCH_ASSOC);                       
                switch ($x) {                
                    case 0:                    
                        $data1['Pagination'] = [                        
                                            'total' => (int) $rows[0]['totalPersons'],                        
                                            'start' => $data['start'],                        
                                            'limit' => $data['limit']                    
                                        ];                    
                        break;                
                    case 1:                    
                        $data1['Images'] = $rows;                    
                        break;            
                }            
                $x++;

            } while ($statement->nextRowset());*/
        return $rows;
    }

}
