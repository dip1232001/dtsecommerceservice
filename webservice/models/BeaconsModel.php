<?php

use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CouponModel
 *
 * @author dipankar
 */
require_once MODEL_PATH . DS . 'App.php';

class BeaconsModel extends AppModel {

    //put your code here
    
    public function __construct($callAuth = false) {
        parent::__construct($callAuth);
    }

    public function addDevice($data) {
        //dd($data,true);
        if (empty($data['i_intFKManufacturerID'])) {
            throw new Exception("Sorry Manufacturer ID is required");
        }
        if (empty($data['i_intFKCompanyID'])) {
            throw new Exception("Sorry Company ID is required");
        }
        if (empty($data['i_intFKBusinessUnitID'])) {
            throw new Exception("Sorry Business Unit ID is required");
        }
        if (empty($data['i_strMACAddress'])) {
            throw new Exception("Sorry MAC Address is required");
        }
        if (empty($data['i_strEddystoneID'])) {
            throw new Exception("Sorry Eddystone ID is required");
        }
        if (empty($data['i_strAliasName'])) {
            throw new Exception("Sorry Alias Name is required");
        }
        if (empty($data['i_strDescription'])) {
            throw new Exception("Sorry Description is required");
        }
                
        
        $addStatement = $this->PDO->prepare("{CALL usp_EddystoneDevice_InsertUpdate (@i_intEddystoneDeviceID = :i_intEddystoneDeviceID, @i_intFKManufacturerID = :i_intFKManufacturerID, @i_intFKCompanyID = :i_intFKCompanyID, @i_intFKBusinessUnitID = :i_intFKBusinessUnitID, @i_strMACAddress = :i_strMACAddress, @i_strEddystoneID = :i_strEddystoneID, @i_strAliasName = :i_strAliasName, @i_strDescription = :i_strDescription, @i_intCreatedBy = :i_intCreatedBy, @i_intUpdatedBy = :i_intUpdatedBy)}");
        
        $addStatement->bindParam(':i_intFKManufacturerID', $data['i_intFKManufacturerID'], PDO::PARAM_INT);
        $addStatement->bindParam(':i_intFKCompanyID', $data['i_intFKCompanyID'], PDO::PARAM_INT);
        $addStatement->bindParam(':i_intFKBusinessUnitID', $data['i_intFKBusinessUnitID'], PDO::PARAM_INT);
        $addStatement->bindParam(':i_strMACAddress', $data['i_strMACAddress'], PDO::PARAM_STR);
        $addStatement->bindParam(':i_strEddystoneID', $data['i_strEddystoneID'], PDO::PARAM_STR);
        $addStatement->bindParam(':i_strAliasName', $data['i_strAliasName'], PDO::PARAM_STR);
        $addStatement->bindParam(':i_strDescription', $data['i_strDescription'], PDO::PARAM_STR);
        $addStatement->bindParam(':i_intCreatedBy', $data['i_intCreatedBy'], PDO::PARAM_INT);
        $addStatement->bindParam(':i_intUpdatedBy', $data['i_intUpdatedBy'], PDO::PARAM_INT);
        
        if (empty($data['i_intEddystoneDeviceID'])) {
            $addStatement->bindParam(':i_intEddystoneDeviceID', $id = 0, PDO::PARAM_INT);
        } else {
            $addStatement->bindParam(':i_intEddystoneDeviceID', $id = $data['i_intEddystoneDeviceID'], PDO::PARAM_INT);
        }
        //$addStatement->bindParam(':newdeviceID', $newdeviceID, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000);
        //dd($data, true);
        
        $addStatement->execute();
        /*return [
            'id' => empty($data['id']) ? $newdeviceID : $data['id'],
            'devicename' => $data['devicename'],
            'deviceid' => $data['deviceid'],
            'msg' => empty($data['id']) ? __t("Device added succesfully") : __t("Device updated succesfully")
        ];*/
        $devices = $addStatement->fetchAll(PDO::FETCH_ASSOC);
        
        
        /*$addStatementStatus = $this->PDO->prepare("{CALL PHP_Beacons_Status_add (@buid = :buid, @deviceID = :deviceID)}");
        
        $addStatementStatus->bindParam(':buid', $data['i_intFKBusinessUnitID'], PDO::PARAM_INT);
        $addStatementStatus->bindParam(':deviceID', $data['i_strEddystoneID'], PDO::PARAM_STR);
        $addStatementStatus->execute();*/
        
                
        return [
            'devices' => $devices
        ]; 
    }
    
    public function getDevice($i_intFKCompanyID, $i_intEddystoneDeviceID,$i_intFKBusinessUnitID) {
        
        if (empty($i_intFKBusinessUnitID) && empty($i_intFKCompanyID)) {
            throw new Exception("Sorry Company ID or Business Unit ID is required");
        }
        
        $getStatement = $this->PDO->prepare("{CALL PHP_Beacons_get (@i_intFKCompanyID = :i_intFKCompanyID, @i_intFKBusinessUnitID = :i_intFKBusinessUnitID, @i_intEddystoneDeviceID=:i_intEddystoneDeviceID)}");
        
        $getStatement->bindParam(':i_intFKCompanyID', $i_intFKCompanyID, PDO::PARAM_INT);
        $getStatement->bindParam(':i_intEddystoneDeviceID', $i_intEddystoneDeviceID, PDO::PARAM_INT);
        $getStatement->bindParam(':i_intFKBusinessUnitID', $i_intFKBusinessUnitID, PDO::PARAM_INT);
        $getStatement->execute();
        
        $devices = $getStatement->fetchAll(PDO::FETCH_ASSOC);
                
        return [
            'devices' => $devices
        ];       
        
    }
    
    public function addDeviceStatus($data) {
        //dd($data,true);
        if (empty($data['companyid'])) {
            throw new Exception("Sorry Company ID is required");
        }
        if (empty($data['uuid'])) {
            throw new Exception("Sorry UUID is required");
        }
        /*if (empty($data['i_intEddystoneDeviceStatus'])) {
            throw new Exception("Sorry Status is required");
        } */
        
        if (!empty($data['status']) || is_numeric($data['status'])) {
            $isActive = $data['status'];
        } else {
            $isActive = 1;
        }
        
        $addStatementStatus = $this->PDO->prepare("{CALL PHP_Beacons_Status_add (@buid = :buid, @deviceID = :deviceID, @status = :status ,@type=:type)}");
        
        $addStatementStatus->bindParam(':buid', $data['companyid'], PDO::PARAM_INT);
        $addStatementStatus->bindParam(':deviceID', $data['uuid'], PDO::PARAM_STR);
        $addStatementStatus->bindParam(':status', $isActive, PDO::PARAM_INT);
        $addStatementStatus->bindParam(':type', $data['type'], PDO::PARAM_STR);
        $addStatementStatus->execute();
        
        $devices = $addStatementStatus->fetchAll(PDO::FETCH_ASSOC);
                
        return [
            'devices' => $devices
        ]; 
    }
    
    public function getDeviceStatus($companyid,$uuid) {
        
        if (empty($companyid)) {
            throw new Exception("Sorry Company ID is required");
        }
        
        $getStatement = $this->PDO->prepare("{CALL PHP_Beacons_Status_get (@companyid = :companyid, @i_intEddystoneDeviceIDuuid=:uuid)}");
        
        $getStatement->bindParam(':companyid', $companyid, PDO::PARAM_INT);
        $getStatement->bindParam(':uuid', $uuid, PDO::PARAM_STR);
        $getStatement->execute();
        
        $devices = $getStatement->fetchAll(PDO::FETCH_ASSOC);
                
        return [
            'devices' => $devices
        ];       
        
    }
    
    public function deleteDevice($id,$buid) {
        
        /*if (empty($id)) {
            throw new Exception("Sorry ID is required");
        }
         if (empty($buid)) {
            throw new Exception("Sorry BUID is required");
        }
        
        $getStatement = $this->PDO->prepare("{CALL PHP_Beacons_delete (@buid = :buid, @id=:id)}");
        
        $getStatement->bindParam(':id', $id, PDO::PARAM_INT);
        $getStatement->bindParam(':buid', $buid, PDO::PARAM_INT);
        $getStatement->execute();
        
        return [
            'msg' => __t("Device Deleted succesfully")
        ];*/
        
    }

}
