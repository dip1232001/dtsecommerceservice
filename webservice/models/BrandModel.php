<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BrandModel
 *
 * @author dipankar
 */
require_once MODEL_PATH . DS . 'App.php';

class BrandModel extends AppModel {

    //put your code here

    public $Table = 'Brands';

    public function __construct() {
        parent::__construct();
        $this->CheckAuthenticated();
    }

    public function addBrand($data) {
        if (empty($data['name'])) {
            throw new Exception("Sorry, Brand name can not be blank");
        }

        $checkBrandExists = $this->PDO->prepare("select count(*) as BrandExist from {$this->DbAlias}{$this->Table} where name=:brandName and buid=:Buid");
        $checkBrandExists->bindParam(':brandName', $data['name'], PDO::PARAM_STR);
        $checkBrandExists->bindParam(':Buid', $this->BUID, PDO::PARAM_INT);
        $checkBrandExists->execute();
        $fetchData = $checkBrandExists->fetch(PDO::FETCH_ASSOC);
        if ($fetchData['BrandExist'] == 0) {
            $insertStatement = $this->PDO->prepare("INSERT INTO {$this->DbAlias}{$this->Table} (name,buid) VALUES (:brandName,:Buid)");
            $insertStatement->bindParam(':brandName', $data['name'], PDO::PARAM_STR);
            $insertStatement->bindParam(':Buid', $this->BUID, PDO::PARAM_INT);
            $insertStatement->execute();
            return [
                'id' => $this->PDO->lastInsertId(),
                'name' => $data['name']
            ];
        }
        throw new Exception("Sorry you have already added this brand");
    }

}
