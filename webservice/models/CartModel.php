<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CartModel
 *
 * @author dipan
 */
require_once MODEL_PATH . DS . 'ProductModel.php';
require_once MODEL_PATH . DS . 'TaxModel.php';
require_once MODEL_PATH . DS . 'EmailModel.php';
require_once MODEL_PATH . DS . 'CustomersModel.php';
require_once MODEL_PATH . DS . 'PaymentModel.php';
class CartModel extends ProductModel
{

    private $productID = null;

    //put your code here

    public function __construct($callAuth = false)
    {
        parent::__construct($callAuth);
    }

    public function AddToCart($data)
    {
        if (!empty($data['BUID'])) {
            $this->BUID = $data['BUID'];
        } else {
            $this->CheckAuthenticated();
        }
        if ($this->BUID == null) {
            throw new Exception("Sorry BUID is required");
        }

        if (empty($data['cartItems'])) {
            throw new Exception(__t("Sorry no cart items added"));
        } else {
            if (isset($data['cartItems']['product_id'])) {
                $data['cartItems'] = [$data['cartItems']];
            }
            // return dd($data['cartItems'],true);
            $productmodel = new ProductModel();
            foreach ($data['cartItems'] as $k => $product) {
                if (empty($product['varients'])) {
                    $productdetails = $productmodel->getProduct($product['product_id'], '', '');
                    if (!empty($productdetails['ProductDetails']['ProductVarients'])) {
                        $varientadded = false;
                        foreach ($productdetails['ProductDetails']['ProductVarients'] as $varient) {
                            if (!$varientadded && $varient['is_required'] == 1) {
                                $data['cartItems'][$k]['varients'][0]['id'] = $varient['options'][0]['id'];
                                $data['cartItems'][$k]['varients'][0]['quantity'] = $product['quantity'];
                                $varientadded = true;
                                break;
                            }
                            continue;
                        }
                        if ($varientadded) {
                            break;
                        }
                    }
                }

            }

            $data['cartItems'] = $this->__prepareXml($data['cartItems']);
        }
        if (empty($data['cartItems'])) {
            throw new Exception(__t("Sorry no cart items added"));
        }
        if (empty($data['identifier'])) {
            if (empty($_COOKIE['identifier'])) {
                $cookieidentifier = 'udtc' . $this->BUID . time();
                setCookies('identifier', $cookieidentifier, time() + 24 * 60 * 60 * 30 * 10, '/');
                $data['identifier'] = $cookieidentifier;
            } else {
                $data['identifier'] = $_COOKIE['identifier'];
            }
        }

        if (empty($data['customer_id'])) {
            $data['customer_id'] = null;
        }

        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_AddTo_Customer_Cart (@BUID=:buid,@customer_id=:customer_id, @identifier=:identifier,@CartXml=:cartitems)}");
        $statement->bindParam(':buid', $this->BUID, PDO::PARAM_INT);
        $statement->bindParam(':customer_id', $data['customer_id'], PDO::PARAM_INT);
        $statement->bindParam(':identifier', $data['identifier'], PDO::PARAM_STR);
        $statement->bindParam(':cartitems', $data['cartItems'], PDO::PARAM_STR);
        $return = $statement->execute();
        $statement->closeCursor();
        return $return;
    }

    private function __prepareXml($productsdata)
    {
        $xml = '';
        foreach ($productsdata as $key => $cartitem) {
            if (empty($cartitem['quantity']) || $cartitem['quantity'] < 1) {
                $cartitem['quantity'] = 1;
            }
            $xml .= '<cartitems>';
            $xml .= '<product_id>' . (int) trim(strip_tags($cartitem['product_id'])) . '</product_id>';
            $xml .= '<quantity>' . (int) trim(strip_tags((empty($cartitem['quantity']) ? 1 : $cartitem['quantity']))) . '</quantity>';
            if (!empty($cartitem['varients'])) {
                $xml .= '<varients>';
                foreach ($cartitem['varients'] as $key => $vari) {
                    $xmljson[] = $vari;
                }
                $xml .= json_encode($xmljson);
                $xml .= '</varients>';
            }
            $xml .= '</cartitems>';
        }

        if (!empty($xml)) {
            return '<data>' . $xml . '</data>';
        }

        return false;
    }

    public function deleteFromCart($data)
    {
        if (!empty($data['BUID'])) {
            $this->BUID = $data['BUID'];
        } else {
            $this->CheckAuthenticated();
        }
        if ($this->BUID == null) {
            throw new Exception("Sorry BUID is required");
        }
        if (empty($data['identifier'])) {
            if (empty($_COOKIE['identifier'])) {
                $cookieidentifier = 'udtc' . $this->BUID . time();
                setCookies('identifier', $cookieidentifier, time() + 24 * 60 * 60 * 30 * 10, '/');
                $data['identifier'] = $cookieidentifier;
            } else {
                $data['identifier'] = $_COOKIE['identifier'];
            }
        }
        if (empty($data['customer_id'])) {
            $data['customer_id'] = null;
        }
        if (empty($data['identifier'])) {
            $data['identifier'] = null;
        }
        if (empty($data['customer_id']) && empty($data['identifier'])) {
            throw new Exception(__t("Sorry identifier and customerid both cant be empty"));
        }

        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_RemoveFromCart (@BUID=:buid,@CustomerID=:customer_id, @identifier=:identifier,@product_id=:product_id)}");
        $statement->bindParam(':buid', $this->BUID, PDO::PARAM_INT);
        $statement->bindParam(':customer_id', $data['customer_id'], PDO::PARAM_INT);
        if (empty($data['identifier'])) {
            $statement->bindParam(':identifier', $data['identifier'], PDO::PARAM_INT);
        } else {
            $statement->bindParam(':identifier', $data['identifier'], PDO::PARAM_STR);
        }

        $statement->bindParam(':product_id', $data['product_id'], PDO::PARAM_INT);

        $return = $statement->execute();
        $statement->closeCursor();
        return $return;
    }

    public function getCartitems($data)
    {
        if (!empty($data['BUID'])) {
            $this->BUID = $data['BUID'];
        } else {
            $this->CheckAuthenticated();
        }
        if ($this->BUID == null) {
            throw new Exception("Sorry BUID is required");
        }

        if (empty($data['identifier'])) {
            if (empty($_COOKIE['identifier'])) {
                $cookieidentifier = 'udtc' . $this->BUID . time();
                setCookies('identifier', $cookieidentifier, time() + 24 * 60 * 60 * 30 * 10, '/');
                $data['identifier'] = $cookieidentifier;
            } else {
                $data['identifier'] = $_COOKIE['identifier'];
            }
        }
        if (empty($data['customer_id'])) {
            $data['customer_id'] = null;
        }
        if (empty($data['customer_id']) && empty($data['identifier'])) {
            throw new Exception(__t("Sorry identifier and customerid both cant be empty"));
        }
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_getCartitems (@BUID=:buid,@CustomerID=:customer_id, @identifier=:identifier)}");
        $statement->bindParam(':buid', $this->BUID, PDO::PARAM_INT);
        $statement->bindParam(':customer_id', $data['customer_id'], PDO::PARAM_INT);
        $statement->bindParam(':identifier', $data['identifier'], PDO::PARAM_STR);
        $statement->execute();
        $data['cartitems'] = $statement->fetchAll(PDO::FETCH_ASSOC);
        $statement->closeCursor();
        //dd($data['cartitems'],true);
        $data['priceInfo'] = [];
        foreach ($data['cartitems'] as $key => &$value) {
            $value['baseprice'] = $value['price'];
            $value['price'] = 0;
            if (!empty($value['varient_options'])) {
                $value['varient_options'] = json_decode($value['varient_options'], true);

                if (!empty($value['varient_options'])) {
                    foreach ($value['varient_options'] as $key => $opt) {
                        $value['addedoptions'][$opt['id']] = $opt;
                    }

                    $statementchild = $this->PDO->prepare("{CALL PHP_Ecommerce_GetVarient (@VarientsOptionID=:varientOptions)}");
                    $statementchild->bindParam(':varientOptions', implode(",", array_keys($value['addedoptions'])), PDO::PARAM_STR);
                    $statementchild->execute();
                    $varients = $statementchild->fetchAll(PDO::FETCH_ASSOC);
                    $statementchild->closeCursor();

                    /*if (!empty($varients)) {
                        foreach ($varients as $key => $varientvalue) {
                            $varientvalue['quantity'] = $value['addedoptions'][$varientvalue['id']]['quantity'];
                            $varientvalue['stock'] = $value['Stock'];
                            if ($varientvalue['pricetype'] == 1) {
                                $value['price'] = $value['price'] + (($value['baseprice'] + ($varientvalue['price'])) * (empty($varientvalue['quantity']) ? 1 : $varientvalue['quantity']));
                            } else {
                                $value['price'] = $value['price'] + (($value['baseprice'] - $varientvalue['price']) * (empty($varientvalue['quantity']) ? 1 : $varientvalue['quantity']));
                            }

                            $value['varients'][] = $varientvalue;
                        }
                    } else {
                        $value['price'] = $value['baseprice'] * $value['cartQuantity'];
                    }*/
                    if (!empty($varients)) {
                        $varientPrice = 0;
                        foreach ($varients as $key => $varientvalue) {
                            $varientvalue['quantity'] = $value['addedoptions'][$varientvalue['id']]['quantity'];
                            $varientvalue['stock'] = $value['Stock'];
                            if ($varientvalue['pricetype'] == 1) {
                                $varientPrice = $varientPrice + ($varientvalue['price']  * (empty($varientvalue['quantity']) ? 1 : $varientvalue['quantity']));
                                //$value['price'] = $value['price'] + ( ($value['baseprice'] + ($varientvalue['price'])) * (empty($varientvalue['quantity']) ? 1 : $varientvalue['quantity']));
                            }
                            else {
                                $varientPrice = $varientPrice - ($varientvalue['price']  * (empty($varientvalue['quantity']) ? 1 : $varientvalue['quantity']));
                                //$value['price'] = $value['price'] + ( ($value['baseprice'] - $varientvalue['price']) * (empty($varientvalue['quantity']) ? 1 : $varientvalue['quantity']));
                            }
                            $value['varients'][] = $varientvalue;
                        }
                        $value['price'] = ($value['baseprice'] + $varientPrice) * $value['cartQuantity'];
                        if($value['price']<0){
                            $value['price'] = 0;
                        }
                    }
                    else {
                        $value['price'] = $value['baseprice'] * $value['cartQuantity'];
                    }
                }
            } else {
                // $value['baseprice'] = $value['price'];
                $value['price'] = $value['baseprice'] * $value['cartQuantity'];

            }
            $TaxModel = new TaxModel();

            //if ($key == (count($data['cartitems']) - 1)) {
            $data['priceInfo']['subtotal'] = (empty($data['priceInfo']['subtotal']) ? 0 : $data['priceInfo']['subtotal']) + $value['price'];
            $taxamount = $TaxModel->CalculateTax($data['priceInfo']['subtotal']);
            $data['priceInfo']['Taxes'] = $taxamount;
            $data['priceInfo']['Discounts'] = 0;
            $data['priceInfo']['shipping'] = 0;
            $data['priceInfo']['Gross'] = ($data['priceInfo']['subtotal'] + $taxamount);
            $data['priceInfo']['Gross'] = round($data['priceInfo']['Gross'],2);

            //}
            $taxamountsub = $TaxModel->CalculateTax($value['price']);
            $value['priceinfo'] = [
                'Taxes' => $taxamountsub,
                'Discounts' => 0,
                'shipping' => 0,
                'Gross' => $value['price'] + $taxamountsub,
            ];

            $value['priceinfo']["Gross"] = round($value['priceinfo']["Gross"],2);

            unset($value['varient_options']);
            unset($value['addedoptions']);
        }
        return $data;
    }

    public function getPaymentTypes($BUID)
    {
        if (!empty($BUID)) {
            $this->BUID = $BUID;
        } else {
            $this->CheckAuthenticated();
        }
        if ($this->BUID == null) {
            throw new Exception("Sorry BUID is required");
        }
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_GetPaymentTypes(@BUID=:buid)}");
        $statement->bindParam(':buid', $this->BUID, PDO::PARAM_INT);
        $statement->execute();
        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    public function gePaymentStatusList()
    {
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_PaymentStatusList()}");
        $statement->execute();
        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    public function GetOrderStatusList($BUID)
    {
        if (!empty($BUID)) {
            $this->BUID = $BUID;
        } else {
            $this->CheckAuthenticated();
        }
        if ($this->BUID == null) {
            throw new Exception("Sorry BUID is required");
        }
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_GetOrderStatusList(@buid=:buid)}");
        $statement->bindParam(':buid', $this->BUID, PDO::PARAM_INT);
        $statement->execute();
        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    public function PrepareOrder($data)
    {
        //dd($data,true);
        if (!empty($data['BUID'])) {
            $this->BUID = $data['BUID'];
        } else {
            $this->CheckAuthenticated();
        }
        if ($this->BUID == null) {
            throw new Exception("Sorry BUID is required");
        }
        if (!is_array($data['ordersummary'])) {
            if (($jsonorderar = json_decode($data['ordersummary'], true)) != false) {
                $data['ordersummary'] = $jsonorderar;
            } else {
                throw new Exception(__t("order details is required"));
            }
        }
        if (empty($data['customer_id'])) {
            throw new Exception(__t("Customerid is required"));
        }
        if (empty($data['ordersummary'])) {
            throw new Exception(__t("order details is required"));
        }
        if (empty($data['ordersummary']['priceInfo']) || !isset($data['ordersummary']['priceInfo']['Gross'])) {
            throw new Exception(__t("cant process order as no price info defined"));
        }
        if (empty($data['payment_type'])) {
            throw new Exception(__t("Payment type is required before processing order"));
        }
        /*if (empty($data['customer_billing_add_id'])) {
        throw new Exception(__t("Customer billing Address is required"));
        }
        if (empty($data['customer_shipping_add_id'])) {
        throw new Exception(__t("Customer shipping Address is required"));
        }*/
        if (empty($data['payment_status_id'])) {
            $data['payment_status_id'] = null;
        }

        if (empty($data['order_status_id'])) {
            $data['order_status_id'] = 1;
        }

        if (!empty($data['tableNo'])) {
            $data['ordersummary']['tableNo'] = $data['tableNo'];
        }

        if (!empty($data['seatno'])) {
            $data['ordersummary']['seatno'] = $data['seatno'];
        }

        if (!empty($data['domain'])) {
            $data['ordersummary']['domain'] = $data['domain'];
        }

        if (!empty($data['bu_name'])) {
            $data['ordersummary']['bu_name'] = $data['bu_name'];
        }
        if (empty($data['todaysdate'])) {
            $data['todaysdate'] = date('Y-m-d');
        } else {
            $data['todaysdate'] = date('Y-m-d', strtotime($data['todaysdate']));
        }
        $discount = 0;
        if (!empty($data['COUPONECODE'])) {
            /////CALCULATE DISCOUNT
            $statement1 = $this->PDO->prepare("{CALL PHP_ECOMMERCE_GetCouponsByCODE("
                . "@buid=:buid,"
                . "@code=:code,"
                . ")}");
            $statement1->bindParam(':buid', $this->BUID, PDO::PARAM_INT);
            $statement1->bindParam(':code', $data['COUPONECODE'], PDO::PARAM_STR);
            $statement1->execute();
            $coupon = $statement1->fetchAll(PDO::FETCH_ASSOC);

            if ($coupon[0]["CouponTypeKey"] == "general") {
                if ($coupon[0]["select_rule"] == ">") {
                    if ($data['ordersummary']['priceInfo']['Gross'] > $coupon[0]["discount_on_price"]) {
                        if ($coupon[0]["discount_type"] == 'Percentage') {
                            $discount = ($data['ordersummary']['priceInfo']['Gross'] * $coupon[0]["discount"]) / 100;
                        } else {
                            $discount = $coupon[0]["discount"];
                        }
                    }
                }

                if ($coupon[0]["select_rule"] == ">=") {
                    if ($data['ordersummary']['priceInfo']['Gross'] >= $coupon[0]["discount_on_price"]) {
                        if ($coupon[0]["discount_type"] == 'Percentage') {
                            $discount = ($data['ordersummary']['priceInfo']['Gross'] * $coupon[0]["discount"]) / 100;
                        } else {
                            $discount = $coupon[0]["discount"];
                        }
                    }
                }

                if ($coupon[0]["select_rule"] == "<") {
                    if ($data['ordersummary']['priceInfo']['Gross'] < $coupon[0]["discount_on_price"]) {
                        if ($coupon[0]["discount_type"] == 'Percentage') {
                            $discount = ($data['ordersummary']['priceInfo']['Gross'] * $coupon[0]["discount"]) / 100;
                        } else {
                            $discount = $coupon[0]["discount"];
                        }
                    }
                }

                if ($coupon[0]["select_rule"] == "<=") {
                    if ($data['ordersummary']['priceInfo']['Gross'] <= $coupon[0]["discount_on_price"]) {
                        if ($coupon[0]["discount_type"] == 'Percentage') {
                            $discount = ($data['ordersummary']['priceInfo']['Gross'] * $coupon[0]["discount"]) / 100;
                        } else {
                            $discount = $coupon[0]["discount"];
                        }
                    }
                }

                if ($coupon[0]["select_rule"] == "=") {
                    if ($data['ordersummary']['priceInfo']['Gross'] == $coupon[0]["discount_on_price"]) {
                        if ($coupon[0]["discount_type"] == 'Percentage') {
                            $discount = ($data['ordersummary']['priceInfo']['Gross'] * $coupon[0]["discount"]) / 100;
                        } else {
                            $discount = $coupon[0]["discount"];
                        }
                    }
                }

            }

            if ($coupon[0]["CouponTypeKey"] == "returning") {
                //get total order and orrder value
                $statementO = $this->PDO->prepare("{CALL PHP_ECOMMERCE_GetOrderAmtForCoupon("
                    . "@buid=:buid,"
                    . "@customerid=:customerid,"
                    . ")}");
                $statementO->bindParam(':buid', $this->BUID, PDO::PARAM_INT);
                $statementO->bindParam(':customerid', $data['customer_id'], PDO::PARAM_INT);
                $statementO->execute();
                $OA = $statementO->fetchAll(PDO::FETCH_ASSOC);
                if ($coupon[0]["useTotalOrderValue"] == null) {
                    $coupon[0]["useTotalOrderValue"] = 0;
                }
                if ($coupon[0]["useTotalNoOfOrder"] == null) {
                    $coupon[0]["useTotalNoOfOrder"] = 0;
                }
                if ($OA[0]['TA'] >= $coupon[0]["useTotalNoOfOrder"] && $OA[0]['TAMT'] >= $coupon[0]["useTotalOrderValue"]) {
                    if ($coupon[0]["select_rule"] == ">") {
                        if ($data['ordersummary']['priceInfo']['Gross'] > $coupon[0]["discount_on_price"]) {
                            if ($coupon[0]["discount_type"] == 'Percentage') {
                                $discount = ($data['ordersummary']['priceInfo']['Gross'] * $coupon[0]["discount"]) / 100;
                            } else {
                                $discount = $coupon[0]["discount"];
                            }
                        }
                    }

                    if ($coupon[0]["select_rule"] == ">=") {
                        if ($data['ordersummary']['priceInfo']['Gross'] >= $coupon[0]["discount_on_price"]) {
                            if ($coupon[0]["discount_type"] == 'Percentage') {
                                $discount = ($data['ordersummary']['priceInfo']['Gross'] * $coupon[0]["discount"]) / 100;
                            } else {
                                $discount = $coupon[0]["discount"];
                            }
                        }
                    }

                    if ($coupon[0]["select_rule"] == "<") {
                        if ($data['ordersummary']['priceInfo']['Gross'] < $coupon[0]["discount_on_price"]) {
                            if ($coupon[0]["discount_type"] == 'Percentage') {
                                $discount = ($data['ordersummary']['priceInfo']['Gross'] * $coupon[0]["discount"]) / 100;
                            } else {
                                $discount = $coupon[0]["discount"];
                            }
                        }
                    }

                    if ($coupon[0]["select_rule"] == "<=") {
                        if ($data['ordersummary']['priceInfo']['Gross'] <= $coupon[0]["discount_on_price"]) {
                            if ($coupon[0]["discount_type"] == 'Percentage') {
                                $discount = ($data['ordersummary']['priceInfo']['Gross'] * $coupon[0]["discount"]) / 100;
                            } else {
                                $discount = $coupon[0]["discount"];
                            }
                        }
                    }

                    if ($coupon[0]["select_rule"] == "=") {
                        if ($data['ordersummary']['priceInfo']['Gross'] == $coupon[0]["discount_on_price"]) {
                            if ($coupon[0]["discount_type"] == 'Percentage') {
                                $discount = ($data['ordersummary']['priceInfo']['Gross'] * $coupon[0]["discount"]) / 100;
                            } else {
                                $discount = $coupon[0]["discount"];
                            }
                        }
                    }

                }
            }

            if ($coupon[0]["CouponTypeKey"] == "special" && $data['customer_id'] !== '') {
                // get customer details
                $statementC = $this->PDO->prepare("{CALL PHP_Ecommerce_GetCustomer(@BUID=:id, @CUSTOMERID=:customer_id)}");
                $statementC->bindParam(':customer_id', $data['customer_id'], PDO::PARAM_INT);
                $statementC->bindParam(':id', $this->BUID, PDO::PARAM_INT);
                $statementC->execute();
                $CUS = $statementC->fetchAll(PDO::FETCH_ASSOC);
                if (count($CUS) > 0) {
                    $CUSTOMER_DOB = $CUS[0]["DOB"];
                    $CUSTOMER_MAD = $CUS[0]["marriage_anniversary_date"];
                    $calculateDiscount = 0;
                    if (!empty($CUSTOMER_DOB) && $coupon[0]["SpecialCouponTypeKey"] == 'bday') {
                        $CUSTOMER_DOB = date('Y-m-d', strtotime($CUSTOMER_DOB));
                        if ($CUSTOMER_DOB == $data['todaysdate']) {
                            $calculateDiscount = 1;
                        }
                    }
                    if (!empty($CUSTOMER_MAD) && $coupon[0]["SpecialCouponTypeKey"] == 'maniversary') {
                        $CUSTOMER_MAD = date('Y-m-d', strtotime($CUSTOMER_MAD));
                        if ($CUSTOMER_MAD == $data['todaysdate']) {
                            $calculateDiscount = 1;
                        }
                    }
                    if ($calculateDiscount == 1) {
                        if ($coupon[0]["select_rule"] == ">") {
                            if ($data['Gross'] > $coupon[0]["discount_on_price"]) {
                                if ($coupon[0]["discount_type"] == 'Percentage') {
                                    $discount = ($data['Gross'] * $coupon[0]["discount"]) / 100;
                                } else {
                                    $discount = $coupon[0]["discount"];
                                }
                            }
                        }
                        if ($coupon[0]["select_rule"] == ">=") {
                            if ($data['Gross'] >= $coupon[0]["discount_on_price"]) {
                                if ($coupon[0]["discount_type"] == 'Percentage') {
                                    $discount = ($data['Gross'] * $coupon[0]["discount"]) / 100;
                                } else {
                                    $discount = $coupon[0]["discount"];
                                }
                            }
                        }
                        if ($coupon[0]["select_rule"] == "<") {
                            if ($data['Gross'] < $coupon[0]["discount_on_price"]) {
                                if ($coupon[0]["discount_type"] == 'Percentage') {
                                    $discount = ($data['Gross'] * $coupon[0]["discount"]) / 100;
                                } else {
                                    $discount = $coupon[0]["discount"];
                                }
                            }
                        }
                        if ($coupon[0]["select_rule"] == "<=") {
                            if ($data['Gross'] <= $coupon[0]["discount_on_price"]) {
                                if ($coupon[0]["discount_type"] == 'Percentage') {
                                    $discount = ($data['Gross'] * $coupon[0]["discount"]) / 100;
                                } else {
                                    $discount = $coupon[0]["discount"];
                                }
                            }
                        }
                        if ($coupon[0]["select_rule"] == "=") {
                            if ($data['Gross'] == $coupon[0]["discount_on_price"]) {
                                if ($coupon[0]["discount_type"] == 'Percentage') {
                                    $discount = ($data['Gross'] * $coupon[0]["discount"]) / 100;
                                } else {
                                    $discount = $coupon[0]["discount"];
                                }
                            }
                        }
                    }
                }
            }

            /*if($coupon[0]["CouponTypeKey"] == "special"){
            // get customer details
            $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_GetCustomer(@BUID=:id, @CUSTOMERID=:customer_id)}");
            $statement->bindParam(':customer_id', $data['customer_id'], PDO::PARAM_INT);
            $statement->bindParam(':id', $this->BUID, PDO::PARAM_INT);
            $statement->execute();
            $CUS = $statement->fetch(PDO::FETCH_ASSOC);

            if($coupon[0]["select_rule"] == ">"){
            if($data['ordersummary']['priceInfo']['Gross'] > $coupon[0]["discount_on_price"]){
            if($coupon[0]["discount_type"] == 'Percentage'){
            $discount = ($data['ordersummary']['priceInfo']['Gross'] * $coupon[0]["discount"])/100;
            }else{
            $discount = $coupon[0]["discount"];
            }
            }
            }

            if($coupon[0]["select_rule"] == ">="){
            if($data['ordersummary']['priceInfo']['Gross'] >= $coupon[0]["discount_on_price"]){
            if($coupon[0]["discount_type"] == 'Percentage'){
            $discount = ($data['ordersummary']['priceInfo']['Gross'] * $coupon[0]["discount"])/100;
            }else{
            $discount = $coupon[0]["discount"];
            }
            }
            }

            if($coupon[0]["select_rule"] == "<"){
            if($data['ordersummary']['priceInfo']['Gross'] < $coupon[0]["discount_on_price"]){
            if($coupon[0]["discount_type"] == 'Percentage'){
            $discount = ($data['ordersummary']['priceInfo']['Gross'] * $coupon[0]["discount"])/100;
            }else{
            $discount = $coupon[0]["discount"];
            }
            }
            }

            if($coupon[0]["select_rule"] == "<="){
            if($data['ordersummary']['priceInfo']['Gross'] <= $coupon[0]["discount_on_price"]){
            if($coupon[0]["discount_type"] == 'Percentage'){
            $discount = ($data['ordersummary']['priceInfo']['Gross'] * $coupon[0]["discount"])/100;
            }else{
            $discount = $coupon[0]["discount"];
            }
            }
            }

            if($coupon[0]["select_rule"] == "="){
            if($data['ordersummary']['priceInfo']['Gross'] == $coupon[0]["discount_on_price"]){
            if($coupon[0]["discount_type"] == 'Percentage'){
            $discount = ($data['ordersummary']['priceInfo']['Gross'] * $coupon[0]["discount"])/100;
            }else{
            $discount = $coupon[0]["discount"];
            }
            }
            }

            }*/

            if ($coupon[0]["maximumDiscount"] !== null && $coupon[0]["maximumDiscount"] !== 'null' && $coupon[0]["maximumDiscount"] > 0) {
                if ($discount > $coupon[0]["maximumDiscount"]) {
                    $discount = $coupon[0]["maximumDiscount"];
                }
            }

            if ($discount > $data['ordersummary']['priceInfo']['Gross']) {
                $discount = $data['ordersummary']['priceInfo']['Gross'];
            }

            $data['ordersummary']['COUPONECODE'] = $data['COUPONECODE'];
            $data['ordersummary']['COUPONEDETAILS'] = $coupon[0];

            //////////
        }

        $discount = round($discount,2);

        if (!empty($data['PICKUPLOCATION'])) {
            /////CALCULATE DISCOUNT
            $statement2 = $this->PDO->prepare("{CALL PHP_ECOMMERCE_GetOutlatesByBU("
                . "@buid=:buid,"
                . "@id=:id,"
                . ")}");
            $statement2->bindParam(':buid', $this->BUID, PDO::PARAM_INT);
            $statement2->bindParam(':id', $data['PICKUPLOCATION'], PDO::PARAM_STR);
            $statement2->execute();
            $PICKUPLOCATION = $statement2->fetchAll(PDO::FETCH_ASSOC);

            $data['ordersummary']['PICKUPLOCATION'] = $data['PICKUPLOCATION'];
            $data['ordersummary']['PICKUPLOCATIONDETAILS'] = $PICKUPLOCATION[0];
            $data['ordersummary']['PICKUPTIME'] = $data['PICKUPTIME'];

            //////////
        }

        if (!empty($data['PREORDERTABLEID'])) {
            $statement31 = $this->PDO->prepare("{ CALL PHP_Ecommerce_GetPreOrderSettings(@buid=:buid)}");
            $statement31->bindParam(':buid', $this->BUID);

            $statement31->execute();
            $PreOrderSettings = $statement31->fetchAll(PDO::FETCH_ASSOC);

            $statement41 = $this->PDO->prepare("{ CALL PHP_Ecommerce_GetPreOrderTables(@buid=:buid,@id=:id)}");
            $statement41->bindParam(':buid', $this->BUID);
            $statement41->bindParam(':id', $data['PREORDERTABLEID']);

            $statement41->execute();
            $TD = $statement41->fetchAll(PDO::FETCH_ASSOC);

            $PREORDERTIMEFROM = strtotime($data['PREORDERTIME'] . ":00");
            $PREORDERTIMETO = date("H:i:s", strtotime('+' . $PreOrderSettings[0]["defaulyreservationlength"] . ' minutes', $PREORDERTIMEFROM));
            $PREORDERTIMEFROM = date("H:i:s", $PREORDERTIMEFROM);
            $PREORDARRAY = array(
                "PREORDERTABLEID" => $data['PREORDERTABLEID'],
                "PREORDERTABLENAME" => $TD[0]["name"],
                "PREORDERDATE" => $data['PREORDERDATE'],
                "PREORDERTIMEFROM" => $PREORDERTIMEFROM,
                "PREORDERTIMETO" => $PREORDERTIMETO,
                "PREORDERTOTALGUEST" => $data['PREORDERTOTALGUEST'],
            );

            $data['ordersummary']['PREORDERDETAILS'] = $PREORDARRAY;
        }

        $data['ordersummary']['priceInfo']['Discounts'] = $discount;
        $data['ordersummary']['priceInfo']['Gross'] = $data['ordersummary']['priceInfo']['Gross'] - $discount;
        $data['ordersummary']['priceInfo']['Gross'] = round($data['ordersummary']['priceInfo']['Gross'],2);

        $cartids = [];
        $productids = [];
        foreach ($data['ordersummary']['cartitems'] as $key => $value) {
            $cartids[] = $value['cartid'];
            $productids[] = $value['id'];
        }

        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_PrepareOrder("
            . "@customer_id=:customer_id,"
            . "@buid=:buid,"
            . "@ordersummary=:ordersummary,"
            . "@grandtotal=:grandtotal,"
            . "@payment_type=:payment_type,"
            . "@payment_status_id=:payment_status_id,"
            . "@order_status_id=:order_status_id,"
            . "@customer_billing_add_id=:customer_billing_add_id,"
            . "@customer_shipping_add_id=:customer_shipping_add_id,"
            . "@OrderID=:OrderID,"
            . "@ID=:ID,"
            . "@CartIDS=:cartids,"
            . "@productids=:productids,"
            . "@couponCode =:couponCode,"
            . "@discount =:discount,"
            . "@pickupLocation =:pickupLocation,"
            . "@pickupTime =:pickupTime,"
            . "@deliveryType =:deliveryType"
            . ")}");
        $ordersummary = json_encode($data['ordersummary']);
        if (empty($data['id'])) {
            $data['id'] = null;
        }
        $orderid = null;
        $statement->bindParam(':customer_id', $data['customer_id'], PDO::PARAM_INT);
        $statement->bindParam(':buid', $this->BUID, PDO::PARAM_INT);
        $statement->bindParam(':ordersummary', $ordersummary, PDO::PARAM_STR);
        $statement->bindParam(':grandtotal', $data['ordersummary']['priceInfo']['Gross']);
        $statement->bindParam(':payment_type', $data['payment_type'], PDO::PARAM_INT);
        $statement->bindParam(':payment_status_id', $data['payment_status_id'], PDO::PARAM_INT);
        $statement->bindParam(':order_status_id', $data['order_status_id'], PDO::PARAM_INT);
        $statement->bindParam(':customer_billing_add_id', $data['customer_billing_add_id'], PDO::PARAM_INT);
        $statement->bindParam(':customer_shipping_add_id', $data['customer_shipping_add_id'], PDO::PARAM_INT);
        $statement->bindParam(':OrderID', $data['id'], PDO::PARAM_INT);
        $statement->bindParam(':ID', $orderid, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 250);
        if (!empty($cartids)) {
            $statement->bindParam(':cartids', $cartids = implode(",", $cartids), PDO::PARAM_STR);
        } else {
            $statement->bindParam(':cartids', $cartids = null, PDO::PARAM_INT);
        }

        if (!empty($productids)) {
            $statement->bindParam(':productids', $productids = implode(",", $productids), PDO::PARAM_STR);
        } else {
            $statement->bindParam(':productids', $productids = null, PDO::PARAM_INT);
        }

        if (!empty($data['COUPONECODE'])) {
            $statement->bindParam(':couponCode', $data['COUPONECODE'], PDO::PARAM_STR);
        } else {
            $statement->bindParam(':couponCode', $data['COUPONECODE'] = null, PDO::PARAM_INT);
        }
        $statement->bindParam(':discount', $discount, PDO::PARAM_STR);
        if (!empty($data['PICKUPLOCATION'])) {
            $statement->bindParam(':pickupLocation', $data['PICKUPLOCATION'], PDO::PARAM_INT);
        } else {
            $statement->bindParam(':pickupLocation', $data['PICKUPLOCATION'] = null, PDO::PARAM_INT);
        }
        //$statement->bindParam(':pickupLocation', $data['PICKUPLOCATION'], PDO::PARAM_INT);
        if (!empty($data['PICKUPTIME'])) {
            $statement->bindParam(':pickupTime', $data['PICKUPTIME'], PDO::PARAM_STR);
        } else {
            $statement->bindParam(':pickupTime', $data['PICKUPTIME'] = null, PDO::PARAM_INT);
        }
        if (!empty($data['deliveryType'])) {
            $statement->bindParam(':deliveryType', $data['deliveryType'], PDO::PARAM_STR);
        } else {
            $statement->bindParam(':deliveryType', $data['deliveryType'] = null, PDO::PARAM_INT);
        }

        $statement->execute();
        if (empty($orderid)) {
            throw new Exception(__t("Order not generated"));
        }
        if (!empty($orderid)) {
            $EmailModel = new EmailModel();
            $emailData = $EmailModel->prepareEmailData($orderid, 'newOrder', $this->BUID);

            if (!empty($data['PREORDERTABLEID'])) {
                //SAVE PREORDER DETAILS
                $statement21 = $this->PDO->prepare("{CALL PHP_Ecommerce_AddPreOrderBooking("
                    . "@buid=:buid,"
                    . "@orderid=:orderid,"
                    . "@tableid=:tableid,"
                    . "@bookingdate=:bookingdate,"
                    . "@bookingtimeFrom=:bookingtimeFrom,"
                    . "@bookingtimeTo=:bookingtimeTo,"
                    . "@noofguest=:noofguest,"
                    . "@status=:status,"
                    . "@PREORDERBOOKINGID=:PREORDERBOOKINGID"
                    . ")}");
                $PREORDERBOOKINGID = null;
                $statement21->bindParam(':buid', $this->BUID, PDO::PARAM_INT);
                $statement21->bindParam(':orderid', $orderid, PDO::PARAM_INT);
                $statement21->bindParam(':tableid', $data['PREORDERTABLEID'], PDO::PARAM_INT);
                $statement21->bindParam(':bookingdate', $data['PREORDERDATE'], PDO::PARAM_STR);
                $statement21->bindParam(':bookingtimeFrom', $PREORDERTIMEFROM, PDO::PARAM_STR);
                $statement21->bindParam(':bookingtimeTo', $PREORDERTIMETO, PDO::PARAM_STR);
                $statement21->bindParam(':noofguest', $data['PREORDERTOTALGUEST'], PDO::PARAM_INT);
                $statement21->bindParam(':status', $status = 1, PDO::PARAM_INT);
                $statement21->bindParam(':PREORDERBOOKINGID', $PREORDERBOOKINGID, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 250);
                $statement21->execute();
            }
        }

        return ['orderid' => $orderid, 'PREORDERBOOKINGID' => $PREORDERBOOKINGID];
    }

    public function ChangeorderStatus($orderids = [], $statusid)
    {

        if (empty($orderids)) {
            throw new Exception("Invalid orders selected");
        } else {
            $orderidsOld = $orderids;
            $orderids = implode(",", $orderids);
        }

        if (empty($statusid)) {
            throw new Exception("Invalid status provided");
        }
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ChangeOrderStatus("
            . "@orderIDs=:orderids,"
            . "@StatusID=:statusid"
            . ")}");
        $statement->bindParam(':orderids', $orderids, PDO::PARAM_STR);
        $statement->bindParam(':statusid', $statusid, PDO::PARAM_INT);
        $update = $statement->execute();
        if (!$update) {
            throw new Exception("Sorry order status not changed");
        } else {
            $EmailModel = new EmailModel();
            foreach ($orderidsOld as $val) {
                $emailData = $EmailModel->prepareEmailData($val, 'statusChange', $this->BUID);
            }
        }

        return $update;
    }

    public function getAllorders($start = 0, $limit = 20, $data, $BUID)
    {
        // dd($data,true);
        if (!empty($BUID)) {
            $this->BUID = $BUID;
        } else {
            $this->CheckAuthenticated();
        }
        if ($this->BUID == null) {
            throw new Exception("Sorry BUID is required");
        }
        if (empty($data['customer_id'])) {
            $data['customer_id'] = null;
        }
        if (empty($data['id'])) {
            $data['id'] = null;
        }
        if (empty($start)) {
            $start = 0;
        }
        if (empty($limit) || $limit > 20) {
            $limit = 20;
        }
        if (empty($data['startdate'])) {
            $data['startdate'] = null;
        } else {
            $data['startdate'] = date('Y-m-d 00:00:00', strtotime(str_replace(['/', '.'], '-', $data['startdate'])));
        }
        if (empty($data['enddate'])) {
            $data['enddate'] = null;
        } else {
            $data['enddate'] = date('Y-m-d 23:59:59', strtotime(str_replace(['/', '.'], '-', $data['enddate'])));
        }
        if (empty($data['identifier'])) {
            $data['identifier'] = null;
        } else {
            $data['identifier'] = str_replace('&nbsp;', '', $data['identifier']);
        }

        if (empty($data['deliveryType'])) {
            $data['deliveryType'] = null;
        }

        //dd($data, true);
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_GetOrders("
            . "@customer_id=:customer_id,"
            . "@order_id=:order_id,"
            . "@buid=:buid,"
            . "@start=:start,"
            . "@Limit=:limit,"
            . "@startdate=:startdate,"
            . "@enddate=:enddate,"
            . "@identifier=:identifier,"
            . "@deliveryType=:deliveryType"
            . ")}");
        $statement->bindParam(':customer_id', $data['customer_id'], PDO::PARAM_INT);
        $statement->bindParam(':order_id', $data['id'], PDO::PARAM_INT);
        $statement->bindParam(':buid', $this->BUID, PDO::PARAM_INT);
        $statement->bindParam(':start', $start, PDO::PARAM_INT);
        $statement->bindParam(':limit', $limit, PDO::PARAM_INT);
        if (!empty($data['startdate'])) {
            $statement->bindParam(':startdate', $data['startdate'], PDO::PARAM_STR);
        } else {
            $statement->bindParam(':startdate', $data['startdate'], PDO::PARAM_INT);
        }
        if (!empty($data['enddate'])) {
            $statement->bindParam(':enddate', $data['enddate'], PDO::PARAM_STR);
        } else {
            $statement->bindParam(':enddate', $data['enddate'], PDO::PARAM_INT);
        }

        $statement->bindParam(':identifier', $data['identifier'], PDO::PARAM_STR);

        if (!empty($data['deliveryType'])) {
            $statement->bindParam(':deliveryType', $data['deliveryType'], PDO::PARAM_STR);
        } else {
            $statement->bindParam(':deliveryType', $data['deliveryType'], PDO::PARAM_INT);
        }
        $statement->execute();
        $data = [];
        do {
            $rows = $statement->fetchAll(PDO::FETCH_ASSOC);

            if ($rows) {
                if (isset($rows[0]['Total'])) {
                    $data['Paginations'] = [
                        'Total' => $rows[0]['Total'],
                        'Start' => $start,
                        'limit' => $limit,
                    ];
                } else {
                    $data['orders'] = array_map(array($this, "json_decode_summary"), $rows);
                    foreach ($data['orders'] as $key => $val) {
                        $orderLink = 'https://' . $_SERVER['HTTP_HOST'] . '/order-details/' . $val['id'];
                        $data['orders'][$key]['qrurl'] = 'http://api.qrserver.com/v1/create-qr-code/?data=' . $orderLink . '&size=512x512&qzone=1';
                        if ($val['PaymentStatusID'] == 2) {
                            foreach ($val['ordersummary']['cartitems'] as $keyoi => $valoi) {
                                //if($valoi['is_virtual']==1){
                                // select doc here
                                $statementD = $this->PDO->prepare("{CALL PHP_Ecommerce_GetProductDocuments("
                                    . "@Productid=:Productid"
                                    . ")}");
                                $statementD->bindParam(':Productid', $valoi['id'], PDO::PARAM_INT);
                                $statementD->execute();
                                $rowsD = $statementD->fetchAll(PDO::FETCH_ASSOC);
                                $data['orders'][$key]['ordersummary']['cartitems'][$keyoi]['DOCUMENTS'] = $rowsD;
                                //}

                            }
                        }
                    }

                }
            }
        } while ($statement->nextRowset());
        if (empty($data['orders'])) {
            $data['orders'] = [];
        }
        return $data;
    }

    public function json_decode_summary(&$array)
    {
        $array['ordersummary'] = json_decode($array['ordersummary'], true);
        $array['FormatedDateTime'] = date('l jS \of F Y h:i:s A', strtotime($array['datetime']));
        $array['datetime'] = date('Y-m-d H:i:s', strtotime($array['datetime']));
        $array['orderidentifier'] = "&nbsp;" . $array['orderidentifier'];
        if (!empty($array['CustomerUpdateDate'])) {
            $array['CustomerUpdateDate'] = date('Y-m-d H:i:s', strtotime($array['CustomerUpdateDate']));
        }
        if (!empty($array['CustomerCreationDate'])) {
            $array['CustomerCreationDate'] = date('Y-m-d H:i:s', strtotime($array['CustomerCreationDate']));
        }
        return $array;
    }

    public function updateOrder($data)
    {
        if (!empty($data['BUID'])) {
            $this->BUID = $data['BUID'];
        } else {
            $this->CheckAuthenticated();
        }
        if ($this->BUID == null) {
            throw new Exception("Sorry BUID is required");
        }
        if (empty($data['order_id'])) {
            throw new Exception(__t("Invalid order id specified"));
        }
        if (empty($data['shipperadress'])) {
            throw new Exception(__t("Invalid shipper"));
        }
        if (empty($data['trackingid'])) {
            throw new Exception(__t("Invalid trackingid"));
        }

        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_UpdateShipment("
            . "@shipperaddress=:shipperadress,"
            . "@tracking_id=:trackingid,"
            . "@orderid=:order_id,"
            . "@BUID=:buid"
            . ")}");
        $statement->bindParam(':shipperadress', trim(strip_tags($data['shipperadress'])), PDO::PARAM_STR);
        $statement->bindParam(':trackingid', trim(strip_tags($data['trackingid'])), PDO::PARAM_STR);
        $statement->bindParam(':order_id', $data['order_id'], PDO::PARAM_STR);
        $statement->bindParam(':buid', $this->BUID, PDO::PARAM_STR);
        $result = $statement->execute();
        if (!$result) {
            throw new Exception(__t("Sorry shipment not updated"));
        }
        if ($result) {
            $EmailModel = new EmailModel();
            $emailData = $EmailModel->prepareEmailData($data['order_id'], 'shipment', $this->BUID);

        }

        return $result;
    }

    public function updateProductstock($data)
    {

        if (empty($data['product_id'])) {
            throw new Exception(__t("Sorry invalid product id"));
        }
        if (empty($data['AppendStock'])) {
            $data['AppendStock'] = 0;
        } else {
            $data['AppendStock'] = 1;
        }

        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_UpdateProductStock(
                                            @ProductID=:product_id,
                                            @stockupdate=:stock,
                                            @Update=:updateornew)
                                            }");

        $statement->bindParam(':product_id', $data['product_id'], PDO::PARAM_INT);
        $statement->bindParam(':stock', $data['stock'], PDO::PARAM_INT);
        $statement->bindParam(':updateornew', $data['AppendStock'], PDO::PARAM_INT);
        if (!$statement->execute()) {
            throw new Exception(__t("Sorry stock not updated"));
        }
        return true;
    }

    public function PrepareSingleOrder($data)
    {

        $customerModel = new CustomersModel();

        if (count($data['paymentDetails']['details']['billingAddress']['addressLine']) > 1) {
            if ($data['paymentDetails']['details']['billingAddress']['addressLine'][0] != '') {
                $addr1 = $data['paymentDetails']['details']['billingAddress']['addressLine'][0];
            } else {
                $addr1 = $data['paymentDetails']['details']['billingAddress']['addressLine'][1];
            }
        } else {
            $addr1 = $data['paymentDetails']['details']['billingAddress']['addressLine'][0];
        }
        //dd($addr, true);
        $checkBillingAddress = $customerModel->checkAddress($data['cartdeldata']['customerid'], 'billing', $addr1);
        if (empty($checkBillingAddress['id'])) {
            $baddr = [
                'customer_id' => $data['cartdeldata']['customerid'],
                'address' => $addr1,
                'name' => $data['paymentDetails']['details']['billingAddress']['recipient'],
                'phoneno' => $data['paymentDetails']['details']['billingAddress']['phone'],
                'pincode' => $data['paymentDetails']['details']['billingAddress']['postalCode'],
                'city' => $data['paymentDetails']['details']['billingAddress']['city'],
                'state' => $data['paymentDetails']['details']['billingAddress']['region'],
                'landmark' => $data['paymentDetails']['details']['billingAddress']['dependentLocality'],
                'alrternetPH' => null,
                'address_type' => 'billing',
            ];
            $addRess = $customerModel->AddCustomerAddress($baddr);
            $checkBillingAddress['id'] = $addRess['AddressID'];
        }

        if (count($data['paymentDetails']['shippingAddress']['addressLine']) > 1) {
            if ($data['paymentDetails']['shippingAddress']['addressLine'][0] != '') {
                $addr = $data['paymentDetails']['shippingAddress']['addressLine'][0];
            } else {
                $addr = $data['paymentDetails']['shippingAddress']['addressLine'][1];
            }
        } else {
            $addr = $data['paymentDetails']['shippingAddress']['addressLine'][0];
        }
        //dd(count($data['paymentDetails']['shippingAddress']['addressLine']), true);
        $checkshippingAddress = $customerModel->checkAddress($data['cartdeldata']['customerid'], 'shipping', $addr);
        if (empty($checkshippingAddress['id'])) {
            $addRess = $customerModel->AddCustomerAddress(
                [
                    'customer_id' => $data['cartdeldata']['customerid'],
                    'address' => $addr,
                    'name' => $data['paymentDetails']['shippingAddress']['recipient'],
                    'phoneno' => $data['paymentDetails']['shippingAddress']['phone'],
                    'pincode' => $data['paymentDetails']['shippingAddress']['postalCode'],
                    'city' => $data['paymentDetails']['shippingAddress']['city'],
                    'state' => $data['paymentDetails']['shippingAddress']['region'],
                    'landmark' => $data['paymentDetails']['shippingAddress']['dependentLocality'],
                    'alrternetPH' => null,
                    'address_type' => 'shipping',
                ]
            );

            $checkshippingAddress['id'] = $addRess['AddressID'];
        }

        $othersinCart = array_filter($data['cartdetails'], function ($item) use ($data) {
            if ((int) $item['id'] != (int) $data['item']['id']) {

                $this->deleteFromCart([
                    'product_id' => $item['id'],
                    'customer_id' => $data['cartdeldata']['customerid'],
                    'identifier' => $data['cartdeldata']['identifier'],
                ]);
            }
            return ((int) $item['id'] != (int) $data['item']['id']);
        });
        $cartitemprepare = $this->getCartitems([
            'customer_id' => $data['cartdeldata']['customerid'],
            'identifier' => $data['cartdeldata']['identifier'],
        ]);
        $orderSummary = [
            'ordersummary' => $cartitemprepare,
            'payment_type' => $data['paymenttype'][0]['id'],
            'domain' => $data['domain'],
            'bu_name' => $data['bu_name'],
            'customer_id' => $data['cartdeldata']['customerid'],
            'customer_billing_add_id' => $checkBillingAddress['id'],
            'customer_shipping_add_id' => $checkshippingAddress['id'],
        ];
        //return $orderSummary;

        $PaymentModel = new PaymentModel();
        $token = $PaymentModel->createstripeToken([
            'card' => $data['paymentDetails']['details']['cardNumber'],
            'exp_month' => $data['paymentDetails']['details']['expiryMonth'],
            'exp_year' => $data['paymentDetails']['details']['expiryYear'],
            'cvc' => $data['paymentDetails']['details']['cardSecurityCode'],
        ]);

        $order = $this->PrepareOrder($orderSummary);
        $orderdetails = $this->getAllorders(0, 1, [
            'id' => $order['orderid'],
        ]);
        $description = "Order refrence #" . $orderdetails['orders'][0]['orderidentifier'];

        if ($PaymentModel->StripePayment([
            'stripeToken' => $token,
            'name' => $data['paymentDetails']['payerName'],
            'phonenumber' => $data['paymentDetails']['payerPhone'],
            'amount' => $cartitemprepare['priceInfo']['Gross'],
            'currency' => $cartitemprepare['cartitems'][0]['currency_code'],
        ], $description)) {

            $PaymentModel->ChangePaymentStatus([$order['orderid']], 2);
            $this->ChangeorderStatus([$order['orderid']], 2);
            if (!empty($othersinCart)) {
                // create cart for these itesm
                $cartitemstoad = [
                    'identifier' => $data['cartdeldata']['identifier'],
                    'customer_id' => $data['cartdeldata']['customerid'],
                ];
                foreach ($othersinCart as $oldcartitem) {
                    $cartitemstoad['cartItems'][] = [
                        'product_id' => $oldcartitem['id'],
                        'quantity' => $oldcartitem['cartQuantity'],
                    ];
                }
                $this->AddToCart($cartitemstoad);

            }
        }
        return ['orderid' => $orderdetails['orders'][0]['orderidentifier']];
    }

    public function processorderWithStripe($data)
    {
        $cartItems = $this->getCartitems([
            'identifier' => $data['identifier'],
            'customer_id' => $data['customer_id'],
        ]);
        if (empty($cartItems)) {
            throw new Exception("Sorry no products in your cart");
        }
        if (is_string($data["extraoptions"]) && ($extraoptions = json_decode($data["extraoptions"], true)) != false) {
            $data["extraoptions"] = $extraoptions;
        }
        //$data['extraoptions'] = json_decode($data['extraoptions'], true);
        $orderSummary = [
            'ordersummary' => $cartItems,
            'payment_type' => 3,
            'domain' => $data['domain'],
            'bu_name' => $data['bu_name'],
            'customer_id' => $data['customer_id'],
            'customer_billing_add_id' => $data['billing_address'],
            'customer_shipping_add_id' => $data['shipping_address'],
            'tableNo' => $data['extraoptions']['tableNo'],
            'seatno' => $data['extraoptions']['seatno'],
            'domain' => $data['extraoptions']['domain'],
            'bu_name' => $data['extraoptions']['bu_name'],
            'COUPONECODE' => $data['extraoptions']['COUPONECODE'],
            'PICKUPLOCATION' => $data['extraoptions']['PICKUPLOCATION'],
            'PICKUPTIME' => $data['extraoptions']['PICKUPTIME'],
            'PREORDERTABLEID' => $data['extraoptions']['PREORDERTABLEID'],
            'PREORDERDATE' => $data['extraoptions']['PREORDERDATE'],
            'PREORDERTIME' => $data['extraoptions']['PREORDERTIME'],
            'PREORDERTOTALGUEST' => $data['extraoptions']['PREORDERTOTALGUEST'],
            'deliveryType' => $data['extraoptions']['deliveryType'],

        ];
        $orderDetails = $this->PrepareOrder($orderSummary);
        if ($orderDetails['orderid']) {
            $order = $this->getAllorders(0, 1, [
                'id' => $orderDetails['orderid'],
            ]);

            $description = "Order refrence #" . $order['orders'][0]['orderidentifier'];
            $PaymentModel = new PaymentModel();
            if ($PaymentModel->StripePayment([
                'stripeToken' => $data['stripeToken'],
                'name' => $data['name'],
                'phonenumber' => $data['phonenumber'],
                'amount' => $cartItems['priceInfo']['Gross'],
                'currency' => $cartItems['cartitems'][0]['currency_code'],
            ], $description)) {
                $PaymentModel->ChangePaymentStatus([$order['orderid']], 2);
                $this->ChangeorderStatus([$orderDetails['orderid']], 2);
                return $order['orders'][0]['orderidentifier'];
            } else {
                throw new Exception(__t("Sorry there was some issue with the payment please try again later"));
            }

        }

    }

}
