<?php

use Aws\S3\Exception\S3Exception;
use Aws\S3\S3Client;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CategoryModel
 *
 * @author dipankar
 */
require_once MODEL_PATH . DS . 'App.php';

class CategoryModel extends AppModel
{

    //put your code here
    private $MapTable = 'Product_Category_Map';
    private $Table = 'categories';

    public function __construct($callAuth = false)
    {
        parent::__construct();
        //$this->CheckAuthenticated();
    }

    public function mapCategoryproducts($product_id, $category_id)
    {
        if (empty($product_id)) {
            throw new Exception("Sorry product is required to map category");
        }
        $this->_checkvalidCategory($category_id);
        $checkstatement = $this->PDO->prepare("INSERT INTO {$this->DbAlias}{$this->MapTable} (category_id,product_id) VALUES (:categoryid,:product_id)");
        $checkstatement->bindParam(':categoryid', $category_id, PDO::PARAM_INT);
        $checkstatement->bindParam(':product_id', $product_id, PDO::PARAM_INT);
        $checkstatement->execute();
    }

    private function _checkvalidCategory($categoryid)
    {
        if (empty($categoryid)) {
            throw new Exception("Sorry category is required to map category");
        }
        $checkstatement = $this->PDO->prepare("SELECT count(*) as cateogrycount from {$this->DbAlias}{$this->Table} where id=:categoryid");
        $checkstatement->bindParam(':categoryid', $categoryid);
        $checkstatement->execute();
        $data = $checkstatement->fetch(PDO::FETCH_ASSOC);
        if ($data['cateogrycount'] == 0) {
            throw new Exception("Sorry invalid category id");
        }
        return true;
    }

    public function addCategory($data, $file = [])
    {
        if (!empty($data['BUID'])) {
            $this->BUID = $data['BUID'];
        } else {
            $this->CheckAuthenticated();
        }
        if ($this->BUID == null) {
            throw new Exception("Sorry BUID is required");
        }
        if (empty($data['name'])) {
            throw new Exception("Sorry category name is required");
        }
        //$this->__checkDuplicate($data['name']);
        $imagename = null;
        if (!empty($file)) {
            $imagename = $this->uploadImage($file);
        }
        if (empty($file) && !empty($data['image_url']) && getimagesize($data['image_url']) != false) {
            $imagename = $data['image_url'];
        }

        if (empty($data['name'])) {
            throw new Exception(__t("Sorry Category name is required"));
        }
        $addStatement = $this->PDO->prepare("{CALL PHP_Ecommerce_AddCategory (@BUID=:buid,@Categoryname=:categoryname,@Image=:image,@ParentID=:parent_id,@ISACTIVE=:IS_Active,@slug=:slug,@displayorder=:displayorder,@ID=:ID, @CATEGORYID=:CategoryID)}");
        //$addStatement = $this->PDO->prepare("INSERT INTO {$this->DbAlias}{$this->Table} (name,buid,image,parent_id) VALUES(:categoryname,:buid,:image,:parent_id)");
        $addStatement->bindParam(':categoryname', $data['name'], PDO::PARAM_STR);
        $addStatement->bindParam(':buid', $this->BUID, PDO::PARAM_INT);
        if (empty($imagename)) {
            $addStatement->bindParam(':image', $imagename = null, PDO::PARAM_INT);
        } else {
            $addStatement->bindParam(':image', $imagename, PDO::PARAM_STR);
        }
        if (empty($data['parent_id']) || !is_numeric($data['parent_id'])) {
            $addStatement->bindParam(':parent_id', $data['parent_id'] = 0, PDO::PARAM_INT);
        } else {
            $addStatement->bindParam(':parent_id', $data['parent_id'], PDO::PARAM_INT);
        }
        $addStatement->bindParam(':slug', $data['slug'] = null, PDO::PARAM_STR);
        if (empty($data['displayorder'])) {
            $addStatement->bindParam(':displayorder', $displayorder = 1, PDO::PARAM_INT);
        } else {
            $addStatement->bindParam(':displayorder', $displayorder = $data['displayorder'], PDO::PARAM_INT);
        }
        if (empty($data['id'])) {
            $addStatement->bindParam(':ID', $id = null, PDO::PARAM_INT);
        } else {
            $addStatement->bindParam(':ID', $id = $data['id'], PDO::PARAM_INT);
        }
        $addStatement->bindParam(':CategoryID', $CategoryID, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000);
        //dd($data['status'], true);
        if (!empty($data['status']) || is_numeric($data['status'])) {
            $isActive = $data['status'];
        } else {
            $isActive = 1;
        }
        $addStatement->bindParam(':IS_Active', $isActive, PDO::PARAM_INT);
        $addStatement->execute();
        return [
            'id' => empty($data['id']) ? $CategoryID : $data['id'],
            'name' => $data['name'],
            'image' => $imagename,
            'parent_id' => $data['parent_id'],
            'msg' => empty($data['id']) ? __t("Category added succesfully") : __t("Category updated succesfully"),
        ];
    }

    public function __checkDuplicate($categoryname)
    {
        if (empty($categoryname)) {
            throw new Exception("Sorry category name is required");
        }
        $checkStatement = $this->PDO->prepare("Select count(*) as totalcats from {$this->DbAlias}{$this->Table} where name=:categoryname and buid=:buid");
        $checkStatement->bindParam(':categoryname', $categoryname, PDO::PARAM_STR);
        $checkStatement->bindParam(':buid', $this->BUID, PDO::PARAM_INT);
        $checkStatement->execute();
        $data = $checkStatement->fetch(PDO::FETCH_ASSOC);
        if ($data['totalcats'] != 0) {
            throw new Exception("Sorry this category already exists");
        }
        return true;
    }

    private function uploadImage($FILES)
    {

        $max_size = file_upload_max_size();
        $client = new S3Client([
            'version' => 'latest',
            'region' => AWS_REGION,
            'credentials' => [
                'key' => AWS_ACCESS_KEY,
                'secret' => AWS_SECRET_KEY,
            ],
        ]);
        $valid_formats = ["jpg", "png", "gif", "jpeg"];
        $FileXml = '';

        if ($FILES['Images']['error'] == 0) {
            $name = $FILES['Images']['name'];
            $size = $FILES['Images']['size'];
            $tmp = $FILES['Images']['tmp_name'];
            $type = $FILES['Images']['type'];

            if ($size > $max_size) {
                throw new Exception(__t("Sorry image " . $name . " size is bigger than accepted size"));
            }
            $ext = strtolower(getExtension($name));
            if (!in_array($ext, $valid_formats)) {
                throw new Exception(__t("Sorry image " . $name . " is not valid ,we accept " . implode(",", $valid_formats)));
            }
            $image_name_actual = time() . "." . $ext;
            try {
                $Result = $client->putObject(array(
                    'Bucket' => AWS_BUCKET_NAME,
                    'Key' => 'BU' . $this->BUID . '/' . $image_name_actual,
                    'SourceFile' => $tmp,
                    'StorageClass' => 'REDUCED_REDUNDANCY',
                    'ACL' => 'public-read',
                    'ContentType' => $type,
                    'Expires' => date('Y-m-d H:i:s', strtotime("2 years")),
                ));
            } catch (S3Exception $exc) {
                throw new Exception("Aws S3 Error : " . $exc->getAwsErrorMessage());
            }
            return $Result['ObjectURL'];
        }

        return false;
    }

    public function GetCategories($data)
    {
        //dd($data,true);
        if (!empty($data['BUID'])) {
            $this->BUID = $data['BUID'];
        } else {
            $this->CheckAuthenticated();
        }
        if ($this->BUID == null) {
            throw new Exception("Sorry BUID is required");
        }
        if (empty($data['category_slug'])) {
            $data['category_slug'] = '';
        }

        $statement = $this->PDO->prepare("{ CALL PHP_Ecommerce_GetCategories(@BUID=:buid,@ParentCategory=:parent_id, @Likename = :LikeName, @id=:id, @slug=:slug)}");
        $statement->bindParam(':buid', $this->BUID);
        if (!empty($data['parent_id']) or $data['parent_id'] == 0) {
            $statement->bindParam(':parent_id', $data['parent_id'], PDO::PARAM_INT);
        } else {
            $statement->bindParam(':parent_id', $data['parent_id'] = null, PDO::PARAM_INT);
        }
        if (!empty($data['name'])) {
            $statement->bindParam(':LikeName', $data['name'], PDO::PARAM_STR);
        } else {
            $statement->bindParam(':LikeName', $data['name'] = null, PDO::PARAM_INT);
        }

        if (!empty($data['category_id'])) {
            $statement->bindParam(':id', $data['category_id'], PDO::PARAM_INT);
        } else {
            $statement->bindParam(':id', $data['category_id'] = null, PDO::PARAM_INT);
        }
        if (!empty($data['category_slug'])) {
            $statement->bindParam(':slug', $data['category_slug'], PDO::PARAM_STR);
        } else {
            $statement->bindParam(':slug', $data['category_slug'] = null, PDO::PARAM_INT);
        }

        $statement->execute();
        $rows = $statement->fetchAll(PDO::FETCH_ASSOC);
        return [
            'parent_id' => $data['parent_id'],
            'categories' => $rows,
        ];
    }

    public function DeleteCategory($id, $BUID)
    {
        if (!empty($BUID)) {
            $this->BUID = $BUID;
        } else {
            $this->CheckAuthenticated();
        }
        if ($this->BUID == null) {
            throw new Exception("Sorry BUID is required");
        }
        if (empty($id)) {
            throw new Exception(__t("Sorry category id not provided"));
        }

        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_CategoryDelete(@BUID=:buid,@CategoryID=:categoryid)}");
        $statement->bindParam(':buid', $this->BUID, PDO::PARAM_INT);
        $statement->bindParam(':categoryid', $id, PDO::PARAM_INT);
        $statement->execute();
        return [
            'id' => $id,
        ];
    }

    public function GetCategoriesTree($BUID, $parent_id, $data)
    {
        if (!empty($BUID)) {
            $this->BUID = $BUID;
        } else {
            $this->CheckAuthenticated();
        }

        $statement = $this->PDO->prepare("{ CALL PHP_Ecommerce_GetCategories(@BUID=:buid,@ParentCategory=:parent_id, @Likename = :LikeName, @id=:id, @slug=:slug)}");
        $statement->bindParam(':buid', $this->BUID);
        $statement->bindParam(':parent_id', $parent_id, PDO::PARAM_INT);

        if (!empty($data['name'])) {
            $statement->bindParam(':LikeName', $data['name'], PDO::PARAM_STR);
        } else {
            $statement->bindParam(':LikeName', $data['name'] = null, PDO::PARAM_INT);
        }

        if (!empty($data['category_id'])) {
            $statement->bindParam(':id', $data['category_id'], PDO::PARAM_INT);
        } else {
            $statement->bindParam(':id', $data['category_id'] = null, PDO::PARAM_INT);
        }

        if (!empty($data['slug'])) {
            $statement->bindParam(':slug', $data['slug'], PDO::PARAM_STR);
        } else {
            $statement->bindParam(':slug', $data['slug'] = null, PDO::PARAM_INT);
        }

        $statement->execute();
        $rows = $statement->fetchAll(PDO::FETCH_ASSOC);

        if (count($rows) > 0) {
            foreach ($rows as $key => $value) {
                # code...
                $rows[$key]['chieldren'] = $this->GetCategoriesTree($this->BUID, $value['id'], $data);
                $tp = 0;
                foreach ($rows[$key]['chieldren'] as $key1 => $value1) {
                    $tp = $tp + $value1["TotalProducts"];
                }
                $rows[$key]['TotalProducts'] = $value['total_products'] + $tp;
            }
        }else{
            //$rows['TotalProducts'] = $value['total_products'];
        }
        return $rows;
    }

    public function UpdateCategoryOrder($data, $BUID)
    {
        if (!empty($BUID)) {
            $this->BUID = $BUID;
        } else {
            $this->CheckAuthenticated();
        }
        //dd($data);
        $FileXml = '';
        $FileXml = $this->prepareXMLForUpdateOrder($data["data"], 0);
        if (strlen(trim($FileXml)) > 0) {
            $FileXml = '<data>' . $FileXml . '</data>';
        }
        //dd($FileXml,true);
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_UpdateCategoryOrder(@data=:data)}");
        $statement->bindParam(':data', $FileXml, PDO::PARAM_STR);
        $statement->execute();

        //dd($FileXml,true);
        return [
            'data' => $data,
        ];
    }

    public function prepareXMLForUpdateOrder($data, $parent)
    {
        $FileXml = '';
        foreach ($data as $key => $value) {
            $FileXml .= '<categories>';
            $FileXml .= '<categoryid>' . $value["id"] . '</categoryid>';
            $FileXml .= '<parentid>' . $parent . '</parentid>';
            $FileXml .= '<dispalyorder>' . ($key + 1) . '</dispalyorder>';
            $FileXml .= '</categories>';
            if (count($value["chieldren"]) > 0) {
                $FileXml .= $this->prepareXMLForUpdateOrder($value["chieldren"], $value["id"]);
            }
        }

        return $FileXml;
    }

}
