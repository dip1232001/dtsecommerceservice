<?php
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ContactsCardsModel
 *
 * @author Abhijit Manna
 */

require_once MODEL_PATH . DS . 'App.php';
//require_once MODEL_PATH . DS . 'EmailModel.php';

class ContactCardsModel extends AppModel {

    //put your code here

    public function __construct($callAuth = false) {
        parent::__construct($callAuth);
    }

    public function GetProfession() {
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_GetProfession()}");
        $statement->execute();
        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }
    
    public function GetIndustries($data) {
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_GetIndustries()}");
        $statement->execute();
        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }
    
    public function AddPerson($data, $file = []) {
        
        if($data['BUID']== "" || $data['BUID']==0){
            throw new Exception(__t("BUID 0"));
        }
       
        if($data['first_name']==""){
            throw new Exception(__t("Enter First Name"));
        }
        if($data['last_name']==""){
            throw new Exception(__t("Enter Last Name"));
        }
        if($data['phone']==""){
            throw new Exception(__t("Enter Phone No"));
        }
        if($data['email']==""){
            throw new Exception(__t("Enter Email"));
        }

        $referer = null;
        if($data['referer']!==""){
            $ref = json_decode(base64_decode($data['referer']));
            if($data['BUID']==$ref['buid']){
                $referer = $ref['referer'];
            }            
        }
        
        /*if($data['customerid'] != "" && $data['customerid'] != null && $data['customerid'] != 0)
        {
            if($data['industry_id']==""){
                throw new Exception(__t("Enter Industry Id"));
            }
            if($data['profession_id']==""){
                throw new Exception(__t("Enter Profession Id"));
            }
            if($data['addressline_1']==""){
                throw new Exception(__t("Enter Addressline 1"));
            }
            if($data['city']==""){
                throw new Exception(__t("Enter City"));
            }
            if($data['state']==""){
                throw new Exception(__t("Enter State"));
            }
            if($data['country']==""){
                throw new Exception(__t("Enter Country"));
            }
            if($data['zip']==""){
                throw new Exception(__t("Enter Zip"));
            }
            if($data['bio']==""){
                throw new Exception(__t("Enter Bio"));
            }
        }*/


            if($data['industry_id']==""){
                $data['industry_id'] = null;
            }
            if($data['profession_id']==""){
                $data['profession_id'] = null;
            }
            if($data['addressline_1']==""){
                $data['addressline_1'] = null;
            }
            if($data['city']==""){
                $data['city'] = null;
            }
            if($data['state']==""){
                $data['state'] = null;
            }
            if($data['country']==""){
                $data['country'] = null;
            }
            if($data['zip']==""){
                $data['zip'] = null;
            }
            if($data['bio']==""){
                $data['bio'] = null;
            }
        
        if($data['customerid']=='' || $data['customerid']=='null' || $data['customerid']==0){
            $data['customerid']=null;
            if($data['subscription_id'] == "")
                throw new Exception(__t("Enter subscription id."));
            if($data['billingCycle'] == "")
                throw new Exception(__t("Enter billing cycle."));
            if($data['price'] == "")
                throw new Exception(__t("Enter price.")); 
        }

        if($data['person_id']=='' || $data['person_id']=='null'){
            $data['person_id']=null;
        }

        $imagename = null;        
        if (!empty($file['Images']['name'])) {
            $imagename = $this->uploadImage($file,$data['BUID']);
        }

        if (empty($file['Images']['name']) && $data['oldimage'] != '') {
            $imagename = $data['oldimage'];
        }
        
        $password = $this->random_password(10);
        
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_AddPrerson(
            @BUID=:BUID,
            @customerid=:customerid,
            @person_id=:person_id,
            @first_name=:first_name,
            @last_name=:last_name,
            @phone=:phone,
            @email=:email,
            @website=:website,
            @industry_id=:industry_id,
            @profession_id=:profession_id,
            @addressline_1=:addressline_1,
            @addressline_2=:addressline_2,
            @city=:city,
            @state=:state,
            @country=:country,
            @zip=:zip,
            @fb_profile=:fb_profile,
            @twitter_profile=:twitter_profile,
            @g_profile=:g_profile,
            @in_profile=:in_profile,
            @instagram_profile=:instagram_profile,
            @resume_link=:resume_link,
            @bio=:bio,
            @qr_code=:qr_code,
            @allow_save=:allow_save,
            @password=:password,
            @company=:company,
            @image=:image,
            @subscription_id =:subscription_id,
            @price =:price,
            @billingCycle =:billingCycle,
            @CountryCode =:CountryCode,
            @referer =:referer 
        )}");
        
        $statement->bindParam(':BUID', $data['BUID'], PDO::PARAM_INT);
        $statement->bindParam(':customerid', $data['customerid'], PDO::PARAM_INT);
        $statement->bindParam(':person_id', $data['person_id'], PDO::PARAM_INT);
        $statement->bindParam(':first_name', $data['first_name'], PDO::PARAM_STR);
        $statement->bindParam(':last_name', $data['last_name'], PDO::PARAM_STR);
        $statement->bindParam(':phone', $data['phone'], PDO::PARAM_STR);
        $statement->bindParam(':email', $data['email'], PDO::PARAM_STR);
        $statement->bindParam(':website', $data['website'], PDO::PARAM_STR);
        $statement->bindParam(':industry_id', $data['industry_id'], PDO::PARAM_INT);
        $statement->bindParam(':profession_id', $data['profession_id'], PDO::PARAM_INT);
        $statement->bindParam(':addressline_1', $data['addressline_1'], PDO::PARAM_STR);
        $statement->bindParam(':addressline_2', $data['addressline_2'], PDO::PARAM_STR);
        $statement->bindParam(':city', $data['city'], PDO::PARAM_STR);
        $statement->bindParam(':state', $data['state'], PDO::PARAM_STR);
        $statement->bindParam(':country', $data['country'], PDO::PARAM_STR);
        $statement->bindParam(':zip', $data['zip'], PDO::PARAM_STR);
        $statement->bindParam(':fb_profile', $data['fb_profile'], PDO::PARAM_STR);
        $statement->bindParam(':twitter_profile', $data['twitter_profile'], PDO::PARAM_STR);
        $statement->bindParam(':g_profile', $data['g_profile'], PDO::PARAM_STR);
        $statement->bindParam(':in_profile', $data['in_profile'], PDO::PARAM_STR);
        $statement->bindParam(':instagram_profile', $data['instagram_profile'], PDO::PARAM_STR);
        $statement->bindParam(':resume_link', $data['resume_link'], PDO::PARAM_STR);
        $statement->bindParam(':password', $password, PDO::PARAM_STR);
        $statement->bindParam(':bio', $data['bio'], PDO::PARAM_STR);
        $statement->bindParam(':company', $data['company'], PDO::PARAM_STR);

        if (!empty($imagename)) {
            $statement->bindParam(':image', $imagename, PDO::PARAM_STR);
        } else {
            $statement->bindParam(':image', $imagename = null, PDO::PARAM_INT);
        }
        if (empty($data['qr_code'])) {
            $statement->bindParam(':qr_code', $data['qr_code'] = 0, PDO::PARAM_INT);
        }
        else{
            $statement->bindParam(':qr_code', $data['qr_code'], PDO::PARAM_INT);
        }
        if (empty($data['allow_save'])) {
            $statement->bindParam(':allow_save', $data['allow_save'] = 0, PDO::PARAM_INT);
        }
        else{
            $statement->bindParam(':allow_save', $data['allow_save'], PDO::PARAM_INT);
        }
        
        $statement->bindParam(':subscription_id', $data['subscription_id'], PDO::PARAM_INT);
        $statement->bindParam(':price', $data['price'], PDO::PARAM_STR);
        $statement->bindParam(':billingCycle', $data['billingCycle'], PDO::PARAM_INT);
        
        if (!empty($data['CountryCode'])) {
            $statement->bindParam(':CountryCode', $data['CountryCode'], PDO::PARAM_STR);
        } else {
            $statement->bindParam(':CountryCode', $data['CountryCode'] = NULL, PDO::PARAM_INT);
        }

        $statement->bindParam(':referer', $referer, PDO::PARAM_INT);
        
        if (!$statement->execute()) {
            throw new Exception(__t("Sorry person not added"));
        }

        $data1 = []; 
        if($data['customerid']===null){       
            do {            
                $rows = $statement->fetchAll(PDO::FETCH_ASSOC);                       
                switch ($x) {                
                    case 0:                         
                    
                        $data1['customerid'] = $rows;
                        //$emailModel = new EmailModel();
                        $emailData = array();
                        $emailData['first_name'] = $data['first_name'];
                        $emailData['last_name'] = $data['last_name'];
                        $emailData['email'] = $data['email'];   
                        $emailData['phone'] = $data['phone'];
                        $emailData['loginurl'] = $data['loginurl'];
                        $emailData['password'] = $password;

                        $this->newContactCardCustomerEmail($emailData);
                        break;                
                    case 1:                    
                        $data1['Personsid'] = $rows;                    
                        break;            
                }            
                $x++;                 
            } while ($statement->nextRowset());
        }
        else{
            $data1['Personsid']=$statement->fetchAll(PDO::FETCH_ASSOC);  
        }
        $statement->closeCursor();
        return $data1;
    }
    
    function random_password( $length = 8 ) {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
        $password = substr( str_shuffle( $chars ), 0, $length );
        return $password;
    }

    public function GetPrerson($data) {
        $buid = $data['BUID'];
        $customerid = $data['customerid'];
        $person_id = $data['person_id'];
        $start = $data['start'];
        $limit = $data['limit'];
        $searchstring = $data['searchstring'];
        $isDefault = $data['isDefault'];

        if($buid== ""){
            throw new Exception(__t("BUID 0"));
        }
		if($customerid==""){
            $customerid=null;
        }
        if($person_id==""){
            $person_id=null;
        }
        if($isDefault=="" || $isDefault==null){
            $isDefault=0;
        }
        if($start==""){
            $start=0;
        }
        if($limit==""){
            $limit=20;
        }
        if($searchstring==""){
            $searchstring=null;
        }
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_GetPrerson(
            @BUID=:buid,
            @customerid=:customerid,
            @person_id=:person_id,
            @Start=:start,
            @Limit=:limit,
            @SearchString=:searchstring,
            @isDefault=:isDefault
        )}");

        $statement->bindParam(':buid', $buid, PDO::PARAM_INT);
        $statement->bindParam(':customerid', $customerid, PDO::PARAM_INT);
        $statement->bindParam(':person_id', $person_id, PDO::PARAM_INT);
        $statement->bindParam(':isDefault', $isDefault, PDO::PARAM_INT);
        $statement->bindParam(':start', $start, PDO::PARAM_INT);
        $statement->bindParam(':limit', $limit, PDO::PARAM_INT);
        $statement->bindParam(':searchstring', $searchstring, PDO::PARAM_STR);
        $statement->execute();

        $x = 0;        
        $data = []; 
        if($person_id===null && $isDefault==0){       
            do {            
                $rows = $statement->fetchAll(PDO::FETCH_ASSOC);                       
                switch ($x) {                
                    case 0:                    
                        $data['Pagination'] = [                        
                                            'total' => (int) $rows[0]['totalPersons'],                        
                                            'start' => $start,                        
                                            'limit' => $limit                    
                                        ];                    
                        break; 

                    case 1:                    
                        $data['Persons'] = $rows;                    
                        break; 

                }            
                $x++;                 
            } while ($statement->nextRowset());
        }
        else{

            do {            
                $rows = $statement->fetchAll(PDO::FETCH_ASSOC);      
                                 
                switch ($x) {                
                    case 0:                    
                        $data['Persons'] = $rows;                    
                        break;                
                    case 1:                    
                        $data['Reviews'] = $rows;                    
                        break;            
                }            
                $x++;                 
            } while ($statement->nextRowset());
        }
        return $data;
    }

    public function CustomerLogin($data) {

        if($data['username']==""){
            throw new Exception(__t("Enter Login String"));
        }
        if($data['password']==""){
            throw new Exception(__t("Enter password"));
        }
        if($data['BUID']==""){
            throw new Exception(__t("Enter BUID"));
        }

        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_CustomerLogin("
            . "@loginstring=:username,"
            . "@password=:password,"
            . "@buid=:buid"
            . ")}");

        $statement->bindParam(':buid', $data['BUID'], PDO::PARAM_INT);
        $statement->bindParam(':username', $data['username'], PDO::PARAM_STR);
        $statement->bindParam(':password', $data['password'], PDO::PARAM_STR);
        $statement->execute();        
        $result = $statement->fetch(PDO::FETCH_ASSOC);
        $data1['CustomerID'] = (int) $result['CustomerID'];
        if (empty($result['CustomerID'])) {
            throw new Exception("invalid login credentials provided");
        }else{

            $newStatementV = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_UpdateCustomerVisit("
                . "@customerid=:customerid,"
                . "@BUID=:buid"                
                . ")}");
            $newStatementV->bindParam(':buid', $data['BUID'], PDO::PARAM_INT);
            $newStatementV->bindParam(':customerid', $result['CustomerID'], PDO::PARAM_INT);
            $newStatementV->execute();
            
            $newStatement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_GetSubscriptionByCustomer("
                . "@BUID=:buid,"
                . "@customerid=:customerid"
                . ")}");
            $newStatement->bindParam(':buid', $data['BUID'], PDO::PARAM_INT);
            $newStatement->bindParam(':customerid', $result['CustomerID'], PDO::PARAM_INT);
            $newStatement->execute();

            $subscription = $newStatement->fetch(PDO::FETCH_ASSOC);

            $data1['subscriptionid'] = $subscription['subscriptionid'];
            $data1['price'] = $subscription['price'];
            $data1['billingCycle'] = $subscription['billingCycle'];
            $data1['expirydate'] = $subscription['expirydate'];
            $data1['IsCancel'] = $subscription['IsCancel'];

            $data1['name'] = $subscription['name'];
            $data1['canShare'] = $subscription['canShare'];
            $data1['canAddPerson'] = $subscription['canAddPerson'];
            $data1['noOfPerson'] = $subscription['noOfPerson'];

            $dateToday = strtotime(date('Y-m-d'));
            $expDate   = strtotime($subscription['expirydate']);
            if($dateToday > $expDate)
                $expire = true;
            else
                $expire = false;

            $data1['expire'] = $expire;    
        }

        return $data1;
    }

    public function newContactCardCustomerEmail($data){
        $emailhtml='<!DOCTYPE html>
        <html>
        <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <title>My Contact Card</title>
        </head>
        <body style="padding: 0; margin: 0;">
            <table width="650" cellpadding="0" cellspacing="0" align="center">
                <thead>
                    <tr>
                        <th style="padding-top: 10px; padding-bottom: 10px; padding-left: 10px; padding-right: 10px; text-align: left; border-bottom: 1px solid #f0632b;"><img src="https://ecom.myteamconnector.com/img/ccemaillogo.jpg" style="display: block;"></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td align="left" valign="top">
                            <p style="padding-top: 30px; padding-bottom: 25px; margin: 0; font-family: Arial; font-size: 16px; line-height: 22px; color: #696969;">Welcome '.$data['first_name'].' '.$data['last_name'].'.</p>
                            <p style="padding-bottom: 25px; margin: 0; font-family: Arial; font-size: 16px; line-height: 22px; color: #696969;">Thanks for joining My Contact Card. Create appealing contact cards to share globally and make a mark.</p>
                            <p style="padding-bottom: 25px; margin: 0; font-family: Arial; font-size: 16px; line-height: 22px; color: #696969;">Please login to the app using these credentials -</p>
                            <p style="font-family: Arial; font-size: 16px; font-weight: bold; color: #087abd; padding-bottom: 10px; margin: 0;">Username: '.$data['email'].' OR '. $data['phone'].'</p>
                            <p style="font-family: Arial; font-size: 16px; font-weight: bold; color: #087abd; padding: 0; margin: 0;">Password: '.$data['password'].' </p>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top" style="padding-top:40px; height: 60px;">
                            <a href="'.$data['loginurl'].'" style="background-color: #f0632b; color: #ffffff; text-decoration: none; font-family: Arial; font-size: 14px; line-height: 24px; padding-top:15px; padding-bottom: 15px; padding-left: 60px; padding-right: 60px;">Click here</a>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="middle" style="background-color: #eaeaea; padding-top: 10px; padding-bottom: 10px;"><p style="font-family: Arial; font-size: 12px; line-height: 16px; margin: 0; padding: 0; color: #3892c7;">&copy; COPYRIGHT 2018 MY CONTACT CARD. ALL RIGHTS RESERVED.</p></td>
                    </tr>
                </tbody>
            </table>
        </body>
        </html>';

        $emailData = array();                        
        $emailData['email_subject'] = "Joining announcement.";            
        $emailData['email_body'] = "Email:".$data["email"].", Phone: ".$data["phone"].", Password: ".$data["password"];            
        $emailData['email_body'] = $emailhtml;
        $emailData['to_email'] = $data['email'];

        $CURLOPT_POSTFIELDS = array(
            'To' => $emailData['to_email'],
            'Subject' => $emailData['email_subject'],
            'MAilBody' => $emailData['email_body']
        );

        $response = callApi(API_URL_ENDPOINT . '/webapi/Mail/SendMail', 'POST', $CURLOPT_POSTFIELDS);
    }

    public function AddFolder($data) {
        if($data['BUID']==""){
            throw new Exception(__t("Enter BUID."));
        }
        if($data['customerid']==""){
            throw new Exception(__t("Enter Customer Id."));
        }
        if($data['foldername']==""){
            throw new Exception(__t("Enter Folder Name."));
        }
        if($data['ParentID']==""){
            $data['ParentID']=null;
        }
        if($data['ID']==""){
            $data['ID']=null;
        }

        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_AddFolder(
            @BUID=:buid,
            @customerid=:customerid,
            @Foldername=:foldername,
            @ParentID=:parentid,
            @ID=:id,
            @FOLDERID=:FOLDERID
        )}");

        $statement->bindParam(':buid', $data['BUID'], PDO::PARAM_INT);
        $statement->bindParam(':customerid', $data['customerid'], PDO::PARAM_INT);
        $statement->bindParam(':foldername', $data['foldername'], PDO::PARAM_STR);
        $statement->bindParam(':parentid', $data['ParentID'], PDO::PARAM_INT);
        $statement->bindParam(':id', $data['ID'], PDO::PARAM_INT);
        $statement->bindParam(':FOLDERID', $FOLDERID, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 400);
        
        if (!$statement->execute()) {
            throw new Exception(__t("Sorry folder not added"));
        }

        $statement->closeCursor();
        return ['FOLDERID' => $FOLDERID];
    }
   
    public function AddCards($data) {

        if($data['BUID']==""){
            throw new Exception(__t("Enter BUID."));
        }
        if($data['customerid']==""){
            throw new Exception(__t("Enter Customer Id."));
        }
        if($data['personid']==""){
            throw new Exception(__t("Enter Person Id."));
        }
        if($data['folderid']==""){
            $data['folderid'] = null;
        }
        if($data['cardname']==""){
            throw new Exception(__t("Enter Card Name."));
        }
        if($data['carddata']==""){
            throw new Exception(__t("Enter Card Data."));
        }        
        if($data['isPublic']=="" || $data['isPublic']=='Null' || $data['isPublic']==Null){
            $data['isPublic']=0;
        }
        if($data['ID']==""){
            $data['ID']=null;
        }

        $ext = 'png';

        try{
            $imgdata = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data['Images']));
            $ext = explode(";base64",$data['canvasImage']);
                $ext = $ext[0];
                $ext = str_replace("data:image/", '',$ext);
        }catch (Exception $exc) {
            throw new Exception(__t("Wrong image type."));
        }
        
        $imageUrl = $this->createImage($imgdata,$data['BUID'],$ext);

        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_AddCards(
            @BUID=:buid,
            @customerid=:customerid,
            @personid=:personid,
            @folderid=:folderid,
            @cardname=:cardname,
            @carddata=:carddata,
            @cardimage=:cardimage,
            @isPublic=:isPublic,
            @ID=:id,
            @CARDID=:cardid
        )}");

        $statement->bindParam(':buid', $data['BUID'], PDO::PARAM_INT);
        $statement->bindParam(':customerid', $data['customerid'], PDO::PARAM_INT);
        $statement->bindParam(':personid', $data['personid'], PDO::PARAM_INT);
        $statement->bindParam(':folderid', $data['folderid'], PDO::PARAM_INT);
        $statement->bindParam(':cardname', $data['cardname'], PDO::PARAM_STR);
        $statement->bindParam(':carddata', $data['carddata'], PDO::PARAM_STR);
        
        if(empty($imageUrl)) {
            $statement->bindParam(':cardimage', $imageUrl = null, PDO::PARAM_INT);
        }else{
            $statement->bindParam(':cardimage', $imageUrl, PDO::PARAM_STR);
        }

        $statement->bindParam(':isPublic', $data['isPublic'], PDO::PARAM_INT);
        $statement->bindParam(':id', $data['ID'], PDO::PARAM_INT);
        
        $statement->bindParam(':cardid', $CARDID, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 400);
        if (!$statement->execute()) {
            throw new Exception(__t("Sorry card not added"));
        }

        $statement->closeCursor();
        return ['CARDID' => $CARDID];
    }
    
    public function deletePerson($data) {
        if($data['personid']==""){
            throw new Exception(__t("Enter Person Id."));
        }
        if($data['BUID']==""){
            throw new Exception(__t("Enter BUID."));
        }
        if($data['customerid']==""){
            throw new Exception(__t("Enter Customer Id."));
        }                

        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_deletePerson(
            @personid=:personid,
            @BUID=:buid,
            @customerid=:customerid
        )}");

        $statement->bindParam(':personid', $data['personid'], PDO::PARAM_INT);
        $statement->bindParam(':buid', $data['BUID'], PDO::PARAM_INT);
        $statement->bindParam(':customerid', $data['customerid'], PDO::PARAM_INT);
        
        if (!$statement->execute()) {
            throw new Exception(__t("Person not deleted successfully."));
        }
        $statement->closeCursor();
    }

    public function deleteFolder($data) {
        if($data['id']==""){
            throw new Exception(__t("Enter Id."));
        }
        if($data['BUID']==""){
            throw new Exception(__t("Enter BUID."));
        }
        if($data['customerid']==""){
            throw new Exception(__t("Enter Customer Id."));
        }                

        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_deleteFolder(
            @id=:id,
            @BUID=:buid,
            @customerid=:customerid
        )}");

        $statement->bindParam(':id', $data['id'], PDO::PARAM_INT);
        $statement->bindParam(':buid', $data['BUID'], PDO::PARAM_INT);
        $statement->bindParam(':customerid', $data['customerid'], PDO::PARAM_INT);
        
        if (!$statement->execute()) {
            throw new Exception(__t("Folder not deleted successfully."));
        }

        $statement->closeCursor();

    }
    
    public function deleteCard($data) {
        if($data['id']==""){
            throw new Exception(__t("Enter Id."));
        }
        if($data['BUID']==""){
            throw new Exception(__t("Enter BUID."));
        }
        if($data['customerid']==""){
            throw new Exception(__t("Enter Customer Id."));
        } 
        if($data['personid']==""){
            throw new Exception(__t("Enter Person Id."));
        }               

        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_deleteCard(
            @id=:id,
            @BUID=:buid,
            @customerid=:customerid,
            @personid=:personid
        )}");

        $statement->bindParam(':id', $data['id'], PDO::PARAM_INT);
        $statement->bindParam(':buid', $data['BUID'], PDO::PARAM_INT);
        $statement->bindParam(':customerid', $data['customerid'], PDO::PARAM_INT);
        $statement->bindParam(':personid', $data['personid'], PDO::PARAM_INT);
        
        if (!$statement->execute()) {
            throw new Exception(__t("Card not deleted successfully."));
        }
        $statement->closeCursor();
    }
    
    public function getFolder($data) {
        if($data['BUID']==0){
            throw new Exception(__t("BUID 0"));
        }
        if($data['BUID']==""){
            throw new Exception(__t("Enter BUID"));
        }
        if($data['customerid']=="" || $data['customerid']<=0){
            throw new Exception(__t("Enter Customer Id"));
        }
        if($data['id']==""){
            $data['id']=null;
        }
        if($data['parentid']==""){
            $data['parentid']=null;
        }        
        if($data['start']==''){
            $data['start']=0;
        }
        if($data['limit']==''){
            $data['limit']=20;
        }
        if($data['searchstring']==""){
            $data['searchstring']=null;
        }
        if($data['cardid']==""){
            $data['cardid']=null;
        }

        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_GetFolder(
            @BUID=:buid,
            @customerid=:customerid,
            @ID=:id,
            @parentid=:parentid,
            @Start=:start,
            @Limit=:limit,
            @SearchString=:searchstring,
            @cardid=:cardid
        )}");

        $statement->bindParam(':buid', $data['BUID'], PDO::PARAM_INT);
        $statement->bindParam(':customerid', $data['customerid'], PDO::PARAM_INT);
        $statement->bindParam(':id', $data['id'], PDO::PARAM_INT);
        $statement->bindParam(':parentid', $data['parentid'], PDO::PARAM_INT);
        $statement->bindParam(':start', $data['start'], PDO::PARAM_INT);
        $statement->bindParam(':limit', $data['limit'], PDO::PARAM_INT);
        $statement->bindParam(':searchstring', $data['searchstring'], PDO::PARAM_STR);
        $statement->bindParam(':cardid', $data['cardid'], PDO::PARAM_INT);

        $statement->execute();

        $x = 0;        
        $data1 = []; 
        if($data['id']===null){   
            do {            
                $rows = $statement->fetchAll(PDO::FETCH_ASSOC);                       
                switch ($x) {                
                    case 0:                    
                        $data1['Pagination'] = [                        
                                            'total' => (int) $rows[0]['totalPersons'],                        
                                            'start' => $data['start'],                        
                                            'limit' => $data['limit']                    
                                        ];                    
                        break;                
                    case 1:                    
                        $data1['Folders'] = $rows;  
                        
                        break;
                }            
                $x++;                 
            } while ($statement->nextRowset());
        }
        else{
            $data1=$statement->fetchAll(PDO::FETCH_ASSOC);  
        }

        if($data['id']===null){ 
            $j=0;
            foreach($data1['Folders'] as $val){
                if($val["customerid"]!=$data['customerid']){
                    $statementSFD = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_GetShareFolderD(
                        @BUID=:buid,
                        @customerid=:customerid,
                        @ID=:id
                    )}");
            
                    $statementSFD->bindParam(':buid', $data['BUID'], PDO::PARAM_INT);
                    $statementSFD->bindParam(':customerid', $data['customerid'], PDO::PARAM_INT);
                    $statementSFD->bindParam(':id', $val['id'], PDO::PARAM_INT);
            
                    $statementSFD->execute();

                    $rowsSFD = $statementSFD->fetchAll(PDO::FETCH_ASSOC);
                    $data1['Folders'][$y]["SharedBY"] = $rowsSFD;
                }
                $j++;
            }
        }

        return $data1;
    }

    public function addFoldersCardsMap($data) {

        if($data['BUID']==""){
            throw new Exception(__t("Enter BUID."));
        }
        if($data['customerid']==""){
            throw new Exception(__t("Enter Customer Id."));
        } 
        if($data['cardid']==""){
            throw new Exception(__t("Enter Card Id."));
        }

        if(count($data['folder'])<=0){
            throw new Exception(__t("Please select a foldar to add this card."));
        }

        $xmlData = "<data>";
        foreach($data['folder'] as $id){
            $xmlData.= "<folders><id>".$id."</id></folders>";
        }
        $xmlData.= "</data>";

        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_AddFoldersCardsMap(
            @BUID=:buid,
            @customerid=:customerid,
            @folder=:folder,
            @cardid=:cardid
        )}");
        
        $statement->bindParam(':buid', $data['BUID'], PDO::PARAM_INT);
        $statement->bindParam(':customerid', $data['customerid'], PDO::PARAM_INT);
        $statement->bindParam(':folder', $xmlData, PDO::PARAM_STR);
        $statement->bindParam(':cardid', $data['cardid'], PDO::PARAM_INT);
        
        if (!$statement->execute()) {
            throw new Exception(__t("Folder cards mapped successfully."));
        }

        $statement->closeCursor();
    }

    public function getCards($data) {

        if($data['BUID']==""){
            throw new Exception(__t("Enter BUID"));
        }
        if($data['customerid']==""){
            throw new Exception(__t("Enter Customer Id"));
        }
        if($data['person_id']==""){
            $data['person_id']=null;
        }
        if($data['folderid']==""){
            $data['folderid']=null;
        }
        if($data['cardid']==""){
            $data['cardid']=null;
        }        
        if($data['start']==''){
            $data['start']=0;
        }
        if($data['limit']==''){
            $data['limit']=20;
        }
        if($data['searchstring']==""){
            $data['searchstring']=null;
        }
        if($data['SearchStringIndustry']==""){
            $data['SearchStringIndustry']=null;
        }
        if($data['SearchStringProfession']==""){
            $data['SearchStringProfession']=null;
        }
        if($data['isLike']==""){
            $data['isLike']=null;
        }

        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_GetCards(
            @BUID=:buid,
            @customerid=:customerid,
            @person_id=:person_id,
            @folderid=:folderid,
            @cardid=:cardid,
            @Start=:start,
            @Limit=:limit,
            @SearchString=:searchstring,
            @SearchStringIndustry=:SearchStringIndustry,
            @SearchStringProfession=:SearchStringProfession,
            @isLike=:isLike
        )}");

        $statement->bindParam(':buid', $data['BUID'], PDO::PARAM_INT);
        $statement->bindParam(':customerid', $data['customerid'], PDO::PARAM_INT);
        $statement->bindParam(':person_id', $data['person_id'], PDO::PARAM_INT);
        $statement->bindParam(':folderid', $data['folderid'], PDO::PARAM_INT);
        $statement->bindParam(':cardid', $data['cardid'], PDO::PARAM_INT);
        $statement->bindParam(':start', $data['start'], PDO::PARAM_INT);
        $statement->bindParam(':limit', $data['limit'], PDO::PARAM_INT);
        $statement->bindParam(':searchstring', $data['searchstring'], PDO::PARAM_STR);
        $statement->bindParam(':SearchStringIndustry', $data['SearchStringIndustry'], PDO::PARAM_STR);
        $statement->bindParam(':SearchStringProfession', $data['SearchStringProfession'], PDO::PARAM_STR);
        $statement->bindParam(':isLike', $data['isLike'], PDO::PARAM_STR);       

        $statement->execute();
        $x = 0;        
        $data1 = []; 
        if($data['cardid']===null){   
            if($data['folderid']===null){
                do {            
                    $rows = $statement->fetchAll(PDO::FETCH_ASSOC);      
                                     
                    switch ($x) {                
                        case 0:                    
                            $data1['Pagination'] = [                        
                                                'total' => (int) $rows[0]['totalCards'],                        
                                                'start' => $data['start'],                        
                                                'limit' => $data['limit']                    
                                            ];                    
                            break;                
                        case 1:                    
                            $data1['Cards'] = $rows;                    
                            break;            
                    }            
                    $x++;                 
                } while ($statement->nextRowset());
            }
            else{
                do {            
                    $rows = $statement->fetchAll(PDO::FETCH_ASSOC);      
                                     
                    switch ($x) {  
                        case 0:                    
                            $data1['foldar'] = $rows;                    
                            break;               
                        case 1:                    
                            $data1['Pagination'] = [                        
                                                'total' => (int) $rows[0]['totalCards'],                        
                                                'start' => $data['start'],                        
                                                'limit' => $data['limit']                    
                                            ];                    
                            break;                
                        case 2:                                            
                            $data1['Cards'] = $rows;                    
                            break;            
                    }            
                    $x++;                 
                } while ($statement->nextRowset());
            }
        }
        else{
            do {            
                $rows = $statement->fetchAll(PDO::FETCH_ASSOC); 
                switch ($x) {                
                    case 0:                    
                        $data1['Cards'] = $rows;                    
                        break;                
                    case 1:                    
                        $data1['Reviews'] = $rows;                    
                        break;            
                }            
                $x++;                 
            } while ($statement->nextRowset());
        }

        return $data1;
    }

    public function uploadProfileImage($data, $file = []) {
        if($data['BUID']==""){
            throw new Exception(__t("Enter BUID."));
        }

        if($data['customerid']==""){
            throw new Exception(__t("Enter Customer Id."));
        }

        if($data['profileid']==""){
            throw new Exception(__t("Enter Profile Id."));
        }

        $imagename = null;        
        if (!empty($file)) {
            $imagename = $this->uploadImage($file,$data['BUID']);
        }
        if (empty($file) && !empty($data['image_url'])) {
            $imagename = $data['image_url'];
        }

        $statement->bindParam(':buid', $data['BUID'], PDO::PARAM_INT);
        $statement->bindParam(':customerid', $data['customerid'], PDO::PARAM_INT);
        $statement->bindParam(':profileid', $data['profileid'], PDO::PARAM_INT);

        if (empty($imagename)) {
            $statement->bindParam(':image', $imagename = null, PDO::PARAM_INT);
        } else {
            $statement->bindParam(':image', $imagename, PDO::PARAM_STR);
        }

        if (!$statement->execute()) {
            throw new Exception(__t("Sorry profile image not uploaded."));
        }

        $statement->closeCursor();
    }

    public function uploadPersonImage($data, $file = []) {
        if($data['BUID']==""){
            throw new Exception(__t("Enter BUID."));
        }
        if($data['customerid']==""){
            throw new Exception(__t("Enter Customer Id."));
        }
        if($data['personid']==""){
            throw new Exception(__t("Enter Person Id."));
        }
        $imagename = null;        
        if (!empty($file)) {
            $imagename = $this->uploadImage($file,$data['BUID']);
        }
        if (empty($file) && !empty($data['image_url'])) {
            $imagename = $data['image_url'];
        }
        if($data['RawImages']!=""){
            $ext = explode(";base64",$data['canvasImage']);
                $ext = $ext[0];
                $ext = str_replace("data:image/", '',$ext);
            $imgdata = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data['RawImages']));
            $imagename = $this->createImage($imgdata,$data['BUID'],$ext);
        }       
        
        
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_AddPersonImage(
            @BUID=:buid,
            @customerid=:customerid,
            @personid=:personid,
            @image=:image
        )}");

        $statement->bindParam(':buid', $data['BUID'], PDO::PARAM_INT);
        $statement->bindParam(':customerid', $data['customerid'], PDO::PARAM_INT);
        $statement->bindParam(':personid', $data['personid'], PDO::PARAM_INT);

        if (empty($imagename)) {
            $statement->bindParam(':image', $imagename = null, PDO::PARAM_INT);
        } else {
            $statement->bindParam(':image', $imagename, PDO::PARAM_STR);
        }
        
        if (!$statement->execute()) {
            throw new Exception(__t("Sorry profile image not uploaded"));
        }

        $data1=$statement->fetchAll(PDO::FETCH_ASSOC);
        $statement->closeCursor();
        return $data1;
    }

    public function getPersonImages($data) {
        if($data['BUID']==0){
            throw new Exception(__t("BUID 0"));
        }
        if($data['BUID']==""){
            throw new Exception(__t("Enter BUID"));
        }
        if($data['customerid']==""){
            throw new Exception(__t("Enter Customer Id"));
        }
        if($data['person_id']==""){
            $data['person_id']=null;
        }
        
        if($data['start']==''){
            $data['start']=0;
        }
        if($data['limit']==''){
            $data['limit']=20;
        }

        //dd($data, true);

        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_GetPersonImages(
            @BUID=:buid,
            @customerid=:customerid,
            @person_id=:person_id,
            @Start=:start,
            @Limit=:limit
        )}");

        $statement->bindParam(':buid', $data['BUID'], PDO::PARAM_INT);
        $statement->bindParam(':customerid', $data['customerid'], PDO::PARAM_INT);
        $statement->bindParam(':person_id', $data['person_id'], PDO::PARAM_INT);
        $statement->bindParam(':start', $data['start'], PDO::PARAM_INT);
        $statement->bindParam(':limit', $data['limit'], PDO::PARAM_INT);
        $statement->execute();
        $x = 0;        
        $data1 = [];

            do {            
                $rows = $statement->fetchAll(PDO::FETCH_ASSOC);                       
                switch ($x) {                
                    case 0:                    
                        $data1['Pagination'] = [                        
                                            'total' => (int) $rows[0]['totalPersons'],                        
                                            'start' => $data['start'],                        
                                            'limit' => $data['limit']                    
                                        ];                    
                        break;                
                    case 1:                    
                        $data1['Images'] = $rows;                    
                        break;            
                }            
                $x++;

            } while ($statement->nextRowset());
        return $data1;
    }

    private function uploadImage($FILES, $BUID) {
        $max_size = file_upload_max_size();
        $client   = new S3Client([
                    'version' => 'latest',
                    'region' => AWS_REGION,
                    'credentials' => [
                    'key' => AWS_ACCESS_KEY,
                    'secret' => AWS_SECRET_KEY
            ]
        ]);

        $valid_formats = ["jpg", "png", "gif", "jpeg"];
        $FileXml = '';

        if ($FILES['Images']['error'] == 0) {
            $name = $FILES['Images']['name'];
            $size = $FILES['Images']['size'];
            $tmp = $FILES['Images']['tmp_name'];
            $type = $FILES['Images']['type'];

            if ($size > $max_size) {
                throw new Exception(__t("Sorry image " . $name . " size is bigger than accepted size"));
            }
            $ext = strtolower(getExtension($name));

            if (!in_array($ext, $valid_formats)) {
                throw new Exception(__t("Sorry image " . $name . " is not valid ,we accept " . implode(",", $valid_formats)));
            }

            $image_name_actual = time() . "." . $ext;
            try {
                $Result = $client->putObject(array(
                    'Bucket'        => AWS_BUCKET_NAME,
                    'Key'           => 'BU' . $BUID . '/' . $image_name_actual,
                    'SourceFile'    => $tmp,
                    'StorageClass'  => 'REDUCED_REDUNDANCY',
                    'ACL'           => 'public-read',
                    'ContentType'   => $type,
                    'Expires'       => date('Y-m-d H:i:s', strtotime("2 years"))
                ));
            } catch (S3Exception $exc) {
                throw new Exception("Aws S3 Error : " . $exc->getAwsErrorMessage());
            }

            return $Result['ObjectURL'];

        }
        return false;
    }

    private function createImage($imgStr, $BUID, $ext) {
        $client = new S3Client([
                'version' => 'latest',
                'region' => AWS_REGION,
                'credentials' => [
                'key' => AWS_ACCESS_KEY,
                'secret' => AWS_SECRET_KEY
            ]
        ]);
        if($ext == ''){
            $ext = "png";
        } 
         
        $image_name_actual = time() . "." . $ext;
        try {
            $Result = $client->putObject(array(
                'Bucket' => AWS_BUCKET_NAME,
                'Key' => 'BU' . $BUID . '/' . $image_name_actual,
                'Body' => $imgStr,
                'StorageClass' => 'REDUCED_REDUNDANCY',
                'ACL' => 'public-read',
                'Expires' => date('Y-m-d H:i:s', strtotime("2 years"))
            ));
        } catch (S3Exception $exc) {
            throw new Exception("Aws S3 Error : " . $exc->getAwsErrorMessage());
        }
        return $Result['ObjectURL'];
    }

    public function deleteFolderCardMap($data) {
        if($data['BUID']==""){
            throw new Exception(__t("Enter BUID."));
        }
        if($data['customerid']==""){
            throw new Exception(__t("Enter Customer Id."));
        }
        if($data['folderid']==""){
            throw new Exception(__t("Enter Folder Id."));
        }
        if($data['cardid']==""){
            throw new Exception(__t("Enter Card Id."));
        }

        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_deleteFolderCardMap(
            @BUID=:buid,
            @customerid=:customerid,
            @folderid=:folderid,
            @cardid=:cardid
        )}");

        $statement->bindParam(':buid', $data['BUID'], PDO::PARAM_INT);
        $statement->bindParam(':customerid', $data['customerid'], PDO::PARAM_INT);
        $statement->bindParam(':folderid', $data['folderid'], PDO::PARAM_INT);
        $statement->bindParam(':cardid', $data['cardid'], PDO::PARAM_INT);

        if (!$statement->execute()) {
            throw new Exception(__t("Folder card map deleted successfully."));
        }
        $statement->closeCursor();

    }

    public function addLike($data) {
        if($data['BUID']==""){
            throw new Exception(__t("Enter BUID."));
        }
        if($data['customerid']=="" || $data['customerid']==0){
            throw new Exception(__t("Enter Customer Id."));
        }
        if($data['cardid']==""){
            throw new Exception(__t("Enter card Id."));
        }

        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_AddLike(
            @BUID=:buid,
            @customerid=:customerid,
            @cardid=:cardid,
            @LIKEID=:likeid
        )}");

        $statement->bindParam(':buid', $data['BUID'], PDO::PARAM_INT);
        $statement->bindParam(':customerid', $data['customerid'], PDO::PARAM_INT);
        $statement->bindParam(':cardid', $data['cardid'], PDO::PARAM_INT);
        $statement->bindParam(':likeid', $LIKEID, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 400);

        if (!$statement->execute()) {
            throw new Exception(__t("Sorry card not added."));
        }
        
        $statement->closeCursor();
        return ['LIKEID' => $LIKEID];
    }

    public function deleteLike($data) {
        if($data['BUID']==""){
            throw new Exception(__t("Enter BUID."));
        }
        if($data['customerid']==""){
            throw new Exception(__t("Enter Customer Id."));
        }
        if($data['cardid']==""){
            throw new Exception(__t("Enter card Id."));
        }
        
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_DeleteLike(
            @BUID=:buid,
            @customerid=:customerid,
            @cardid=:cardid,
            @ID=:id
        )}");

        $statement->bindParam(':buid', $data['BUID'], PDO::PARAM_INT);
        $statement->bindParam(':customerid', $data['customerid'], PDO::PARAM_INT);
        $statement->bindParam(':cardid', $data['cardid'], PDO::PARAM_INT);        
        $statement->bindParam(':id', $ID, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 400);
        
        if (!$statement->execute()) {
            throw new Exception(__t("Sorry card not deleted."));
        }

        $statement->closeCursor();
        return ['ID' => $ID];
    }

    public function getShareCustomerList($data) {
        if($data['BUID']==0){
            throw new Exception(__t("BUID 0"));
        }        
        if($data['BUID']==""){
            throw new Exception(__t("Enter BUID"));
        }
        if($data['customerid']==""){
            throw new Exception(__t("Enter Customer Id"));
        }                   
        if($data['start']==''){
            $data['start']=0;
        }
        if($data['limit']==''){
            $data['limit']=20;
        }
        if($data['searchstring']==""){
            $data['searchstring']=null;
        }

        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_GetShareCustomerList(
            @BUID=:buid,
            @customerid=:customerid,            
            @Start=:start,
            @Limit=:limit,
            @SearchString=:searchstring
        )}");

        $statement->bindParam(':buid', $data['BUID'], PDO::PARAM_INT);
        $statement->bindParam(':customerid', $data['customerid'], PDO::PARAM_INT);
        $statement->bindParam(':start', $data['start'], PDO::PARAM_INT);
        $statement->bindParam(':limit', $data['limit'], PDO::PARAM_INT);
        $statement->bindParam(':searchstring', $data['searchstring'], PDO::PARAM_STR);
        $statement->execute();

        $x = 0;        
        $data1 = []; 
        if($data['id']===null){   
            do {            
                $rows = $statement->fetchAll(PDO::FETCH_ASSOC);                       
                switch ($x) {                
                    case 0:                    
                        $data1['Pagination'] = [                        
                                            'total' => (int) $rows[0]['totalPersons'],                        
                                            'start' => $data['start'],                        
                                            'limit' => $data['limit']                    
                                        ];                    
                        break;                
                    case 1:                    
                        $data1['sharecustomerlist'] = $rows;                    
                        break;            
                }            
                $x++;                 
            } while ($statement->nextRowset());
        }
        else{
            $data1=$statement->fetchAll(PDO::FETCH_ASSOC);  
        }
        return $data1;
    }

    public function addSubscription($data) {
        if($data['name']==""){
            throw new Exception(__t("Enter Name."));
        }
        if($data['description']==""){
            throw new Exception(__t("Enter description."));
        }
        if($data['price']==""){
            throw new Exception(__t("Enter price"));
        }
        if($data['currency']==""){
            throw new Exception(__t("Enter currency."));
        }
        if($data['billingCycle']==""){
            throw new Exception(__t("Enter billing cycle."));
        }
        if($data['status']==""){
            throw new Exception(__t("Enter status."));
        }

        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_AddSubscription("
            . "@name=:name,"
            . "@description=:description,"
            . "@price=:price,"
            . "@currency=:currency,"
            . "@billingCycle=:billingCycle,"
            . "@status=:status,"
            . "@ID=:id,"
            . "@SUBSCRIPTIONID=:subscriptionid"
            . ")}");

        $statement->bindParam(':name', $data['name'], PDO::PARAM_STR);
        $statement->bindParam(':description', $data['description'], PDO::PARAM_STR);
        $statement->bindParam(':price', $data['price'], PDO::PARAM_STR);
        $statement->bindParam(':currency', $data['currency'], PDO::PARAM_STR);
        $statement->bindParam(':billingCycle', $data['billingCycle'], PDO::PARAM_INT);
        $statement->bindParam(':status', $data['status'], PDO::PARAM_INT);
        $statement->bindParam(':id', $data['id'], PDO::PARAM_INT);
        $statement->bindParam(':subscriptionid', $SUBSCRIPTIONID, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 400);

        if (!$statement->execute()) {
            throw new Exception(__t("Sorry subscription not added."));
        } 

        $statement->closeCursor();
        return ['subscriptionid' => $SUBSCRIPTIONID];
    }
    
    public function getActiveSUbscriptions($data) {
        if($data['buid']==""){
            throw new Exception(__t("Enter BUID."));
        }
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_GetActiveSUbscriptions(@BUID=:BUID)}");
        $statement->bindParam(':BUID', $data['buid'], PDO::PARAM_INT);
        $statement->execute();
        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    public function upgradeSubscription($data) {

        if($data['BUID']==""){
            throw new Exception(__t("Enter BUID."));
        }
        if($data['customerid']==""){
            throw new Exception(__t("Enter customer Id."));
        }
        if($data['subscription_id']==""){
            throw new Exception(__t("Enter subscription Id"));
        }
        if($data['price']==""){
            throw new Exception(__t("Enter price."));
        }
        if($data['billingCycle']==""){
            throw new Exception(__t("Enter billing cycle."));
        }
                
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_UpgradeSubscription("
            . "@BUID=:buid,"
            . "@customerid=:customerid,"
            . "@subscription_id=:subscription_id,"
            . "@price=:price,"
            . "@billingCycle=:billingCycle"
            . ")}");

        $statement->bindParam(':buid', $data['BUID'], PDO::PARAM_INT);
        $statement->bindParam(':customerid', $data['customerid'], PDO::PARAM_INT);
        $statement->bindParam(':subscription_id', $data['subscription_id'], PDO::PARAM_INT);
        $statement->bindParam(':price', $data['price'], PDO::PARAM_STR);
        $statement->bindParam(':billingCycle', $data['billingCycle'], PDO::PARAM_INT);

        if (!$statement->execute()) {
            throw new Exception(__t("Sorry subscription not upgraded."));
        } 

        $statement->closeCursor();
    }

    public function getAllCards($data) {
        if($data['BUID']==0){
            throw new Exception(__t("BUID 0"));
        }
        if($data['BUID']==""){
            throw new Exception(__t("Enter BUID"));
        }     
        if($data['start']==''){
            $data['start']=0;
        }
        if($data['limit']==''){
            $data['limit']=20;
        }
        if($data['searchstring']==""){
            $data['searchstring']=null;
        }
        if($data['SearchStringIndustry']==""){
            $data['SearchStringIndustry']=null;
        }
        if($data['SearchStringProfession']==""){
            $data['SearchStringProfession']=null;
        }
		if($data['customerid']==""){
            $data['customerid']=null;
        }
        if($data['SearchStringCity']==""){
            $data['SearchStringCity']=null;
        }
        if($data['SearchStringState']==""){
            $data['SearchStringState']=null;
        }
        if($data['SearchStringCountry']==""){
            $data['SearchStringCountry']=null;
        }
        if($data['SearchStringZip']==""){
            $data['SearchStringZip']=null;
        }
        if($data['SearchStringName']==""){
            $data['SearchStringName']=null;
        }
        if($data['SearchStringEmail']==""){
            $data['SearchStringEmail']=null;
        }
        if($data['SearchStringPhone']==""){
            $data['SearchStringPhone']=null;
        }

        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_GetAllCards(
            @BUID=:buid,            
            @Start=:start,
            @Limit=:limit,
            @SearchString=:searchstring,
            @SearchStringIndustry=:SearchStringIndustry,
            @SearchStringProfession=:SearchStringProfession,
            @customerid=:customerid,
            @SearchStringCity=:SearchStringCity,
            @SearchStringState=:SearchStringState,
            @SearchStringCountry=:SearchStringCountry,
            @SearchStringZip=:SearchStringZip,
            @SearchStringName=:SearchStringName,
            @SearchStringEmail=:SearchStringEmail,
            @SearchStringPhone=:SearchStringPhone
        )}");

        $statement->bindParam(':buid', $data['BUID'], PDO::PARAM_INT);        
        $statement->bindParam(':start', $data['start'], PDO::PARAM_INT);
        $statement->bindParam(':limit', $data['limit'], PDO::PARAM_INT);
        $statement->bindParam(':searchstring', $data['searchstring'], PDO::PARAM_STR);
        $statement->bindParam(':SearchStringIndustry', $data['SearchStringIndustry'], PDO::PARAM_STR);
        $statement->bindParam(':SearchStringProfession', $data['SearchStringProfession'], PDO::PARAM_STR);
        $statement->bindParam(':customerid', $data['customerid'], PDO::PARAM_INT);
        $statement->bindParam(':SearchStringCity', $data['SearchStringCity'], PDO::PARAM_STR);
        $statement->bindParam(':SearchStringState', $data['SearchStringState'], PDO::PARAM_STR);
        $statement->bindParam(':SearchStringCountry', $data['SearchStringCountry'], PDO::PARAM_STR);
        $statement->bindParam(':SearchStringZip', $data['SearchStringZip'], PDO::PARAM_STR);

        $statement->bindParam(':SearchStringName', $data['SearchStringName'], PDO::PARAM_STR);
        $statement->bindParam(':SearchStringEmail', $data['SearchStringEmail'], PDO::PARAM_STR);
        $statement->bindParam(':SearchStringPhone', $data['SearchStringPhone'], PDO::PARAM_STR);

        $statement->execute();

        $x = 0;        
        $data1 = []; 
        if($data['id']===null){   
            do {            
                $rows = $statement->fetchAll(PDO::FETCH_ASSOC); 
                switch ($x) {                
                    case 0:                    
                        $data1['Pagination'] = [                        
                                            'total' => (int) $rows[0]['totalCards'],                        
                                            'start' => $data['start'],                        
                                            'limit' => $data['limit']                    
                                        ];                    
                        break;                
                    case 1:                    
                        $data1['Cards'] = $rows;                    
                        break;            
                }            
                $x++;                 
            } while ($statement->nextRowset());
        }
        else{
            $data1=$statement->fetchAll(PDO::FETCH_ASSOC);  
        }

        return $data1;
    }

    public function shareCard($data) {

        if($data['BUID']==""){
            throw new Exception(__t("Enter BUID."));
        }
        if($data['sharedby']==""){
            throw new Exception(__t("Enter shared by Id."));
        }
        if($data['sharedto']==""){
            throw new Exception(__t("Enter shared to Id."));
        }
        if($data['cardid']==""){
            throw new Exception(__t("Enter card Id."));
        }

        $xmlData = "<data>";
        foreach($data['sharedto'] as $id){
            $xmlData.= "<sharedto><id>".$id."</id></sharedto>";
        }
        $xmlData.= "</data>";

        //dd($data, true);
                           
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_ShareCard("
            . "@BUID=:buid,"
            . "@sharedby=:sharedby,"
            . "@sharedto=:sharedto,"
            . "@cardid=:cardid"
            . ")}");

        $statement->bindParam(':buid', $data['BUID'], PDO::PARAM_INT);
        $statement->bindParam(':sharedby', $data['sharedby'], PDO::PARAM_INT);
        $statement->bindParam(':sharedto', $xmlData, PDO::PARAM_STR);
        $statement->bindParam(':cardid', $data['cardid'], PDO::PARAM_INT);

        if (!$statement->execute()) {
            throw new Exception(__t("Card not shared successfully."));
        }

        $statement->closeCursor();
    }
    
    public function shareFolder($data) {

        if($data['BUID']==""){
            throw new Exception(__t("Enter BUID."));
        }
        if($data['sharedby']==""){
            throw new Exception(__t("Enter shared by Id."));
        }
        if($data['sharedto']==""){
            throw new Exception(__t("Enter shared to Id."));
        }
        if($data['folderid']==""){
            throw new Exception(__t("Enter folder Id."));
        }

        $xmlData = "<data>";
        foreach($data['sharedto'] as $id){
            $xmlData.= "<sharedto><id>".$id."</id></sharedto>";
        }
        $xmlData.= "</data>";
                        
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_ShareFolder("
            . "@BUID=:buid,"
            . "@sharedby=:sharedby,"
            . "@sharedto=:sharedto,"
            . "@folderid=:folderid"
            . ")}");

        $statement->bindParam(':buid', $data['BUID'], PDO::PARAM_INT);
        $statement->bindParam(':sharedby', $data['sharedby'], PDO::PARAM_INT);
        $statement->bindParam(':sharedto', $xmlData, PDO::PARAM_STR);
        $statement->bindParam(':folderid', $data['folderid'], PDO::PARAM_INT);

        if (!$statement->execute()) {
            throw new Exception(__t("Folder not shared successfully."));
        }

        $statement->closeCursor();
    }

    public function getAllCardsShareTo($data) {

        if($data['BUID']==""){
            throw new Exception(__t("Enter BUID."));
        }
        if($data['customerid']==""){
            throw new Exception(__t("Enter customerid."));
        }
        if($data['start']==''){
            $data['start']=0;
        }
        if($data['limit']==''){
            $data['limit']=20;
        }
        if($data['searchstring']==""){
            $data['searchstring']=null;
        }
        if($data['SearchStringIndustry']==""){
            $data['SearchStringIndustry']=null;
        }
        if($data['SearchStringProfession']==""){
            $data['SearchStringProfession']=null;
        }
                        
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_GetAllCardsShareTo("
            . "@BUID=:buid,"
            . "@customerid=:customerid,"
            . "@Start=:start,"
            . "@Limit=:limit,"
            . "@SearchString=:searchstring,"
            . "@SearchStringIndustry=:searchstringindustry,"
            . "@SearchStringProfession=:searchstringprofession"
            . ")}");

        $statement->bindParam(':buid', $data['BUID'], PDO::PARAM_INT);
        $statement->bindParam(':customerid', $data['customerid'], PDO::PARAM_INT);
        $statement->bindParam(':start', $data['start'], PDO::PARAM_INT);
        $statement->bindParam(':limit', $data['limit'], PDO::PARAM_INT);
        $statement->bindParam(':searchstring', $data['searchstring'], PDO::PARAM_STR);
        $statement->bindParam(':searchstringindustry', $data['searchstringindustry'], PDO::PARAM_INT);
        $statement->bindParam(':searchstringprofession', $data['searchstringprofession'], PDO::PARAM_INT);

        $statement->execute();

        $x = 0;        
        $data1 = []; 
        if($data['id']===null){   
            do {            
                $rows = $statement->fetchAll(PDO::FETCH_ASSOC); 

                switch ($x) {                
                    case 0:                    
                        $data1['Pagination'] = [                        
                                            'total' => (int) $rows[0]['totalCards'],                        
                                            'start' => $data['start'],                        
                                            'limit' => $data['limit']                    
                                        ];                    
                        break;                
                    case 1:                    
                        $data1['Cards'] = $rows;                    
                        break;            
                }            
                $x++;                 
            } while ($statement->nextRowset());
        }
        else{
            $data1=$statement->fetchAll(PDO::FETCH_ASSOC);  
        }

        return $data1;
    }

    public function addReview($data) {
        if($data['BUID']==""){
            throw new Exception(__t("Enter BUID."));
        }
        if($data['customerid']==""){
            throw new Exception(__t("Enter Customer Id."));
        }
        if($data['personid']==""){
            throw new Exception(__t("Enter personid."));
        }
        if($data['reviews']==""){
            throw new Exception(__t("Enter reviews."));
        }
        
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_AddReview(
            @BUID=:buid,
            @customerid=:customerid,
            @personid=:personid,
            @reviews=:reviews,
            @REVIEWID=:reviewid
        )}");

        $statement->bindParam(':buid', $data['BUID'], PDO::PARAM_INT);
        $statement->bindParam(':customerid', $data['customerid'], PDO::PARAM_INT);
        $statement->bindParam(':personid', $data['personid'], PDO::PARAM_INT);
        $statement->bindParam(':reviews', $data['reviews'], PDO::PARAM_STR);        
        $statement->bindParam(':reviewid', $REVIEWID, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 400);
       
        if (!$statement->execute()) {
            throw new Exception(__t("Sorry review not added successfully."));
        }

        $statement->closeCursor();
        return ['REVIEWID' => $REVIEWID];
    }

    public function getReviews($data) {

        if($data['BUID']==""){
            throw new Exception(__t("Enter BUID."));
        }
        if($data['personid']==""){
            throw new Exception(__t("Enter personid."));
        }

        if($data['start']==''){
            $data['start']=0;
        }
        if($data['limit']==''){
            $data['limit']=20;
        }

        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_GetReviews("
            . "@BUID=:buid,"
            . "@personid=:personid,"
            . "@Start=:start,"
            . "@Limit=:limit"
            . ")}");

        $statement->bindParam(':buid', $data['BUID'], PDO::PARAM_INT);
        $statement->bindParam(':personid', $data['personid'], PDO::PARAM_INT);
        $statement->bindParam(':start', $data['start'], PDO::PARAM_INT);
        $statement->bindParam(':limit', $data['limit'], PDO::PARAM_INT);

        $statement->execute();

        $x = 0;        
        $data1 = []; 
        if($data['id']===null){   
            do {            
                $rows = $statement->fetchAll(PDO::FETCH_ASSOC);

                switch ($x) {                
                    case 0:                    
                        $data1['Pagination'] = [                        
                                            'total' => (int) $rows[0]['totalCards'],                        
                                            'start' => $data['start'],                        
                                            'limit' => $data['limit']                    
                                        ];                    
                        break;                
                    case 1:                    
                        $data1['reviews'] = $rows;                    
                        break;            
                }            
                $x++;                 
            } while ($statement->nextRowset());
        }
        else{
            $data1=$statement->fetchAll(PDO::FETCH_ASSOC);  
        }
        return $data1;
    }

    public function getAllFolder($data) {
        if($data['BUID']==0){
            throw new Exception(__t("BUID 0"));
        }
        if($data['BUID']==""){
            throw new Exception(__t("Enter BUID"));
        }
        if($data['customerid']==""){
            throw new Exception(__t("Enter Customer Id"));
        }
        if($data['id']==""){
            $data['id']=null;
        }
        if($data['parentid']==""){
            $data['parentid']=null;
        }        
        if($data['start']==''){
            $data['start']=0;
        }
        if($data['limit']==''){
            $data['limit']=20;
        }
        if($data['searchstring']==""){
            $data['searchstring']=null;
        }

        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_GetAllFolder(
            @BUID=:buid,
            @customerid=:customerid,
            @ID=:id,
            @parentid=:parentid,
            @Start=:start,
            @Limit=:limit,
            @SearchString=:searchstring
        )}");

        $statement->bindParam(':buid', $data['BUID'], PDO::PARAM_INT);
        $statement->bindParam(':customerid', $data['customerid'], PDO::PARAM_INT);
        $statement->bindParam(':id', $data['id'], PDO::PARAM_INT);
        $statement->bindParam(':parentid', $data['parentid'], PDO::PARAM_INT);
        $statement->bindParam(':start', $data['start'], PDO::PARAM_INT);
        $statement->bindParam(':limit', $data['limit'], PDO::PARAM_INT);
        $statement->bindParam(':searchstring', $data['searchstring'], PDO::PARAM_STR);

        $statement->execute();

        $x = 0;        
        $data1 = []; 
        if($data['id']===null){   
            do {            
                $rows = $statement->fetchAll(PDO::FETCH_ASSOC);                       
                switch ($x) {                
                    case 0:                    
                        $data1['Pagination'] = [                        
                                            'total' => (int) $rows[0]['totalPersons'],                        
                                            'start' => $data['start'],                        
                                            'limit' => $data['limit']                    
                                        ];                    
                        break;                
                    case 1:                    
                        $data1['Folders'] = $rows;                    
                        break;            
                }            
                $x++;                 
            } while ($statement->nextRowset());
        }
        else{
            $data1=$statement->fetchAll(PDO::FETCH_ASSOC);  
        }

        if($data['id']===null){ 
            $j=0;
            foreach($data1['Folders'] as $val){
                if($val["customerid"]!=$data['customerid']){
                    $statementSFD = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_GetShareFolderD(
                        @BUID=:buid,
                        @customerid=:customerid,
                        @ID=:id
                    )}");
            
                    $statementSFD->bindParam(':buid', $data['BUID'], PDO::PARAM_INT);
                    $statementSFD->bindParam(':customerid', $data['customerid'], PDO::PARAM_INT);
                    $statementSFD->bindParam(':id', $val['id'], PDO::PARAM_INT);
            
                    $statementSFD->execute();

                    $rowsSFD = $statementSFD->fetchAll(PDO::FETCH_ASSOC);
                    $data1['Folders'][$j]["SharedBY"] = $rowsSFD;
                }
                $j++;
            }
        }

        return $data1;
    }

    public function getMyCard($data) {
        $buid = $data['BUID'];
        $person_id = $data['person_id'];

        if($buid== ""){
            throw new Exception(__t("BUID 0"));
        }

        if($person_id== ""){
            throw new Exception(__t("Enter Person ID"));
        }
        
        //dd($data, true);

        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_GetMyCard(
            @BUID=:buid,
            @person_id=:person_id,
        )}");

        $statement->bindParam(':buid', $buid, PDO::PARAM_INT);
        $statement->bindParam(':person_id', $person_id, PDO::PARAM_INT);
        $statement->execute();

        $data1 = $statement->fetchAll(PDO::FETCH_ASSOC);

        
        return $data1;
    }

    public function deleteShareFolder($data) {
        if($data['BUID']==""){
            throw new Exception(__t("Enter BUID."));
        }
        if($data['sharedby']==""){
            throw new Exception(__t("Enter shareby Id."));
        }
        if($data['sharedto']==""){
            throw new Exception(__t("Enter sharedto Id."));
        }
        if($data['folderid']==""){
            throw new Exception(__t("Enter folder Id."));
        }
        
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_DeleteShareFolder(
            @BUID=:buid,
            @sharedby=:sharedby,
            @sharedto=:sharedto,
            @folderid=:folderid,
            @ID=:id
        )}");

        $statement->bindParam(':buid', $data['BUID'], PDO::PARAM_INT);
        $statement->bindParam(':sharedby', $data['sharedby'], PDO::PARAM_INT);
        $statement->bindParam(':sharedto', $data['sharedto'], PDO::PARAM_INT);
        $statement->bindParam(':folderid', $data['folderid'], PDO::PARAM_INT);        
        $statement->bindParam(':id', $ID, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 400);
        
        if (!$statement->execute()) {
            throw new Exception(__t("Sorry folder share not deleted."));
        }

        $statement->closeCursor();

        return ['ID' => $ID];
    }
    
    public function getHtml($data) {
        if($data['url']==""){
            throw new Exception(__t("Enter URL."));
        }
        $page_content = file_get_contents($data['url']);        
        
        return ['HTML' => $page_content];
    }

    public function addNotifications($data) {
        if($data['BUID']==0){
            throw new Exception(__t("BUID 0"));
        }
        if($data['BUID']==""){
            throw new Exception(__t("Enter BUID"));
        }
        if($data['customerid']==""){
            throw new Exception(__t("Enter Customer Id"));
        }
        if($data['notifiedBY']==""){
            throw new Exception(__t("Enter Notified by"));
        }
        if($data['notificationFor']==""){
            throw new Exception(__t("Enter Notifications for."));
        }
        if($data['notificationType']==""){
            throw new Exception(__t("Enter notifications type."));
        }
        if($data['itemID']==""){
            throw new Exception(__t("Enter notifications type."));
        }
        if($data['message']==""){
            $data['message']=NULL;
        }
        if($data['url']==""){
            $data['url']=NULL;
        }
        if($data['reviewid']==""){
            $data['reviewid']=NULL;
        }
        if($data['notificationFor']!='Person' && $data['notificationFor']!='Card' && $data['notificationFor']!='Folder'){
            throw new Exception(__t("Notifications for should be Person or card or Folder."));
        }
        if($data['notificationType']!='Like' && $data['notificationType']!='Share' && $data['notificationType']!='Review'){
            throw new Exception(__t("Notifications type should be Like or Share or Review."));
        }

        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_AddNotifications( 
            @BUID=:buid,
            @customerid=:customerid,
            @notifiedBY=:notifiedby,
            @notificationFor=:notificationfor,
            @notificationType=:notificationtype,
            @itemID=:itemid,
            @message=:message,
            @url=:url,
            @reviewID=:reviewid,
            @NOTIFICATIONID=:notificationid
        )}");

        $statement->bindParam(':buid', $data['BUID'], PDO::PARAM_INT);
        $statement->bindParam(':customerid', $data['customerid'], PDO::PARAM_INT);
        $statement->bindParam(':notifiedby', $data['notifiedBY'], PDO::PARAM_INT);
        $statement->bindParam(':notificationfor', $data['notificationFor'], PDO::PARAM_STR);
        $statement->bindParam(':notificationtype', $data['notificationType'], PDO::PARAM_STR);
        $statement->bindParam(':itemid', $data['itemID'], PDO::PARAM_INT);
        $statement->bindParam(':message', $data['message'], PDO::PARAM_STR);
        $statement->bindParam(':url', $data['url'], PDO::PARAM_STR);
        $statement->bindParam(':reviewid', $data['reviewid'], PDO::PARAM_INT);
        $statement->bindParam(':notificationid', $NOTIFICATIONID, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 400);
        $statement->execute();

        if (!$statement->execute()) {
            throw new Exception(__t("Sorry notification not added."));
        }

        $statement->closeCursor();
        return ['NOTIFICATIONID' => $NOTIFICATIONID];
    }

    public function sendPushNotification($BUID, $customerid,$title,$msg){

        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_Get_ActionsList_Data(@buid=:buid,@userid=:userid)}");
        $statement->bindParam(':buid', $BUID, PDO::PARAM_INT);
        $statement->bindParam(':userid', $customerid, PDO::PARAM_INT);
        /* $statement->bindParam(':buid', $buid, PDO::PARAM_INT);
          $statement->bindParam(':userid', $cid, PDO::PARAM_INT); */
        $statement->execute();
        $uniqueids = $statement->fetch(PDO::FETCH_ASSOC);

        //dd(PUSHERKEY, true);

        try {
            $pusher = new Pusher\Pusher(PUSHERKEY, PUSHERSECRET, PUSHERAPPID, array('encrypted' => true));
        } catch (Exception $e) {
            //echo $e->getMessage();
        }
        foreach ($uniqueids as $val) {
            $result = [
                'error' => false,
                'msg' => '',
                'data' => $pusher->trigger($val, 'pushMessage', [
                    'title' => strip_tags($title),
                    'description' => strip_tags($msg)
                ])
            ];
        }
        return  $result;
    }

    public function updateNotificationStatus($data){
        
        if($data['BUID']==0){
            throw new Exception(__t("BUID 0"));
        }
        if($data['BUID']==""){
            throw new Exception(__t("Enter BUID"));
        }
        if($data['notificationid']==""){
            throw new Exception(__t("Enter Notification Id."));
        }
        if($data['status']==""){
            $data['status']='read';
        }
        
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_updateNotificationStatus( 
            @BUID=:buid,
            @notificationid=:notificationid,
            @status=:status,            
            @ID=:id
        )}");

        $statement->bindParam(':buid', $data['BUID'], PDO::PARAM_INT);
        $statement->bindParam(':notificationid', $data['notificationid'], PDO::PARAM_INT);
        $statement->bindParam(':status', $data['status'], PDO::PARAM_STR);
        $statement->bindParam(':id', $ID, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 400);
        $statement->execute();

        if (!$statement->execute()) {
            throw new Exception(__t("Sorry notification not updated."));
        }

        $statement->closeCursor();
        return ['ID' => $ID];
    
    }

    public function getAllNotofocations($data) {
        if($data['BUID']==0){
            throw new Exception(__t("BUID 0"));
        }
        if($data['BUID']==""){
            throw new Exception(__t("Enter BUID"));
        }
        if($data['customerid']=="" || $data['customerid']<=0){
            throw new Exception(__t("Enter Customer Id"));
        }
        if($data['status']==""){
            $data['status']='new';
        }      
        if($data['start']==''){
            $data['start']=0;
        }
        if($data['limit']==''){
            $data['limit']=20;
        }

        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_GetAllNotofocations(
            @BUID=:buid,
            @customerid=:customerid,
            @status=:status,
            @Start=:start,
            @Limit=:limit            
        )}");

        $statement->bindParam(':buid', $data['BUID'], PDO::PARAM_INT);
        $statement->bindParam(':customerid', $data['customerid'], PDO::PARAM_INT);
        $statement->bindParam(':status', $data['status'], PDO::PARAM_STR);
        $statement->bindParam(':start', $data['start'], PDO::PARAM_INT);
        $statement->bindParam(':limit', $data['limit'], PDO::PARAM_INT);
        $statement->execute();

        $x = 0;        
        $data1 = [];  
            do {            
                $rows = $statement->fetchAll(PDO::FETCH_ASSOC);
                switch ($x) {                
                    case 0:                    
                        $data1['Pagination'] = [                        
                                            'total' => (int) $rows[0]['total'],                        
                                            'start' => $data['start'],                        
                                            'limit' => $data['limit']                    
                                        ];                    
                        break;                
                    case 1:                    
                        $data1['Notifications'] = $rows;                    
                        break;
                }            
                $x++;                 
            } while ($statement->nextRowset());
            
        return $data1;
    }

    public function generatevcard($data) {
        if($data['BUID']==0){
            throw new Exception(__t("BUID 0"));
        }
        if($data['BUID']==""){
            throw new Exception(__t("Enter BUID"));
        }
        if($data['customerid']==""){
            throw new Exception(__t("Enter Customer Id"));
        }
        if($data['personid']==""){
            throw new Exception(__t("Enter personid."));
        }        

        $isDefault = null;

        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_GetPrerson(
            @BUID=:buid,
            @customerid=:customerid,
            @personid=:personid,            
            @isDefault=:isDefault
        )}");

        $statement->bindParam(':buid', $data['BUID'], PDO::PARAM_INT);
        $statement->bindParam(':customerid', $data['customerid'], PDO::PARAM_INT);
        $statement->bindParam(':personid', $data['personid'], PDO::PARAM_INT);        
        $statement->bindParam(':isDefault', $isDefault, PDO::PARAM_INT);        
        $statement->execute();

        $rows = $statement->fetchAll(PDO::FETCH_ASSOC);  

        $first_name     = $rows[0]['first_name'];
        $last_name      = $rows[0]['last_name'];
        $phone          = $rows[0]['phone'];
        $email          = $rows[0]['email'];
        $website        = $rows[0]['website'];
        $industry_id    = $rows[0]['industry_id'];
        $profession_id  = $rows[0]['profession_id'];        
        $addressline_1  = $rows[0]['addressline_1'];
        $addressline_2  = $rows[0]['addressline_2'];
        $city           = $rows[0]['city'];
        $state          = $rows[0]['state'];
        $country        = $rows[0]['country'];
        $zip            = $rows[0]['zip'];
        $company        = $rows[0]['company'];

        $str = "";
        $str.= "BEGIN:VCARD\n";
        $str.= "VERSION:3.0\n";
        $str.= "N:".$last_name.";".$first_name.";;;\n";
        $str.= "FN:".$first_name." ".$last_name."\n";
        $str.= "ORG:".$company."\n";
        //$str.= "TITLE:".$last_name."\n";
		$str.= "TEL;TYPE=WORK,VOICE:".$phone."\n";
        $str.= "ADR;TYPE=WORK,PREF:;;".$addressline_1.";".$city.",".$state.",".$zip.",".$country."\n";
        if($addressline_1!=''){
            $str.= "LABEL;TYPE=WORK,PREF:".$addressline_1."\n".$city."\n".$state." ".$zip."\n".$country."\n"; 
        }               
        $str.= "EMAIL;TYPE=WORK,EMAIL:".$email."\n";
        $str.= "END:VCARD";

        $fileName = time().".vcf";
        $fileName1 = "../vcf/".$fileName;
        $file = fopen($fileName1, "w") or die("Unable to open file!");
        fwrite($file, $str);
        fclose($file);

        //return ["filepath"=>"https://ecom.myteamconnector.com/vcf/".$fileName];

        return ["filepath"=>"https://".$_SERVER['HTTP_HOST']."/vcf/".$fileName];
    }

	public function pushchatdata($data){

        try {
			$options = array(
				'cluster' => 'ap2',
				'encrypted' => true
			 );
            //$pusher = new Pusher\Pusher(PUSHERKEY, PUSHERSECRET, PUSHERAPPID, $options);
			$pusher = new Pusher\Pusher('76793177739cbfa6508f','d6227eec8a0ef31f8b45', '447619', $options);
        } catch (Exception $e) {
            //echo $e->getMessage();
        }	
		
		if(@$data['msg']!='' || @$data['msg']!=null){
			$msg = $data['msg'];
			$msg1 = base64_decode($msg);			
			$msg1=json_decode($msg1, true);
			$msg1["timestamp"] = gmdate(DATE_ATOM);
			//dd($msg1, true);
			if($data['eventType']=='vc_chat'){
				$statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_AddChat(
					@chatkey=:chatkey, 
					@fromID=:fromID,
					@toID=:toID, 
					@messages=:messages, 
					@addDate=:addDate,
					@CHATID=:CHATID
				)}");	
				
				$statement->bindParam(':fromID', $msg1['id'], PDO::PARAM_INT);				
				$statement->bindParam(':chatkey', $msg1['key'], PDO::PARAM_STR);
				$statement->bindParam(':toID', $msg1['to'], PDO::PARAM_INT);
				$statement->bindParam(':messages', $msg1['msg'], PDO::PARAM_STR);
                $statement->bindParam(':addDate', $msg1['timestamp'], PDO::PARAM_STR);
                //$statement->bindParam(':addDate', gmdate(DATE_ATOM), PDO::PARAM_STR);
                $statement->bindParam(':CHATID', $CHATID, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 400);
                //$statement->bindParam(':CHATDATE', $CHATDATE, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 400);

				if (!$statement->execute()) {
					throw new Exception(__t("Sorry chat not added."));
				}else{
                    $msg1["chatid"] = $CHATID;	
                    //$msg1["timestamp"] = $CHATDATE;
				}
							
			}
			$msg2=json_encode($msg1, true);
			$data['message'] = base64_encode($msg2);
			$pusher->trigger('vc_chatroom', $data['eventType'], $data);
			return 1;
		} else {
			return 0;
		}
    }
	
	public function getchatdata($data){

        if($data['key']==""){
            throw new Exception(__t("Enter key."));
        } 

        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_GetChat(
            @chatkey=:chatkey
        )}");					
        $statement->bindParam(':chatkey', $data['key'], PDO::PARAM_STR);
        $statement->execute();
			
		$x = 0;        
        $data1 = [];  
            do {            
                $rows = $statement->fetchAll(PDO::FETCH_ASSOC);
                switch ($x) {                
                    case 0:                    
                        $data1['Pagination'] = [                        
                                            'total' => (int) $rows[0]['total'],                        
                                            'start' => $data['start'],                        
                                            'limit' => $data['limit']                    
                                        ];                    
                        break;                
                    case 1:                    
                        $data1['Notifications'] = $rows;                    
                        break;
                }            
                $x++;                 
            } while ($statement->nextRowset());
            
        return $data1;
    }
	
	public function ChangeChatReadByID($data) {
        if($data['id']==""){
            throw new Exception(__t("Enter chat id."));
        }
        
		$statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_ChangeChatReadByID(@id=:id)}");

        $statement->bindParam(':id', $data['id'], PDO::PARAM_INT);
        
        if (!$statement->execute()) {
            throw new Exception(__t("Sorry ."));
        }

        return $data;
    }
	
	public function getchatUnreaddata($data){
        if($data['personid']=="" || $data['personid']<=0){
            throw new Exception(__t("Enter person id."));
        }


        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_GetUnreadChat(
            @customerid=:customerid
        )}");					
        $statement->bindParam(':customerid', $data['personid'], PDO::PARAM_INT);
        $statement->execute();
        
        $rows = $statement->fetchAll(PDO::FETCH_ASSOC);
            
        return $rows;
    }
	
	public function ChangeChatReadByCustomer($data) {
        if($data['personid']==""){
            throw new Exception(__t("Enter person id."));
        }
		if($data['chatkey']==""){
            throw new Exception(__t("Enter chatkey."));
        }
        
		$statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_ChangeChatReadByCustomerID(@chatkey=:chatkey, @customerid=:customerid)}");

        $statement->bindParam(':customerid', $data['personid'], PDO::PARAM_INT);
		$statement->bindParam(':chatkey', $data['chatkey'], PDO::PARAM_STR);
        
        if (!$statement->execute()) {
            throw new Exception(__t("Sorry ."));
        }

        return $data;
    }

    public function RenewSubscription($data) {
        
        if($data['BUID']== "" || $data['BUID']==0){
            throw new Exception(__t("BUID 0"));
        }

        if($data['customerid']== "" || $data['customerid']==0){
            throw new Exception(__t("customerid 0"));
        }

        if($data['subscription_id'] == ""){
            throw new Exception(__t("Enter subscription id."));
        }
            
        if($data['billingCycle'] == ""){
            throw new Exception(__t("Enter billing cycle."));
        }
            
        if($data['price'] == ""){
            throw new Exception(__t("Enter price.")); 
        }        
        
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_RenewSubscription(
            @BUID=:BUID,
            @customerid=:customerid,
            @subscription_id =:subscription_id,
            @price =:price,
            @billingCycle =:billingCycle
        )}");
        
        $statement->bindParam(':BUID', $data['BUID'], PDO::PARAM_INT);
        $statement->bindParam(':customerid', $data['customerid'], PDO::PARAM_INT);
        $statement->bindParam(':subscription_id', $data['subscription_id'], PDO::PARAM_INT);
        $statement->bindParam(':price', $data['price'], PDO::PARAM_STR);
        $statement->bindParam(':billingCycle', $data['billingCycle'], PDO::PARAM_INT);
        
        if (!$statement->execute()) {
            throw new Exception(__t("Sorry person not added"));
        }

        $data1 = []; 
        $rows = $statement->fetchAll(PDO::FETCH_ASSOC);   
        $data1['result'] = $rows;        
        $statement->closeCursor();
        return $data1;
    }

    public function ipushdata($data){        
        try {
            $options = array(
                'cluster' => 'ap2',
                'encrypted' => true
                );
            //$pusher = new Pusher\Pusher(PUSHERKEY, PUSHERSECRET, PUSHERAPPID, $options);
            $pusher = new Pusher\Pusher('76793177739cbfa6508f','d6227eec8a0ef31f8b45', '447619', $options);
        } catch (Exception $e) {
            //echo $e->getMessage();
        }	
        $pusher->trigger('vc_chatroom', $data['eventType'], $data);
        return 1;                        
    }

    public function CustomerSocialLogin($data) {
        
        if($data['BUID']==""){
            throw new Exception(__t("Enter BUID"));
        } 
        if($data['SocialID']==""){
            throw new Exception(__t("Enter BUSocialIDID"));
        }        
        if($data['SocialType']==""){
            throw new Exception(__t("Enter SocialType"));
        } 

        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_SocialLogin(
            @BUID=:BUID
            ,@SocialID=:SocialID
            ,@SocialType=:SocialType    
            ,@first_name=:first_name
            ,@last_name=:last_name
            ,@phone=:phone
            ,@email=:email          
            ,@subscription_id=:subscription_id
            ,@price=:price
            ,@billingCycle=:billingCycle
        )}");

        $statement->bindParam(':BUID', $data['BUID'], PDO::PARAM_INT);
        $statement->bindParam(':SocialID', $data['SocialID'], PDO::PARAM_STR);
        $statement->bindParam(':SocialType', $data['SocialType'], PDO::PARAM_STR);
        $statement->bindParam(':first_name', $data['first_name'], PDO::PARAM_STR);        
        $statement->bindParam(':last_name', $data['last_name'], PDO::PARAM_STR);
        $statement->bindParam(':phone', $data['phone'], PDO::PARAM_STR);
        $statement->bindParam(':email', $data['email'], PDO::PARAM_STR);
        $statement->bindParam(':subscription_id', $data['subscription_id'], PDO::PARAM_INT);
        $statement->bindParam(':price', $data['price'], PDO::PARAM_STR);
        $statement->bindParam(':billingCycle', $data['billingCycle'], PDO::PARAM_INT);  
        //$statement->bindParam(':CustomerID', $CustomerID, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 400);
        
        if (!$statement->execute()) {
            throw new Exception(__t("Sorry"));
        }

        $data1 = [];      
        do {            
            $rows = $statement->fetchAll(PDO::FETCH_ASSOC);   
            switch ($x) {                
                case 0:      
                    $data1['CustomerID'] = $rows[0]["CuStomerID"];
                    break;                
                case 1:                    
                    $data1['ISnew'] = $rows[0]["ISnew"];                    
                    break;  
                case 2:                    
                    $data1['personID'] = $rows[0]["personID"];                    
                    break;           
            }            
            $x++;                 
        } while ($statement->nextRowset());

        //$data1['CustomerID'] = $CustomerID;
        if (empty($data1['CustomerID'])) {
            throw new Exception("invalid login credentials provided");
        }else{
            
            $newStatement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_GetSubscriptionByCustomer("
                . "@BUID=:buid,"
                . "@customerid=:customerid"
                . ")}");
            $newStatement->bindParam(':buid', $data['BUID'], PDO::PARAM_INT);
            $newStatement->bindParam(':customerid', $data1['CustomerID'], PDO::PARAM_INT);
            $newStatement->execute();

            $subscription = $newStatement->fetch(PDO::FETCH_ASSOC);

            $data1['subscriptionid'] = $subscription['subscriptionid'];
            $data1['price'] = $subscription['price'];
            $data1['billingCycle'] = $subscription['billingCycle'];
            $data1['expirydate'] = $subscription['expirydate'];
            $data1['IsCancel'] = $subscription['IsCancel'];

            $data1['name'] = $subscription['name'];
            $data1['canShare'] = $subscription['canShare'];
            $data1['canAddPerson'] = $subscription['canAddPerson'];
            $data1['noOfPerson'] = $subscription['noOfPerson'];

            $dateToday = strtotime(date('Y-m-d'));
            $expDate   = strtotime($subscription['expirydate']);
            if($dateToday > $expDate)
                $expire = true;
            else
                $expire = false;

            $data1['expire'] = $expire;    
        }

        return $data1;
    }

    public function ProfileSocialImage($data, $file = []) {
        if($data['BUID']==""){
            throw new Exception(__t("Enter BUID."));
        }
        if($data['customerid']==""){
            throw new Exception(__t("Enter Customer Id."));
        }
        if($data['personid']==""){
            throw new Exception(__t("Enter Person Id."));
        }
        $imagename = null;        
        if (!empty($file)) {
            $imagename = $this->uploadImage($file,$data['BUID']);
        }
        if (empty($file) && !empty($data['image_url'])) {
            $imagename = $data['image_url'];
        }
        
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_AddProfileSocialImage(
            @BUID=:buid,
            @customerid=:customerid,
            @personid=:personid,
            @image=:image
        )}");

        $statement->bindParam(':buid', $data['BUID'], PDO::PARAM_INT);
        $statement->bindParam(':customerid', $data['customerid'], PDO::PARAM_INT);
        $statement->bindParam(':personid', $data['personid'], PDO::PARAM_INT);

        if (empty($imagename)) {
            $statement->bindParam(':image', $imagename = null, PDO::PARAM_INT);
        } else {
            $statement->bindParam(':image', $imagename, PDO::PARAM_STR);
        }
        
        if (!$statement->execute()) {
            throw new Exception(__t("Sorry profile image not uploaded"));
        }

        $data1=$statement->fetchAll(PDO::FETCH_ASSOC);
        $statement->closeCursor();
        return $data1;
    }

    public function UploadRawImageData($data) {    
        $ext = 'png';                
        try{
            $imgdata = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data['Images']));
            $ext = explode(";base64",$data['canvasImage']);
                $ext = $ext[0];
                $ext = str_replace("data:image/", '',$ext);
        }catch (Exception $exc) {
            throw new Exception(__t("Wrong image type."));
        }
        
        $imageUrl = $this->createImage($imgdata,$data['BUID'],$ext);

        return ['imageUrl' => $imageUrl];    
    }

    public function GetCountryCodes() {
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_GetCountryCodes()}");
        $statement->execute();
        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    public function SendForgotPasswordEmail($data) {
        $buid = $data['BUID'];
        $email = $data['customer'];
        
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_VerifyCustomer(
            @BUID=:buid,
            @email=:email
        )}");

        $statement->bindParam(':buid', $data['BUID'], PDO::PARAM_INT);
        $statement->bindParam(':email', $data['customer'], PDO::PARAM_STR);
        
        if (!$statement->execute()) {
            throw new Exception(__t("Sorry folder not added"));
        }

        $rows = $statement->fetchAll(PDO::FETCH_ASSOC); 
        $rowsN = $rows[0];  
        $data1['customerid'] = $rowsN["CustomerID"];        
        $statement->closeCursor();

        if($rowsN["CustomerID"] != null){
            $rowsN["loginurl"] = $data['url'] . "&key=".md5($rowsN["CustomerID"]);

            $statementN = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_insertResetPasswordKey(
                @BUID=:buid,
                @CustomerID=:CustomerID,
                @key=:key
            )}");
    
            $statementN->bindParam(':buid', $data['BUID'], PDO::PARAM_INT);
            $statementN->bindParam(':CustomerID', $rowsN['CustomerID'], PDO::PARAM_STR);
            $statementN->bindParam(':key', md5($rowsN["CustomerID"]), PDO::PARAM_STR);
            $statementN->execute();

            $this->NewSendForgotPasswordEmail($rowsN);
        }else{
            throw new Exception(__t("Customer doesnot exists."));
        }

        return $data1;
    }

    public function NewSendForgotPasswordEmail($data){
        $emailhtml='<!DOCTYPE html>
        <html>
        <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <title>My Contact Card</title>
        </head>
        <body style="padding: 0; margin: 0;">
            <table width="650" cellpadding="0" cellspacing="0" align="center">
                <thead>
                    <tr>
                        <th style="padding-top: 10px; padding-bottom: 10px; padding-left: 10px; padding-right: 10px; text-align: left; border-bottom: 1px solid #f0632b;"><img src="https://ecom.myteamconnector.com/img/ccemaillogo.jpg" style="display: block;"></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td align="left" valign="top">
                            <p style="padding-top: 30px; padding-bottom: 25px; margin: 0; font-family: Arial; font-size: 16px; line-height: 22px; color: #696969;">Hi '.$data['FirstName'].' '.$data['LastName'].',</p>
                            <p style="padding-bottom: 25px; margin: 0; font-family: Arial; font-size: 16px; line-height: 22px; color: #696969;">There was a request to change your password.</p>
                            <p style="padding-bottom: 25px; margin: 0; font-family: Arial; font-size: 16px; line-height: 22px; color: #696969;">&nbsp;</p>
                            <p style="padding-bottom: 25px; margin: 0; font-family: Arial; font-size: 16px; line-height: 22px; color: #696969;">If you did not make this request, just ignore this email. Otherwise please click or copy the link bellow to change the password. </p>
                        </td>
                    </tr>
                    
                    <tr>
                        <td align="center" valign="top" style="padding-top:40px; height: 60px;">
                            <a href="'.$data['loginurl'].'" style="background-color: #f0632b; color: #ffffff; text-decoration: none; font-family: Arial; font-size: 14px; line-height: 24px; padding-top:15px; padding-bottom: 15px; padding-left: 60px; padding-right: 60px;">Click here</a>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="middle" style="background-color: #eaeaea; padding-top: 10px; padding-bottom: 10px;"><p style="font-family: Arial; font-size: 12px; line-height: 16px; margin: 0; padding: 0; color: #3892c7;">&copy; COPYRIGHT 2018 MY CONTACT CARD. ALL RIGHTS RESERVED.</p></td>
                    </tr>
                </tbody>
            </table>
        </body>
        </html>';

        $emailData = array();                        
        $emailData['email_subject'] = "Reset Password..";            
        $emailData['email_body'] = $emailhtml;
        $emailData['to_email'] = $data['Email'];
        
        $CURLOPT_POSTFIELDS = array(
            'To' => $emailData['to_email'],
            'Subject' => $emailData['email_subject'],
            'MAilBody' => $emailData['email_body']
        );

        $response = callApi(API_URL_ENDPOINT . '/webapi/Mail/SendMail', 'POST', $CURLOPT_POSTFIELDS);
    }

    public function CheckForgotPasswordKey($data) {
        $key = $data['key'];
        
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_verifyResetPasswordKey(
            @key=:key
        )}");

        $statement->bindParam(':key', $data['key'], PDO::PARAM_STR);
        
        if (!$statement->execute()) {
            throw new Exception(__t("Sorry folder not added"));
        }

        $rows = $statement->fetchAll(PDO::FETCH_ASSOC); 
        $rowsN = $rows[0];  
        $data1 = array();        
        $statement->closeCursor();

        if($rowsN["customerID"] != null){
            if($rowsN["MinuteDiff"]<=1440){
                $data1['customerID'] = $rowsN["customerID"];   
            }else{
                throw new Exception(__t("Key expired."));
            }
            
        }else{
            throw new Exception(__t("Invalid key."));
        }

        return $data1;
    }

    public function UpdateForgotPassword($data) {

        if($data['BUID']==""){
            throw new Exception(__t("Enter BUID"));
        } 
        if($data['CustomerID']==""){
            throw new Exception(__t("Enter CustomerID"));
        }        
        if($data['password']==""){
            throw new Exception(__t("Enter password"));
        } 
        if($data['cpassword']==""){
            throw new Exception(__t("Enter confirm password"));
        } 
        if($data['password'] != $data['cpassword']){
            throw new Exception(__t("Password and Enter confirm password does not matched!"));
        }

        
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_ResetPassword(
            @BUID=:BUID,
            @CustomerID=:CustomerID,
            @password=:password
        )}");

        $statement->bindParam(':BUID', $data['BUID'], PDO::PARAM_INT);
        $statement->bindParam(':CustomerID', $data['CustomerID'], PDO::PARAM_INT);
        $statement->bindParam(':password', $data['password'], PDO::PARAM_STR);
        
        if (!$statement->execute()) {
            throw new Exception(__t("Please try again later."));
        }
     
        $statement->closeCursor();

        

        return $data;
    }

    public function addNetworkRequest($data){

        if($data['BUID']==""){
            throw new Exception(__t("Enter BUID."));
        }
        if($data['fromID']<=0){
            throw new Exception(__t("Please login to send network request."));
        }
        if($data['toID']<=0){
            throw new Exception(__t("Enter to customer ID."));
        }        

        $timestamp = gmdate(DATE_ATOM);

        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_AddCustomerNetworkRequest(
            @BUID=:BUID, 
            @fromID=:fromID,
            @toID=:toID, 
            @addDate=:addDate,
            @NetworkRequestID=:NetworkRequestID
        )}");	
        
        $statement->bindParam(':BUID', $data['BUID'], PDO::PARAM_INT);				
        $statement->bindParam(':fromID', $data['fromID'], PDO::PARAM_INT);
        $statement->bindParam(':toID', $data['toID'], PDO::PARAM_INT);
        $statement->bindParam(':addDate', $timestamp, PDO::PARAM_STR);
        //$statement->bindParam(':addDate', gmdate(DATE_ATOM), PDO::PARAM_STR);
        $statement->bindParam(':NetworkRequestID', $NetworkRequestID, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 400);
        //$statement->bindParam(':CHATDATE', $CHATDATE, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 400);

        if (!$statement->execute()) {
            throw new Exception(__t("Sorry request not added."));
        }else{
            $msg1["chatid"] = $NetworkRequestID;	
            //$msg1["timestamp"] = $CHATDATE;
        }

        return 1;
		
		
    }

    public function GetCustomerNetworkRequest($data) {

        if($data['BUID']==""){
            throw new Exception(__t("Enter BUID."));
        }
        if($data['customerid']=="" || $data['customerid']<=0){
            throw new Exception(__t("Enter customerid."));
        }
        if($data['start']==''){
            $data['start']=0;
        }
        if($data['limit']==''){
            $data['limit']=20;
        }
        if($data['searchstring']==""){
            $data['searchstring']=null;
        }
        
                        
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_GetCustomerNetworkRequest("
            . "@BUID=:buid,"
            . "@customerid=:customerid,"
            . "@Start=:start,"
            . "@Limit=:limit,"
            . "@SearchString=:searchstring"
            . ")}");

        $statement->bindParam(':buid', $data['BUID'], PDO::PARAM_INT);
        $statement->bindParam(':customerid', $data['customerid'], PDO::PARAM_INT);
        $statement->bindParam(':start', $data['start'], PDO::PARAM_INT);
        $statement->bindParam(':limit', $data['limit'], PDO::PARAM_INT);
        $statement->bindParam(':searchstring', $data['searchstring'], PDO::PARAM_STR);

        $statement->execute();

        $x = 0;        
        $data1 = []; 
        if($data['id']===null){   
            do {            
                $rows = $statement->fetchAll(PDO::FETCH_ASSOC); 

                switch ($x) {                
                    case 0:                    
                        $data1['Pagination'] = [                        
                                            'total' => (int) $rows[0]['totalCards'],                        
                                            'start' => $data['start'],                        
                                            'limit' => $data['limit']                    
                                        ];                    
                        break;                
                    case 1:                    
                        $data1['Cards'] = $rows;                    
                        break;            
                }            
                $x++;                 
            } while ($statement->nextRowset());
        }
        else{
            $data1=$statement->fetchAll(PDO::FETCH_ASSOC);  
        }

        return $data1;
    }

    public function GetCustomerNetworkSuggestion($data) {

        if($data['BUID']==""){
            throw new Exception(__t("Enter BUID."));
        }
        if($data['customerid']=="" || $data['customerid']<=0){
            throw new Exception(__t("Enter customerid."));
        }
        if($data['start']==''){
            $data['start']=0;
        }
        if($data['limit']==''){
            $data['limit']=20;
        }
        if($data['searchstring']==""){
            $data['searchstring']=null;
        }
        
                        
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_GetCustomerNetworkSuggestion("
            . "@BUID=:buid,"
            . "@customerid=:customerid,"
            . "@Start=:start,"
            . "@Limit=:limit,"
            . "@SearchString=:searchstring"
            . ")}");

        $statement->bindParam(':buid', $data['BUID'], PDO::PARAM_INT);
        $statement->bindParam(':customerid', $data['customerid'], PDO::PARAM_INT);
        $statement->bindParam(':start', $data['start'], PDO::PARAM_INT);
        $statement->bindParam(':limit', $data['limit'], PDO::PARAM_INT);
        $statement->bindParam(':searchstring', $data['searchstring'], PDO::PARAM_STR);

        $statement->execute();

        $x = 0;        
        $data1 = []; 
        if($data['id']===null){   
            do {            
                $rows = $statement->fetchAll(PDO::FETCH_ASSOC); 

                switch ($x) {                
                    case 0:                    
                        $data1['Pagination'] = [                        
                                            'total' => (int) $rows[0]['totalCards'],                        
                                            'start' => $data['start'],                        
                                            'limit' => $data['limit']                    
                                        ];                    
                        break;                
                    case 1:                    
                        $data1['Cards'] = $rows;                    
                        break;            
                }            
                $x++;                 
            } while ($statement->nextRowset());
        }
        else{
            $data1=$statement->fetchAll(PDO::FETCH_ASSOC);  
        }

        return $data1;
    }

    public function ChangeCustomerNetworkRequestStatus($data){

        $timestamp = gmdate(DATE_ATOM);

        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_ChangeCustomerNetworkRequestStatus(
            @BUID=:BUID, 
            @fromID=:fromID,
            @toID=:toID, 
            @addDate=:addDate,
            @NetworkRequestID=:NetworkRequestID,  
	        @status=:status,  
	        @NetworkID=:NetworkID
        )}");	
        
        $statement->bindParam(':BUID', $data['BUID'], PDO::PARAM_INT);				
        $statement->bindParam(':fromID', $data['fromID'], PDO::PARAM_INT);
        $statement->bindParam(':toID', $data['toID'], PDO::PARAM_INT);
        $statement->bindParam(':addDate', $timestamp, PDO::PARAM_STR);
        $statement->bindParam(':NetworkRequestID', $data['NetworkRequestID'], PDO::PARAM_INT);
        $statement->bindParam(':status', $data['status'], PDO::PARAM_INT);
        $statement->bindParam(':NetworkID', $NetworkID, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 400);

        if (!$statement->execute()) {
            throw new Exception(__t("Sorry request not added."));
        }else{
            $msg1["chatid"] = $NetworkID;
        }

        return 1;
		
		
    }

    public function DeleteCustomerNetworkRequest($data){

        $timestamp = gmdate(DATE_ATOM);

        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_DeleteCustomerNetworkRequest(
            @BUID=:BUID, 
            @NetworkRequestID=:NetworkRequestID, 
	        @NetworkID=:NetworkRequestID
        )}");	
        
        $statement->bindParam(':BUID', $data['BUID'], PDO::PARAM_INT);
        $statement->bindParam(':NetworkRequestID', $data['NetworkRequestID'], PDO::PARAM_INT);
        $statement->bindParam(':NetworkID', $NetworkID, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 400);

        if (!$statement->execute()) {
            throw new Exception(__t("Sorry request not deleted."));
        }else{
            $msg1["chatid"] = $NetworkID;
        }

        return 1;
		
		
    }

    public function GetCustomerNetworkList($data) {

        if($data['BUID']==""){
            throw new Exception(__t("Enter BUID."));
        }
        if($data['customerid']==""){
            throw new Exception(__t("Enter customerid."));
        }
        if($data['start']==''){
            $data['start']=0;
        }
        if($data['limit']==''){
            $data['limit']=20;
        }
        if($data['searchstring']==""){
            $data['searchstring']=null;
        }
        
                        
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_GetCustomerNetworkList("
            . "@BUID=:buid,"
            . "@customerid=:customerid,"
            . "@Start=:start,"
            . "@Limit=:limit,"
            . "@SearchString=:searchstring"
            . ")}");

        $statement->bindParam(':buid', $data['BUID'], PDO::PARAM_INT);
        $statement->bindParam(':customerid', $data['customerid'], PDO::PARAM_INT);
        $statement->bindParam(':start', $data['start'], PDO::PARAM_INT);
        $statement->bindParam(':limit', $data['limit'], PDO::PARAM_INT);
        $statement->bindParam(':searchstring', $data['searchstring'], PDO::PARAM_STR);

        $statement->execute();

        $x = 0;        
        $data1 = []; 
        if($data['id']===null){   
            do {            
                $rows = $statement->fetchAll(PDO::FETCH_ASSOC); 

                switch ($x) {                
                    case 0:                    
                        $data1['Pagination'] = [                        
                                            'total' => (int) $rows[0]['totalCards'],                        
                                            'start' => $data['start'],                        
                                            'limit' => $data['limit']                    
                                        ];                    
                        break;                
                    case 1:                    
                        $data1['Cards'] = $rows;                    
                        break;            
                }            
                $x++;                 
            } while ($statement->nextRowset());
        }
        else{
            $data1=$statement->fetchAll(PDO::FETCH_ASSOC);  
        }

        return $data1;
    }

    public function DeleteCustomerNetwork($data){

        $statement = $this->PDO->prepare("{CALL [PHP_Ecommerce_ContactCards_DeleteCustomerNetwork](
            @BUID=:BUID, 
            @fromID=:fromID, 
            @toID=:toID,
	        @NetworkID=:NetworkID
        )}");	
        
        $statement->bindParam(':BUID', $data['BUID'], PDO::PARAM_INT);
        $statement->bindParam(':fromID', $data['fromID'], PDO::PARAM_INT);
        $statement->bindParam(':toID', $data['toID'], PDO::PARAM_INT);
        $statement->bindParam(':NetworkID', $data['NetworkID'], PDO::PARAM_INT);

        if (!$statement->execute()) {
            throw new Exception(__t("Sorry request not deleted."));
        }else{
            $msg1["chatid"] = $NetworkID;
        }

        return 1;
		
		
    }

    public function addCardVault($data, $file = []) {
        
        //echo ini_get("upload_max_filesize");
        //dd($file,true);
        //dd($data, true);
        if($data['BUID']==""){
            throw new Exception(__t("Enter BUID."));
        }
        if($data['customerid']==""){
            throw new Exception(__t("Enter Customer Id."));
        }
        $imagename = null;        
        if (!empty($file)) {
            $imagename = $this->uploadImage($file,$data['BUID']);
        }
        if (empty($file) && !empty($data['image_url'])) {
            $imagename = $data['image_url'];
        }
        if (!empty($data['canvasImage'])) {
            try{
                $ext = explode(";base64",$data['canvasImage']);
                $ext = $ext[0];
                $ext = str_replace("data:image/", '',$ext);

                $imgdata = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data['canvasImage']));
                $imagename = $this->createImage($imgdata,$data['BUID'],$ext);
            }catch (Exception $exc) {
                throw new Exception(__t("Wrong image type."));
            }
        }

        //dd($imagename, true);
                   
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_addCardVault(
            @BUID=:buid,
            @customerid=:customerid,
            @image=:image
        )}");

        $statement->bindParam(':buid', $data['BUID'], PDO::PARAM_INT);
        $statement->bindParam(':customerid', $data['customerid'], PDO::PARAM_INT);

        if (empty($imagename)) {
            $statement->bindParam(':image', $imagename = null, PDO::PARAM_INT);
        } else {
            $statement->bindParam(':image', $imagename, PDO::PARAM_STR);
        }
        
        if (!$statement->execute()) {
            throw new Exception(__t("Sorry profile image not uploaded"));
        }

        $data1=$statement->fetchAll(PDO::FETCH_ASSOC);
        $statement->closeCursor();
        return $data1;
    }

    public function getCardVault($data) {
        if($data['BUID']==""){
            throw new Exception(__t("Enter BUID."));
        }
        if($data['customerid']==""){
            throw new Exception(__t("Enter Customer Id."));
        }        
                   
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_getCardVault(
            @BUID=:buid,
            @customerid=:customerid
        )}");

        $statement->bindParam(':buid', $data['BUID'], PDO::PARAM_INT);
        $statement->bindParam(':customerid', $data['customerid'], PDO::PARAM_INT);
        
        if (!$statement->execute()) {
            throw new Exception(__t("Sorry profile image not uploaded"));
        }

        $data1=$statement->fetchAll(PDO::FETCH_ASSOC);
        $statement->closeCursor();
        return $data1;
    }

    public function deleteCardVault($data) {
        if($data['BUID']==""){
            throw new Exception(__t("Enter BUID."));
        }
        if($data['customerid']==""){
            throw new Exception(__t("Enter Customer Id."));
        }  
        if($data['cardvaultid']==""){
            throw new Exception(__t("Enter card vault Id."));
        }        
                   
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_deleteCardVault(
            @id=:id,
            @BUID=:buid,
            @customerid=:customerid
        )}");
        $statement->bindParam(':id', $data['cardvaultid'], PDO::PARAM_INT);
        $statement->bindParam(':buid', $data['BUID'], PDO::PARAM_INT);
        $statement->bindParam(':customerid', $data['customerid'], PDO::PARAM_INT);
        
        if (!$statement->execute()) {
            throw new Exception(__t("Sorry image not deleted"));
        }
        //$data1=$statement->fetchAll(PDO::FETCH_ASSOC);
        $statement->closeCursor();
        return true;
    }

    public function CheckOCRContact($data) {
        if($data['BUID']==""){
            throw new Exception(__t("Enter BUID."));
        }
        if($data['customerid']==""){
            throw new Exception(__t("Enter Customer Id."));
        }
        if($data['phone']==""){
            throw new Exception(__t("Enter phone no."));
        }
        if($data['email']==""){
            throw new Exception(__t("Enter email."));
        }
        
        
                   
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_CheckOCRContact(
            @BUID=:buid,
            @customerid=:customerid,
            @phone=:phone,
            @email=:email
        )}");

        $statement->bindParam(':buid', $data['BUID'], PDO::PARAM_INT);
        $statement->bindParam(':customerid', $data['customerid'], PDO::PARAM_INT);
        $statement->bindParam(':phone', $data['phone'], PDO::PARAM_STR);
        $statement->bindParam(':email', $data['email'], PDO::PARAM_STR);

        
        
        if (!$statement->execute()) {
            throw new Exception(__t("Sorry profile not uploaded"));
        }

        $data1=$statement->fetchAll(PDO::FETCH_ASSOC);
        $statement->closeCursor();
        return $data1;
    }

    public function AddOCRContact($data){
        if($data['BUID']== "" || $data['BUID']==0){
            throw new Exception(__t("BUID 0"));
        }

        if($data['customerid']== "" || $data['customerid']==0){
            throw new Exception(__t("customerid 0"));
        }
        if($data['cardVaultID']== "" || $data['cardVaultID']==0){
            throw new Exception(__t("cardVaultID 0"));
        }
       
        if($data['first_name']==""){
            throw new Exception(__t("Enter First Name"));
        }
        if($data['last_name']==""){
            throw new Exception(__t("Enter Last Name"));
        }
        if($data['phone']==""){
            throw new Exception(__t("Enter Phone No"));
        }
        if($data['email']==""){
            throw new Exception(__t("Enter Email"));
        }  

        if($data['cardname']==""){
            throw new Exception(__t("Enter Card Name."));
        }
        if($data['carddata']==""){
            throw new Exception(__t("Enter Card Data."));
        } 

        $ext = 'png';

        try{
            $imgdata = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data['CardImage']));
            $ext = explode(";base64",$data['canvasImage']);
                $ext = $ext[0];
                $ext = str_replace("data:image/", '',$ext);
        }catch (Exception $exc) {
            throw new Exception(__t("Wrong image type."));
        }
        
        $cardimageUrl = $this->createImage($imgdata,$data['BUID'],$ext);


        if($data['industry_id']==""){
            $data['industry_id'] = null;
        }
        if($data['profession_id']==""){
            $data['profession_id'] = null;
        }
        if($data['addressline_1']==""){
            $data['addressline_1'] = null;
        }
        if($data['city']==""){
            $data['city'] = null;
        }
        if($data['state']==""){
            $data['state'] = null;
        }
        if($data['country']==""){
            $data['country'] = null;
        }
        if($data['zip']==""){
            $data['zip'] = null;
        }
        if($data['bio']==""){
            $data['bio'] = null;
        }
        
        $statementS = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_GetActiveSUbscriptions(@BUID=:BUID)}");
        $statementS->bindParam(':BUID', $data['BUID'], PDO::PARAM_INT);
        $statementS->execute();
        $subscriptions = $statementS->fetchAll(PDO::FETCH_ASSOC);
        
        foreach($subscriptions as $tag){
            //dd($tag);
            if($tag['name']=='FREE'){
                //dd($tag);
                $data['subscription_id'] = $tag['id'];
            }
        }

        //dd($subscriptions,true);

        $data['billingCycle']=0;
        $data['price']=0;
        

        //dd($data["subscription_id"],true);

        $imagename = '';        
        /*if (!empty($file['Images']['name'])) {
            $imagename = $this->uploadImage($file,$data['BUID']);
        }

        if (empty($file['Images']['name']) && $data['oldimage'] != '') {
            $imagename = $data['oldimage'];
        }*/
        
        $password = $this->random_password(10);
        
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_AddOCRContact(
            @BUID=:BUID,
            @customerid=:customerid,
            @cardVaultID=:cardVaultID,
            @first_name=:first_name,
            @last_name=:last_name,
            @phone=:phone,
            @email=:email,
            @website=:website,
            @industry_id=:industry_id,
            @profession_id=:profession_id,
            @addressline_1=:addressline_1,
            @addressline_2=:addressline_2,
            @city=:city,
            @state=:state,
            @country=:country,
            @zip=:zip,
            @fb_profile=:fb_profile,
            @twitter_profile=:twitter_profile,
            @g_profile=:g_profile,
            @in_profile=:in_profile,
            @instagram_profile=:instagram_profile,
            @resume_link=:resume_link,
            @bio=:bio,
            @qr_code=:qr_code,
            @allow_save=:allow_save,
            @password=:password,
            @company=:company,
            @image=:image,
            @subscription_id =:subscription_id,
            @price =:price,
            @billingCycle =:billingCycle,
            @CountryCode =:CountryCode,
            @cardname =:cardname,
            @carddata =:carddata,
            @cardimage =:cardimage
        )}");
        
        $statement->bindParam(':BUID', $data['BUID'], PDO::PARAM_INT);
        $statement->bindParam(':customerid', $data['customerid'], PDO::PARAM_INT);
        $statement->bindParam(':cardVaultID', $data['cardVaultID'], PDO::PARAM_INT);
        $statement->bindParam(':first_name', $data['first_name'], PDO::PARAM_STR);
        $statement->bindParam(':last_name', $data['last_name'], PDO::PARAM_STR);
        $statement->bindParam(':phone', $data['phone'], PDO::PARAM_STR);
        $statement->bindParam(':email', $data['email'], PDO::PARAM_STR);        
        $statement->bindParam(':website', $data['website'], PDO::PARAM_STR);
        $statement->bindParam(':industry_id', $data['industry_id'], PDO::PARAM_INT);
        $statement->bindParam(':profession_id', $data['profession_id'], PDO::PARAM_INT);
        $statement->bindParam(':addressline_1', $data['addressline_1'], PDO::PARAM_STR);
        $statement->bindParam(':addressline_2', $data['addressline_2'], PDO::PARAM_STR);
        $statement->bindParam(':city', $data['city'], PDO::PARAM_STR);
        $statement->bindParam(':state', $data['state'], PDO::PARAM_STR);
        $statement->bindParam(':country', $data['country'], PDO::PARAM_STR);
        $statement->bindParam(':zip', $data['zip'], PDO::PARAM_STR);
        $statement->bindParam(':fb_profile', $data['fb_profile'], PDO::PARAM_STR);
        $statement->bindParam(':twitter_profile', $data['twitter_profile'], PDO::PARAM_STR);
        $statement->bindParam(':g_profile', $data['g_profile'], PDO::PARAM_STR);
        $statement->bindParam(':in_profile', $data['in_profile'], PDO::PARAM_STR);
        $statement->bindParam(':instagram_profile', $data['instagram_profile'], PDO::PARAM_STR);
        $statement->bindParam(':resume_link', $data['resume_link'], PDO::PARAM_STR);
        $statement->bindParam(':bio', $data['bio'], PDO::PARAM_STR);
        if (empty($data['qr_code'])) {
            $statement->bindParam(':qr_code', $data['qr_code'] = 0, PDO::PARAM_INT);
        }
        else{
            $statement->bindParam(':qr_code', $data['qr_code'], PDO::PARAM_INT);
        }
        if (empty($data['allow_save'])) {
            $statement->bindParam(':allow_save', $data['allow_save'] = 0, PDO::PARAM_INT);
        }
        else{
            $statement->bindParam(':allow_save', $data['allow_save'], PDO::PARAM_INT);
        }
        $statement->bindParam(':password', $password, PDO::PARAM_STR);
        $statement->bindParam(':company', $data['company'], PDO::PARAM_STR);
        if (!empty($imagename)) {
            $statement->bindParam(':image', $imagename, PDO::PARAM_STR);
        } else {
            $statement->bindParam(':image', $imagename = null, PDO::PARAM_INT);
        }
        $statement->bindParam(':subscription_id', $data['subscription_id'], PDO::PARAM_INT);
        $statement->bindParam(':price', $data['price'], PDO::PARAM_STR);
        $statement->bindParam(':billingCycle', $data['billingCycle'], PDO::PARAM_INT);
        
        if (!empty($data['CountryCode'])) {
            $statement->bindParam(':CountryCode', $data['CountryCode'], PDO::PARAM_STR);
        } else {
            $statement->bindParam(':CountryCode', $data['CountryCode'] = NULL, PDO::PARAM_INT);
        }

        $statement->bindParam(':cardname', $data['cardname'], PDO::PARAM_STR);
        $statement->bindParam(':carddata', $data['carddata'], PDO::PARAM_STR);
        //$statement->bindParam(':cardimage', $data['cardimage'], PDO::PARAM_STR);
        if(empty($cardimageUrl)) {
            $statement->bindParam(':cardimage', $cardimageUrl = null, PDO::PARAM_INT);
        }else{
            $statement->bindParam(':cardimage', $cardimageUrl, PDO::PARAM_STR);
        }
        
        if (!$statement->execute()) {
            throw new Exception(__t("Sorry person not added"));
        }

        $rows = $statement->fetchAll(PDO::FETCH_ASSOC);

        $emailData = array();
        $emailData['first_name'] = $data['first_name'];
        $emailData['last_name'] = $data['last_name'];
        $emailData['email'] = $data['email'];   
        $emailData['phone'] = $data['phone'];
        $emailData['loginurl'] = $data['loginurl'];
        $emailData['profileURL'] = $data['profileurl'].$rows[0]['personid'];
        $emailData['cardimage'] = $cardimageUrl;        
        $emailData['password'] = $password;

        $this->newContactCardCustomerEmailOCR($emailData);

        //$rows = $statement->fetchAll(PDO::FETCH_ASSOC);         
        $statement->closeCursor();
        
        return $rows;
    }

    public function newContactCardCustomerEmailOCR($data){
        $emailhtml='
        <html>

        <body data-getresponse="true" style="margin: 0; padding: 0;">

            <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" data-mobile="true" dir="ltr" data-width="550"
            style="font-size: 16px; background-color: rgb(255, 255, 255);">
                <tbody>
                    <tr>
                        <td align="center" valign="top" style="margin:0;padding:0 0 99px 0;">
                            <table align="center" border="0" cellspacing="0" cellpadding="0" width="550" style="width: 550px;">
                                <tbody>
                                    <tr>
                                        <td align="center" valign="top" style="margin: 0px; padding: 0px;">
                                            <table cellpadding="0" cellspacing="0" align="center" data-editable="image" data-mobile-stretch="1" width="51%">
                                                <tbody>
                                                    <tr>
                                                        <td valign="top" align="center" style="display: inline-block; padding: 10px 0px; margin: 0px;">
                                                            <img createnew="true" src="https://ecom.myteamconnector.com/img/ccemaillogo.jpg?img1515006407624" width="280" style="border-width: 0px; border-style: none; border-color: transparent; font-size: 12px; display: block;">
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" valign="top" style="margin: 0px; padding: 0px;">
                                            <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" data-editable="text">
                                                <tbody>
                                                    <tr>
                                                        <td align="left" valign="top" style="padding: 10px; font-size: 16px; font-family: Times New Roman, Times, serif; line-height: 1.15;">
                                                            <div style="text-align: center;">
                                                                <span style="background-color: inherit; color: rgb(42, 165, 255); font-weight: bold; font-family: Georgia, serif;">
                                                                    <font style="font-size: 78px;" size="78">WELCOME</font>
                                                                </span>
                                                            </div>
                                                            <span style="color: rgb(38, 38, 38); font-weight: bold;">
                                                                <div style="text-align: center;">
                                                                    <span style="background-color: transparent; font-family: Georgia, serif;">
                                                                        <font style="font-size: 21px;" size="21">TO #</font>
                                                                        <font style="font-size: 28px;" size="28">1</font>
                                                                        <font style="font-size: 21px;" size="21"> BUSINESS CARD APP</font>
                                                                    </span>
                                                                </div>
                                                            </span>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" style="padding:0;margin:0">
                                            <table align="center" width="100%" border="0" cellpadding="0" cellspacing="0" style="margin-right: auto; margin-left: auto;">
                                                <tbody>
                                                    <tr>
                                                        <td align="center" valign="top" style="padding:0;margin:0;">
                                                            <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center">
                                                                <tbody>
                                                                    <tr>
                                                                        <td align="center" valign="top" style="margin:0;padding:0;">
                                                                            <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" data-editable="line">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td align="center" valign="top" style="padding: 24px 42px 24px 37px; margin: 0px;">
                                                                                            <div style="height:1px;line-height:1px;border-top:1px solid #e4e4e4;">
                                                                                                <img createnew="true" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" alt="" width="1"
                                                                                                height="1" style="display:block;">
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                        <td align="center" valign="top" style="margin:0;padding:0;">
                                                                            <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" data-editable="line">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td align="center" valign="top" style="padding: 24px 42px 24px 34px; margin: 0px;">
                                                                                            <div style="height:1px;line-height:1px;border-top:1px solid #e4e4e4;">
                                                                                                <img createnew="true" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" alt="" width="1"
                                                                                                height="1" style="display:block;">
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top" style="margin:0;padding:0;">
                                            <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
                                                <tbody>
                                                    <tr>
                                                        <td valign="top" align="left" style="margin: 0px; padding: 10px 22px 0px 37px; font-size: 16px; font-family: Times New Roman, Times, serif; line-height: 1.15;">
                                                            <span style="font-size: 20px; color: rgb(83, 83, 83); font-weight: normal;">
                                                                <span style="font-weight: bold;">Hello&nbsp;</span>
                                                            </span>
                                                            <span style="font-weight: bold; color: rgb(83, 83, 83);">
                                                                <font style="font-size: 20px;" size="20">'.$data['first_name'].' '.$data['last_name'].',</font>
                                                            </span>
                                                            <span style="font-size: 20px; color: rgb(83, 83, 83); font-weight: normal;">
                                                                <span style="font-weight: bold;">
                                                                    <br>
                                                                    <br>
                                                                </span>It is so cool to have you with us!
                                                                <br>
                                                                <br>Thanks for joining My Contact Card. Create appealing contact cards to share globally and make a mark. Also
                                                                you can scan, manage and exchange business cards using My Contact Card in few clicks.</span>
                                                            <div>
                                                                <span style="font-size: 20px; color: rgb(83, 83, 83); font-weight: normal;">
                                                                    <br>
                                                                </span>
                                                            </div>
                                                            <div>
                                                                <span style="font-size: 20px; color: rgb(83, 83, 83); font-weight: normal;">As a My Contact Card customer, you will get exclusive access to these benefits and services:
                                                                    <br>
                                                                </span>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top" style="margin:0;padding:0;">
                                            <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
                                                <tbody>
                                                    <tr>
                                                        <td valign="top" align="left" style="margin: 0px; padding: 10px;">
                                                            <ul style="font-size: 20px; color: rgb(58, 57, 57); font-weight: normal; font-style:italic;">
                                                                <li style="margin:10px;">
                                                                    Use My Contact Card to capture all your business cards, and all the contact information can be read and saved to your smartphone.
                                                                </li>
                                                                <li style="margin:10px;">
                                                                    Exchange conact cards when meeting new people at meetings, tradeshows, seminars and other business social occasions. Go paperless
                                                                    now.
                                                                </li>
                                                                <li style="margin:10px;">
                                                                    All your business cards are stored in the cloud and synchronized across smartphones, tablets and the web app in real-time.
                                                                    Always available.
                                                                </li>
                                                                <li style="margin:10px;">
                                                                    Complete your own Profile to make a better impression to your contacts
                                                                </li>
                                                            </ul>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top" style="margin:0;padding:0;">
                                            <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
                                                <tbody>
                                                    <tr>
                                                        <td valign="top" align="left" style="margin: 0px; padding: 0px 44px 0px 37px;">
                                                            <span style="font-size: 20px; color: rgb(58, 57, 57); font-weight: normal;">and so much more!
                                                                <br>
                                                                <br>
                                                                <span style="font-weight: bold;">Please login to the app using the following credentials to get access to all features</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top" align="left" style="margin: 0px; padding: 10px;">
                                                            <ul style="font-size: 20px; color: rgb(58, 57, 57); font-weight: normal; font-style:italic;">
                                                                <li style="margin:10px;">
                                                                    Username: '.$data['email'].' <b>OR</b> '. $data['phone'].'
                                                                </li>
                                                                <li style="margin:10px;">
                                                                    Password: '.$data['password'].'
                                                                </li>
                                                            </ul>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td bgcolor="#ffffff" valign="top" align="center" style="padding:0 0 28px 0;margin:0;">
                                            <table border="0" cellpadding="0" cellspacing="0" align="center" style="margin: 0px; padding: 0px;" width="100%">
                                                <tbody>
                                                    <tr>
                                                        <td valign="middle" align="center" bgcolor="#ffffff" style="color: rgb(255, 255, 255); font-family: Helvetica, sans-serif; font-size: 20px; font-weight: bold; padding: 0px;">
                                                            <div data-box="button" style="width: 100%; margin-top: 0px; margin-bottom: 0px; text-align: center;">
                                                                <table border="0" cellpadding="0" cellspacing="0" align="center" data-editable="button" style="margin: 0px auto;">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td valign="middle" align="center" style="display: inline-block; padding: 15px 35px; margin: 0px; border-radius: 5px; background-color: rgb(13, 167, 255);">
                                                                                <a href="'.$data['loginurl'].'" style="font-family:Helvetica,sans-serif;color:#ffffff;font-size:30px;text-decoration:none;">Click Here to Login</a>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </body>

        </html>
        ';
        /*$emailhtml='<!DOCTYPE html>
        <html>
        <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <title>My Contact Card</title>
        </head>
        <body style="padding: 0; margin: 0;">
            <table width="650" cellpadding="0" cellspacing="0" align="center">
                <thead>
                    <tr>
                        <th style="padding-top: 10px; padding-bottom: 10px; padding-left: 10px; padding-right: 10px; text-align: left; border-bottom: 1px solid #f0632b;"><img src="https://ecom.myteamconnector.com/img/ccemaillogo.jpg" style="display: block;"></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td align="left" valign="top">
                            <p style="padding-top: 30px; padding-bottom: 25px; margin: 0; font-family: Arial; font-size: 16px; line-height: 22px; color: #696969;">Welcome '.$data['first_name'].' '.$data['last_name'].'.</p>
                            <p style="padding-bottom: 25px; margin: 0; font-family: Arial; font-size: 16px; line-height: 22px; color: #696969;">Thanks for joining My Contact Card. Create appealing contact cards to share globally and make a mark.</p>
                            <p style="padding-bottom: 25px; margin: 0; font-family: Arial; font-size: 16px; line-height: 22px; color: #696969;">Please login to the app using these credentials -</p>
                            <p style="font-family: Arial; font-size: 16px; font-weight: bold; color: #087abd; padding-bottom: 10px; margin: 0;">Username: '.$data['email'].' OR '. $data['phone'].'</p>
                            <p style="font-family: Arial; font-size: 16px; font-weight: bold; color: #087abd; padding: 0; margin: 0;">Password: '.$data['password'].' </p>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top" style="padding-top:40px; height: 60px;">
                            <a href="'.$data['loginurl'].'" style="background-color: #f0632b; color: #ffffff; text-decoration: none; font-family: Arial; font-size: 14px; line-height: 24px; padding-top:15px; padding-bottom: 15px; padding-left: 60px; padding-right: 60px;">Click here</a>
                        </td>
                    </tr>

                    <tr>
                        <td align="center" valign="top" style="padding-top:20px;">
                            <a href="'.$data['profileURL'].'" style="text-decoration: none;padding-top:15px; padding-bottom: 15px; padding-left: 60px; padding-right: 60px;"><img style="width: 300px;" src="'.$data['cardimage'].'"/></a>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="middle" style="background-color: #eaeaea; padding-top: 10px; padding-bottom: 10px;"><p style="font-family: Arial; font-size: 12px; line-height: 16px; margin: 0; padding: 0; color: #3892c7;">&copy; COPYRIGHT 2018 MY CONTACT CARD. ALL RIGHTS RESERVED.</p></td>
                    </tr>
                </tbody>
            </table>
        </body>
        </html>';*/

        $emailData = array();                        
        $emailData['email_subject'] = "Joining announcement.";            
        //$emailData['email_body'] = "Email:".$data["email"].", Phone: ".$data["phone"].", Password: ".$data["password"];            
        $emailData['email_body'] = $emailhtml;
        $emailData['to_email'] = $data['email'];

        $CURLOPT_POSTFIELDS = array(
            'To' => $emailData['to_email'],
            'Subject' => $emailData['email_subject'],
            'MAilBody' => $emailData['email_body']
        );

        $response = callApi(API_URL_ENDPOINT . '/webapi/Mail/SendMail', 'POST', $CURLOPT_POSTFIELDS);
    }

    public function AddOCRContactWithExisting($data, $file = []) {
        
        if($data['BUID']== "" || $data['BUID']==0){
            throw new Exception(__t("BUID 0"));
        }

        if($data['customerid']== "" || $data['customerid']==0){
            throw new Exception(__t("customerid 0"));
        }
        if($data['cardVaultID']== "" || $data['cardVaultID']==0){
            throw new Exception(__t("cardVaultID 0"));
        }
        if($data['cardOCRCustomerID']== "" || $data['cardOCRCustomerID']==0){
            throw new Exception(__t("cardOCRCustomerID 0"));
        }
        
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_AddOCRExistingContact(
            @BUID=:BUID,
            @customerid=:customerid,
            @cardVaultID=:cardVaultID,
            @cardOCRCustomerID=:cardOCRCustomerID
        )}");
        
        $statement->bindParam(':BUID', $data['BUID'], PDO::PARAM_INT);
        $statement->bindParam(':customerid', $data['customerid'], PDO::PARAM_INT);
        $statement->bindParam(':cardVaultID', $data['cardVaultID'], PDO::PARAM_INT);
        $statement->bindParam(':cardOCRCustomerID', $data['cardOCRCustomerID'], PDO::PARAM_INT);
        
        if (!$statement->execute()) {
            throw new Exception(__t("Sorry person not added"));
        }

        $rows = $statement->fetchAll(PDO::FETCH_ASSOC);         
        $statement->closeCursor();
        
        return $rows;
    }    

    public function GetCustomerCardVaultList($data) {

        if($data['BUID']==""){
            throw new Exception(__t("Enter BUID."));
        }
        if($data['customerid']==""){
            throw new Exception(__t("Enter customerid."));
        }
        if($data['start']==''){
            $data['start']=0;
        }
        if($data['limit']==''){
            $data['limit']=20;
        }
        if($data['searchstring']==""){
            $data['searchstring']=null;
        }
        
                        
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_GetCustomerCardVaultList("
            . "@BUID=:buid,"
            . "@customerid=:customerid,"
            . "@Start=:start,"
            . "@Limit=:limit,"
            . "@SearchString=:searchstring"
            . ")}");

        $statement->bindParam(':buid', $data['BUID'], PDO::PARAM_INT);
        $statement->bindParam(':customerid', $data['customerid'], PDO::PARAM_INT);
        $statement->bindParam(':start', $data['start'], PDO::PARAM_INT);
        $statement->bindParam(':limit', $data['limit'], PDO::PARAM_INT);
        $statement->bindParam(':searchstring', $data['searchstring'], PDO::PARAM_STR);

        $statement->execute();

        $x = 0;        
        $data1 = []; 
        if($data['id']===null){   
            do {            
                $rows = $statement->fetchAll(PDO::FETCH_ASSOC); 

                switch ($x) {                
                    case 0:                    
                        $data1['Pagination'] = [                        
                                            'total' => (int) $rows[0]['totalCards'],                        
                                            'start' => $data['start'],                        
                                            'limit' => $data['limit']                    
                                        ];                    
                        break;                
                    case 1:                    
                        $data1['Cards'] = $rows;                    
                        break;            
                }            
                $x++;                 
            } while ($statement->nextRowset());
        }
        else{
            $data1=$statement->fetchAll(PDO::FETCH_ASSOC);  
        }

        return $data1;
    }

    public function deleteOCRContact($data) {
        if($data['BUID']==""){
            throw new Exception(__t("Enter BUID."));
        }
        if($data['customerid']==""){
            throw new Exception(__t("Enter Customer Id."));
        }  
        if($data['ocrcontactid']==""){
            throw new Exception(__t("Enter ocr contact Id."));
        }        
                   
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_deleteOCRContact(
            @id=:id,
            @BUID=:buid,
            @customerid=:customerid
        )}");
        $statement->bindParam(':id', $data['ocrcontactid'], PDO::PARAM_INT);
        $statement->bindParam(':buid', $data['BUID'], PDO::PARAM_INT);
        $statement->bindParam(':customerid', $data['customerid'], PDO::PARAM_INT);
        
        if (!$statement->execute()) {
            throw new Exception(__t("Sorry contact not deleted"));
        }
        //$data1=$statement->fetchAll(PDO::FETCH_ASSOC);
        $statement->closeCursor();
        return true;
    }

}
