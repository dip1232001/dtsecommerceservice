<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProductModel
 *
 * @author dipankar
 */
require_once MODEL_PATH . DS . 'App.php';
require_once MODEL_PATH . DS . 'CategoryModel.php';
require_once MODEL_PATH . DS . 'PriceModel.php';
require_once MODEL_PATH . DS . 'ProductStockModel.php';
require_once MODEL_PATH . DS . 'VarientModel.php';
require_once MODEL_PATH . DS . 'ProductImageModel.php';

class CopyEcomDataModel extends AppModel
{

    public function __construct($callAuth = false)
    {
        parent::__construct($callAuth);
    }

    public function CopyData($data)
    {
        $fromBUID = $data['fromBUID'];
        $toBUID = $data['toBUID'];

        $CategoryModel = new CategoryModel();
        $ProductModel = new ProductModel();

        $catMap = [];

        $categories = $this->GetCategories(['BUID'=>$fromBUID]);
        //dd($categories, true);
        $products = $ProductModel->Getproducts(0, 10000, [], '', '', $fromBUID, null);
        $prodArr = [];
        foreach ($products['Products'] as $key => $value) {
            $prod = $ProductModel->getProduct($value['id'],$fromBUID);
            $prodArr[] = $prod['ProductDetails'];
        }

        foreach ($categories['categories'] as $key => $value) {
            $value['BUID'] = $toBUID;
            $catMap[$value['id']] = $this->addCategory($value, 'Add');
        }
        $categoriesN = $this->GetCategories(['BUID'=>$toBUID]);
        foreach ($categoriesN['categories'] as $key => $value) {
            if($value['parent_id'] > 0){
                $value['BUID'] = $toBUID;
                $value['parent_id'] = $catMap[$value['parent_id']];
                $this->addCategory($value, 'Edit');
            }
           
        }
        //dd($catMap);exit;
        foreach ($prodArr as $key => $value) {
            $value['BUID'] = $toBUID;
            $this->addProduct($value,$catMap);
        }
        //dd($categories['categories']);
        //dd($prodArr, true);
        return $data;
    }

    public function GetCategories($data)
    {
        //dd($data,true);
        if (!empty($data['BUID'])) {
            $this->BUID = $data['BUID'];
        } else {
            $this->CheckAuthenticated();
        }
        if ($this->BUID == null) {
            throw new Exception("Sorry BUID is required");
        }
        if (empty($data['category_slug'])) {
            $data['category_slug'] = '';
        }

        $statement = $this->PDO->prepare("{ CALL PHP_Ecommerce_GetCategoriesForCopy(@BUID=:buid,@ParentCategory=:parent_id, @Likename = :LikeName, @id=:id, @slug=:slug)}");
        $statement->bindParam(':buid', $this->BUID);
        if (!empty($data['parent_id']) or $data['parent_id'] == 0) {
            $statement->bindParam(':parent_id', $data['parent_id'], PDO::PARAM_INT);
        } else {
            $statement->bindParam(':parent_id', $data['parent_id'] = null, PDO::PARAM_INT);
        }
        if (!empty($data['name'])) {
            $statement->bindParam(':LikeName', $data['name'], PDO::PARAM_STR);
        } else {
            $statement->bindParam(':LikeName', $data['name'] = null, PDO::PARAM_INT);
        }

        if (!empty($data['category_id'])) {
            $statement->bindParam(':id', $data['category_id'], PDO::PARAM_INT);
        } else {
            $statement->bindParam(':id', $data['category_id'] = null, PDO::PARAM_INT);
        }
        if (!empty($data['category_slug'])) {
            $statement->bindParam(':slug', $data['category_slug'], PDO::PARAM_STR);
        } else {
            $statement->bindParam(':slug', $data['category_slug'] = null, PDO::PARAM_INT);
        }

        $statement->execute();
        $rows = $statement->fetchAll(PDO::FETCH_ASSOC);
        return [
            'parent_id' => $data['parent_id'],
            'categories' => $rows,
        ];
    }

    public function GetCategoriesTree($BUID, $parent_id, $data)
    {
        if (!empty($BUID)) {
            $this->BUID = $BUID;
        } else {
            $this->CheckAuthenticated();
        }

        $statement = $this->PDO->prepare("{ CALL PHP_Ecommerce_GetCategories(@BUID=:buid,@ParentCategory=:parent_id, @Likename = :LikeName, @id=:id, @slug=:slug)}");
        $statement->bindParam(':buid', $this->BUID);
        $statement->bindParam(':parent_id', $parent_id, PDO::PARAM_INT);

        if (!empty($data['name'])) {
            $statement->bindParam(':LikeName', $data['name'], PDO::PARAM_STR);
        } else {
            $statement->bindParam(':LikeName', $data['name'] = null, PDO::PARAM_INT);
        }

        if (!empty($data['category_id'])) {
            $statement->bindParam(':id', $data['category_id'], PDO::PARAM_INT);
        } else {
            $statement->bindParam(':id', $data['category_id'] = null, PDO::PARAM_INT);
        }

        if (!empty($data['slug'])) {
            $statement->bindParam(':slug', $data['slug'], PDO::PARAM_STR);
        } else {
            $statement->bindParam(':slug', $data['slug'] = null, PDO::PARAM_INT);
        }

        $statement->execute();
        $rows = $statement->fetchAll(PDO::FETCH_ASSOC);
        $ProductModel = new ProductModel();

        if (count($rows) > 0) {
            foreach ($rows as $key => $value) {
                # code...
                $rows[$key]['chieldren'] = $this->GetCategoriesTree($this->BUID, $value['id'], $data);
                $tp = 0;
                foreach ($rows[$key]['chieldren'] as $key1 => $value1) {
                    $tp = $tp + $value1["TotalProducts"];
                }
                $rows[$key]['TotalProducts'] = $value['total_products'] + $tp;
                /*if($value['total_products']>0){
                    $ppp = $ProductModel->Getproducts(0, $value['total_products'], [], $value['id'], '', $this->BUID, null);
                    $rows[$key]['products'] = $ppp['Products'];
                }*/
            }
        }else{
            //$rows['TotalProducts'] = $value['total_products'];
        }
        return $rows;
    }

    public function Getproducts($start = 0, $limit = 10, $BUID) {
        
        $stateMent = $this->PDO->prepare("{CALL PHP_Ecommerce_Getproducts(@buid = :buid, @Start = :Start, @Limit = :Limit, @Likename = :LikeName, @INDUSTRYID = :Industry_ID, @CATAGORYID = :Category_id, @SHOWALL = :ShowAll, @PRICEOVER = :PriceOver, @ISFEATURED = :IsFeatured, @categoryslug=:categoryslug)}");
        $stateMent->bindParam(':buid', $BUID, PDO::PARAM_INT);
        $stateMent->bindParam(':Start', $start, PDO::PARAM_INT);
        $stateMent->bindParam(':Limit', $limit, PDO::PARAM_INT);

        $stateMent->bindParam(':LikeName', $name = null, PDO::PARAM_STR);
        $stateMent->bindParam(':Industry_ID', $searchparams['industry_id'] = null, PDO::PARAM_INT);
        $stateMent->bindParam(':Category_id', $searchparams['category_id'] = null, PDO::PARAM_INT);
        $stateMent->bindParam(':ShowAll', $searchparams['showall'] = null, PDO::PARAM_INT);
        $stateMent->bindParam(':PriceOver', $searchparams['priceover'] = null, PDO::PARAM_INT);
        $stateMent->bindParam(':IsFeatured', $searchparams['isfeatured'] = null, PDO::PARAM_INT);
        
        
       
        $stateMent->bindParam(':categoryslug', $category_slug = null, PDO::PARAM_STR);

        $stateMent->execute();
        do {
            $rows = $stateMent->fetchAll(PDO::FETCH_ASSOC);

            if ($rows) {
                if (isset($rows[0]['TotalProducts'])) {
                    $data['Paginations'] = [
                        'Total' => $rows[0] ['TotalProducts'],
                        'Start' => $start,
                        'limit' => $limit
                    ];
                } else {
                    $data['Products'] = $rows;
                }
            }
        } while ($stateMent->nextRowset());
        $stateMent->closeCursor();
        if (empty($data['Products'])) {
            $data['Products'] = [];
        }

        return $data;
    }

    public function addCategory($data, $type)
    {
        $CategoryID = 0 ;
        $addStatement = $this->PDO->prepare("{CALL PHP_Ecommerce_AddCategory (@BUID=:buid,@Categoryname=:categoryname,@Image=:image,@ParentID=:parent_id,@ISACTIVE=:IS_Active,@slug=:slug,@displayorder=:displayorder,@ID=:ID, @CATEGORYID=:CategoryID)}");
        $addStatement->bindParam(':buid', $data['BUID'], PDO::PARAM_INT);
        $addStatement->bindParam(':categoryname', $data['name'], PDO::PARAM_STR);
        if (empty($data['image'])) {
            $addStatement->bindParam(':image', $imagename = null, PDO::PARAM_INT);
        } else {
            $addStatement->bindParam(':image', $data['image'], PDO::PARAM_STR);
        }
        $addStatement->bindParam(':parent_id', $data['parent_id'], PDO::PARAM_INT);
        $addStatement->bindParam(':IS_Active', $data['is_active'], PDO::PARAM_INT);        
        $addStatement->bindParam(':slug', $data['slug'], PDO::PARAM_STR);
        $addStatement->bindParam(':displayorder', $data['displayorder'], PDO::PARAM_INT); 
        if($type == 'Add'){
            $addStatement->bindParam(':ID', $id = null, PDO::PARAM_INT); 
        } else {
            $addStatement->bindParam(':ID', $data['id'], PDO::PARAM_INT); 
        }       
               
        $addStatement->bindParam(':CategoryID', $CategoryID, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000);
                
        $addStatement->execute();
        return $CategoryID;
    }

    public function addProduct($data, $catMap) {

        $inserProductStatement = $this->PDO->prepare("{CALL PHP_Ecommerce_AddProducts ("
                . "@Name=:Name,"
                . "@description=:description,"
                . "@sku=:sku,"
                . "@category_id=:category_id,"
                . "@PriceTypeID=:PriceTypeID,"
                . "@Amount=:Amount,"
                . "@stock=:stock,"
                . "@buid=:buid,"
                . "@slug=:slug,"
                . "@brand_id=:brand_id,"
                . "@supplier_id=:supplier_id,"
                . "@supplier_code=:supplier_code,"
                . "@sales_account_code=:sales_account_code,"
                . "@purchase_account_code=:purchase_account_code,"
                . "@units=:units,"
                . "@VARIENTS=:VARIENTS,"
                . "@PRODUCTIMAGES=:PRODUCTIMAGES,"
                . "@PRODUCTVIDEOS=:PRODUCTVIDEOS,"
                . "@PRODUCTDOCUMENTS=:PRODUCTDOCUMENTS,"
                . "@PRODUCTARTAGS=:PRODUCTARTAGS," 
                . "@ID=:ID,"
                . "@ISDISBALED=:IsDisbaled,"
                . "@ISFEATURED=:IsFeatured,"
                . "@VIDEOURL=:videoUrl,"
                . "@is_virtual=:is_virtual,"
                . "@PRODUCTID=:PRODUCTID"
                . ")}");

        $inserProductStatement->bindParam(':Name', $data['Productname'], PDO::PARAM_STR);
        $inserProductStatement->bindParam(':description', strip_tags( $data['description'],'<br><hr><blockquote><u><i><a><p><div><table><td><tr><code><pre><ul><ol><li><span><article><h1><h2><h3><h4><h5><h6><title><abbr><img><b><strong>'), PDO::PARAM_STR);
        $inserProductStatement->bindParam(':sku', $data['sku'], PDO::PARAM_STR);
        
        if (!empty($data['CATEGORIES'])) {
            $ProductCategories = $this->prepareCategotyXML($data["CATEGORIES"], $catMap);
            if (!empty($ProductCategories)) {
                $inserProductStatement->bindParam(':category_id', $ProductCategories, PDO::PARAM_STR);
            } else {
                $inserProductStatement->bindParam(':category_id', $ProductCategories = null, PDO::PARAM_INT);
            }
        }else{
            $inserProductStatement->bindParam(':category_id', $PRODUCTVIDEOS = null, PDO::PARAM_INT);
        }
        
        $inserProductStatement->bindParam(':PriceTypeID', $data['PriceTypeID'], PDO::PARAM_INT);
        $inserProductStatement->bindParam(':Amount', $data['price'], PDO::PARAM_STR);
        $inserProductStatement->bindParam(':stock', $data['Stock'], PDO::PARAM_STR);
        $inserProductStatement->bindParam(':buid', $data['BUID'], PDO::PARAM_INT);
        $inserProductStatement->bindParam(':slug', $data['slug'] = null, PDO::PARAM_STR);
        $inserProductStatement->bindParam(':brand_id', $data['brand_id'] = null, PDO::PARAM_INT);
        $inserProductStatement->bindParam(':supplier_id', $data['supplier_id'] = null, PDO::PARAM_INT);
        $inserProductStatement->bindParam(':supplier_code', $data['supplier_code'] = null, PDO::PARAM_INT);
        $inserProductStatement->bindParam(':sales_account_code', $data['sales_account_code'] = null, PDO::PARAM_INT);
        $inserProductStatement->bindParam(':purchase_account_code', $data['purchase_account_code'] = null, PDO::PARAM_INT);
        if (!empty($data['PriceType'])) {
            $inserProductStatement->bindParam(':units', $data['PriceType'], PDO::PARAM_STR);
        } else {
            $inserProductStatement->bindParam(':units', $data['PriceType'] = null, PDO::PARAM_INT);
        }
        $this->PrepareVarientXmlFromArray($data['ProductVarients']);
        if (!empty($data['ProductVarients']['XML'])) {
            $inserProductStatement->bindParam(':VARIENTS', $data['ProductVarients']['XML'], PDO::PARAM_STR);
        } else {
            $inserProductStatement->bindParam(':VARIENTS', $data['ProductVarients'] = null, PDO::PARAM_INT);
        }           

        if (!empty($data['ProductImages'])) {            
            $ProductImages = $this->prePareImageXml($data['ProductImages']);
            if (!empty($ProductImages)) {
                $inserProductStatement->bindParam(':PRODUCTIMAGES', $ProductImages, PDO::PARAM_STR);
            } else {
                $inserProductStatement->bindParam(':PRODUCTIMAGES', $ProductImages = null, PDO::PARAM_INT);
            }
        } else {
            $inserProductStatement->bindParam(':PRODUCTIMAGES', $ProductImages = null, PDO::PARAM_INT);
        }
        if (!empty($data['VIDEOS'])) {
            $ProductVideos = $this->prepareVideoXML($data["VIDEOS"]);
            //dd($ProductVideos, true);
            if (!empty($ProductVideos)) {
                $inserProductStatement->bindParam(':PRODUCTVIDEOS', $ProductVideos, PDO::PARAM_STR);
            } else {
                $inserProductStatement->bindParam(':PRODUCTVIDEOS', $ProductVideos = null, PDO::PARAM_INT);
            }
        }else{
            $inserProductStatement->bindParam(':PRODUCTVIDEOS', $PRODUCTVIDEOS = null, PDO::PARAM_INT);
        }
        
        if(!empty($data['DOCUMENTS'])){
            $ProductDocuments = $this->prepareDocumentXML($data['DOCUMENTS']);
            if (!empty($ProductDocuments)) {
                $inserProductStatement->bindParam(':PRODUCTDOCUMENTS', $ProductDocuments, PDO::PARAM_STR);
            } else {
                $inserProductStatement->bindParam(':PRODUCTDOCUMENTS', $ProductDocuments = null, PDO::PARAM_INT);
            }
        }else{
            $inserProductStatement->bindParam(':PRODUCTDOCUMENTS', $PRODUCTDOCUMENTS=null, PDO::PARAM_INT);
        }  
        
        if (!empty($data['ARTAGS'])) {
            $ProductARTags = $this->prepareARTagXML($data["ARTAGS"]);
            //dd($ProductTags, true);
            if (!empty($ProductARTags)) {
                $inserProductStatement->bindParam(':PRODUCTARTAGS', $ProductARTags, PDO::PARAM_STR);
            } else {
                $inserProductStatement->bindParam(':PRODUCTARTAGS', $ProductARTags = null, PDO::PARAM_INT);
            }
        }else{
            $inserProductStatement->bindParam(':PRODUCTARTAGS', $PRODUCTARTAGS = null, PDO::PARAM_INT);
        }

        $inserProductStatement->bindParam(':ID', $data['id'] = null, PDO::PARAM_INT);

        
        if (empty($data['is_disbaled'])) {
            $data['is_disbaled'] = 0;
        }
        if (empty($data['is_featured'])) {
            $data['is_featured'] = 0;
        }
        $inserProductStatement->bindParam(':IsFeatured', $data['is_featured'], PDO::PARAM_INT);
        $inserProductStatement->bindParam(':IsDisbaled', $data['is_disbaled'], PDO::PARAM_INT);
        $inserProductStatement->bindParam(':videoUrl', $data['videoUrl'] = null, PDO::PARAM_INT);

        if (empty($data['is_virtual'])) {
            $inserProductStatement->bindParam(':is_virtual', $data['is_virtual'] = 0, PDO::PARAM_INT);
        } else {            
            $inserProductStatement->bindParam(':is_virtual', $data['is_virtual'], PDO::PARAM_STR);
        }
        $ProductID = null;
        $inserProductStatement->bindParam(':PRODUCTID', $ProductID, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 250);
        
        $inserProductStatement->execute();
        if (empty($ProductID)) {
            throw new Exception(__t("Sorry there was a problem while saving this product"));
        }
        
        return [
            'id' => $ProductID
        ];
    }

    public function prepareCategotyXML($categories, $cat) {
        $FileXml = '';
        foreach ($categories as $key => $value) {
            if(!empty($value['category_id'])){
                $FileXml .= '<categories>';
                $FileXml .= '<categoryid>' . $cat[$value['category_id']] . '</categoryid>';
                $FileXml .= '<displayorder>' . $value['productorder'] . '</displayorder>';
                $FileXml .= '</categories>';
            }            
        }
        if (strlen(trim($FileXml)) > 0) {
            return '<data>' . $FileXml . '</data>';
        }
        return false;
    }

    public function PrepareVarientXmlFromArray(&$varients) {
        if (!empty($varients)) {
            $varients['XML'] = '<data>';

            foreach ($varients as $key => $varient) {
                if (!is_numeric($key)) {
                    continue;
                }
                if (empty($varient['name'])) {
                    throw new Exception(__t("Sorry varient name is required"));
                }
                if (empty($varient['options'])) {
                    throw new Exception(__t("Sorry varient options are required"));
                }

                $varients['XML'] .= '<varient>';
                $varients['XML'] .= '<is_required>' . trim(strip_tags($varient['is_required'])) . '</is_required>';
                $varients['XML'] .= '<name><![CDATA[' . (trim(strip_tags($varient['name']))) . ']]></name>';
                $varients['XML'] .= '<sort_order>' . (int) (empty($varient['sort_order']) ? 1 : trim(strip_tags($varient['sort_order']))) . '</sort_order>';
                $varients['XML'] .= '<is_multiple><![CDATA[' . (empty($varient['is_multiple']) ? 0 : trim(strip_tags($varient['is_multiple']))) . ']]></is_multiple>';
                $varients['XML'] .= '<childrens>';
                foreach ($varient['options'] as $varientoption) {
                    if (strtolower($varientoption['title']) == 'select') {
                        continue;
                    }
                    if (empty($varientoption['title'])) {
                        throw new Exception("Sorry varient option title can not be blank");
                    }
                    if (empty($varientoption['pricetype'])) {
                        throw new Exception("Sorry varient option pricetype can not be blank");
                    }
                    if (trim($varientoption['pricetype']) == "+") {
                        $varientoption['pricetype'] = 1;
                    } else {
                        $varientoption['pricetype'] = 0;
                    }
                    if ($varientoption['price'] == '0.00' || $varientoption['price'] == '.00' || empty($varientoption['price'])) {
                        $varientoption['price'] = 0;
                    }
                    $varients['XML'] .= '<children>';
                    $varients['XML'] .= '<title><![CDATA[' . (trim(trim(strip_tags($varientoption['title'])))) . ']]></title>';
                    $varients['XML'] .= '<price><![CDATA[' . trim(strip_tags($varientoption['price'])) . ']]></price>';
                    $varients['XML'] .= '<pricetype><![CDATA[' . (string) ((empty($varientoption['pricetype'])) ? trim(strip_tags($varientoption['pricetype'])) : 1 ) . ']]></pricetype>';
                    $varients['XML'] .= '<sort_order>' . (int) (empty($varientoption['sort_order']) ? 1 : trim(strip_tags($varientoption['sort_order']))) . '</sort_order>';
                    $varients['XML'] .= '</children>';
                }
                $varients['XML'] .= '</childrens>';

                $varients['XML'] .= '</varient>';
            }
            $varients['XML'] .= '</data>';
        }
        return false;
    }

    public function prePareImageXml($images) {

        $FileXml = '';
        foreach ($images as $key => $value) {  
            $FileXml .= '<images>';
            $FileXml .= '<name><![CDATA[' . $value['name'] . ']]></name>';
            $FileXml .= '<imagePath><![CDATA[' . $value['imagePath'] . ']]></imagePath>';
            $FileXml .= '<default_image><![CDATA[' . $value['default_image'] . ']]></default_image>';
            $FileXml .= '<dispalyorder>' . $value['displayorder'] . '</dispalyorder>';
            $FileXml .= '</images>';
        }
        if (strlen(trim($FileXml)) > 0) {
            return '<data>' . $FileXml . '</data>';
        }
        return false;
    }

    public function prepareVideoXML($videos) {
        $FileXml = '';
        foreach ($videos as $key => $value) {
            $FileXml .= '<videos>';
            $FileXml .= '<name>' . strip_tags(trim($value['name'])) . '</name>';
            $FileXml .= '<videoURL>' . base64_encode(trim($value['videoURL'])) . '</videoURL>';
            $FileXml .= '<default_video>' . $value['default_video'] . '</default_video>';
            $FileXml .= '</videos>';
        }
        if (strlen(trim($FileXml)) > 0) {
            return '<data>' . $FileXml . '</data>';
        }
        return false;
    }

    public function prepareDocumentXML($FILESD) {
        $FileXmlD = '';
        foreach ($FILESD as $key => $value) {
            $FileXmlD .= '<documents>';
            $FileXmlD .= '<name><![CDATA[' . strip_tags(trim($value['name'])) . ']]></name>';
            $FileXmlD .= '<docURL><![CDATA[' . $value['docURL'] . ']]></docURL>';
            $FileXmlD .= '<default_doc><![CDATA[' . $value['default_doc'] . ']]></default_doc>';
            $FileXmlD .= '</documents>';
        }
        if (strlen(trim($FileXmlD)) > 0) {
            return '<data>' . $FileXmlD . '</data>';
        }
        return false;
    }

    public function prepareARTagXML($tag) {
        $tags = explode(',', $tag);
        $FileXml = '';
        foreach ($tags as $key => $value) {
            $FileXml .= '<artags>';
            $FileXml .= '<name>' . strip_tags(trim($value)) . '</name>';
            $FileXml .= '</artags>';
        }
        if (strlen(trim($FileXml)) > 0) {
            return '<data>' . $FileXml . '</data>';
        }
        return false;
    }




}
