<?php

use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CouponModel
 *
 * @author dipankar
 */
require_once MODEL_PATH . DS . 'App.php';

class CouponModel extends AppModel {

    //put your code here
    private $MapTable = 'Product_Category_Map';
    private $Table = 'categories';

    public function __construct($callAuth = false) {
        parent::__construct();
        //$this->CheckAuthenticated();
    }

    public function GetCouponTypes() {
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_GetCouponTypes()}");
        $statement->execute();
        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    public function GetCouponTypeSpecial() {
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_GetCouponTypeSpecial()}");
        $statement->execute();
        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    public function addCoupon($data) {
        //dd($data,true);
        if(!empty($data['BUID'])){
            $this->BUID = $data['BUID'];
        }else{
            $this->CheckAuthenticated();
        }
        if($this->BUID == null){
            throw new Exception("Sorry BUID is required");
        }
        if (empty($data['name'])) {
            throw new Exception("Sorry coupon name is required");
        }
        if (empty($data['code'])) {
            throw new Exception("Sorry coupon code is required");
        }
        if (empty($data['date_of_issue'])) {
            throw new Exception("Sorry coupon issue date is required");
        }
        if (empty($data['expiry_date'])) {
            throw new Exception("Sorry coupon expiry date is required");
        }
        if (empty($data['discount_type'])) {
            throw new Exception("Sorry coupon discount type is required");
        }
        if (empty($data['discount'])) {
            throw new Exception("Sorry discount is required");
        }
        if (empty($data['select_rule'])) {
            throw new Exception("Sorry coupon discount rule is required");
        }
        if (empty($data['discount_on_price'])) {
            throw new Exception("Sorry coupon discount on price is required");
        }
        if (empty($data['type'])) {
            throw new Exception("Sorry coupon type is required");
        }
        if (empty($data['description'])) {
            throw new Exception("Sorry coupon type is required");
        }
        /*if ($data['status']=='') {
            throw new Exception("Sorry coupon status is required");
        }*/
        //$this->__checkDuplicate($data['name']);
        /*if($data['discount_type'] == "Percentage"){
            $description = "If your order amount is ".$data['select_rule']." ".$data['discount_on_price']." then you will get ".$data['discount']."% off . MAX ".$data['maximumDiscount'];
        }else{
            $description = "If your order amount is ".$data['select_rule']." ".$data['discount_on_price']." then you will get ".$data['discount']." off";
        }*/
        
        
        $CouponID = null;
        $addStatement = $this->PDO->prepare("{CALL PHP_Ecommerce_Coupons_Add (@buid = :buid,@name = :name,@code = :code,@date_of_issue = :date_of_issue,@expiry_date = :expiry_date,@discount_type = :discount_type,@discount = :discount,@select_rule = :select_rule,@discount_on_price = :discount_on_price,@status = :status,@type = :type,@maximumDiscount = :maximumDiscount,@specialType = :specialType,@useTotalOrderValue = :useTotalOrderValue,@useTotalNoOfOrder = :useTotalNoOfOrder,@description=:description,@ID=:ID, @COUPONEID=:CouponID)}");
        //$addStatement = $this->PDO->prepare("INSERT INTO {$this->DbAlias}{$this->Table} (name,buid,image,parent_id) VALUES(:categoryname,:buid,:image,:parent_id)");
        $addStatement->bindParam(':name', $data['name'], PDO::PARAM_STR);
        $addStatement->bindParam(':code', $data['code'], PDO::PARAM_STR);
        $addStatement->bindParam(':date_of_issue', $data['date_of_issue'], PDO::PARAM_STR);
        $addStatement->bindParam(':expiry_date', $data['expiry_date'], PDO::PARAM_STR);
        $addStatement->bindParam(':discount_type', $data['discount_type'], PDO::PARAM_STR);
        $addStatement->bindParam(':discount', $data['discount'], PDO::PARAM_STR);
        $addStatement->bindParam(':select_rule', $data['select_rule'], PDO::PARAM_STR);
        $addStatement->bindParam(':discount_on_price', $data['discount_on_price'], PDO::PARAM_STR);
        $addStatement->bindParam(':buid', $this->BUID, PDO::PARAM_INT);
        //dd($data['status'], true);
        if (!empty($data['status']) || is_numeric($data['status'])) {
            $isActive = $data['status'];
        } else {
            $isActive = 1;
        }
        $addStatement->bindParam(':status', $isActive, PDO::PARAM_INT);
        $addStatement->bindParam(':type', $data['type'], PDO::PARAM_INT);
        $addStatement->bindParam(':maximumDiscount', $data['maximumDiscount'], PDO::PARAM_STR);
        if (!empty($data['specialType']) || is_numeric($data['specialType'])) {
            $specialType = $data['specialType'];
        } else {
            $specialType = NULL;
        }
        $addStatement->bindParam(':specialType', $specialType, PDO::PARAM_INT);
        if (!empty($data['useTotalOrderValue']) || is_numeric($data['useTotalOrderValue'])) {
            $useTotalOrderValue = $data['useTotalOrderValue'];
        } else {
            $useTotalOrderValue = NULL;
        }
        $addStatement->bindParam(':useTotalOrderValue', $useTotalOrderValue, PDO::PARAM_INT);
        if (!empty($data['useTotalNoOfOrder']) || is_numeric($data['useTotalNoOfOrder'])) {
            $useTotalNoOfOrder = $data['useTotalNoOfOrder'];
        } else {
            $useTotalNoOfOrder = NULL;
        }
        $addStatement->bindParam(':useTotalNoOfOrder', $useTotalNoOfOrder, PDO::PARAM_INT);
        $addStatement->bindParam(':description', $data['description'], PDO::PARAM_INT);
        
        if (empty($data['id'])) {
            $addStatement->bindParam(':ID', $id = null, PDO::PARAM_INT);
        } else {
            $addStatement->bindParam(':ID', $id = $data['id'], PDO::PARAM_INT);
        }
        $addStatement->bindParam(':CouponID', $CouponID, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000);
        
        $addStatement->execute();
        return [
            'id' => empty($data['id']) ? $CouponID : $data['id'],
            'name' => $data['name'],
            'code' => $data['code'],
            'date_of_issue' => $data['date_of_issue'],
            'expiry_date' => $data['expiry_date'],
            'discount_type' => $data['discount_type'],
            'discount' => $data['discount'],
            'select_rule' => $data['select_rule'],
            'discount_on_price' => $data['discount_on_price'],
            'status' => $data['status'],
            'msg' => empty($data['id']) ? __t("Coupon added succesfully") : __t("Coupon updated succesfully")
        ];
    }
    
    public function getCoupon($id, $BUID) {
        if(!empty($BUID)){
            $this->BUID = $BUID;
        }else{
            $this->CheckAuthenticated();
        }
        if($this->BUID == null){
            throw new Exception("Sorry BUID is required");
        }
        $getStatement = $this->PDO->prepare("{CALL PHP_ECOMMERCE_GetCoupons (@buid = :buid, @id=:id)}");
        
        $getStatement->bindParam(':id', $id, PDO::PARAM_INT);
        $getStatement->bindParam(':buid', $this->BUID, PDO::PARAM_INT);
        $getStatement->execute();
        
        $coupons = $getStatement->fetchAll(PDO::FETCH_ASSOC);
        if (!empty( $coupons )) {
            $coupons= array_map(array($this, "json_decode_summary"), $coupons);
            
        }
        
        return [
            'coupons' => $coupons
        ];       
        
    }
    
    public function json_decode_summary(&$array)
    {
        $array['dateofissueformated'] = date('Y-m-d', strtotime($array['date_of_issue']));
        $array['expirydateformated'] = date('Y-m-d', strtotime($array['expiry_date']));
        return $array;
    }

    public function getCouponByBU($id, $BUID) {
        if(!empty($BUID)){
            $this->BUID = $BUID;
        }else{
            $this->CheckAuthenticated();
        }
        if($this->BUID == null){
            throw new Exception("Sorry BUID is required");
        }

        $statement = $this->PDO->prepare("{ CALL PHP_Ecommerce_GetGeneralSettings(@BUID=:buid)}");
        $statement->bindParam(':buid', $this->BUID, PDO::PARAM_INT);
        $statement->execute();
        $gs =  $statement->fetch(PDO::FETCH_ASSOC);


        if($gs["couponEnable"] == 1){
            $getStatement = $this->PDO->prepare("{CALL PHP_ECOMMERCE_GetCouponsByBU (@buid = :buid, @id=:id)}");
                
            $getStatement->bindParam(':id', $id, PDO::PARAM_INT);
            $getStatement->bindParam(':buid', $this->BUID, PDO::PARAM_INT);
            $getStatement->execute();
            
            $coupons = $getStatement->fetchAll(PDO::FETCH_ASSOC);
            if (!empty( $coupons )) {
                $coupons= array_map(array($this, "json_decode_summary"), $coupons);
                
            }
        }else{
            $coupons=[];
        }

        
        
        return [
            'coupons' => $coupons
        ];       
        
    }

    public function getCouponByBUByType($id, $BUID) {
        if(!empty($BUID)){
            $this->BUID = $BUID;
        }else{
            $this->CheckAuthenticated();
        }
        if($this->BUID == null){
            throw new Exception("Sorry BUID is required");
        }

        if($id===null){
            $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_GetCouponTypes()}");
            $statement->execute();
            $CT = $statement->fetchAll(PDO::FETCH_ASSOC);

            foreach ($CT as $key => $value) {
                //dd($value, true);
                $getStatement = $this->PDO->prepare("{CALL PHP_ECOMMERCE_GetCouponsByBUType (@buid = :buid, @type=:type)}");
                $getStatement->bindParam(':type', $value["name"], PDO::PARAM_STR);
                $getStatement->bindParam(':buid', $this->BUID, PDO::PARAM_INT);
                $getStatement->execute();
                $coupons[$value["name"]] = $getStatement->fetchAll(PDO::FETCH_ASSOC);
            }
        }else{
            $getStatement = $this->PDO->prepare("{CALL PHP_ECOMMERCE_GetCouponsByBU (@buid = :buid, @id=:id)}");
            
            $getStatement->bindParam(':id', $id, PDO::PARAM_INT);
            $getStatement->bindParam(':buid', $this->BUID, PDO::PARAM_INT);
            $getStatement->execute();
            
            $coupons = $getStatement->fetchAll(PDO::FETCH_ASSOC);
        }
        
        return [
            'coupons' => $coupons
        ];       
        
    }

    public function getDiscount($data) {
        //dd($data,true);
        if(!empty($data['BUID'])){
            $this->BUID = $data['BUID'];
        }else{
            $this->CheckAuthenticated();
        }
        if($this->BUID == null){
            throw new Exception("Sorry BUID is required");
        }
        if (empty($data['COUPONECODE'])) {
            throw new Exception("Sorry coupon code is required");
        }
        if (empty($data['Gross'])) {
            throw new Exception("Sorry coupon Gross Amount is required");
        }
        if (empty($data['customerid'])) {
            $data['customerid'] = '';
        }
        if (empty($data['todaysdate'])) {
            $data['todaysdate'] = date('Y-m-d');
        }else{
            $data['todaysdate'] = date('Y-m-d',strtotime($data['todaysdate']));
        }
        $discount = 0;
        $message = '';
        if (!empty($data['COUPONECODE'])) {
            /////CALCULATE DISCOUNT
            $statement = $this->PDO->prepare("{CALL PHP_ECOMMERCE_GetCouponsByCODE("                
                . "@buid=:buid,"
                . "@code=:code,"
                . ")}");
            $statement->bindParam(':buid', $this->BUID, PDO::PARAM_INT);
            $statement->bindParam(':code', $data['COUPONECODE'], PDO::PARAM_STR);
            $statement->execute();
            $coupon = $statement->fetchAll(PDO::FETCH_ASSOC);
            //dd($coupon,true);
            if(count($coupon)>0){
                if($coupon[0]["CouponTypeKey"] == "general"){
                    if($coupon[0]["select_rule"] == ">"){
                        if($data['Gross'] > $coupon[0]["discount_on_price"]){
                            if($coupon[0]["discount_type"] == 'Percentage'){
                                $discount = ($data['Gross'] * $coupon[0]["discount"])/100;
                            }else{
                                $discount = $coupon[0]["discount"];
                            }
                        }else{
                            $message = 'Your cart amount does not match with the coupon ('.$data['COUPONECODE'].') minimum purchase amount.';
                        }
                    }
        
                    if($coupon[0]["select_rule"] == ">="){
                        if($data['Gross'] >= $coupon[0]["discount_on_price"]){
                            if($coupon[0]["discount_type"] == 'Percentage'){
                                $discount = ($data['Gross'] * $coupon[0]["discount"])/100;
                            }else{
                                $discount = $coupon[0]["discount"];
                            }
                        }else{
                            $message = 'Your cart amount does not match with the coupon ('.$data['COUPONECODE'].') minimum purchase amount.';
                        }
                    }
        
                    if($coupon[0]["select_rule"] == "<"){
                        if($data['Gross'] < $coupon[0]["discount_on_price"]){
                            if($coupon[0]["discount_type"] == 'Percentage'){
                                $discount = ($data['Gross'] * $coupon[0]["discount"])/100;
                            }else{
                                $discount = $coupon[0]["discount"];
                            }
                        }else{
                            $message = 'Your cart amount does not match with the coupon ('.$data['COUPONECODE'].') maximum purchase amount.';
                        }
                    }
        
                    if($coupon[0]["select_rule"] == "<="){
                        if($data['Gross'] <= $coupon[0]["discount_on_price"]){
                            if($coupon[0]["discount_type"] == 'Percentage'){
                                $discount = ($data['Gross'] * $coupon[0]["discount"])/100;
                            }else{
                                $discount = $coupon[0]["discount"];
                            }
                        }else{
                            $message = 'Your cart amount does not match with the coupon ('.$data['COUPONECODE'].') maximum purchase amount.';
                        }
                    }
        
                    if($coupon[0]["select_rule"] == "="){
                        if($data['Gross'] == $coupon[0]["discount_on_price"]){
                            if($coupon[0]["discount_type"] == 'Percentage'){
                                $discount = ($data['Gross'] * $coupon[0]["discount"])/100;
                            }else{
                                $discount = $coupon[0]["discount"];
                            }
                        }else{
                            $message = 'Your cart amount does not match with the coupon ('.$data['COUPONECODE'].') exact purchase amount.';
                        }
                    }   
                    
                }
                if($coupon[0]["CouponTypeKey"] == "returning" && $data['customerid'] !== ''){
                    //get total order and orrder value
                    $statementO = $this->PDO->prepare("{CALL PHP_ECOMMERCE_GetOrderAmtForCoupon("                
                        . "@buid=:buid,"
                        . "@customerid=:customerid,"
                        . ")}");
                    $statementO->bindParam(':buid', $this->BUID, PDO::PARAM_INT);
                    $statementO->bindParam(':customerid', $data['customerid'], PDO::PARAM_INT);
                    $statementO->execute();
                    $OA = $statementO->fetchAll(PDO::FETCH_ASSOC);
                    if($coupon[0]["useTotalOrderValue"] == null){
                        $coupon[0]["useTotalOrderValue"] = 0;
                    }
                    if($coupon[0]["useTotalNoOfOrder"] == null){
                        $coupon[0]["useTotalNoOfOrder"] = 0;
                    }
                    if($OA[0]['TA'] >= $coupon[0]["useTotalNoOfOrder"] && $OA[0]['TAMT'] >= $coupon[0]["useTotalOrderValue"]){
                        if($coupon[0]["select_rule"] == ">"){
                            if($data['Gross'] > $coupon[0]["discount_on_price"]){
                                if($coupon[0]["discount_type"] == 'Percentage'){
                                    $discount = ($data['Gross'] * $coupon[0]["discount"])/100;
                                }else{
                                    $discount = $coupon[0]["discount"];
                                }
                            }
                        }
            
                        if($coupon[0]["select_rule"] == ">="){
                            if($data['Gross'] >= $coupon[0]["discount_on_price"]){
                                if($coupon[0]["discount_type"] == 'Percentage'){
                                    $discount = ($data['Gross'] * $coupon[0]["discount"])/100;
                                }else{
                                    $discount = $coupon[0]["discount"];
                                }
                            }
                        }
            
                        if($coupon[0]["select_rule"] == "<"){
                            if($data['Gross'] < $coupon[0]["discount_on_price"]){
                                if($coupon[0]["discount_type"] == 'Percentage'){
                                    $discount = ($data['Gross'] * $coupon[0]["discount"])/100;
                                }else{
                                    $discount = $coupon[0]["discount"];
                                }
                            }
                        }
            
                        if($coupon[0]["select_rule"] == "<="){
                            if($data['Gross'] <= $coupon[0]["discount_on_price"]){
                                if($coupon[0]["discount_type"] == 'Percentage'){
                                    $discount = ($data['Gross'] * $coupon[0]["discount"])/100;
                                }else{
                                    $discount = $coupon[0]["discount"];
                                }
                            }
                        }
            
                        if($coupon[0]["select_rule"] == "="){
                            if($data['Gross'] == $coupon[0]["discount_on_price"]){
                                if($coupon[0]["discount_type"] == 'Percentage'){
                                    $discount = ($data['Gross'] * $coupon[0]["discount"])/100;
                                }else{
                                    $discount = $coupon[0]["discount"];
                                }
                            }
                        }    
                    }                
                }
                if($coupon[0]["CouponTypeKey"] == "special" && $data['customerid'] !== ''){                    
                    // get customer details
                    $statementC = $this->PDO->prepare("{CALL PHP_Ecommerce_GetCustomer(@BUID=:id, @CUSTOMERID=:customer_id)}");
                    $statementC->bindParam(':customer_id', $data['customerid'], PDO::PARAM_INT);
                    $statementC->bindParam(':id', $this->BUID, PDO::PARAM_INT);
                    $statementC->execute();
                    $CUS = $statementC->fetchAll(PDO::FETCH_ASSOC);                    
                    if(count($CUS)>0){
                        $CUSTOMER_DOB = $CUS[0]["DOB"];
                        $CUSTOMER_MAD = $CUS[0]["marriage_anniversary_date"];
                        $calculateDiscount = 0;
                        if(!empty($CUSTOMER_DOB) && $coupon[0]["SpecialCouponTypeKey"] == 'bday'){
                            $CUSTOMER_DOB = date('Y-m-d',strtotime($CUSTOMER_DOB));
                            if($CUSTOMER_DOB == $data['todaysdate']){
                                $calculateDiscount = 1;
                            }
                        }
                        if(!empty($CUSTOMER_MAD) && $coupon[0]["SpecialCouponTypeKey"] == 'maniversary'){
                            $CUSTOMER_MAD = date('Y-m-d',strtotime($CUSTOMER_MAD));
                            if($CUSTOMER_MAD == $data['todaysdate']){
                                $calculateDiscount = 1;
                            }
                        }
                        if($calculateDiscount == 1){
                            if($coupon[0]["select_rule"] == ">"){
                                if($data['Gross'] > $coupon[0]["discount_on_price"]){
                                    if($coupon[0]["discount_type"] == 'Percentage'){
                                        $discount = ($data['Gross'] * $coupon[0]["discount"])/100;
                                    }else{
                                        $discount = $coupon[0]["discount"];
                                    }
                                }
                            }                
                            if($coupon[0]["select_rule"] == ">="){
                                if($data['Gross'] >= $coupon[0]["discount_on_price"]){
                                    if($coupon[0]["discount_type"] == 'Percentage'){
                                        $discount = ($data['Gross'] * $coupon[0]["discount"])/100;
                                    }else{
                                        $discount = $coupon[0]["discount"];
                                    }
                                }
                            }                
                            if($coupon[0]["select_rule"] == "<"){
                                if($data['Gross'] < $coupon[0]["discount_on_price"]){
                                    if($coupon[0]["discount_type"] == 'Percentage'){
                                        $discount = ($data['Gross'] * $coupon[0]["discount"])/100;
                                    }else{
                                        $discount = $coupon[0]["discount"];
                                    }
                                }
                            }                
                            if($coupon[0]["select_rule"] == "<="){
                                if($data['Gross'] <= $coupon[0]["discount_on_price"]){
                                    if($coupon[0]["discount_type"] == 'Percentage'){
                                        $discount = ($data['Gross'] * $coupon[0]["discount"])/100;
                                    }else{
                                        $discount = $coupon[0]["discount"];
                                    }
                                }
                            }                
                            if($coupon[0]["select_rule"] == "="){
                                if($data['Gross'] == $coupon[0]["discount_on_price"]){
                                    if($coupon[0]["discount_type"] == 'Percentage'){
                                        $discount = ($data['Gross'] * $coupon[0]["discount"])/100;
                                    }else{
                                        $discount = $coupon[0]["discount"];
                                    }
                                }
                            }
                        }
                    }                    
                }
    
                if($coupon[0]["maximumDiscount"]!== null && $coupon[0]["maximumDiscount"]!== 'null' && $coupon[0]["maximumDiscount"]>0){
                    if($discount > $coupon[0]["maximumDiscount"]){
                        $discount = $coupon[0]["maximumDiscount"];
                    }
                }
                if($discount>$data['Gross']){
                    $discount = $data['Gross'];
                }

            }
            //////////
        }
        $status = true;
        if($discount == 0){
            $status = false;
            //$message = 'This coupon is not applicable for this order. Please check the coupon dtails carefully.';
        }else{
            $message = '';
        }
        
        
        return [
            'discount' => $discount,
            'status' => $status,
            'message' => $message
        ];
    }

    public function ChangeCouponStatus($data)
    {
        if(!empty($data['BUID'])){
            $this->BUID = $data['BUID'];
        }else{
            $this->CheckAuthenticated();
        }
        if($this->BUID == null){
            throw new Exception("Sorry BUID is required");
        }
        if (empty($data['id'])) {
            throw new Exception(__t("Sorry coupon id is needed"));
        }
        $statement=$this->PDO->prepare("{ CALL PHP_Ecommerce_CouponRuleStatusupdate(
                                                @id=:id,
                                                @status=:status,
                                                @buid=:buid)}");
        $statement->bindParam(':id', $data['id'], PDO::PARAM_INT);
        $statement->bindParam(':status', $data['status'], PDO::PARAM_INT);
        $statement->bindParam(':buid', $this->BUID, PDO::PARAM_INT);
        if (!$statement->execute()) {
            throw new Exception(__t("Sorry coupon status was not updated"));
        }
        return true;
    }

    public function DeleteCoupon($data)
    {
        if(!empty($data['BUID'])){
            $this->BUID = $data['BUID'];
        }else{
            $this->CheckAuthenticated();
        }
        if($this->BUID == null){
            throw new Exception("Sorry BUID is required");
        }
        if (empty($data['id'])) {
            throw new Exception(__t("Sorry invalid coupon id"));
        }
          $statement= $this->PDO->prepare("{ CALL PHP_Ecommerce_DeleteCoupon (
                                                @id=:id,
                                                @buid=:buid)}");

        $statement->bindParam(':id', $data['id'], PDO::PARAM_INT);
        $statement->bindParam(':buid', $this->BUID, PDO::PARAM_INT);
        if (!$statement->execute()) {
            throw new Exception(__t("Sorry can not get coupon"));
        }
        return true;
    }


}
