<?php

require_once MODEL_PATH . DS . 'App.php';

class CustomerAuth extends AppModel {

    public function __construct() {
        parent::__construct(false);
    }

    public function RegisterUser($postData) {
        if (empty($postData['FKBusinessUnitID'])) {
            throw new Exception("Sorry invalid App");
        }
        if (empty($postData['FirstName'])) {
            throw new Exception("Sorry First name is required");
        }
        if (empty($postData['LastName'])) {
            throw new Exception("Sorry last name is required");
        }
        if (!filter_var($postData['Email'], FILTER_VALIDATE_EMAIL)) {
            throw new Exception("Sorry a valid email is required");
        }
        if (!empty($postData['Password']) && strlen($postData['Password']) <= 7) {
            throw new Exception("Passwords must be at least 8 characters long");
        }
        if (!empty($postData['Password']) && $postData['Password'] !== $postData['ConfirmPassword']) {
            throw new Exception("Sorry password and confirm password mismatched");
        }
        if (!empty($postData['DOB'])) {
            $postData['DOB'] = date('Y-m-d H:i:s', strtotime($postData['DOB']));
        } else {
            $postData['DOB'] = NULL;
        }
        if (!empty($postData['marriageAnniversaryDate'])) {
            $postData['marriageAnniversaryDate'] = date('Y-m-d H:i:s', strtotime($postData['marriageAnniversaryDate']));
        } else {
            $postData['marriageAnniversaryDate'] = NULL;
        }
        if(empty($postData['type'])){
            $postData['type'] = 1;
        }
        $data = [
            'email' => $postData['Email'],
            'phone' => $postData['PhoneNumber'],
            'firstname' => $postData['FirstName'],
            'lastname' => $postData['LastName'],
            'password' => (empty($postData['Password']) ? null : $postData['Password']),
            'buid' => $postData['FKBusinessUnitID'],
            'dob' => $postData['DOB'],
            'CountryCode' => $postData['CountryCode'],
            'Gender' => $postData['Gender'],
            'marriageAnniversaryDate' => $postData['marriageAnniversaryDate'],
            'type' => $postData['type']
        ];

        //dd($data, true);

        if (!empty($postData['CustomerID'])) {
            $data['id'] = (int) $postData['CustomerID'];
        }
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_CustomerRegistration("
                . "@EMAIL=:email,"
                . "@PHONENumber=:phone,"
                . "@FIRSTNAME=:firstname,"
                . "@LASTNAME=:lastname,"
                . "@PASSWORD=:password,"
                . "@DOB=:dob,"
                . "@BUID=:buid,"
                . "@CustomerID=:createdcustomer,"
                . "@CountryCode=:CountryCode,"
                . "@Gender=:Gender,"
                . "@marriageAnniversaryDate=:marriageAnniversaryDate,"
                . "@regType=:regType,"
                . "@marketplaceCustomerID=:marketplaceCustomerID,"
                . "@marketplaceCustomerDataRef=:marketplaceCustomerDataRef,"
                . "@ID=:customer"
                . ")}");
        $statement->bindParam(':email', $data['email'], PDO::PARAM_STR);
        $statement->bindParam(':phone', $data['phone'], PDO::PARAM_STR);
        $statement->bindParam(':firstname', $data['firstname'], PDO::PARAM_STR);
        $statement->bindParam(':lastname', $data['lastname'], PDO::PARAM_STR);
        if (empty($data['password'])) {
            $statement->bindParam(':password', $data['password'], PDO::PARAM_INT);
        } else {
            $statement->bindParam(':password', $data['password'], PDO::PARAM_STR);
        }
        $statement->bindParam(':buid', $this->BUID = $data['buid'], PDO::PARAM_INT);
        if (empty($data['id'])) {
            $data['id'] = null;
        }
        $statement->bindParam(':createdcustomer', $data['id'], PDO::PARAM_INT);
        if (!empty($data['dob'])) {
            $statement->bindParam(':dob', $data['dob'], PDO::PARAM_STR);
        } else {
            $statement->bindParam(':dob', $data['dob'] = NULL, PDO::PARAM_INT);
        }
        if (!empty($data['CountryCode'])) {
            $statement->bindParam(':CountryCode', $data['CountryCode'], PDO::PARAM_STR);
        } else {
            $statement->bindParam(':CountryCode', $data['CountryCode'] = NULL, PDO::PARAM_INT);
        }
        $statement->bindParam(':Gender', $data['Gender'], PDO::PARAM_INT);
        if (!empty($data['marriageAnniversaryDate'])) {
            $statement->bindParam(':marriageAnniversaryDate', $data['marriageAnniversaryDate'], PDO::PARAM_STR);
        } else {
            $statement->bindParam(':marriageAnniversaryDate', $data['marriageAnniversaryDate'] = NULL, PDO::PARAM_INT);
        }
        $statement->bindParam(':regType', $data['type'], PDO::PARAM_INT);

        $statement->bindParam(':marketplaceCustomerID', $data['marketplaceCustomerID'] = NULL, PDO::PARAM_INT);
        $statement->bindParam(':marketplaceCustomerDataRef', $data['marketplaceCustomerDataRef'] = '', PDO::PARAM_STR);

        $statement->bindParam(':customer', $customerID, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 500);
        $statement->execute();
        if (empty($customerID)) {
            throw new Exception("Sorry customer not created");
        }
        return ['CustomerID' => $customerID];
    }

    public function CustomerLogin($loginstring, $password, $buid) {
        //dd($loginstring);
        //dd($password,true);
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_CustomerLogin("
                . "@loginstring=:username,"
                . "@password=:password,"
                . "@buid=:buid"
                . ")}");
        $statement->bindParam(':buid', $this->BUID = $buid, PDO::PARAM_INT);
        $statement->bindParam(':username', $loginstring, PDO::PARAM_STR);
        $statement->bindParam(':password', $password, PDO::PARAM_STR);
        $statement->execute();
        $result = $statement->fetch(PDO::FETCH_ASSOC);
        if (empty($result['CustomerID'])) {
            throw new Exception("invalid login credentials provided");
        }
        return ['CustomerID' => (int) $result['CustomerID']];
    }

    public function CustomerSendForgotEmail($emailOFCustomer, $buid, $LinkForForgotpassword) {
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_CreateResetPasswordHash("
                . "@Email=:email,"
                . "@BUID=:buid"
                . ")}");
        $statement->bindParam(':buid', $this->BUID = $buid, PDO::PARAM_INT);
        $statement->bindParam(':email', $emailOFCustomer, PDO::PARAM_STR);
        $statement->execute();
        $result = $statement->fetch(PDO::FETCH_ASSOC);
        if (empty($result['hashedKey'])) {
            throw new Exception("Sorry your account does not exisits, Please register with us !");
        }
        if (parse_url($LinkForForgotpassword, PHP_URL_QUERY)) {
            $LinkForForgotpassword = $LinkForForgotpassword . '&k=' . $result['hashedKey'];
        } else {
            $LinkForForgotpassword = $LinkForForgotpassword . '?k=' . $result['hashedKey'];
        }

        /*$EmailTemplate = '<html>
                            <head>
                            </head>

                            <body style="font-family: Arial; font-size: 12px;">
                            <div>
                                <p>
                                    You have requested a password reset, please follow the link below to reset your password.
                                </p>
                                <p>
                                    Please ignore this email if you did not request a password change.
                                </p>

                                <p>
                                    <a href="' . $LinkForForgotpassword . '">
                                        Follow this link to reset your password.
                                    </a>
                                </p>
                            </div>
                            </body>
                            </html>';*/
        $EmailTemplate = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                            <html xmlns="http://www.w3.org/1999/xhtml">
                              <head>
                                <title>Passion - Responsive Email Template</title>
                                <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                                <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
                              </head>
                              <body style="margin: 0; padding: 0; background: #dfdfdf;">
                                <table border="0" cellpadding="0" cellspacing="0" width="700" align="center" style="background-color:#fff; font-family: arial; font-size: 13px; line-height: 20px;">
                                  <tr>
                                    <td>
                                      <table width="100%" cellpadding="0" cellspacing="0" align="center" border="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border:0; text-align:center; background: #9b1616">
                                        <tr>
                                          <td>
                                            <img src="https://webkon-ecommerce.s3.ap-south-1.amazonaws.com/TEST-BU/logo.png" width="200" alt="" title="" border="0" style="border:0; padding: 10px 0;"/>
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td style="position: relative; height: auto">
                                      <img src="https://webkon-ecommerce.s3.ap-south-1.amazonaws.com/TEST-BU/content-bg.png" width="100%" alt="" title="" border="0"/>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      <table width="600" align="center" style="text-align: left;">
                                        <tr>
                                          <td>
                                            <p>
                                              <br>
                                              <p><strong>Hey there!</strong></p>
                                              <p>We have recieved a request to reset the password associated with this '.$emailOFCustomer.' account. If you have made this request, please use the below mentioned link to get started</p>
                                              <p style="font-size: 14px; line-height: 24px; color: #fff; font-family: Arial, Helvetica, sans-serif; margin: 0 0 15px; padding: 0; text-align: center;">
                                                  <a href="'.$LinkForForgotpassword.'" target="_blank" style="color:#fff;background-color: #a12328; padding: 10px 20px; text-decoration: none;border-radius: 4px;margin: 15px 0; display: inline-block;">Reset Your Password</a>
                                              </p>
                                              <p>If you did not request a password reset for your Webkon account, you can still safely ignore this email. Only a person with access to your registered email can reset your password. <br></p>
                                              <br>
                                              <p><strong>Thank you</strong></p>
                                              <p><strong>-The Team Webkon</strong></p>
                                            </p>
                                          </td>
                                        </tr>
                                        
                                      </table>
                                    </td>
                                  </tr>              
                                  <tr>
                                    <td height="20" width="100%">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td>
                                      <table width="650" align="center">
                                        <tr>
                                          <td style="background: #dfdfdf;color: #000;padding: 8px; display: block;text-align: center;font-size: 11px;border-radius: 5px;">
                                            For support requests, contact our 24x7 services on <i><b><a href="tel:8008408970" style="color:#000;"> 800-840-8970 (USA Call free)</a></b></i> number. You may also send us and email on <i><b><a href="mailto:hello@webkon.co?Subject=User Contacted Webkon"style="color:#000; ">hello@webkon.co</a></b></i> and our support executive will get back to you within 24 hours.
                                          </td>
                                        </tr>
                                        <tr>
                                          <td style="text-align: center;">
                                            <p><b>Call Us at :</b> <a style="font-size: 12px;color: #333;text-decoration: none;" href="tel:8008408970"> 800-840-8970 (USA Call free) </a> / <a style="font-size: 12px;color: #333;text-decoration: none;" href="tel:08081891494"> 08081-891-494 (UK Call free) </a> / <a style="font-size: 12px;color: #333;text-decoration: none;" href="tel:03340040627"> +91-033-40040627 (India) </a></p>
                                          </td>
                                        </tr>   
                                      </table>
                                    </td>
                                  </tr> 
                                  <tr>
                                    <td height="20" width="100%">&nbsp;</td>
                                  </tr>
                                  </table>
                                  <table width="700" align="center" border="0" cellpadding="0" cellspacing="0" bgcolor="#f5f5f5" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;font-family: arial; font-size: 13px; line-height: 20px; text-align: center">
                                    <tr>
                                      <td>
                                        <table width="600" align="center" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; text-align: center" >
                                          <tr>
                                            <td width="100%" height="20"></td>
                                          </tr>
                                          <tr>
                                            <td>
                                              <table align="center" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;"> 
                                                <tr>
                                                  <td style="color: #414d5f; font-family: Raleway, Helvetica, Arial, sans-serif; font-size: 12px; letter-spacing:.5px; font-weight: 400;">
                                                    © 2018 <a href="https://webkon.co" target="_blank" style="color: #414d5f; font-weight: 600; text-decoration:none;">webkon.co</a>. All Rights Reserved. <a href="https://webkon.co/terms-of-services/" target="_blank" style="color: #414d5f;">TERMS OF SERVICES</a> - <a href="https://webkon.co/privacy-policy/" target="_blank" style="color: #414d5f;">PRIVACY POLICY</a>
                                                  </td>
                                                </tr>
                                              </table>                 
                                            </td>
                                          </tr>
                                          <tr>
                                            <td width="100%" height="20"></td>
                                          </tr>
                                        </table>
                                      </td>
                                    </tr>
                                  </table>
                                </body>
                              </html>';

        $statementBU = $this->PDO->prepare("{CALL PHP_AR_GetBUDetails(
            @BUID=:BUID
        )}");

        $statementBU->bindParam(':BUID', $buid, PDO::PARAM_INT);
        $statementBU->execute();
        $rows = $statementBU->fetchAll(PDO::FETCH_ASSOC);
        $BUDEtails = $rows[0];

        $CURLOPT_POSTFIELDS = array(
            'To' => $emailOFCustomer,
            'Subject' => "Please verify your email, to retrieve your password",
            'MAilBody' => $EmailTemplate,
            'CompanyID' => $BUDEtails["FKCompanyID"]
        );

        return callApi(API_URL_ENDPOINT . '/webapi/Mail/SendMail', 'POST', $CURLOPT_POSTFIELDS);
    }

    private function randomPassword() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    public function CustomerGetDataFROmhash($hash, $loginlink) {
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_CreateGetUserFomHash("
                . "@HASH=:hash"
                . ")}");
        $statement->bindParam(':hash', $hash, PDO::PARAM_STR);
        $statement->execute();
        $result = $statement->fetch(PDO::FETCH_ASSOC);
        if (empty($result['CustomerID'])) {
            throw new Exception("Sorry invalid request !");
        }

        $randompass = $this->randomPassword();
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_UPDATEPASSWORD("
                . "@CUSTOMERID=:customerid,"
                . "@BUID=:buid,"
                . "@PASSWORD=:password"
                . ")}");
        $statement->bindParam(':customerid', $result['CustomerID'], PDO::PARAM_INT);
        $statement->bindParam(':buid', $result['buid'], PDO::PARAM_INT);
        $statement->bindParam(':password', $randompass, PDO::PARAM_STR);
        $statement->execute();
        $EmailTemplate = '<html>
                            <head>
                            </head>

                            <body style="font-family: Arial; font-size: 12px;">
                            <div>
                                <p>
                                    You have requested a password reset, we have update your password.
                                </p>
                                <p>
                                    Please check your new login details below
                                </p>

                                <p>
                                   <table>
                                   <tr>
                                    <th align="left">Email<th>
                                    <td align="left">' . $result['Email'] . '</td>
                                    </tr>
                                   <tr>
                                    <th align="left">Password<th>
                                    <td align="left">' . $randompass . '</td>
                                    </tr>
                                   </table>
                                </p>
                                <p>
                                    <a href="' . $loginlink . '">
                                        Follow this link to login.
                                    </a>
                                </p>
                            </div>
                            </body>
                            </html>';
        
        $statementBU = $this->PDO->prepare("{CALL PHP_AR_GetBUDetails(
            @BUID=:BUID
        )}");

        $statementBU->bindParam(':BUID', $result['buid'], PDO::PARAM_INT);
        $statementBU->execute();
        $rows = $statementBU->fetchAll(PDO::FETCH_ASSOC);
        $BUDEtails = $rows[0];



        $CURLOPT_POSTFIELDS = array(
            'To' => $result['Email'],
            'Subject' => "Your password updated succesfully",
            'MAilBody' => $EmailTemplate,
            'CompanyID' => $BUDEtails["FKCompanyID"]
        );

        return callApi(API_URL_ENDPOINT . '/webapi/Mail/SendMail', 'POST', $CURLOPT_POSTFIELDS);
    }

    public function getCustomerDetails($customerID, $buid) {
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_GetCustomer(@BUID=:id, @CUSTOMERID=:customer_id)}");
        $statement->bindParam(':customer_id', $customerID, PDO::PARAM_INT);
        $statement->bindParam(':id', $this->BUID = $buid, PDO::PARAM_INT);
        $statement->execute();
        return $statement->fetch(PDO::FETCH_ASSOC);
    }

    public function changePassword($data) {

        if (empty($data['buid'])) {
            throw new Exception(__t("Sorry invalid app"));
        }
        if (empty($data['memberid'])) {
            throw new Exception(__t("Please login before you change password"));
        }
        if (empty($data['oldpassword'])) {
            throw new Exception(__t("Please put your old password before proceeding!"));
        }
        if (empty($data['newpassword'])) {
            throw new Exception(__t("Please put your new password before proceeding!"));
        }
        if ($data['newpassword'] !== $data['confirmpassword']) {
            throw new Exception(__t("Your new password and confirm password does not match"));
        }



        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_GetCustomerWithPasword(@BUID=:id, @CUSTOMERID=:customer_id)}");
        $statement->bindParam(':customer_id', $data['memberid'], PDO::PARAM_INT);
        $statement->bindParam(':id', $this->BUID = $data['buid'], PDO::PARAM_INT);
        $statement->execute();
        $customerdetails = $statement->fetch(PDO::FETCH_ASSOC);

        $newpasswordstatement = $this->PDO->prepare("{CALL PHP_Ecommerce_HashMD5(@PASSOWRD=:passowrd)}");
        $newpasswordstatement->bindParam(':passowrd', $data['oldpassword'], PDO::PARAM_STR);
        $newpasswordstatement->execute();
        $oldpasswordofuser = $newpasswordstatement->fetch(PDO::FETCH_ASSOC);
        if (($customerdetails['password']) != $oldpasswordofuser['password']) {
            throw new Exception(__t("Sorrry your old password does not match, if you have forgot your password please use forgot password !!"));
        }

        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_UPDATEPASSWORD("
                . "@CUSTOMERID=:customerid,"
                . "@BUID=:buid,"
                . "@PASSWORD=:password"
                . ")}");
        $statement->bindParam(':customerid', $data['memberid'], PDO::PARAM_INT);
        $statement->bindParam(':buid', $data['buid'], PDO::PARAM_INT);
        $statement->bindParam(':password', $data['newpassword'], PDO::PARAM_STR);
        $statement->execute();

        $EmailTemplate = '<html>
                            <head>
                            </head>

                            <body style="font-family: Arial; font-size: 12px;">
                            <div>
                                <p>
                                    Your password has been changed successfully .
                                </p>
                                <p>
                                    Please check your new login details below
                                </p>

                                <p>
                                   <table>
                                   <tr>
                                    <th align="left">Email<th>
                                    <td align="left">' . $customerdetails['Email'] . '</td>
                                    </tr>
                                   <tr>
                                    <th align="left">Password<th>
                                    <td align="left">' . $data['newpassword'] . '</td>
                                    </tr>
                                   </table>
                                </p>
                            </div>
                            </body>
                            </html>';
        $statementBU = $this->PDO->prepare("{CALL PHP_AR_GetBUDetails(
            @BUID=:BUID
        )}");

        $statementBU->bindParam(':BUID', $data['buid'], PDO::PARAM_INT);
        $statementBU->execute();
        $rows = $statementBU->fetchAll(PDO::FETCH_ASSOC);
        $BUDEtails = $rows[0];
        $CURLOPT_POSTFIELDS = array(
            'To' => $customerdetails['Email'],
            'Subject' => "Your password updated succesfully",
            'MAilBody' => $EmailTemplate,
            'CompanyID' => $BUDEtails["FKCompanyID"]
        );

        callApi(API_URL_ENDPOINT . '/webapi/Mail/SendMail', 'POST', $CURLOPT_POSTFIELDS);

        return true;
    }

    public function SocialLogin($data) {
        $email = '';
        $phonenumber = '';
        if (filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
            $email = $data['email'];
        }
        if (empty($data['id'])) {
            throw new Exception(__t("Sorry you cant login with " . $data['type'] . " login, please try again later"));
        }
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_SocialLogin("
                . "@BUID=:buid,"
                . "@SocialID=:socialid,"
                . "@FirstName=:firstname,"
                . "@LastName=:lastname,"
                . "@Email=:email,"
                . "@PhoneNumber=:phonenumber,"
                . "@SocialType=:socialtype,"
                . "@ID=:customer"
                . ")}");
        $statement->bindParam(':buid', $this->BUID = $data['buid'], PDO::PARAM_INT);
        $statement->bindParam(':socialid', $data['id'], PDO::PARAM_STR);
        $statement->bindParam(':firstname', $data['first_name'], PDO::PARAM_STR);
        $statement->bindParam(':lastname', $data['last_name'], PDO::PARAM_STR);
        $statement->bindParam(':email', $data['email'], PDO::PARAM_STR);
        $statement->bindParam(':phonenumber', $phonenumber, PDO::PARAM_STR);
        $statement->bindParam(':socialtype', $data['type'], PDO::PARAM_STR);
        $statement->bindParam(':customer', $customerID, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 500);
        $statement->execute();
        return ['CustomerID' => $customerID];
    }

    /*public function updateCustomer($postData) {
        if (empty($postData['BUID'])) {
            throw new Exception("Sorry invalid App");
        }
        if (empty($postData['FirstName'])) {
            throw new Exception("Sorry First name is required");
        }
        if (empty($postData['LastName'])) {
            throw new Exception("Sorry last name is required");
        }
        
        if (!empty($postData['DOB'])) {
            $postData['DOB'] = date('Y-m-d H:i:s', strtotime($postData['DOB']));
        } else {
            $postData['DOB'] = NULL;
        }
        if (!empty($postData['MAD'])) {
            $postData['MAD'] = date('Y-m-d H:i:s', strtotime($postData['MAD']));
        } else {
            $postData['MAD'] = NULL;
        }
        $data = [
            'firstname' => $postData['FirstName'],
            'lastname' => $postData['LastName'],
            'buid' => $postData['BUID'],
            'dob' => $postData['DOB'],
            'Gender' => $postData['Gender']
        ];

        if (!empty($postData['CustomerID'])) {
            $data['id'] = (int) $postData['CustomerID'];
        }
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_CustomerRegistration("
                . "@EMAIL=:email,"
                . "@PHONENumber=:phone,"
                . "@FIRSTNAME=:firstname,"
                . "@LASTNAME=:lastname,"
                . "@PASSWORD=:password,"
                . "@DOB=:dob,"
                . "@BUID=:buid,"
                . "@CustomerID=:createdcustomer,"
                . "@CountryCode=:CountryCode,"
                . "@Gender=:Gender,"
                . "@marriageAnniversaryDate=:marriageAnniversaryDate,"
                . "@ID=:customer"
                . ")}");
        $statement->bindParam(':email', $data['email'], PDO::PARAM_STR);
        $statement->bindParam(':phone', $data['phone'], PDO::PARAM_STR);
        $statement->bindParam(':firstname', $data['firstname'], PDO::PARAM_STR);
        $statement->bindParam(':lastname', $data['lastname'], PDO::PARAM_STR);
        if (empty($data['password'])) {
            $statement->bindParam(':password', $data['password'], PDO::PARAM_INT);
        } else {
            $statement->bindParam(':password', $data['password'], PDO::PARAM_STR);
        }
        $statement->bindParam(':buid', $this->BUID = $data['buid'], PDO::PARAM_INT);
        if (empty($data['id'])) {
            $data['id'] = null;
        }
        $statement->bindParam(':createdcustomer', $data['id'], PDO::PARAM_INT);
        if (!empty($data['dob'])) {
            $statement->bindParam(':dob', $data['dob'], PDO::PARAM_STR);
        } else {
            $statement->bindParam(':dob', $data['dob'] = NULL, PDO::PARAM_INT);
        }
        if (!empty($data['CountryCode'])) {
            $statement->bindParam(':CountryCode', $data['CountryCode'], PDO::PARAM_STR);
        } else {
            $statement->bindParam(':CountryCode', $data['CountryCode'] = NULL, PDO::PARAM_INT);
        }
        $statement->bindParam(':Gender', $data['Gender'], PDO::PARAM_INT);
        if (!empty($data['marriageAnniversaryDate'])) {
            $statement->bindParam(':marriageAnniversaryDate', $data['marriageAnniversaryDate'], PDO::PARAM_STR);
        } else {
            $statement->bindParam(':dob', $data['marriageAnniversaryDate'] = NULL, PDO::PARAM_INT);
        }
        
        $statement->bindParam(':customer', $customerID, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 500);
        $statement->execute();
        if (empty($customerID)) {
            throw new Exception("Sorry customer not created");
        }
        return ['CustomerID' => $customerID];
    }*/

}
