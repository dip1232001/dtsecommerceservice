<?php
use function GuzzleHttp\json_encode;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CustomersModel
 *
 * @author dipankar
 */
require_once MODEL_PATH . DS . 'App.php';

class CustomersModel extends AppModel
{

    //put your code here

    public function __construct($callAuth = false)
    {
        parent::__construct();
        //$this->CheckAuthenticated();
    }

    public function GetCustomers($start = 0, $limit = 20, $searchstring = null, $BUID)
    {
        if (!empty($BUID)) {
            $this->BUID = $BUID;
        } else {
            $this->CheckAuthenticated();
        }
        if ($this->BUID == null) {
            throw new Exception("Sorry BUID is required");
        }
        if (empty($start)) {
            $start = 0;
        }
        if (empty($limit) || $limit > 20) {
            $limit = 20;
        }
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_GetCustomers(@BUID=:buid,@Start=:start,@Limit=:limit,@SearchString=:searchstring)}");
        $statement->bindParam(':buid', $this->BUID, PDO::PARAM_INT);
        $statement->bindParam(':start', $start, PDO::PARAM_INT);
        $statement->bindParam(':limit', $limit, PDO::PARAM_INT);
        if (empty($searchstring)) {
            $statement->bindParam(':searchstring', $searchstring, PDO::PARAM_INT);
        } else {
            $searchstring = trim(strip_tags($searchstring));
            $statement->bindParam(':searchstring', $searchstring, PDO::PARAM_STR);
        }
        $statement->execute();
        $x = 0;
        $data = [];
        do {
            $rows = $statement->fetchAll(PDO::FETCH_ASSOC);

            //if ($rows!=false) {
            switch ($x) {
                case 0:
                    $data['Pagination'] = [
                        'total' => (int)$rows[0]['totalCustomers'],
                        'start' => $start,
                        'limit' => $limit
                    ];
                    break;
                case 1:
                    $data['Customers'] = $rows;
                    break;
            }
            $x++;
            // }
        } while ($statement->nextRowset());
        $statement->closeCursor();
        return $data;
    }

    public function AddCustomerAddress($data)
    {
        if (empty($data['is_default'])) {
            $data['is_default'] = 0;
        }
        if (empty($data['alrternetPH'])) {
            $data['alrternetPH'] = null;
        }
        if (empty($data['landmark'])) {
            $data['landmark'] = null;
        }
        if (empty($data['phonecode'])) {
            $data['phonecode'] = null;
        }
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_AddCustomerAddress(
            @customer_id=:customer_id,
            @address=:address,
            @name=:name,
            @phoneno=:phoneno,
            @phonecode=:pincode,
            @address_type=:address_type,
            @city=:city,
            @state=:state,
            @landmark=:landmark,
            @alrternetPH=:alrternetPH,
            @is_default=:is_default,
            @ID=:ID,
            @Country=:country,
            @AddressID=:AddressID)}");
        if (empty($data['customer_id'])) {
            $data['customer_id'] = null;
        }
        $statement->bindParam(':customer_id', $data['customer_id'], PDO::PARAM_INT);
        $statement->bindParam(':address', $data['address'], PDO::PARAM_STR);
        $statement->bindParam(':name', $data['name'], PDO::PARAM_STR);
        $statement->bindParam(':phoneno', $data['phoneno'], PDO::PARAM_STR);
        $statement->bindParam(':pincode', $data['pincode'], PDO::PARAM_STR);
        $statement->bindParam(':city', $data['city'], PDO::PARAM_STR);
        $statement->bindParam(':state', $data['state'], PDO::PARAM_STR);
        $statement->bindParam(':country', $data['country'], PDO::PARAM_STR);
        if (empty($data['landmark'])) {
            $statement->bindParam(':landmark', $data['landmark'] = null, PDO::PARAM_INT);
        } else {
            $statement->bindParam(':landmark', $data['landmark'], PDO::PARAM_STR);
        }
        if ($data['alrternetPH']) {
            $statement->bindParam(':alrternetPH', $data['alrternetPH'] = null, PDO::PARAM_INT);
        } else {
            $statement->bindParam(':alrternetPH', $data['alrternetPH'], PDO::PARAM_STR);
        }
        $statement->bindParam(':is_default', $data['is_default'], PDO::PARAM_INT);
        if (empty($data['address_type']) || ($data['address_type'] != "shipping" && $data['address_type'] != "billing")) {
            $data['address_type'] = "shipping";
        }
        $statement->bindParam(':address_type', $data['address_type']);
        if (empty($data['id'])) {
            $data['id'] = null;
        }
        $statement->bindParam(':ID', $data['id'], PDO::PARAM_INT);
        $statement->bindParam(':AddressID', $AddressID, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 40);
        if (!$statement->execute()) {
            throw new Exception(__t("Sorry customer address not added"));
        }
        $statement->closeCursor();
        return ['AddressID' => $AddressID];
    }

    public function getAddresses($customerid, $addresstype = 'shipping')
    {
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_GetSavedAddresses(@customer_id=:customer_id,@address_type=:addresstype)}");
        $statement->bindParam(':customer_id', $customerid, PDO::PARAM_INT);
        $statement->bindParam(':addresstype', $addresstype, PDO::PARAM_STR);
        $statement->execute();
        $data = $statement->fetchAll(PDO::FETCH_ASSOC);
        $statement->closeCursor();
        return $data;
    }

    public function setDefaultAddress($customerID, $id, $addresstype = 'shipping')
    {
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_Set_CustomerDefaultAddress(@ID=:id, @customer_id=:customer_id,@address_type=:address_type)}");
        $statement->bindParam(':customer_id', $customerID, PDO::PARAM_INT);
        $statement->bindParam(':address_type', $addresstype, PDO::PARAM_STR);
        $statement->bindParam(':id', $id, PDO::PARAM_INT);
        $return = $statement->execute();
        $statement->closeCursor();
        return $return;
    }

    public function DeleteCustomerAddress($customerID, $id)
    {
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_DeleteCustomerAdress(@ID=:id, @customer_id=:customer_id)}");
        $statement->bindParam(':customer_id', $customerID, PDO::PARAM_INT);
        $statement->bindParam(':id', $id, PDO::PARAM_INT);
        return $statement->execute();
    }

    public function getCustomerDetails($customerID, $BUID)
    {
        if (!empty($BUID)) {
            $this->BUID = $BUID;
        } else {
            $this->CheckAuthenticated();
        }
        if ($this->BUID == null) {
            throw new Exception("Sorry BUID is required");
        }
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_GetCustomer(@BUID=:id, @CUSTOMERID=:customer_id)}");
        $statement->bindParam(':customer_id', $customerID, PDO::PARAM_INT);
        $statement->bindParam(':id', $this->BUID, PDO::PARAM_INT);
        $statement->execute();
        $customer = $statement->fetch(PDO::FETCH_ASSOC);
        if ($customer['Gender'] != NULL) {
            $customer['Gender'] = $customer['Gender'];
        } else {
            $customer['Gender'] = 0;
        }

        if ($customer['DOB'] != NULL) {
            $customer['DOBformated'] = date('Y-m-d', strtotime($customer['DOB']));
        } else {
            $customer['DOBformated'] = $customer['DOB'];
        }
        //$customer['DOBformated'] = date('Y-m-d', strtotime($customer['DOB']));
        if ($customer['marriage_anniversary_date']) {
            $customer['MADformated'] = date('Y-m-d', strtotime($customer['marriage_anniversary_date']));
        } else {
            $customer['MADformated'] = $customer['marriage_anniversary_date'];
        }
        return $customer;
    }

    public function RegisterUser($data)
    {
        //dd($data, true);
        if (!empty($data['BUID'])) {
            $this->BUID = $data['BUID'];
        } else {
            $this->CheckAuthenticated();
        }
        if ($this->BUID == null) {
            throw new Exception("Sorry BUID is required");
        }
        if (!empty($data['DOB'])) {
            $data['dob'] = $data['DOB'];
        }
        if (empty($data['type'])) {
            $data['type'] = 1;
        }
        if (empty($data['Gender']) || $data['Gender'] == null) {
            $data['Gender'] = 0;
        }
        if (empty($data['id'])) {
            $data['id'] = null;
        }

        $retArr = [];
        //Marketplace Registration
        if (!empty($data['EnableMarketPlaceSubscription']) && $data['EnableMarketPlaceSubscription'] == 1 && $data['type'] == 1) {
            $regArr = [
                "email" => $this->BUID . $data['email'],
                "password" => $data["password"],
                "first_name" => $data['firstname'],
                "last_name" => $data['lastname'],
                "nickname" => $data['firstname'],
                "subscription_plan" => $data['subscription_plan'],
                "CouponCode" => $data['CouponCode'],
                "AdditionInfo" => [
                    "phone_no" => $data['phone'],
                    "patientName" => $data['patientName'],
                    "clientID" => $data['clientID'],
                    "AffiliateTracking" => $data['AffiliateTracking']
                ]
            ];

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://testwp.myteamconnector.com/" . $this->BUID . "/wp-json/webkon/user/registration",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => json_encode($regArr),
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json",
                    "Token: d2Via29u",
                    "cache-control: no-cache"
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                throw new Exception("Sorry customer not created");
            } else {
                $response = json_decode($response, true);
                if ($response["error"]) {
                    throw new Exception("Sorry customer not created");
                } else {
                    $data['marketplaceCustomerID'] = $response["data"]["userID"];
                    $data['marketplaceCustomerDataRef'] = json_encode($regArr);
                    $retArr["marketplaceCustomerID"] = $response["data"]["userID"];
                    $retArr["Token"] = $response["data"]["Token"];
                    $retArr["url"] = $response["data"]["url"];
                }
            }
        } else {
            $data['marketplaceCustomerID'] = null;
            $data['marketplaceCustomerDataRef'] = '';
            $retArr["marketplaceCustomerID"] = null;
            $retArr["Token"] = '';
            $retArr["url"] = '';
        }


        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_CustomerRegistration("
            . "@EMAIL=:email,"
            . "@PHONENumber=:phone,"
            . "@FIRSTNAME=:firstname,"
            . "@LASTNAME=:lastname,"
            . "@PASSWORD=:password,"
            . "@DOB=:dob,"
            . "@BUID=:buid,"
            . "@CustomerID=:createdcustomer,"
            . "@CountryCode=:CountryCode,"
            . "@Gender=:Gender,"
            . "@marriageAnniversaryDate=:marriageAnniversaryDate,"
            . "@regType=:regType,"
            . "@marketplaceCustomerID=:marketplaceCustomerID,"
            . "@marketplaceCustomerDataRef=:marketplaceCustomerDataRef,"
            . "@ID=:customer"
            . ")}");
        $statement->bindParam(':email', $data['email'], PDO::PARAM_STR);
        $statement->bindParam(':phone', $data['phone'], PDO::PARAM_STR);
        $statement->bindParam(':firstname', $data['firstname'], PDO::PARAM_STR);
        $statement->bindParam(':lastname', $data['lastname'], PDO::PARAM_STR);
        $statement->bindParam(':password', $data['password'], PDO::PARAM_STR);
        $statement->bindParam(':buid', $this->BUID, PDO::PARAM_INT);

        $statement->bindParam(':createdcustomer', $data['id'], PDO::PARAM_INT);
        if (!empty($data['dob'])) {
            $statement->bindParam(':dob', $data['dob'], PDO::PARAM_STR);
        } else {
            $statement->bindParam(':dob', $data['dob'] = NULL, PDO::PARAM_INT);
        }
        if (!empty($data['CountryCode'])) {
            $statement->bindParam(':CountryCode', $data['CountryCode'], PDO::PARAM_STR);
        } else {
            $statement->bindParam(':CountryCode', $data['CountryCode'] = '', PDO::PARAM_STR);
        }
        $statement->bindParam(':Gender', $data['Gender'], PDO::PARAM_INT);
        if (!empty($data['marriageAnniversaryDate'])) {
            $statement->bindParam(':marriageAnniversaryDate', $data['marriageAnniversaryDate'], PDO::PARAM_STR);
        } else {
            $statement->bindParam(':marriageAnniversaryDate', $data['marriageAnniversaryDate'] = NULL, PDO::PARAM_INT);
        }
        $statement->bindParam(':regType', $data['type'], PDO::PARAM_INT);

        $statement->bindParam(':marketplaceCustomerID', $data['marketplaceCustomerID'], PDO::PARAM_INT);
        $statement->bindParam(':marketplaceCustomerDataRef', $data['marketplaceCustomerDataRef'], PDO::PARAM_STR);

        $statement->bindParam(':customer', $customerID, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 500);
        //dd($data, true);
        $statement->execute();
        if (empty($customerID)) {
            throw new Exception("Sorry customer not created");
        } else {
            $retArr["CustomerID"] = $customerID;
        }
        return $retArr;
    }

    public function CustomerLogin($loginstring, $password, $BUID)
    {
        //dd($loginstring);
        //dd($password,true);
        if (!empty($BUID)) {
            $this->BUID = $BUID;
        } else {
            $this->CheckAuthenticated();
        }
        if ($this->BUID == null) {
            throw new Exception("Sorry BUID is required");
        }
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_CustomerLogin("
            . "@loginstring=:username,"
            . "@password=:password,"
            . "@buid=:buid"
            . ")}");
        $statement->bindParam(':buid', $this->BUID, PDO::PARAM_INT);
        $statement->bindParam(':username', $loginstring, PDO::PARAM_STR);
        $statement->bindParam(':password', $password, PDO::PARAM_STR);
        $statement->execute();
        $result = $statement->fetch(PDO::FETCH_ASSOC);
        if (empty($result['CustomerID'])) {
            throw new Exception("invalid login credentials provided");
        }
        //dd($result, true);
        return ["CustomerID" => (int)$result["CustomerID"], "marketplaceCustomerID" => $result["marketplaceCustomerID"], "marketplaceCustomerPlan" => ltrim(rtrim($result["marketplaceCustomerPlan"]))];
    }

    public function checkAddress($customerid, $address_type, $address)
    {

        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_CheckCustomerAddress(
            @customer_id=:customer_id,
            @address=:address,
            @address_type=:address_type)}");

        $statement->bindParam(':customer_id', $customerid, PDO::PARAM_INT);
        $statement->bindParam(':address', $address, PDO::PARAM_STR);
        $statement->bindParam(':address_type', $address_type, PDO::PARAM_STR);
        $statement->execute();
        return $statement->fetch(PDO::FETCH_ASSOC);
    }

    public function updateCustomerMarketplacePlan($data)
    {
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_updateCustomerMarketplacePlan("
            . "@marketplaceCustomerID=:marketplaceCustomerID,"
            . "@marketplaceCustomerPlan=:marketplaceCustomerPlan"
            . ")}");

        $statement->bindParam(':marketplaceCustomerID', $data['marketplaceCustomerID'], PDO::PARAM_INT);
        $statement->bindParam(':marketplaceCustomerPlan', $data['marketplaceCustomerPlan'], PDO::PARAM_STR);

        $statement->execute();

        return true;
    }
}
