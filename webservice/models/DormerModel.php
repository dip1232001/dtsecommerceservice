<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DormerModel
 *
 * @author Abhijit Manna
 */

require_once MODEL_PATH . DS . 'App.php';
//require_once MODEL_PATH . DS . 'EmailModel.php';

class DormerModel extends AppModel {

    //put your code here

    public function __construct($callAuth = false) {
        parent::__construct($callAuth);
    }

    public function RegisterUser($postData) {
        if (empty($postData['buid'])) {
            throw new Exception("Sorry invalid App");
        }
        if (empty($postData['FirstName'])) {
            throw new Exception("Sorry First name is required");
        }
        if (empty($postData['LastName'])) {
            throw new Exception("Sorry last name is required");
        }
        if (!filter_var($postData['Email'], FILTER_VALIDATE_EMAIL)) {
            throw new Exception("Sorry a valid email is required");
        }
        if (!empty($postData['Password']) && strlen($postData['Password']) <= 7) {
            throw new Exception("Passwords must be at least 8 characters long");
        }
        if (!empty($postData['Password']) && $postData['Password'] !== $postData['ConfirmPassword']) {
            throw new Exception("Sorry password and confirm password mismatched");
        }
        if (!empty($postData['DOB'])) {
            $postData['DOB'] = date('Y-m-d H:i:s', strtotime($postData['DOB']));
        } else {
            $postData['DOB'] = NULL;
        }
        if (!empty($postData['marriageAnniversaryDate'])) {
            $postData['marriageAnniversaryDate'] = date('Y-m-d H:i:s', strtotime($postData['marriageAnniversaryDate']));
        } else {
            $postData['marriageAnniversaryDate'] = NULL;
        }
        if(empty($postData['type'])){
            $postData['type'] = 1;
        }
        $data = [
            'email' => $postData['Email'],
            'phone' => $postData['PhoneNumber'],
            'firstname' => $postData['FirstName'],
            'lastname' => $postData['LastName'],
            'password' => (empty($postData['Password']) ? null : $postData['Password']),
            'buid' => $postData['buid'],
            'dob' => $postData['DOB'],
            'CountryCode' => $postData['CountryCode'],
            'Gender' => $postData['Gender'],
            'marriageAnniversaryDate' => $postData['marriageAnniversaryDate'],
            'type' => $postData['type']
        ];

        if (!empty($postData['CustomerID'])) {
            $data['id'] = (int) $postData['CustomerID'];
        }
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_CustomerRegistration("
                . "@EMAIL=:email,"
                . "@PHONENumber=:phone,"
                . "@FIRSTNAME=:firstname,"
                . "@LASTNAME=:lastname,"
                . "@PASSWORD=:password,"
                . "@DOB=:dob,"
                . "@BUID=:buid,"
                . "@CustomerID=:createdcustomer,"
                . "@CountryCode=:CountryCode,"
                . "@Gender=:Gender,"
                . "@marriageAnniversaryDate=:marriageAnniversaryDate,"
                . "@regType=:regType,"
                . "@marketplaceCustomerID=:marketplaceCustomerID,"
                . "@marketplaceCustomerDataRef=:marketplaceCustomerDataRef,"
                . "@ID=:customer"
                . ")}");
        $statement->bindParam(':email', $data['email'], PDO::PARAM_STR);
        $statement->bindParam(':phone', $data['phone'], PDO::PARAM_STR);
        $statement->bindParam(':firstname', $data['firstname'], PDO::PARAM_STR);
        $statement->bindParam(':lastname', $data['lastname'], PDO::PARAM_STR);
        if (empty($data['password'])) {
            $statement->bindParam(':password', $data['password'], PDO::PARAM_INT);
        } else {
            $statement->bindParam(':password', $data['password'], PDO::PARAM_STR);
        }
        $statement->bindParam(':buid', $this->BUID = $data['buid'], PDO::PARAM_INT);
        if (empty($data['id'])) {
            $data['id'] = null;
        }
        $statement->bindParam(':createdcustomer', $data['id'], PDO::PARAM_INT);
        if (!empty($data['dob'])) {
            $statement->bindParam(':dob', $data['dob'], PDO::PARAM_STR);
        } else {
            $statement->bindParam(':dob', $data['dob'] = NULL, PDO::PARAM_INT);
        }
        if (!empty($data['CountryCode'])) {
            $statement->bindParam(':CountryCode', $data['CountryCode'], PDO::PARAM_STR);
        } else {
            $statement->bindParam(':CountryCode', $data['CountryCode'] = NULL, PDO::PARAM_INT);
        }
        $statement->bindParam(':Gender', $data['Gender'], PDO::PARAM_INT);
        if (!empty($data['marriageAnniversaryDate'])) {
            $statement->bindParam(':marriageAnniversaryDate', $data['marriageAnniversaryDate'], PDO::PARAM_STR);
        } else {
            $statement->bindParam(':marriageAnniversaryDate', $data['marriageAnniversaryDate'] = NULL, PDO::PARAM_INT);
        }
        $statement->bindParam(':regType', $data['type'], PDO::PARAM_INT);

        $statement->bindParam(':marketplaceCustomerID', $data['marketplaceCustomerID'] = NULL, PDO::PARAM_INT);
        $statement->bindParam(':marketplaceCustomerDataRef', $data['marketplaceCustomerDataRef'] = '', PDO::PARAM_STR);
        
        $statement->bindParam(':customer', $customerID, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 500);
        $statement->execute();
        if (empty($customerID)) {
            throw new Exception("Sorry customer not created");
        }
        return ['CustomerID' => $customerID];
    }

    public function CustomerLogin($loginstring, $password, $buid) {
        if (empty($buid)) {
            throw new Exception("Sorry invalid App");
        }
        if (empty($loginstring)) {
            throw new Exception("Sorry login id is required");
        }
        if (empty($password)) {
            throw new Exception("Sorry password is required");
        }
        //dd($loginstring);
        //dd($password,true);
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_CustomerLogin("
                . "@loginstring=:username,"
                . "@password=:password,"
                . "@buid=:buid"
                . ")}");
        $statement->bindParam(':buid', $this->BUID = $buid, PDO::PARAM_INT);
        $statement->bindParam(':username', $loginstring, PDO::PARAM_STR);
        $statement->bindParam(':password', $password, PDO::PARAM_STR);
        $statement->execute();
        $result = $statement->fetch(PDO::FETCH_ASSOC);
        if (empty($result['CustomerID'])) {
            throw new Exception("invalid login credentials provided");
        }else{
            $statement1 = $this->PDO->prepare("{CALL PHP_Ecommerce_GetCustomer(@BUID=:id, @CUSTOMERID=:customer_id)}");
            $statement1->bindParam(':customer_id', $result['CustomerID'], PDO::PARAM_INT);
            $statement1->bindParam(':id', $this->BUID = $buid, PDO::PARAM_INT);
            $statement1->execute();
            $row = $statement1->fetch(PDO::FETCH_ASSOC);
        }
        return ['data' => $row ];
    }

    public function getCustomerDetails($customerID, $buid) {
        if (empty($buid)) {
            throw new Exception("Sorry invalid App");
        }
        if (empty($customerID)) {
            throw new Exception("Sorry customer id is required");
        }
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_GetCustomer(@BUID=:id, @CUSTOMERID=:customer_id)}");
        $statement->bindParam(':customer_id', $customerID, PDO::PARAM_INT);
        $statement->bindParam(':id', $this->BUID = $buid, PDO::PARAM_INT);
        $statement->execute();
        return $statement->fetch(PDO::FETCH_ASSOC);
    }

    public function changePassword($data) {

        if (empty($data['buid'])) {
            throw new Exception(__t("Sorry invalid app"));
        }
        if (empty($data['memberid'])) {
            throw new Exception(__t("Please login before you change password"));
        }
        if (empty($data['oldpassword'])) {
            throw new Exception(__t("Please put your old password before proceeding!"));
        }
        if (empty($data['newpassword'])) {
            throw new Exception(__t("Please put your new password before proceeding!"));
        }
        if ($data['newpassword'] !== $data['confirmpassword']) {
            throw new Exception(__t("Your new password and confirm password does not match"));
        }



        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_GetCustomerWithPasword(@BUID=:id, @CUSTOMERID=:customer_id)}");
        $statement->bindParam(':customer_id', $data['memberid'], PDO::PARAM_INT);
        $statement->bindParam(':id', $this->BUID = $data['buid'], PDO::PARAM_INT);
        $statement->execute();
        $customerdetails = $statement->fetch(PDO::FETCH_ASSOC);

        $newpasswordstatement = $this->PDO->prepare("{CALL PHP_Ecommerce_HashMD5(@PASSOWRD=:passowrd)}");
        $newpasswordstatement->bindParam(':passowrd', $data['oldpassword'], PDO::PARAM_STR);
        $newpasswordstatement->execute();
        $oldpasswordofuser = $newpasswordstatement->fetch(PDO::FETCH_ASSOC);
        if (($customerdetails['password']) != $oldpasswordofuser['password']) {
            throw new Exception(__t("Sorrry your old password does not match, if you have forgot your password please use forgot password !!"));
        }

        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_UPDATEPASSWORD("
                . "@CUSTOMERID=:customerid,"
                . "@BUID=:buid,"
                . "@PASSWORD=:password"
                . ")}");
        $statement->bindParam(':customerid', $data['memberid'], PDO::PARAM_INT);
        $statement->bindParam(':buid', $data['buid'], PDO::PARAM_INT);
        $statement->bindParam(':password', $data['newpassword'], PDO::PARAM_STR);
        $statement->execute();

        return true;
    }

    public function SocialLogin($data) {
        $email = '';
        $phonenumber = '';
        if (filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
            $email = $data['email'];
        }
        if (empty($data['id'])) {
            throw new Exception(__t("Sorry you cant login with " . $data['type'] . " login, please try again later"));
        }
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_SocialLogin("
                . "@BUID=:buid,"
                . "@SocialID=:socialid,"
                . "@FirstName=:firstname,"
                . "@LastName=:lastname,"
                . "@Email=:email,"
                . "@PhoneNumber=:phonenumber,"
                . "@SocialType=:socialtype,"
                . "@ID=:customer"
                . ")}");
        $statement->bindParam(':buid', $this->BUID = $data['buid'], PDO::PARAM_INT);
        $statement->bindParam(':socialid', $data['id'], PDO::PARAM_STR);
        $statement->bindParam(':firstname', $data['first_name'], PDO::PARAM_STR);
        $statement->bindParam(':lastname', $data['last_name'], PDO::PARAM_STR);
        $statement->bindParam(':email', $data['email'], PDO::PARAM_STR);
        $statement->bindParam(':phonenumber', $phonenumber, PDO::PARAM_STR);
        $statement->bindParam(':socialtype', $data['type'], PDO::PARAM_STR);
        $statement->bindParam(':customer', $customerID, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 500);
        $statement->execute();
        return ['CustomerID' => $customerID];
    }

    public function SendForgotPasswordEmail($data) {
        $buid = $data['BUID'];
        $email = $data['customer'];
        
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_VerifyCustomer(
            @BUID=:buid,
            @email=:email
        )}");

        $statement->bindParam(':buid', $data['BUID'], PDO::PARAM_INT);
        $statement->bindParam(':email', $data['customer'], PDO::PARAM_STR);
        
        if (!$statement->execute()) {
            throw new Exception(__t("Sorry folder not added"));
        }

        $rows = $statement->fetchAll(PDO::FETCH_ASSOC); 
        $rowsN = $rows[0];  
        $data1['customerid'] = $rowsN["CustomerID"];        
        $statement->closeCursor();

        if($rowsN["CustomerID"] != null){
            $rowsN["loginurl"] = $data['url'] . "&key=".md5($rowsN["CustomerID"]);

            $statementN = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_insertResetPasswordKey(
                @BUID=:buid,
                @CustomerID=:CustomerID,
                @key=:key
            )}");
    
            $statementN->bindParam(':buid', $data['BUID'], PDO::PARAM_INT);
            $statementN->bindParam(':CustomerID', $rowsN['CustomerID'], PDO::PARAM_STR);
            $statementN->bindParam(':key', md5($rowsN["CustomerID"]), PDO::PARAM_STR);
            $statementN->execute();

            $this->NewSendForgotPasswordEmail($rowsN);
        }else{
            throw new Exception(__t("Customer doesnot exists."));
        }

        return $data1;
    }

    public function NewSendForgotPasswordEmail($data){
        $emailhtml='<!DOCTYPE html>
        <html>
        <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <title>My Contact Card</title>
        </head>
        <body style="padding: 0; margin: 0;">
            <table width="650" cellpadding="0" cellspacing="0" align="center">
                <thead>
                    <tr>
                        <th style="padding-top: 10px; padding-bottom: 10px; padding-left: 10px; padding-right: 10px; text-align: left; border-bottom: 1px solid #f0632b;"><img src="https://ecom.myteamconnector.com/img/demailligo.png" style="display: block;"></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td align="left" valign="top">
                            <p style="padding-top: 30px; padding-bottom: 25px; margin: 0; font-family: Arial; font-size: 16px; line-height: 22px; color: #696969;">Hi '.$data['FirstName'].' '.$data['LastName'].',</p>
                            <p style="padding-bottom: 25px; margin: 0; font-family: Arial; font-size: 16px; line-height: 22px; color: #696969;">There was a request to change your password.</p>
                            <p style="padding-bottom: 25px; margin: 0; font-family: Arial; font-size: 16px; line-height: 22px; color: #696969;">&nbsp;</p>
                            <p style="padding-bottom: 25px; margin: 0; font-family: Arial; font-size: 16px; line-height: 22px; color: #696969;">If you did not make this request, just ignore this email. Otherwise please click or copy the link bellow to change the password. </p>
                        </td>
                    </tr>
                    
                    <tr>
                        <td align="center" valign="top" style="padding-top:40px; height: 60px;">
                            <a href="'.$data['loginurl'].'" style="background-color: #f0632b; color: #ffffff; text-decoration: none; font-family: Arial; font-size: 14px; line-height: 24px; padding-top:15px; padding-bottom: 15px; padding-left: 60px; padding-right: 60px;">Click here</a>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="middle" style="background-color: #eaeaea; padding-top: 10px; padding-bottom: 10px;"><p style="font-family: Arial; font-size: 12px; line-height: 16px; margin: 0; padding: 0; color: #3892c7;">&copy; COPYRIGHT 2018 DORMER. ALL RIGHTS RESERVED.</p></td>
                    </tr>
                </tbody>
            </table>
        </body>
        </html>';

        $emailData = array();                        
        $emailData['email_subject'] = "Reset Password..";            
        $emailData['email_body'] = $emailhtml;
        $emailData['to_email'] = $data['Email'];
        
        $CURLOPT_POSTFIELDS = array(
            'To' => $emailData['to_email'],
            'Subject' => $emailData['email_subject'],
            'MAilBody' => $emailData['email_body']
        );

        $response = callApi(API_URL_ENDPOINT . '/webapi/Mail/SendMail', 'POST', $CURLOPT_POSTFIELDS);
        
    }

    public function CheckForgotPasswordKey($data) {
        $key = $data['key'];
        
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_verifyResetPasswordKey(
            @key=:key
        )}");

        $statement->bindParam(':key', $data['key'], PDO::PARAM_STR);
        
        if (!$statement->execute()) {
            throw new Exception(__t("Please try again later"));
        }

        $rows = $statement->fetchAll(PDO::FETCH_ASSOC); 
        $rowsN = $rows[0];  
        $data1 = array();        
        $statement->closeCursor();

        if($rowsN["customerID"] != null){
            if($rowsN["MinuteDiff"]<=1440){
                $data1['customerID'] = $rowsN["customerID"];   
            }else{
                throw new Exception(__t("Key expired."));
            }
            
        }else{
            throw new Exception(__t("Invalid key."));
        }

        return $data1;
    }

    public function UpdateForgotPassword($data) {

        if($data['BUID']==""){
            throw new Exception(__t("Enter BUID"));
        } 
        if($data['CustomerID']==""){
            throw new Exception(__t("Enter CustomerID"));
        }        
        if($data['password']==""){
            throw new Exception(__t("Enter password"));
        } 
        if($data['cpassword']==""){
            throw new Exception(__t("Enter confirm password"));
        } 
        if($data['password'] != $data['cpassword']){
            throw new Exception(__t("Password and Enter confirm password does not matched!"));
        }

        
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_ContactCards_ResetPassword(
            @BUID=:BUID,
            @CustomerID=:CustomerID,
            @password=:password
        )}");

        $statement->bindParam(':BUID', $data['BUID'], PDO::PARAM_INT);
        $statement->bindParam(':CustomerID', $data['CustomerID'], PDO::PARAM_INT);
        $statement->bindParam(':password', $data['password'], PDO::PARAM_STR);
        
        if (!$statement->execute()) {
            throw new Exception(__t("Please try again later."));
        }
     
        $statement->closeCursor();

        

        return $data;
    }

    public function sendGiftEmail($data){
        $emailhtml='<!DOCTYPE html>
        <html>
        <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <title>My Contact Card</title>
        </head>
        <body style="padding: 0; margin: 0;">
            <table width="650" cellpadding="0" cellspacing="0" align="center">
                <thead>
                    <tr>
                        <th style="padding-top: 10px; padding-bottom: 10px; padding-left: 10px; padding-right: 10px; text-align: left; border-bottom: 1px solid #f0632b;"><img src="https://ecom.myteamconnector.com/img/demailligo.png" style="display: block;"></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td align="left" valign="top">
                            <p style="padding-top: 30px; padding-bottom: 25px; margin: 0; font-family: Arial; font-size: 16px; line-height: 22px; color: #696969;">Hello '.$data['FirstName'].' '.$data['LastName'].',</p>
                            <!--<p style="padding-bottom: 25px; margin: 0; font-family: Arial; font-size: 16px; line-height: 22px; color: #696969;">You earned '.$data['Points'].' points from Dormer Pramet Trivia</p>-->
                            <p style="padding-bottom: 25px; margin: 0; font-family: Arial; font-size: 16px; line-height: 22px; color: #696969;">'.$data['description'].'</p>
                            <p style="padding-bottom: 25px; margin: 0; font-family: Arial; font-size: 16px; line-height: 22px; color: #696969;"> </p>
                        </td>
                    </tr>
                    
                    <tr>
                        <td align="center" valign="top" style="padding-top:40px;">
                            <img src="'.$data['giftURL'].'" style="max-width:550px">
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top">
                            <p style="padding-bottom: 25px; margin: 0; font-family: Arial; font-size: 16px; line-height: 22px; color: #696969;">Thanks,</p>
                            <p style="padding-bottom: 25px; margin: 0; font-family: Arial; font-size: 16px; line-height: 22px; color: #696969;">Your Dormer Pramet Team.</p>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="middle" style="background-color: #eaeaea; padding-top: 10px; padding-bottom: 10px;"><p style="font-family: Arial; font-size: 12px; line-height: 16px; margin: 0; padding: 0; color: #3892c7;">&copy; COPYRIGHT 2018 DORMER. ALL RIGHTS RESERVED.</p></td>
                    </tr>
                </tbody>
            </table>
        </body>
        </html>';

        $emailData = array();                        
        $emailData['email_subject'] = "Congratulations!! Collect your prize at Dormer Pramet’s booth (W-432432) on IMTS";            
        $emailData['email_body'] = $emailhtml;
        $emailData['to_email'] = $data['Email'];
        
        $CURLOPT_POSTFIELDS = array(
            'To' => $emailData['to_email'],
            'Subject' => $emailData['email_subject'],
            'MAilBody' => $emailData['email_body']
        );

        $response = callApi(API_URL_ENDPOINT . '/webapi/Mail/SendMail', 'POST', $CURLOPT_POSTFIELDS);

        return $response;
        
    }

    public function getCustomerPoint($data) {
        if($data['buid']== "" || $data['buid']==0){
            throw new Exception(__t("Enter BUID"));
        }
        if($data['customerid']== "" || $data['customerid']==0){
            throw new Exception(__t("Enter customer id"));
        }
        $statement = $this->PDO->prepare("{CALL PHP_Trivia_getCustomerPoint(@BUID=:BUID, @customerid=:customerid)}");
        $statement->bindParam(':BUID', $data["buid"], PDO::PARAM_INT);
        $statement->bindParam(':customerid', $data["customerid"], PDO::PARAM_INT);
        $statement->execute();
        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    public function setCustomerPoint($data) {
        if($data['buid']== "" || $data['buid']==0){
            throw new Exception(__t("Enter BUID"));
        }
        if($data['customerid']== "" || $data['customerid']==0){
            throw new Exception(__t("Enter customer id"));
        }
        if($data['points']== "" || $data['points']==0){
            throw new Exception(__t("Enter points"));
        }
        
        $statement = $this->PDO->prepare("{CALL PHP_Trivia_setCustomerPoint(@BUID=:BUID, @customerid=:customerid,@points=:points,@minGiftPoint=:minGiftPoint)}");
        $statement->bindParam(':BUID', $data["buid"], PDO::PARAM_INT);
        $statement->bindParam(':customerid', $data["customerid"], PDO::PARAM_INT);
        $statement->bindParam(':points', $data["points"], PDO::PARAM_STR);
        $statement->bindParam(':minGiftPoint', $data["minGiftPoint"], PDO::PARAM_STR);
        $statement->execute();
        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

}
