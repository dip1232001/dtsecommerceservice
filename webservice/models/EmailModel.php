<?php

require_once MODEL_PATH . DS . 'App.php';

class EmailModel extends AppModel {

    public function __construct($Auth = false) {
        parent::__construct();

        /*if ($Auth != true && !empty($buid)) {
            $this->BUID = $buid;
        } else {
            $this->CheckAuthenticated();
        }*/
    }

    public function getEmailList() {
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_GetEmailTemplateList()}");
        $statement->execute();
        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getEmailDetails($Email_Type,$BUID) {
        if(!empty($BUID)){
            $this->BUID = $BUID;
        }else{
            $this->CheckAuthenticated();
        }
        if($this->BUID == null){
            throw new Exception("Sorry BUID is required");
        }
        if (empty($Email_Type)) {
            throw new Exception(__t("Sorry email type not specified"));
        }
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_GetEmailTemplate(
                                            @BUID=:buid,
                                            @TeamplateType=:TemplateType)}");
        $statement->bindParam(':buid', $this->BUID, PDO::PARAM_INT);
        $statement->bindParam(':TemplateType', $Email_Type, PDO::PARAM_STR);
        $statement->execute();
        return $statement->fetch(PDO::FETCH_ASSOC);
    }

    public function prepareEmailData($order_id, $Email_Type,$BUID = '') {
        if(!empty($BUID)){
            $this->BUID = $BUID;
        }else{
            $this->CheckAuthenticated();
        }
        //dd($this->BUID, true);
        if (empty($Email_Type)) {
            throw new Exception(__t("Sorry email type not specified"));
        }

        $pusherArr = array('PushTitle' => '', 'PushMessage' => '');

        $statement = $this->PDO->prepare("{ CALL PHP_Ecommerce_GetGeneralSettings(@BUID=:buid)}");
        $statement->bindParam(':buid', $this->BUID, PDO::PARAM_INT);
        $statement->execute();
        $generalSettings = $statement->fetch(PDO::FETCH_ASSOC);
        //dd($generalSettings);

        $start = 0;
        $limit = 20;
        $data = array('customer_id' => null, 'startdate' => null, 'enddate' => null, 'identifier' => null);

        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_GetOrders("
                . "@customer_id=:customer_id,"
                . "@order_id=:order_id,"
                . "@buid=:buid,"
                . "@start=:start,"
                . "@Limit=:limit,"
                . "@startdate=:startdate,"
                . "@enddate=:enddate,"
                . "@identifier=:identifier"
                . ")}");
        $statement->bindParam(':customer_id', $data['customer_id'], PDO::PARAM_INT);
        $statement->bindParam(':order_id', $order_id, PDO::PARAM_INT);
        $statement->bindParam(':buid', $this->BUID, PDO::PARAM_INT);
        $statement->bindParam(':start', $start, PDO::PARAM_INT);
        $statement->bindParam(':limit', $limit, PDO::PARAM_INT);
        $statement->bindParam(':startdate', $data['startdate'], PDO::PARAM_INT);
        $statement->bindParam(':enddate', $data['enddate'], PDO::PARAM_INT);
        $statement->bindParam(':identifier', $data['identifier'], PDO::PARAM_STR);

        $statement->execute();
        $data = [];
        do {
            $rows = $statement->fetchAll(PDO::FETCH_ASSOC);

            if ($rows) {
                if (isset($rows[0]['Total'])) {
                    $data['Paginations'] = [
                        'Total' => $rows[0] ['Total'],
                        'Start' => $start,
                        'limit' => $limit
                    ];
                } else {
                    $data['orders'] = array_map(array($this, "json_decode_summary"), $rows);
                }
            }
        } while ($statement->nextRowset());

        $orders = $data['orders'];
        //dd($orders, true);

        $orderdetails = '';
        foreach ($orders[0]['ordersummary']['cartitems'] as $v) {
            $orderdetails .='<tr>
                                <td style="border-bottom:1px solid #ddd;font-size:14px;font-weight:bold;padding:5px 10px;"><img src="' . $v['imagePath'] . '" style="width:100px"></td>
                                <td style="border-bottom:1px solid #ddd; font-size:14px;font-weight:bold;padding:5px 10px;">' . $v['Productname'] . '</td>
                                <td style="border-bottom:1px solid #ddd;font-size:14px;font-weight:bold;padding:5px 10px;text-align:right;">' . $v['cartQuantity'] . '</td>
                                <td style="border-bottom:1px solid #ddd;font-size:14px;font-weight:bold;padding:5px 10px;text-align:right;">' . $orders[0]['OrderCurrecnyCode'] . ' ' . number_format($v['baseprice'], 2) . '</td>
                                <td style="border-bottom:1px solid #ddd;font-size:14px;font-weight:bold;padding:5px 10px;text-align:right;width:150px;">' . $orders[0]['OrderCurrecnyCode'] . ' ' . number_format($v['price'], 2) . '</td>
                            </tr>';
        }

        $tableno = '';
        if($orders[0]['ordersummary']['tableNo']!=''){
            $tableno = '<b>Table No:</b> '.$orders[0]['ordersummary']['tableNo'];
        }

        $billingAddress = '';        
        if($orders[0]['Billingaddressid']!=''){
            $billingAddress = '<b>Billing Details</b> <br /> '.$orders[0]['Billingname'] . ", " . $orders[0]['Billingaddress'] . ", " . $orders[0]['BillingCity'] . ", " . $orders[0]['BillingState'] . ", " . $orders[0]['ShippingCountry'] . ", " . $orders[0]['BillingPincode'];
        }

        $shippinhAddress = '';        
        if($orders[0]['ShippingaddressID']!=''){
            $shippinhAddress = '<b>Ship To </b><br /> '.$orders[0]['Shippingname'] . ", " . $orders[0]['Shippingaddress'] . ", " . $orders[0]['ShippingCity'] . ", " . $orders[0]['ShippingState'] . ", " . $orders[0]['ShippingCountry'] . ", " . $orders[0]['ShippingPincode'];
        }

        $email_subject_keywords = ['##CUSTOMERNAME##' => $orders[0]['Billingname'],
            '##ORDERIDENTIFIER##' => $orders[0]['orderidentifier'],
            '&nbsp;' => ''];

        $email_body_keywords = ['##ORDERIDENTIFIER##' => $orders[0]['orderidentifier'],
            '##CUSTOMERNAME##' => $orders[0]['Billingname'],
            '##BILLIBGNAME##' => $orders[0]['Billingname'],
            '##BILLINGADDRESS##' => $orders[0]['Billingaddress'] . ", " . $orders[0]['BillingCity'],
            '##BILLINGADDRESS1##' => $orders[0]['BillingState'] . ", " . $orders[0]['ShippingCountry'] . ", " . $orders[0]['BillingPincode'],
            '##PAYMENTMETHOD##' => $orders[0]['PaymentTypeName'],
            '##SHIPPINGNAME##' => $orders[0]['Shippingname'],
            '##SHIPPINGADDRESS##' => $orders[0]['Shippingaddress'] . ", " . $orders[0]['ShippingCity'],
            '##SHIPPINGADDRESS1##' => $orders[0]['ShippingState'] . ", " . $orders[0]['ShippingCountry'] . ", " . $orders[0]['ShippingPincode'],
            '##SHIPPIGMETHOD##' => '',
            '<!--ORDERDETAILS-->' => $orderdetails,
            '<!--TABLENO-->' => $tableno,
            '<!--SHIPTO-->' => $shippinhAddress,
            '<!--BILLINGDETAILS-->' => $billingAddress,
            '##SUBTOTAL##' => number_format($orders[0]['ordersummary']['priceInfo']['subtotal'], 2),
            '##SHIPPINGCHARGE##' => number_format($orders[0]['ordersummary']['priceInfo']['shipping'], 2),
            '##TAX##' => number_format($orders[0]['ordersummary']['priceInfo']['Taxes'], 2),
            '##OTHERTAX##' => '0.00',
            '##TOTAL##' => number_format($orders[0]['ordersummary']['priceInfo']['Gross'], 2),
            '##SHIPPEDBY##' => $orders[0]['shipperaddress'],
            '##TRACKINGID##' => $orders[0]['trackingid'],
            '##STORENAME##' => $generalSettings['store_name'],
            '##STOREEMAIL##' => $generalSettings['store_email'],
            '##STOREPHONE##' => $generalSettings['phoneNo'],
            '##STOREADDRESS##' => $generalSettings['address'],
            '##LOGO##' => $generalSettings['default_image'],
            '##CUSTOMEREMAIL##' => $orders[0]['Email'],
            '##ORDERDATE##' => $orders[0]['FormatedDateTime'],
            '##ORDERCURRENCY##' => $orders[0]['OrderCurrecnyCode'],
            '##DISCOUNT##' => number_format($orders[0]['ordersummary']['priceInfo']['Discounts'], 2),
            '##SHIPPINGDETAILS##' => $orders[0]['Shippingname'] . ", " . $orders[0]['Shippingaddress'] . ", " . $orders[0]['ShippingCity'] . ", " . $orders[0]['ShippingState'] . ", " . $orders[0]['ShippingCountry'] . ", " . $orders[0]['ShippingPincode'],
            '##BILLINGDETAILS##' => $orders[0]['Billingname'] . ", " . $orders[0]['Billingaddress'] . ", " . $orders[0]['BillingCity'] . ", " . $orders[0]['BillingState'] . ", " . $orders[0]['ShippingCountry'] . ", " . $orders[0]['BillingPincode'],
            '##ORDERSTATUS##' => $orders[0]['OrderStatus'],
            '##CUSTOMERPHONE##' => $orders[0]['BillingPhone'],
            '##COPYRIGHTYEAR##' => date('Y')
        ];

        if ($Email_Type == 'shipment') {

            $pusherArr['PushTitle'] = 'Shipment Notice';
            $pusherArr['PushMessage'] = 'Dear ' . $orders[0]['Billingname'] . ', Your Shipment for Order ' . $orders[0]['orderidentifier'] . ' has been despatched';

            $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_GetEmailTemplate(
                                            @BUID=:buid,
                                            @TeamplateType=:TemplateType)}");
            $statement->bindParam(':buid', $this->BUID, PDO::PARAM_INT);
            $statement->bindParam(':TemplateType', $Email_Type, PDO::PARAM_STR);
            $statement->execute();
            $emailTemplate = $statement->fetch(PDO::FETCH_ASSOC);

            $retData = array();

            $retData['email_subject'] = str_replace(array_keys($email_subject_keywords), $email_subject_keywords, $emailTemplate['email_subject']);

            $retData['email_body'] = str_replace(array_keys($email_body_keywords), $email_body_keywords, $emailTemplate['email_body']);

            $retData['to_email'] = $orders[0]['Email'];

            $statementBU = $this->PDO->prepare("{CALL PHP_AR_GetBUDetails(
                @BUID=:BUID
            )}");
    
            $statementBU->bindParam(':BUID', $this->BUID, PDO::PARAM_INT);
            $statementBU->execute();
            $rows = $statementBU->fetchAll(PDO::FETCH_ASSOC);
            $BUDEtails = $rows[0];

            $CURLOPT_POSTFIELDS = array(
                'To' => $retData['to_email'],
                'Subject' => $retData['email_subject'],
                'MAilBody' => $retData['email_body'],
                'ReplyTo' => $generalSettings['store_email'],
                'ReplyToDisplay' => $generalSettings['store_name'],
                'MailFromDisplay' => $generalSettings['store_name'],
                'CompanyID' => $BUDEtails["FKCompanyID"]
            );

            $response = callApi(API_URL_ENDPOINT . '/webapi/Mail/SendMail', 'POST', $CURLOPT_POSTFIELDS);
        }
        if ($Email_Type == 'newOrder') {
            $et = "new_order_placed";

            $pusherArr['PushTitle'] = 'New Order Placed';
            $pusherArr['PushMessage'] = 'Dear ' . $orders[0]['Billingname'] . ', Your Order ' . $orders[0]['orderidentifier'] . ' Placed Successfully';

            $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_GetEmailTemplate(
                                            @BUID=:buid,
                                            @TeamplateType=:TemplateType)}");
            $statement->bindParam(':buid', $this->BUID, PDO::PARAM_INT);
            $statement->bindParam(':TemplateType', $et, PDO::PARAM_STR);
            $statement->execute();
            $emailTemplate = $statement->fetch(PDO::FETCH_ASSOC);

            $retData = array();

            $retData['email_subject'] = str_replace(array_keys($email_subject_keywords), $email_subject_keywords, $emailTemplate['email_subject']);

            $retData['email_body'] = str_replace(array_keys($email_body_keywords), $email_body_keywords, $emailTemplate['email_body']);

            $retData['to_email'] = $orders[0]['Email'];
            //$retData['to_email'] = 'achyut.sinha@dreamztech.com';

            $statementBU = $this->PDO->prepare("{CALL PHP_AR_GetBUDetails(
                @BUID=:BUID
            )}");
    
            $statementBU->bindParam(':BUID', $this->BUID, PDO::PARAM_INT);
            $statementBU->execute();
            $rows = $statementBU->fetchAll(PDO::FETCH_ASSOC);
            $BUDEtails = $rows[0];
			//$BUDEtails["FKCompanyID"] = 758;

            $CURLOPT_POSTFIELDS = array(
                'To' => $retData['to_email'],
                'Subject' => $retData['email_subject'],
                'MAilBody' => $retData['email_body'],
                'ReplyTo' => $generalSettings['store_email'],
                'ReplyToDisplay' => $generalSettings['store_name'],
                'MailFromDisplay' => $generalSettings['store_name'],
                'CompanyID' => $BUDEtails["FKCompanyID"]
            );

            //dd($CURLOPT_POSTFIELDS, true);

            $response = callApi(API_URL_ENDPOINT . '/webapi/Mail/SendMail', 'POST', $CURLOPT_POSTFIELDS);

            $et = "new_order_app_owner";
            $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_GetEmailTemplate(
                                            @BUID=:buid,
                                            @TeamplateType=:TemplateType)}");
            $statement->bindParam(':buid', $this->BUID, PDO::PARAM_INT);
            $statement->bindParam(':TemplateType', $et, PDO::PARAM_STR);
            $statement->execute();
            $emailTemplate = $statement->fetch(PDO::FETCH_ASSOC);

            $retData = array();

            $retData['email_subject'] = str_replace(array_keys($email_subject_keywords), $email_subject_keywords, $emailTemplate['email_subject']);

            $retData['email_body'] = str_replace(array_keys($email_body_keywords), $email_body_keywords, $emailTemplate['email_body']);

            $retData['to_email'] = $orders[0]['Email'];

            $statementBU = $this->PDO->prepare("{CALL PHP_AR_GetBUDetails(
                @BUID=:BUID
            )}");
    
            /*$statementBU->bindParam(':BUID', $this->BUID, PDO::PARAM_INT);
            $statementBU->execute();
            $rows = $statementBU->fetchAll(PDO::FETCH_ASSOC);
            $BUDEtails = $rows[0];*/

            $CURLOPT_POSTFIELDS = array(
                'To' => $generalSettings['store_email'],
                'Subject' => $retData['email_subject'],
                'MAilBody' => $retData['email_body'],
                'ReplyTo' => $generalSettings['store_email'],
                'ReplyToDisplay' => $generalSettings['store_name'],
                'MailFromDisplay' => $generalSettings['store_name'],
                'CompanyID' => $BUDEtails["FKCompanyID"]
            );

            $response = callApi(API_URL_ENDPOINT . '/webapi/Mail/SendMail', 'POST', $CURLOPT_POSTFIELDS);
        }

        if ($Email_Type == 'statusChange') {

            $et = "statusChange";

            $pusherArr['PushTitle'] = 'Order status change';
            $pusherArr['PushMessage'] = 'Dear ' . $orders[0]['Billingname'] . ', Your order ' . $orders[0]['orderidentifier'] . ' status changed successfully to ' . $orders[0]['OrderStatus'];

            $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_GetEmailTemplate(
                                            @BUID=:buid,
                                            @TeamplateType=:TemplateType)}");
            $statement->bindParam(':buid', $this->BUID, PDO::PARAM_INT);
            $statement->bindParam(':TemplateType', $et, PDO::PARAM_STR);
            $statement->execute();
            $emailTemplate = $statement->fetch(PDO::FETCH_ASSOC);


            $retData = array();

            $retData['email_subject'] = str_replace(array_keys($email_subject_keywords), $email_subject_keywords, $emailTemplate['email_subject']);

            $retData['email_body'] = str_replace(array_keys($email_body_keywords), $email_body_keywords, $emailTemplate['email_body']);

            $retData['to_email'] = $orders[0]['Email'];
            //$retData['to_email'] = 'achyut.sinha@dreamztech.com';

            $statementBU = $this->PDO->prepare("{CALL PHP_AR_GetBUDetails(
                @BUID=:BUID
            )}");
    
            $statementBU->bindParam(':BUID', $this->BUID, PDO::PARAM_INT);
            $statementBU->execute();
            $rows = $statementBU->fetchAll(PDO::FETCH_ASSOC);
            $BUDEtails = $rows[0];

            $CURLOPT_POSTFIELDS = array(
                'To' => $retData['to_email'],
                'Subject' => $retData['email_subject'],
                'MAilBody' => $retData['email_body'],
                'ReplyTo' => $generalSettings['store_email'],
                'ReplyToDisplay' => $generalSettings['store_name'],
                'MailFromDisplay' => $generalSettings['store_name'],
                'CompanyID' => $BUDEtails["FKCompanyID"]
            );
            //dd($CURLOPT_POSTFIELDS);
            //echo API_URL_ENDPOINT . '/webapi/Mail/SendMail'; exit;
            $response = callApi(API_URL_ENDPOINT . '/webapi/Mail/SendMail', 'POST', $CURLOPT_POSTFIELDS);
            //dd($response, true);
        }

        /* $buid=325;
          $cid=163; */
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_Get_ActionsList_Data(@buid=:buid,@userid=:userid)}");
        $statement->bindParam(':buid', $this->BUID, PDO::PARAM_INT);
        $statement->bindParam(':userid', $orders[0]['customer_id'], PDO::PARAM_INT);
        /* $statement->bindParam(':buid', $buid, PDO::PARAM_INT);
          $statement->bindParam(':userid', $cid, PDO::PARAM_INT); */
        $statement->execute();
        $uniqueids = $statement->fetch(PDO::FETCH_ASSOC);

        //dd($uniqueids, true);
        //$uniqueids = implode(",", $uniqueids);

        $search = array('##CUSTOMERNAME##', '##ORDERIDENTIFIER##', '&nbsp;');
        $replace = array($orders[0]['Billingname'], $orders[0]['orderidentifier'], '');
        $pusherArr['PushTitle'] = str_replace($search, $replace, $pusherArr['PushTitle']);
        $pusherArr['PushMessage'] = str_replace($search, $replace, $pusherArr['PushMessage']);
        try {
            $pusher = new Pusher\Pusher(PUSHERKEY, PUSHERSECRET, PUSHERAPPID, array('encrypted' => true));
        } catch (Exception $e) {
            //echo $e->getMessage();
        }
        foreach ($uniqueids as $val) {
            $result = [
                'error' => false,
                'msg' => '',
                'data' => $pusher->trigger($val, 'pushMessage', [
                    'title' => strip_tags($pusherArr['PushTitle']),
                    'description' => strip_tags($pusherArr['PushMessage'])
                ])
            ];
        }

        $duid = array($orders[0]['ordersummary']['bu_name']);

        $offlinepush = array(
            'title' => $pusherArr['PushTitle'],
            'body' => $pusherArr['PushMessage'],
            'BUID' => $duid,
            'memberid' => $orders[0]['customer_id'],
            'domain' => $orders[0]['ordersummary']['domain']
        );
        try {
            $offlinepushresponse = callApi(OFFLINE_PUSHER_API_ENDPOINT . '/webservices/index.php', 'POST', $offlinepush, ['action' => 'offlinepush'], false, false, false);
        } catch (Exception $e) {
            //echo $e->getMessage();
        }

        return $response;
    }

    public function json_decode_summary(&$array) {
        $array['ordersummary'] = json_decode($array['ordersummary'], true);
        $array['FormatedDateTime'] = date('l jS \of F Y h:i:s A', strtotime($array['datetime']));
        $array['datetime'] = date('Y-m-d H:i:s', strtotime($array['datetime']));
        $array['orderidentifier'] = "&nbsp;" . $array['orderidentifier'];
        if (!empty($array['CustomerUpdateDate'])) {
            $array['CustomerUpdateDate'] = date('Y-m-d H:i:s', strtotime($array['CustomerUpdateDate']));
        }
        if (!empty($array['CustomerCreationDate'])) {
            $array['CustomerCreationDate'] = date('Y-m-d H:i:s', strtotime($array['CustomerCreationDate']));
        }
        return $array;
    }

    public function editEmailTemplate($data) {
        //dd($data,true);
        if(!empty($data['BUID'])){
            $this->BUID = $data['BUID'];
        }else{
            $this->CheckAuthenticated();
        }
        if($this->BUID == null){
            throw new Exception("Sorry BUID is required");
        }
        if (empty($data['TeamplateType'])) {
            throw new Exception("Sorry Template Type is required");
        }
        if (empty($data['emailsubject'])) {
            throw new Exception("Sorry Email Subject is required");
        }
        if (empty($data['emailbody'])) {
            throw new Exception("Sorry Email Body is required");
        }

        $addStatement = $this->PDO->prepare("{CALL PHP_Ecommerce_Edit_Email_Template (@buid = :buid, @TeamplateType = :TeamplateType, @emailsubject = :emailsubject, @emailbody = :emailbody, @NEWEMAILTEMPLATEID=:NEWEMAILTEMPLATEI)}");

        $addStatement->bindParam(':TeamplateType', $data['TeamplateType'], PDO::PARAM_STR);
        $addStatement->bindParam(':emailsubject', $data['emailsubject'], PDO::PARAM_STR);
        $addStatement->bindParam(':emailbody', $data['emailbody'], PDO::PARAM_STR);
        $addStatement->bindParam(':buid', $this->BUID, PDO::PARAM_INT);

        $addStatement->bindParam(':NEWEMAILTEMPLATEI', $NEWEMAILTEMPLATEI, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000);

        $addStatement->execute();

        return [
            'msg' => __t("Template updated succesfully")
        ];
    }

    public function newContactCardCustomerEmail($data){ 
      
        $emailData = array();                        
        $emailData['email_subject'] = "Joining announcement.";            
        $emailData['email_body'] = "Email:".$data["email"].", Phone: ".$data["phone"].", Password: ".$data["password"];            
        $emailData['to_email'] = $data['email'];

        $CURLOPT_POSTFIELDS = array(
            'To' => $emailData['to_email'],
            'Subject' => $emailData['email_subject'],
            'MAilBody' => $emailData['email_body']
        );

        $response = callApi(API_URL_ENDPOINT . '/webapi/Mail/SendMail', 'POST', $CURLOPT_POSTFIELDS);


    }




}
