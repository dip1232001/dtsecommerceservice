<?php

use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of GeneralSettings
 *
 * @author dipan
 */
require_once MODEL_PATH . DS . 'App.php';

class GeneralSettingsModel extends AppModel
{

    //put your code here

    public function __construct($callAuth = false)
    {
        parent::__construct();
        //$this->CheckAuthenticated();
    }

    public function setGeneralSettings($data, $files)
    {
        if(!empty($data['BUID'])){
            $this->BUID = $data['BUID'];
        }else{
            $this->CheckAuthenticated();
        }
        if($this->BUID == null){
            throw new Exception("Sorry BUID is required");
        }
        if (empty($data['name'])) {
            throw new Exception("Sorry Store name can not be empty");
        }
        if (empty($data['email'])) {
            throw new Exception("Sorry Store email can not be empty");
        }
        if (empty($data['currency'])) {
            throw new Exception("Sorry Store currency can not be empty");
        }

        if (!empty($files)) {
            $data['image'] = $this->uploadImage($files);
        } else {
            if(!empty($data['Imges_url'])){
                $data['image'] = $data['Imges_url'];
            }else{
                $data['image'] = 'https://webkon-ecommerce.s3.ap-south-1.amazonaws.com/BU500/1500897348.jpg'; 
            }
            //$data['image'] = 'https://webkon-ecommerce.s3.ap-south-1.amazonaws.com/BU500/1500897348.jpg';
        }

        $statement = $this->PDO->prepare("{ CALL PHP_Ecommerce_SetGeneralSettings (@BUID=:buid,@STORE_NAME=:name,@STORE_EMAIL=:email,@STORE_CURRENCY=:currency,@DEFAULT_IMAGE=:image,@address=:address,@vatNo=:vatNo,@isVatNoViewInInvoice=:isVatNoViewInInvoice,@phoneNo=:phoneNo)}");
        $statement->bindParam(':buid', $this->BUID, PDO::PARAM_INT);
        $statement->bindParam(':name', $data['name'], PDO::PARAM_STR);
        $statement->bindParam(':email', $data['email'], PDO::PARAM_STR);
        $statement->bindParam(':currency', $data['currency'], PDO::PARAM_STR);
        $statement->bindParam(':image', $data['image'], PDO::PARAM_STR);
        $statement->bindParam(':address', $data['address'], PDO::PARAM_STR);
        $statement->bindParam(':vatNo', $data['vatNo'], PDO::PARAM_STR);
        $statement->bindParam(':isVatNoViewInInvoice', $data['isVatNoViewInInvoice'], PDO::PARAM_INT);
        $statement->bindParam(':phoneNo', $data['phoneNo'], PDO::PARAM_INT);
        return $statement->execute();
    }

    private function uploadImage($FILES)
    {

        $max_size = file_upload_max_size();
        $client = new S3Client([
            'version' => 'latest',
            'region' => AWS_REGION,
            'credentials' => [
                'key' => AWS_ACCESS_KEY,
                'secret' => AWS_SECRET_KEY
            ]
        ]);
        $valid_formats = ["jpg", "png", "gif", "jpeg"];
        $FileXml = '';

        if ($FILES['Images']['error'] == 0) {
            $name = $FILES['Images']['name'];
            $size = $FILES['Images']['size'];
            $tmp = $FILES['Images']['tmp_name'];
            $type = $FILES['Images']['type'];

            if ($size > $max_size) {
                throw new Exception(__t("Sorry image " . $name . " size is bigger than accepted size"));
            }
            $ext = strtolower(getExtension($name));
            if (!in_array($ext, $valid_formats)) {
                throw new Exception(__t("Sorry image " . $name . " is not valid ,we accept " . implode(",", $valid_formats)));
            }
            $image_name_actual = time() . "." . $ext;
            try {
                $Result = $client->putObject(array(
                    'Bucket' => AWS_BUCKET_NAME,
                    'Key' => 'BU' . $this->BUID . '/' . $image_name_actual,
                    'SourceFile' => $tmp,
                    'StorageClass' => 'REDUCED_REDUNDANCY',
                    'ACL' => 'public-read',
                    'ContentType' => $type,
                    'Expires' => date('Y-m-d H:i:s', strtotime("2 years"))
                ));
            } catch (S3Exception $exc) {
                throw new Exception("Aws S3 Error : " . $exc->getAwsErrorMessage());
            }
            return $Result['ObjectURL'];
        }


        return false;
    }

    public function getCurrencyList()
    {
        $statement = $this->PDO->prepare("{ CALL PHP_Ecommerce_Currencylist()}");
        $statement->execute();
        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getGeneralSettings($BUID)
    {
        if(!empty($BUID)){
            $this->BUID = $BUID;
        }else{
            $this->CheckAuthenticated();
        }
        if($this->BUID == null){
            throw new Exception("Sorry BUID is required");
        }
        $statement = $this->PDO->prepare("{ CALL PHP_Ecommerce_GetGeneralSettings(@BUID=:buid)}");
        $statement->bindParam(':buid', $this->BUID, PDO::PARAM_INT);
        $statement->execute();
        return $statement->fetch(PDO::FETCH_ASSOC);
    }

    public function GetAllPaymentMethods()
    {
        $statement = $this->PDO->prepare("{ CALL PHP_Ecommerce_GetAllPaymentTypes()}");
        $statement->execute();
        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getPaymentMethods($data = [])
    {
        if(!empty($data['BUID'])){
            $this->BUID = $data['BUID'];
        }else{
            $this->CheckAuthenticated();
        }
        if($this->BUID == null){
            throw new Exception("Sorry BUID is required");
        }
        if (empty($data['id'])) {
            $data['id']=null;
        }
         $statement = $this->PDO->prepare("{ CALL PHP_Ecommerce_getAddedPaymentDetails(@BUID=:buid ,@ID=:id)}");
         $statement->bindParam(':buid', $this->BUID, PDO::PARAM_INT);
            $statement->bindParam(':id', $data['id'], PDO::PARAM_INT);
            $statement->execute();
            $paymentTypes=$statement->fetchAll(PDO::FETCH_ASSOC);
        return array_map(array($this,'__addPaymentSandBoxUrls'), $paymentTypes);
    }

    public function __addPaymentSandBoxUrls($array)
    {
        if ($array['paymentmethodid']==2) {
            $array['PaymentUrl']='https://www.sandbox.paypal.com/cgi-bin/webscr';
            if (PAYMENTMODE=='LIVE') {
                $array['PaymentUrl']='https://www.paypal.com/cgi-bin/webscr';
            }
            if(!empty($array['is_live'])){
                if($array['is_live'] == 1){
                    $array['PaymentUrl']='https://www.paypal.com/cgi-bin/webscr';
                }else{
                    $array['PaymentUrl']='https://www.sandbox.paypal.com/cgi-bin/webscr';
                }
            }
        }
        return $array;
    }
    public function AddPaymentTypes($data)
    {
        if(!empty($data['BUID'])){
            $this->BUID = $data['BUID'];
        }else{
            $this->CheckAuthenticated();
        }
        if($this->BUID == null){
            throw new Exception("Sorry BUID is required");
        }
        if (empty($data['CLIENTKEY'])) {
            $data['CLIENTKEY']=null;
        }
        if (empty($data['PAYMENTYPE'])) {
            throw new Exception(__t("Sorry payment type is needed"));
        }
        if (!isset($data['ISACTIVE'])) {
            $data['ISACTIVE']=0;
        }
        if (!isset($data['ISLIVE'])) {
            $data['ISLIVE']=0;
        }
        if (empty($data['CLIENTSECRET'])) {
            $data['CLIENTSECRET']=null;
        }
        if (empty($data['id'])) {
            $data['id']=null;
        }
        $statement = $this->PDO->prepare("{ CALL PHP_Ecommerce_AddPaymentTypes(
                                                @BUID=:BUID,
                                                @PAYMENTYPEID=:PAYMENTYPE,
                                                @ISACTIVE=:ISACTIVE,
                                                @ClientKey=:CLIENTKEY,
                                                @ClientSeret=:CLIENTSECRET,
                                                @ISLIVE=:ISLIVE,
                                                @ID=:ID, 
                                                @OUTID=:OUTID
                                                )}");
        $statement->bindParam(':BUID', $this->BUID, PDO::PARAM_INT);
        $statement->bindParam(':PAYMENTYPE', $data['PAYMENTYPE'], PDO::PARAM_INT);
        $statement->bindParam(':ISACTIVE', $data['ISACTIVE'], PDO::PARAM_INT);

        if (!empty($data['CLIENTKEY'])) {
            $statement->bindParam(':CLIENTKEY', $data['CLIENTKEY'], PDO::PARAM_STR);
        } else {
            $statement->bindParam(':CLIENTKEY', $data['CLIENTKEY'], PDO::PARAM_INT);
        }

        if (!empty($data['CLIENTSECRET'])) {
            $statement->bindParam(':CLIENTSECRET', $data['CLIENTSECRET'], PDO::PARAM_STR);
        } else {
            $statement->bindParam(':CLIENTSECRET', $data['CLIENTSECRET'], PDO::PARAM_INT);
        }

        $statement->bindParam(':ISLIVE', $data['ISLIVE'], PDO::PARAM_INT);
        
        $statement->bindParam(':ID', $data['id'], PDO::PARAM_INT);
        $statement->bindParam(':OUTID', $OUTID, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 400);
        $statement->execute();
        if (empty($OUTID)) {
            throw new Exception(__t("Sorry payment type not added"));
        }

        return ['id'=>$OUTID];
    }

    public function enableSettings($data)
    {
        if(!empty($data['BUID'])){
            $this->BUID = $data['BUID'];
        }else{
            $this->CheckAuthenticated();
        }
        if($this->BUID == null){
            throw new Exception("Sorry BUID is required");
        }
        
        $addStatement = $this->PDO->prepare("{ CALL PHP_Ecommerce_enableGSettings(
                                                @BUID=:BUID,
                                                @couponEnable=:couponEnable,
                                                @deliveryEnable=:deliveryEnable,
                                                @dieinEnable=:dieinEnable,
                                                @pickupEnable=:pickupEnable,
                                                @preorderEnable=:preorderEnable,
                                                @GuestdeliveryEnable=:GuestdeliveryEnable,
                                                @GuestdieinEnable=:GuestdieinEnable,
                                                @GuestpickupEnable=:GuestpickupEnable,
                                                @GuestpreorderEnable=:GuestpreorderEnable
                                                )}");
        $addStatement->bindParam(':BUID', $this->BUID, PDO::PARAM_INT);
        if (!empty($data['couponEnable']) || is_numeric($data['couponEnable'])) {
            $couponEnable = $data['couponEnable'];
        } else {
            $couponEnable = 0;
        }
        $addStatement->bindParam(':couponEnable', $couponEnable, PDO::PARAM_INT);

        if (!empty($data['deliveryEnable']) || is_numeric($data['deliveryEnable'])) {
            $deliveryEnable = $data['deliveryEnable'];
        } else {
            $deliveryEnable = 0;
        }
        $addStatement->bindParam(':deliveryEnable', $deliveryEnable, PDO::PARAM_INT);

        if (!empty($data['dieinEnable']) || is_numeric($data['dieinEnable'])) {
            $dieinEnable = $data['dieinEnable'];
        } else {
            $dieinEnable = 0;
        }
        $addStatement->bindParam(':dieinEnable', $dieinEnable, PDO::PARAM_INT);

        if (!empty($data['pickupEnable']) || is_numeric($data['pickupEnable'])) {
            $pickupEnable = $data['pickupEnable'];
        } else {
            $pickupEnable = 0;
        }
        $addStatement->bindParam(':pickupEnable', $pickupEnable, PDO::PARAM_INT);

        if (!empty($data['preorderEnable']) || is_numeric($data['preorderEnable'])) {
            $preorderEnable = $data['preorderEnable'];
        } else {
            $preorderEnable = 0;
        }
        $addStatement->bindParam(':preorderEnable', $preorderEnable, PDO::PARAM_INT);

        //GUEST
        if (!empty($data['GuestdeliveryEnable']) || is_numeric($data['GuestdeliveryEnable'])) {
            $GuestdeliveryEnable = $data['GuestdeliveryEnable'];
        } else {
            $GuestdeliveryEnable = 0;
        }
        $addStatement->bindParam(':GuestdeliveryEnable', $GuestdeliveryEnable, PDO::PARAM_INT);

        if (!empty($data['GuestdieinEnable']) || is_numeric($data['GuestdieinEnable'])) {
            $GuestdieinEnable = $data['GuestdieinEnable'];
        } else {
            $GuestdieinEnable = 0;
        }
        $addStatement->bindParam(':GuestdieinEnable', $GuestdieinEnable, PDO::PARAM_INT);

        if (!empty($data['GuestpickupEnable']) || is_numeric($data['GuestpickupEnable'])) {
            $GuestpickupEnable = $data['GuestpickupEnable'];
        } else {
            $GuestpickupEnable = 0;
        }
        $addStatement->bindParam(':GuestpickupEnable', $GuestpickupEnable, PDO::PARAM_INT);

        if (!empty($data['GuestpreorderEnable']) || is_numeric($data['GuestpreorderEnable'])) {
            $GuestpreorderEnable = $data['GuestpreorderEnable'];
        } else {
            $GuestpreorderEnable = 0;
        }
        $addStatement->bindParam(':GuestpreorderEnable', $GuestpreorderEnable, PDO::PARAM_INT);


        $addStatement->execute();
        /*if (empty($OUTID)) {
            throw new Exception(__t("Sorry payment type not added"));
        }*/

        return ['id'=>$this->BUID];
    }

    public function GetDeliveyOptionsByBU($data)
    {
        if(!empty($data['BUID'])){
            $this->BUID = $data['BUID'];
        }else{
            $this->CheckAuthenticated();
        }
        if($this->BUID == null){
            throw new Exception("Sorry BUID is required");
        }
        $statement = $this->PDO->prepare("{ CALL PHP_Ecommerce_GetDeliveryOptionsByBU(@BUID=:buid)}");
        $statement->bindParam(':buid', $this->BUID, PDO::PARAM_INT);
        $statement->execute();
        $ret = [];
        do {            
            $rows = $statement->fetchAll(PDO::FETCH_ASSOC); 
            $ret[] =  $rows[0];                         
            $x++;                 
        } while ($statement->nextRowset());
        return $ret;
    }

    public function DeleteEcomForBU($buid)
    {
        if(empty($buid)){
            throw new Exception("Sorry BUID is required");
        }
        $reqDate = gmdate(DATE_ATOM);
        $addStatement = $this->PDO->prepare("{ CALL PHP_Ecommerce_DeleteEcomForBU(@buid=:BUID)}");
        $addStatement->bindParam(':BUID', $buid, PDO::PARAM_INT);

        $addStatement->execute();

        return true;
    }
}
