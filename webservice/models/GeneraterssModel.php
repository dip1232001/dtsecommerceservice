<?php

use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CouponModel
 *
 * @author dipankar
 */
require_once MODEL_PATH . DS . 'App.php';

class GeneraterssModel extends AppModel {

    //put your code here

    public function __construct($callAuth = false) {
        parent::__construct($callAuth);
    }

    public function Generaterss($rssurl) {
        //dd($data,true);
        $contents = file_get_contents($rssurl);
        $xml = simplexml_load_string($contents);
        return  json_encode($xml);
    }

}
