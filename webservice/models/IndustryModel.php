<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of IndustryModel
 *
 * @author dipankar
 */
require_once MODEL_PATH . DS . 'App.php';

class IndustryModel extends AppModel {

    //put your code here
    protected $Table = 'industries';
    protected $BuMapTable = 'BU_INDUSTRY_map';

    public function __construct($callAuth = false) {
        parent::__construct();
        //$this->CheckAuthenticated();
    }

    public function addIndustry($data) {
        if (empty($data['name'])) {
            throw new Exception("Sorry, industry name is required");
        }
        $statement = $this->PDO->prepare("SELECT count(*) as totalIndustry from {$this->DbAlias}{$this->Table} where name=:name");
        $statement->bindParam(':name', $data['name'], PDO::PARAM_STR);
        $statement->execute();
        $doesExists = $statement->fetch(PDO::FETCH_ASSOC);
        if ($doesExists['totalIndustry'] == 0) {
            $insertStateMent = $this->PDO->prepare("INSERT INTO {$this->DbAlias}{$this->Table} (name,folderpath) VALUES (:name,:folderpath)");
            $insertStateMent->bindParam(':name', $data['name'], PDO::PARAM_STR);
            if (!empty($data['folderpath'])) {
                $insertStateMent->bindParam(':folderpath', $data['folderpath'], PDO::PARAM_STR);
            } else {
                $insertStateMent->bindParam(':folderpath', $data['folderpath'] = null, PDO::PARAM_INT);
            }

            $insertStateMent->execute();
            return [
                'name' => $data['name'],
                'id' => $this->PDO->lastInsertId(),
                'folderpath' => $data['folderpath']
            ];
        }
        throw new Exception("Sorry this Industry already added for this Bussiness");
    }

    public function deleteIndustry($industryID) {
        if (empty($industryID)) {
            throw new Exception("Sorry invalid industry");
        }
        if (!is_array($industryID)) {
            $industryID = [$industryID];
        }
        foreach ($industryID as $key => $indID) {
            $indID = (int) $indID;
            if (is_numeric($indID)) {
                $ids[] = (int) $indID;
            }
        }
        $statement = $this->PDO->prepare("DELETE FROM {$this->DbAlias}{$this->Table} where id IN (" . implode(",", $ids) . ")");
        $statement->execute();
        return [
            'id' => $industryID
        ];
    }

    public function mapBuWithIndustry($industryID, $BUID) {
        if(!empty($BUID)){
            $this->BUID = $BUID;
        }else{
            $this->CheckAuthenticated();
        }
        if($this->BUID == null){
            throw new Exception("Sorry BUID is required");
        }
        if (empty($industryID)) {
            throw new Exception("Sorry invalid industry");
        }
        $this->isValid($industryID);
        $insertStatement = $this->PDO->prepare("INSERT INTO {$this->DbAlias}{$this->BuMapTable} (buid,industry_id) VALUES(:buid,:industryid)");
        $insertStatement->bindParam(':buid', $this->BUID, PDO::PARAM_INT);
        $insertStatement->bindParam(':industryid', $industryID, PDO::PARAM_INT);
        $insertStatement->execute();
        return [
            'industry_id' => $industryID,
            'buid' => $this->BUID,
            'id' => $this->PDO->lastInsertId()
        ];
    }

    public function isValid(&$industryID) {
        $checkIFindustryExits = $this->PDO->prepare("Select id  from {$this->DbAlias}{$this->BuMapTable} where buid=:buid");
        $checkIFindustryExits->bindParam(':buid', $this->BUID, PDO::PARAM_INT);
        $checkIFindustryExits->execute();
        $data = $checkIFindustryExits->fetch(PDO::FETCH_ASSOC);
        dd($data,true);
        if (empty($data['id'])) {
            throw new Exception("Sorry Industry not specified for this company");
        }
        $industryID = $data['id'];
        return true;
    }

}
