<?php

use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProductModel
 *
 * @author dipankar
 */
require_once MODEL_PATH . DS . 'App.php';

class LambdaProductModel extends AppModel {

    //put your code here
    public $Table = 'Products';

    public function __construct($callAuth = false) {
        parent::__construct($callAuth);
    }    

    public function GetCategories($BUID, $start = 0, $limit = 9) {

        $statement = $this->PDO->prepare("{ CALL PHP_Lambda_GetCategories(@BUID=:buid,@start=:Start,@limit=:Limit)}");
        $statement->bindParam(':buid', $BUID, PDO::PARAM_INT);
        $statement->bindParam(':Start', $start, PDO::PARAM_INT);
        $statement->bindParam(':Limit', $limit, PDO::PARAM_INT);     

        $statement->execute();
        do {
            $rows = $statement->fetchAll(PDO::FETCH_ASSOC);

            if ($rows) {
                if (isset($rows[0]['TotalProducts'])) {
                    $data['Paginations'] = [
                        'Total' => $rows[0] ['TotalProducts'],
                        'Start' => $start,
                        'limit' => $limit
                    ];
                } else {
                    $data['categories'] = $rows;
                }
            }
        } while ($statement->nextRowset());
        $statement->closeCursor();
        if (empty($data['categories'])) {
            $data['categories'] = [];
        }

        return $data;
    }
    

    public function Getproducts($BUID, $start = 0, $limit = 9, $searchparams = [],  $category_id='', $name='') {
        $stateMent = $this->PDO->prepare("{CALL PHP_Ecommerce_Getproducts(@buid = :buid, @Start = :Start, @Limit = :Limit, @Likename = :LikeName, @INDUSTRYID = :Industry_ID, @CATAGORYID = :Category_id, @SHOWALL = :ShowAll, @PRICEOVER = :PriceOver, @ISFEATURED = :IsFeatured, @categoryslug=:categoryslug)}");
        $stateMent->bindParam(':buid', $BUID, PDO::PARAM_INT);
        $stateMent->bindParam(':Start', $start, PDO::PARAM_INT);
        $stateMent->bindParam(':Limit', $limit, PDO::PARAM_INT);
        $stateMent->bindParam(':LikeName', $searchparams['name'] = null, PDO::PARAM_INT);
        $stateMent->bindParam(':Industry_ID', $searchparams['industry_id'] = null, PDO::PARAM_INT);
        $stateMent->bindParam(':Category_id', $category_id, PDO::PARAM_INT);
        $stateMent->bindParam(':ShowAll', $searchparams['showall'] = null, PDO::PARAM_BOOL);
        $stateMent->bindParam(':PriceOver', $searchparams['priceover'] = null, PDO::PARAM_INT);
        $stateMent->bindParam(':IsFeatured', $searchparams['isfeatured'] = null, PDO::PARAM_INT);
        $stateMent->bindParam(':categoryslug', $category_slug = '', PDO::PARAM_STR);

        $stateMent->execute();
        do {
            $rows = $stateMent->fetchAll(PDO::FETCH_ASSOC);

            if ($rows) {
                if (isset($rows[0]['TotalProducts'])) {
                    $data['Paginations'] = [
                        'Total' => $rows[0] ['TotalProducts'],
                        'Start' => $start,
                        'limit' => $limit
                    ];
                } else {
                    $data['Products'] = $rows;
                }
            }
        } while ($stateMent->nextRowset());
        $stateMent->closeCursor();
        if (empty($data['Products'])) {
            $data['Products'] = [];
        }

        return $data;
    }

    public function addAppointment($data) {

        $statement = $this->PDO->prepare("{ CALL PHP_Lex_AddAppontment(
            @BUID=:BUID
            ,@name=:name
            ,@phone=:phone
            ,@bookingDate=:bookingDate
            ,@bookingTime=:bookingTime
            ,@description=:description
            ,@ID=:ID)}");
        $statement->bindParam(':BUID', $data['BUID'], PDO::PARAM_INT);
        $statement->bindParam(':name', $data['name'], PDO::PARAM_STR);
        $statement->bindParam(':phone', $data['phone'], PDO::PARAM_STR);  
        $statement->bindParam(':bookingDate', $data['bookingDate'], PDO::PARAM_STR);  
        $statement->bindParam(':bookingTime', $data['bookingTime'], PDO::PARAM_STR);  
        $statement->bindParam(':description', $data['description'], PDO::PARAM_STR);  
        $statement->bindParam(':ID', $ID, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 500);

        $statement->execute();
        if (empty($ID)) {
            throw new Exception("Sorry appoinment not created");
        }
        return ['ID' => $ID];
    }

    public function GetAppointments($BUID, $start = 0, $limit = 9, $searchparams = []) {
        $stateMent = $this->PDO->prepare("{CALL PHP_Lambda_GetAppoinments(@BUID = :buid, @Start = :Start, @Limit = :Limit)}");
        $stateMent->bindParam(':buid', $BUID, PDO::PARAM_INT);
        $stateMent->bindParam(':Start', $start, PDO::PARAM_INT);
        $stateMent->bindParam(':Limit', $limit, PDO::PARAM_INT);
        $stateMent->execute();
        do {
            $rows = $stateMent->fetchAll(PDO::FETCH_ASSOC);

            if ($rows) {
                if (isset($rows[0]['TotalProducts'])) {
                    $data['Paginations'] = [
                        'Total' => $rows[0] ['TotalProducts'],
                        'Start' => $start,
                        'limit' => $limit
                    ];
                } else {
                    $data['Products'] = $rows;
                }
            }
        } while ($stateMent->nextRowset());
        $stateMent->closeCursor();
        if (empty($data['Products'])) {
            $data['Products'] = [];
        }

        return $data;
    }


    public function addFeedack($data) {

        $statement = $this->PDO->prepare("{ CALL PHP_Lex_AddAppontment(
            @BUID=:BUID
            ,@name=:name
            ,@phone=:phone
            ,@bookingDate=:bookingDate
            ,@bookingTime=:bookingTime
            ,@description=:description
            ,@ID=:ID)}");
        $statement->bindParam(':BUID', $data['BUID'], PDO::PARAM_INT);
        $statement->bindParam(':name', $data['name'], PDO::PARAM_STR);
        $statement->bindParam(':phone', $data['phone'], PDO::PARAM_STR);  
        $statement->bindParam(':bookingDate', $data['bookingDate'], PDO::PARAM_STR);  
        $statement->bindParam(':bookingTime', $data['bookingTime'], PDO::PARAM_STR);  
        $statement->bindParam(':description', $data['description'], PDO::PARAM_STR);  
        $statement->bindParam(':ID', $ID, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 500);

        $statement->execute();
        if (empty($ID)) {
            throw new Exception("Sorry appoinment not created");
        }
        return ['ID' => $ID];
    }

    public function placeLambdaOrder($data){

        //$url = $data['img'];
        $folder="../qrimg/";
        $file_name="order-JSON.txt";
        $file_nameR='qrimg/'.$file_name;
        $file_name=$folder.$file_name;
        
        $fp = fopen ($file_name, 'w+');
        fwrite($fp, json_encode($data));        
        fclose($fp);




        $CUSTOMERID = NULL ;
        //Check Existing Customer
        $customer = $this->GetCustomers($data['BUID'],$data['email'],0,2);
        if($customer['Pagination']['total']>0){
            $CUSTOMERID = $customer['Customers'][0]['id'];
        }else{
            $customer = $this->GetCustomers($data['BUID'],$data['phone'],0,2);
            if($customer['Pagination']['total']>0){
                $CUSTOMERID = $customer['Customers'][0]['id'];
            }
        }
        
        //CREATE NEW CUSTOMER
        if($CUSTOMERID == NULL){
            $CreateUser = [
                'email' => $data['email'],
                'phone' => $data['phone'],
                'firstname' => $data['firstname'],
                'lastname' => $data['lastname'],
                'password' => '123456',
                'BUID' => $data['BUID']
            ];
            $ret = $this->CreateUser($CreateUser);
            $CUSTOMERID = $ret['CustomerID'];
        }

        //CHECK CUSTOMER BILLING ADDRESS
        $checkBillingAddress = $this->checkAddress($CUSTOMERID, 'billing', $data['street']);
        if (empty($checkBillingAddress['id'])) {
            $baddr = [
                'customer_id' => $CUSTOMERID,
                'address' => $data['street'],
                'name' => $data['firstname'] . ' ' . $data['lastname'],
                'phoneno' => $data['phone'],
                'pincode' => $data['postalCode'],
                'city' => $data['city'],
                'state' => $data['state'],
                'landmark' => $data['landmark'],
                'alrternetPH' => null,
                'address_type' => 'billing'
            ];
            $addRess = $this->AddCustomerAddress($baddr);
            $checkBillingAddress['id'] = $addRess['AddressID'];
        }
        
        //CHECK CUSTOMER SHIPPING ADDRESS
        $checkshippingAddress = $this->checkAddress($CUSTOMERID, 'shipping', $data['street']);
        if (empty($checkshippingAddress['id'])) {
            $addRess = $this->AddCustomerAddress(
                [
                    'customer_id' => $CUSTOMERID,
                    'address' => $data['street'],
                    'name' => $data['firstname'] . ' ' . $data['lastname'],
                    'phoneno' => $data['phone'],
                    'pincode' => $data['postalCode'],
                    'city' => $data['city'],
                    'state' => $data['state'],
                    'landmark' => $data['landmark'],
                    'alrternetPH' => null,
                    'address_type' => 'shipping'
                ]
            );
            $checkshippingAddress['id'] = $addRess['AddressID'];
        }

        $PRODUCT = $this->getProduct($data['BUID'],$data['product_id']);

        $CARTITEMS = array();
        $CARTITEMS[0] = $PRODUCT['ProductDetails'];
        $CARTITEMS[0]["baseprice"] = $PRODUCT['ProductDetails']['price'];
        $CARTITEMS[0]["cartQuantity"] = $data['qty'];
        $CARTITEMS[0]["cartid"] = "";

       

        $PAYMENTTYPES = $this->getPaymentTypes($data['BUID']);

        $ordersummary['cartitems'] = $CARTITEMS;
        $ordersummary['customer_id'] = $CUSTOMERID;
        $ordersummary['identifier'] = '';
        $ordersummary['priceInfo']['Discounts'] = 0;
        $ordersummary['priceInfo']['Gross'] = $PRODUCT['ProductDetails']['price'] * $data['qty'];
        $ordersummary['priceInfo']['shipping'] = 0;
        $ordersummary['priceInfo']['subtotal'] = $PRODUCT['ProductDetails']['price'] * $data['qty'];

        $orderSummary = [
            'ordersummary' => $ordersummary,
            'payment_type' => $PAYMENTTYPES[0]['id'],
            'customer_id' => $CUSTOMERID,
            'customer_billing_add_id' => $checkBillingAddress['id'],
            'customer_shipping_add_id' => $checkshippingAddress['id']
        ];

        $order = $this->PrepareOrder($data['BUID'], $orderSummary);

        dd($order, true);
    }

    public function GetCustomers($buid, $searchstring = null, $start = 0, $limit = 20) {
        if (empty($start)) {
            $start = 0;
        }
        if (empty($limit) || $limit > 20) {
            $limit = 20;
        }
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_GetCustomers(@BUID=:buid,@Start=:start,@Limit=:limit,@SearchString=:searchstring)}");
        $statement->bindParam(':buid', $buid, PDO::PARAM_INT);
        $statement->bindParam(':start', $start, PDO::PARAM_INT);
        $statement->bindParam(':limit', $limit, PDO::PARAM_INT);
        if (empty($searchstring)) {
            $statement->bindParam(':searchstring', $searchstring, PDO::PARAM_INT);
        } else {
            $searchstring = trim(strip_tags($searchstring));
            $statement->bindParam(':searchstring', $searchstring, PDO::PARAM_STR);
        }
        $statement->execute();
        $x = 0;
        $data = [];
        do {
            $rows = $statement->fetchAll(PDO::FETCH_ASSOC);

            //if ($rows!=false) {
            switch ($x) {
                case 0:
                    $data['Pagination'] = [
                        'total' => (int) $rows[0]['totalCustomers'],
                        'start' => $start,
                        'limit' => $limit
                    ];
                    break;
                case 1:
                    $data['Customers'] = $rows;
                    break;
            }
            $x++;
            // }
        } while ($statement->nextRowset());
        $statement->closeCursor();
        return $data;
    }

    public function CreateUser($data) {
        if(empty($data['type'])){
            $data['type'] = 1;
        }
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_CustomerRegistration("
                . "@EMAIL=:email,"
                . "@PHONENumber=:phone,"
                . "@FIRSTNAME=:firstname,"
                . "@LASTNAME=:lastname,"
                . "@PASSWORD=:password,"
                . "@DOB=:dob,"
                . "@BUID=:buid,"
                . "@CustomerID=:createdcustomer,"
                . "@CountryCode=:CountryCode,"
                . "@Gender=:Gender,"
                . "@marriageAnniversaryDate=:marriageAnniversaryDate,"
                . "@regType=:regType,"
                . "@marketplaceCustomerID=:marketplaceCustomerID,"
                . "@marketplaceCustomerDataRef=:marketplaceCustomerDataRef,"
                . "@ID=:customer"
                . ")}");
        $statement->bindParam(':email', $data['email'], PDO::PARAM_STR);
        $statement->bindParam(':phone', $data['phone'], PDO::PARAM_STR);
        $statement->bindParam(':firstname', $data['firstname'], PDO::PARAM_STR);
        $statement->bindParam(':lastname', $data['lastname'], PDO::PARAM_STR);
        $statement->bindParam(':password', $data['password'], PDO::PARAM_STR);
        $statement->bindParam(':buid', $data['BUID'], PDO::PARAM_INT);
        if (empty($data['id'])) {
            $data['id'] = null;
        }
        $statement->bindParam(':createdcustomer', $data['id'], PDO::PARAM_INT);
        if (!empty($data['dob'])) {
            $statement->bindParam(':dob', $data['dob'], PDO::PARAM_STR);
        } else {
            $statement->bindParam(':dob', $data['dob'] = NULL, PDO::PARAM_INT);
        }
        if (!empty($data['CountryCode'])) {
            $statement->bindParam(':CountryCode', $data['CountryCode'], PDO::PARAM_STR);
        } else {
            $statement->bindParam(':CountryCode', $data['CountryCode'] = NULL, PDO::PARAM_INT);
        }
        $statement->bindParam(':Gender', $data['Gender'], PDO::PARAM_INT);
        if (!empty($data['marriageAnniversaryDate'])) {
            $statement->bindParam(':marriageAnniversaryDate', $data['marriageAnniversaryDate'], PDO::PARAM_STR);
        } else {
            $statement->bindParam(':marriageAnniversaryDate', $data['marriageAnniversaryDate'] = NULL, PDO::PARAM_INT);
        }
        $statement->bindParam(':regType', $data['type'], PDO::PARAM_INT);

        $statement->bindParam(':marketplaceCustomerID', $data['marketplaceCustomerID'] = NULL, PDO::PARAM_INT);
        $statement->bindParam(':marketplaceCustomerDataRef', $data['marketplaceCustomerDataRef'] = '', PDO::PARAM_STR);
        
        $statement->bindParam(':customer', $customerID, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 500);
        $statement->execute();
        if (empty($customerID)) {
            throw new Exception("Sorry customer not created");
        }
        return ['CustomerID' => $customerID];
    }

    public function checkAddress($customerid, $address_type, $address) {
        
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_CheckCustomerAddress(
            @customer_id=:customer_id,
            @address=:address,
            @address_type=:address_type)}");
        
        $statement->bindParam(':customer_id', $customerid, PDO::PARAM_INT);
        $statement->bindParam(':address', $address, PDO::PARAM_STR);
        $statement->bindParam(':address_type', $address_type, PDO::PARAM_STR);
        $statement->execute();
        return $statement->fetch(PDO::FETCH_ASSOC);
    }

    public function AddCustomerAddress($data) {
        if (empty($data['is_default'])) {
            $data['is_default'] = 0;
        }
        if (empty($data['alrternetPH'])) {
            $data['alrternetPH'] = null;
        }
        if (empty($data['landmark'])) {
            $data['landmark'] = null;
        }
        if (empty($data['phonecode'])) {
            $data['phonecode'] = null;
        }
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_AddCustomerAddress(
            @customer_id=:customer_id,
            @address=:address,
            @name=:name,
            @phoneno=:phoneno,
            @phonecode=:pincode,
            @address_type=:address_type,
            @city=:city,
            @state=:state,
            @landmark=:landmark,
            @alrternetPH=:alrternetPH,
            @is_default=:is_default,
            @ID=:ID,
            @Country=:country,
            @AddressID=:AddressID)}");
        if (empty($data['customer_id'])) {
            $data['customer_id'] = null;
        }
        $statement->bindParam(':customer_id', $data['customer_id'], PDO::PARAM_INT);
        $statement->bindParam(':address', $data['address'], PDO::PARAM_STR);
        $statement->bindParam(':name', $data['name'], PDO::PARAM_STR);
        $statement->bindParam(':phoneno', $data['phoneno'], PDO::PARAM_STR);
        $statement->bindParam(':pincode', $data['pincode'], PDO::PARAM_STR);
        $statement->bindParam(':city', $data['city'], PDO::PARAM_STR);
        $statement->bindParam(':state', $data['state'], PDO::PARAM_STR);
        $statement->bindParam(':country', $data['country'], PDO::PARAM_STR);
        if (empty($data['landmark'])) {
            $statement->bindParam(':landmark', $data['landmark'] = null, PDO::PARAM_INT);
        } else {
            $statement->bindParam(':landmark', $data['landmark'], PDO::PARAM_STR);
        }
        if ($data['alrternetPH']) {
            $statement->bindParam(':alrternetPH', $data['alrternetPH'] = null, PDO::PARAM_INT);
        } else {
            $statement->bindParam(':alrternetPH', $data['alrternetPH'], PDO::PARAM_STR);
        }
        $statement->bindParam(':is_default', $data['is_default'], PDO::PARAM_INT);
        if (empty($data['address_type']) || ($data['address_type'] != "shipping" && $data['address_type'] != "billing")) {
            $data['address_type'] = "shipping";
        }
        $statement->bindParam(':address_type', $data['address_type']);
        if (empty($data['id'])) {
            $data['id'] = null;
        }
        $statement->bindParam(':ID', $data['id'], PDO::PARAM_INT);
        $statement->bindParam(':AddressID', $AddressID, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 40);
        if (!$statement->execute()) {
            throw new Exception(__t("Sorry customer address not added"));
        }
        $statement->closeCursor();
        return ['AddressID' => $AddressID];
    }

    public function AddToCart($data)
    {

        if (empty($data['cartItems'])) {
            throw new Exception(__t("Sorry no cart items added"));
        }
        else {
            if (isset($data['cartItems']['product_id'])) {
                $data['cartItems'] = [$data['cartItems']];
            }
            //return dd($data['cartItems'],true);
            /*$productmodel = new ProductModel();
            foreach ($data['cartItems'] as $k => $product) {
                if (empty($product['varients'])) {
                    $productdetails = $productmodel->getProduct($product['product_id']);
                    if (!empty($productdetails['ProductDetails']['ProductVarients'])) {
                        $varientadded = false;
                        foreach ($productdetails['ProductDetails']['ProductVarients'] as $varient) {
                            if (!$varientadded && $varient['is_required'] == 1) {
                                $data['cartItems'][$k]['varients'][0]['id'] = $varient['options'][0]['id'];
                                $data['cartItems'][$k]['varients'][0]['quantity'] = $product['quantity'];
                                $varientadded = true;
                                break;
                            }
                            continue;
                        }
                        if ($varientadded) {
                            break;
                        }
                    }
                }

            }*/

            $data['cartItems'] = $this->__prepareXml($data['cartItems']);
        }
        if (empty($data['cartItems'])) {
            throw new Exception(__t("Sorry no cart items added"));
        }
        if (empty($data['identifier'])) {
            if (empty($_COOKIE['identifier'])) {
                $cookieidentifier = 'udtc' . $data['BUID'] . time();
                setCookies('identifier', $cookieidentifier, time() + 24 * 60 * 60 * 30 * 10, '/');
                $data['identifier'] = $cookieidentifier;
            }
            else {
                $data['identifier'] = $_COOKIE['identifier'];
            }
        }

        if (empty($data['customer_id'])) {
            $data['customer_id'] = null;
        }

        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_AddTo_Customer_Cart (@BUID=:buid,@customer_id=:customer_id, @identifier=:identifier,@CartXml=:cartitems)}");
        $statement->bindParam(':buid', $data['BUID'], PDO::PARAM_INT);
        $statement->bindParam(':customer_id', $data['customer_id'], PDO::PARAM_INT);
        $statement->bindParam(':identifier', $data['identifier'], PDO::PARAM_STR);
        $statement->bindParam(':cartitems', $data['cartItems'], PDO::PARAM_STR);
        $return = $statement->execute();
        $statement->closeCursor();
        return $return;
    }

    private function __prepareXml($productsdata)
    {
        $xml = '';
        foreach ($productsdata as $key => $cartitem) {
            $xml .= '<cartitems>';
            $xml .= '<product_id>' . (int)trim(strip_tags($cartitem['product_id'])) . '</product_id>';
            $xml .= '<quantity>' . (int)trim(strip_tags( (empty($cartitem['quantity']) ? 1 : $cartitem['quantity']))) . '</quantity>';
            if (!empty($cartitem['varients'])) {
                $xml .= '<varients>';
                foreach ($cartitem['varients'] as $key => $vari) {
                    $xmljson[] = $vari;
                }
                $xml .= json_encode($xmljson);
                $xml .= '</varients>';
            }
            $xml .= '</cartitems>';
        }

        if (!empty($xml)) {
            return '<data>' . $xml . '</data>';
        }

        return false;
    }

    public function getProduct($BUID, $ProductID) {
        if (empty($ProductID)) {
            throw new Exception("Product id not specified");
        }
        $stateMent = $this->PDO->prepare("{CALL PHP_Ecommerce_GetProductDetails(@BUID=:buid,@PRODUCTID=:productid,@PRODUCTSLUG=:PRODUCTSLUG)}");
        $stateMent->bindParam(':buid', $BUID, PDO::PARAM_INT);
        $stateMent->bindParam(':productid', $ProductID, PDO::PARAM_INT);
        $stateMent->bindParam(':PRODUCTSLUG', $ProductSlug, PDO::PARAM_STR);
        $stateMent->execute();
        $x = 0;
        $data = [];
        do {
            $rows = $stateMent->fetchAll(PDO::FETCH_ASSOC);
            //dd($rows);
            //if ($rows!=false) {
            switch ($x) {
                case 0:
                    $data['ProductDetails'] = $rows[0];
                    /*if ($data['ProductDetails']['is_featured'] == 1) {
                        $data['ProductDetails']['is_featured'] = true;
                    } else {
                        $data['ProductDetails']['is_featured'] = false;
                    }*/
                    break;
                case 1:
                    //$data['ProductDetails']['ProductImages'] = $rows;
                    break;
                case 2:
                    /*foreach($rows as $k=>$v){
                        //dd($v,true);
                        $rows[$k]['videoURL'] = base64_decode($v['videoURL']);
                    }*/
                    //$data['ProductDetails']['VIDEOS'] = $rows;
                    break;
                case 3:
                    //$data['ProductDetails']['DOCUMENTS'] = $rows;
                    break;
                case 4:
                    /*$artags = '';
                    foreach ($rows as &$tag) {
                        $artags = $artags . $tag['name']. ','; 
                    }
                    $artags  = rtrim($artags,',');
                    $data['ProductDetails']['ARTAGS'] = $artags;*/
                    break;
                case 5:
                    //$data['ProductDetails']['ProductVarients'] = $rows;
                    break;
                case 6:
                    /*foreach ($rows as &$varentoption) {
                        if ((int) $varentoption['pricetype'] == 0) {
                            $varentoption['pricetype'] = "-";
                        } else {
                            $varentoption['pricetype'] = "+";
                        }
                        if ($varentoption['price'] == .00) {
                            $varentoption['price'] = 0.00;
                        }
                        $keyVarientmap = array_search_find_key($data['ProductDetails']['ProductVarients'], 'id', $varentoption['varient_id']);
                        if ($data['ProductDetails']['ProductVarients'][$keyVarientmap]['is_required'] == 0) {
                            $data['ProductDetails']['ProductVarients'][$keyVarientmap]['is_required'] = false;
                        } else {
                            $data['ProductDetails']['ProductVarients'][$keyVarientmap]['is_required'] = true;
                        }
                        if ($data['ProductDetails']['ProductVarients'][$keyVarientmap]['is_multiple'] == 0) {
                            $data['ProductDetails']['ProductVarients'][$keyVarientmap]['is_multiple'] = false;
                        } else {
                            $data['ProductDetails']['ProductVarients'][$keyVarientmap]['is_multiple'] = true;
                        }
                        if ($data['ProductDetails']['ProductVarients'][$keyVarientmap]['is_required'] == false && strtolower($varentoption['title']) !== 'select') {
                            if (!isset($data['ProductDetails']['ProductVarients'][$keyVarientmap]['options'][0])) {
                                $data['ProductDetails']['ProductVarients'][$keyVarientmap]['options'][0] = [
                                    'title' => __t("Select"),
                                    'price' => 0,
                                    'pricetype' => '+',
                                    'sort_order' => 0,
                                    'id' => 0,
                                    'type' => 'default'
                                ];
                            }
                        }
                        $data['ProductDetails']['ProductVarients'][$keyVarientmap]['options'][] = $varentoption;
                    }*/

                    break;
            }
            $x++;
            // }
        } while ($stateMent->nextRowset());
        $stateMent->closeCursor();
        if (empty($data)) {
            throw new Exception("Sorry Product does not exists");
        }
        /*if (!isset($data['ProductDetails']['ProductImages'])) {
            $data['ProductDetails']['ProductImages'] = [];
        }
        if (!isset($data['ProductDetails']['ProductVarients'])) {
            $data['ProductDetails']['ProductVarients'] = [];
        }*/
        return $data;
    }

    public function getPaymentTypes($BUID)
    {
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_GetPaymentTypes(@BUID=:buid)}");
        $statement->bindParam(':buid', $BUID, PDO::PARAM_INT);
        $statement->execute();
        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    public function PrepareOrder($BUID, $data)
    {
        //dd($data,true);
        if (!is_array($data['ordersummary'])) {
            if ( ($jsonorderar = json_decode($data['ordersummary'], true)) != false) {
                $data['ordersummary'] = $jsonorderar;
            }
            else {
                throw new Exception(__t("order details is required"));
            }
        }
        if (empty($data['customer_id'])) {
            throw new Exception(__t("Customerid is required"));
        }
        if (empty($data['ordersummary'])) {
            throw new Exception(__t("order details is required"));
        }
        if (empty($data['ordersummary']['priceInfo']) || !isset($data['ordersummary']['priceInfo']['Gross'])) {
            throw new Exception(__t("cant process order as no price info defined"));
        }
        if (empty($data['payment_type'])) {
            throw new Exception(__t("Payment type is required before processing order"));
        }
        /*if (empty($data['customer_billing_add_id'])) {
            throw new Exception(__t("Customer billing Address is required"));
        }
        if (empty($data['customer_shipping_add_id'])) {
            throw new Exception(__t("Customer shipping Address is required"));
        }*/
        if (empty($data['payment_status_id'])) {
            $data['payment_status_id'] = null;
        }

        if (empty($data['order_status_id'])) {
            $data['order_status_id'] = 1;
        }

        if (!empty($data['tableNo'])) {
            $data['ordersummary']['tableNo'] = $data['tableNo'];
        }

        if (!empty($data['domain'])) {
            $data['ordersummary']['domain'] = $data['domain'];
        }

        if (!empty($data['bu_name'])) {
            $data['ordersummary']['bu_name'] = $data['bu_name'];
        }


        $cartids = [];
        $productids = [];
        foreach ($data['ordersummary']['cartitems'] as $key => $value) {
            $cartids[] = $value['cartid'];
            $productids[] = $value['id'];
        }

        $statement = $this->PDO->prepare("{CALL PHP_Lambda_PrepareOrder("
            . "@customer_id=:customer_id,"
            . "@buid=:buid,"
            . "@ordersummary=:ordersummary,"
            . "@grandtotal=:grandtotal,"
            . "@payment_type=:payment_type,"
            . "@payment_status_id=:payment_status_id,"
            . "@order_status_id=:order_status_id,"
            . "@customer_billing_add_id=:customer_billing_add_id,"
            . "@customer_shipping_add_id=:customer_shipping_add_id,"
            . "@OrderID=:OrderID,"
            . "@ID=:ID,"
            . "@CartIDS=:cartids,"
            . "@productids=:productids"
            . ")}");
        $ordersummary = json_encode($data['ordersummary']);
        if (empty($data['id'])) {
            $data['id'] = null;
        }
        $statement->bindParam(':customer_id', $data['customer_id'], PDO::PARAM_INT);
        $statement->bindParam(':buid', $BUID, PDO::PARAM_INT);
        $statement->bindParam(':ordersummary', $ordersummary, PDO::PARAM_STR);
        $statement->bindParam(':grandtotal', $data['ordersummary']['priceInfo']['Gross']);
        $statement->bindParam(':payment_type', $data['payment_type'], PDO::PARAM_INT);
        $statement->bindParam(':payment_status_id', $data['payment_status_id'], PDO::PARAM_INT);
        $statement->bindParam(':order_status_id', $data['order_status_id'], PDO::PARAM_INT);
        $statement->bindParam(':customer_billing_add_id', $data['customer_billing_add_id'], PDO::PARAM_INT);
        $statement->bindParam(':customer_shipping_add_id', $data['customer_shipping_add_id'], PDO::PARAM_INT);
        $statement->bindParam(':OrderID', $data['id'], PDO::PARAM_INT);
        $statement->bindParam(':ID', $orderid, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 250);
        /*if (!empty($cartids)) {
            $statement->bindParam(':cartids', $cartids = implode(",", $cartids), PDO::PARAM_STR);
        }
        else {
            $statement->bindParam(':cartids', $cartids = null, PDO::PARAM_INT);
        }*/
        $statement->bindParam(':cartids', $cartids = 6, PDO::PARAM_INT);

        if (!empty($productids)) {
            $statement->bindParam(':productids', $productids = implode(",", $productids), PDO::PARAM_STR);
        }
        else {
            $statement->bindParam(':productids', $productids = null, PDO::PARAM_INT);
        }
        $statement->execute();
        if (empty($orderid)) {
            throw new Exception(__t("Order not generated"));
        }
        if (!empty($orderid)) {
            //$EmailModel = new EmailModel();
            $emailData = $this->prepareEmailData($BUID,$orderid, 'newOrder');

        }

        return ['orderid' => $orderid];
    }

    public function prepareEmailData($BUID, $order_id, $Email_Type) {
        if (empty($Email_Type)) {
            throw new Exception(__t("Sorry email type not specified"));
        }

        $pusherArr = array('PushTitle' => '', 'PushMessage' => '');

        $statement = $this->PDO->prepare("{ CALL PHP_Ecommerce_GetGeneralSettings(@BUID=:buid)}");
        $statement->bindParam(':buid', $BUID, PDO::PARAM_INT);
        $statement->execute();
        $generalSettings = $statement->fetch(PDO::FETCH_ASSOC);
        //dd($generalSettings);

        $start = 0;
        $limit = 20;
        $data = array('customer_id' => null, 'startdate' => null, 'enddate' => null, 'identifier' => null);

        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_GetOrders("
                . "@customer_id=:customer_id,"
                . "@order_id=:order_id,"
                . "@buid=:buid,"
                . "@start=:start,"
                . "@Limit=:limit,"
                . "@startdate=:startdate,"
                . "@enddate=:enddate,"
                . "@identifier=:identifier"
                . ")}");
        $statement->bindParam(':customer_id', $data['customer_id'], PDO::PARAM_INT);
        $statement->bindParam(':order_id', $order_id, PDO::PARAM_INT);
        $statement->bindParam(':buid', $BUID, PDO::PARAM_INT);
        $statement->bindParam(':start', $start, PDO::PARAM_INT);
        $statement->bindParam(':limit', $limit, PDO::PARAM_INT);
        $statement->bindParam(':startdate', $data['startdate'], PDO::PARAM_INT);
        $statement->bindParam(':enddate', $data['enddate'], PDO::PARAM_INT);
        $statement->bindParam(':identifier', $data['identifier'], PDO::PARAM_STR);

        $statement->execute();
        $data = [];
        do {
            $rows = $statement->fetchAll(PDO::FETCH_ASSOC);

            if ($rows) {
                if (isset($rows[0]['Total'])) {
                    $data['Paginations'] = [
                        'Total' => $rows[0] ['Total'],
                        'Start' => $start,
                        'limit' => $limit
                    ];
                } else {
                    $data['orders'] = array_map(array($this, "json_decode_summary"), $rows);
                }
            }
        } while ($statement->nextRowset());

        $orders = $data['orders'];
        //dd($orders, true);

        $orderdetails = '';
        foreach ($orders[0]['ordersummary']['cartitems'] as $v) {
            $orderdetails .='<tr>
                                <td style="border-bottom:1px solid #ddd;font-size:14px;font-weight:bold;padding:5px 10px;"><img src="' . $v['imagePath'] . '" style="width:100px"></td>
                                <td style="border-bottom:1px solid #ddd; font-size:14px;font-weight:bold;padding:5px 10px;">' . $v['Productname'] . '</td>
                                <td style="border-bottom:1px solid #ddd;font-size:14px;font-weight:bold;padding:5px 10px;text-align:right;">' . $v['cartQuantity'] . '</td>
                                <td style="border-bottom:1px solid #ddd;font-size:14px;font-weight:bold;padding:5px 10px;text-align:right;">' . $orders[0]['OrderCurrecnyCode'] . ' ' . number_format($v['baseprice'], 2) . '</td>
                                <td style="border-bottom:1px solid #ddd;font-size:14px;font-weight:bold;padding:5px 10px;text-align:right;width:150px;">' . $orders[0]['OrderCurrecnyCode'] . ' ' . number_format($v['price'], 2) . '</td>
                            </tr>';
        }

        $tableno = '';
        if($orders[0]['ordersummary']['tableNo']!=''){
            $tableno = '<b>Table No:</b> '.$orders[0]['ordersummary']['tableNo'];
        }

        $billingAddress = '';        
        if($orders[0]['Billingaddressid']!=''){
            $billingAddress = '<b>Billing Details</b> <br /> '.$orders[0]['Billingname'] . ", " . $orders[0]['Billingaddress'] . ", " . $orders[0]['BillingCity'] . ", " . $orders[0]['BillingState'] . ", " . $orders[0]['ShippingCountry'] . ", " . $orders[0]['BillingPincode'];
        }

        $shippinhAddress = '';        
        if($orders[0]['ShippingaddressID']!=''){
            $shippinhAddress = '<b>Ship To </b><br /> '.$orders[0]['Shippingname'] . ", " . $orders[0]['Shippingaddress'] . ", " . $orders[0]['ShippingCity'] . ", " . $orders[0]['ShippingState'] . ", " . $orders[0]['ShippingCountry'] . ", " . $orders[0]['ShippingPincode'];
        }

        $email_subject_keywords = ['##CUSTOMERNAME##' => $orders[0]['Billingname'],
            '##ORDERIDENTIFIER##' => $orders[0]['orderidentifier'],
            '&nbsp;' => ''];

        $email_body_keywords = ['##ORDERIDENTIFIER##' => $orders[0]['orderidentifier'],
            '##CUSTOMERNAME##' => $orders[0]['Billingname'],
            '##BILLIBGNAME##' => $orders[0]['Billingname'],
            '##BILLINGADDRESS##' => $orders[0]['Billingaddress'] . ", " . $orders[0]['BillingCity'],
            '##BILLINGADDRESS1##' => $orders[0]['BillingState'] . ", " . $orders[0]['ShippingCountry'] . ", " . $orders[0]['BillingPincode'],
            '##PAYMENTMETHOD##' => $orders[0]['PaymentTypeName'],
            '##SHIPPINGNAME##' => $orders[0]['Shippingname'],
            '##SHIPPINGADDRESS##' => $orders[0]['Shippingaddress'] . ", " . $orders[0]['ShippingCity'],
            '##SHIPPINGADDRESS1##' => $orders[0]['ShippingState'] . ", " . $orders[0]['ShippingCountry'] . ", " . $orders[0]['ShippingPincode'],
            '##SHIPPIGMETHOD##' => '',
            '<!--ORDERDETAILS-->' => $orderdetails,
            '<!--TABLENO-->' => $tableno,
            '<!--SHIPTO-->' => $shippinhAddress,
            '<!--BILLINGDETAILS-->' => $billingAddress,
            '##SUBTOTAL##' => number_format($orders[0]['ordersummary']['priceInfo']['subtotal'], 2),
            '##SHIPPINGCHARGE##' => number_format($orders[0]['ordersummary']['priceInfo']['shipping'], 2),
            '##TAX##' => number_format($orders[0]['ordersummary']['priceInfo']['Taxes'], 2),
            '##OTHERTAX##' => '0.00',
            '##TOTAL##' => number_format($orders[0]['ordersummary']['priceInfo']['Gross'], 2),
            '##SHIPPEDBY##' => $orders[0]['shipperaddress'],
            '##TRACKINGID##' => $orders[0]['trackingid'],
            '##STORENAME##' => $generalSettings['store_name'],
            '##STOREEMAIL##' => $generalSettings['store_email'],
            '##LOGO##' => $generalSettings['default_image'],
            '##CUSTOMEREMAIL##' => $orders[0]['Email'],
            '##ORDERDATE##' => $orders[0]['FormatedDateTime'],
            '##ORDERCURRENCY##' => $orders[0]['OrderCurrecnyCode'],
            '##DISCOUNT##' => number_format($orders[0]['ordersummary']['priceInfo']['Discounts'], 2),
            '##SHIPPINGDETAILS##' => $orders[0]['Shippingname'] . ", " . $orders[0]['Shippingaddress'] . ", " . $orders[0]['ShippingCity'] . ", " . $orders[0]['ShippingState'] . ", " . $orders[0]['ShippingCountry'] . ", " . $orders[0]['ShippingPincode'],
            '##BILLINGDETAILS##' => $orders[0]['Billingname'] . ", " . $orders[0]['Billingaddress'] . ", " . $orders[0]['BillingCity'] . ", " . $orders[0]['BillingState'] . ", " . $orders[0]['ShippingCountry'] . ", " . $orders[0]['BillingPincode'],
            '##ORDERSTATUS##' => $orders[0]['OrderStatus'],
            '##CUSTOMERPHONE##' => ''
        ];

        if ($Email_Type == 'shipment') {

            $pusherArr['PushTitle'] = 'Shipment Notice';
            $pusherArr['PushMessage'] = 'Dear ' . $orders[0]['Billingname'] . ', Your Shipment for Order ' . $orders[0]['orderidentifier'] . ' has been despatched';

            $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_GetEmailTemplate(
                                            @BUID=:buid,
                                            @TeamplateType=:TemplateType)}");
            $statement->bindParam(':buid', $BUID, PDO::PARAM_INT);
            $statement->bindParam(':TemplateType', $Email_Type, PDO::PARAM_STR);
            $statement->execute();
            $emailTemplate = $statement->fetch(PDO::FETCH_ASSOC);

            $retData = array();

            $retData['email_subject'] = str_replace(array_keys($email_subject_keywords), $email_subject_keywords, $emailTemplate['email_subject']);

            $retData['email_body'] = str_replace(array_keys($email_body_keywords), $email_body_keywords, $emailTemplate['email_body']);

            $retData['to_email'] = $orders[0]['Email'];

            $CURLOPT_POSTFIELDS = array(
                'To' => $retData['to_email'],
                'Subject' => $retData['email_subject'],
                'MAilBody' => $retData['email_body'],
                'ReplyTo' => $generalSettings['store_email'],
                'ReplyToDisplay' => $generalSettings['store_name'],
                'MailFromDisplay' => $generalSettings['store_name']
            );

            $response = callApi(API_URL_ENDPOINT . '/webapi/Mail/SendMail', 'POST', $CURLOPT_POSTFIELDS);
        }
        if ($Email_Type == 'newOrder') {
            $et = "new_order_placed";

            $pusherArr['PushTitle'] = 'New Order Placed';
            $pusherArr['PushMessage'] = 'Dear ' . $orders[0]['Billingname'] . ', Your Order ' . $orders[0]['orderidentifier'] . ' Placed Successfully';

            $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_GetEmailTemplate(
                                            @BUID=:buid,
                                            @TeamplateType=:TemplateType)}");
            $statement->bindParam(':buid', $BUID, PDO::PARAM_INT);
            $statement->bindParam(':TemplateType', $et, PDO::PARAM_STR);
            $statement->execute();
            $emailTemplate = $statement->fetch(PDO::FETCH_ASSOC);

            $retData = array();

            $retData['email_subject'] = str_replace(array_keys($email_subject_keywords), $email_subject_keywords, $emailTemplate['email_subject']);

            $retData['email_body'] = str_replace(array_keys($email_body_keywords), $email_body_keywords, $emailTemplate['email_body']);

            $retData['to_email'] = $orders[0]['Email'];
            //$retData['to_email'] = 'achyut.sinha@dreamztech.com';

            $CURLOPT_POSTFIELDS = array(
                'To' => $retData['to_email'],
                'Subject' => $retData['email_subject'],
                'MAilBody' => $retData['email_body'],
                'ReplyTo' => $generalSettings['store_email'],
                'ReplyToDisplay' => $generalSettings['store_name'],
                'MailFromDisplay' => $generalSettings['store_name']
            );

            $response = callApi(API_URL_ENDPOINT . '/webapi/Mail/SendMail', 'POST', $CURLOPT_POSTFIELDS);

            $et = "new_order_app_owner";
            $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_GetEmailTemplate(
                                            @BUID=:buid,
                                            @TeamplateType=:TemplateType)}");
            $statement->bindParam(':buid', $BUID, PDO::PARAM_INT);
            $statement->bindParam(':TemplateType', $et, PDO::PARAM_STR);
            $statement->execute();
            $emailTemplate = $statement->fetch(PDO::FETCH_ASSOC);

            $retData = array();

            $retData['email_subject'] = str_replace(array_keys($email_subject_keywords), $email_subject_keywords, $emailTemplate['email_subject']);

            $retData['email_body'] = str_replace(array_keys($email_body_keywords), $email_body_keywords, $emailTemplate['email_body']);

            $retData['to_email'] = $orders[0]['Email'];

            $CURLOPT_POSTFIELDS = array(
                'To' => $generalSettings['store_email'],
                'Subject' => $retData['email_subject'],
                'MAilBody' => $retData['email_body'],
                'ReplyTo' => $generalSettings['store_email'],
                'ReplyToDisplay' => $generalSettings['store_name'],
                'MailFromDisplay' => $generalSettings['store_name']
            );

            $response = callApi(API_URL_ENDPOINT . '/webapi/Mail/SendMail', 'POST', $CURLOPT_POSTFIELDS);
        }

        if ($Email_Type == 'statusChange') {

            $et = "statusChange";

            $pusherArr['PushTitle'] = 'Order status change';
            $pusherArr['PushMessage'] = 'Dear ' . $orders[0]['Billingname'] . ', Your order ' . $orders[0]['orderidentifier'] . ' status changed successfully to ' . $orders[0]['OrderStatus'];

            $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_GetEmailTemplate(
                                            @BUID=:buid,
                                            @TeamplateType=:TemplateType)}");
            $statement->bindParam(':buid', $BUID, PDO::PARAM_INT);
            $statement->bindParam(':TemplateType', $et, PDO::PARAM_STR);
            $statement->execute();
            $emailTemplate = $statement->fetch(PDO::FETCH_ASSOC);


            $retData = array();

            $retData['email_subject'] = str_replace(array_keys($email_subject_keywords), $email_subject_keywords, $emailTemplate['email_subject']);

            $retData['email_body'] = str_replace(array_keys($email_body_keywords), $email_body_keywords, $emailTemplate['email_body']);

            $retData['to_email'] = $orders[0]['Email'];
            //$retData['to_email'] = 'achyut.sinha@dreamztech.com';

            $CURLOPT_POSTFIELDS = array(
                'To' => $retData['to_email'],
                'Subject' => $retData['email_subject'],
                'MAilBody' => $retData['email_body'],
                'ReplyTo' => $generalSettings['store_email'],
                'ReplyToDisplay' => $generalSettings['store_name'],
                'MailFromDisplay' => $generalSettings['store_name']
            );
            //dd($CURLOPT_POSTFIELDS);
            //echo API_URL_ENDPOINT . '/webapi/Mail/SendMail'; exit;
            $response = callApi(API_URL_ENDPOINT . '/webapi/Mail/SendMail', 'POST', $CURLOPT_POSTFIELDS);
            //dd($response, true);
        }

        /* $buid=325;
          $cid=163; */
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_Get_ActionsList_Data(@buid=:buid,@userid=:userid)}");
        $statement->bindParam(':buid', $BUID, PDO::PARAM_INT);
        $statement->bindParam(':userid', $orders[0]['customer_id'], PDO::PARAM_INT);
        /* $statement->bindParam(':buid', $buid, PDO::PARAM_INT);
          $statement->bindParam(':userid', $cid, PDO::PARAM_INT); */
        $statement->execute();
        $uniqueids = $statement->fetch(PDO::FETCH_ASSOC);

        //dd($uniqueids, true);
        //$uniqueids = implode(",", $uniqueids);

        $search = array('##CUSTOMERNAME##', '##ORDERIDENTIFIER##', '&nbsp;');
        $replace = array($orders[0]['Billingname'], $orders[0]['orderidentifier'], '');
        $pusherArr['PushTitle'] = str_replace($search, $replace, $pusherArr['PushTitle']);
        $pusherArr['PushMessage'] = str_replace($search, $replace, $pusherArr['PushMessage']);
        try {
            $pusher = new Pusher\Pusher(PUSHERKEY, PUSHERSECRET, PUSHERAPPID, array('encrypted' => true));
        } catch (Exception $e) {
            //echo $e->getMessage();
        }
        foreach ($uniqueids as $val) {
            $result = [
                'error' => false,
                'msg' => '',
                'data' => $pusher->trigger($val, 'pushMessage', [
                    'title' => strip_tags($pusherArr['PushTitle']),
                    'description' => strip_tags($pusherArr['PushMessage'])
                ])
            ];
        }

        $duid = array($orders[0]['ordersummary']['bu_name']);

        $offlinepush = array(
            'title' => $pusherArr['PushTitle'],
            'body' => $pusherArr['PushMessage'],
            'BUID' => $duid,
            'memberid' => $orders[0]['customer_id'],
            'domain' => $orders[0]['ordersummary']['domain']
        );
        try {
            $offlinepushresponse = callApi(OFFLINE_PUSHER_API_ENDPOINT . '/webservices/index.php', 'POST', $offlinepush, ['action' => 'offlinepush'], false, false, false);
        } catch (Exception $e) {
            //echo $e->getMessage();
        }

        return $response;
    }

    public function json_decode_summary(&$array) {
        $array['ordersummary'] = json_decode($array['ordersummary'], true);
        $array['FormatedDateTime'] = date('l jS \of F Y h:i:s A', strtotime($array['datetime']));
        $array['datetime'] = date('Y-m-d H:i:s', strtotime($array['datetime']));
        $array['orderidentifier'] = "&nbsp;" . $array['orderidentifier'];
        if (!empty($array['CustomerUpdateDate'])) {
            $array['CustomerUpdateDate'] = date('Y-m-d H:i:s', strtotime($array['CustomerUpdateDate']));
        }
        if (!empty($array['CustomerCreationDate'])) {
            $array['CustomerCreationDate'] = date('Y-m-d H:i:s', strtotime($array['CustomerCreationDate']));
        }
        return $array;
    }



}
