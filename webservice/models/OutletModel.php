<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of IndustryModel
 *
 * @author dipankar
 */
require_once MODEL_PATH . DS . 'App.php';

class OutletModel extends AppModel {

    //put your code here
    protected $Table = 'industries';
    protected $BuMapTable = 'BU_INDUSTRY_map';

    public function __construct($callAuth = false) {
        parent::__construct();
        //$this->CheckAuthenticated();
    }

    public function getOutletsForFrontend($params) {
        if(!empty($params['BUID'])){
            $this->BUID = $params['BUID'];
        }else{
            $this->CheckAuthenticated();
        }
        if($this->BUID == null){
            throw new Exception("Sorry BUID is required");
        }
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_GetOutlatesForBU(@buid = :buid)}");
        $statement->bindParam(':buid', $this->BUID, PDO::PARAM_INT);
        $statement->execute();
        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }
    
    public function getOutletsByBusinessOutletId($params) {
        if(!empty($params['BUID'])){
            $this->BUID = $params['BUID'];
        }else{
            $this->CheckAuthenticated();
        }
        if($this->BUID == null){
            throw new Exception("Sorry required fields BUID, OutletId is required");
        }
        $statement = $this->PDO->prepare("{CALL PHP_ECOMMERCE_GetOutlatesByBU(@buid = :buid, @id=:id)}");
        $statement->bindParam(':buid', $this->BUID, PDO::PARAM_INT);
        if (!empty($params['id'])) {
            $statement->bindParam(':id', $params['id'], PDO::PARAM_INT);
        } else {
            $statement->bindParam(':id', NULL, PDO::PARAM_INT);
        }
        $statement->execute();
        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }
    
    public function addOutlet($params) {
        if(!empty($params['BUID'])){
            $this->BUID = $params['BUID'];
        }else{
            $this->CheckAuthenticated();
        }
        if($this->BUID == null){
            throw new Exception("Sorry BUID is required");
        }   
        if (empty($params['name'])) {
            throw new Exception("Outlet name should not be blank!!");
        }
        $addStatement = $this->PDO->prepare("{CALL PHP_Ecommerce_Add_Outlate (@buid = :buid, @name = :name, @status = :status, @ID = :id, @OUTLATEID = :OUTLATEID)}");
        $addStatement->bindParam(':buid', $this->BUID, PDO::PARAM_INT);
        $addStatement->bindParam(':name', $params['name'], PDO::PARAM_STR);
        
        $params['status'] = (empty($params['status'])? 0: $params['status']);
        $addStatement->bindParam(':status', $params['status'], PDO::PARAM_INT);
        
        $params['id'] = (empty($params['id'])? NULL: $params['id']);
        $addStatement->bindParam(':id', $params['id'], PDO::PARAM_INT);
        
        $addStatement->bindParam(':OUTLATEID', $OutLetId, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000);        
        $addStatement->execute();
        $params['OUTLATEID'] = $OutLetId;
        return $params;        
    }

    public function deleteOutlet($params) {
        if(!empty($params['BUID'])){
            $this->BUID = $params['BUID'];
        }else{
            $this->CheckAuthenticated();
        }
        if($this->BUID == null){
            throw new Exception("Sorry BUID is required");
        }
        if (empty($params['id'])) {
            throw new Exception(__t("Sorry invalid outlet id"));
        }
        $statement= $this->PDO->prepare("{ CALL PHP_Ecommerce_DeleteOutlate (@id=:id, @buid=:buid)}");
        $statement->bindParam(':id', $params['id'], PDO::PARAM_INT);
        $statement->bindParam(':buid', $this->BUID, PDO::PARAM_INT);
        if (!$statement->execute()) {
            throw new Exception(__t("Sorry can not get outlet"));
        }
        return true;
    }

}
