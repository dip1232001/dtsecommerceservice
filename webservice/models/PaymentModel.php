<?php
require_once MODEL_PATH . DS . 'CartModel.php';
require_once MODEL_PATH . DS . 'GeneralSettingsModel.php';
class PaymentModel extends CartModel
{

    public function __construct()
    {
        parent::__construct(false);
    }

    public function __processPaypal()
    {
        
        // STEP 1: read POST data
// Reading POSTed data directly from $_POST causes serialization issues with array data in the POST.
// Instead, read raw POST data from the input stream.
        $raw_post_data = file_get_contents('php://input');
        $raw_post_array = explode('&', $raw_post_data);
        //file_put_contents(MODEL_PATH . DS . 'test.txt', print_r($raw_post_array, true), FILE_APPEND);
        $myPost = array();
        foreach ($raw_post_array as $keyval) {
            $keyval = explode('=', $keyval);
            if (count($keyval) == 2) {
                $myPost[$keyval[0]] = urldecode($keyval[1]);
            }
        }
// read the IPN message sent from PayPal and prepend 'cmd=_notify-validate'
        $req = 'cmd=_notify-validate';
        if (function_exists('get_magic_quotes_gpc')) {
            $get_magic_quotes_exists = true;
        }
        foreach ($myPost as $key => $value) {
            if ($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
                $value = urlencode(stripslashes($value));
            }
            else {
                $value = urlencode($value);
            }
            $req .= "&$key=$value";
        }
        $ipnverificationcehck = 'https://ipnpb.sandbox.paypal.com/cgi-bin/webscr';
        if (PAYMENTMODE == 'LIVE') {
            $ipnverificationcehck = 'https://ipnpb.paypal.com/cgi-bin/webscr';
        }


        $ch = curl_init($ipnverificationcehck);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));
        if (! ($res = curl_exec($ch))) {
            curl_close($ch);
            exit;
        }
        curl_close($ch);
        //file_put_contents(MODEL_PATH . DS . 'test.txt', print_r($res, true), FILE_APPEND);
        if (strcmp($res, "VERIFIED") == 0) {
            $item_name = $_POST['item_name'];
            $item_number = $_POST['item_number'];
            $payment_status = $_POST['payment_status'];
            $payment_amount = $_POST['mc_gross'];
            $payment_currency = $_POST['mc_currency'];
            $txn_id = $_POST['txn_id'];
            $custom = explode("::", urldecode($_POST['custom']));
            $receiver_email = $_POST['receiver_email'];
            $payer_email = $_POST['payer_email'];

            if (count($custom) < 3 || count($custom) > 3) {
                throw new Exception(__t("Sorry invalid ipn order"));
            }
            //file_put_contents(MODEL_PATH . DS . 'test.txt', print_r($_POST, true), FILE_APPEND);
            //file_put_contents(MODEL_PATH . DS . 'test.txt', print_r($custom, true), FILE_APPEND);
            $statement = $this->PDO->prepare("{ CALL PHP_Ecommere_PaymentDetailsLog(
                                        @ORDERID=:order_id,
                                        @BUID=:buid,
                                        @PaymentType=:paymentype,
                                        @PaymentDate=:paymentdate,
                                        @PaymentStatus=:PaymentStatus,
                                        @transaction_id=:transaction_id,
                                        @raw_payment_data=:raw_payment_data,
                                        @amount=:amount
                                        ) }");
            $statement->bindParam(':order_id', $custom[2], PDO::PARAM_INT);
            $statement->bindParam(':buid', $this->BUID = $custom[0], PDO::PARAM_INT);
            $statement->bindParam(':paymentype', $paytype = 'Paypal', PDO::PARAM_STR);
            $statement->bindParam(':paymentdate', $paydate = date('Y-m-d H:i:s'), PDO::PARAM_STR);
            $statement->bindParam(':PaymentStatus', $payment_status, PDO::PARAM_STR);
            $statement->bindParam(':transaction_id', $txn_id, PDO::PARAM_STR);
            $statement->bindParam(':raw_payment_data', $raw_payment_data = json_encode($_POST), PDO::PARAM_STR);
            $statement->bindParam(':amount', $payment_amount, PDO::PARAM_STR);

            if ($statement->execute()) {
                $getCartItems = $this->getAllorders(0, 1, ['id' => (int)$custom[2]])['orders'][0];
                //file_put_contents(MODEL_PATH . DS . 'test.txt', print_r('aded', true), FILE_APPEND);
                $dbpaymentStatus = 1;
                switch (strtolower($payment_status)) {
                    case 'completed' :
                        $dbpaymentStatus = 2;
                        break;
                    case 'denied' :
                        $dbpaymentStatus = 4;
                        break;
                    case 'failed' :
                        $dbpaymentStatus = 4;
                        break;
                    case 'refunded' :
                        $dbpaymentStatus = 4;
                        break;
                    case 'reversed' :
                        $dbpaymentStatus = 4;
                        break;
                    case 'voided' :
                        $dbpaymentStatus = 4;
                        break;
                    default :
                        $dbpaymentStatus = 1;
                        break;
                }
               // file_put_contents(MODEL_PATH . DS . 'test.txt', print_r($getCartItems, true), FILE_APPEND);
                $orderupdated = $this->ChangePaymentStatus($custom[2], $dbpaymentStatus);
                if ($orderupdated && $dbpaymentStatus == 2 && $getCartItems['PaymentStatusID'] != 2) {
                    // make the stock update here tommorow
                    if ($this->ChangeorderStatus([$custom[2]], 2)) {
                    /*
                         * Update Product Stock
                         */
                      //  file_put_contents(MODEL_PATH . DS . 'test.txt', print_r($getCartItems['ordersummary']['cartitems'], true), FILE_APPEND);
                        if (!empty($getCartItems['ordersummary']['cartitems'])) {
                            foreach ($getCartItems['ordersummary']['cartitems'] as $cartitem) {
                                $this->updateProductstock([
                                    'product_id' => $cartitem['id'],
                                    'AppendStock' => 1,
                                    'stock' => -$cartitem['cartQuantity']
                                ]);
                            }
                        }
                    }
                }
            }
            else {
                throw new Exception(__t("could not update db: " . $res));
                //file_put_contents(MODEL_PATH . DS . 'test.txt', print_r('not aded', true), FILE_APPEND);











            }
        }
        elseif (strcmp($res, "INVALID") == 0) {
            // IPN invalid, log for manual investigation
            throw new Exception(__t("The response from IPN was: " . $res));
        }
    }

    public function ChangePaymentStatus($orderids, $dbpaymentStatus)
    {
        if (empty($orderids)) {
            throw new Exception(__t("Sorry order number required"));
        }
        if (empty($dbpaymentStatus)) {
            throw new Exception(__t("Sorry payment status can not be empty"));
        }
        if (!is_array($orderids)) {
            $orderids = [$orderids];
        }
        $pdostate = $this->PDO->prepare("{CALL PHP_Ecommerce_ChangepaymentStatus (@orderIDs=:order_id,@StatusID=:status)}");
        $pdostate->bindParam(':order_id', $orderids = implode(",", $orderids), PDO::PARAM_STR);
        $pdostate->bindParam(':status', $dbpaymentStatus, PDO::PARAM_INT);

        if (!$pdostate->execute()) {
            throw new Exception(__t("Sorry payment status not changed"));
        }
        return true;
    }



    public function createstripeToken($data)
    {
        $SettingsModel = new GeneralSettingsModel();
        $paymentDetails = $SettingsModel->getPaymentMethods([
            'id' => 3
        ]);
        $stripe = array(
            "secret_key" => $paymentDetails[0]['clientsecret'],
            "publishable_key" => $paymentDetails[0]['clientkey']
        );

        \Stripe\Stripe::setApiKey($stripe['secret_key']);
        $tokejson = \Stripe\Token::create(array(
            "card" => array(
                "number" => $data['card'],
                "exp_month" => $data['exp_month'],
                "exp_year" => $data['exp_year'],
                "cvc" => $data['cvc']
            )
        ));

        return $tokejson->id;
    }
    public function StripePayment($data, $description)
    {
        $SettingsModel = new GeneralSettingsModel();
        $paymentDetails = $SettingsModel->getPaymentMethods([
            'id' => 3
        ]);
        $stripe = array(
            "secret_key" => $paymentDetails[0]['clientsecret'],
            "publishable_key" => $paymentDetails[0]['clientkey']
        );

        \Stripe\Stripe::setApiKey($stripe['secret_key']);
        $token = $data['stripeToken'];

        $customer = \Stripe\Customer::create(array(
            'source' => $token,
            'email' => $data['email']
        ));

        $charge = \Stripe\Charge::create(array(
            'customer' => $customer->id,
            'amount' => ($data['amount'] * 100),
            'currency' => $data['currency'],
            'capture' => true,
            'description' => $description,
            'receipt_email' => $data['email']
        ));
        return true;
    }


}
