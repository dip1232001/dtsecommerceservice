<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CategoryModel
 *
 * @author dipankar
 */
require_once MODEL_PATH . DS . 'App.php';

class PreorderModel extends AppModel
{

    //put your code here

    public function __construct($callAuth = false)
    {
        parent::__construct();
    }

    public function addPreorder($data)
    {
        //dd($data, true);
        if (!empty($data['BUID'])) {
            $this->BUID = $data['BUID'];
        } else {
            $this->CheckAuthenticated();
        }
        if ($this->BUID == null) {
            throw new Exception("Sorry BUID is required");
        }

        $addStatement = $this->PDO->prepare("{CALL PHP_Ecommerce_AddPreOrderSettings (
            @buid=:buid,
           @monsh=:monsh,
           @monsm=:monsm,
           @moneh=:moneh,
           @monem=:monem,
           @ismonoffday=:ismonoffday,
           @tuesh=:tuesh,
           @tuesm=:tuesm,
           @tueeh=:tueeh,
           @tueem=:tueem,
           @istueoffday=:istueoffday,
           @wedsh=:wedsh,
           @wedsm=:wedsm,
           @wedeh=:wedeh,
           @wedem=:wedem,
           @iswedoffday=:iswedoffday,
           @thush=:thush,
           @thusm=:thusm,
           @thueh=:thueh,
           @thuem=:thuem,
           @isthuoffday=:isthuoffday,
           @frish=:frish,
           @frism=:frism,
           @frieh=:frieh,
           @friem=:friem,
           @isfrioffday=:isfrioffday,
           @satsh=:satsh,
           @satsm=:satsm,
           @sateh=:sateh,
           @satem=:satem,
           @issatoffday=:issatoffday,
           @sunsh=:sunsh,
           @sunsm=:sunsm,
           @suneh=:suneh,
           @sunem=:sunem,
           @issunfoffday=:issunfoffday,
           @defaulyreservationlength=:defaulyreservationlength,
           @reservexhrsearlier=:reservexhrsearlier,
		   @ID=:ID
            )}");

        $addStatement->bindParam(':buid', $this->BUID, PDO::PARAM_INT);
        $addStatement->bindParam(':monsh', $data['monsh'], PDO::PARAM_STR);
        $addStatement->bindParam(':monsm', $data['monsm'], PDO::PARAM_STR);
        $addStatement->bindParam(':moneh', $data['moneh'], PDO::PARAM_STR);
        $addStatement->bindParam(':monem', $data['monem'], PDO::PARAM_STR);
        $addStatement->bindParam(':ismonoffday', $data['ismonoffday'], PDO::PARAM_INT);
        $addStatement->bindParam(':tuesh', $data['tuesh'], PDO::PARAM_STR);
        $addStatement->bindParam(':tuesm', $data['tuesm'], PDO::PARAM_STR);
        $addStatement->bindParam(':tueeh', $data['tueeh'], PDO::PARAM_STR);
        $addStatement->bindParam(':tueem', $data['tueem'], PDO::PARAM_STR);
        $addStatement->bindParam(':istueoffday', $data['istueoffday'], PDO::PARAM_INT);
        $addStatement->bindParam(':wedsh', $data['wedsh'], PDO::PARAM_STR);
        $addStatement->bindParam(':wedsm', $data['wedsm'], PDO::PARAM_STR);
        $addStatement->bindParam(':wedeh', $data['wedeh'], PDO::PARAM_STR);
        $addStatement->bindParam(':wedem', $data['wedem'], PDO::PARAM_STR);
        $addStatement->bindParam(':iswedoffday', $data['iswedoffday'], PDO::PARAM_INT);
        $addStatement->bindParam(':thush', $data['thush'], PDO::PARAM_STR);
        $addStatement->bindParam(':thusm', $data['thusm'], PDO::PARAM_STR);
        $addStatement->bindParam(':thueh', $data['thueh'], PDO::PARAM_STR);
        $addStatement->bindParam(':thuem', $data['thuem'], PDO::PARAM_STR);
        $addStatement->bindParam(':isthuoffday', $data['isthuoffday'], PDO::PARAM_INT);
        $addStatement->bindParam(':frish', $data['frish'], PDO::PARAM_STR);
        $addStatement->bindParam(':frism', $data['frism'], PDO::PARAM_STR);
        $addStatement->bindParam(':frieh', $data['frieh'], PDO::PARAM_STR);
        $addStatement->bindParam(':friem', $data['friem'], PDO::PARAM_STR);
        $addStatement->bindParam(':isfrioffday', $data['isfrioffday'], PDO::PARAM_INT);
        $addStatement->bindParam(':satsh', $data['satsh'], PDO::PARAM_STR);
        $addStatement->bindParam(':satsm', $data['satsm'], PDO::PARAM_STR);
        $addStatement->bindParam(':sateh', $data['sateh'], PDO::PARAM_STR);
        $addStatement->bindParam(':satem', $data['satem'], PDO::PARAM_STR);
        $addStatement->bindParam(':issatoffday', $data['issatoffday'], PDO::PARAM_INT);
        $addStatement->bindParam(':sunsh', $data['sunsh'], PDO::PARAM_STR);
        $addStatement->bindParam(':sunsm', $data['sunsm'], PDO::PARAM_STR);
        $addStatement->bindParam(':suneh', $data['suneh'], PDO::PARAM_STR);
        $addStatement->bindParam(':sunem', $data['sunem'], PDO::PARAM_STR);
        $addStatement->bindParam(':issunfoffday', $data['issunfoffday'], PDO::PARAM_INT);
        $addStatement->bindParam(':defaulyreservationlength', $data['defaulyreservationlength'], PDO::PARAM_INT);
        $addStatement->bindParam(':reservexhrsearlier', $data['reservexhrsearlier'], PDO::PARAM_INT);
        $addStatement->bindParam(':ID', $data['id'], PDO::PARAM_INT);

        //$addStatement->bindParam(':categoryname', $data['name'], PDO::PARAM_STR);
        //$addStatement->bindParam(':buid', $this->BUID, PDO::PARAM_INT);

        $addStatement->execute();
        return [
            'id' => $data['id'],
            'msg' => "Data saved successfully.",
        ];
    }

    public function getPreorder($data)
    {
        //dd($data,true);
        if (!empty($data['BUID'])) {
            $this->BUID = $data['BUID'];
        } else {
            $this->CheckAuthenticated();
        }
        if ($this->BUID == null) {
            throw new Exception("Sorry BUID is required");
        }

        $statement = $this->PDO->prepare("{ CALL PHP_Ecommerce_GetPreOrderSettings(@buid=:buid)}");
        $statement->bindParam(':buid', $this->BUID);

        $statement->execute();
        $rows = $statement->fetchAll(PDO::FETCH_ASSOC);
        return [
            'PreOrderSettings' => $rows,
        ];
    }

    public function addPreorderTable($data)
    {
        //dd($data, true);
        if (!empty($data['BUID'])) {
            $this->BUID = $data['BUID'];
        } else {
            $this->CheckAuthenticated();
        }
        if ($this->BUID == null) {
            throw new Exception("Sorry BUID is required");
        }

        $addStatement = $this->PDO->prepare("{CALL PHP_Ecommerce_AddPreOrderTable (
            @buid=:buid,
           @name=:name,
           @capacity=:capacity,
           @status=:status,
           @ID=:ID,
           @TABLEID=:TABLEID
            )}");

        $addStatement->bindParam(':buid', $this->BUID, PDO::PARAM_INT);
        $addStatement->bindParam(':name', $data['name'], PDO::PARAM_STR);
        $addStatement->bindParam(':capacity', $data['capacity'], PDO::PARAM_INT);
        $addStatement->bindParam(':status', $data['status'], PDO::PARAM_INT);
        $addStatement->bindParam(':ID', $data['id'], PDO::PARAM_INT);
        $addStatement->bindParam(':TABLEID', $data['id'], PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000);

        //$addStatement->bindParam(':categoryname', $data['name'], PDO::PARAM_STR);
        //$addStatement->bindParam(':buid', $this->BUID, PDO::PARAM_INT);

        $addStatement->execute();
        return [
            'id' => $data['id'],
            'msg' => "Data saved successfully.",
        ];
    }

    public function getPreorderTable($data)
    {
        //dd($data,true);
        if (!empty($data['BUID'])) {
            $this->BUID = $data['BUID'];
        } else {
            $this->CheckAuthenticated();
        }
        if ($this->BUID == null) {
            throw new Exception("Sorry BUID is required");
        }

        $statement = $this->PDO->prepare("{ CALL PHP_Ecommerce_GetPreOrderTables(@buid=:buid)}");
        $statement->bindParam(':buid', $this->BUID);

        $statement->execute();
        $rows = $statement->fetchAll(PDO::FETCH_ASSOC);
        return [
            'PreOrderTables' => $rows,
        ];
    }

    public function GetPreOrderSettingsByDate($data)
    {
        //dd($data,true);
        if (!empty($data['BUID'])) {
            $this->BUID = $data['BUID'];
        } else {
            $this->CheckAuthenticated();
        }
        if ($this->BUID == null) {
            throw new Exception("Sorry BUID is required");
        }

        if (empty($data['PreOrderdate'])) {
            throw new Exception("Sorry Pre Order date is required");
        }

        $statement = $this->PDO->prepare("{ CALL PHP_Ecommerce_GetPreOrderSettings(@buid=:buid)}");
        $statement->bindParam(':buid', $this->BUID);

        $statement->execute();
        $rows = $statement->fetchAll(PDO::FETCH_ASSOC);

        $statement = $this->PDO->prepare("{ CALL PHP_Ecommerce_GetPreOrderTablesMaxMinCapacity(@buid=:buid)}");
        $statement->bindParam(':buid', $this->BUID);

        $statement->execute();
        $MaxMinCapacity = $statement->fetchAll(PDO::FETCH_ASSOC);

        $timestamp = strtotime($data['PreOrderdate']);
        $day = date('w', $timestamp);
        $retArr = [];
        switch ($day) {
            case 0:
                $retArr["startTime"] = date('H:i', strtotime($rows[0]["sunsh"] . ":" . $rows[0]["sunsm"] . ":00"));
                $retArr["endTime"] = date('H:i', strtotime($rows[0]["suneh"] . ":" . $rows[0]["sunem"] . ":00"));
                $retArr["isOffDay"] = $rows[0]["issunfoffday"];
                break;
            case 1:
                $retArr["startTime"] = date('H:i', strtotime($rows[0]["monsh"] . ":" . $rows[0]["monsm"] . ":00"));
                $retArr["endTime"] = date('H:i', strtotime($rows[0]["moneh"] . ":" . $rows[0]["monem"] . ":00"));
                $retArr["isOffDay"] = $rows[0]["ismonoffday"];
                break;
            case 2:
                $retArr["startTime"] = date('H:i', strtotime($rows[0]["tuesh"] . ":" . $rows[0]["tuesm"] . ":00"));
                $retArr["endTime"] = date('H:i', strtotime($rows[0]["tueeh"] . ":" . $rows[0]["tueem"] . ":00"));
                $retArr["isOffDay"] = $rows[0]["istueoffday"];
                break;
            case 3:
                $retArr["startTime"] = date('H:i', strtotime($rows[0]["wedsh"] . ":" . $rows[0]["wedsm"] . ":00"));
                $retArr["endTime"] = date('H:i', strtotime($rows[0]["wedeh"] . ":" . $rows[0]["wedem"] . ":00"));
                $retArr["isOffDay"] = $rows[0]["iswedoffday"];
                break;
            case 4:
                $retArr["startTime"] = date('H:i', strtotime($rows[0]["thush"] . ":" . $rows[0]["thusm"] . ":00"));
                $retArr["endTime"] = date('H:i', strtotime($rows[0]["thueh"] . ":" . $rows[0]["thuem"] . ":00"));
                $retArr["isOffDay"] = $rows[0]["isthuoffday"];
                break;
            case 5:
                $retArr["startTime"] = date('H:i', strtotime($rows[0]["frish"] . ":" . $rows[0]["frism"] . ":00"));
                $retArr["endTime"] = date('H:i', strtotime($rows[0]["frieh"] . ":" . $rows[0]["friem"] . ":00"));
                $retArr["isOffDay"] = $rows[0]["isfrioffday"];
                break;
            case 6:
                $retArr["startTime"] = date('H:i', strtotime($rows[0]["satsh"] . ":" . $rows[0]["satsm"] . ":00"));
                $retArr["endTime"] = date('H:i', strtotime($rows[0]["sateh"] . ":" . $rows[0]["satem"] . ":00"));
                $retArr["isOffDay"] = $rows[0]["issatoffday"];
                break;
            default:
                $retArr["startTime"] = date('H:i', strtotime($rows[0]["monsh"] . ":" . $rows[0]["monsm"] . ":00"));
                $retArr["endTime"] = date('H:i', strtotime($rows[0]["moneh"] . ":" . $rows[0]["monem"] . ":00"));
                $retArr["isOffDay"] = $rows[0]["ismonoffday"];
                break;
        }
        if($retArr["isOffDay"] == 1){
            $retArr["startTime"] = "";
            $retArr["endTime"] = "";
        }
        $retArr["defaulyreservationlength"] = $rows[0]["defaulyreservationlength"];
        $retArr["reservexhrsearlier"] = $rows[0]["reservexhrsearlier"];
        $retArr["MAXcapacity"] = $MaxMinCapacity[0]["MAXcapacity"];
        $retArr["MINcapacity"] = $MaxMinCapacity[0]["MINcapacity"];

        return [
            'PreOrderSettings' => $retArr,
        ];
    }

    public function GetPreOrderTableListByDateAndTime($data)
    {
        //dd($data);
        if (!empty($data['BUID'])) {
            $this->BUID = $data['BUID'];
        } else {
            $this->CheckAuthenticated();
        }
        if ($this->BUID == null) {
            throw new Exception("Sorry BUID is required");
        }

        if (empty($data['PreOrderdate'])) {
            throw new Exception("Sorry Pre Order date is required");
        }

        if (empty($data['PreOrdertime'])) {
            throw new Exception("Sorry Pre Order time is required");
        }

        if (empty($data['noofguest'])) {
            throw new Exception("Sorry noofguest is required");
        }

        $statement = $this->PDO->prepare("{ CALL PHP_Ecommerce_GetPreOrderTablesMaxMinCapacity(@buid=:buid)}");
        $statement->bindParam(':buid', $this->BUID);

        $statement->execute();
        $MaxMinCapacity = $statement->fetchAll(PDO::FETCH_ASSOC);

        $PreOrdertime = date("H:i:s", strtotime($data['PreOrdertime'] . ":00"));
        
        $noofguestP = $data['noofguest'] + 2;
        if($noofguestP < $MaxMinCapacity[0]["MINcapacity"]){
            $noofguestP = $MaxMinCapacity[0]["MINcapacity"];
        }

        $statement = $this->PDO->prepare("{ CALL PHP_Ecommerce_GetPreOrderTableListByDateAndTime(
            @BUID=:buid,
            @bookingdate=:bookingdate,
            @bookingtime=:bookingtime,
            @noofguest=:noofguest,
            @noofguestP=:noofguestP
            )}");
        $statement->bindParam(':buid', $this->BUID, PDO::PARAM_INT);
        $statement->bindParam(':bookingdate', $data['PreOrderdate'], PDO::PARAM_STR);
        $statement->bindParam(':bookingtime', $PreOrdertime, PDO::PARAM_STR);
        $statement->bindParam(':noofguest', $data['noofguest'], PDO::PARAM_INT);
        $statement->bindParam(':noofguestP', $noofguestP, PDO::PARAM_INT);

        $statement->execute();
        $rows = $statement->fetchAll(PDO::FETCH_ASSOC);

        return [
            'PreOrderTables' => $rows,
        ];
    }

    public function deletePreOrderTable($params)
    {
        if (!empty($params['BUID'])) {
            $this->BUID = $params['BUID'];
        } else {
            $this->CheckAuthenticated();
        }
        if ($this->BUID == null) {
            throw new Exception("Sorry BUID is required");
        }
        if (empty($params['id'])) {
            throw new Exception(__t("Sorry invalid table id"));
        }
        $statement = $this->PDO->prepare("{ CALL PHP_Ecommerce_deletePreOrderTable (@id=:id, @buid=:buid)}");
        $statement->bindParam(':id', $params['id'], PDO::PARAM_INT);
        $statement->bindParam(':buid', $this->BUID, PDO::PARAM_INT);
        if (!$statement->execute()) {
            throw new Exception(__t("Sorry can not get table"));
        }
        return true;
    }

    public function GetNewOrderCount($params)
    {
        if (!empty($params['BUID'])) {
            $this->BUID = $params['BUID'];
        } else {
            $this->CheckAuthenticated();
        }
        if ($this->BUID == null) {
            throw new Exception("Sorry BUID is required");
        }

        $statement = $this->PDO->prepare("{ CALL PHP_Ecommerce_GetNewOrderCountByBU(@buid=:buid)}");
        $statement->bindParam(':buid', $this->BUID, PDO::PARAM_INT);
        $statement->execute();

        $ret = [];
        do {
            $rows = $statement->fetchAll(PDO::FETCH_ASSOC);
            switch ($x) {
                case 0:
                    $ret['delivery'] = $rows[0];
                    break;
                case 1:
                    $ret['dinein'] = $rows[0];
                    break;
                case 2:
                    $ret['pickup'] = $rows[0];
                    break;
                case 3:
                    $ret['preorder'] = $rows[0];
                    break;
            }
            //$ret[] = $rows[0];
            $x++;
        } while ($statement->nextRowset());

        return $ret;
    }
}
