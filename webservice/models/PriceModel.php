<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PriceModel
 *
 * @author dipankar
 */
require_once MODEL_PATH . DS . 'ProductModel.php';

class PriceModel extends ProductModel {

    //put your code here
    public $Table = 'Price_Types';
    public $TableMapProduct = 'Product_Price_Map';

    public function __construct($callAuth = false) {
        parent::__construct();
        if ($callAuth) {
            $this->CheckAuthenticated();
        }
    }

    public function addPriceType($data) {
        if(!empty($data['BUID'])){
            $this->BUID = $data['BUID'];
        }else{
            $this->CheckAuthenticated(); 
        }
        if($this->BUID == null){
            throw new Exception("Sorry BUID is required");
        }
        if (empty($data['name'])) {
            throw new Exception("Sorry price type name needed");
        }
        $addPriceTypestatement = $this->PDO->prepare("INSERT INTO {$this->DbAlias}{$this->Table} (name) VALUES(:name)");
        $addPriceTypestatement->bindParam(':name', $data['name']);
        $addPriceTypestatement->execute();
        return [
            'name' => $data['name'],
            'id' => $this->PDO->lastInsertId()
        ];
    }

    public function mapPriceToProduct($productID, $PriceType, $amount, $units = null) {
        // add logic for surcharge etc later
        if (empty($productID)) {
            throw new Exception("Sorry product id is needed to map price");
        }
        if (empty($PriceType)) {
            throw new Exception("Sorry price type is needed to map price");
        }
        if (empty($amount)) {
            throw new Exception("Sorry amount is needed to map price");
        }
        $insertStatement = $this->PDO->prepare("INSERT INTO {$this->DbAlias}{$this->TableMapProduct} (product_id,price_type_id,price,units) "
                . "VALUES(:product_id,:price_type_id,:price,:units)");
        $insertStatement->bindParam(':product_id', $productID, PDO::PARAM_INT);
        $insertStatement->bindParam(':price_type_id', $PriceType, PDO::PARAM_INT);
        $insertStatement->bindParam(':price', $amount, PDO::PARAM_STR);
        if (empty($units)) {
            $insertStatement->bindParam(':units', $units, PDO::PARAM_INT);
        } else {
            $insertStatement->bindParam(':units', $units, PDO::PARAM_STR);
        }
        $insertStatement->execute();
        return [
            'id' => $this->PDO->lastInsertId()
        ];
    }

    public function getPrieTypes() {
        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_GetPriceTypes()}");
        $statement->execute();
        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

}
