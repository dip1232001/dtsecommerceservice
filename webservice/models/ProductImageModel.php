<?php

use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProductImageModel
 *
 * @author dipankar
 */
require_once MODEL_PATH . DS . 'ProductModel.php';

class ProductImageModel extends ProductModel {

    //put your code here

    public function __construct() {
        parent::__construct();
    }

    public function prePareImageXml($FILES,$lastDO = 0) {

        $max_size = file_upload_max_size();
        $client = new S3Client([
            'version' => 'latest',
            'region' => AWS_REGION,
            'credentials' => [
                'key' => AWS_ACCESS_KEY,
                'secret' => AWS_SECRET_KEY
            ]
        ]);
        $valid_formats = ["jpg", "png", "gif", "jpeg"];
        $FileXml = '';
        foreach ($FILES['Images']['tmp_name'] as $key => $value) {
            if ($FILES['Images']['error'][$key] == 0) {
                $name = $FILES['Images']['name'][$key];
                $size = $FILES['Images']['size'][$key];
                $tmp = $FILES['Images']['tmp_name'][$key];
                $type = $FILES['Images']['type'][$key];

                if ($size > $max_size) {
                    throw new Exception(__t("Sorry image " . $name . " size is bigger than accepted size"));
                }
                $ext = strtolower(getExtension($name));
                if (!in_array($ext, $valid_formats)) {
                    throw new Exception(__t("Sorry image " . $name . " is not valid ,we accept " . implode(",", $valid_formats)));
                }
                $image_name_actual = time() ."-".rand(10,100). "." . $ext;
                try {
                    $Result = $client->putObject(array(
                        'Bucket' => AWS_BUCKET_NAME,
                        'Key' => 'BU' . $this->BUID . '/' . $image_name_actual,
                        'SourceFile' => $tmp,
                        'StorageClass' => 'REDUCED_REDUNDANCY',
                        'ACL' => 'public-read',
                        'ContentType' => $type,
                        'Expires' => date('Y-m-d H:i:s', strtotime("2 years"))
                    ));
                } catch (S3Exception $exc) {
                    throw new Exception("Aws S3 Error : " . $exc->getAwsErrorMessage());
                }

                $FileXml .= '<images>';
                $FileXml .= '<name><![CDATA[' . strip_tags(trim($name)) . ']]></name>';
                $FileXml .= '<imagePath><![CDATA[' . $Result['ObjectURL'] . ']]></imagePath>';
                $FileXml .= '<default_image><![CDATA[' . ( ($key == 0) ? 1 : 0 ) . ']]></default_image>';
                $FileXml .= '<dispalyorder>' . ($lastDO + $key + 1) . '</dispalyorder>';
                $FileXml .= '</images>';
            }
        }
        if (strlen(trim($FileXml)) > 0) {
            return '<data>' . $FileXml . '</data>';
        }
        return false;
    }

    public function prepareImageFromLink($imageurls) {
        $FileXml = '';
        foreach ($imageurls as $key => $value) {
            $FileXml .= '<images>';
            $FileXml .= '<name>' . strip_tags(trim($value['name'])) . '</name>';
            $FileXml .= '<imagePath>' . strip_tags(trim($value['imagePath'])) . '</imagePath>';
            $FileXml .= '<default_image>' . ( ($key == 0 ) ? 1 : 0 ) . '</default_image>';
            $FileXml .= '</images>';
        }
        if (strlen(trim($FileXml)) > 0) {
            return '<data>' . $FileXml . '</data>';
        }
        return false;
    }

    public function prepareVideoXML($videos) {
        $FileXml = '';
        foreach ($videos as $key => $value) {
            $FileXml .= '<videos>';
            $FileXml .= '<name>' . strip_tags(trim($value['name'])) . '</name>';
            $FileXml .= '<videoURL>' . base64_encode(trim($value['videoURL'])) . '</videoURL>';
            $FileXml .= '<default_video>' . ( ($key == 0 ) ? 1 : 0 ) . '</default_video>';
            $FileXml .= '</videos>';
        }
        if (strlen(trim($FileXml)) > 0) {
            return '<data>' . $FileXml . '</data>';
        }
        return false;
    }

    public function prepareVideoXMLBU($videos) {
        $FileXml = '';
        foreach ($videos as $key => $value) {
            $FileXml .= '<videos>';
            $FileXml .= '<name>' . strip_tags(trim($value['name'])) . '</name>';
            $FileXml .= '<videoURL>' . base64_encode(trim($value['videoURL'])) . '</videoURL>';
            $FileXml .= '<default_video>' . ( ($key == 0 ) ? 1 : 0 ) . '</default_video>';
            $FileXml .= '</videos>';
        }
        if (strlen(trim($FileXml)) > 0) {
            return '<data>' . $FileXml . '</data>';
        }
        return false;
    }

    public function prepareDocumentXMLBU($Documents) {
        $FileXml = '';
        foreach ($Documents as $key => $value) {
            $FileXml .= '<documents>';
            $FileXml .= '<name>' . strip_tags(trim($value['name'])) . '</name>';
            $FileXml .= '<docURL>' . strip_tags(trim($value['docURL'])) . '</docURL>';
            $FileXml .= '<default_doc>' . ( ($key == 0 ) ? 1 : 0 ) . '</default_doc>';
            $FileXml .= '</documents>';
        }
        if (strlen(trim($FileXml)) > 0) {
            return '<data>' . $FileXml . '</data>';
        }
        return false;
    }

    public function prepareDocumentXML($FILESD) {

        //$max_size = file_upload_max_size();
        $client = new S3Client([
            'version' => 'latest',
            'region' => AWS_REGION,
            'credentials' => [
                'key' => AWS_ACCESS_KEY,
                'secret' => AWS_SECRET_KEY
            ]
        ]);
        $FileXmlD = '';
        foreach ($FILESD['PRODUCTDOCUMENTS']['tmp_name'] as $key => $value) {
            if ($FILESD['PRODUCTDOCUMENTS']['error'][$key] == 0) {
                $name = $FILESD['PRODUCTDOCUMENTS']['name'][$key];
                $size = $FILESD['PRODUCTDOCUMENTS']['size'][$key];
                $tmp = $FILESD['PRODUCTDOCUMENTS']['tmp_name'][$key];
                $type = $FILESD['PRODUCTDOCUMENTS']['type'][$key];

                $ext = strtolower(getExtension($name));
                
                $image_name_actual = time() . "." . $ext;
                try {
                    $Result = $client->putObject(array(
                        'Bucket' => AWS_BUCKET_NAME,
                        'Key' => 'BU' . $this->BUID . '/' . $image_name_actual,
                        'SourceFile' => $tmp,
                        'StorageClass' => 'REDUCED_REDUNDANCY',
                        'ACL' => 'public-read',
                        'ContentType' => $type,
                        'Expires' => date('Y-m-d H:i:s', strtotime("2 years"))
                    ));
                    $FileXmlD .= '<documents>';
                    $FileXmlD .= '<name><![CDATA[' . strip_tags(trim($name)) . ']]></name>';
                    $FileXmlD .= '<docURL><![CDATA[' . $Result['ObjectURL'] . ']]></docURL>';
                    $FileXmlD .= '<default_doc><![CDATA[' . ( ($key == 0) ? 1 : 0 ) . ']]></default_doc>';
                    $FileXmlD .= '</documents>';
                } catch (S3Exception $exc) {
                    throw new Exception("Aws S3 Error : " . $exc->getAwsErrorMessage());
                }
            }
        }
        if (strlen(trim($FileXmlD)) > 0) {
            return '<data>' . $FileXmlD . '</data>';
        }
        return false;
    }

    public function prepareARTagXML($tag) {
        $tags = explode(',', $tag);
        $FileXml = '';
        foreach ($tags as $key => $value) {
            $FileXml .= '<artags>';
            $FileXml .= '<name>' . strip_tags(trim($value)) . '</name>';
            $FileXml .= '</artags>';
        }
        if (strlen(trim($FileXml)) > 0) {
            return '<data>' . $FileXml . '</data>';
        }
        return false;
    }

    public function prepareImageOrderXML($tag) {
        $FileXml = '';
        foreach ($tag as $key => $value) {
            $FileXml .= '<images>';
            $FileXml .= '<imageid>' . $value["id"] . '</imageid>';
            $FileXml .= '<displayorder>' . $value["displayorder"] . '</displayorder>';
            $FileXml .= '</images>';
        }
        if (strlen(trim($FileXml)) > 0) {
            return '<data>' . $FileXml . '</data>';
        }
        return false;
    }

    public function prepareCategotyXML($categories) {
        $FileXml = '';
        foreach ($categories as $key => $value) {
            if(!empty($value['status'])){
                $FileXml .= '<categories>';
                $FileXml .= '<categoryid>' . strip_tags(trim($value['category_id'])) . '</categoryid>';
                $FileXml .= '<displayorder>' . strip_tags(trim($value['productorder'])) . '</displayorder>';
                $FileXml .= '</categories>';
            }
        }
        if (strlen(trim($FileXml)) > 0) {
            return '<data>' . $FileXml . '</data>';
        }
        return false;
    }

    public function prepareCategotyXMLCSVUPLOAD($categories) {
        $FileXml = '';
        foreach ($categories as $key => $value) {
            $FileXml .= '<categories>';
            $FileXml .= '<categoryid>' . trim($value) . '</categoryid>';
            $FileXml .= '<displayorder>1</displayorder>';
            $FileXml .= '</categories>';
        }
        if (strlen(trim($FileXml)) > 0) {
            return '<data>' . $FileXml . '</data>';
        }
        return false;
    }

    public function prePareImageXmlCSVUPLOAD($data) {
        $FileXml = '';
        foreach ($data as $key => $value) {
            $value1 = explode("?",$value);
            $FileXml .= '<images>';
            $FileXml .= '<name>Image-'. ($key + 1) . '</name>';
            $FileXml .= '<imagePath>' . trim($value1[0]) . '</imagePath>';
            $FileXml .= '<default_image><![CDATA[' . ( ($key == 0) ? 1 : 0 ) . ']]></default_image>';
            $FileXml .= '<dispalyorder>' . ($key + 1) . '</dispalyorder>';
            $FileXml .= '</images>';
        }
        if (strlen(trim($FileXml)) > 0) {
            return '<data>' . $FileXml . '</data>';
        }
        return false;
    }

    public function prepareVideoXMLBUCSVUPLOAD($videos) {
        $FileXml = '';
        foreach ($videos as $key => $value) {
            $FileXml .= '<videos>';
            $FileXml .= '<name>Video-'. ($key + 1) . '</name>';
            $FileXml .= '<videoURL>' . base64_encode(trim($value)) . '</videoURL>';
            $FileXml .= '<default_video>' . ( ($key == 0 ) ? 1 : 0 ) . '</default_video>';
            $FileXml .= '</videos>';
        }
        if (strlen(trim($FileXml)) > 0) {
            return '<data>' . $FileXml . '</data>';
        }
        return false;
    }

    public function prepareDocumentXMLBUCSVUPLOAD($Documents) {
        $FileXml = '';
        foreach ($Documents as $key => $value) {
            $FileXml .= '<documents>';
            $FileXml .= '<name>Doc-'. ($key + 1) . '</name>';
            $FileXml .= '<docURL>' . strip_tags(trim($value)) . '</docURL>';
            $FileXml .= '<default_doc>' . ( ($key == 0 ) ? 1 : 0 ) . '</default_doc>';
            $FileXml .= '</documents>';
        }
        if (strlen(trim($FileXml)) > 0) {
            return '<data>' . $FileXml . '</data>';
        }
        return false;
    }

    public function prepareCategotyProductOrderXML($categories) {
        $FileXml = '';
        foreach ($categories as $key => $value) {
            $FileXml .= '<categories>';
            $FileXml .= '<categoryMapid>' . strip_tags(trim($value['id'])) . '</categoryMapid>';
            $FileXml .= '<displayorder>' . strip_tags(trim($value['productorder'])) . '</displayorder>';
            $FileXml .= '</categories>';
        }
        if (strlen(trim($FileXml)) > 0) {
            return '<data>' . $FileXml . '</data>';
        }
        return false;
    }

}
