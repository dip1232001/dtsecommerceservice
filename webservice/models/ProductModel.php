<?php

use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProductModel
 *
 * @author dipankar
 */
require_once MODEL_PATH . DS . 'App.php';
require_once MODEL_PATH . DS . 'CategoryModel.php';
require_once MODEL_PATH . DS . 'PriceModel.php';
require_once MODEL_PATH . DS . 'ProductStockModel.php';
require_once MODEL_PATH . DS . 'VarientModel.php';
require_once MODEL_PATH . DS . 'ProductImageModel.php';

class ProductModel extends AppModel {

    //put your code here
    public $Table = 'Products';

    public function __construct($callAuth = false) {
        parent::__construct();
        if ($callAuth) {
            $this->CheckAuthenticated();
        }
    }

    public function addProduct($data, $Files = []) {
        //$this->__validate($data);
        //dd($data, true);
        if(!empty($data['BUID'])){
            $this->BUID = $data['BUID'];
        }else{
            $this->CheckAuthenticated();
        }
        if($this->BUID == null){
            throw new Exception("Sorry BUID is required");
        }
        if (empty($data['Name'])) {
            throw new Exception("Sorry name is required");
        }
        if (empty($data['stock'])) {
            throw new Exception("Sorry quantity can not be 0 or empty");
        }
        $ProductImageModel = new ProductImageModel();
        $inserProductStatement = $this->PDO->prepare("{CALL PHP_Ecommerce_AddProducts ("
                . "@Name=:Name,"
                . "@description=:description,"
                . "@sku=:sku,"
                . "@category_id=:category_id,"
                . "@PriceTypeID=:PriceTypeID,"
                . "@Amount=:Amount,"
                . "@stock=:stock,"
                . "@buid=:buid,"
                . "@slug=:slug,"
                . "@brand_id=:brand_id,"
                . "@supplier_id=:supplier_id,"
                . "@supplier_code=:supplier_code,"
                . "@sales_account_code=:sales_account_code,"
                . "@purchase_account_code=:purchase_account_code,"
                . "@units=:units,"
                . "@VARIENTS=:VARIENTS,"
                . "@PRODUCTIMAGES=:PRODUCTIMAGES,"
                . "@PRODUCTVIDEOS=:PRODUCTVIDEOS,"
                . "@PRODUCTDOCUMENTS=:PRODUCTDOCUMENTS,"
                . "@PRODUCTARTAGS=:PRODUCTARTAGS," 
                . "@ID=:ID,"
                . "@ISDISBALED=:IsDisbaled,"
                . "@ISFEATURED=:IsFeatured,"
                . "@VIDEOURL=:videoUrl,"
                . "@is_virtual=:is_virtual,"
                . "@PRODUCTID=:PRODUCTID"
                . ")}");

        $inserProductStatement->bindParam(':Name', $data['Name'], PDO::PARAM_STR);
        $inserProductStatement->bindParam(':description', strip_tags( $data['description'],'<br><hr><blockquote><u><i><a><p><div><table><td><tr><code><pre><ul><ol><li><span><article><h1><h2><h3><h4><h5><h6><title><abbr><img><b><strong>'), PDO::PARAM_STR);
        $inserProductStatement->bindParam(':sku', $data['sku'], PDO::PARAM_STR);
        
        if (!empty($data['CATEGORIES'])) {
            $ProductCategories = $ProductImageModel->prepareCategotyXML($data["CATEGORIES"]);
            if (!empty($ProductCategories)) {
                $inserProductStatement->bindParam(':category_id', $ProductCategories, PDO::PARAM_STR);
            } else {
                $inserProductStatement->bindParam(':category_id', $ProductCategories = null, PDO::PARAM_INT);
            }
        }else{
            $inserProductStatement->bindParam(':category_id', $PRODUCTVIDEOS = null, PDO::PARAM_INT);
        }
        
        //$inserProductStatement->bindParam(':category_id', $data['category_id'], PDO::PARAM_INT);
        $inserProductStatement->bindParam(':PriceTypeID', $data['PriceTypeID'], PDO::PARAM_INT);
        $inserProductStatement->bindParam(':Amount', $data['Amount'], PDO::PARAM_STR);
        $inserProductStatement->bindParam(':stock', $data['stock'], PDO::PARAM_STR);
        $inserProductStatement->bindParam(':buid', $this->BUID, PDO::PARAM_INT);
        $inserProductStatement->bindParam(':slug', $data['slug'] = null, PDO::PARAM_STR);
        $inserProductStatement->bindParam(':brand_id', $data['brand_id'] = null, PDO::PARAM_INT);
        $inserProductStatement->bindParam(':supplier_id', $data['supplier_id'] = null, PDO::PARAM_INT);
        $inserProductStatement->bindParam(':supplier_code', $data['supplier_code'] = null, PDO::PARAM_INT);
        $inserProductStatement->bindParam(':sales_account_code', $data['sales_account_code'] = null, PDO::PARAM_INT);
        $inserProductStatement->bindParam(':purchase_account_code', $data['purchase_account_code'] = null, PDO::PARAM_INT);
        if (!empty($data['unit'])) {
            $inserProductStatement->bindParam(':units', $data['unit'], PDO::PARAM_STR);
        } else {
            $inserProductStatement->bindParam(':units', $data['unit'] = null, PDO::PARAM_INT);
        }
        $VarientModel = new VarientModel();
        $VarientModel->PrepareVarientXmlFromArray($data['Varient']);
        //dd($data['Varient']['XML'],true);
        if (!empty($data['Varient']['XML'])) {
            $inserProductStatement->bindParam(':VARIENTS', $data['Varient']['XML'], PDO::PARAM_STR);
        } else {
            $inserProductStatement->bindParam(':VARIENTS', $data['Varient'] = null, PDO::PARAM_INT);
        }
        

        $maxDO = 0;
        if(!empty($data['image_url'])){
            foreach ($data['image_url'] as $key => $value) {
                if($value["displayorder"] > $maxDO){
                    $maxDO = $value["displayorder"];
                }                
            }
        }

        if (!empty($Files['Images'])) {            
            $ProductImages = $ProductImageModel->prePareImageXml($Files, $maxDO);
            if (!empty($ProductImages)) {
                $inserProductStatement->bindParam(':PRODUCTIMAGES', $ProductImages, PDO::PARAM_STR);
            } else {
                $inserProductStatement->bindParam(':PRODUCTIMAGES', $ProductImages = null, PDO::PARAM_INT);
            }
        } /*elseif (!empty($data['image_url'])) {
            //$ProductImageModel = new ProductImageModel();
            $ProductImages = $ProductImageModel->prepareImageFromLink($data['image_url']);
            if (!empty($ProductImages)) {
                $inserProductStatement->bindParam(':PRODUCTIMAGES', $ProductImages, PDO::PARAM_STR);
            } else {
                $inserProductStatement->bindParam(':PRODUCTIMAGES', $ProductImages = null, PDO::PARAM_INT);
            }
        }*/ else {
            $inserProductStatement->bindParam(':PRODUCTIMAGES', $ProductImages = null, PDO::PARAM_INT);
        }
        if (!empty($data['VIDEOS'])) {
            $ProductVideos = $ProductImageModel->prepareVideoXML($data["VIDEOS"]);
            //dd($ProductVideos, true);
            if (!empty($ProductVideos)) {
                $inserProductStatement->bindParam(':PRODUCTVIDEOS', $ProductVideos, PDO::PARAM_STR);
            } else {
                $inserProductStatement->bindParam(':PRODUCTVIDEOS', $ProductVideos = null, PDO::PARAM_INT);
            }
        }else{
            $inserProductStatement->bindParam(':PRODUCTVIDEOS', $PRODUCTVIDEOS = null, PDO::PARAM_INT);
        }
        
        if(!empty($Files['PRODUCTDOCUMENTS'])){
            $ProductDocuments = $ProductImageModel->prepareDocumentXML($Files);
            //dd($ProductDocuments,true);
            if (!empty($ProductDocuments)) {
                $inserProductStatement->bindParam(':PRODUCTDOCUMENTS', $ProductDocuments, PDO::PARAM_STR);
            } else {
                $inserProductStatement->bindParam(':PRODUCTDOCUMENTS', $ProductDocuments = null, PDO::PARAM_INT);
            }
        }else{
            $inserProductStatement->bindParam(':PRODUCTDOCUMENTS', $PRODUCTDOCUMENTS=null, PDO::PARAM_INT);
        }  
        
        if (!empty($data['ARTAGS'])) {
            $ProductARTags = $ProductImageModel->prepareARTagXML($data["ARTAGS"]);
            //dd($ProductTags, true);
            if (!empty($ProductARTags)) {
                $inserProductStatement->bindParam(':PRODUCTARTAGS', $ProductARTags, PDO::PARAM_STR);
            } else {
                $inserProductStatement->bindParam(':PRODUCTARTAGS', $ProductARTags = null, PDO::PARAM_INT);
            }
        }else{
            $inserProductStatement->bindParam(':PRODUCTARTAGS', $PRODUCTARTAGS = null, PDO::PARAM_INT);
        }

        if (!empty($data['id'])) {
            $inserProductStatement->bindParam(':ID', $data['id'], PDO::PARAM_INT);
        } else {
            $inserProductStatement->bindParam(':ID', $data['id'] = null, PDO::PARAM_INT);
        }
        if (empty($data['is_disbaled'])) {
            $data['is_disbaled'] = 0;
        }
        if (empty($data['is_featured'])) {
            $data['is_featured'] = 0;
        }
        $inserProductStatement->bindParam(':IsFeatured', $data['is_featured'], PDO::PARAM_INT);
        $inserProductStatement->bindParam(':IsDisbaled', $data['is_disbaled'], PDO::PARAM_INT);
        $inserProductStatement->bindParam(':videoUrl', $data['videoUrl'] = null, PDO::PARAM_INT);

        /*if (empty($data['videoUrl'])) {
            $inserProductStatement->bindParam(':videoUrl', $data['videoUrl'] = null, PDO::PARAM_INT);
        } else {
            if (strpos($data['videoUrl'], 'youtube') > 0) {
                
            } elseif (strpos($data['videoUrl'], 'vimeo') > 0) {
                
            } else {
                //throw new Exception(__t("We accept only vimeo and youtube video urls"));
            }
            $inserProductStatement->bindParam(':videoUrl', $data['videoUrl'], PDO::PARAM_STR);
        }*/

        if (empty($data['is_virtual'])) {
            $inserProductStatement->bindParam(':is_virtual', $data['is_virtual'] = 0, PDO::PARAM_INT);
        } else {            
            $inserProductStatement->bindParam(':is_virtual', $data['is_virtual'], PDO::PARAM_STR);
        }
        $ProductID = null;
        $inserProductStatement->bindParam(':PRODUCTID', $ProductID, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 250);
        
        $inserProductStatement->execute();
        if (empty($ProductID)) {
            throw new Exception(__t("Sorry there was a problem while saving this product"));
        }

        ///REORDER
        if (!empty($data['image_url'])){
            $iamgeOrderXML = $ProductImageModel->prepareImageOrderXML($data['image_url']);
            $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_UpdateProductImageOrder(@data=:data)}");
            $statement->bindParam(':data', $iamgeOrderXML, PDO::PARAM_STR);
            $statement->execute();
        }
        
        return [
            'id' => $ProductID
        ];
    }

    public function getProduct($ProductID, $BUID = null,$ProductSlug = null) {
        if(!empty($BUID)){
            $this->BUID = $BUID;
        }else{
            $this->CheckAuthenticated();
        }
        if($this->BUID == null){
            throw new Exception("Sorry BUID is required");
        }
        if (empty($ProductID) && empty($ProductSlug)) {
            throw new Exception("Product id or slug not specified");
        }
        $stateMent = $this->PDO->prepare("{CALL PHP_Ecommerce_GetProductDetails(@BUID=:buid,@PRODUCTID=:productid,@PRODUCTSLUG=:PRODUCTSLUG)}");
        $stateMent->bindParam(':buid', $this->BUID, PDO::PARAM_INT);
        $stateMent->bindParam(':productid', $ProductID, PDO::PARAM_INT);
        $stateMent->bindParam(':PRODUCTSLUG', $ProductSlug, PDO::PARAM_STR);
        $stateMent->execute();
        $x = 0;
        $data = [];
        do {
            $rows = $stateMent->fetchAll(PDO::FETCH_ASSOC);
            //dd($rows);
            //if ($rows!=false) {
            switch ($x) {
                case 0:
                    $data['ProductDetails'] = $rows[0];
                    if ($data['ProductDetails']['is_featured'] == 1) {
                        $data['ProductDetails']['is_featured'] = true;
                    } else {
                        $data['ProductDetails']['is_featured'] = false;
                    }
                    break;
                case 1:
                    $data['ProductDetails']['ProductImages'] = $rows;
                    break;
                case 2:
                    foreach($rows as $k=>$v){
                        //dd($v,true);
                        $rows[$k]['videoURL'] = base64_decode($v['videoURL']);
                    }
                    $data['ProductDetails']['VIDEOS'] = $rows;
                    break;
                case 3:
                    $data['ProductDetails']['DOCUMENTS'] = $rows;
                    break;
                case 4:
                    $artags = '';
                    foreach ($rows as &$tag) {
                        $artags = $artags . $tag['name']. ','; 
                    }
                    $artags  = rtrim($artags,',');
                    $data['ProductDetails']['ARTAGS'] = $artags;
                    break;
                case 5:
                    $data['ProductDetails']['CATEGORIES'] = $rows;
                    $CATEGORIESIDS = [];
                    foreach ($rows as $val) {
                        $CATEGORIESIDS[$val['category_id']]["category_id"] = $val['category_id']; 
                        $CATEGORIESIDS[$val['category_id']]["productorder"] = $val['productorder']; 
                        $CATEGORIESIDS[$val['category_id']]["status"] = true; 
                    }
                    $data['ProductDetails']['CATEGORIESFORADMIN'] = $CATEGORIESIDS;
                    break;
                case 6:
                    $CATEGORIESIDS = [];
                    foreach ($rows as $val) {
                        $CATEGORIESIDS[] = $val['category_id']; 
                    }
                    $data['ProductDetails']['CATEGORIESIDS'] = $CATEGORIESIDS;
                    break;
                case 7:
                    $data['ProductDetails']['ProductVarients'] = $rows;
                    break;
                case 8:
                    foreach ($rows as &$varentoption) {
                        if ((int) $varentoption['pricetype'] == 0) {
                            $varentoption['pricetype'] = "-";
                        } else {
                            $varentoption['pricetype'] = "+";
                        }
                        if ($varentoption['price'] == .00) {
                            $varentoption['price'] = 0.00;
                        }
                        $keyVarientmap = array_search_find_key($data['ProductDetails']['ProductVarients'], 'id', $varentoption['varient_id']);
                        if ($data['ProductDetails']['ProductVarients'][$keyVarientmap]['is_required'] == 0) {
                            $data['ProductDetails']['ProductVarients'][$keyVarientmap]['is_required'] = false;
                        } else {
                            $data['ProductDetails']['ProductVarients'][$keyVarientmap]['is_required'] = true;
                        }
                        if ($data['ProductDetails']['ProductVarients'][$keyVarientmap]['is_multiple'] == 0) {
                            $data['ProductDetails']['ProductVarients'][$keyVarientmap]['is_multiple'] = false;
                        } else {
                            $data['ProductDetails']['ProductVarients'][$keyVarientmap]['is_multiple'] = true;
                        }
                        if ($data['ProductDetails']['ProductVarients'][$keyVarientmap]['is_required'] == false && strtolower($varentoption['title']) !== 'select') {
                            if (!isset($data['ProductDetails']['ProductVarients'][$keyVarientmap]['options'][0])) {
                                $data['ProductDetails']['ProductVarients'][$keyVarientmap]['options'][0] = [
                                    'title' => __t("Select"),
                                    'price' => 0,
                                    'pricetype' => '+',
                                    'sort_order' => 0,
                                    'id' => 0,
                                    'type' => 'default'
                                ];
                            }
                        }
                        $data['ProductDetails']['ProductVarients'][$keyVarientmap]['options'][] = $varentoption;
                    }

                    break;
            }
            $x++;
            // }
        } while ($stateMent->nextRowset());
        $stateMent->closeCursor();
        if (empty($data)) {
            throw new Exception("Sorry Product does not exists");
        }
        if (!isset($data['ProductDetails']['ProductImages'])) {
            $data['ProductDetails']['ProductImages'] = [];
        }
        if (!isset($data['ProductDetails']['ProductVarients'])) {
            $data['ProductDetails']['ProductVarients'] = [];
        }
        return $data;
    }

    public function Getproducts($start = 0, $limit = 10, $searchparams = [],  $category_id='', $name='',$BUID, $category_slug=null) {
        if(!empty($BUID)){
            $this->BUID = $BUID;
        }else{
            $this->CheckAuthenticated();
        }
        if($this->BUID == null){
            throw new Exception("Sorry BUID is required");
        }

        if(empty($category_slug)){
            $category_slug = null;
        }
        $stateMent = $this->PDO->prepare("{CALL PHP_Ecommerce_Getproducts(@buid = :buid, @Start = :Start, @Limit = :Limit, @Likename = :LikeName, @INDUSTRYID = :Industry_ID, @CATAGORYID = :Category_id, @SHOWALL = :ShowAll, @PRICEOVER = :PriceOver, @ISFEATURED = :IsFeatured, @categoryslug=:categoryslug)}");
        $stateMent->bindParam(':buid', $this->BUID, PDO::PARAM_INT);
        $stateMent->bindParam(':Start', $start, PDO::PARAM_INT);
        $stateMent->bindParam(':Limit', $limit, PDO::PARAM_INT);

        if($name!=''){
            $stateMent->bindParam(':LikeName', $name, PDO::PARAM_STR);
        }else{
            if (!empty($searchparams['name'])) {
                $stateMent->bindParam(':LikeName', $searchparams ['name'], PDO::PARAM_STR);
            } else {
                $stateMent->bindParam(':LikeName', $searchparams['name'] = null, PDO::PARAM_INT);
            }
        }
        if (!empty($searchparams['industry_id'])) {
            $stateMent->bindParam(':Industry_ID', $searchparams['industry_id'], PDO::PARAM_INT);
        } else {
            $stateMent->bindParam(':Industry_ID', $searchparams['industry_id'] = null, PDO::PARAM_INT);
        }
        if($category_id!=''){
            $stateMent->bindParam(':Category_id', $category_id, PDO::PARAM_INT);
        }else{
            if (!empty($searchparams['category_id'])) {
                $stateMent->bindParam(':Category_id', $searchparams['category_id'], PDO::PARAM_INT);
            } else {
                $stateMent->bindParam(':Category_id', $searchparams['category_id'] = null, PDO::PARAM_INT);
            }
        }
        
        if (!empty($searchparams['showall'])) {
            $stateMent->bindParam(':ShowAll', $searchparams['showall'], PDO::PARAM_BOOL);
        } else {
            $stateMent->bindParam(':ShowAll', $searchparams['showall'] = null, PDO::PARAM_INT);
        }
        if (!empty($searchparams['priceover'])) {
            $stateMent->bindParam(':PriceOver', $searchparams['priceover'], PDO::PARAM_STR);
        } else {
            $stateMent->bindParam(':PriceOver', $searchparams['priceover'] = null, PDO::PARAM_INT);
        }
        if (!empty($searchparams['isfeatured'])) {
            $stateMent->bindParam(':IsFeatured', $searchparams['isfeatured'], PDO::PARAM_INT);
        } else {
            $stateMent->bindParam(':IsFeatured', $searchparams['isfeatured'] = null, PDO::PARAM_INT);
        }

        $stateMent->bindParam(':categoryslug', $category_slug, PDO::PARAM_STR);

        $stateMent->execute();
        do {
            $rows = $stateMent->fetchAll(PDO::FETCH_ASSOC);

            if ($rows) {
                if (isset($rows[0]['TotalProducts'])) {
                    $data['Paginations'] = [
                        'Total' => $rows[0] ['TotalProducts'],
                        'Start' => $start,
                        'limit' => $limit
                    ];
                } else {
                    $data['Products'] = $rows;
                }
            }
        } while ($stateMent->nextRowset());
        $stateMent->closeCursor();
        if (empty($data['Products'])) {
            $data['Products'] = [];
        }

        return $data;
    }

    private function __validate(&$data) {
        if (empty($data['Name'])) {
            throw new Exception("Sorry name is required");
        }
        if (empty($data['slug'])) {
            $data['slug'] = $this->_validSlug(slugify($data['Name']), true);
        } elseif (!empty($data['slug'])) {
            $data['slug'] = $this->_validSlug($data['slug']);
        }
        if (empty($data['description'])) {
            throw new Exception("Sorry product description Can not be blank");
        }
        if (strlen($data['description']) <= 5 && strlen($data['description']) > 1000) {
            throw new Exception("Sorry product description length should be between 5 to 1000 charectors");
        }
        $this->_validSku($data['sku']);
    }

    private function _validSlug($slug, $createDup = false) {
        if (empty($slug)) {
            throw new Exception("Sorry slug can not be empty");
        }

        $CheckStateMent = $this->PDO->prepare("SELECT count(*) as slugcount from {$this->DbAlias}{$this->Table} where slug LIKE :slug and buid=:buid");
        $checkSlub = (string) $slug . "%";
        $CheckStateMent->bindparam(':slug', $checkSlub, PDO::PARAM_STR);
        $CheckStateMent->bindparam(':buid', $this->BUID, PDO::PARAM_INT);
        $CheckStateMent->execute();
        $data = $CheckStateMent->fetch(PDO::FETCH_ASSOC);
        if ($data['slugcount'] != 0) {
            if (!$createDup) {
                throw new Exception("Sorry invalid slug specified");
            } else {
                $newSlug = slugify($slug . " " . ($data['slugcount'] + 1));
                return $this->_validSlug($newSlug, $createDup);
            }
        }
        return $slug;
    }

    private function _validSku($sku) {
        if (empty($sku)) {
            throw new Exception("Sorry Sku can not be empty");
        }
        $CheckStateMent = $this->PDO->prepare("SELECT count(*) as skuCount from {$this->DbAlias}{$this->Table} where sku=:sku and buid=:buid");
        $CheckStateMent->bindparam(':sku', $sku, PDO::PARAM_STR);
        $CheckStateMent->bindparam(':buid', $this->BUID, PDO::PARAM_INT);
        $CheckStateMent->execute();
        $data = $CheckStateMent->fetch(PDO::FETCH_ASSOC);
        if ($data['skuCount'] != 0) {
            throw new Exception("Sorry this sku already exists");
        }
    }

    public function makeInactive($data) {
        if(!empty($data['BUID'])){
            $this->BUID = $data['BUID'];
        }else{
            $this->CheckAuthenticated();
        }
        if($this->BUID == null){
            throw new Exception("Sorry BUID is required");
        }
        if (empty($data['product_id'])) {
            throw new Exception("Sorry product id not valid");
        }
        if ($data['is_disabled'] != 0 && $data['is_disabled'] != 1) {
            $data['is_disabled'] = null;
        }
        if ($data['is_featured'] != 0 && $data['is_featured'] != 1) {
            $data['is_featured'] = null;
        }
        $data['product_id'] = implode(",", $data['product_id']);
        $CheckStateMent = $this->PDO->prepare("{CALL PHP_Ecommerce_ChangeProductStats (@BUID=:Buid,@ProductID=:productID,@Status=:Is_Disbaled,@Featured=:IS_Featured)}");
        $CheckStateMent->bindparam(':Buid', $this->BUID, PDO::PARAM_INT);
        $CheckStateMent->bindparam(':productID', $data['product_id'], PDO::PARAM_STR);
        $CheckStateMent->bindparam(':Is_Disbaled', $data['is_disabled'], PDO::PARAM_INT);
        $CheckStateMent->bindparam(':IS_Featured', $data['is_featured'], PDO::PARAM_INT);
        $CheckStateMent->execute();
        return true;
    }

    public function deleteProduct($data) {
        if(!empty($data['BUID'])){
            $this->BUID = $data['BUID'];
        }else{
            $this->CheckAuthenticated();
        }
        if($this->BUID == null){
            throw new Exception("Sorry BUID is required");
        }
        if (empty($data['product_id'])) {
            throw new Exception("Sorry product id not valid");
        }
        $CheckStateMent = $this->PDO->prepare("{CALL PHP_Ecommerce_DeleteProduct (@BUID=:Buid,@ProductID=:productID)}");
        $CheckStateMent->bindparam(':Buid', $this->BUID, PDO::PARAM_INT);
        $CheckStateMent->bindparam(':productID', $data['product_id'], PDO::PARAM_INT);
        $CheckStateMent->execute();
        return true;
    }

    public function importProducts($FILES) {
        $this->CheckAuthenticated();
        $max_size = file_upload_max_size();
        $client = new S3Client([
            'version' => 'latest',
            'region' => AWS_REGION,
            'credentials' => [
                'key' => AWS_ACCESS_KEY,
                'secret' => AWS_SECRET_KEY
            ]
        ]);
        $valid_formats = ["csv"];
        $UploadFileXml = [];
        foreach ($FILES['Import']['tmp_name'] as $key => $value) {
            if ($FILES['Import']['error'][$key] == 0) {
                $name = $FILES['Import']['name'][$key];
                $size = $FILES['Import']['size'][$key];
                $tmp = $FILES['Import']['tmp_name'][$key];
                $type = $FILES['Import']['type'][$key];
                if ($size > $max_size) {
                    throw new Exception(__t("Sorry file " . $name . " size is bigger than accepted size"));
                }
                $ext = strtolower(getExtension($name));
                if (!in_array($ext, $valid_formats)) {
                    throw new Exception(__t("Sorry file " . $name . " is not valid ,we accept " . implode(",", $valid_formats)));
                }
                $image_name_actual = time() . "." . $ext;
                try {
                    $Result = $client->putObject(array(
                        'Bucket' => AWS_BUCKET_NAME,
                        'Key' => 'BU' . $this->BUID . "/Import/" . $image_name_actual,
                        'SourceFile' => $tmp,
                        'StorageClass' => 'REDUCED_REDUNDANCY',
                        'ACL' => 'public-read',
                        'ContentType' => $type,
                        'Expires' => date('Y-m-d H:i:s', strtotime("2 years"))
                    ));
                } catch (S3Exception $exc) {
                    throw new Exception("Aws S3 Error : " . $exc->getAwsErrorMessage());
                }
                $UploadFileXml[] = $Result['ObjectURL'];
            }
        }

        if (empty($UploadFileXml)) {
            throw new Exception(__t("Sorry file not uploaded"));
        }

        $ProductJson = file_get_contents(FILEUPLOADPATH . "/webapi/BusinessUnit/CSVtoJsonConvertion?filepath=" . $UploadFileXml[0]);
        if (!$ProductJson) {
            throw new Exception(__t("Sorry file not parsed"));
        }
        $PARR=json_decode($ProductJson); 
        //dd($PARR, true);

        $ProductImageModel = new ProductImageModel();
        foreach($PARR as $key=>$val){

            $inserProductStatement = $this->PDO->prepare("{CALL PHP_Ecommerce_AddProducts ("
                . "@Name=:Name,"
                . "@description=:description,"
                . "@sku=:sku,"
                . "@category_id=:category_id,"
                . "@PriceTypeID=:PriceTypeID,"
                . "@Amount=:Amount,"
                . "@stock=:stock,"
                . "@buid=:buid,"
                . "@slug=:slug,"
                . "@brand_id=:brand_id,"
                . "@supplier_id=:supplier_id,"
                . "@supplier_code=:supplier_code,"
                . "@sales_account_code=:sales_account_code,"
                . "@purchase_account_code=:purchase_account_code,"
                . "@units=:units,"
                . "@VARIENTS=:VARIENTS,"
                . "@PRODUCTIMAGES=:PRODUCTIMAGES,"
                . "@PRODUCTVIDEOS=:PRODUCTVIDEOS,"
                . "@PRODUCTDOCUMENTS=:PRODUCTDOCUMENTS,"
                . "@PRODUCTARTAGS=:PRODUCTARTAGS," 
                . "@ID=:ID,"
                . "@ISDISBALED=:IsDisbaled,"
                . "@ISFEATURED=:IsFeatured,"
                . "@VIDEOURL=:videoUrl,"
                . "@is_virtual=:is_virtual,"
                . "@PRODUCTID=:PRODUCTID"
                . ")}");

            $inserProductStatement->bindParam(':Name', $val->Product_Name, PDO::PARAM_STR);
            $inserProductStatement->bindParam(':description', strip_tags( $val->Description,'<br><hr><blockquote><u><i><a><p><div><table><td><tr><code><pre><ul><ol><li><span><article><h1><h2><h3><h4><h5><h6><title><abbr><img><b><strong>'), PDO::PARAM_STR);
            $inserProductStatement->bindParam(':sku', $val->SKU, PDO::PARAM_STR);
            
            if (!empty($val->Category_id)) {
                $categories=explode('|',$val->Category_id);
                //dd($categories);
                $ProductCategories = $ProductImageModel->prepareCategotyXMLCSVUPLOAD($categories);
                if (!empty($ProductCategories)) {
                    $inserProductStatement->bindParam(':category_id', $ProductCategories, PDO::PARAM_STR);
                } else {
                    $inserProductStatement->bindParam(':category_id', $ProductCategories = null, PDO::PARAM_INT);
                }
            }else{
                $inserProductStatement->bindParam(':category_id', $PRODUCTVIDEOS = null, PDO::PARAM_INT);
            }

            $inserProductStatement->bindParam(':PriceTypeID', $PriceTypeID = 38, PDO::PARAM_INT);
            if (!empty($val->Unit)) {
                $inserProductStatement->bindParam(':units', $val->Unit, PDO::PARAM_STR);
            } else {
                $inserProductStatement->bindParam(':units', $val->Unit = null, PDO::PARAM_INT);
            }            
            $inserProductStatement->bindParam(':Amount', $val->Price, PDO::PARAM_STR);
            $inserProductStatement->bindParam(':stock', $val->Quantity, PDO::PARAM_STR);
            $inserProductStatement->bindParam(':buid', $this->BUID, PDO::PARAM_INT);            
            $inserProductStatement->bindParam(':slug', $slug = null, PDO::PARAM_STR);
            $inserProductStatement->bindParam(':brand_id', $brand = null, PDO::PARAM_INT);
            $inserProductStatement->bindParam(':supplier_id', $supplier = null, PDO::PARAM_INT);
            $inserProductStatement->bindParam(':supplier_code', $supplier_code = null, PDO::PARAM_INT);
            $inserProductStatement->bindParam(':sales_account_code', $sales_account_code = null, PDO::PARAM_INT);
            $inserProductStatement->bindParam(':purchase_account_code', $purchase_account_code = null, PDO::PARAM_INT);
            $inserProductStatement->bindParam(':VARIENTS', $VARIENTS = null, PDO::PARAM_INT);
            
            if (!empty($val->Images)) {   
                $images=explode('|',$val->Images); 
                //dd($images);
                $ProductImages = $ProductImageModel->prePareImageXmlCSVUPLOAD($images);
                if (!empty($ProductImages)) {
                    $inserProductStatement->bindParam(':PRODUCTIMAGES', $ProductImages, PDO::PARAM_STR);
                } else {
                    $inserProductStatement->bindParam(':PRODUCTIMAGES', $ProductImages = null, PDO::PARAM_INT);
                }
            }else {
                $inserProductStatement->bindParam(':PRODUCTIMAGES', $ProductImages = null, PDO::PARAM_INT);
            }
            
            if($val->Video!=''){
                $VIDEOS=explode('|',$val->Video); 
                //dd($VIDEOS);
                $ProductVideos = $ProductImageModel->prepareVideoXMLBUCSVUPLOAD($VIDEOS);
                if (!empty($ProductVideos)) {
                    $inserProductStatement->bindParam(':PRODUCTVIDEOS', $ProductVideos, PDO::PARAM_STR);
                } else {
                    $inserProductStatement->bindParam(':PRODUCTVIDEOS', $ProductVideos = null, PDO::PARAM_INT);
                }

            }else{
                $inserProductStatement->bindParam(':PRODUCTVIDEOS', $PRODUCTVIDEOS = null, PDO::PARAM_INT);
            }

            if($val->Document!=''){
                $Document=explode('|',$val->Document); 
                $ProductDocument = $ProductImageModel->prepareDocumentXMLBUCSVUPLOAD($Document);
                if (!empty($ProductDocument)) {
                    $inserProductStatement->bindParam(':PRODUCTDOCUMENTS', $ProductDocument, PDO::PARAM_STR);
                } else {
                    $inserProductStatement->bindParam(':PRODUCTDOCUMENTS', $ProductDocument = null, PDO::PARAM_INT);
                }

            }else{
                $inserProductStatement->bindParam(':PRODUCTDOCUMENTS', $PRODUCTDOCUMENTS = null, PDO::PARAM_INT);
            }  
            
            $inserProductStatement->bindParam(':PRODUCTARTAGS', $PRODUCTARTAGS = null, PDO::PARAM_INT);
            $inserProductStatement->bindParam(':ID', $id = null, PDO::PARAM_INT);           
            $inserProductStatement->bindParam(':IsFeatured', $val->IsFeatured, PDO::PARAM_INT);
            $inserProductStatement->bindParam(':IsDisbaled', $val->IsDisbaled, PDO::PARAM_INT);
            $inserProductStatement->bindParam(':videoUrl', $videoUrl = null, PDO::PARAM_INT);
            $inserProductStatement->bindParam(':is_virtual', $val->is_virtual, PDO::PARAM_INT);    
            $ProductID = null;           
            $inserProductStatement->bindParam(':PRODUCTID', $ProductID, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 250);
            //exit;
            $inserProductStatement->execute();
            
        }
        return $UploadFileXml;
    }

    public function convertCSVToJSON($file){
        $csv= file_get_contents($file);
        $csvArr = explode("\n", $csv);
        $array = array_map("str_getcsv", explode("\n", $csv));
        //$json = json_encode($array);
        return $array;
    }

    public function AddProductTestimonial($data){
        if(!empty($data['BUID'])){
            $this->BUID = $data['BUID'];
        }else{
            $this->CheckAuthenticated();
        }
        if($this->BUID == null){
            throw new Exception("Sorry BUID is required");
        }
        $testimonial_on = gmdate(DATE_ATOM);
        $stateMent = $this->PDO->prepare("{CALL PHP_Ecommerce_AddproductTestimonials(
            @BUID=:BUID
		    , @product_id=:product_id
		    , @testimonial_by=:testimonial_by
		    , @testimonial=:testimonial
		    , @testimonial_on=:testimonial_on
            )}");
        $stateMent->bindParam(':BUID', $this->BUID, PDO::PARAM_INT);
        $stateMent->bindParam(':product_id', $data['product_id'], PDO::PARAM_INT);
        $stateMent->bindParam(':testimonial_by', $data['testimonial_by'], PDO::PARAM_STR);
        $stateMent->bindParam(':testimonial', $data['testimonial'], PDO::PARAM_STR);
        $stateMent->bindParam(':testimonial_on', $testimonial_on, PDO::PARAM_STR);

        $stateMent->execute();
        $rows = $stateMent->fetchAll(PDO::FETCH_ASSOC);
        $stateMent->closeCursor();

        return $rows;
    }

    public function getProductTestimonials($data){
        if(!empty($data['BUID'])){
            $this->BUID = $data['BUID'];
        }else{
            $this->CheckAuthenticated();
        }
        if($this->BUID == null){
            throw new Exception("Sorry BUID is required");
        }
        $stateMent = $this->PDO->prepare("{CALL PHP_Ecommerce_GetproductTestimonials(
            @buid = :buid
            , @product_id = :product_id
            , @Start = :Start
            , @Limit = :Limit
            , @SearchString = :SearchString
            )}");
        $stateMent->bindParam(':buid', $this->BUID, PDO::PARAM_INT);
        $stateMent->bindParam(':product_id', $data['product_id'], PDO::PARAM_INT);
        $stateMent->bindParam(':Start', $data['start'], PDO::PARAM_INT);
        $stateMent->bindParam(':Limit', $data['limit'], PDO::PARAM_INT);

        if (!empty($data['SearchString'])) {
            $stateMent->bindParam(':SearchString', $data ['SearchString'], PDO::PARAM_STR);
        } else {
            $stateMent->bindParam(':SearchString', $data['SearchString'] = null, PDO::PARAM_INT);
        }

        $stateMent->execute();
        do {
            $rows = $stateMent->fetchAll(PDO::FETCH_ASSOC);

            if ($rows) {
                if (isset($rows[0]['TotalProducts'])) {
                    $dataRet['Pagination'] = [
                        'total' => $rows[0] ['TotalProducts'],
                        'start' => $data['start'],
                        'limit' => $data['limit']
                    ];
                } else {
                    $dataRet['Testimonials'] = $rows;
                }
            }
        } while ($stateMent->nextRowset());
        $stateMent->closeCursor();
        if (empty($dataRet['Testimonials'])) {
            $dataRet['Testimonials'] = [];
        }

        return $dataRet;
    }

    public function deleteProductImage($data) {
        if(!empty($data['BUID'])){
            $this->BUID = $data['BUID'];
        }else{
            $this->CheckAuthenticated();
        }
        if($this->BUID == null){
            throw new Exception("Sorry BUID is required");
        }
        if (empty($data['product_id'])) {
            throw new Exception("Sorry product id not valid");
        }
        if (empty($data['imageid'])) {
            throw new Exception("Sorry image id not valid");
        }
        
        $CheckStateMent = $this->PDO->prepare("{CALL PHP_Ecommerce_DeleteProductImage (@BUID=:Buid,@ProductID=:productID,@ImageID=:ImageID)}");
        $CheckStateMent->bindparam(':Buid', $this->BUID, PDO::PARAM_INT);
        $CheckStateMent->bindparam(':productID', $data['product_id'], PDO::PARAM_INT);
        $CheckStateMent->bindparam(':ImageID', $data['imageid'], PDO::PARAM_INT);
        $CheckStateMent->execute();
        return true;
    }

    public function deleteProductVideo($data) {
        if(!empty($data['BUID'])){
            $this->BUID = $data['BUID'];
        }else{
            $this->CheckAuthenticated();
        }
        if($this->BUID == null){
            throw new Exception("Sorry BUID is required");
        }
        if (empty($data['product_id'])) {
            throw new Exception("Sorry product id not valid");
        }
        if (empty($data['videoid'])) {
            throw new Exception("Sorry video id not valid");
        }
        $CheckStateMent = $this->PDO->prepare("{CALL PHP_Ecommerce_DeleteProductVideo (@BUID=:Buid,@ProductID=:productID,@VideoID=:VideoID)}");
        $CheckStateMent->bindparam(':Buid', $this->BUID, PDO::PARAM_INT);
        $CheckStateMent->bindparam(':productID', $data['product_id'], PDO::PARAM_INT);
        $CheckStateMent->bindparam(':VideoID', $data['videoid'], PDO::PARAM_INT);
        $CheckStateMent->execute();
        return true;
    }

    public function deleteProductDocument($data) {
        if(!empty($data['BUID'])){
            $this->BUID = $data['BUID'];
        }else{
            $this->CheckAuthenticated();
        }
        if($this->BUID == null){
            throw new Exception("Sorry BUID is required");
        }
        if (empty($data['product_id'])) {
            throw new Exception("Sorry product id not valid");
        }
        if (empty($data['documentid'])) {
            throw new Exception("Sorry document id not valid");
        }
        $CheckStateMent = $this->PDO->prepare("{CALL PHP_Ecommerce_DeleteProductDocument (@BUID=:Buid,@ProductID=:productID,@DocumentID=:DocumentID)}");
        $CheckStateMent->bindparam(':Buid', $this->BUID, PDO::PARAM_INT);
        $CheckStateMent->bindparam(':productID', $data['product_id'], PDO::PARAM_INT);
        $CheckStateMent->bindparam(':DocumentID', $data['documentid'], PDO::PARAM_INT);
        $CheckStateMent->execute();
        return true;
    }

    public function deleteProductTestimonial($data) {
        if(!empty($data['BUID'])){
            $this->BUID = $data['BUID'];
        }else{
            $this->CheckAuthenticated();
        }
        if($this->BUID == null){
            throw new Exception("Sorry BUID is required");
        }
        if (empty($data['product_id'])) {
            throw new Exception("Sorry product id not valid");
        }
        if (empty($data['testimonialid'])) {
            throw new Exception("Sorry testimonial id not valid");
        }
        $CheckStateMent = $this->PDO->prepare("{CALL PHP_Ecommerce_DeleteProductTestimonial (@BUID=:Buid,@ProductID=:productID,@TestimonialID=:TestimonialID)}");
        $CheckStateMent->bindparam(':Buid', $this->BUID, PDO::PARAM_INT);
        $CheckStateMent->bindparam(':productID', $data['product_id'], PDO::PARAM_INT);
        $CheckStateMent->bindparam(':TestimonialID', $data['testimonialid'], PDO::PARAM_INT);
        $CheckStateMent->execute();
        return true;
    }

    public function getAllProductsByARTag($BUID, $start = 0, $limit = 10, $searchparams = []) {
        if(!empty($BUID)){
            $this->BUID = $BUID;
        }else{
            $this->CheckAuthenticated();
        }
        if($this->BUID == null){
            throw new Exception("Sorry BUID is required");
        }
        if($searchparams['ARTag']==""){
            throw new Exception(__t("Enter AR Tag Name"));
        }

        /*******************************/
        $LINK = '';
        $url = API_URL_ENDPOINT . "/webapi/Authenticate/login";

        //curl headers
        $headers = array(
            "authorization: Bearer apiuser:wXPocq0fG24=",
            "cache-control: no-cache",
            "content-type: application/x-www-form-urlencoded"
        );

        //request body (make an array, then convert it to a query string)
        $body_arr = array();

        $body_str = http_build_query($body_arr);

        //Set the CURL Options
        $curl_opts = array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_URL => $url,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $body_str,
            CURLOPT_CONNECTTIMEOUT => 10,
            CURLOPT_TIMEOUT => 20,
            CURLOPT_HEADER => 1,
            CURLOPT_VERBOSE => 1
        );

        //Set up the CURL request
        $resCurl = curl_init();
        curl_setopt_array($resCurl, $curl_opts);

        // Send the request & save response to $resp
        $response = curl_exec($resCurl);
        $info = curl_getinfo($resCurl, CURLINFO_HEADER_SIZE);
        $err_no = curl_errno($resCurl);
        curl_close($resCurl);
        $res = get_headers_from_curl_response($response);

        if (0 != $err_no) {
            throw new Exception('Error Retrieving API Auth Token. Curl Err:' . $err_no);
        } else {
            if (isset($res['error'])) {
                throw new Exception('Error Retrieving API Auth Token. API err:' . $res['error'] . " - " . $res['error_description']);
            }
            $token = $res['Token'];

            //dd($token, true);

            $url = API_URL_ENDPOINT."/webapi/BusinessUnit/GetECommerceComponentInfo";
        
            $headers = array(
                "Token: ".$token,
                "cache-control: no-cache",
                "content-type: application/x-www-form-urlencoded"
            );
            //echo $this->BUID; exit;
            $body_arr = array("buid"=>$this->BUID);
            $body_str = http_build_query($body_arr);
            $curl_opts = array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_HTTPHEADER => $headers,
                CURLOPT_URL => $url,
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => $body_str,
                CURLOPT_CONNECTTIMEOUT => 10,
                CURLOPT_TIMEOUT => 20,
                CURLOPT_HEADER => 0,
                CURLOPT_VERBOSE => 1
            );

            $resCurl = curl_init();
            curl_setopt_array($resCurl, $curl_opts);
            $response = curl_exec($resCurl);
            $info = curl_getinfo($resCurl, CURLINFO_HEADER_SIZE);
            $err_no = curl_errno($resCurl);
            curl_close($resCurl);
            //dd($response, true);
            if (0 != $err_no) {
                throw new Exception('Error Retrieving GetECommerceComponentInfo API. Curl Err:' . $err_no);
            } else {
                $temARR = json_decode($response);
                $TID=0;
                //dd($temARR, true);
                foreach ($temARR->Templates as $val){
                    if($val->TemplateType == 'Product_Details'){
                        $TID = $val->PKID;
                    }
                }

                //$LINK = "/template/".$TID."-Template?product_id=".$data['product_id'].'&category_id='.$data['category_id'];
                $LINK = "/template/".$TID."-Template";
            }

        }
        /****************************/

        $stateMent = $this->PDO->prepare("{CALL PHP_Ecommerce_GetproductsByTag(@buid = :buid, @Start = :Start, @Limit = :Limit, @Likename = :LikeName, @INDUSTRYID = :Industry_ID, @CATAGORYID = :Category_id, @SHOWALL = :ShowAll, @PRICEOVER = :PriceOver, @ISFEATURED = :IsFeatured, @ARTag=:ARTag)}");
        $stateMent->bindParam(':buid', $this->BUID, PDO::PARAM_INT);
        $stateMent->bindParam(':Start', $start, PDO::PARAM_INT);
        $stateMent->bindParam(':Limit', $limit, PDO::PARAM_INT);

        if (!empty($searchparams['name'])) {
            $stateMent->bindParam(':LikeName', $searchparams ['name'], PDO::PARAM_STR);
        } else {
            $stateMent->bindParam(':LikeName', $searchparams['name'] = null, PDO::PARAM_INT);
        }
        if (!empty($searchparams['industry_id'])) {
            $stateMent->bindParam(':Industry_ID', $searchparams['industry_id'], PDO::PARAM_INT);
        } else {
            $stateMent->bindParam(':Industry_ID', $searchparams['industry_id'] = null, PDO::PARAM_INT);
        }
        if (!empty($searchparams['category_id'])) {
            $stateMent->bindParam(':Category_id', $searchparams['category_id'], PDO::PARAM_INT);
        } else {
            $stateMent->bindParam(':Category_id', $searchparams['category_id'] = null, PDO::PARAM_INT);
        }
        if (!empty($searchparams['showall'])) {
            $stateMent->bindParam(':ShowAll', $searchparams['showall'], PDO::PARAM_BOOL);
        } else {
            $stateMent->bindParam(':ShowAll', $searchparams['showall'] = null, PDO::PARAM_INT);
        }
        if (!empty($searchparams['priceover'])) {
            $stateMent->bindParam(':PriceOver', $searchparams['priceover'], PDO::PARAM_STR);
        } else {
            $stateMent->bindParam(':PriceOver', $searchparams['priceover'] = null, PDO::PARAM_INT);
        }
        if (!empty($searchparams['isfeatured'])) {
            $stateMent->bindParam(':IsFeatured', $searchparams['isfeatured'], PDO::PARAM_INT);
        } else {
            $stateMent->bindParam(':IsFeatured', $searchparams['isfeatured'] = null, PDO::PARAM_INT);
        }

        $stateMent->bindParam(':ARTag', $searchparams['ARTag'], PDO::PARAM_STR);

        $stateMent->execute();
        do {
            $rows = $stateMent->fetchAll(PDO::FETCH_ASSOC);

            if ($rows) {
                if (isset($rows[0]['TotalProducts'])) {
                    $data['Paginations'] = [
                        'Total' => $rows[0] ['TotalProducts'],
                        'Start' => $start,
                        'limit' => $limit,
                        'ProductLink' => $LINK
                    ];
                } else {
                    $data['Products'] = $rows;
                }
            }
        } while ($stateMent->nextRowset());
        $stateMent->closeCursor();
        if (empty($data['Products'])) {
            $data['Products'] = [];
        }

        return $data;
    }

    public function generateProductQR($data){
        if(!empty($data['BUID'])){
            $this->BUID = $data['BUID'];
        }else{
            $this->CheckAuthenticated();
        }
        if($this->BUID == null){
            throw new Exception("Sorry BUID is required");
        }
        $url = API_URL_ENDPOINT . "/webapi/Authenticate/login";

        //curl headers
        $headers = array(
            "authorization: Bearer apiuser:wXPocq0fG24=",
            "cache-control: no-cache",
            "content-type: application/x-www-form-urlencoded"
        );

        //request body (make an array, then convert it to a query string)
        $body_arr = array();

        $body_str = http_build_query($body_arr);

        //Set the CURL Options
        $curl_opts = array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_URL => $url,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $body_str,
            CURLOPT_CONNECTTIMEOUT => 10,
            CURLOPT_TIMEOUT => 20,
            CURLOPT_HEADER => 1,
            CURLOPT_VERBOSE => 1
        );

        //Set up the CURL request
        $resCurl = curl_init();
        curl_setopt_array($resCurl, $curl_opts);

        // Send the request & save response to $resp
        $response = curl_exec($resCurl);
        $info = curl_getinfo($resCurl, CURLINFO_HEADER_SIZE);
        $err_no = curl_errno($resCurl);
        curl_close($resCurl);
        $res = get_headers_from_curl_response($response);

        if (0 != $err_no) {
            throw new Exception('Error Retrieving API Auth Token. Curl Err:' . $err_no);
        } else {
            if (isset($res['error'])) {
                throw new Exception('Error Retrieving API Auth Token. API err:' . $res['error'] . " - " . $res['error_description']);
            }
            $token = $res['Token'];

            //dd($token, true);

            $url = API_URL_ENDPOINT."/webapi/BusinessUnit/GetECommerceComponentInfo";
        
            $headers = array(
                "Token: ".$token,
                "cache-control: no-cache",
                "content-type: application/x-www-form-urlencoded"
            );
            //echo $this->BUID; exit;
            $body_arr = array("buid"=>$this->BUID);
            $body_str = http_build_query($body_arr);
            $curl_opts = array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_HTTPHEADER => $headers,
                CURLOPT_URL => $url,
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => $body_str,
                CURLOPT_CONNECTTIMEOUT => 10,
                CURLOPT_TIMEOUT => 20,
                CURLOPT_HEADER => 0,
                CURLOPT_VERBOSE => 1
            );

            $resCurl = curl_init();
            curl_setopt_array($resCurl, $curl_opts);
            $response = curl_exec($resCurl);
            $info = curl_getinfo($resCurl, CURLINFO_HEADER_SIZE);
            $err_no = curl_errno($resCurl);
            curl_close($resCurl);
            //dd($response, true);
            if (0 != $err_no) {
                throw new Exception('Error Retrieving GetECommerceComponentInfo API. Curl Err:' . $err_no);
            } else {
                $temARR = json_decode($response);
                $TID=0;
                //dd($temARR, true);
                foreach ($temARR->Templates as $val){
                    if($val->TemplateType == 'Product_Details'){
                        $TID = $val->PKID;
                    }
                }

                $LINK = '/template/'.$TID.'-Template?product_id='.$data['product_id'].'&category_id='.$data['category_id'];
                /*include('phpqrcode/qrlib.php');

                $folder="../qrimg/";
                $file_name=$data['product_id']."-QR.png";
                $file_nameR='qrimg/'.$file_name;
                $file_name=$folder.$file_name;
                
                $dataNew = '{"type":"Product","id":"'.$data['product_id'].'", "CategoryID ":"'.$data['category_id'].'","link":'.$LINK.',"image":"'.$data['image'].'"}';
                QRcode::png($dataNew,$file_name);*/
                $dataNew = '{"type":"Product","id":"'.$data['product_id'].'", "CategoryID ":"'.$data['category_id'].'","link":'.urlencode($LINK).',"image":"'.$data['image'].'"}';
                return $dataNew;
            }

        }





        //dd($data);
        






        
    }

    public function generateProductQRPDF($data){

        //dd($data);
        require('fpdf/fpdf.php');

        $pdf = new PDF();
        $pdf->AliasNbPages();
        $pdf->AddPage();
        $pdf->SetFont('Times','',12);
        $pdf->Image($data['image'],10,6,30);
        $pdf->Output();
        
        

        /*include('phpqrcode/qrlib.php');

        $folder="../qrimg/";
        $file_name=$data['product_id']."-QR.png";
        $file_nameR='qrimg/'.$file_name;
        $file_name=$folder.$file_name;
        
        $dataNew = '{"type":"Product","id":"'.$data['product_id'].'", "CategoryID ":"'.$data['category_id'].'","link":'.$LINK.',"image":"'.$data['image'].'"}';
        QRcode::png($dataNew,$file_name);

        return $file_nameR;*/





        
    }

    public function getDefinedARTagsByBU($data) {
        //dd($this->BUID, true);
        if(!empty($data['BUID'])){
            $this->BUID = $data['BUID'];
        }else{
            $this->CheckAuthenticated();
        }
        if($this->BUID == null){
            throw new Exception("Sorry BUID is required");
        }
        $stateMent = $this->PDO->prepare("{CALL PHP_Ecommerce_GetARTagsByBU(@BUID = :buid)}");
        $stateMent->bindParam(':buid', $this->BUID, PDO::PARAM_INT);        

        $stateMent->execute();
        do {
            $rows = $stateMent->fetchAll(PDO::FETCH_ASSOC);
            if ($rows) {                
                $data['default'][] = $rows;
            }
        } while ($stateMent->nextRowset());
        $stateMent->closeCursor();
        $d['default'] = array();
        foreach($data['default'][0] as $val){
            //$d['default'][] = $val;
            //$d['default']
            array_push($d['default'], $val);
        }
         
        if(count($data['default']>1)){
            foreach($data['default'][1] as $val){
                array_push($d['default'], $val);
            }
        }
       
        return $d;
    }

    public function makeDefaultProductImage($data) {
        if(!empty($data['BUID'])){
            $this->BUID = $data['BUID'];
        }else{
            $this->CheckAuthenticated();
        }
        if($this->BUID == null){
            throw new Exception("Sorry BUID is required");
        }
        if (empty($data['product_id'])) {
            throw new Exception("Sorry product id not valid");
        }
        if (empty($data['imageid'])) {
            throw new Exception("Sorry image id not valid");
        }
        
        $CheckStateMent = $this->PDO->prepare("{CALL PHP_Ecommerce_makeDefaultProductImage (@BUID=:Buid,@ProductID=:productID,@ImageID=:ImageID)}");
        $CheckStateMent->bindparam(':Buid', $this->BUID, PDO::PARAM_INT);
        $CheckStateMent->bindparam(':productID', $data['product_id'], PDO::PARAM_INT);
        $CheckStateMent->bindparam(':ImageID', $data['imageid'], PDO::PARAM_INT);
        $CheckStateMent->execute();
        return true;
    }

    public function GetProductOrderForCategory($categoryid='',$BUID, $name='') {
        if(!empty($BUID)){
            $this->BUID = $BUID;
        }else{
            $this->CheckAuthenticated();
        }
        if($this->BUID == null){
            throw new Exception("Sorry BUID is required");
        }

        /*if(empty($category_slug)){
            $category_slug = null;
        }*/
        $stateMent = $this->PDO->prepare("{CALL PHP_Ecommerce_GetProductOrderForCategory(@categoryid = :categoryid, @buid = :buid)}");
        $stateMent->bindParam(':categoryid', $categoryid, PDO::PARAM_INT);
        $stateMent->bindParam(':buid', $this->BUID, PDO::PARAM_INT);

        $stateMent->execute();
        $rows = $stateMent->fetchAll(PDO::FETCH_ASSOC);
        $data['Products'] = $rows;

        if (empty($data['Products'])) {
            $data['Products'] = [];
        }

        return $data;
    }

    public function SetProductOrderForCategory($data,$BUID) {
        if(!empty($BUID)){
            $this->BUID = $BUID;
        }else{
            $this->CheckAuthenticated();
        }
        if($this->BUID == null){
            throw new Exception("Sorry BUID is required");
        }

        $ProductImageModel = new ProductImageModel();
        $stateMent = $this->PDO->prepare("{CALL PHP_Ecommerce_SetProductOrderForCategory(@data = :data, @buid = :buid)}");
        $ProductCategories = $ProductImageModel->prepareCategotyProductOrderXML($data);
        $stateMent->bindParam(':data', $ProductCategories, PDO::PARAM_STR);
        $stateMent->bindParam(':buid', $this->BUID, PDO::PARAM_INT);

        $stateMent->execute();
        
        return true;
    }

}
