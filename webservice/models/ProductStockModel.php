<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProductStockModel
 *
 * @author dipankar
 */
require_once MODEL_PATH . DS . 'ProductModel.php';

class ProductStockModel extends ProductModel {

    //put your code here
    public $Table = 'Product_Stock';

    public function __construct() {
        parent::__construct();
    }

    public function addStock($productID, $StockAmount) {
        if (empty($productID)) {
            throw new Exception("Sorry product id needed to add stock");
        }
        if (empty($StockAmount)) {
            throw new Exception("Sorry stock needed to add stock");
        }
        $checkExistsStatement = $this->PDO->prepare("SELECT TOP 1 id from {$this->DbAlias}{$this->Table} where product_id=:productID");
        $checkExistsStatement->bindParam(':productID', $productID);
        $checkExistsStatement->execute();
        $data = $checkExistsStatement->fetch(PDO::FETCH_ASSOC);
        if (empty($data['id'])) {
            $sqlStatement = $this->PDO->prepare("INSERT INTO {$this->DbAlias}{$this->Table} (product_id,volume) VALUES (:productID,:stockamount)");
            $sqlStatement->bindParam(':productID', $productID);
            $sqlStatement->bindParam(':stockamount', $StockAmount);
            $sqlStatement->execute();
            return [
                'id' => $this->PDO->lastinsertId()
            ];
        }
    }

}
