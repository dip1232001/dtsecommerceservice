<?php 
// Server file
class PushNotifications
{

	// (Android)API access key from Google API's Console.
	private  $API_ACCESS_KEY = 'AIzaSyCReSAyJSxmdlv7DuoGHSSRsxzV1SC0Vtw';
	private $IOS_PEMPATH = WWW_ROOT . DS . 'pem' . DS . 'superdealDevPush.pem';
	// (iOS) Private key's passphrase.
	private static $passphrase = '';

	public function iOS($data, $devicetoken, $pempath = null)
	{
		if ($pempath != null) {
			$this->IOS_PEMPATH = WWW_ROOT . DS . 'pem' . DS . $pempath;
		}
		$deviceToken = $devicetoken;

		$ctx = stream_context_create();
		// ck.pem is your certificate file
		stream_context_set_option($ctx, 'ssl', 'local_cert', $this->IOS_PEMPATH);

		stream_context_set_option($ctx, 'ssl', 'passphrase', '');

		// Open a connection to the APNS server
		$fp = stream_socket_client(
			'ssl://gateway.push.apple.com:2195',
			$err,
			$errstr,
			60,
			STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT,
			$ctx
		);

		if (!$fp) {
			return ("Failed to connect: $err $errstr");
		}
			

		// Create the payload body
		$body['aps'] = array(
			'alert' => array(
				'title' => $data['mtitle'],
				'body' => $data['mdesc'],
				'userId' => $data['userId'],
				'type' => $data['type'],
				'url' => $data['url']
			),
			'sound' => 'default'
		);

		// Encode the payload as JSON
		$payload = json_encode($body,JSON_UNESCAPED_UNICODE);

		// Build the binary notification
		$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

		// Send it to the server
		$result = fwrite($fp, $msg, strlen($msg));
		
		// Close the connection to the server
		fclose($fp);

		if (!$result)
			return 'Message not delivered';
		else
			return 'Message successfully delivered';

	}
	
	// Curl 
	public function useCurl($url, $headers, $fields = null)
	{
	        // Open connection
		$ch = curl_init();
		if ($url) {
	            // Set the url, number of POST vars, POST data


			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	     
	            // Disabling SSL Certificate support temporarly
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			if ($fields) {
				curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
			}
	     
	            // Execute post
			$result = curl_exec($ch);
			print_r($result);
			if ($result === FALSE) {
				die('Curl failed: ' . curl_error($ch));
			}
	     
	            // Close connection
			curl_close($ch);

			return $result;
		}
	}
    
     // Sends Push notification for Android users
	public function android($data, $reg_id,$key=null)
	{
		if(!empty($key)){
			$this->API_ACCESS_KEY=$key;
		}
		$url = 'https://android.googleapis.com/gcm/send';
		$message = array(
			'title' => $data['mtitle'],
			'message' => $data['mdesc'],
			'subtitle' => '',
			'tickerText' => '',
			'msgcnt' => 1,
			'vibrate' => 1,
			'userId' => $data['userId'],
			'type' => $data['type'],
			'body' => $data['mdesc'],
			'url' => $data['url']
		);

		$headers = array(
			'Authorization: key=' . $this->API_ACCESS_KEY,
			'Content-Type: application/json'
		);

		$fields = array(
			'registration_ids' => array($reg_id),
			'data' => $message,
		);

		return self::useCurl($url, $headers, json_encode($fields));
	}

}
?>