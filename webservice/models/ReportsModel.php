<?php
require_once MODEL_PATH . DS . 'ProductModel.php';
require_once MODEL_PATH . DS . 'TaxModel.php';
class ReportsModels extends ProductModel
{
    public function __construct($callAuth = false)
    {
        parent::__construct();
    }

    public function SalesReport($data)
    {
        if(!empty($data['BUID'])){
            $this->BUID = $data['BUID'];
        }else{
            $this->CheckAuthenticated();
        }
        if($this->BUID == null){
            throw new Exception("Sorry BUID is required");
        }

        $statement = $this->PDO->prepare("{CALL PHP_Ecommerce_GetSalesReport (
                                                @startdate=:startdate,
                                                @enddate=:enddate,
                                                @BUID=:buid,
                                                @STATUS=:status
                                                )}");
        $statement->bindParam(':buid', $this->BUID, PDO::PARAM_INT);
        if (empty($data['startdate'])) {
             $statement->bindParam(':startdate', $data['startdate']=null, PDO::PARAM_INT);
        } else {
             $data['startdate']= date('Y-m-d 23:59:59', strtotime(str_replace(['/', '.'], '-', $data['startdate'])));
            $statement->bindParam(':startdate', $data['startdate'], PDO::PARAM_STR);
        }
        if (empty($data['enddate'])) {
              $statement->bindParam(':enddate', $data['enddate']=null, PDO::PARAM_INT);
        } else {
            $data['enddate']= date('Y-m-d 23:59:59', strtotime(str_replace(['/', '.'], '-', $data['enddate'])));
            $statement->bindParam(':enddate', $data['enddate'], PDO::PARAM_STR);
        }
        if (empty($data['status'])) {
            $data['status']=null;
        }
        $statement->bindParam(':status', $data['status'], PDO::PARAM_INT);
        if (!$statement->execute()) {
            throw new Exception(__t("Sorry cant featch sales reports"));
        }

        $orders = $statement->fetchAll(PDO::FETCH_ASSOC);
         $orderdetails=[];
        if (!empty( $orders )) {
            $orders= array_map(array($this, "json_decode_summary"), $orders);
            foreach ($orders as $order) {
                $order['datetime']=date('Y-m-d', strtotime($order['datetime']));
                $orderdetails[$order['datetime']]['ProductQuanitytCount']+=$order['ProductQuanitytCount'];
                $orderdetails[$order['datetime']]['grandtotal']+=$order['grandtotal'];
                $orderdetails[$order['datetime']]['orderProductCount']+=$order['orderProductCount'];
                $orderdetails[$order['datetime']]['datetime']=$order['datetime'];
                $orderdetails[$order['datetime']]['totalOrders']+=1;
            }
            $orderdetails=array_values($orderdetails);
        }
       
       
          
        return $orderdetails;
    }

    public function json_decode_summary(&$array)
    {
        $array['ordersummary'] = json_decode($array['ordersummary'], true);
        $array['orderProductCount']=count($array['ordersummary']['cartitems']);
      //  $array['ProductQuanitytCount'] = array_map(array($this, "addQUantity"), $array['ordersummary']['cartitems']);
        foreach ($array['ordersummary']['cartitems'] as $key => $value) {
            $array['ProductQuanitytCount']+=$value['cartQuantity'];
        }
        unset( $array['ordersummary'] );
        return $array;
    }
}
