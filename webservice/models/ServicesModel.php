<?php
require_once MODEL_PATH . DS . 'App.php';
require_once MODEL_PATH . DS . 'PushNotifications.php';

class ServicesModel extends AppModel
{

    //put your code here

    public function __construct()
    {
        parent::__construct();
    }

    public function setBuFeedStatus($data)
    {
        //dd($data,true);
        if (empty($data['buid'])) {
            throw new Exception(__t("Sorry in valid bu specified"));
        }

        if (empty($data['allowpush'])) {
            $data['allowpush'] = false;
        }
        if (empty($data['pageid'])) {
            throw new Exception(__t("Sorry pageid is required"));

        }
        if (empty($data['feedurl'])) {
            throw new Exception(__t("Sorry feed url is required"));

        }
        $stateMent = $this->PDO->prepare("{CALL PHP_Webservice_SetBuFeedPush(@buid=:buid,@allowpush=:allowpush,@feedurl=:feedurl,@timeupdated=:timeupdated,
                                            @pageid=:pageid)}");
        $stateMent->bindParam(':buid', $this->BUID = $data['buid'], PDO::PARAM_INT);
        $stateMent->bindParam(':allowpush', $data['allowpush'], PDO::PARAM_BOOL);
        $stateMent->bindParam(':feedurl', $data['feedurl'], PDO::PARAM_STR);
        if (!empty($data['timeupdated'])) {
            $stateMent->bindParam(':timeupdated', $data['timeupdated'], PDO::PARAM_STR);
        } else {
            $stateMent->bindParam(':timeupdated', $data['timeupdated'] = NULL, PDO::PARAM_INT);
        }
        $stateMent->bindParam(':pageid', $data['pageid'], PDO::PARAM_INT);
        $stateMent->execute();
        return true;
    }

    private function SendPush($title, $decription, $url, $companyid, $ioskey, $androidkey)
    {

        $getStatement = $this->PDO->prepare("{CALL PHP_Beacons_Status_get (@companyid = :companyid, @uuid=:uuid)}");
        $getStatement->bindParam(':companyid', $companyid, PDO::PARAM_INT);
        $getStatement->bindParam(':uuid', $uuid = null, PDO::PARAM_INT);
        $getStatement->execute();
        $devices = $getStatement->fetchAll(PDO::FETCH_ASSOC);
        $pushNotification = new PushNotifications();
        $androidDevices = [];
        foreach ($devices as $device) {
            if ($device['status'] == 0) {
                continue;
            }
            if (strtolower($device['type']) == 'android') {
                $pushresul['Android'][] = $pushNotification->android([
                    'mtitle' => $title,
                    'mdesc' => $decription,
                    'userId' => null,
                    'type' => null,
                    'url' => $url
                ], $device['uuid'], $androidkey);
            } else if (strtolower($device['type']) == 'ios') {
                $pushresul['IOS'][] = $pushNotification->iOS([
                    'mtitle' => $title,
                    'mdesc' => $decription,
                    'userId' => null,
                    'type' => null,
                    'url' => $url
                ], $device['uuid'], $ioskey);

            }
        }

        return $pushresul;

    }
    public function FeedPush($pushcertpathIos = 'superdealDevPush.pem', $androidKey = 'AIzaSyCReSAyJSxmdlv7DuoGHSSRsxzV1SC0Vtw')
    {
        $statement = $this->PDO->prepare("{CALL PHP_Webservice_GetBuFeedPush()}");
        $statement->execute();
        $feeds = $statement->fetchAll(PDO::FETCH_ASSOC);
        $pushres = [];
        foreach ($feeds as $feed) {
            $simplexml = simplexml_load_file($feed['feedurl']);
            $feedlastupdatedtime = strtotime((string)$simplexml->channel->item[0]->pubDate);
            $dblastupdatedtime = (!empty($feed['lastupdatedtime']) ? $feed['lastupdatedtime'] : null);

            if ($dblastupdatedtime == null || ($feedlastupdatedtime > $dblastupdatedtime)) {
                $domdoc = new DOMDocument();
                $domdoc->loadHTML((string)$simplexml->channel->item[0]->description);
                $pushres[] = $this->SendPush((string)$simplexml->channel->item[0]->title, (string)$domdoc->getElementsByTagName('p')->item(0)->nodeValue, (String)$simplexml->channel->item[0]->link, $feed['buid'], $pushcertpathIos, $androidKey);
                $this->setBuFeedStatus([
                    'buid' => $feed['buid'],
                    'allowpush' => $feed['allowpush'],
                    'pageid' => 37,
                    'timeupdated' => $feedlastupdatedtime,
                    'feedurl' => $feed['feedurl']
                ]);
            }

        }


        return $pushres;
    }

}
