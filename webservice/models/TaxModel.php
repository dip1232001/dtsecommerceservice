<?php

require_once MODEL_PATH . DS . 'App.php';

class TaxModel extends AppModel
{

    private $allowedRatetypes = [
        'Flat',
        'Percentage'
    ];
    private $allowedRuletypes = [
        '>'=>'greater_than',
        '>='=>'greater_than_equals',
        '='=>'equals',
        '<='=>'less_than_equals',
        '<'=>'less_than'
    ];

    public function __construct($callAuth = false)
    {
        parent::__construct();
        //$this->CheckAuthenticated();
    }


    public function AddTaxRule($data)
    {
        if(!empty($data['BUID'])){
            $this->BUID = $data['BUID'];
        }else{
            $this->CheckAuthenticated();
        }
        if($this->BUID == null){
            throw new Exception("Sorry BUID is required");
        }
        if (empty($data['rule_type']) || !in_array($data['rule_type'], array_keys($this->allowedRuletypes))) {
            throw new Exception(__t("Sorry invalid rule type !"));
        }
        if (strlen($data['amount']) == 0) {
            throw new Exception("Sorry amount is needed to add tax");
        }

        if (strlen($data['price']) == 0) {
            throw new Exception("Sorry tax price is needed to add tax");
        }
        if (strlen($data['rate_type']) == 0 || !in_array($data['rate_type'], $this->allowedRatetypes)) {
            throw new Exception("Sorry rate type is needed to add tax");
        }
        if (empty($data['id'])) {
            $data['id'] = null;
        }

        $statement = $this->PDO->prepare("{ CALL PHP_Ecommmerce_AddTaxRules(
                                                @id=:id,
                                                @amount=:amount,
                                                @ruletype=:ruletype,
                                                @price=:price,
                                                @selecttype=:selecttype,
                                                @buid=:buid,
                                                @TAXID=:returnid)}");
        $statement->bindParam(':id', $data['id'], PDO::PARAM_INT);
        $statement->bindParam(':amount', trim($data['amount']), PDO::PARAM_INT);
        $statement->bindParam(':ruletype', trim($data['rule_type']), PDO::PARAM_STR);
        $statement->bindParam(':price', trim(strip_tags($data['price'])), PDO::PARAM_INT);
        $statement->bindParam(':selecttype', trim(strip_tags($data['rate_type'])), PDO::PARAM_INT);
        $statement->bindParam(':buid', $this->BUID, PDO::PARAM_INT);
        $statement->bindParam(':returnid', $returnid, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 400);
        $statement->execute();
        if (empty($returnid)) {
            throw new Exception(__t("Sorry tax was not addded"));
        }

        return ['id' => $returnid];
    }

    public function TaxRuleStatusChange($data)
    {
        if(!empty($data['BUID'])){
            $this->BUID = $data['BUID'];
        }else{
            $this->CheckAuthenticated();
        }
        if($this->BUID == null){
            throw new Exception("Sorry BUID is required");
        }
        if (empty($data['id'])) {
            throw new Exception(__t("Sorry tax id is needed"));
        }
        $statement=$this->PDO->prepare("{ CALL PHP_Ecommerce_TaxRuleStatusupdate(
                                                @id=:id,
                                                @status=:status,
                                                @buid=:buid)}");
        $statement->bindParam(':id', $data['id'], PDO::PARAM_INT);
        $statement->bindParam(':status', $data['status'], PDO::PARAM_INT);
        $statement->bindParam(':buid', $this->BUID, PDO::PARAM_INT);
        if (!$statement->execute()) {
            throw new Exception(__t("Sorry tax status was not updated"));
        }
        return true;
    }

    public function getTaxRules($data)
    {
        if(!empty($data['BUID'])){
            $this->BUID = $data['BUID'];
        }else{
            $this->CheckAuthenticated();
        }
        if($this->BUID == null){
            throw new Exception("Sorry BUID is required");
        }
        if (empty($data['id'])) {
            $data['id']=null;
        }

        $statement= $this->PDO->prepare("{ CALL PHP_Ecommerce_GetTaxRules (
                                                @id=:id,
                                                @buid=:buid)}");
        $statement->bindParam(':id', $data['id'], PDO::PARAM_INT);
        $statement->bindParam(':buid', $this->BUID, PDO::PARAM_INT);
        if (!$statement->execute()) {
            throw new Exception(__t("Sorry can not get taxes"));
        }

        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }


    public function DeleteTax($data)
    {
        if(!empty($data['BUID'])){
            $this->BUID = $data['BUID'];
        }else{
            $this->CheckAuthenticated();
        }
        if($this->BUID == null){
            throw new Exception("Sorry BUID is required");
        }
        if (empty($data['id'])) {
            throw new Exception(__t("Sorry invalid tax id"));
        }
          $statement= $this->PDO->prepare("{ CALL PHP_Ecommerce_DeleteTax (
                                                @id=:id,
                                                @buid=:buid)}");

        $statement->bindParam(':id', $data['id'], PDO::PARAM_INT);
        $statement->bindParam(':buid', $this->BUID, PDO::PARAM_INT);
        if (!$statement->execute()) {
            throw new Exception(__t("Sorry can not get taxes"));
        }
        return true;
    }

    public function getTaxRulesFE($data)
    {
        if(!empty($data['BUID'])){
            $this->BUID = $data['BUID'];
        }else{
            $this->CheckAuthenticated();
        }
        if($this->BUID == null){
            throw new Exception("Sorry BUID is required");
        }
        if (empty($data['id'])) {
            $data['id']=null;
        }

        $statement= $this->PDO->prepare("{ CALL PHP_Ecommerce_GetTaxRulesFE (
                                                @id=:id,
                                                @buid=:buid)}");
        $statement->bindParam(':id', $data['id'], PDO::PARAM_INT);
        $statement->bindParam(':buid', $this->BUID, PDO::PARAM_INT);
        if (!$statement->execute()) {
            throw new Exception(__t("Sorry can not get taxes"));
        }

        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }


    public function CalculateTax($grandTotal)
    {
        if ($grandTotal==0) {
            return 0;
        }

        $data['BUID'] = $this->BUID;

        $taxRules=$this->getTaxRulesFE($data);
      
        if (empty($taxRules)) {
            return 0;
        }
        $matchedRules=[];
        usort($taxRules, function ($a, $b) {
            if ($a['amount']==$b['amount']) {
                return 0;
            }
            return $a['amount'] < $b['amount']?1:-1;
        });
        foreach ($taxRules as $key => $value) {
            if (in_array(trim($value['ruletype']), array_keys($this->allowedRuletypes))) {
                if ($this->allowedRuletypes[trim($value['ruletype'])]($grandTotal,trim($value['amount']))) {
                    $matchedRules=$value;
                    break;
                }
            }
        }
        if ($matchedRules['selecttype']=='Flat') {
                return (empty($matchedRules['price'])?0:$matchedRules['price']);
        }
        if ($matchedRules['selecttype']=='Percentage') {
                return (empty($matchedRules['price'])?0:(($grandTotal*$matchedRules['price'])/100));
        }
        return 0;
    }

    public function DeleteMultiTaxes($data)
    {
        if(!empty($data['BUID'])){
            $this->BUID = $data['BUID'];
        }else{
            $this->CheckAuthenticated();
        }
        if($this->BUID == null){
            throw new Exception("Sorry BUID is required");
        }

        //dd($data,true);
        foreach ($data["taxids"] as $key => $value) {
            $statement= $this->PDO->prepare("{ CALL PHP_Ecommerce_DeleteTax(@id=:id, @buid=:buid)}");
            $statement->bindParam(':id', $value, PDO::PARAM_INT);
            $statement->bindParam(':buid', $this->BUID, PDO::PARAM_INT);
            $statement->execute();
        }
        return true;
    }
}
