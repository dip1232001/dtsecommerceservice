<?php
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of WebcomponentModel
 *
 * @author Achyut Sinha
 */

require_once MODEL_PATH . DS . 'App.php';

class UploadfilesModel extends AppModel
{

    //put your code here

    public function __construct($callAuth = false)
    {
        parent::__construct($callAuth);
    }

    private function uploadFile($FILES, $data) {
        
        $client   = new S3Client([
                    'version' => 'latest',
                    'region' => $data['AWS_REGION'],
                    'credentials' => [
                    'key' => $data['AWS_ACCESS_KEY'],
                    'secret' => $data['AWS_SECRET_KEY']
            ]
        ]);

        $valid_formats = ["jpg", "png", "gif", "jpeg"];
        $FileXml = '';

        if ($FILES['Images']['error'] == 0) {
            $name = $FILES['Images']['name'];
            $size = $FILES['Images']['size'];
            $tmp = $FILES['Images']['tmp_name'];
            $type = $FILES['Images']['type'];

            if ($size > $max_size) {
                throw new Exception(__t("Sorry image " . $name . " size is bigger than accepted size"));
            }
            $ext = strtolower(getExtension($name));

            /*if (!in_array($ext, $valid_formats)) {
                throw new Exception(__t("Sorry image " . $name . " is not valid ,we accept " . implode(",", $valid_formats)));
            }*/

            $image_name_actual = time() . "." . $ext;
            try {
                $Result = $client->putObject(array(
                    'Bucket'        => $data['AWS_BUCKET_NAME'],
                    'Key'           => $data['FOLDER_NAME'] . '/' . $image_name_actual,
                    'SourceFile'    => $tmp,
                    'StorageClass'  => 'REDUCED_REDUNDANCY',
                    'ACL'           => 'public-read',
                    'ContentType'   => $type,
                    'Expires'       => date('Y-m-d H:i:s', strtotime("2 years"))
                ));
            } catch (S3Exception $exc) {
                throw new Exception("Aws S3 Error : " . $exc->getAwsErrorMessage());
            }

            return $Result['ObjectURL'];

        }
        return false;
    }
}
