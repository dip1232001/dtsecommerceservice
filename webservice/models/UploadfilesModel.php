<?php
use Aws\S3\Exception\S3Exception;
use Aws\S3\S3Client;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of WebcomponentModel
 *
 * @author Achyut Sinha
 */

require_once MODEL_PATH . DS . 'App.php';

class UploadfilesModel extends AppModel
{

    //put your code here

    public function __construct($callAuth = false)
    {
        parent::__construct($callAuth);
    }

    public function uploadFile($FILES, $data)
    {
        if (!empty($data['AWS_REGION']) && !empty($data['AWS_ACCESS_KEY']) && !empty($data['AWS_SECRET_KEY'])) {
            $client = new S3Client([
                'version' => 'latest',
                'region' => $data['AWS_REGION'],
                'credentials' => [
                    'key' => $data['AWS_ACCESS_KEY'],
                    'secret' => $data['AWS_SECRET_KEY'],
                ],
            ]);
        } else {
            $client = new S3Client([
                'version' => 'latest',
                'region' => AWS_REGION,
                'credentials' => [
                    'key' => AWS_ACCESS_KEY,
                    'secret' => AWS_SECRET_KEY,
                ],
            ]);
        }

        if(empty($data['AWS_BUCKET_NAME'])){
            $data['AWS_BUCKET_NAME'] = AWS_BUCKET_NAME;
        }

        if(empty($data['FOLDER_NAME'])){
            $data['FOLDER_NAME'] = 'AllUploads';
        }
        $valid_formats = ["jpg", "png", "gif", "jpeg"];
        $FileXml = '';

        if ($FILES['Images']['error'] == 0) {
            $name = $FILES['Images']['name'];
            $size = $FILES['Images']['size'];
            $tmp = $FILES['Images']['tmp_name'];
            $type = $FILES['Images']['type'];

            /*if ($size > $max_size) {
            throw new Exception(__t("Sorry image " . $name . " size is bigger than accepted size"));
            }*/
            $ext = strtolower(getExtension($name));

            /*if (!in_array($ext, $valid_formats)) {
            throw new Exception(__t("Sorry image " . $name . " is not valid ,we accept " . implode(",", $valid_formats)));
            }*/

            $image_name_actual = time() . "." . $ext;
            try {
                $Result = $client->putObject(array(
                    'Bucket' => $data['AWS_BUCKET_NAME'],
                    'Key' => $data['FOLDER_NAME'] . '/' . $image_name_actual,
                    'SourceFile' => $tmp,
                    'StorageClass' => 'REDUCED_REDUNDANCY',
                    'ACL' => 'public-read',
                    'ContentType' => $type,
                    'Expires' => date('Y-m-d H:i:s', strtotime("2 years")),
                ));
            } catch (S3Exception $exc) {
                throw new Exception("Aws S3 Error : " . $exc->getAwsErrorMessage());
            }

            //dd($Result, true);

            return ["URL" => $Result['ObjectURL'], "SIZE" => $size];

        }
        return false;
    }

    public function getImageData($data)
    {
        if ($data['url'] == "") {
            throw new Exception(__t("Enter URL."));
        }
        $url = $data['url'];
        $allow = ['gif', 'jpg', 'png']; // allowed extensions
        $img = file_get_contents($url);
        $url_info = pathinfo($url);
        $src = '';
        if (in_array($url_info['extension'], $allow)) {
            $src = 'data:image/' . $url_info['extension'] . ';base64,' . base64_encode($img);
        }
        echo $src;exit;
        return ['ImageData' => $src];
    }

    public function deleteFileS3($data){
        if (!empty($data['AWS_REGION']) && !empty($data['AWS_ACCESS_KEY']) && !empty($data['AWS_SECRET_KEY'])) {
            $client = new S3Client([
                'version' => 'latest',
                'region' => $data['AWS_REGION'],
                'credentials' => [
                    'key' => $data['AWS_ACCESS_KEY'],
                    'secret' => $data['AWS_SECRET_KEY'],
                ],
            ]);
        } else {
            $client = new S3Client([
                'version' => 'latest',
                'region' => AWS_REGION,
                'credentials' => [
                    'key' => AWS_ACCESS_KEY,
                    'secret' => AWS_SECRET_KEY,
                ],
            ]);
        }

        if(empty($data['AWS_BUCKET_NAME'])){
            $data['AWS_BUCKET_NAME'] = AWS_BUCKET_NAME;
        }

        if(empty($data['FOLDER_NAME'])){
            $data['FOLDER_NAME'] = 'AllUploads';
        }
        if (!empty($data['filename'])) {
            $client->deleteObject([
                'Bucket' => $data['AWS_BUCKET_NAME'],
                'Key'    => $data['FOLDER_NAME'] . '/' . $data['filename']
            ]);
        }

        return true;

    }
}
