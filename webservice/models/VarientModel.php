<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of VarientModel
 *
 * @author dipankar
 */
require_once MODEL_PATH . DS . 'ProductModel.php';

class VarientModel extends ProductModel {

    public $Table = 'Product_Varients';
    public $ProductVariantOptionsTable = 'Product_Varient_Options_Map';

    //put your code here
    public function __construct() {
        parent::__construct();
    }

    public function addVarient($data) {
        if (empty($data['isRequired'])) {
            $data['isRequired'] = false;
        }
        if (empty($data['sort_order'])) {
            $data['sort_order'] = 1;
        }
        if (empty($data['varientOptions'])) {
            throw new Exception("Sorry Varient options needed to add varient");
        }
        $this->__checkDuplicate($data['product_id'], $data['name']);
        $insertStatement = $this->PDO->prepare("INSERT INTO {$this->DbAlias}{$this->Table} (is_required,name,product_id,sort_order) VALUES (:is_required,:name,:product_id,:sort_order)");
        $insertStatement->bindParam(':is_required', $data['isRequired'], PDO::PARAM_BOOL);
        $insertStatement->bindParam(':name', $data['name'], PDO::PARAM_STR);
        $insertStatement->bindParam(':product_id', $data['product_id'], PDO::PARAM_INT);
        $insertStatement->bindParam(':sort_order', $data['sort_order'], PDO::PARAM_INT);
        $insertStatement->execute();
        if (($varientID = $this->PDO->lastInsertId()) != false) {
            $sqlStatement = "INSERT INTO {$this->DbAlias}{$this->ProductVariantOptionsTable}(varient_id,title,price,pricetype,sort_order)";
            $sqlValues = [];
            foreach ($data['varientOptions'] as $key => $varientOption) {
                if (empty($varientOption['title'])) {
                    throw new Exception("Sorry varient option title is required");
                }
                if (empty($varientOption['price'])) {
                    throw new Exception("Sorry varient option price is required");
                }
                if (empty($varientOption['pricetype'])) {
                    throw new Exception("Sorry varient option pricetype is required");
                }
                if (empty($varientOption['pricetype'])) {
                    $varientOption['pricetype'] = "+";
                }
                if (empty($varientOption['sort_order'])) {
                    $varientOption['sort_order'] = 1;
                }
                $sqlValues[] = "VALUES ({$varientID},{$varientOption['title']},{$varientOption['price']},{$varientOption['pricetype']},{$varientOption['sort_order']})";
            }
            if (empty($sqlValues)) {
                throw new Exception("Sorry Varient options needed to add varient");
            }
            $sqlStatement .= implode(",", $sqlValues);
            $optionsinsert = $this->PDO->prepare($sqlStatement);
            $optionsinsert->execute();
        }
    }

    private function __checkDuplicate($product_id, $name) {
        if (empty($name)) {
            throw new Exception("Sorry varient Name is required");
        }
        if (empty($product_id)) {
            throw new Exception("Sorry product is required");
        }
        $checkStatement = $this->PDO->prepare("SELECT count(*) as totaldups from {$this->DbAlias}{$this->Table} where product_id=:productID and name=:name");
        $checkStatement->bindParam(':productID', $product_id, PDO::PARAM_INT);
        $checkStatement->bindParam(':name', $name, PDO::PARAM_STR);
        $checkStatement->execute();
        $data = $checkStatement->fetch(PDO::FETCH_ASSOC);
        if ($data['totaldups'] > 0) {
            throw new Exception("{$name} varient is already added for this product");
        }
    }

    public function PrepareVarientXmlFromArray(&$varients) {
        if (!empty($varients)) {
            $varients['XML'] = '<data>';

            foreach ($varients as $key => $varient) {
                if (!is_numeric($key)) {
                    continue;
                }
                if (empty($varient['name'])) {
                    throw new Exception(__t("Sorry varient name is required"));
                }
                if (empty($varient['options'])) {
                    throw new Exception(__t("Sorry varient options are required"));
                }

                $varients['XML'] .= '<varient>';
                $varients['XML'] .= '<is_required>' . trim(strip_tags($varient['is_required'])) . '</is_required>';
                $varients['XML'] .= '<name><![CDATA[' . (trim(strip_tags($varient['name']))) . ']]></name>';
                $varients['XML'] .= '<sort_order>' . (int) (empty($varient['sort_order']) ? 1 : trim(strip_tags($varient['sort_order']))) . '</sort_order>';
                $varients['XML'] .= '<is_multiple><![CDATA[' . (empty($varient['is_multiple']) ? 0 : trim(strip_tags($varient['is_multiple']))) . ']]></is_multiple>';
                $varients['XML'] .= '<childrens>';
                foreach ($varient['options'] as $varientoption) {
                    if (strtolower($varientoption['title']) == 'select') {
                        continue;
                    }
                    if (empty($varientoption['title'])) {
                        throw new Exception("Sorry varient option title can not be blank");
                    }
                    if (empty($varientoption['pricetype'])) {
                        throw new Exception("Sorry varient option pricetype can not be blank");
                    }
                    if (trim($varientoption['pricetype']) == "+") {
                        $varientoption['pricetype'] = 1;
                    } else {
                        $varientoption['pricetype'] = 0;
                    }
                    if ($varientoption['price'] == '0.00' || $varientoption['price'] == '.00' || empty($varientoption['price'])) {
                        $varientoption['price'] = 0;
                    }
                    $varients['XML'] .= '<children>';
                    $varients['XML'] .= '<title><![CDATA[' . (trim(trim(strip_tags($varientoption['title'])))) . ']]></title>';
                    $varients['XML'] .= '<price><![CDATA[' . trim(strip_tags($varientoption['price'])) . ']]></price>';
                    $varients['XML'] .= '<pricetype><![CDATA[' . (string) ((empty($varientoption['pricetype'])) ? trim(strip_tags($varientoption['pricetype'])) : 1 ) . ']]></pricetype>';
                    $varients['XML'] .= '<sort_order>' . (int) (empty($varientoption['sort_order']) ? 1 : trim(strip_tags($varientoption['sort_order']))) . '</sort_order>';
                    $varients['XML'] .= '</children>';
                }
                $varients['XML'] .= '</childrens>';

                $varients['XML'] .= '</varient>';
            }
            $varients['XML'] .= '</data>';
        }
        return false;
    }

}
