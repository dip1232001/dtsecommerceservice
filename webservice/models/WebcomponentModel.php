<?php
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of WebcomponentModel
 *
 * @author Achyut Sinha
 */

require_once MODEL_PATH . DS . 'App.php';
//require_once MODEL_PATH . DS . 'EmailModel.php';

class WebcomponentModel extends AppModel
{

    //put your code here

    public function __construct($callAuth = false)
    {
        parent::__construct($callAuth);
    }

    public function uploadComponent($data, $file = [])
    {
        $directory = "../webComponents/";
        $valid_formats = ["zip"];
        $max_size = file_upload_max_size();

        if ($file[0]['component']['error'] == 0) {
            $name = $file['component']['name'];
            $size = $file['component']['size'];
            $tmp = $file['component']['tmp_name'];
            $type = $file['component']['type'];

            if ($size > $max_size) {
                throw new Exception(__t("Sorry " . $name . " size is bigger than accepted size"));
            }
            $ext = strtolower(end(explode(".", $name)));
            $fe = explode(".", $name);
            $fe = $fe[0];
            if (!in_array($ext, $valid_formats)) {
                throw new Exception(__t("Sorry " . $name . " is not valid ,we accept " . implode(",", $valid_formats)));
            }

            $filename = $directory . $name;
            if ($name != '') {
                move_uploaded_file($tmp, $filename);
                $zip = new ZipArchive;
                $x = $zip->open($filename);
                $filesInFolder = [];
                $filesNameInFolder = [];
                if ($x === true) {
                    $zip->extractTo($directory);
                    $zip->close();
                    //unlink($filename);
                    $dir = $directory . $fe;
                    if (is_dir($dir)) {
                        if ($dh = opendir($dir)) {
                            $i = 0;
                            while (($files = readdir($dh)) !== false) {
                                if($files == ".." || $files =="."){
                                    //$filesInFolder[$i] = $dir . "/" . $files;
                                    //$i++;
                                }else{
                                    $filesInFolder[$i] = $dir . "/" . $files;
                                    $filesNameInFolder[$i] = $files;
                                    $i++;
                                } 
                            }
                            closedir($dh);
                        }
                    }
                }
                $fileContaints = [];
                $SETTINGS = [];
                foreach($filesInFolder as $key=>$val){
                    if($filesNameInFolder[$key] == "settings.json"){
                        $SETTINGS = file_get_contents($val);
                    }
                }
                $SETTINGS = json_decode($SETTINGS,true);
                foreach($filesInFolder as $key=>$val){
                    $imagename = $this->uploadFile($val,$filesNameInFolder[$key], $SETTINGS["name"], $SETTINGS["type"]);
                    $fext = strtolower(end(explode(".", $filesNameInFolder[$key])));
                    $fileContaints[$key] = ['fileName'=>$filesNameInFolder[$key], 'fileType' => $fext, 'fileContent'=>$imagename];
                    /*if($filesNameInFolder[$key] == 'settings.json'){
                        $SETTINGS["Ndependencies"]["settings"][0] = ['fileName'=>$filesNameInFolder[$key], 'fileType' => $fext, 'fileContent'=>$imagename];
                    }*/                    
                }
                $imagename = $this->uploadFile($filename,$name, $SETTINGS["name"], $SETTINGS["type"]);
                $SETTINGS["ZipFile"] = $imagename;
                unlink($filename);
                system("rm -rf ".escapeshellarg($dir));

                $statement = $this->PDO->prepare("{CALL PHP_Component_AddComponent(
                    @Name=:name
                    , @version=:version 
                    , @type=:type
                    , @namespace=:namespace
                    , @zipURL=:zipURL
                    , @dependencies=:dependencies
                    , @appmenu=:appmenu 
                    , @editmenu=:editmenu
                    , @data=:data
                    , @ID=:id
                )}");
        
                $statement->bindParam(':name', $SETTINGS['name'], PDO::PARAM_STR);
                $statement->bindParam(':type', $SETTINGS['type'], PDO::PARAM_STR);
                $statement->bindParam(':version', $SETTINGS['version'], PDO::PARAM_STR);
                $statement->bindParam(':namespace', $SETTINGS['namespace'], PDO::PARAM_STR);
                $statement->bindParam(':zipURL', $SETTINGS['ZipFile'], PDO::PARAM_STR);
                $statement->bindParam(':dependencies', json_encode($SETTINGS['dependencies']), PDO::PARAM_STR);
                $statement->bindParam(':appmenu', json_encode($SETTINGS['appmenu']), PDO::PARAM_STR);
                $statement->bindParam(':editmenu', json_encode($SETTINGS['editmenu']), PDO::PARAM_STR);
                
                if (!empty($fileContaints)) {
                    $ProductVideos = $this->prepareXML($fileContaints);
                    //dd($ProductVideos, true);
                    if (!empty($ProductVideos)) {
                        $statement->bindParam(':data', $ProductVideos, PDO::PARAM_STR);
                    } else {
                        $statement->bindParam(':data', $ProductVideos = null, PDO::PARAM_INT);
                    }
                }else{
                    $statement->bindParam(':data', $PRODUCTVIDEOS = null, PDO::PARAM_INT);
                }

                $statement->bindParam(':id', $componentid, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 400);
                
                if (!$statement->execute()) {
                    throw new Exception(__t("Sorry job not added"));
                }
        
                $statement->closeCursor();

                

                /*foreach($SETTINGS["dependencies"] as $key=>$val){
                    foreach ($val as $key1 => $value1) {
                        foreach ($fileContaints as $key2 => $value2) {
                            if($value1 == $value2["fileName"]){
                                $SETTINGS["Ndependencies"][$key][$key1]=$value2;
                            }
                        }
                    }
                }*/
                //dd($SETTINGS);
                //dd($fileContaints, true);
                return ['ID' => $componentid];
            }
            {
                throw new Exception(__t("Please input a .zip files to upload."));
            }

        } else {
            throw new Exception(__t("Please input a .zip files to upload. czccxzczc"));
        }
    }

    private function uploadFile($file, $filename, $name, $type) {
        $client   = new S3Client([
                    'version' => 'latest',
                    'region' => AWS_REGION,
                    'credentials' => [
                    'key' => AWS_ACCESS_KEY,
                    'secret' => AWS_SECRET_KEY
            ]
        ]);
        try {
            $Result = $client->putObject(array(
                'Bucket'        => AWS_BUCKET_NAME,
                'Key'           => 'MODULES/'.$type.'/'.$name.'/' . $filename,
                'SourceFile'    => $file,
                'StorageClass'  => 'REDUCED_REDUNDANCY',
                'ACL'           => 'public-read'
            ));
        } catch (S3Exception $exc) {
            throw new Exception("Aws S3 Error : " . $exc->getAwsErrorMessage());
        }
        return $Result['ObjectURL'];
    }

    public function prepareXML($data) {
        $FileXml = '';
        foreach ($data as $key => $value) {
            $FileXml .= '<files>';
            $FileXml .= '<name>' . strip_tags(trim($value['fileName'])) . '</name>';
            $FileXml .= '<type>' . strip_tags(trim($value['fileType'])) . '</type>';
            $FileXml .= '<path>' . strip_tags(trim($value['fileContent'])) . '</path>';
            $FileXml .= '</files>';
        }
        if (strlen(trim($FileXml)) > 0) {
            return '<data>' . $FileXml . '</data>';
        }
        return false;
    }

    public function mapComponentWithBU($data)
    {
        $statement = $this->PDO->prepare("{CALL PHP_Component_MapBU(
            @BUID=:buid
            , @data=:data
        )}");

        $statement->bindParam(':buid', $data['buid'], PDO::PARAM_STR);
        
        if (!empty($data['components'])) {
            $ProductVideos = $this->prepareXMLC($data['components']);
            //dd($ProductVideos, true);
            if (!empty($ProductVideos)) {
                $statement->bindParam(':data', $ProductVideos, PDO::PARAM_STR);
            } else {
                $statement->bindParam(':data', $ProductVideos = null, PDO::PARAM_INT);
            }
        }else{
            $statement->bindParam(':data', $PRODUCTVIDEOS = null, PDO::PARAM_INT);
        }
        
        if (!$statement->execute()) {
            throw new Exception(__t("Sorry job not added"));
        }

        $statement->closeCursor();

        return true;
    }  
    
    public function prepareXMLC($data) {
        $FileXml = '';
        foreach ($data as $key => $value) {
            $FileXml .= '<component>';
            $FileXml .= '<cid>' . strip_tags(trim($value)) . '</cid>';
            $FileXml .= '</component>';
        }
        if (strlen(trim($FileXml)) > 0) {
            return '<data>' . $FileXml . '</data>';
        }
        return false;
    }

}
