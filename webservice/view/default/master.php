<!DOCTYPE html>
<html lang="en" data-ng-app="DestinationApp">
    <head>
        <script>
            function loadGTM(w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({'gtm.start':
                            new Date().getTime(), event: 'gtm.js'});
                var f = d.getElementsByTagName(s)[0],
                        j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src = 'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                f.parentNode.insertBefore(j, f);
            }
        </script>
        <link rel="preconnect" href="//maps.google.com" crossorigin="">
        <link rel="preconnect" href="//fonts.gstatic.com" crossorigin="">
        <link rel="preconnect" href="//fonts.googleapis.com" crossorigin="">
        <link rel="preconnect" href="//maxcdn.bootstrapcdn.com" crossorigin="">
        <?php
        View::loadView('partials/meta.php');
        ?>

    </head>
    <body>
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K4MCKTZ"
                          height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
        <div>
            <?php
            View::loadView('partials/navbar.php');
            View::loadView();
            View::loadView('partials/footer.php');
            if (!$_SESSION['CustomerID']) {
                View::loadView('partials/login.php');
                View::loadView('partials/register.php');
                View::loadView('partials/forgot_password.php');
                View::loadView('partials/reset_password.php');
            }
            View::loadView('partials/inviteTripEmailModal.php');
            View::loadView('partials/afterLoginModal.php');
            ?>
        </div>
        <script type="text/javascript">
            window.dataLayer = [];
            window.lang = '<?php echo $_SESSION['lang']; ?>';
            window.PurlID = '<?php echo $_SESSION['PURL']['PurlID']; ?>';
            window.mapKey = '<?php echo MAP_KEY; ?>';
            window.MAP_IMAGE_KEY = '<?php echo MAP_IMAGE_KEY; ?>';
            window.isMobile = false;
            window.FB_APP_ID = '<?php echo ((!IS_REWARD_SITE) ? FACEBOOK_ID : TRAVEL_CLUB_FACEBOOK_ID); ?>';
            window.cartTotal = '<?php echo (empty($_SESSION['PURL']['TripCounter']['InShoppingCartCount']) ? 0 : $_SESSION['PURL']['TripCounter']['InShoppingCartCount']); ?>';
            window.ItineraryCount = '<?php echo (empty($_SESSION['PURL']['TripCounter']['TotalItineraryCount']) ? 0 : $_SESSION['PURL']['TripCounter']['TotalItineraryCount']); ?>';
            window.forgotPasswordTag = '<?php echo $_GET['access_code']; ?>';
            window.conversionsRate = <?php echo json_encode($_SESSION['CurrencySession']); ?>;
            window.usercurrency = '<?php echo $_COOKIE['currency'] ?>';
            window.usercurrencySymbol = '<?php echo $_COOKIE['currencysymbol'] ?>';
            window.PusherKey = '<?php echo PUSHERKEY ?>';
            window.ShareBoxID = '<?php echo $_SESSION['PURL']['ShareBoxID'] ?>';
            window.FirstName = '<?php echo $_SESSION['FirstName'] ?>';
            window.LastName = '<?php echo $_SESSION['LastName'] ?>';
            window.PurlCustomerFirstName = '<?php echo $_SESSION['PURL']['PurlCustomerDetails']['FirstName'] ?>';
            window.PurlCustomerLastName = '<?php echo $_SESSION['PURL']['PurlCustomerDetails']['LastName'] ?>';
            window.PurlCustomerAvatar = '<?php echo $_SESSION['PURL']['PurlCustomerDetails']['avatar'] ?>';
            window.prerenderReady = false;
            window.PurlCustomerID = '<?php echo $_SESSION['PURL']['CustomerID'] ?>';
            window.AgentID = '<?php echo $_SESSION['PURL']['AgentID'] ?>';
            window.CompanyID = '<?php echo COMPANY_ID ?>';
            window.Avatar = '<?php echo $_SESSION['avatar'] ?>';
            window.ChatSupportEmail = '<?php echo CHAT_SUPPORT; ?>';
            window.languagePhrazes =<?php
            global $LANG;
            echo json_encode($LANG);
            ?>;
            window.IsLoggedIn = <?php echo (!empty($_SESSION['CustomerID']) && $_SESSION['CustomerID'] > 0 && !empty($_SESSION['STAG']) ? 1 : 0); ?>;
            window.LoginForm = '<?php echo $_SESSION['LoginFrom']; ?>';
            window.rewardsProgramMember = <?php echo (($_SESSION['IsPaid'] && IS_REWARD_SITE) ? 1 : 0); ?>;
        </script>        


        <?php if (APP_DEBUG): ?>
            <script src="/assets/default/js/vendor/jquery/jquery-3.1.1.min.js" type="text/javascript"></script>
            <script src="/assets/default/js/vendor/jquery/bootstrap.min.js"  type="text/javascript"></script>        
            <script src="/assets/default/js/vendor/angular/angular.min.js"></script>   
            <script src="/node_modules/angular-update-meta/dist/update-meta.min.js"></script>   
            <script src="/assets/default/js/vendor/angular-lazy-img/release/angular-lazy-img.min.js"></script>
            <script src="/assets/default/js/vendor/bootstrap-datepicker/bootstrap-datepicker.min.js" charset="utf-8"></script>       
            <script src="/assets/default/js/vendor/custom/custom.js"></script>           
        <?php else: ?>
            <script src="/assets/default/min/master.js?v=<?php echo VERSION; ?>" type="text/javascript"></script>
        <?php endif; ?>
        <script async src="https://maps.google.com/maps/api/js?key=<?php echo MAP_KEY; ?>"></script>
        <script src="//js.pusher.com/3.0/pusher.min.js"></script>
        <?php if (APP_DEBUG): ?>
            <script src="/assets/default/js/vendor/map/ng-map.js"></script>
            <script src="/assets/default/js/vendor/pusher/pusher-angular.min.js"></script>
            <script src="/assets/default/js/vendor/angular/angular-sanitize.js"></script>
            <script src="/assets/default/js/app/app.js"></script>        
            <script src="/assets/default/js/app/directive.js"></script>
            <script src="/assets/default/js/app/Chat/controllers/chatController.js"></script>        
            <script src="/assets/default/js/app/Chat/service.js"></script>
        <?php else: ?>
            <script src="/assets/default/min/master2.js?v=<?php echo VERSION; ?>" type="text/javascript"></script> 
        <?php endif; ?>
        <?php echo View::flushJs(); ?>
        <?php if (!APP_DEBUG): ?>
            <script type="text/javascript">"undefined" == typeof console ? window.console = {log: function () {}} : console.log = function () {};</script>
        <?php endif; ?>
        <script src="//28f5aec90cab41ff9ac436707a4c29cf.js.ubembed.com" async></script>
    </body>
</html>


