<!DOCTYPE html>
<html lang="en" data-ng-app="DestinationApp">
    <head>
        <!-- Google Tag Manager -->
        <script>(function (w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({'gtm.start':
                            new Date().getTime(), event: 'gtm.js'});
                var f = d.getElementsByTagName(s)[0],
                        j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src = 'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', 'GTM-K4MCKTZ');</script>
        <!-- End Google Tag Manager -->
        <link rel="preconnect" href="//maps.google.com" crossorigin="">
        <link rel="preconnect" href="//fonts.gstatic.com" crossorigin="">
        <link rel="preconnect" href="//maxcdn.bootstrapcdn.com" crossorigin="">
        <?php
        $viewObj->loadView('partials/meta.php');
        ?>

    </head>
    <body>
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K4MCKTZ" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
        <div>
            <?php
            $viewObj->loadView('partials/navbar.php');
            if ($detectMobile->isMobile()) {
                $viewObj->loadview('partials/searchNav');
            } else {
                $viewObj->loadview('partials/recommendedItineraryMenu.php');
            }
            $viewObj->loadView('partials/404-error');
            if (APP_DEBUG):
                ?>            
                <div style="border:4px double #FF0000; padding:15px; margin:10px;"><?php echo $errorMsg; ?></div>
                <?php
            endif;
            $viewObj->loadView('partials/footer.php');
            if (!$_SESSION['CustomerID']) {
                $viewObj->loadView('partials/login.php');
                $viewObj->loadView('partials/register.php');
                $viewObj->loadView('partials/forgot_password.php');
                $viewObj->loadView('partials/reset_password.php');
            }
            $viewObj->loadView('partials/inviteTripEmailModal.php');
            if ($detectMobile->isMobile()) {
                $viewObj->loadView('partials/startTrips');
            }
            ?>
        </div>
        <script type="text/javascript">
            window.lang = '<?php echo $_SESSION['lang']; ?>';
            window.PurlID = '<?php echo $_SESSION['PURL']['PurlID']; ?>';
            window.mapKey = '<?php echo MAP_KEY; ?>';
            window.MAP_IMAGE_KEY = '<?php echo MAP_IMAGE_KEY; ?>';
            window.isMobile = false;
            window.FB_APP_ID = '<?php echo FACEBOOK_ID; ?>';
            window.cartTotal = '<?php echo (empty($_SESSION['PURL']['TripCounter']['InShoppingCartCount']) ? 0 : $_SESSION['PURL']['TripCounter']['InShoppingCartCount']); ?>';
            window.forgotPasswordTag = '<?php echo $_GET['access_code']; ?>';
            window.conversionsRate = <?php echo $_COOKIE['conversionsRate']; ?>;
            window.usercurrency = '<?php echo $_COOKIE['currency'] ?>';
            window.usercurrencySymbol = '<?php echo $_COOKIE['currencysymbol'] ?>';
            window.PusherKey = '<?php echo PUSHERKEY ?>';
            window.ShareBoxID = '<?php echo $_SESSION['PURL']['ShareBoxID'] ?>';
            window.FirstName = '<?php echo $_SESSION['FirstName'] ?>';
            window.LastName = '<?php echo $_SESSION['LastName'] ?>';
            window.PurlCustomerFirstName = '<?php echo $_SESSION['PURL']['PurlCustomerDetails']['FirstName'] ?>';
            window.PurlCustomerLastName = '<?php echo $_SESSION['PURL']['PurlCustomerDetails']['LastName'] ?>';
            window.PurlCustomerAvatar = '<?php echo $_SESSION['PURL']['PurlCustomerDetails']['avatar'] ?>';
            window.prerenderReady = false;
            window.PurlCustomerID = '<?php echo $_SESSION['PURL']['CustomerID'] ?>';
            window.AgentID = '<?php echo $_SESSION['PURL']['AgentID'] ?>';
            window.CompanyID = '<?php echo COMPANY_ID ?>';
            window.Avatar = '<?php echo $_SESSION['avatar'] ?>';
            window.ChatSupportEmail = '<?php echo CHAT_SUPPORT; ?>';
        </script>        

        <?php if (APP_DEBUG): ?>
            <script src="/assets/default/js/vendor/jquery/jquery-3.1.1.min.js" type="text/javascript"></script>
            <script src="/assets/default/js/vendor/jquery/bootstrap.min.js"  type="text/javascript"></script>        
            <script src="/assets/default/js/vendor/angular/angular.min.js"></script>   
            <script src="/node_modules/angular-update-meta/dist/update-meta.min.js"></script>   
            <script src="/assets/default/js/vendor/angular-lazy-img/release/angular-lazy-img.min.js"></script>
            <script src="/assets/default/js/vendor/bootstrap-datepicker/bootstrap-datepicker.min.js" charset="utf-8"></script>       
            <script src="/assets/default/js/vendor/custom/custom.js"></script>           
        <?php else: ?>
            <script src="/assets/default/min/master.js?v=<?php echo VERSION; ?>" type="text/javascript"></script>
        <?php endif; ?>
        <script src="https://maps.google.com/maps/api/js?key=<?php echo MAP_KEY; ?>"></script>
        <script src="//js.pusher.com/3.0/pusher.min.js"></script>
        <?php if (APP_DEBUG): ?>
            <script src="/assets/default/js/vendor/map/ng-map.js"></script>
            <script src="/assets/default/js/vendor/pusher/pusher-angular.min.js"></script>
            <script src="/assets/default/js/vendor/angular/angular-sanitize.js"></script>
            <script src="/assets/default/js/app/app.js"></script>        
            <script src="/assets/default/js/app/directive.js"></script>
            <script src="/assets/default/js/app/Chat/controllers/chatController.js"></script>        
            <script src="/assets/default/js/app/Chat/service.js"></script>
        <?php else: ?>
            <script src="/assets/default/min/master2.js?v=<?php echo VERSION; ?>" type="text/javascript"></script> 
        <?php endif; ?>
        <?php echo $viewObj->flushJs(); ?>
        <?php if (!APP_DEBUG): ?>
            <script type="text/javascript">"undefined" == typeof console ? window.console = {log: function () {}} : console.log = function () {};</script>
        <?php endif; ?>
    </body>
</html>


